#!/usr/local/bin/perl -w 
##################################################################
# jobflow.pl
# This is the entry point for the JOBFLOW application
#
##################################################################

use strict;
use warnings;

use CGI qw/ Accept /;
use CGI::Carp qw( fatalsToBrowser carp cluck confess );
use CGI::Cookie;
use CGI::Session;

use URI qw();
use URI::QueryParam qw();
use HTML::Template;
use Template;
use Scalar::Util qw( looks_like_number );
use File::Copy;
use Net::FTP;
use JSON;
# use Time::HiRes qw/time/; uncomment to record elapsed time in under a second, otherwise used core::time

# Home grown
use lib 'modules';
use lib 'modules/Utils';

use Validate;
use SessionConstants;

require 'config.pl';
require 'general.pl';

use constant REQUIRE_DEVELOPER_PERMISSIONS => -1;
use constant REQUIRE_EXPLORER_PERMISSIONS => -2;		## Shitty hack for small test group access until new functions
													## can be globally accessed or  until we overhaul the terrible
													## user permissions system

my $debug = 0;

# Action mapping
# 1) Most commands use query params action and subaction, but some earlier ones do not, but if action is defined it must be alphanumeric
# 2) If action is undefined, we look for a small set of other query params such as changeLocation and adgateDeliver (so action becomes that key if found) 

our @legacy_actions = ( 'changeLocation',  'checkIn', 'unlock', 'assignjobs', 'requestjob', 'takejob', 'rejectjobsacctman', 'repoCheckIn',
			'repoCheckOut', 'repoSendJobs', 'adgateDeliver', 'alterEstimate', 'restoreJob', 'chooseDestination' );
	
# 3) The action is mapped to ( category, subaction ) -- see LEGACY::upgrade_action_map($action); so action now takes the form category/subaction
#    so for example dashboard => 'jobdashboard/view' and restorejob => 'job/restore'
# 4) Now we use the category as the key to access the $actions hash, giving an anonymous hash, which has keys which include subactions and also "_requires".
# 5) Check the anon hash to see if the subaction has an entry, die if not. This entry will be either a function handle or another anon hash.
# 6) if the sub action entry is a hash, then look for keys func (the function handler) and roles, a list of numeric role ids. 
#    If roles is present check that the current session roleid matches one of these (or skip if the developer flag is set) and die if no match
# 7) Now load the requires as shown by the _requires key for the category
# 8) Invoke the function, returning a value
# 9) or some commands, there is a bodge to handle the post processing
#    a) for the masters/jrs, the post handle action is to print the redirect URL
#    b) for action changeroleid, it displays the dashboard (so involves checking the role can use the dashboard)
# Exception handling
#     In the past, we just trap the exception and display an HTML page with an obsure error message.
#     However, the calls are using a mix of GET and POST requests and some of these require a JSON formatted response.
#     Brians first cut, uses the CGI Accept statement to determine of the request accepts JSON although perhaps we ought to test for mode=json too.
#     If sending JSON response to an error, then the payload includes a field "error" with the error text (perhaps shortened, to remove " at modules/...")
# Session/Logged in status handling
# a) If we dont have a session then display the Login dialog
# b) if the session says we are not logged on and action is not ProcessLogin then display the Login dialog
# c) if running from the command line, use config.default_userid to initialise the session.
# d) The masters/jrs redirects only happen if query.userid matches session.id or query.userid is undef (wierd).
# e) if we do display the Login dialog, save the current query (a la queryString) and unmarshall it after Login completes
 
############################################# See PD-2404
#############################################
##  ! IMPORTANT ! IMPORTANT ! IMPORTANT !  ##
##
## Rules for creating actions:
##   1. Don't
##   2. ** DON'T **
##   3. If you still must, use singular-form nouns
##   4. If what you want to add doesn't fit, put it in the 'legacy' category for now
##         and you will still be able to access it through the old links
##
## Rules for creating subactions:
##   1. Use verbs where possible (view, save, upload, retry, fix), but keep it
##		   within the context of the main action, or add the context as a second word
##   2. Separate words with underscores (view_dashboard)
##   3. Use one of the following where available:
##          list : To show a list of entries
##          view : To show the details of a single entry
##           add : To insert a new entry
##        remove : To delete a single entry
##          save : To update a single entry
##          link : Associate an entry with something
##        unlink : Disassociate an entry from something
##           get : To return JSON-formatted data
##   4. Always use lowercase for category and subaction names
##   5. Each sub action hash (if not a function handle), can optionally have
##		func - function handle (required)
##		roles - list of numeric roleids to match
##		norolecheck - skips the check for roles, so roles doesnt have to appear
##		postaction - action name to execute after the invocation of the current action
##		nologin - avoids the logged in check, so only used by login/logout
##		redirect - defers printing response header, uses response from invocation as URL to redirect to 
##
#############################################
#############################################

##		Category	 Action			[ Function ]
our $actions = { # Ordered by category

		broadcast => {
			_requires			=> 'broadcast.pl',
			get_details			=> \&Broadcast::Applypopup,
			apply_update		=> \&Broadcast::apply_update,
			save				=> \&Broadcast::update_broadcast,
			update_status_notes	=> \&Broadcast::updateStatusNotes,
			updatedatetime		=> \&Broadcast::update_datetime,
		},
		checklist => { 
			_requires			=> 'Checklist.pl',
			save_template			=> { func => \&Checklist::saveChecklistdata, roles => [ 4 ] },
			update_template			=> { func => \&Checklist::Savedescriptiondata, roles => [ 4 ] },		##TODO: Merge save_template with update_template, they do basically the same thing
			reorder_template		=> { func => \&Checklist::ApplyChecklistReorder, roles => [ 4 ] },
			view_template			=> { func => \&Checklist::Editchecklist, roles => [ 4 ] },
			save_checklist			=> { func => \&Checklist::Savejobchecklist, roles => [ 1,3,4,6 ] },
			# and additional ones from the legacy list
			checklistcustomers		=> { func => \&Checklist::getcustomernamebyopcoid, roles => [ 4 ] },
			checklist			=> { func => \&Checklist::CheckList, roles => [ 4 ] },
			updatedescriptiondata		=> { func => \&Checklist::Updatetaskstage, roles => [ 4 ] },
            savecombovalues             => {func => \&Checklist::saveChecklistdata, roles =>[4] },
			deletestagetask			=> { func => \&Checklist::Deletetaskstage, roles => [ 4 ] },
			showchecklistedit		=> { func => \&Checklist::Showchecklistedit, roles => [ 1,3,4,6 ] },
			companylist			=> { func => \&Checklist::companyList, roles => [ 4 ] },
            checklistroles      => \&Checklist::productchecklistroles,
		},
	
		comment => {
			_requires			=> 'jobs.pl',
			add				=> \&Jobs::addJobComment,
			save				=> \&Jobs::updateJobComment,
		},
	
		company => {
			_requires			=> 'companies.pl',
			list				=> { func => \&Companies::ListCompanies, roles => [ 4 ] },
			view				=> \&Companies::EditCompany, 					# TODO missing entry in legacy map
			add				=> { func => \&Companies::AddCompany, roles => [ 4 ] },
			save				=> { func => \&Companies::EditCompany, roles => [ 4 ] },
		},
	
		bundle => {
            _requires           => 'jobbundle.pl',
            sendemail           => \&JobBundle::sendEmail,
            download            => \&JobBundle::bundleDownload,
			find_assets			=> \&JobBundle::findAssets, 
			make_bundle			=> \&JobBundle::makeBundle,
        },
	
		compare => { 
			_requires			=> 'comparejobs.pl escomparejobs.pl',
			content				=> \&CompareJobs::actionHandler,
			es				=> \&ESCompareJobs::actionHandler,
		},
	
		customer => {
			_requires			=> 'opcos.pl',
			add				=> { func => \&Opcos::AddCustomer, roles => [ 4 ] },
			save				=> \&Opcos::EditCustomer,
			list				=> { func => \&Opcos::ListCustomers, roles => [ 4 ] },
			link_user			=> \&Opcos::AddCustomerUser,
			unlink_user			=> \&Opcos::RemoveCustomerUser,
			link_server			=> \&Opcos::AddCustomerFTP,
			save_server_link		=> \&Opcos::EditCustomerFTP,
			save_server_alternate		=> { func => \&Opcos::updateFTPServer, roles => [ 4 ] },
		},

# TODO Refactor customeruser
	
		dashboard => { 
			_requires				=> 'dashboard.pl',
			view					=> { func => \&Dashboard::newDashboard, roles => [ 1, 3, 4, 5, 6 ] },
			view_calendar			=> \&Dashboard::calendar,
			edit_calendar			=> \&Dashboard::calendarEdit,
			dasshboard_filter		=> \&Dashboard::dasshboardFilter,
			update_resource			=> \&Dashboard::updateResource,
		},
		digitalasset => {
			_requires			=> 'digitalassets.pl',
			constructassetdetails	=> \&DigitalAssets::constructAssetDetails,
			reviewdigitalasset		=> \&DigitalAssets::reviewDigitalAsset,
			remove_all          => \&DigitalAssets::deleteAllAssets,
		},
		digitaltab => {
			_requires		=> 'digital.pl',
			show_version	=> \&Digital::digitalShowVersion,
			roll_back		=> \&Digital::digitalRollBack,
			delete_asset	=> \&Digital::digitalDeleteAsset,
			static_file_upload => \&Digital::staticFileUploaded,
			digital_delete_file => \&Digital::erase,
		},
		document => {
			 _requires		=> 'document.pl',
			show_version	=> \&Document::showversion,
			roll_back		=> \&Document::rollback,
			delete_asset	=> \&Document::deleteAsset,
			delete_document	=> \&Document::eraseDocument,
		},
		estimate => {
			_requires			=> 'estimate.pl',
			alterestimate			=> \&Estimate::AlterEstimate,
			save				=> \&Estimate::updateEstimate,
			processnewestimate		=> { func => \&Estimate::processNewEstimate, roles => [ 1, 3, 4, 5, 6 ], postaction => 'dashboard' },
		},
		file => {
			_requires			=> 'jobs.pl',
			remove_cache			=> \&Jobs::deleteCache,
			send_pdf_to_cmd			=> \&Jobs::sendRenderedPdfToCMD,
			renderpdf			=> \&Jobs::renderPdf,
		},

		form => {
			_requires			=> 'forms.pl',
			list				=> \&Forms::ListForms,
			view				=> \&Forms::EditForm,
			view_alternate			=> \&Forms::ViewForm,
			get_brand			=> \&Forms::GetBrandDetails,
			get_printer			=> \&Forms::GetPrinterDetails,
			get_brandowner		=> \&Forms::GetBrandOwnerDetails,
			send_xml			=> \&Forms::sendXml,
			open_fileupload		=> \&Forms::OpenFileUpload,
			upload_tas_document => \&Forms::UploadTasDocument,
			upload_tas_document_temp_location	=> \&Forms::UploadTasDocumentToTemp,
			delete_temp_tas_document	=>	\&Forms::DeleteDocumentsFromTemp,
			get_cutter_ref		=> \&Forms::GetCutterRefDetails,
			get_packformat_details => \&Forms::GetPackFormatDetails,
			get_templateref_details => \&Forms::GetTemplateRefDetails,
			get_allergens_underlined => \&Forms::GetAllergensUnderlined,
								 
			## AB Inbev Forms
			view_abinbev			=> \&Forms::ShowAbForm,
			save_abinbev			=> \&Forms::SaveAbInbevForm,	
			commit_to_production		=> \&Forms::CommitToProduction,
		},
		group => {
			_requires			=> 'jobs.pl',
			checkgroupname			=> \&Jobs::CheckGroupName,
			sendgrouplist			=> \&Jobs::SendGroupList,
			showcreategroup			=> \&Jobs::GetUsers,
			viewgroupname			=> \&Jobs::GetGroup,
			deletegroupname			=> \&Jobs::DeleteGroupName,
			viewgroupdetails		=> \&Jobs::viewGroupDetails,
		},
		imageworkflow => {
			_requires			=> 'imageworkflow.pl',
			add				=> \&Imageworkflow::BookImage,
			save				=> \&Imageworkflow::EditImageworkflow,
		},
		job => { 
			_requires => 'jobs.pl audit.pl applyselection.pl',
			restore				=> \&Jobs::RestoreJobs,
			request				=> \&Jobs::RequestJob,
			take				=> \&Jobs::TakeJob,
			assign				=> \&Jobs::AssignJobs,
			unlock				=> { func => \&Jobs::UnlockJobs, postaction => 'dashboard', roles => [ 4, 6 ], }, 
			complete			=> \&Jobs::completeJob,
			reject				=> \&Jobs::rejectJob,
			checkin				=> \&Jobs::CheckIn,
			changelocation			=> \&Jobs::ChangeLocation,
			qc_status			=> \&Jobs::QCstatus,
			processcheckin			=> \&Jobs::processCheckIn,
			setlocation			=> \&Jobs::SetLocation,
			recordopenjob			=> \&Jobs::recordOpenJob,

			showtaskprocessdialog		=> \&Jobs::ShowTaskProcessDialog,
			view_history			=> \&Jobs::ViewHistory,
			view_activity			=> { func => \&Jobs::ViewActivity, roles => [ 4 ] },
			view_log			=> { func => \&Audit::ShowLogs, roles => [ 1, 3, 4, 5, 6 ] },
			applyselection		=> \&Applyselection::ApplytoSelection,
			updateapplytoselection => \&Applyselection::updateApplytoSelection,
		},
		jobdashboard => {
			_requires				=> 'joblist.pl', # Please dont add other modules to this heavily used action
			view						=> { func => \&Joblist::Dashboard, roles => [ 1, 3, 4, 5, 6, 7, 8 ] }, # maybe should be norolecheck => 1
		},
		jobimport => {
			_requires			=> 'job_import.pl',
			view				=> { func => \&JobImport::view, roles => [ REQUIRE_EXPLORER_PERMISSIONS ] },
			search				=> { func => \&JobImport::search, roles => [ REQUIRE_EXPLORER_PERMISSIONS ] },
			viewsettings		=> { func => \&JobImport::viewsettings, roles => [ REQUIRE_EXPLORER_PERMISSIONS ] },
			editjobimportfield	=> { func => \&JobImport::editjobimportfield, roles => [ REQUIRE_EXPLORER_PERMISSIONS ] },
			savejobimportfield	=> { func => \&JobImport::savejobimportfield, roles => [ REQUIRE_EXPLORER_PERMISSIONS ] },
			deletejobimportfield => { func => \&JobImport::deletejobimportfield, roles => [ REQUIRE_EXPLORER_PERMISSIONS ] },
			viewimportsubstitutions		=> { func => \&JobImport::viewimportsubstitutions, roles => [ REQUIRE_EXPLORER_PERMISSIONS ] },
			editjobimportsubstitution	=> { func => \&JobImport::editjobimportsubstitution, roles => [ REQUIRE_EXPLORER_PERMISSIONS ] },
			savejobimportsubstitution	=> { func => \&JobImport::savejobimportsubstitution, roles => [ REQUIRE_EXPLORER_PERMISSIONS ] },
			deletejobimportsubstitution => { func => \&JobImport::deletejobimportsubstitution, roles => [ REQUIRE_EXPLORER_PERMISSIONS ] },
			},
		kanban  => {
			_requires						=> 'kanbanboard.pl',
			kanbanboard_view				=> \&KanbanBoard::kanbanBoard,
			updatekanbanboard				=> \&KanbanBoard::updateKanbanboard,
			swimlaneconfig					=> \&KanbanBoard::swimlaneConfigBoard,
			getswimlaneorderbyvalues		=> \&KanbanBoard::getSwimLaneOrderByValues,
			updateswimlane					=> \&KanbanBoard::updateSwimLane,
			filterowneraccess				=> \&KanbanBoard::checkFilterOwnerAccess,
			swimlaneuserlist				=> \&KanbanBoard::getSwimLaneUsersList,
			checkqcstatus					=> \&KanbanBoard::checkQCstatus,
			kanban_get_comments				=> \&KanbanBoard::getBoardviewBriefComments,
		},
		legacy => {
			_requires => 'jobs.pl groups.pl dashboard.pl',
			updatedepartment		=> \&Groups::update_department_status,
			updateteam			=> \&Groups::update_team_status,
			updatequerytype			=> \&Groups::update_querytype,
			getlanguagelist			=> \&Jobs::get_language_list,
			rejectjobsacctman		=> \&Jobs::RejectJobsAcctMan,
		},
		maintenance => {
			_requires			=> 'jobs.pl daemonpanel.pl',
			list_repo_checkin		=> { func => \&Jobs::CheckInQueue, roles => [ 4 ] },
			list_repo_checkout		=> { func => \&Jobs::CheckOutQueue, roles => [ 4 ] },
			list_repo_history		=> { func => \&Jobs::CheckInQueueHistory, roles => [ 4 ] },
			list_daemons			=> { func => \&DaemonPanel::listDaemons, roles => [ REQUIRE_DEVELOPER_PERMISSIONS ] },
		},
		masters => {
			_requires                       => 'masters.pl',
			masters				=> { func => \&Masters::masters, redirect => 1, norolecheck => 1 },
			mastersqueue			=> { func => \&Masters::masterstudios, redirect => 1, norolecheck => 1 },
			jrs				=> { func => \&Masters::jrs, redirect => 1, norolecheck => 1 },
		},
		message => { 
			_requires			=> 'jobs.pl',
			view				=> \&Jobs::GetMessageView,
			add				=> \&Jobs::SendMessage,
			remove				=> \&Jobs::DeleteMessage,
			list				=> { func => \&Jobs::GetMessageList, roles => [ 1, 3, 4, 5, 6 ] },
			get_count			=> { func => \&Jobs::GetMessageCount, roles => [ 1, 3, 4, 5, 6 ] },
			showsend			=> \&Jobs::GetGroupUsers,
			checkjoblocation		=> \&Jobs::Msgjoblocation,
		},
		news => {
			_requires			=> 'news.pl',
			add				=> { func => \&News::AddNews, roles => [ 4 ] },
			list				=> \&News::NewsSummary,
			list_alternate			=> { func => \&News::ListNews, roles => [ 4 ] }, ## Literally the same thing as list_news
			save				=> \&News::EditNews,
		},
		one2edit => {
			_requires			=> 'one2edit.pl',
			action_handler		=> \&One2Edit::action_handler,
		},
		opco => { 
			_requires			=> 'opcos.pl useredit.pl',
			list				=> { func => \&Opcos::ListOperatingCompanies, roles => [ 4 ] },
			view				=> \&Opcos::EditOperatingCompany,
			add				=> { func => \&Opcos::AddOperatingCompany, roles => [ 4 ] },
			save				=> { func => \&Opcos::EditOperatingCompany, roles => [ 4 ] },
			link_user			=> { func => \&UserEdit::AddUsersToOpco, roles => [ 4 ] },		# TODO move to useredit category ?
			save_vendor			=> { func => \&Opcos::EditOperatingCompanyVendors, roles => [ 4 ] },
			getmediaestimation		=> \&Opcos::getCustomerDefaultEstimate,
		},
		opcocmd => {
			_requires                       => 'opcosCMD.pl',
			archivescheduler		=> { func => \&OpcosCMD::ArchiveScheduler, roles => [ 4 ] },
			opcoadmincmd			=> { func => \&OpcosCMD::OpcoAdminCMD, roles => [ 4 ] },
		},
		outsource => { 
			_requires			=> 'outsource.pl',
			add				=> { func => \&Outsource::AddOutsource, roles => [ 4 ] },
			list				=> { func => \&Outsource::ListOutsources, roles => [ 4 ] },
			save				=> \&Outsource::EditOutsource,
			remove				=> { func => \&Outsource::DeleteOutsource, roles => [ 4 ], postaction => 'listoutsource' },
		},
		product => { 
			_requires			=> 'products.pl databaseJob.pl',
			start				=> \&Products::StartProduct,   # TODO refactor as products.pl
			pause				=> \&Products::PauseProduct,
			stop		 		=> \&Products::StopProduct,
								 
			updateProductChecklistEndTime => \&ECMDBJob::updateProductEndTime,  # used in js/checklistdata.js TODO1
			list_reasons			=> \&Products::GetProductRestartReasons,
			addrevisionreason		=> \&Products::AddRevisionReason,
			view				=> \&Products::viewTimeCapture,
			view_history			=> \&Products::ViewProductHistory,
			link_operator			=> \&Products::AssignProduct,
			link_department			=> \&Products::finishProductAssignToDept,
			exclude_product			=> \&Products::excludeProduct,
			get_latest_version_products => \&Products::getLatestVersionProducts,
		},
		report => {
			_requires			=> 'reports.pl',
			list				=> { func => \&Reports::ListReports, roles => [ 4 ] },
			view				=> { func => \&Reports::ShowReport, roles => [ 4 ] },
			add				=> { func => \&Reports::AddReport, roles => [ 4 ] },
			save				=> { func => \&Reports::EditReport, roles => [ 4 ] },
			remove				=> { func => \&Reports::DeleteReport, postaction => 'listreports', roles => [ 4 ] }, 
		},
		repository => { 
			_requires			=> 'jobs.pl repojobs.pl',
			send_opts			=> \&Jobs::RepoSendJobs,
			checkin_opts			=> \&Jobs::RepoCheckIn,
			checkout_opts			=> \&Jobs::RepoCheckOut,
			send				=> \&Jobs::processRepoSendJobs,
			checkin				=> \&Jobs::processRepoCheckIn,
			checkout			=> \&Jobs::processRepoCheckOut,
			retry				=> \&RepoJobs::RetryRepoJob,
			reset				=> \&RepoJobs::ResetRepoJobs,
			fix				=> \&RepoJobs::BaseRepoJob,
			list				=> \&RepoJobs::FixRepoJobs,
		},
		search => {
			_requires                       => 'search.pl',
			searchjobs						=> \&Search::SearchJobs,
		},
		sendjobs => {
			_requires                       => 'sendjobs.pl databaseProducts.pl databaseCompanies.pl',
			approve				=> \&SendJobs::Approve,
			download_asset                  => { func => \&SendJobs::DownloadAsset, roles => [ 2, 4 ] },
			download_lowres                 => \&SendJobs::getLowResZip,
			view_lowres                     => \&SendJobs::ViewLowRes,
			viewpreflight			=> \&SendJobs::ViewPreflight,
			gethires			=> \&SendJobs::GetHiRes,
			choosedestination		=> \&SendJobs::chooseDestination,
			sendbyemail			=> \&SendJobs::sendByEmail,
			sendbyemailclient		=> \&SendJobs::sendByEmail,
			sendbyemailmediaowner		=> \&SendJobs::sendByEmail,
			sendbyemailvendor		=> \&SendJobs::sendByEmail,
			sendbyemaildigital		=> \&SendJobs::sendByEmail,
			sendbyemailindex		=> \&SendJobs::sendByEmail,
			sendByftp			=> \&SendJobs::sendByFTP,
			adgatedeliver			=> \&SendJobs::AdGateDeliver,
			sendbyproject			=> \&SendJobs::Sendbyproject,
		},
		showjob => {
			_requires			=> 'showjob.pl',
			showjob				=> \&ShowJob::Showjob, 
			upload_asset		=> \&ShowJob::uploadSelectedAllAssets, # deprecated
			removetempdocuments		=> \&ShowJob::removetempdocuments,
            checkcampaign           => \&ShowJob::Showjob, #Pass campaign status flag
		},
		
		support => {
			_requires                       => 'support.pl',
			support				=> \&Support::SupportPage,
		},
		upload => {
			_requires			=> 'upload.pl',
			upload				=> { func => \&Upload::upload, roles => [ 1, 4, 6 ] }, # Has subaction handler
		},
		useredit => { 
			_requires			=> 'useredit.pl',
			list				=> { func => \&UserEdit::Listusers, roles => [ 4 ] },
			add				=> { func => \&UserEdit::Adduser, roles => [ 4 ] },
			add_vendor			=> { func => \&UserEdit::Addvendor, roles => [ 4 ] },
			save				=> { func => \&UserEdit::Edituser, roles => [ 4 ] },
			edituser			=> { func => \&UserEdit::Edituser, roles => [ 4 ] },  # TODO1 is this needed ?
			view_settings			=> { func => \&UserEdit::EditSettings, roles => [ 1, 3, 4, 5, 6 ] },
			save_settings			=> \&UserEdit::EditSettings,
			remove				=> { func => \&UserEdit::Deleteuser, postaction => 'listusers', roles => [ 4 ] },
			dashboardeditresource		=> \&UserEdit::dashboardEditResource,
		},
		usercolumnmanager => {
			_requires                       => 'usercolumnmanager.pl',
			apply				=> \&UserColumnManager::ApplyColumnManager,
			update				=> \&UserColumnManager::UpdateColumnManager,
			showcolumnmanagerdialog		=> \&UserColumnManager::ShowColumnManagerDialog,
		},
		userfilter => {
			_requires               => 'userfilter.pl',
			save_filter				=> \&UserFilter::SaveFilter,
			remove_filter			=> \&UserFilter::DeleteFilter,
			rename_filter			=> \&UserFilter::RenameFilter,
			get_filter				=> \&UserFilter::LoadFilter,			## AJAX
			showsavefilterdialog	=> \&UserFilter::ShowSaveFilterDialog,
			filterpinning			=> \&UserFilter::updateFilterPinning,
		},
		userlogin => {
			_requires                       => 'users.pl',
			login				=> { func => \&Users::ProcessLogin, nologin => 1, norolecheck => 1 },
			logout				=> { func => \&Users::ProcessLogout, nologin => 1, norolecheck => 1 },
			rolechange			=> { func => \&Users::HandleRoleidChange, postaction => 'dashboard', norolecheck => 1 },
			sessiondetails		=> \&Users::get_session_details,
		},
		watchlist => { 
			_requires			=> 'Watchlist.pl',
			add				=> \&Watchlist::addwatchlist,
			remove				=> \&Watchlist::stopwatchlist,
		},
	 	digital   => {
			_requires			=> 'digitalIndexPage.pl',
			show				=> \&Digitalindexpage::ShowDigitalIndexPage,
		},

}; # end of $actions
			
##################################################################
# defaults
#
##################################################################

##################################################################
# CGI and session Initialisation
#
##################################################################

## Used to determine if we are running in a CGI context, for debugging reasons
use constant IS_MOD_PERL => exists $ENV{'MOD_PERL'};
use constant IS_CGI      => IS_MOD_PERL || exists $ENV{'GATEWAY_INTERFACE'};

use constant DEFAULT_SESSION_EXPIRY => '+6400m'; # about 4 days

# Log time used for an action
# - status is either 0 or 1, 0 is ok, 1 if the action failed (threw an exception)
#
# To record time less that 1 second, uncomment the line at the top of this file
# use Time::HiRes qw/time/;
# and change the calculation of elapsed to something like
# my $elapsed = int(($end_time - $start_time) * 1000) / 1000;
# Gives seconds with 3 dps

sub log_action {
	my ( $userid, $roleid, $action, $start_time, $status ) = @_;

	$userid = 0 unless defined $userid;
	$roleid = 0 unless defined $roleid;
	$status = 0 unless defined $status;
	$action = 'undef' unless defined $action;

	my $log_action = Config::getConfig( 'action_log' ) || 0;			# 0 to disable, 1 to enable log to action_log table
	my $log_threshold = Config::getConfig( 'action_threshold' ) || 0;		# elapsed has to be this or more to log

	my $log_exclude = Config::getConfig( 'action_exclude' ) || '';		# List of actions to exclude from log if any
	return if ( ( $log_exclude ne '' ) && ( $action =~ /$log_exclude/i ) );

	my $end_time = time();
	my $elapsed = $end_time - $start_time; # Time in seconds (rounded down, see note above)

	if ( $elapsed >= $log_threshold ) {
		if ( $log_action ) {
			eval {
				$Config::dba->process_sql("INSERT INTO action_log ( userid, roleid, action, end_time, elapsed_time, failed ) 
					VALUES ( ?, ?, ?, now(), ?, ? )", [ $userid, $roleid, $action, $elapsed, $status ]);
			};
			if ( $@ ) {
				carp "log_action: Failed userid $userid, $roleid, action $action, start $start_time, status $status, $@<br>";
			}
		}
	}
}

sub test_action { # fudge to get round once warning for Config::dba used in log_action above
	my ( $userid ) = @_;
	my ( $count ) = $Config::dba->process_oneline_sql( "SELECT COUNT(*) FROM action_log WHERE userid = ?", [ $userid ] );
	return $count;
}

sub get_action {
	my ( $query ) = @_;
	
	my $action = $query->param( 'action' );
	if ( defined $action ) {
		## An interim bodge to take care of selectVendor 'subactions'
		foreach my $fake_action ( @legacy_actions ) {
			if($query->param($fake_action)) {
				$action = $fake_action;
				last;
			}
		}
	}
	$action = "dashboard" unless defined $action;
	die "Invalid action '$action'" unless ( Validate::isAlphaNumeric( $action ) );
	return $action;
}

# Lookup supplied action, return subaction hash or die trying

sub lookup_action {
	my ( $query, $session, $action ) = @_;

	## Parse the category/action out of the incoming action
	my $category = $query->param('category');
	my $subaction = $query->param('subaction');
	my $newaction = LEGACY::upgrade_action_map( lc($action) ); # eg 'reports/save' for action savereport
	($category, $subaction) = $newaction =~ m/^(.*)\/(.*)$/;

		
	## Load only the relevant modules
	my $category_map = $actions->{$category};
	die "No valid category for '$category/$subaction', action '$action'" if !defined $category_map;

	my $subaction_map = $category_map->{ $subaction };
	die "No valid action for sub action '$category/$subaction', action '$action'" if !defined $subaction_map;

	# Caller expects a hash, even if (in some cases its just a function handle)
	if ( ref $subaction_map eq 'HASH' ) {
		my $func = $subaction_map->{func};
		die "Subaction map doesnt have valid function, for sub action '$category/$subaction', action '$action'" unless ( ( defined $func ) && ( ref $func eq "CODE" ) );
	} elsif ( ref $subaction_map eq 'CODE' ) {
		my $func = $subaction_map;
		$subaction_map = { func => $func, norolecheck => 1, };
	} else {
		die "Got subaction map other than hash or function, for sub action '$category/$subaction', action '$action'";
	}
	
	# Load the required modules here (the alternative would be to return the requires list as an array)
	die "Category $category, action $action', no _requires map" unless exists $category_map->{_requires};

	foreach my $require (split(/ /, $category_map->{_requires})) { 
		eval {
			if ( ! exists $INC{$require} ) {
				require "modules/$require";
				"modules/$require"->import();
			}
		};
		if ( $@ ) {
			die "Failed to import '$require' (for category '$category', action '$action'): $@";
		}
	}

	return ( $category, $subaction, $subaction_map );
}

# Check whether user (in their current role), is authorised to use the action
# Returns 0 or 1, 1 if authorised

sub action_authorised {
	my ( $userid, $roleid, $roles ) = @_;

	die "Missing list of roles" unless ( ( defined $roles ) && ( ref $roles eq "ARRAY" ) );

	my $is_developer = Users::isDeveloper( $userid );
	my $is_explorer = Users::isExplorer( $userid );
	my $authorised = 0;
	foreach my $role ( @{ $roles } ) {
		if (	( $role == $roleid ) ||
			( ($role == REQUIRE_DEVELOPER_PERMISSIONS) && $is_developer ) ||
			( ($role == REQUIRE_EXPLORER_PERMISSIONS) && $is_explorer )
			) {
			$authorised = 1;
		} 
	}
	return $authorised;
}

## Create the query and session and cookie
my $query = new CGI;
my $sid = $query->cookie("CGISESSID");
my $session = new CGI::Session("driver:File", $sid, { Directory => '/tmp' } );

# $session->expire( Config::getConfig( 'sessionexpiry' ) || DEFAULT_SESSION_EXPIRY );   # defer until after database connection

my $cookie;
my $cookie2;
my $viaCmdLine = ( IS_MOD_PERL || IS_CGI ) ? 0 : 1; # set viaCmdLine if via command line
my $printed_header = 0; # Keep track of whether we've output header as its deferred in some cases

# The following are here, so there are available in exception handler and for log_action call

my $start_time = 0; # used for calls to log_action (which determines end time and hence elapsed time)
my $action;
my $userid;
my $roleid;
my $session_warning;

eval { # Main eval block

	die "No session" unless defined $session;

	$cookie = $query->cookie( -name =>'CGISESSID', -value => $session->id, -httponly => 1 );


	## More gracefully handle database errors
	eval {
		Config::initialise_config();
		
		require 'legacy.pl';
		require 'general.pl';
		require 'users.pl';
	};
	if ( $@ ) {
		die "Database error: Failed on connect, reading config, or initial requires, $@";
	}
	#DBAgent::enable_debug_messages();
	$cookie2 = $query->cookie( -name =>'CLIENTSESSIONUSERID', -value => $session->param('userid') || 0,  -expires =>Config::getConfig( 'client_session_expiry' ) );
	
	# Now set session expiry
	$session->expire( Config::getConfig( 'sessionexpiry' ) || DEFAULT_SESSION_EXPIRY ); 

	## Bypass local checks if we're deliberately running outside of a CGI context (i.e. through Apache)
	# Check for invalid referrer or web site down BEFORE any action lookup
	if ( IS_MOD_PERL || IS_CGI ) {
		# check the http referrer and die if its incorrect
		my $referer = $ENV{HTTP_REFERER};
		if ( ( ( Config::getConfig( 'environment' ) ||'' ) eq "PROD" ) && ( $referer ) ) {
			my $validreferer = Config::getConfig( 'validreferer' ) || '';
			# die "Invalid referer $referer" unless $referer =~ m/$validreferer/i;
			die "Invalid referer" unless $referer =~ m/$validreferer/i;
		}
		die "Website down" unless ( Config::getConfig('websitestatus')||'off' eq "off" );
	}

	$start_time = time();

	$action = get_action( $query );
	my ( $category, $subaction, $subact ) = lookup_action( $query, $session, $action ); # Lookup to get sub action map and loads any requires

	if ( $viaCmdLine == 1 ) {
		print "***********\n";
		print "*** " . join(', ', ( (!IS_MOD_PERL ? 'MOD_PERL' : undef), (!IS_CGI ? 'CGI' : undef) )) . " not set; running in local mode ***\n";
		
		## If the database supplies a default userid, use that
		my $default_userid = $query->param('userid') || Config::getConfig('default_userid');
		if ( $default_userid ) {
			my $roleid = $query->param('roleid') || 1; # Default to operator
			print "Logging on as user: '$default_userid', role: $roleid\n";
			Users::complete_login($query, $session, $default_userid);
			$session->param( 'roleid', $roleid );
		}
		my $opcoid = $query->param('opcoid');
		if ( ( defined $opcoid ) && ( looks_like_number( $opcoid ) ) ) {
			$session->param( SessionConstants::SESS_OPCOID, $opcoid );
		}
		print "***********\n\n\n";
	}

	die "Not logged on:" if ( ( ( $subact->{nologin} || 0 ) == 0 ) &&  ( ! Users::isLoggedIn( $session ) ) );

	$userid = $session->param( 'userid' );
	$roleid = $session->param( 'roleid' );

	my $query_userid = $query->param( 'userid' ); # TODO Some legacy code checked that if userid supplied in query it must match session.userid

	if ( ( ( $subact->{nologin} || 0 ) == 0 ) && ( ( $subact->{norolecheck} || 0 ) == 0 ) ) {
		# Check session.role against roles allowed for action
		die "Roles list missing for '$category/$subaction', action '$action'" unless exists $subact->{roles};
		die "You are not authorised to access '$category/$subaction'" unless action_authorised( $userid, $roleid, $subact->{roles}  );
	}
		
	##!! THIS HAS TO BE BELOW THE ABOVE BLOCK SO THAT THE REDIRECTION ACTUALLY WORKS
	##!! REDIRECTION DOESN'T WORK IF YOU'VE ALREADY SENT THE '200 OK' RESPONSE HEADER

	# Also dont print the header if action is processLogin as we may have to redirect after login to masters or jrs

	my $redirect = $subact->{redirect} || 0;	# If set disables printing header before and prints redirect after
	if ( ( ! $redirect ) && ( $action ne "processLogin" ) ) {
		print $query->header( -cookie => [$cookie,$cookie2], -charset => 'utf-8' );
		$printed_header = 1;
	}

	## Run the action
	my $func = $subact->{func};
	my $retval = $func->($query, $session);

	log_action( $userid, $roleid, $action, $start_time, 0 ); # status arg is 0, ok
	$start_time = 0;

	# Then the post processing
	if ( $redirect ) {
		die "Expected '$action' to return URL" unless ( ( defined $retval ) && ( $retval ne '' ) );
		print $query->redirect( -url => $retval );

	} else { 
		my $first_action = $action;
		# warn "retval for first action '$first_action' is $retval" if looks_like_number( $retval );

		$action = $subact->{postaction}; # true for several "remove" actions and users/changeroleid
		if ( defined $action ) {
			# warn "action '$first_action' has postaction '$post_action'" if defined $action;
		} else {
			if ( ( $first_action eq "processLogin" ) && ( looks_like_number( $retval ) && ( $retval == 0 ) ) ) {
				#warn "Last action was processLogin and returned rc = 0";
				my $post_login = $session->param( SessionConstants::SESS_POSTLOGIN );
				if ( defined $post_login ) {
					Users::RestoreQuery( $query, $session );
					$action = get_action( $query );
				}
				$roleid = $session->param( SessionConstants::SESS_ROLEID );
				$action = 'dashboard' unless defined $action;
			} elsif ( ( defined $retval ) && ( lc( $retval ) eq 'dashboard' ) ) {
				# Things like BookImage conditionally return 'dashboard'
				$action = "dashboard";
			}
		}
		if ( defined $action ) {

			$start_time = time();

			my ( $category, $subaction, $subact ) = lookup_action( $query, $session, $action ); 
			$roleid = $session->param( 'roleid' );  # May have change eg handleroleidchange action
			if ( ( $subact->{norolecheck} || 0 ) == 0 ) {
				die "You are not authorised to access '$category/$subaction'" unless action_authorised( $userid, $roleid, $subact->{roles}  );
			}
			my $redirect = $subact->{redirect} || 0;	# If set disables printing header before and prints redirect after
			if ( ( ! $redirect ) && ( $printed_header == 0 ) ) {
				print $query->header( -cookie => [$cookie,$cookie2], -charset => 'utf-8' );
				$printed_header = 1;
			}
			eval {
				# cluck "Invoking 2nd action '$action', category $category, subaction $subaction";
				my $func = $subact->{func};
				my $retval = $func->( $query, $session );
				if ( $redirect ) {
					die "Expected '$action' to return URL" unless ( ( defined $retval ) && ( $retval ne '' ) );
					print $query->redirect( -url => $retval );
				}
			};
			if ( $@ ) {
				cluck "Failed on 2nd action '$action', $@";
			}
			log_action( $userid, $roleid, $action, $start_time, 0 ); # status arg is 0, ok
		}
	}
};
if ( $@ ) {

	my $error = $@;
	my $full_error = $error; 	# Full unadulterated error (viewable in browser network window if needed)
	#$error =~ s/ at module.*//sg;	# Error to be displayed by AJAX error handling
	$error =~ s/ at \/home.*//sg;
	$error =~ s/ at jobflow.*//sg;

	if ( ( $start_time != 0 ) && ( defined $userid ) ) {
		log_action( $userid, $roleid, $action, $start_time, 1 ); # status arg is 1, not ok
	}

	# Print header if not already printed (may have deferred for redirect actions)
	print $query->header( -cookie => [$cookie,$cookie2] ) unless $printed_header;

	# Determine if caller is expecting JSON or not
	my $mode = $query->param( 'mode' ) || '';
	if ( ( lc( $mode ) eq 'json' ) || ( Accept('application/json') > Accept('text/html') ) ) {

		# If JSON, just return the error
		print encode_json({ error => $error, fullerror => $full_error });
	} else {
		if( $error =~ /^Not logged on/ ) {
			# Save existing query, so we can restore
			Users::SaveQuery( $query, $session );
			Users::Login( $query ); # Display Login dialog
	
		} elsif ( $error =~ /^Access denied/ ) {
			General::NoAccess();

		} elsif ( $error =~ /^Database error/ ) {
			General::NoAccess( $error, $roleid );   # Might want to refine this

		} elsif ( $error =~ /^Website down/ ) {
			General::Maintainence();

		} elsif ( $error =~ /^Invalid referrer/ ) {
			# Dont display anything

		} else {
			$error =~ s/\n/<br>/g;
			General::NoAccess( $error, $roleid );
		}
	}
}
		
##############################################################################
##############################################################################
#       --- end of file --- 
#      
##############################################################################
##############################################################################

