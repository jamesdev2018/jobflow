#!/usr/local/bin/perl
package Groups;
#############################################################################################################################################################################################
# Script	: Groups.pl
# Purpose	: To group the jobs based on advisory, teams, departments, etc
# Author	: Feroz khan N
#############################################################################################################################################################################################

##USE 
use strict;
use warnings;
use CTSConstants;
use JSON;
use lib 'modules';


##REQUIRE
require "databasegroups.pl";
require "databaseJob.pl";
require "Watchlist.pl";

##DB

##GLOBAL DECLARATIONS


################################################################################################
# Function	:update_department_status
# Purpose	:To update the group of jobs department status
# Parameters	:Query, Session
# Authors	:Feroz Khan N
################################################################################################
sub update_department_status
{
    my ($query, $session) = @_;

    my $deptid  = $query->param('deptid') || 0;
    my $deptname= $query->param( 'depname' );
    my $joblist = $query->param('jobs');
    my $watchlistflg = $query->param('watchlistflg');
    my $opcoid  = $session->param('opcoid');
    my $loc_mismatch = 0;

    my @jobs = split(/,/, $joblist);

    ##As per ticket PD-2519 If job is another location in watch list it should not update.
    if ($watchlistflg == 1)
    {
            foreach my $jobnumber (@jobs)
            {
                my $location = ECMDBJob::db_getLocation($jobnumber);
                if ( $location != $opcoid )
                {
                    $loc_mismatch = 1;
                }
            }
    }

    if ($loc_mismatch == 0)
    {
            ECMDBGroups::db_deptstatusupdate($deptid, $joblist);

            foreach my $jobnumber (@jobs)
            {
                Audit::auditLog($session->param('userid'), "departments", $jobnumber, "Job: $jobnumber is assigned to department $deptname" );
                ECMDBJob::db_recordJobAction($jobnumber, $session->param('userid'), CTSConstants::DEPARTMENTS);
            }
    }

    if($query->param('mode') eq 'json')
    {
        print encode_json({ dummy => 1, deptid => $deptid, jobs => $joblist, loc_mismatch => $loc_mismatch });
    }
}
################################################################################################
# Function      :update_team_status
# Purpose       :To update the group of jobs team status
# Parameters    :Query, Session
# Authors       :Feroz Khan N
################################################################################################
sub update_team_status
{
    my ($query, $session) = @_;

    my $jobs        					= $query->param( 'jobs' );
    my $mode        					= $query->param( 'mode' );
	my $userid							= $session->param('userid');
    my $opcoid  						= $session->param('opcoid');
    my $teamid      					= $query->param('teamid') || 0;
    my $teamname    					= $query->param( 'teamname' );
    my $watchlistflg 					= $query->param('watchlistflg');
    my $loc_mismatch 					= 0;
	my $userJobnumberMapping_ref		= undef;

    my @jobs        					= split (/,/, $jobs);

    ##As per ticket PD-2519 If job is another location in watch list it should not update.
    if ($watchlistflg == 1)
    {
        foreach my $jobnumber (@jobs)
        {
                my $location = ECMDBJob::db_getLocation($jobnumber);
                if ( $location != $opcoid )
                {
                        $loc_mismatch = 1;
                }
        }
    }

    if ($loc_mismatch == 0)
    {
            ECMDBGroups::db_teamstatusupdate($teamid, $jobs);

            foreach my $jobnumber (@jobs)
            {
                Audit::auditLog( $userid, "teams", $jobnumber, "Job: $jobnumber is assigned to team $teamname" );
                ECMDBJob::db_recordJobAction($jobnumber, $session->param('userid'), CTSConstants::DEPARTMENTS);
            }
			$userJobnumberMapping_ref   = Watchlist::Watchlistnotification( $userid, $opcoid, "Job(s) is assigned to team $teamname ", \@jobs, "TEAMS" );
    }

    if($query->param('mode') eq 'json')
    {
        print encode_json({ dummy => 1, teamid => $teamid, jobs => $jobs,loc_mismatch => $loc_mismatch, userJobnumberMapping => $userJobnumberMapping_ref  });
    }
}
################################################################################################
# Function	: update_querytype
# Purpose       :To update the group of jobs department status
# Parameters    :Query, Session
# Authors       :Feroz Khan N
################################################################################################
sub update_querytype
{
    my ($query, $session) = @_;
    $session->param( "highlightjob","" );#emptying the highlight job from session so that after advisory action select job is deselected 


    my $querytypeid = $query->param('querytypeid') || 0;
    my $joblist = $query->param('jobs') || '';
    my $querytypename = $query->param('querytypename');

    my @jobs = split(/,/, $joblist);
    my $watchlistflg = $query->param('watchlistflg');
    my $opcoid  = $session->param('opcoid');
    my $loc_mismatch = 0;

    ##As per ticket PD-2519 If job is another location in watch list it should not update.
    if ($watchlistflg == 1)
    {
        foreach my $jobnumber (@jobs)
        {
                my $location = ECMDBJob::db_getLocation($jobnumber);
                if ( $location != $opcoid )
                {
                        $loc_mismatch = 1;
                }
        }
    }

    if ($loc_mismatch == 0)
    {
            ECMDBGroups::db_updatequerytype($querytypeid, $joblist);

            foreach my $jobnumber (@jobs)
            {
                Audit::auditLog($session->param('userid'), "querytype", $jobnumber, "Job: $jobnumber Advisory has been set to $querytypename" );
                ECMDBJob::db_recordJobAction($jobnumber, $session->param('userid'), CTSConstants::QUERYTYPEUPDATE);
            }
    }

    if($query->param('mode') eq 'json')
    {
        print encode_json({ dummy => 1, querytypeid => $querytypeid, jobs => $joblist,loc_mismatch => $loc_mismatch  });
    }
}


1;
