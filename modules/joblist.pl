#!/usr/local/bin/perl
package Joblist;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use Template;
use JSON;
use Data::Dumper;
use Cwd;

# Home grown

use lib 'modules';

use Validate;
use SessionConstants;

require "config.pl";
require "filters.pl"; # package Filters, get_joblist_filters, update_filter_values
require "databaseCustomerUser.pl"; # package ECMDBCustomerUser, getUserCustomerIdsByOpco
require "databaseUser.pl";

require "databaseOpcos.pl"; # package ECMDBOpcos, db_getUserOpcos (need to remove this eventually)

require "databaseJoblist.pl"; # package ECMDBJoblist, db_getMyJobs, db_getWatchedJobs
require "streamhelper.pl"; # createKey

# require "databaseSearch.pl"; # package ECMDBSearch, db_checkSearchContent, db_getCustomerName, db_getRemotCustomerID, getOpcoNameFromId, canUserSeeOpcoid

use constant OPCO_MULTIPLE_LOCATIONS => 17;

use constant DASHBOARD_SAVED_QUERY => 'SESS_DASHBOARD_SAVED';
use constant DASHBOARD_SAVED_SUBACTION => 'SESS_DASHBOARD_LAST_SUBACTION'; # Used for Sort By/Refresh action
use constant DASHBOARD_SAVED_RESOURCE_PARAMS => 'SESS_DASHBOARD_LAST_RESOURCE_PARAMS'; # Remember previous resource request

use constant BLANK => '(blank)'; # Substitute for '' in filter values

use constant QP_SEARCH_TYPE_ID => 'searchTypeId';
use constant QP_SEARCH_CONTENT => 'searchContent';
use constant QP_ARCHIVED => 'archived';

use constant JOBLIST_JOBS_PER_PAGE_DEFAULT => 300;
use constant JOBLIST_JOBS_PER_PAGE_KEY => "jobs_per_page";

use constant JOBLIST_APPLY_TIMER_PERIOD_DEFAULT => 1500;
use constant JOBLIST_APPLY_TIMER_PERIOD_KEY => "joblist_apply_timer_period";

use constant ROLE_EXTERNAL => 7; # External or Forms user role

##################################################################
# the template object 
##################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";

sub pluralof {
	my ( $n, $single, $plural ) = @_;

	return "1 $single" if ( $n == 1 );
	return "$n $plural";
}

# update_filter_values moved to filters.pl

# save/restore query parameters (based on SaveQuery in modules/users.pl)
# So if we get a GET action=dashboard (and mode not defined), just save all the query values
# for the eventual POST action=dashboard mode=json 
# and restore the query values there.

sub save_query_values {
	my ( $query, $session ) = @_;

	my $action = $query->param( "action" ) || '';
	return unless ( $action eq 'dashboard' );

	my @params = $query->param;  # List of query parameter names
	my @values;

	foreach my $k ( @params ) {
		push( @values, "$k=" . $query->param( $k ) );
	}
	my $queryString = join( "&", @values );
	$session->param( DASHBOARD_SAVED_QUERY, $queryString );
}

sub restore_query_from_string {
	my ( $query, $queryString ) = @_;

	# warn "restore_query_from_string, string $queryString";
	my @values = split /&/, $queryString;
	foreach my $pair ( @values ) {
		my ( $k, $v ) = split /=/, $pair;
		my $query_val = $query->param( $k );
		# Set Query value as long as query value not already set
		if ( ! $query_val ) {
			# warn "restoring name => $k, -value => $v";
			$query->param( -name => $k, -value => $v );
		}
	}	
}

sub restore_query_values {
	my ( $query, $session, $keep ) = @_;

	$keep = 0 unless defined $keep;
	my $queryString = $session->param( DASHBOARD_SAVED_QUERY );
	if ( defined $queryString  ) {
		$session->clear( DASHBOARD_SAVED_QUERY ) unless $keep; # used when subaction is refdata
		restore_query_from_string( $query, $queryString );
	}
}

# get_operating_company_flags - get opcoid specific flags

sub get_operating_company_flags {
	my ( $opcoid, $vars ) = @_;

	my $opflags = ECMDBJoblist::get_operating_company_flags( $opcoid );
	return $opflags;
}

# reference data - could refine to cache in client and only request once
# For most of the reference data, it returns an array of { name, id }

sub dashboard_ref_data {
	my ( $vars ) = @_;

	$vars->{qcstatus}       = ECMDBJoblist::db_getidnamebytablename('qcstatus','status');

	$vars->{preflight}      = ECMDBJoblist::db_getidnamebytablename('preflightstatus','status', { orderby => 'status ASC' } );

	# We need both if displaying jobs which have a mix of broadcast and non broadcast workflow
	# but then the filter displays just the non broadcast values.

	$vars->{operatorstatuses} = ECMDBJoblist::db_getidnamebytablename('operatorstatus','status', { orderby => 'status ASC' } );
	$vars->{broadcast_operatorstatus} = ECMDBJoblist::db_getidnamebytablename('broadcast_operatorstatus', 'status', { orderby => 'status ASC' } );

	$vars->{prodstatus}     = ECMDBJoblist::db_getidnamebytablename('productionstatus','status', { orderby => 'status ASC' } );

	$vars->{departmentlist} = ECMDBJoblist::db_getidnamebytablename('departments','name', { orderby => 'name ASC' } );

	$vars->{teamlist}       = ECMDBJoblist::db_getidnamebytablename('teams','name', { orderby => 'name ASC' } );

	$vars->{querytypelist}  = ECMDBJoblist::db_getidnamebytablename('querytypes','querytype', { orderby => 'querytype ASC' } );

	$vars->{getcountries}   = ECMDBJoblist::db_getidnamebytablename('countries','country');

	$vars->{videoratios}    = ECMDBJoblist::db_getidnamebytablename('videoratio','ratio');

	$vars->{videostandards} = ECMDBJoblist::db_getidnamebytablename('videostandard','standard');

	$vars->{videoframerates} = ECMDBJoblist::db_getidnamebytablename('videoframerate','ratio');

	$vars->{videoletterboxes} = ECMDBJoblist::db_getidnamebytablename('videoletterbox','ratio');

	$vars->{audiostandards} = ECMDBJoblist::db_getidnamebytablename('audiostandard','standard');

	$vars->{producerresponsible} = ECMDBJoblist::db_getidnamebytablename('producerresponsible','name');

	$vars->{ref_data}		= [ 'qcstatus', 'preflight', 'operatorstatuses', 'prodstatus', 'departmentlist', 'teamlist', 'querytypelist', 'getcountries', 
								'videoratios', 'videostandards', 'videoframerates', 'videoletterboxes', 'audiostandards', 'producerresponsible',
								'broadcast_operatorstatus' ];
}

# Get user specific data
# Inputs : userid
# Outputs (in vars hash)
# - useropcos. array of opcoid (ref)
# - userinteractive, auto_refresh

sub get_user_specific {
	my ( $userid, $vars ) = @_;

	my @useropcos				= ECMDBOpcos::db_getUserOpcos( $userid );
	$vars->{useropcos}			= \@useropcos;

	my @cust = ECMDBCustomerUser::getUsersCustomers( $userid ); # TODO could save for later use ...
	die "User $userid has no customers" if ( ( scalar @cust ) == 0 );

	$vars->{userinteractive}	= ECMDBUser::db_interactive( $userid ); # query on usermediatypes inner join mediatypes
	$vars->{auto_refresh}		= ECMDBUser::db_get_autorefresh_duration( $userid ); # returns users.auto_refresh
	$vars->{userdata} 			= { 
									statusNoteFlag => ECMDBUser::db_getStatusNoteFlag($userid),
									userinteractive => $vars->{userinteractive},
								  };
}

# Get role specific data
# Inputs: roleid
# Outputs (in vars hash)
# permissions: sendtovendorpermission, unlockjobpermission, checkoutpermission, checkinpermission

sub get_role_specific {
	my ( $roleid, $vars ) = @_;

	die "Invalid Access" unless ( $roleid <= 7 );

	$vars->{sendtovendorpermission}		= ( $roleid =~ m/^(3|4|5)$/ ) || 0;		#TODO General::isPermissioned( "releasetovendorfcn", $roleid );
	$vars->{unlockjobpermission}		= ( $roleid =~ m/^(4|6)$/ ) || 0;		#TODO General::isPermissioned( "unlockjobfcn", $roleid );
	$vars->{checkoutpermission}			= ( $roleid =~ m/^(3|4|6)$/ ) || 0;		#TODO General::isPermissioned( "checkoutfcn", $roleid );
	$vars->{checkinpermission}			= ( $roleid =~ m/^(4|6)$/ ) || 0;		#TODO General::isPermissioned( "checkinfcn", $roleid ) );
}

# Get opcoid specific data
# Inputs: opcoid
# Outputs in vars hash
#  - assigneelist, operators, prodstatus

sub opco_specific_data {
	my ( $opcoid, $vars ) = @_;

	$vars->{assigneelist}		= ECMDBJoblist::db_userlist( $opcoid ); # { name, id } id prefixed with ASS_

	my @operators				= ECMDBCustomerUser::getOpcoOperators( $opcoid );
	$vars->{operators}			= \@operators;
}

# Get common set of flags such as orderby as these are used by both Dashboard_page and the JSON calls
# Used in both Dashboard_page and also Dashboard (for the JSON case)

sub get_common_settings {
	my ( $query, $session, $vars ) = @_;

	my $sortby		= $query->param( "sortby" ) || 'desc';
	my $sortInd		= 0;
	if ( $sortby eq 'asc' ) {
		$sortInd = 1;
	} elsif ( $sortby eq 'desc' ) {
		$sortInd = 2;
	}
	$vars->{orderbyid}	= $query->param( "orderbyid" ) || '';
	$vars->{sortby}		= $sortby;
	$vars->{sortInd}	= $sortInd; # is either 0 (undefined), 1 (for asc) or 2 (for desc)
	$vars->{sidemenu}	= $query->param( 'sidemenu' ) || '';
}

# Setter for job data and job count

sub set_job_data {
	my ( $jobs, $vars ) = @_;

	my $count = 0;
	$count = scalar @{$jobs} if ( defined $jobs );
	$vars->{jobs}		= $jobs;
	$vars->{job_count}	= $count;
}

# Extract distinct sets of values from job data and add as arrays to $vars
# eg derive_filter_values( $jobs, { agencies => agency, clients => client, projects => project, mediatypelist => mediatype, ... }, $vars );
#
# The result of each key (the key is used as the key in the target hash) is a hash { id, name }
# so for example, by including agencies => agency above, we scan the column agency in all rows of $jobs
# and create an array 'agencies' and put a ref to this array in $vars->{ agencies }.
#
sub derive_filter_values {
	my ( $mapping, $jobs, $vars ) = @_;

	# warn "derive_filter_values " . ref( $mapping ) ;
	# warn "derive_filter_values " . Dumper( $jobs );

	my @mapping_keys = keys %$mapping; # The key we use when adding the array to $vars eg agencies
	my @mapping_values = values %$mapping;  # The key we use to examine $jobs  eg agency

	# warn "derive_filter_values keys " . join( ", ", @mapping_keys ) . "\n";
	# warn "derive_filter_values values " . join( ", ", @mapping_values ) . "\n";

	my %tree;

	foreach my $job ( @$jobs ) {
		foreach my $key ( @mapping_values ) { # key is the result column name to check
			my $v = $job->{ $key };
			if ( ! defined $v ) {
				$v = "undef";
			} else {
				$v =~ s/\s*$//g;
				$v =~ s/^\s*//g;
				$v = BLANK if $v eq ''; # Treat '' as '(blank)';
			}
			# tree{ $key } is the entry for a specific mapping
			# Create 1st level hash entry if it doesnt exist
			$tree{ $key } = {} if ( ! exists $tree{ $key } );
			my $h = $tree{ $key };
			if ( exists $h->{ "$v" } ) {
				$h->{ "$v" } = $h->{ "$v" } + 1;
			} else {
				$h->{ "$v" } = 1;
			}
		} 
	}
	# warn "derive_filter_values " . Dumper( \%tree ) . "\n";

	foreach my $target_key ( @mapping_keys ) {
		my $key = $mapping->{ $target_key };
		my $key_uc = uc( $key );
		my $node = $tree{ $key };
		if ( ! defined $node ) {
			$vars->{ $target_key } = \(); # Create ref to empty array
		} else { # node is hash ref (for individual classification), so keys of this are the values we want
			my @list = keys %{$node}; # The keys are the actual values such as Customer names
			my @list2;
			my $key_index = 0;
			foreach my $k ( @list ) {
				$key_index ++;
				push( @list2, { 
					# id => $key . '_' . $key_index, Not needed
					name => $k, count => $node->{ $k } }  ) 
			}
			my @sorted_list = sort { $a->{name} cmp $b->{name} } @list2; # Will mean things with numbers before alphas
			$vars->{ $target_key } = \@sorted_list; # keys of hash entry for agencies will be distinct values of agency
		}
		push( @{$vars->{filter_columns}}, $target_key );
	}
	$vars->{filter_values_updated} = 1;
}

sub derive_client_agency_project {
	my ( $jobs, $vars, $opflags ) = @_;

	# For each key value pair, left hand is name of array in vars, right hand is column in $jobs 
	my $mappings = {
			agencies => "agency", 
			clients => "client", 
			projects => "project", 
			opcompany => "opcompany", 
			worktype => "worktype", 
			formatsize => "formatsize", 
			publication => "publication", 
			location => "location",
	};
	if ( $opflags->{publishing} == 0 ) {
		$mappings->{ mediatypelist } = "actiontype"; # actiontype/t.workflow
	} else {
		$mappings->{ mediatypelist } = "mediatype"; # mediatype/t.formatsize
	}
	derive_filter_values( $mappings, $jobs, $vars );
}

# Extract data from returned jobs (much as db_getJobs already does)

sub derived_job_data {
	my ( $jobs, $vars ) = @_;

	# jobs and job_count set by set_job_data

	if ( defined $jobs ) {

		# For each key value pair, left hand is name of array in vars, right hand is column in $jobs 
		derive_filter_values( { 	
				locationlist => 'location', 
				bookedby => 'bookedby', 
				accounthandler => 'accounthandler',
				mediachoiceidlist => 'mediachoiceid', 
				countrytypelist => 'country', 
				producerlist => 'producer',
				assigneelist => 'assignee',
			},
			$jobs, $vars );
		$vars->{derived_job_data} = 1;
	}
}


# Check dashboard constraints
# currently used to check whether the user has specified enough customers for the given date range.
# Called like check_dashboard_constraints( $fromId, { customer => $agencyFilter, client => $clientFilter, $vars } );
# Throws an exception if a constraint not met.

sub check_dashboard_constraints {
	my ( $fromId, $filters, $userid, $opcoid, $vars ) = @_;

	push( @{$vars->{history}}, "check_dashboard_constraints: userid $userid, opcoid $opcoid" );
	
	my $failed_constraint = '';
	eval {

		# SELECT days, filter_name, min_value, max_value, description FROM dashboard_constraints WHERE days <= ?", [ $fromId ]
		# filter_name eg customer, is used as key into filter hash

		my $rows = ECMDBJoblist::get_dashboard_constraints( $fromId );
		if ( defined $rows ) {

			push( @{$vars->{history}}, "check_dashboard_constraints: got " . pluralof( scalar( @$rows ), 'row', 'rows' ). " from dashboard_constraints" );
			foreach my $row ( @$rows ) {

				my $constraint = $row->{filter_name};

				my $setting = $filters->{ $constraint }; # ie for constraint customer, look at the filter value passed with key customer (agencyFilter)
				$setting = '' unless defined $setting;	# be careful not to translate "0" to ""
				$setting =~ s/,\s*$//g;	# Remove trailing , and spaces
				$setting =~ s/^\s*,//g; # Remove leading spaces and comma
				my $count = scalar( split /,/, $setting ); # count filter values passed

				my $min_value = $row->{min_value};
				my $max_value = $row->{max_value};
				my $min_days = $row->{min_days};
				my $max_days = $row->{max_days};

				# warn " - fromId $fromId, min days $min_days, max days $max_days, constraint $constraint, setting $setting, min $min_value, max $max_value\n";

				if ( ( ( $min_value != -1 ) && ( $count < $min_value ) ) 
					|| ( ( $max_value != -1 ) && ( $count > $max_value ) ) ) {
					my $text = '';
					if ( $min_value == $max_value ) {
						$text = "Please select just " .pluralof( $min_value, $constraint,  $constraint . 's' ) ." for period " . $row->{description};
					} else {
						$text = "Please select between $min_value and $max_value " . $constraint . "s for period " . $row->{description};
					}
					push( @{$vars->{warning_msgs}}, $text );
					push( @{$vars->{history}}, "check_dashboard_constraints: Failed constraint $constraint" );
					$failed_constraint = $constraint;
					last;
				}
			}
		}
	};
	if ( $@ ) {
		my $error = $@;
		push( @{$vars->{history}}, "check_dashboard_constraints, threw '$error', fromId $fromId" );
		$error =~ s/ at .*//gs;
		# warn "check_dashboard_constraints: " . Dumper( \@agencies );
		die $error;	# Rethrow the error
	}
}

# JSON handler for main Search (that returns job details)
# Original search is done by dashboardSearch in js/custom.js which uses action=search
# and that is handled by SearchJobs in search.pl, which returns details of how many jobs the search found
# and summarises the jobs found which are not visible.
# For type 7, Elastic search using brief/comment it returns a list of job numbers

sub Dashboard_search2 {
	my ( $query, $session, $vars ) = @_;

	$vars->{jobslist}		= $query->param( 'jobs' ) || '';

	push( @{$vars->{history}}, "Dashboard_search2: Jobs '" . $vars->{jobslist} . "'" );

	eval {
		my $opflags = get_operating_company_flags( $vars->{opcoid} );

		die "Job list blank" unless ( $vars->{jobslist} ne '' );

		push( @{$vars->{history}}, "Dashboard_search2: calling ECMDBJoblist::db_searchJobs" );

		my ( $jobs, $sql ) = ECMDBJoblist::db_searchJobs( $vars->{userid}, $vars->{opcoid}, 
						$vars->{orderbyid}, $vars->{sortby}, $opflags, $vars, $vars->{jobslist} );

		push( @{$vars->{history}}, "Dashboard_search2: after calling ECMDBJoblist::db_searchJobs" );

		$vars->{sql} = $sql if ( $vars->{tag_env} ne 'LIVE' );
		set_job_data( $jobs, $vars );

		# get product assignee details
		get_product_assignee_data( $jobs, $vars );

		# get product op status details
		get_product_op_status_data( $jobs, $vars );

		my $noderived = $query->param( 'noder' ) || 0; # For testing purpose, use noder=1 to omit derived data 
		derived_job_data( $jobs, $vars ) if ( ( defined $jobs ) && ( $noderived == 0 ) );

		$vars->{filter_values_updated} = 0;
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = 'Dashboard_search2: ' . $error;
	}
}

# handler for My Jobs dashboard call that returns jobs assigned to user
# Jobs page, click on My Jobs: action=dashboard&mode=json&sidemenu=&myjobsFlg=Yes&formPosted=yes&assigneeFilter=330&allflg=0&reset_flag=2&archived=
# Resource page, Click on User: action=dashboard&mode=json&sidemenu=&myjobsFlg=Yes&formPosted=yes&assigneeFilter=330&allflg=0&reset_flag=2&archived=

sub Dashboard_myjobs {
	my ( $query, $session, $vars ) = @_;

	my $assignee = $query->param( 'assignee' );
	if ( ( defined $assignee ) && ( looks_like_number( $assignee ) && ( $assignee != 0 ) ) ) {
		# @TODO could add check to see if user has right to view other users jobs ...
		$vars->{userid} = $assignee;
	}
	my $opflags = get_operating_company_flags( $vars->{opcoid} );

	my ( $jobs, $sql ) = ECMDBJoblist::db_getMyJobs( $vars->{userid}, $vars->{orderbyid}, $vars->{sortby}, $opflags, $vars );

	$vars->{sql} = $sql if ( $vars->{tag_env} ne 'LIVE' );
	set_job_data( $jobs, $vars );
		
	# get product assignee details
	get_product_assignee_data( $jobs, $vars );
	
	# get product op status details
	get_product_op_status_data( $jobs, $vars );

	my $noderived = $query->param( 'noder' ) || 0; # For testing purpose, use noder=1 to omit derived data 
	# derived_job_data( $jobs, $vars ) if ( ( defined $jobs ) && ( $noderived == 0 ) );
}

# handler for Created By Me dashboard call that returns jobs created by user

sub Dashboard_createdByMe {
	my ( $query, $session, $vars ) = @_;

	# Get employeeid using userid (employeeid is numeric employeeid, booked by is full name (well part of it)
	my $employeeid = ECMDBJoblist::db_getEmployeeid( $vars->{userid} ); # Its a varchar(30) field in tracker3

	$employeeid =~ s/^\s*//g; # Remove leading and training spaces
	$employeeid =~ s/\s*$//g;
	die "Employeeid for user " . $vars->{userid} . " is undefined" if ( $employeeid eq '' );
	$vars->{employeeid} = $employeeid;

	my $opflags = get_operating_company_flags( $vars->{opcoid} );

	my ( $jobs, $sql ) = ECMDBJoblist::db_getCreatedByMe( $vars->{employeeid}, $vars->{orderbyid}, $vars->{sortby}, $opflags, $vars );

	$vars->{sql} = $sql if ( $vars->{tag_env} ne 'LIVE' );
	set_job_data( $jobs, $vars );

	# get product assignee details
	get_product_assignee_data( $jobs, $vars );

	# get product op status details
	get_product_op_status_data( $jobs, $vars );

	my $noderived = $query->param( 'noder' ) || 0; # For testing purpose, use noder=1 to omit derived data 
	# derived_job_data( $jobs, $vars ) if ( ( defined $jobs ) && ( $noderived == 0 ) );
}

# handler for Watch list dashboard call that returns list of jobs that user has elected to watch
#
# The Watchlist does use the filters (but not the fromId)
#
# a) Watchlist on LHN, action=dashboard mode=json sidemenu=SidepanelWatchJob, ...
#

sub Dashboard_watchlist {
	my ( $query, $session, $vars ) = @_;

	my $opflags = get_operating_company_flags( $vars->{opcoid} );

	my $symbol_table = Filters::getMatchConditionSymbols( $opflags, $vars->{roleid}, $vars->{rolename}, $vars->{relaxroles} );

	my $filters = Filters::get_joblist_filters( $symbol_table ); # Uses all filters bar fromId
	# push( @{$vars->{history}},  "Filters " . Dumper( $filters ) );

	my $query_params = $query->Vars; # Get query params as a hash
	my $filter_values = Filters::get_filter_values( $query_params, $filters );
	push( @{$vars->{history}},  "Filter values " . Dumper( $filter_values ) );

	my ( $jobs, $sql ) = ECMDBJoblist::db_getWatchedJobs( $vars->{userid}, $vars->{orderbyid}, $vars->{sortby}, $opflags, $vars, $filters, $filter_values );

	$vars->{sql} = $sql if ( $vars->{tag_env} ne 'LIVE' );
	set_job_data( $jobs, $vars );

	# get product assignee details
	get_product_assignee_data( $jobs, $vars );

	# get product op status details
	get_product_op_status_data( $jobs, $vars );

	# With the watchlist, we expect a relatively small number of rows, so if no filter values were specified
	# then we can use the list of jobs returned for both sets of filter value derivations
	# a) derived_job_data, acts on just the data returned
	# b) derive_client_agency_project, returns the distinct set of agency etc (but again uses filters if present)

	derived_job_data( $jobs, $vars ) if ( ( defined $jobs ) && ( $vars->{noderived} == 0 ) );

	if ( ( scalar (keys %$filter_values) > 0 ) && ( $vars->{noderived} == 0 ) ) {  # If there are any filter values
		my $sql2;
		( $jobs, $sql2 ) = ECMDBJoblist::db_getWatchedJobs( $vars->{userid}, $vars->{orderbyid}, $vars->{sortby}, $opflags, $vars ); # with no filters
		$vars->{sql2} = $sql2 if ( $vars->{tag_env} ne 'LIVE' );
	}
	derive_client_agency_project( $jobs, $vars, $opflags ) if ( ( defined $jobs ) && ( $vars->{noderived} == 0 ) );

	# Update filter values in response
	Filters::update_filter_values( $filter_values, $filters, $vars );
}

# handler for View dashboard call that returns list of Filtered Jobs
#
# Called under several circumstances
# a) On login, the initial GET action=dashboard loads the HTML page but soon afterwards
#    the javascript makes an ajax call with action=dashboard mode=json ...
#
# b) Click on Jobs on LHN, does ajax call, action=dashboard, mode=json, sidemenu=SidepanelJob, ...
#
# c) Click on Sort By (eg Sort by Assignee Ascending), action=dashboard, mode=json, orderbyid=4, sortby=asc
#
# d) Click on Refresh, does ajax call action=dashboard, mode=json, subaction=refresh
#
sub Dashboard_view {
	my ( $query, $session, $vars ) = @_;

	eval {
		push( @{$vars->{history}}, "Dashboard_view entry" );

		my $opflags = get_operating_company_flags( $vars->{opcoid} );

		my $symbol_table = Filters::getMatchConditionSymbols( $opflags, $vars->{roleid}, $vars->{rolename}, $vars->{relaxroles} );
		my $filters = Filters::get_joblist_filters( $symbol_table );
		my $query_params = $query->Vars();
		#foreach my $key (keys %$filters) { $query_params->{$filters->{$key}->{inputs}} ||= 'value'; }
		my $filter_values = Filters::get_filter_values( $query_params, $filters );

		$filter_values->{archived} = $vars->{archived};  # Set to '1' if user checks 'Show Archived' cb
		push( @{$vars->{history}},  "Filter values " . Dumper( $filter_values ) );

		# Check dashboard constraints, adds reference list to vars as needed
		my $fromId = $filter_values->{fromdate};
		if ( ( ! defined $fromId ) || ( $fromId == 0 ) ) {
			push( @{$vars->{history}}, "Setting fromId to 3" );
			$fromId = 3; #Setting the same value what we have given in joblist_ensure_fromid_set(). We are setting the default value as 3
			$filter_values->{fromdate} = $fromId;
		}

		my $roleid = $session->param( 'roleid' );
		if ( $roleid != ROLE_EXTERNAL ){  # External users doesn't require filter operations

			push( @{$vars->{history}}, "Checking constraints" );
			check_dashboard_constraints( $fromId, $filter_values, $vars->{userid}, $vars->{opcoid}, $vars ) if ( $fromId > 1 );

			if ( ! exists $vars->{warning_msgs} ) {
				push( @{$vars->{history}}, "No warnings" );

				my ( $jobs, $sql ) = ECMDBJoblist::db_getFilteredJobs( $vars->{userid}, $vars->{orderbyid}, $vars->{sortby}, $opflags, $vars, $filters, $filter_values, $vars->{opcoid}, $fromId );
				$vars->{sql} = $sql if ( $vars->{tag_env} ne 'LIVE' );

				# Standard view handling
				set_job_data( $jobs, $vars ) unless ( $vars->{subaction} eq 'filterupdate' ); # filterupdate doesnt update job list, just the filter
	
				# get product assignee details
				get_product_assignee_data( $jobs, $vars );

				# get product op status details
				get_product_op_status_data( $jobs, $vars );

				if ( ( defined $jobs ) && ( $vars->{noderived} == 0 ) ) {
					derived_job_data( $jobs, $vars );
					derive_client_agency_project( $jobs, $vars, $opflags );
				}
			}

			# Update filter values in response
			Filters::update_filter_values( $filter_values, $filters, $vars );
		}
	};
	if ( $@ ) {
		my $error = $@;
		warn "Dashboard_view: error $@\n";
		$error =~ s/ at .*//gs;
		$vars->{error} = $error;
	}	
}

# handler for Resource dashboard call 
#
# Called when subaction is 'resource'
# If called from the Resource dashboard, we get extra params box, boxop, boxcust, boxmedia, selectedDate
# However function boxclick (in js/newcustom.js), helpfully adds the standard filter parameter names newdeptFilter, agencyFilter, newmediatypeFilter
# 
# eg making a selection from each filter then clicking on Waiting for QC, we get params
#  box=waitqc boxop=8544 boxcust=ALL boxmedia=undefined selectedDate=2017-05-03 newdeptFilter=2,1 agencyFilter=AMERICAN_EXPRESS newmediatypeFilter=POS
# So the only work left is to map the box to filters (but there are some akward OR predicates, so we add box as a filter in its own right)
# Suspect that boxcust and boxmedia are not actually used
#
# Query params
# - action=dashboard 
# - subaction=resource
# - boxopco, opcoid from Location dropdown
# - box, One of (pending,inprogress,reject,waitqc,totaljobs,over)
# - boxcust=ALL (defunct, replaced by agencyFilter)
# - boxmedia=undefined (defunct, replaced by newmediatypeFilter)
# - selectedDate, specific date in ISO format, eg 2017-08-18
# - newdeptFilter, list of departmentvalue eg 2,3,4
# - agencyFilter, list of customer names, =AMERICAN_EXPRESS,CANCER_RESEARCH_UK
# - newmediatypeFilter, list of media types, =BROADCAST,OUTDOOR.OOH,PRESS
# nb the list above is translated using the contents of table resource_filters

sub Dashboard_resource {
	my ( $query, $session, $vars ) = @_;

	my $save_params = 1; # Save the query params unless we are processing a refresh request

	eval {
		push( @{$vars->{history}}, "Dashboard_resource entry" );

		# if boxopcoid is valid and <> $vars->{opcoid} need to re get opflags

		my $boxopcoid = $query->param( "boxop" ); 
		if ( ! defined $boxopcoid ) {
			# Perhaps its a refresh, in which case check for saved query params

			my $queryString = $session->param( DASHBOARD_SAVED_RESOURCE_PARAMS );
			die "Dashboard_resource: boxopcoid param not supplied" unless defined $queryString;

			restore_query_from_string( $query, $queryString );
			$boxopcoid = $query->param( "boxop" );
			die "Dashboard_resource: boxopcoid param not supplied" unless defined $boxopcoid;
			$save_params = 0;
		}

		if ( $save_params ) {

			# Save resource params for subsequent refresh (as we dont populate the job filter values)

			my @params = $query->param;  # List of query parameter names
			my @values;

			foreach my $k ( @params ) {
				push( @values, "$k=" . $query->param( $k ) );
			}
			my $queryString = join( "&", @values );
			$session->param( DASHBOARD_SAVED_RESOURCE_PARAMS, $queryString );
		}

		if ( ( defined $boxopcoid ) && looks_like_number( $boxopcoid ) && ( $boxopcoid != 0 ) && ( $boxopcoid != $vars->{opcoid} ) ) {
			# @TODO, check that user has access to this opcoid ...
			$vars->{opcoid} = $boxopcoid;
		}

		my $opflags = get_operating_company_flags( $vars->{opcoid} );

		my $symbol_table = Filters::getMatchConditionSymbols( $opflags, $vars->{roleid}, $vars->{rolename}, $vars->{relaxroles} );
		my $filters = Filters::get_resource_filters( $symbol_table );
		my $query_params = $query->Vars();
		#foreach my $key (keys %$filters) { $query_params->{$filters->{$key}->{inputs}} ||= 'value'; }
		my $filter_values = Filters::get_filter_values( $query_params, $filters );

		# Not including archive setting as selectedDate is just one date
		# $filter_values->{archived} = $vars->{archived};  # Set to '1' if user checks 'Show Archived' cb

		push( @{$vars->{history}},  "Resource filter values " . Dumper( $filter_values ) );

		if ( $vars->{roleid} == ROLE_EXTERNAL ) { 

			 # External users doesn't require filter operations, so dont return any jobs
			push( @{$vars->{history}}, "Role is " . $vars->{rolename} . " excluded" );

		} else {

			my $selectedDate = $filter_values->{selecteddate};
			die "Missing selected date" unless ( ( defined $selectedDate ) && ( $selectedDate ne '' ) );

			my ( $jobs, $sql ) = ECMDBJoblist::db_getResourceJobs( $vars->{userid}, $vars->{orderbyid}, $vars->{sortby}, $opflags, $vars, $filters, $filter_values, $vars->{opcoid} );
			$vars->{sql} = $sql if ( $vars->{tag_env} ne 'LIVE' );

			# Standard view handling
			set_job_data( $jobs, $vars );
	
			# get product assignee details
			get_product_assignee_data( $jobs, $vars );

			# get product op status details
			get_product_op_status_data( $jobs, $vars );

			# We dont want to update the filter (at this point) so dont call derive_client_agency_project or derived_job_data
			#if ( ( defined $jobs ) && ( $vars->{noderived} == 0 ) ) {
			#	derived_job_data( $jobs, $vars );
			#	derive_client_agency_project( $jobs, $vars, $opflags );
			#}

			# Update filter values in response (useful to have this in the response)
			Filters::update_filter_values( $filter_values, $filters, $vars );
		}
	};
	if ( $@ ) {
		my $error = $@;
		warn "Dashboard_resource: error $@\n";
		$error =~ s/ at .*//gs;
		$vars->{error} = $error;
	}	
}

# Dashboard entry point from jobflow.pl

# The possible calls to this function

# 1) Initial load after login, 
# a) POST jobflow.pl action:processLogin  .. returns dashboard HTML page
# b) after the page load, Ajax calls sessiondetails, dashboaard_filter, 
# c) Ajax action:dashboard, mode:json, allflg:0, reset_flag:2

# 2) Actions on Job Dashboard page
# a) Jobs on LHN, GET jobflow.pl action=dashboard, sidemenu=SidepanelJob, ...
#	response is HTML so as per 1 a-c),
# b) Watchlist on LHN, action=dashboard sidemenu=SidepanelWatchJob, ...
#   response is HTML so as per 1 a-c),
# c) My Jobs, action=dashboard   myjobsFlg=Yes assigneeFilter=...
#   response is HTML so as per 1 a-c),
# d) Search - Job number, POST action=dashboard mode=json subaction=search searchTypeId=n, searchContent=... 
#    then if that returns 1 or more jobs, POST action=dashboard mode=json  {set of filter key=value} allflg=0 reset_flag=2
# e) Sort By, POST action=dashboard mode=json orderbyid=5 sortby=asc|desc allflg=0 reset_flag=1
# f) Refresh, POST action=dashboard subaction=refresh formPosted=no
#   response is HTML so as per 1 a-c), picks up last saved Session settings

# 3) Actions on Resource Dashboard, under Resources
# a) Click on Box in Resource dashboard, GET action=dashboard box=inprogress boxop=8590 boxcust=ALL boxmedia=undefined
#    as per 1a-c, results in
#    POST action=dashboard mode=json boxopcoid=8590 boxreject= orderbyid=5 sortby=asc allflg=0 reset_flag=1
# b) Click on User in Resource dashboard, GET action=dashboardeditresource type=GET mode=json userID=2522 selectedDate=2017-02-20
#   then POST formPosted=yes action=dashboard assigneeFilter=2522 myjobsFlg=Yes
#   eventually POST action=dashboard&mode=json boxreject= orderbyid=5 sortby=asc allflg=0 reset_flag=2

sub Dashboard {
	my ( $query, $session ) = @_;

	my $mode				= $query->param( 'mode' ) || '';

	my $vars = { 
		userid				=> $session->param( SessionConstants::SESS_USERID ),		# Can be overridden by assigneeFilter in Dashboard_myjobs
		assignee			=> $session->param( SessionConstants::SESS_USERID ),
		roleid				=> $session->param( SessionConstants::SESS_ROLEID ),
		rolename			=> $session->param( SessionConstants::SESS_ROLENAME ),
		session_user_id		=> $session->param( SessionConstants::SESS_USERID ),
		session_opco_id		=> $session->param( SessionConstants::SESS_OPCOID ),
		session_opco_name	=> $session->param( SessionConstants::SESS_OPCONAME ),
		opcoid				=> $session->param( SessionConstants::SESS_OPCOID ),		# Can be overridden by boxopcoid in Dashboard resource
		relaxroles			=> $session->param( SessionConstants::SESS_RELAXROLES ),
		usersettings		=> $session->param( SessionConstants::SESS_USERSETTINGS ),	# Current column settings
		highlightjob		=> $session->param( 'highlightjob' ),
		filter_values_updated => 0,	# Set to 1 for most calls, but not for My Jobs
		ref_data_present	=> 0,
		derived_job_data	=> 0,
		tag_env				=> uc( $ENV{TAG_ENV} || "DEV" ),	# Use when deciding to include SQL in response JSON (not in LIVE)
	};

	return Dashboard_page( $query, $session, $vars ) if ( $mode ne 'json' );

	my $start_time = time();
	my @history;

	my $subactions = { # subaction dispatch table
		'search2'		=> \&Dashboard_search2,		# Subsequent search returning job data
		'myjobs'		=> \&Dashboard_myjobs,		# My Jobs, only show jobs assigned
		'watchlist'		=> \&Dashboard_watchlist,	# Watchlist, jobs I'm watching
		'filtered'		=> \&Dashboard_view,		# Production Jobs ie Jobs at correct location, with filters applied
		'sortby'		=> \&Dashboard_view,		# (ditto) but as a result of clicking on Sort By
		'view'			=> \&Dashboard_view,		# (ditto) default if nothing identifiable
		'resource'		=> \&Dashboard_resource,	# From Resource dashboard with different set of filters
		'refresh'		=> \&Dashboard_view,		# Refresh added here but in reality gets intercepted, user clicks on Refresh button
		'filterupdate'	=> \&Dashboard_view,		# User changing filter settings, result just updates filter
		'filterapply'	=> \&Dashboard_view,		# User clicks on Apply filter, updates both filters and job list
		'refdata'		=> \&Dashboard_refdata,		# Return Reference data and Job filter data structure plus some session related data
		'createdbyme'	=> \&Dashboard_createdByMe,	# Jobs created by user
		'kanban'		=> \&Dashboard_view, 		# Like view but return just kanban data (eed to refactor into 1 call returning html)
	};

	eval {
		# Initial checks on callers userid, roleid, opcoid, customers etc, these calls can throw exceptions

		get_role_specific( $vars->{roleid}, $vars ); # Set permissions, also checks access

		if( $vars->{roleid} != ROLE_EXTERNAL ){   # External users doesn't require user specific operations
			get_user_specific( $vars->{userid}, $vars ); # Get users opcos, see if user has any customers
		}

		# What follows is a bit of a fiddle, to deal with params being passed on a GET, saved and then being
		# used on a subsequest POST. Especially useful when going from the Resource dashboard to Production page.

		my $subaction = $query->param( 'subaction' ) || '';
		push( @history, "On entry  subaction is $subaction" ) unless $subaction eq 'search'; # Dont update history if search

		my $saved_query = $session->param( DASHBOARD_SAVED_QUERY );
		if ( defined $saved_query  ) {

			if ( $subaction eq 'refdata' ) {
				push( @history, "Restoring saved query values for '$subaction', query '$saved_query'" );
				restore_query_values( $query, $session, 1 ); # Restore but keep for the subsequent view request

			} elsif ( $subaction eq 'view' ) {
				push( @history, "Restoring saved query values for '$subaction', query '$saved_query'" );
				$query->param( -name => 'subaction', -value => '' ); # Allow restore_query_values to change subaction
				restore_query_values( $query, $session ); # By default clears the saved query
				$subaction = $query->param( 'subaction' ) || '';
				push( @history, "Restored saved query values, subaction now '$subaction'" );

				# Also clear last saved sub action
				$session->clear( DASHBOARD_SAVED_SUBACTION );

			} else {
				push( @history, "Ignoring saved query values for '$subaction', query '$saved_query'" ) unless ( $subaction eq 'search' );
				# Something other than view
				$session->clear( DASHBOARD_SAVED_QUERY );
			}
		}

		get_common_settings( $query, $session, $vars ); # Other various settings, eg orderbyid, sortby, sortind, sidemenu etc

		# Get opcoid related flags in sub action handlers if needed, rather than here 
		
		# Decide subaction if not already supplied

		my $searchTypeId	= $query->param( QP_SEARCH_TYPE_ID );
		my $searchContent	= $query->param( QP_SEARCH_CONTENT );
		my $jobnumber		= $query->param( 'jobnumber' ); 		# jobnumber= is shortcut for searchTypeId=1 searchContent=...
		my $myjobsflg		= $query->param( 'myjobsFlg' ) || '';	# Set to Yes instead of action=myjobs
		my $archived		= $query->param( QP_ARCHIVED );			# Set to true to include archived jobs

		$vars->{noderived} = $query->param( 'noder' ) || 0;			# For testing purpose, use noder=1 to omit derived data
		$vars->{noreference} = $query->param( 'noref' ) || 0;		# For testing purpose, use noref=1 to omit reference data

		if ( defined $archived ) {
			$archived = lc( $archived );
			$archived = ( $archived =~ /^(true|1)$/ ); # Convert "true" or "1" to 1, anything else to 0
		} else {
			$archived = 0;
		}
		$vars->{archived} = $archived;

		# if ( ( $subaction eq '' ) || ( $subaction eq 'view' ) ) {

		if ( $subaction eq '' ) {
			if ( $myjobsflg eq 'Yes' ) {
				$subaction = 'myjobs';
				push( @history, "myjobs" );

			} elsif ( ( defined $searchTypeId ) && looks_like_number( $searchTypeId ) && ( $searchTypeId != 0 ) ) {
				$subaction = 'search2'; # Main search query after initial Search returned at least 1 row
				$vars->{searchTypeId} = $searchTypeId;
				$searchContent = '' unless defined $searchContent;
				$vars->{searchContent} = $searchContent;
				push( @history, "search, typeId $searchTypeId, content '$searchContent'" );

			} elsif ( ( defined $jobnumber ) && looks_like_number( $jobnumber ) ) {
				$subaction = 'search2';  # @TODO maybe just 'search'
				$vars->{searchTypeId} = 1;
				$vars->{searchContent} = $jobnumber;

			} elsif ( $vars->{sidemenu} eq "SidepanelWatchJob" ) {
				$subaction = 'watchlist';
				push( @history, "watchlist" );

			} elsif ( $vars->{sidemenu} eq "SidepanelJob" ) {
				$subaction = 'filtered';
				push( @history, "Filtered jobs" );

			} elsif ( defined $vars->{orderbyid} ) { 
				# Click on Sort by, action=dashboard mode=json orderbyid=n sortby=asc
				$subaction = 'sortby';

			} else {
				push( @history, "defaulting to view" );
				$subaction = 'view';
			}
		}	

		# For operator login default need to show only myjobs
		# On user login, session.opmyjobs is set in complete_login, in users.pl
		# So ensure the 1st dashboard Ajax call tests and clears opmyjobs and if role is operator, force myjobs.

		my $opmyjobs = $session->param( SessionConstants::SESS_OPMYJOBS ); # set by complete_login in users.pl
		if ( ( defined $opmyjobs ) && ( $opmyjobs == 1 ) ) {  # Make sure we clear opmyjobs the first time even if role is not operator
			if ( ( $vars->{roleid} == 1 ) && ( $subaction eq 'view' ) && ( ! $query->param('noopmyjobs') ) ) {
				push( @history, "opmyjobs, setting subaction=myjobs" );
				$subaction = 'myjobs';
			}
			$session->param( SessionConstants::SESS_OPMYJOBS, 0 );
		}

		# For Sort By and Refresh, we just repeat the last action, otherwise save the last action
		# Click on Refresh action=dashboard formPosted=no subaction=refresh
		# @TODO deal with allflg, reset_flag

		my $saved_subaction = $session->param( DASHBOARD_SAVED_SUBACTION ) || '';
		if ( $subaction =~ /(sortby|refresh)/ ) {

			if ( $saved_subaction eq '' ) {
				push( @history, "Subaction is '$subaction', but no saved subaction, so using view" );
				$subaction = 'view';
			} else {
				push( @history, "Subaction is '$subaction', using saved subaction '$saved_subaction'" );
				$subaction = $saved_subaction;

				if ( $subaction eq 'resource' ) {
					# @TODO restore last resource params if needed from DASHBOARD_SAVED_RESOURCE_PARAMS
				}
			}

		} elsif ( $subaction =~ /(search|search2|refdata)/ ) {
			# Dont save the last subaction
			push( @history, "Subaction is '$subaction', clearing last subaction" ) unless ( $subaction eq 'search' );
			$session->clear( DASHBOARD_SAVED_SUBACTION );

		} else {
			# Save the subaction (so will work for Watchlist and My Jobs, but not save filter settings)
			push( @history, "Subaction is '$subaction', setting last subaction to '$subaction'" );
			$session->param( DASHBOARD_SAVED_SUBACTION, $subaction );	
		}
		$vars->{subaction} = $subaction;
		# push( @history, "Dispatching, subaction '$subaction'" );
		$vars->{history} = \@history;

		my $func = $subactions->{ $subaction };
		die "Dashboard: invalid subaction '$subaction'" unless defined $func;

		# Dispatch to the sub action handler, which takes care of writing the JSON response

		eval {
			$func->( $query, $session, $vars );
		};
		if ( $@ ) {
			my $error = $@;
			#$error =~ s/ at .*//gs;
			$vars->{error} = "Dashboard::$subaction error, $error";
		}
	};
	if ( $@ ) {
		my $error = $@;	# make sure roleid is set, as that enables menu items if not set
		$error =~ s/ at .*//gs;
		$vars->{error} = $error;
	}

	$vars->{elapsedtime} = time() - $start_time;

	my $json = encode_json ( $vars );
	print $json;
	return $json;
}

# Render the job dashboard page

# Inputs ( $query, $session, $vars )
# Vars (hash) has settings from session namely: 
# - userid, assignee (defaults to userid), roleid, rolename, session_user_id, session_opco_id, relaxroles, usersettings
#
# Outputs in $vars, Template dashboard.html uses 
# - roleid, assignee, relaxroles, usersettings (supplied as inputs by Dashboard)
# - userinteractive, set by get_user_specific
# - usejobrepository, estimateon, isjobassignment, (opcoid specific)
# - orderbyid, sortby, sortInd, sidemenu, searchTypeId, sidepanel=0 (set in Dashboard)
# - sendtovendorpermission, unlockjobpermission, (set by get_role_specific)
# - rnd_no, sessionid, (see call to StreamHelper::createKey below))
# header.html uses nomenu, extracss, login_associate, roleid, jobbag_url, archivejoburl, mongooseserver, set by setHeaderVars 
# footer.html uses nomenu; lefthandnav.html uses sidemenu, noleftpanel=0, roleid; escompare.html - ?
#
sub Dashboard_page {
	my ( $query, $session, $vars ) = @_;

	eval {
		# Just plain GET/POST, so prepare to render dashboard.html but save the query variables passed, to be restored on next JSON request

		save_query_values( $query, $session );

		# Initial checks on callers userid, roleid, opcoid, customers etc, these calls can throw exceptions

		get_role_specific( $vars->{roleid}, $vars ); # Set permissions, also checks access

		get_user_specific( $vars->{userid}, $vars ); # Get users opcos, see if user has any customers

		# Get flags for the users current opcoid (so may be at odds if user redirecting to resource page with boxopcoid option)
		my $opflags = get_operating_company_flags( $vars->{opcoid} ); # set publish, ArchantChk, estimateon, isjobassignment, isadgatesend, isthumbview, bookingformnav

		# Copy flags into vars structure (as HTML template uses them unqualified)
		foreach my $k ( keys %$opflags ) {
			$vars->{ $k } = $opflags->{ $k };
		}

		my @orderbylist = ECMDBJoblist::getSortByList( $opflags, $vars->{roleid}, $vars->{rolename}, $vars->{relaxroles} ); # returns array of hash { column_id, orderbyid, label, sortmenu }

		$vars->{orderbylist} = \@orderbylist;
		$vars->{orderbyid} = $query->param( 'orderbyid' ) || '';

		$vars->{searchTypeId} = $query->param( QP_SEARCH_TYPE_ID ) || 1;
		$vars->{searchContents} = $query->param( QP_SEARCH_CONTENT ) || '';
		$vars->{assignee} = $query->param( 'assignee' ) || $vars->{userid};

		my $createdbycount = 0;
		my $assigntomecnt = 0;
		eval {
			$assigntomecnt = ECMDBJoblist::db_getAssignedtomecount( $vars->{userid}, $vars->{opcoid} );
			my $employeeid = ECMDBJoblist::db_getEmployeeid( $vars->{userid} );
			$employeeid =~ s/^\s*//g;
			if ( $employeeid ne '' ) {
				$createdbycount = ECMDBJoblist::db_getCreatedbycount( $vars->{userid}, $employeeid, $vars->{opcoid} );
			}
		};
		if ( $@ ) {
			my $error = $@;
			$error =~ s/ at .*//gs;
			warn "joblist: getting assigntome/createdbyme count: $error\n";
		}
		$vars->{createdbycount} = $createdbycount;
		$vars->{assigntomecnt} = $assigntomecnt;

		my @pinnedfilters     = ECMDBUserFilter::getUserFilterSettings( $vars->{userid}, $vars->{opcoid} );
		$vars->{pinnedfilters} = \@pinnedfilters;

		# PD-2622 Generate random number to pass to stream scripts. Only do if displaying HTML as that has the configSettings.
		$vars->{ rnd_no } = StreamHelper::createKey( $session );
		$vars->{ sessionid } = $session->id;

		# Add build info to right of Filter div (except on Live)
		$vars->{ buildinfo } = '';
		if ( $vars->{ tag_env } ne 'LIVE' ) {

			# See if buildid.txt exists, if not display current working directory
			my $document = '';
			eval {
				my $file = "buildid.txt";
				$document = do {
				    local $/ = undef;
				    open my $fh, "<", $file or die "could not open $file: $!";
				    <$fh>;
				};
			};
			if ( $@ ) {
				$document = cwd();
			}
			chomp $document;
			$vars->{ buildinfo } = $vars->{ tag_env } . ' ' . $document;
		}

	};
	if ( $@ ) {
		my $error = $@;	# make sure roleid is set, as that enables menu items if not set
		warn "Dashboard_page: $error\n";
		$error =~ s/ at .*//gs;
		$vars->{error} = $error;
		if ( $vars->{error} =~ /^Invalid/ ) {
			# General::NoAccess(); // save dragging in general module
			$vars->{ message } = 'Your role does not permit access to Job dashboard';
			$tt->process('ecm_html/noaccess.html', $vars ) || die $tt->error(), "\n";
			return;
		}
	}
	$vars->{ job_dashboard } = 1;
	# push( @{ $vars->{extracss}}, 'joblist' ); -- done in dashboard.html directly

	$vars->{session} = $session; # Make all session variables visible from template

	General::setHeaderVars( $vars, $session ); # sets jobbag_url, archivejoburl, mongooseserver, etc

	$tt->process( 'ecm_html/dashboard.html', $vars ) || die $tt->error(), "\n";
}

# SearchJobs now in search.pl (as it loads the JFElastic module and all that entails)

# Dashboard_refdata - return Reference data ie unchanging when user switches opcoid/role
# Also return Job filter data structure, used in custom.js for anything filter related
# TODO return useropcos

sub Dashboard_refdata {
	my ( $query, $session, $vars ) = @_;

	eval {
		dashboard_ref_data( $vars );

		my $opflags = get_operating_company_flags( $vars->{opcoid} );
		$vars->{opflags} = $opflags;

		$vars->{adsendurl} = Config::getConfig("adsendurl") || 'Unknown adsendurl';

		$vars->{jobs_per_page} = int( Config::getConfig( JOBLIST_JOBS_PER_PAGE_KEY ) ) || JOBLIST_JOBS_PER_PAGE_DEFAULT;
		$vars->{apply_timer_period} = int( Config::getConfig( JOBLIST_APPLY_TIMER_PERIOD_KEY ) ) || JOBLIST_APPLY_TIMER_PERIOD_DEFAULT;

		my $symbol_table = Filters::getMatchConditionSymbols( $opflags, $vars->{roleid}, $vars->{rolename}, $vars->{relaxroles} );

		my $filters = Filters::get_joblist_filters( $symbol_table );
		# push( @{$vars->{history}},  "Filters " . Dumper( $filters ) );
		$vars->{job_filter} = $filters;

		# Get list of all columns (filtered by roleid, opco flags, so there should be no duplicate column_id)
		my $column_desc = ECMDBJoblist::getVisibleColumns( $vars->{userid}, $vars->{opcoid}, $symbol_table );
		# Returns [ { column_id, column_label, column_name, opflags, roles, type, default_val, null_equiv } ] + visible

		# warn "Post getVisibleColumns\n";
		foreach my $r ( @{$column_desc} ) {
			# warn " Column " . $r->{column_id} . ", sort " . $r->{sort_order} . "\n";
		}

		$vars->{columns} = $column_desc;

		# @TODO these could be held in tables rather than hard coding here
		$vars->{fromdatelist} = [ 
			{ id => "1", name => "1 day" }, { id => "3", name => "3 days" },
			{ id => "7", name => "1 week" }, { id => "21", name => "3 weeks" }, 
			{ id => "30", name => "1 month" }, { id => "90", name => "3 months" }, 
			{ id => "180", name => "6 months" }, { id => "270", name => "9 months" },
			{ id => "365", name => "1 year" }, { id => "730", name => "2 years" }, { id => "1095", name => "3 years" },
			{ id => "1461", name => "4 years" }, { id => "1826", name => "5 years" }, { id => "3652", name => "10 years" },
			
		 ];
		$vars->{archivelist} = [ { id => 1, name => "Show Archived" } ];

		push( @{$vars->{filter_columns}}, 'fromdatelist', 'archivelist' );

		# Get order by list (used in joblist_draw_page for grouping)

		my @orderByList = ECMDBJoblist::getSortByList( $opflags, $vars->{roleid}, $vars->{rolename}, $vars->{relaxroles} );
		$vars->{orderbylist} = \@orderByList;

		# Add customer list too as user will need to select a customer before selecting a large fromId (to avoid getting error message)

		my $customers = ECMDBCustomerUser::getUsersCustomerNamesByOpco( $vars->{userid}, $vars->{opcoid} ); # returns array of array
		my @agencies;
		if ( ! defined $customers ) {
			push( @{$vars->{history}}, "getUsersCustomerNamesByOpco returned undef" );
		} else {
			push( @{$vars->{history}}, "Adding agencies, count " . scalar( @{$customers} ) );
			my $index = 0;
			foreach my $c ( @{$customers} ) {
				my ( $custname ) = @{$c};
				$index++;
				push( @agencies, { name => $custname } );
			}
			$vars->{agencies} = \@agencies;
			push( @{$vars->{filter_columns}}, 'agencies' );
			$vars->{filter_values_updated} = 1;
		}
		my $producers = ECMDBJoblist::db_getproducernamesbyopco( $vars->{opcoid} ); # returns array
		$vars->{producers} = $producers;

	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at modules.*//gs;
		$vars->{error} = $error;
	}
}

sub get_product_assignee_data {
	my ($jobs,$vars) = @_;

	my @product_assignee_details;

	my $job_number = "";
	foreach my $job ( @$jobs ) {
		Validate::job_number($job->{job_number});	
		$job_number	=	$job_number.$job->{job_number}.",";
	}
	$job_number =~ s/,$//; #Eliminating last comma
   	
	@product_assignee_details	=	ECMDBJoblist::getProductAssignee($job_number);

	my $assigned_product;
	my $count;
	foreach my $data (@product_assignee_details) {
		#if count is 1 => the multiple products assigned to diff user.
		#if count is 2 => user completed all the product for that job_number
		#If the count is 0, products are not assigned to anyone (or) only one product is assigned to single user
		$count = 0;
		if(	$data->{product_asisgned_user_count} > 1 )	{
			$count	=	1;
		} 
		elsif( $data->{completed_product_count} > 0 && $data->{product_asisgned_user_count} == 0 ) {
			$count	=	2;	
		}
		elsif( $data->{product_asisgned_user_count} == 1) {
			#If the only one product is assigned to any user, update that user as a job assignee.
            if($data->{userid} > 0)
            {
                ECMDBJoblist::updateJobAssignee($data->{userid},$data->{job_number});
            }
			$count = 0;
		}
		
		$assigned_product->{ $data->{job_number} } = $count;
	}
	$vars->{product_assignee}	=	$assigned_product;	
}

sub get_product_op_status_data {
	my ($jobs,$vars) = @_;
	my @product_op_status_details;

	my $job_number = "";
	foreach my $job ( @$jobs ) {
		Validate::job_number($job->{job_number});
		$job_number =   $job_number.$job->{job_number}.",";
	}
	$job_number =~ s/,$//; #Eliminating last comma

	@product_op_status_details = ECMDBJoblist::getProductOpStatus($job_number);

	my $product;
	my $count;
	my ($total_count, $none_count, $total_assigned, $total_working, $total_completed);

	foreach my $data (@product_op_status_details) {
		$count = 0;	# if count = 0 then opstatus is None

		$total_count	= $data->{product_count};
		$none_count		= $data->{none_count};
		$total_assigned	= $data->{product_asisgned_count};
		$total_working	= $data->{product_working_count};
		$total_completed = $data->{product_completed_count};	

		if( $total_count == $none_count ){
			$count = 0;
		}
		elsif( ($total_count == $total_assigned) || ($total_count == ($total_assigned + $none_count)) )  {
			$count = 1;		# if count = 1 then opstatus is Assigned
		}
		elsif( ($total_count == $total_working) || ($total_count == ($total_working + $none_count)) )  {
			$count = 2;		# if count = 2 then opstatus is Working
		}
		elsif( ($total_count == $total_completed) || ($total_count == ($total_completed + $none_count)) )  {
			$count = 3;		# if count = 3 then opstatus is Completed
		}
		elsif( ($total_assigned > 0) && (($total_working > 0) || ($total_completed > 0)) ) {
			$count = 4;		# if count = 4 then opstatus is multiple
		}
		elsif( ($total_working > 0) && (($total_assigned > 0) || ($total_completed > 0)) ) {
			$count = 4;		# if count = 4 then opstatus is multiple
		}
		elsif( ($total_completed > 0) && (($total_assigned > 0) || ($total_working > 0)) ) {
			$count = 4;		# if count = 4 then opstatus is multiple
		}

		$product->{ $data->{job_number} } = $count;
	}

	$vars->{product_opstatus} = $product;
}

1;

