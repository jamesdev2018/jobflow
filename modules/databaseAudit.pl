#!/usr/local/bin/perl
package ECMDBAudit;

use strict;
use Scalar::Util qw( looks_like_number );
use Carp qw/carp/;
use Encode;
use utf8;
use URI::Escape;
use URI::URL;

use lib 'modules';

use Validate;

require "config.pl";

use constant MAX_AUDIT_CHUNKS => 10;
use constant CHUNK_SIZE => 255; # ie size of auditlog.changelog (VARCHAR)

##############################################################################
#       db_auditLog()
#
##############################################################################
sub db_auditLog {
	
	my ( $userid, $tablename, $rowid, $changelog ) = @_;

	# rowid and changelog are nullable columns

	eval {
		Validate::userid( $userid );

		die "Invalid table name" unless ( defined $tablename );
		die "Tablename $tablename too long" if ( length( $tablename ) > 100 );

		die "rowid $rowid is not a number" if  ( defined $rowid ) && ( ! looks_like_number( $rowid ) );# rowid can be NULL

		my $changelen = 0;
		if ( defined $changelog ) { # For some reason, calls to db_auditLog can specify undef for changelog
			chomp $changelog;
			$changelen = length( $changelog );
			if ( $changelen > CHUNK_SIZE ) {
				# If this string contains UTF8 characters but as a 2 or 3 byte sequence rather than code points then decode back to code points
				# which will then mean we can use length and substr on characters rather than the encoded bytes
				if ( ! Encode::is_utf8( $changelog ) ) {
					eval {
						my $changelog2 = Encode::decode_utf8( $changelog ); # Convert to code points
						if ( ( defined $changelog2 ) && ( length ( $changelog2 ) > 0 ) && length ( $changelog2 ) < $changelen ) {
							$changelog = $changelog2;
							$changelen = length( $changelog );
							# warn "changelog length now $changelen after decode";
						}
					};
					if ( $@ ) {
						# In case of decode error, just stick with supplied string
						warn "db_auditLog: Oops, $@"; 
					}
				}
			}
		}
		for ( my $i = 0; $i < MAX_AUDIT_CHUNKS; $i++ ) { 

			my $chunk;

			if ( ! defined $changelog ) {
				# leave chunk undefined - its a row with null changelog

			} elsif ( $i == 0 ) { # First row
				if ( $changelen <= CHUNK_SIZE ) {
					# First row but not chunked
					$chunk = $changelog;
					$changelog = undef;
				} else {
					# First row of chunked data
					my $wanted = CHUNK_SIZE - 3;
					$chunk = substr( $changelog, 0, $wanted ) . '...';
					$changelog = substr( $changelog, $wanted );
				}
			} elsif ( length( $changelog ) <= (CHUNK_SIZE - 3) ) {
				# Last row of chunked data
				$chunk = '...' . $changelog;
				$changelog = undef;
			} else {  # Subsequent row (allow for ... at start and ... at end)
				$chunk = '...' . substr( $changelog, 0, CHUNK_SIZE - 6 ) . '...';
				$changelog = substr( $changelog, CHUNK_SIZE - 6 );
			}

			$Config::dba->process_sql("INSERT INTO AUDITLOG (USERID, TABLENAME, ROWID, CHANGELOG, DATETIME) 
					VALUES ( ?, ?, ?, ?, now() )", [ $userid, $tablename, $rowid, $chunk ]);

			last if ( ! defined $changelog );
		}
	}; 
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$userid ||= '';
		$tablename ||= '';
		$rowid ||= 0;
		$changelog ||= '';

		warn "db_auditLog: Failed userid '$userid', table '$tablename', row '$rowid', change '$changelog', $error";
	}
}

##############################################################################
#       db_getLogs()
#
##############################################################################
sub db_getLogs {

	my $jobid 	= $_[0];
	my $userid = $_[1];
	my $from = $_[2];
	my $to = $_[3];

	my $jobClause = "";
	my $jobClauseArchive;
	my $rows;
    my @filelist;

	eval {
		Validate::job_number( $jobid ) if ( $jobid ); # Not sure if called with undef or 0 ...

		my $idValue = "";
		$idValue = db_notesID( $jobid ) if ( $jobid ); 
		my $messageidvalue = "";
		$messageidvalue =  db_getMessagesId( $jobid ) if ( $jobid );

		my $userlink = "";
		$userlink = " AND a.userid = $userid " if ( $userid );
		my $fromlink = "";
		$fromlink = " AND a.DATETIME >= '$from' " if( $from );
        my $tolink = "";
		$tolink = " AND a.DATETIME <= '$to' " if( $to );

		$jobClause = " AND a.rowid = $jobid " if ( $jobid );
		$jobClause .= " AND a.userid = $userid " if ( $userid );
		$jobClause .= " AND a.DATETIME >= '$from' " if( $from );
		$jobClause .= " AND a.DATETIME <= '$to' " if( $to );
		$jobClause .= " OR (a.rowid IN ($idValue) AND a.tablename IN ('JOBNOTES') $userlink $fromlink $tolink) " if ( $idValue );
		$jobClause .= " OR (a.rowid IN ($messageidvalue) AND a.tablename IN ('messages') $userlink $fromlink $tolink)  " if ( $messageidvalue );


		$rows = $Config::dba->hashed_process_sql(
			"SELECT u.username, a.tablename, a.rowid, a.changelog, a.datetime, a.userid  FROM (
				SELECT a.userid, a.tablename, a.rowid, a.changelog, a.datetime FROM auditlog a
					WHERE 1=1 $jobClause
					ORDER BY a.id DESC
					LIMIT 500
				) a
			INNER JOIN users u ON u.id = a.userid;"
		);
=cc
        if ( defined $rows ) {
            foreach my $files (@$rows) {
				my $changelog = $files->{changelog};
				$changelog = uri_escape_utf8($changelog) if defined $changelog;
                push(@filelist, {
                    changelog => ( $changelog ) || "-",
                    username => ($files->{username}) || "-",
                    tablename => ($files->{tablename}) || "-",
                    rowid => ($files->{rowid}) || "-",
                    datetime => ($files->{datetime}),
                    userid => ($files->{userid}) || "-"});
            }
        }
=cut


	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "db_getLogs: $error";
	}		
	#return @filelist;;
	return $rows ? @$rows : ();

}

##############################################################################
##       db_notesID
##
###############################################################################
sub db_notesID {
	my $myjobid	= $_[0];

	my @results;

	eval {
		Validate::job_number( $myjobid );

		my $rows = $Config::dba->hashed_process_sql( "SELECT id FROM JOBNOTES WHERE job_number=?", [ $myjobid ] );

		foreach my $row (@$rows) {
			my $id = $row->{id};
			push( @results, $row->{id} );
		}
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "db_notesID: $error";
	}
	return join( ',', @results );
}

##############################################################################
###       db_getMessagesId()
###
################################################################################
sub db_getMessagesId {
    my $myjobid = $_[0];

	my @results;

	eval {
		Validate::job_number( $myjobid );
		my $rows = $Config::dba->hashed_process_sql("SELECT messageid FROM messages WHERE job_number=?", [ $myjobid ]);
		foreach my $row (@$rows) {
			push( @results, $row->{messageid} );
		}
    };
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "db_getMessagesId: $error";
	}
    return join( ',', @results );
}

##############################################################################
## 
##############################################################################
sub updateHistory {
	my ($userid, $jobid) = @_;

	eval {
		Validate::job_number( $jobid );
		Validate::userid( $userid );
		$Config::dba->process_sql("INSERT INTO UPLOADHISTORY (job_number, userid, rec_timestamp) VALUES (?, ?, now())", [ $jobid, $userid ]);
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "updateHistory: $error";
	}
	return 1;
}

##############################################################################
##
##############################################################################
sub updateVendorHistory {
	my ($jobid, $userid, $vendorid) = @_;

	eval {
		Validate::job_number( $jobid );
		Validate::userid( $userid );
		die "Invalid vendor id"  if ( ! defined $vendorid ) ||  !looks_like_number( $vendorid );
	
		$Config::dba->process_sql(
			"INSERT INTO RELEASETOVENDORHISTORY (job_number, userid, vendorid, rec_timestamp) VALUES (?, ?, ?, now())", 
			[ $jobid, $userid, $vendorid ] );
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "updateVendorHistory: $error";
	}
	return 1;
}

#############################################################################
#       logOutsourceHistory()
# 		record that this job has been outsourced to xyz on dd/mm/yy 
#
##############################################################################
sub logOutsourceHistory {
	my ($jobid, $userid, $outsourceopcoid) = @_;

	eval {
		Validate::job_number( $jobid );
		Validate::userid( $userid );
		die "Invalid soutsource opcoid" if ( ! defined $outsourceopcoid ) || !looks_like_number( $outsourceopcoid );
	
		$Config::dba->process_sql("INSERT INTO OUTSOURCEHISTORY (job_number, userid, outsourceopcoid, rec_timestamp) VALUES (?, ?, ?, now())", 
			[ $jobid, $userid, $outsourceopcoid ]);
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "logOutsourceHistory: $error";
	}
	return 1;
}

#############################################################################
#       db_recordJobAction() - cloned from databaseJob.
#
##############################################################################
sub db_recordJobAction {
	my ($jobid, $userid, $action, $destination) = @_;

	eval {
		# job_number, userid and destination are nullable (entries from Feb 2016 with action=docdownload userid=null)
		Validate::job_number( $jobid ) if defined $jobid;
		Validate::userid( $userid ) if defined $userid;
		# TODO validate action & destination

		$Config::dba->process_sql("INSERT INTO JOBEVENTS (job_number, userid, action, destination, rec_timestamp)
					VALUES (?, ?, ?, ?, now())", [ $jobid, $userid, $action, $destination ]);
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "db_recordJobAction: $error";
		return 0;
	}
	return 1;
}


##############################################################################

1;

