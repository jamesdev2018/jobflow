#!/usr/bin/perl
package Document;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use File::Path;
use File::Copy;
use LWP::Simple;
use JSON qw( decode_json );
use Net::FTP::File;
use URI::Escape;
use URI::URL;

use Template;
use Data::UUID;
use XML::Simple;
use Data::Dumper;
use Date::Simple ('date', 'today');
use JSON;
use POSIX qw/strftime/;
use Time::Local;
use Encode qw(decode encode);
use IO::Compress::Zip qw(zip $ZipError);
use Cwd;
use POSIX qw( strftime );
use Image::Size;
use Carp qw/carp/;

# Home grown
use lib 'modules';

use XMLResponse qw ( :statuses );
use CTSConstants;
use Validate;

require "config.pl";
require "general.pl";
require "jobpath.pl";
require "databaseJob.pl";
require "databaseUser.pl";
require 'databaseAudit.pl';
require "digitalassets.pl";
require "streamhelper.pl";

##################################################################
# the template object 
##################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";

sub getDocumentFolder {
	return Config::getConfig("documentAssetsFolder" ) || "SUPPORTING_DOCUMENTS";
}

sub getUploadDir {
	return Config::getConfig("uploaddirectory");
}

######################################################################
# showversion
# Under "documents" tab, from the document list - if you click on "History" from corresponding documents row
# this action will be called to show the version history dialog for that document
######################################################################
sub showversion {
        my ( $query, $session ) = @_;

        my $jobid               = $query->param( "jobid" );
		my $filename			= $query->param("filename");
		my $type				= $query->param("type") || '';
        my $mode                = $query->param( "mode" ) ||'';
        my $opcoid              = $session->param( 'opcoid' );

        my $customerid          = ECMDBJob::db_getCustomerIdJob( $jobid );
        my @checkversionhistory;

		if ( $type eq "document") {
        	@checkversionhistory = ECMDBJob::getdocVersionHistory( $jobid, $filename );
		} else {
        	@checkversionhistory = ECMDBJob::getVersionHistory( $jobid, $filename );
		}

        my $vars = {
			checkversionhistory	=>\@checkversionhistory,
			opcoid				=>$opcoid,
		};
        if ( $mode eq "json") {
                my $json = encode_json ( $vars );
                print $json;
                return $json;
        }
}

######################################################################################
# sub getFileNameAndExt: To get the file prefix and extension. (cloned from jobs.pl)
#
######################################################################################
sub getFileNameAndExt {
	my ( $fileName )    = @_;

	my $fileNamePrefix  = undef;
	my $fileExt         = undef;

	if ( $fileName =~ /^(.*)\.(.*)$/ ) {
		$fileNamePrefix = $1;
		$fileExt        = $2;
	}

	return ( $fileNamePrefix, $fileExt );
}

#########################################################################################
## rollback: Roll back ther version to have based on the user choice.
# Inside "Version history dialog" - versions of that document will be displayed 
# We can revert any version to current document - Revert / Current functionality
# on clicking "Revert / Current" this action will be called
##########################################################################################
sub rollback { 
		my ( $query, $session ) = @_; 

		my $jobid                       = $query->param( "jobid" );
		my $filename                    = $query->param("filename");
		my $version						= $query->param("version");
		my $type						= $query->param("type") ||'';
		my $mode                        = $query->param( "mode" ) ||'';
        my $assetID                     = $query->param( "assetID" );
		my $opcoid                      = $session->param( 'opcoid' );
        my $assetName                   = '';
 		my $userName                    = $session->param( 'username' );
        my $userid                      = $session->param( 'userid' );
		my $documentAssetsFolder		= getDocumentFolder();
		my $vars;

		eval {
			my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobid );

			my $folder;
			my $rollBack;
			# my $assetReopPath;
			my @checkversionhistory;

			$assetName  = ECMDBJob::db_getSingelAssetName($assetID,'Document');
			$filename   = encode("utf8",$assetName);

			$folder 	= $documentAssetsFolder;
			$rollBack	= ECMDBJob::rollBack( $jobid, $filename, $version, "JOBDOCUMENTS", "documentname" ); 
			@checkversionhistory = ECMDBJob::getdocVersionHistory( $jobid, $filename );
			my $assetReopPath = $jobpath . '/' . $folder;

			my ($splitfilename, $filetype) = getFileNameAndExt($filename);
			my $dup_filename = $splitfilename . '_v' . $version . '.' . $filetype;

			my ( $ftpServer, $ftpUserName, $ftpPassword ) = StreamHelper::db_getFTPDetails( $jobopcoid );
			die "Invalid FTP Server for opcoid $jobopcoid" unless ( $ftpServer );

			my $ftp = Net::FTP->new( $ftpServer, Debug => 0 ) or die "Cannot connect to FTP server $@";
			if ( $ftp ) {

				$ftp->login( $ftpUserName, $ftpPassword ) or die "Cannot login", $ftp->message;
				$ftp->cwd( "$assetReopPath" );
				$ftp->binary(); 
				$ftp->copy( "$assetReopPath/$dup_filename", "$assetReopPath/$filename" ) or die "Copy fails for from $dup_filename to $filename\n", $ftp->message;
				$ftp->close();
				$ftp = undef;
			}

			$vars = {
				checkversionhistory	=> \@checkversionhistory,
				opcoid				=> $opcoid,
				jobopcoid			=> $jobopcoid,  # Maybe it should be returning this instead TODO
			};
		};
		if ( $@ ) {
			my $error = $@;
			carp "rollback, $error";
			$error =~ s/ at .*//gs;
			$vars = { error => $error };
		}
		if ( $mode eq "json") {
			my $json = encode_json ( $vars );
			print $json;
			return $json;
		}
} #rollback

##########################################################################
# documentUpload: To upload the selected documents.
##########################################################################

sub documentUpload {
	my ( $query, $uploaderid, $opcoid, $upload_dir ) = @_; 

    my $jobid               = $query->param( "jobid" );
    my $documentOri_Name    = $query->param( "uploadDoc_Original" );        #documen original Name
    my $documentName        = $query->param( "uploadDoc" );        #document Name
	$documentName			=~ s/\s+|-/_/g;
    my $documentSize        = $query->param("documentSize");            #size of document
    my $documentType        = $query->param("documentType");            #type of document
    my $approved            = $query->param("qcstatus");
    my $mode                = $query->param( "mode" ) || '';
	my $documentAssetsFolder = getDocumentFolder();
	my $uploadUserName = ECMDBUser::db_getUsername( $uploaderid );
 
	my $weburl              = 'ABCD';
	my $approverid          = "0";
    my $current             = "1";
	my $vars;
	my $sourceAssetNameVer;
	eval {
		# quit if document size is 0
		die "Document $documentOri_Name is zero length" unless ( $documentSize > 0 );

		# upload to temp - start
		die "uploadDoc not supplied" unless ( $documentName );
        $documentName =~ s/\s+|-/_/g;

        my $docTempDir = sprintf("%s/%s/%s", "$upload_dir", "$documentAssetsFolder","$uploaderid");

        my ( $splitDocName, $docExt ) = getFileNameAndExt( $documentName );
        my $bak_docName = sprintf("%s_%s.%s", "$splitDocName", "bak", "$docExt");

		umask( 0 );

        my $upload_DocHandle = $query->upload( "doc" );

        if ( ! -e "$docTempDir" ) {
            mkpath( "$docTempDir", { mode => 0777, verbose => 0 } );
            chmod( 0777, "$docTempDir" );
        }

        my $filename = "$docTempDir/$bak_docName";
        open( UPLOADFILE, ">$filename" ) or die "Failed to open '$filename' for output, $!";
        binmode UPLOADFILE;
        while ( <$upload_DocHandle> ) {
            print UPLOADFILE;
        }
        close UPLOADFILE;
        chmod ( 0777, $filename );
		# upload to temp - end

		my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobid );
		my $destLocation = $jobpath . '/' . $documentAssetsFolder;

		#get max version to upload dup_files
		#Check file already availble in DB to update the version

		my $docVersion_ref      = ECMDBJob::db_DocumentVersion( $jobid, $documentName );
		my @docVersion_arr_ref  = @$docVersion_ref;
		my $maxVersion          = undef;

		foreach my $val ( @docVersion_arr_ref ) {
			my %docVersionHash = %$val;
			my @mKeys = keys ( %docVersionHash );
			$maxVersion = $docVersionHash{$mKeys[0]};
		}

		my $version = 1;
		if ( $maxVersion ) {
			$version = $maxVersion + 1;
			ECMDBJob::updateDocumentCurrentValue( $jobid, $documentName );
		}

		if ( $documentName ) {
			my ( $docNamePrefix, $docExt ) = getFileNameAndExt( $documentName );
			my $dup_DocumentName = sprintf( "%s_%s%d.%s", "$docNamePrefix", "v", "$version", "$docExt" );
			my $bak_docName = sprintf("%s_%s.%s", "$docNamePrefix", "bak", "$docExt");

			copy ( "$docTempDir/$bak_docName", "$docTempDir/$dup_DocumentName" );
			chmod ( 0777, "$docTempDir/$dup_DocumentName" );
			copy ( "$docTempDir/$dup_DocumentName", "$docTempDir/$documentName" );
			chmod ( 0777, "$docTempDir/$documentName" );
			#unlink("$docTempDir/$bak_docName");

			$sourceAssetNameVer = sprintf ("%s/%s", "$docTempDir", "$dup_DocumentName" );
			DigitalAssets::storeDigitalAssetsInGPFS( $jobid, $sourceAssetNameVer, $dup_DocumentName, $documentName, 1);
		}

		#For CTS report.
		ECMDBJob::db_recordJobAction($jobid, $uploaderid, CTSConstants::DOC_UPLOAD );

		#save file details in DB table jobasse
		#Check file already availble in DB to update the version

		ECMDBJob::loadDocument( $jobid, $customerid, $uploaderid, $weburl, $version, $documentName, $documentType, $documentSize, $approved, $approverid, $current );           
		#Log upload document history.
		ECMDBAudit::db_auditLog( $uploaderid, "DOCUPLOAD", $jobid, "Document $documentName uploaded by $uploadUserName" );
		if($documentName ne $documentOri_Name){
			ECMDBAudit::db_auditLog( $uploaderid, "DOCUPLOAD", $jobid, "Document $documentOri_Name has been renamed as $documentName by  $uploadUserName" );
		}
	
		$vars = {
			upLoaded => "YES",
		};
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => "documentUpload: ".$error };
	}

	return $vars;
}

##############################################################################
##### eraseDocument(): To delete the db, and physical content of document.
# Under documents tab - document list will be displayed - for each document there is an option "Delete"
# if you click on that "Delete" link this action will be called. 
# The document will be permanently deleted from the job path 
###############################################################################
sub eraseDocument {
	my ( $query, $session ) = @_;

	my $jobid						= $query->param( "jobid" );
	my $documentName				= $query->param( "documentName" );
	my $mode						= $query->param( "mode" ) || '';
	my $docDeleteUserID				= $session->param( 'userid' );
	my $docDelUserName				= ECMDBUser::db_getUsername( $docDeleteUserID );
	my $documentAssetsFolder		= getDocumentFolder();

	my $vars;
	my $ftp;

	eval {
		my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobid );

		my $docVersion_ref      		= ECMDBJob::db_DocumentVersion( $jobid, $documentName );
		#my $destRootDir         		= getAssetRepositoryJobPath( $jobid );
		my $destLocation        		= $jobpath . '/' . $documentAssetsFolder;

		# To delete the document details from DB.
		my $erasefromDB = ECMDBJob::eraseDocDBDetails( $jobid, $documentName );

		my $maxVersion = 0;
		my @docVersion_arr_ref  = @$docVersion_ref;

		foreach my $val ( @docVersion_arr_ref ) {
			my %docVersionHash = %$val;
			my @mkeys = keys ( %docVersionHash );
			$maxVersion = $docVersionHash{$mkeys[0]};
		}

		#To delete file in ftp location 
		my ( $ftpServer, $ftpUserName, $ftpPassword ) = StreamHelper::db_getFTPDetails( $jobopcoid ) ;
		die "Invalid FTP Server for opcoid $jobopcoid" unless ( $ftpServer );

		my $ftp = Net::FTP->new( $ftpServer, Debug => 0 ) or die "Cannot connect to $ftpServer, $@";
		die "No FTP object created for $ftpServer" unless ( $ftp );

		$ftp->login( $ftpUserName, $ftpPassword ) or die "Cannot login", $ftp->message;
		$ftp->cwd( "$destLocation" );

		# check whether the file exists or not
		my $isFileExists = $ftp->isfile($documentName);
		
		if( $isFileExists ){
			$ftp->delete( "$documentName" ) or die "Asset deletion failed for $jobid, $documentName\n", $ftp->message;
		}

		#To delete assets including version files.
		my ($splitfilename, $filetype) = getFileNameAndExt($documentName);

		for ( my $i = 1; $i <= $maxVersion; $i++ ) {
			my $assetFileNameVer = sprintf ( "%s/%s_v%s.%s", "$destLocation", "$splitfilename", "$i", "$filetype" );
			$ftp->delete( "$assetFileNameVer" );
		}

		$ftp->close();
		$ftp = undef;

		#Log deleted document history.
		ECMDBAudit::db_auditLog( $docDeleteUserID, "DOCDELETE", $jobid, "Document $documentName deleted by $docDelUserName" );

		$vars = {
			Doc_job_num     => $jobid,
		};
	};
	if ( $@ ) {
		my $error = $@;
		carp "eraseDocument, $error";
		$error =~ s/ at .*//gs;
		$vars = { error => $error };
	}

	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

#################################################################
# Inside "Version history dialog" - we can able to delete versions of the documents other than current
# so on clicking "Delete" link this action will be called
#
#################################################################
sub deleteAsset {
	my ( $query, $session ) = @_; 

	my $documentName        = $query->param( "filename" );
	my $deleteFileVersion   = $query->param("deleteFileVersion");
	my $jobid               = $query->param( "jobid" );
	my $mode                = $query->param( "mode" ) || '';
	my $type                = $query->param( "type" ) || '';
	my $userid				= $session->param( 'userid' );
	my $username			= $session->param( 'username' );
	my $documentAssetsFolder = getDocumentFolder();

	my $vars;
	eval {
		die "documentName not supplied" unless ( $documentName );
		$documentName =~ s/\s+|-/_/g;
		my ( $fileNamePrepix, $fileExt ) = getFileNameAndExt( $documentName );

		die "deleteFileVersion not supplied" unless ( $deleteFileVersion );

		my $deleteAssetName	= sprintf("%s_v%s.%s", "$fileNamePrepix", "$deleteFileVersion", "$fileExt");
		my $assetID;
		my $retVal;
		my @checkversionhistory;

		$assetID	= ECMDBJob::db_getAssetID( $jobid, $documentName, $deleteFileVersion, "JOBDOCUMENTS", "documentname" );
		$retVal     = ECMDBJob::db_deleteAsset( $jobid, $documentName, $deleteFileVersion, "JOBDOCUMENTS", "documentname" );

		#Third argument indicates that the assets is version type.
		DigitalAssets::deleteDigitAssetFrmGPFS( $jobid, $deleteAssetName, 1, $type );	
		@checkversionhistory = ECMDBJob::getdocVersionHistory( $jobid, $documentName );

		if ( $retVal =~ /^1$/ && $assetID ne "" ) {
			ECMDBAudit::db_auditLog( $userid, "DOCDELETE", $jobid, "Document $documentName ($assetID, version: $deleteFileVersion) was deleted by user: $username " );
		}

		$vars = {
			checkversionhistory     =>\@checkversionhistory,
		};
	};
	if ( $@ ) {
		my $error = $@;
		carp "deleteAsset, $error";
		$error =~ s/ at .*//gs;
		$vars = { error => $error };
		ECMDBAudit::db_auditLog( $userid, "DOCDELETE", $jobid, "Document $documentName (version: $deleteFileVersion) deletion failed by user: $username " );
	}

	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
} # end of deleteAsset

1;
