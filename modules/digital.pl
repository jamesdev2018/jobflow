#!/usr/local/bin/perl
package Digital;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use File::Path;
use File::Copy;
use LWP::Simple;
use JSON qw( decode_json );
use Net::FTP::File;
use URI::Escape;
use URI::URL;

use Template;
use Data::UUID;
use XML::Simple;
use Data::Dumper;
use Date::Simple ('date', 'today');
use JSON;
use POSIX qw/strftime/;
use Time::Local;
use Encode qw(decode encode);
use IO::Compress::Zip qw(zip $ZipError);
use Cwd;
use POSIX qw( strftime );
use Image::Size;
use Carp qw/carp/;

# Home grown
use lib 'modules';

use CTSConstants;
use Validate;

use constant DEFAULT_DIGITAL_ASSET_DIR => 'DIGITAL_ASSETS';
use constant DEFAULT_FFMPEG_CMD => '/usr/bin/ffmpeg';
use constant DEFAULT_DIGITAL_UPLOAD_DIR => '/temp/digital_upload';

require "config.pl";
require "general.pl";
require "jobpath.pl";
require "databaseJob.pl";
require "databaseUser.pl";
require 'databaseAudit.pl';
require "digitalassets.pl";
require "streamhelper.pl";

##################################################################
# the template object 
##################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";

sub getDigitalAssetFolder {
	return Config::getConfig("digitalAssetsFolder") || DEFAULT_DIGITAL_ASSET_DIR;
}

sub getUploadDirectory {
    return Config::getConfig("uploaddirectory") || "";
}

sub getDigitalConvertFormats {
	return Config::getConfig("digital_convert")||"";
}

sub getFfmpegCmd {
	return Config::getConfig("ffmpegCmd" ) || DEFAULT_FFMPEG_CMD;
}

########################################################################
# getFileNameAndExt: To get the file prefix and extension. (cloned from jobs.pl)
#
########################################################################
sub getFileNameAndExt {
	my ( $fileName )    = @_;

	my $fileNamePrefix  = undef;
	my $fileExt         = undef;

	if ( $fileName =~ /^(.*)\.(.*)$/ ) {
		$fileNamePrefix = $1;
		$fileExt        = $2;
	}

	return ( $fileNamePrefix, $fileExt );
}

########################################################################
# recursiveBackFileName
#
########################################################################
sub recursiveBackFileName {
    my ( $fileName, $tmpDirName ) = @_;
    my $currVer;

    if ( $fileName =~ /(.*)_bak(\d{1,})\.(.*)/ ) {
        $currVer = $2 + 1;
    }

    $fileName = sprintf ("%s_%s%s.%s", "$1", "bak", "$currVer", "$3");

    if ( -s "$tmpDirName/$fileName" ) {
        $fileName = recursiveBackFileName( $fileName, $tmpDirName );
    }

    return $fileName;
} #recursiveBackFileName

sub spawnCommand {
	my ( $command, $history ) = @_;
	my $t0 = time;
	push( @$history, "Spawning $command" );
	$command =~ s/'/'\\''/g;   # escape single quotes so ' becomes '' (2 single quotes)
	my $result = `$command`;
	my $elapsed = time - $t0;
	push( @$history, "Elapsed time $elapsed" );
	warn "$command $elapsed\n"; # So some record of this in the apache error log
	return $result;
}

##############################################################################
## digitalUpload: Upload selected asset into '/gpfs' and store the asset details in DB.
###############################################################################
sub digitalUpload {
	my ( $query, $uploaderid, $opcoid, $customerid, $tempdir ) = @_;

	my $jobid               = $query->param( "jobid" );
	my $mode 				= $query->param("mode") || ''; 
	my $uploadUserName 		= ECMDBUser::db_getUsername( $uploaderid );
    my $campaflag           = $query->param( 'campflag' );
	my $weburl				='ABCD';
	my $version				= 1;
	my $filename 			= undef;
	my $filesize 			= undef;
	my $filetype 			= undef;
	my $dimension 			= undef;
	my $approved 			= undef;
	my $convert_flag		= undef;
    my $fileOri_Name        = undef;
	my $vars;

	$filename			= $query->param( "uploadfile" );        #filename 
	$filesize			= $query->param("filesize");		#size of file
	$filetype			= $query->param("filetype");		#type of file
	$approved			= $query->param("qcstatus");
	$dimension			= $query->param("dimension");
	$fileOri_Name    = $query->param("uploadfile_Original");      #documen original Name

	my $ffmpegCmd       = getFfmpegCmd();
	my $digital_convert_formats = getDigitalConvertFormats();

	my $approverid			= 0;
	my $renderedPDFDir;
	my @history;

	eval {
		# quit if file size is 0kb
		 die "Digital asset $fileOri_Name is zero length" unless ( $filesize > 0 );

		# TODO suspect this is a global reference AARGH!
		my $current				= "1";

		my $db_maxversion		= undef;
		$filename =~ s/\s+|-/_/g;

		die "No filename" unless $filename;

			# storeFile start
			my ($splitfilename, $filetype) = getFileNameAndExt($filename);
			my $bak_filename = $splitfilename.'_bak1'.'.'.$filetype;

			if ( ! -e "$tempdir" ) {
				mkpath( "$tempdir", { mode => 0777, verbose => 0 } );
				chmod( 0777, "$tempdir" );
			}

			$renderedPDFDir = sprintf ( "%s/%s", "$tempdir","RENDERED_PDF" );

			if ( ! -d "$renderedPDFDir" ) {
				mkpath( "$renderedPDFDir", { mode => 0777, verbose => 0 } );
				chmod( 0777, "$renderedPDFDir" );
			}

			if ( -d "$tempdir" ) {
				if ( -s "$tempdir/$bak_filename" ) {
					$bak_filename = recursiveBackFileName( $bak_filename, $tempdir );
				}
			}

			# upload the file if it was selected
			umask( 0 );

			my $upload_filehandle = $query->upload( "file" );

			open( UPLOADFILE, ">$tempdir/$bak_filename" ) or die "Failed to open '$tempdir/$bak_filename', $!";
			binmode UPLOADFILE;

			while ( <$upload_filehandle> ) {
				print UPLOADFILE;
			}
			close UPLOADFILE;
			chmod ( 0777, "$tempdir/$bak_filename" );

			if($filetype =~ /^zip$/i ){
				my $dir = $tempdir."/$bak_filename";
				my $val = `unzip -l $dir | awk '/-----/ {p = ++p % 2; next} p {print \$NF"@@"}'`;

				my @first_column = split /@@/,$val ;
				my @archive_dir = split /\//,$first_column[0];

				if( $archive_dir[0] eq $splitfilename){
					$vars = {
						fileUploadSucc    => "YES",
					};
				}
				else{
					unlink($tempdir.'/'.$bak_filename);
					die "Upload failed for ZIP file: $archive_dir[0], $splitfilename, $fileOri_Name";

					$vars = {
						fileUploadSucc    => "NO",
					};
				}
			} # Zip files IF end
			else{
				$vars = {
            		fileUploadSucc    => "YES",
        		};
    		}	
			# storeFile end

			#get max version to upload dup_files
			#Check file already availble in DB to update the version

			my $fileVer_ref			= ECMDBJob::db_fileversion($jobid, $filename);
			$db_maxversion = $fileVer_ref->[0]->{'version'};

			if ( $db_maxversion ) {
				$version = $db_maxversion + 1;
				ECMDBJob::updateCurrentValue( $jobid, $filename);
			}

			#save file details in DB table jobasse
			#Check file already availble in DB to update the version

			#save file in corresponding repository
			#split file name add version to avoid duplicate file names in file system
			my $dup_filename = sprintf ( "%s_%s%s.%s", "$splitfilename", "v", "$version", "$filetype" );

			$filetype = $filetype;
			copy ("$tempdir/$bak_filename", "$tempdir/$dup_filename"); 
			copy ("$tempdir/$dup_filename", "$tempdir/$filename"); 
			chmod ( 0777, "$tempdir/$filename" );        
			unlink($tempdir.'/'.$bak_filename);

			if ( $filetype =~ /^pdf$/i ) {
				my $interactivePdfInfo = spawnCommand( "pdfinfo $tempdir/$filename|grep 'Page size' ", \@history );
				chomp( $interactivePdfInfo );
				if ( $interactivePdfInfo =~ /(.*)\s+(.*\d) x (.*\d)/ ) {
					$dimension = sprintf( "%sx%s", "$2", "$3" );
				}
			} elsif ( $filetype =~ /$digital_convert_formats/i ) {
				my $videoInfo = `$ffmpegCmd -i $tempdir/$filename 2>&1 | grep Stream.*Video:`;
				chomp( $videoInfo );	
				$convert_flag   = 0; #conversion required.
=comm
			my $assetFastStart = sprintf ( "%s_%s%s_%s.%s", "$splitfilename", "v", "$version", "fs", "mp4" );
			if ( ( $filetype =~ /^ogv$|^mp4$|^3gp$|^3g2$|^mov$/ ) && ( $vide0Info =~ /h264/i ) ) {
                `$ffmpegCmd -i $tempdir/$filename -codec copy -map 0 -movflags +faststart $tempdir/$assetFastStart; chmod 0777 $tempdir/$assetFastStart;`;
			} elsif ( $filetype =~ /^ogv$|^3gp$|^3g2$|^mov$|^mp4$/ ) {
                `$ffmpegCmd -i $tempdir/$filename -qscale 0 -ab 64k -ar 44100 -movflags +faststart $tempdir/$assetFastStart 2>&1; chmod 0777 $tempdir/$assetFastStart;`;
			}

			my $errorMsg = $?;
			if ( $? ) {
				ECMDBAudit::db_auditLog( $uploaderid, "DIGITAL", $jobid, "ASSET $filename conversion failed uploaded by $uploadUserName error:$errorMsg" );				
			}
=cut
				if ( $videoInfo =~ /.*, (\d.*x\d+).*/g ) {
					$dimension = $1;
				}
			} elsif ( $filetype =~ /^gif$|^jpg$|^jpeg$|^png$/i || $dimension =~ /^undefined/i || $dimension eq "" ) {
				my $assetWidth = 0;
				my $assetHeight = 0;

				( $assetWidth, $assetHeight )  = imgsize( "$tempdir/$filename" );

				$dimension = sprintf( "%sx%s", "$assetWidth", "$assetHeight" );
			} 

			$dimension = "600x450" if ( ! defined $dimension ) || ( $dimension =~ /^(|x)$/i ) || ( $dimension =~ /^undefined/i ); # For setting default dimension

			my $sourceAssetNameVer	= sprintf ("%s/%s", "$tempdir", "$dup_filename" );
			DigitalAssets::storeDigitalAssetsInGPFS( $jobid, $sourceAssetNameVer, $dup_filename, $filename );

			ECMDBJob::loadFile( $jobid, $customerid, $uploaderid, $weburl, $version, $filename, $filetype, $filesize, $approved, $approverid, $current, $dimension,$campaflag, $convert_flag);
			my @digitalFileDet              = ECMDBJob::db_filedetails( $jobid, $filename );

			ECMDBJob::db_recordJobAction($jobid, $uploaderid, CTSConstants::DIGITAL_UPLOAD );
			ECMDBAudit::db_auditLog( $uploaderid, "DIGITAL", $jobid, "ASSET $filename uploaded by $uploadUserName" );
        	if($filename ne $fileOri_Name)
			{
				ECMDBAudit::db_auditLog( $uploaderid, "DIGITAL", $jobid, "ASSET $fileOri_Name has been renamed as $filename by  $uploadUserName" );
			}

			$vars = {
				upLoaded    => "YES",
				digitalFileDet    => @digitalFileDet,
				history => \@history,
			};
	};
    if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => "digitalUpload: ".$error };
		ECMDBAudit::db_auditLog( $uploaderid, "DIGITAL", $jobid, "ASSET $filename upload failed by $uploadUserName" );
	}
	#}
	return $vars;
}

######################################################################################
# digitalShowVersion
#
######################################################################################
sub digitalShowVersion {
        my ( $query, $session ) = @_;

        my $jobid               = $query->param( "jobid" );
		my $filename			= $query->param("filename");
        my $mode                = $query->param( "mode" ) ||'';
        my $opcoid              = $session->param( 'opcoid' );

        my @checkversionhistory;

        @checkversionhistory = ECMDBJob::getVersionHistory( $jobid, $filename );

        my $vars = {
			checkversionhistory	=>\@checkversionhistory,
			opcoid				=>$opcoid,
		};
        if ( $mode eq "json") {
                my $json = encode_json ( $vars );
                print $json;
                return $json;
        }
}

#########################################################################################
## digitalRollBack: Roll back ther version to have based on the user choice.
##########################################################################################
sub digitalRollBack { 
		my ( $query, $session ) = @_; 

		my $jobid                       = $query->param( "jobid" );
		my $filename                    = $query->param("filename");
		my $version						= $query->param("version");
		my $type						= $query->param("type") ||'';
		my $mode                        = $query->param( "mode" ) ||'';
        my $assetID                     = $query->param( "assetID" );
		my $opcoid                      = $session->param( 'opcoid' );
        my $assetName                   = '';
 		my $userName                    = $session->param( 'username' );
        my $userid                      = $session->param( 'userid' );
		my $digitAssetsFolder			= getDigitalAssetFolder();
		my $digital_convert_formats		= getDigitalConvertFormats();
		my $vars;
		my $ftp;

		eval {
			my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobid );

			my $folder;
			my $rollBack;
			# my $assetReopPath;
			my @checkversionhistory;

        $assetName  = ECMDBJob::db_getSingelAssetName($assetID,'Digital');
        $filename   = encode("utf8",$assetName);
				$folder		= $digitAssetsFolder;
				$rollBack	= ECMDBJob::rollBack( $jobid, $assetName, $version, "jobassets", "filename" );
#rajag
				ECMDBAudit::db_auditLog( $userid, "DIGITALASSET", $jobid, "Asset: $filename was reverted to version $version and has been set to 'QC REQUIRED' by $userName" ) if ( $rollBack == 1 );
				@checkversionhistory = ECMDBJob::getVersionHistory( $jobid, $assetName );

			my $assetReopPath = $jobpath . '/' . $folder;

			my ($splitfilename, $filetype) = getFileNameAndExt($filename);
			my $dup_filename = $splitfilename . '_v' . $version . '.' . $filetype;

			my ( $ftpServer, $ftpUserName, $ftpPassword ) = StreamHelper::db_getFTPDetails( $jobopcoid );
			die "Invalid FTP Server for opcoid $jobopcoid" unless ( $ftpServer );

			$ftp = Net::FTP->new( $ftpServer, Debug => 0 ) or die "Cannot connect to FTP server $@";
			if ( $ftp ) {

				$ftp->login( $ftpUserName, $ftpPassword ) or die "Cannot login", $ftp->message;
				$ftp->cwd( "$assetReopPath" );
				$ftp->binary(); 
				$ftp->copy( "$assetReopPath/$dup_filename", "$assetReopPath/$filename" ) or die "Copy fails for from $dup_filename to $filename\n", $ftp->message;
				if ( $type =~ /DIGITAL/i && $filetype =~ /$digital_convert_formats/i ) {
					my $assetVerFastStart 	= sprintf ( "%s_%s%s_%s.%s", "$splitfilename", "v", "$version", "fs", "mp4" );
					my $assetFastStart 	= sprintf ( "%s_%s.%s", "$splitfilename", "fs", "mp4" );
					$ftp->copy( "$assetReopPath/$assetVerFastStart", "$assetReopPath/$assetFastStart" ) or die "Copy fails for from $dup_filename to $filename\n", $ftp->message;
				}
				$ftp->close();
				$ftp = undef;
			}

			$vars = {
				checkversionhistory	=> \@checkversionhistory,
				opcoid				=> $opcoid,
				jobopcoid			=> $jobopcoid,  # Maybe it should be returning this instead TODO
			};
		};
		if ( $@ ) {
			my $error = $@;
			carp "digitalrollback, $error";
			$error =~ s/ at .*//gs;
			$vars = { error => $error };
			$ftp->quit() if ( $ftp );
		}
		if ( $mode eq "json") {
			my $json = encode_json ( $vars );
			print $json;
			return $json;
		}
} #digitalRollBack

#########################################################################################
## erase: Function is used to detete the assets from both '/temp' and 'gpfs'
##########################################################################################

sub erase {
		my ( $query, $session ) = @_;

		my $jobid		= $query->param( "jobid" );
		my $filename	= $query->param("filename");
		my $mode		= $query->param( "mode" ) || '';
		my $userid		= $session->param( 'userid' );
		my $userName	= $session->param( 'username' );
		my $digitAssetsFolder = getDigitalAssetFolder();
		my $vars;
		my $ftp;

		eval {
			my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobid );

			my $fileVer_ref		= ECMDBJob::db_fileversion($jobid, $filename);
			my $maxversion		= $fileVer_ref->[0]->{'version'};;
			my $assetReopPath	= $jobpath . '/' .  $digitAssetsFolder;
			my $assetFileName	= $assetReopPath . '/' . $filename;

			my ( $ftpServer, $ftpUserName, $ftpPassword ) = StreamHelper::db_getFTPDetails( $jobopcoid ) ;
			die "Invalid FTP server for $jobopcoid" unless ( $ftpServer );

			my ( $splitfilename, $filetype ) = getFileNameAndExt( $filename );

			##To delete the assets from remote server using FTP '/gpfs' location.
			$ftp = Net::FTP->new( $ftpServer, Debug => 0 ) or die "Cannot connect to FTP server $ftpServer,  $@";
			die "No FTP object created for server $ftpServer" unless $ftp;

			$ftp->login( $ftpUserName, $ftpPassword ) or die "Cannot login", $ftp->message;
			$ftp->cwd( "$assetReopPath" );
			$ftp->delete( "$assetFileName" ) or die "Asset deletion failed for $jobid, $assetFileName\n", $ftp->message;
		        ##MP2 duplicate mp3 file also delete here
			if( $filetype eq 'mp2' )
                        {
                                  my $assetFileNamemp2 = sprintf ( "%s/%s_fs.%s", "$assetReopPath", "$splitfilename", "mp3" );
                                  $ftp->delete( "$assetFileNamemp2" ) or die "Asset deletion failed for $jobid, $assetFileName\n", $ftp->message;
                        }

			# To delete assets including version files.
			for ( my $i = 1; $i <= $maxversion; $i++ )
			 {
				my $assetFileNameVer = sprintf ( "%s/%s_v%s.%s", "$assetReopPath", "$splitfilename", "$i", "$filetype" );
				$ftp->delete( "$assetFileNameVer" );
		                ##MP2 duplicate mp3 file also delete here
				if ( $filetype eq "mp2" )
                                {
                                       my $assetFileNameVermp2 = sprintf ( "%s/%s_v%s_fs.%s", "$assetReopPath", "$splitfilename", "$i", "mp3" );
                                        $ftp->delete( "$assetFileNameVermp2" );
                                }
			}

			my $staticFlag = ECMDBJob::db_getStaticFlag( $jobid, $filename );
			if ( $staticFlag == 2 ) {
			
				my @digitalAssets = $ftp->ls();
				chomp( @digitalAssets );
				my @statiAssetArray = grep(/($splitfilename)_static\.(.*)$/i, @digitalAssets );
				if (scalar  @statiAssetArray ) {
					$ftp->delete( "$statiAssetArray[0]" ) or die "Asset deletion failed for $jobid, $statiAssetArray[0]\n", $ftp->message;
				}
			}
			$ftp->close();
			$ftp = undef;

			#To delete file information form DB.
			my $erasefromDB	= ECMDBJob::erasefromDB($jobid, $filename);
	
			ECMDBAudit::db_auditLog( $userid, "DIGITALASSET", $jobid, "filename $filename was deleted by user: $userName" );

			$vars = {
				job_num     => $jobid,
			};
		};
		if ( $@ ) {
			my $error = $@;
			carp "erase: $error";
			$error =~ s/ at .*//gs;
			$vars = { error => $error };
			$ftp->quit() if ( $ftp );
			ECMDBAudit::db_auditLog( $userid, "DIGITALASSET", $jobid, "filename $filename was delete failed by user: $userName" );
		}

		if ( $mode eq "json") {
			my $json = encode_json ( $vars );
			print $json;
			return $json;
		}
} #erase

sub digitalDeleteAsset {
	my ( $query, $session ) = @_; 

	my $documentName        = $query->param( "filename" );
	my $deleteFileVersion   = $query->param("deleteFileVersion");
	my $jobid               = $query->param( "jobid" );
	my $mode                = $query->param( "mode" ) || '';
	my $type                = $query->param( "type" ) || '';

	my $userid				= $session->param( 'userid' );
	my $username			= $session->param( 'username' );

	my $vars;
	eval {
		die "documentName not supplied" unless ( $documentName );
		$documentName =~ s/\s+|-/_/g;
		my ( $fileNamePrepix, $fileExt ) = getFileNameAndExt( $documentName );

		die "deleteFileVersion not supplied" unless ( $deleteFileVersion );

		my $deleteAssetName		= sprintf("%s_v%s.%s", "$fileNamePrepix", "$deleteFileVersion", "$fileExt");
		my $assetID;
		my $retVal;
		my @checkversionhistory;

		$assetID	= ECMDBJob::db_getAssetID( $jobid, $documentName, $deleteFileVersion, "jobassets", "filename" );
		$retVal     = ECMDBJob::db_deleteAsset( $jobid, $documentName, $deleteFileVersion, "jobassets", "filename" );

		#Third argument indicates that the assets is version type.
		DigitalAssets::deleteDigitAssetFrmGPFS( $jobid, $deleteAssetName, 1, $type );
		@checkversionhistory = ECMDBJob::getVersionHistory( $jobid, $documentName );

		if ( $retVal =~ /^1$/ && $assetID ne "" ) {
			ECMDBAudit::db_auditLog( $userid, "DIGITALFILE", $jobid, "Asset $documentName ($assetID, version: $deleteFileVersion) was deleted by user: $username " );
		}

		$vars = {
			checkversionhistory     =>\@checkversionhistory,
		};
	};
	if ( $@ ) {
		ECMDBAudit::db_auditLog( $userid, "DIGITALFILE", $jobid, "Asset $documentName (version: $deleteFileVersion) deletion failed by user: $username " );
		my $error = $@;
		carp "digitalDeleteAsset, $error";
		$error =~ s/ at .*//gs;
		$vars = { error => $error };
	}
	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
} # end of digitalDeleteAsset

sub staticFileUploaded {
	my ( $query, $session ) = @_;

	my $jobid				= $query->param( 'jobid' );
	my $mode				= $query->param( 'mode' ) || '';
	my $assetID				= $query->param( 'assetName' );
	my $staticAssetName		= $query->param( 'ori_assetName' );
	my $actionType			= $query->param( 'actionType' );
	my $assetName			= ECMDBJob::db_getSingelAssetName($assetID);
	my $vars;
	my $userName			= $session->param( 'username' );
	my $userid				= $session->param( 'userid' );
	my $temp_path = Config::getConfig( 'digital_upload_temp_dir' ) || DEFAULT_DIGITAL_UPLOAD_DIR;

	eval {
		# get a new uuid
		my $ug      = new Data::UUID;
		my $uuid    = $ug->create();
		my $uuidstr = $ug->to_string( $uuid );	

		my $path = $temp_path . '/' . $uuidstr . '_'.$jobid;

		#remove the file and path if already exist
		if (  -e "$path" ) {
			system("rm -rf $path");
		}

		if ( ! -e "$path" ) {
			mkpath( "$path", { mode => 0777, verbose => 0 } );
			chmod( 0777, "$path" );
		}

		my $uploadLoc = sprintf ("%s/%s_%s", "$path", "$jobid", "assets");

		my ( $filePrefix, $fileExt ) =	getFileNameAndExt($assetName);
		my ( $staticFilePrefix, $staticFileExt ) =	getFileNameAndExt($staticAssetName);

		my $staticBackAssetName 	= encode('utf8', sprintf( "%s_%s_%s.%s", "$filePrefix", "static", "bak", "$staticFileExt" ));
		my $uploadStaticAssetName	= encode('utf8', sprintf( "%s_%s.%s", "$filePrefix", "static", "$staticFileExt" ));

		if ( $actionType =~ /^UPLOAD$/i ) {
			# copy to temp - start
			umask( 0 );
			my $upload_filehandle = $query->upload( "staticAssetName" );
			open( UPLOADFILE, ">$path/$staticBackAssetName" ) or die "Failed to open '$path/$staticBackAssetName', $!";
			binmode UPLOADFILE;

			while ( <$upload_filehandle> ) {
				print UPLOADFILE;
			}

			close UPLOADFILE;
			chmod ( 0777, "$path/$staticBackAssetName" );
			# copy to temp - end

			DigitalAssets::storeDigitalAssetsInGPFS( $jobid, "$uploadLoc/$staticBackAssetName", $uploadStaticAssetName );
			ECMDBJob::db_updateStaticUpload( $jobid, $assetName, "2" );  #3rd Argument two(2) indicates the Static file uploaded successfully.

#	move( "$staticBackAssetName", "$uploadLoc/$uploadStaticAssetName" ) or die "Failed to move $staticBackAssetName, to $uploadLoc/$uploadStaticAssetName";

			# delete temp dir - start
			# Delete any *_bak* files in temp directory
			my $dir;
			opendir( $dir, $uploadLoc );  # TODO or die ?
			my @files = grep(/\_bak/,readdir($dir));
			closedir($dir);

			foreach my $file (@files) {
				unlink( $uploadLoc . '/' . $file );
			}

			if ( ( defined $uploadLoc ) && ( $uploadLoc ne '' )  && ( -e "$uploadLoc" ) ) {
				system("rm -rf $uploadLoc");
			}
			# delete temp dir - end

			ECMDBAudit::db_auditLog( $userid, "DIGITAL", $jobid, "Static image uploaded for ASSET $assetName uploaded by $userName" );

			$vars = {
				staticAssetUpload => "YES",
			};
		} elsif ( $actionType =~ /^DELETE$/i ) {
			my $filename = "$uploadLoc/$staticBackAssetName";

			if ( -e $filename ) {
				unlink( $filename ) or die "Failed to delete $filename";
			}

			$vars = {
				staticAssetDelete => "YES",
			};

			ECMDBAudit::db_auditLog( $userid, "DIGITAL", $jobid, "Static image deleted for ASSET $assetName uploaded by $userName" );
		}

		# remove the temp path
		if ( ( defined $path ) && ( $path ne '' )  && ( -e "$path" ) ) {
			system("rm -rf $path");
		}
	};
	if ( $@ ) {
		my $error = $@;
		carp "staticFileUploaded, $error";
		$error =~ s/ at .*//gs;
		$vars = { error => $error };
		ECMDBAudit::db_auditLog( $userid, "DIGITAL", $jobid, "Static image upload failed for ASSET $assetName uploaded by $userName" );
	}

	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

1;
