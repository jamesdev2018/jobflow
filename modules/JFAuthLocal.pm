#!/usr/local/bin/perl

package JFAuthLocal;

use strict;
use warnings;

use Crypt::CBC;
use Carp qw/carp/;
use Data::Dumper;

# Home grown

use lib 'modules';

use parent 'JFAuthBase';

require "databaseUser.pl";

use constant TEMP_USER_ID => 2;
use constant TEMP_USER_ROLEID => 8; # Visitor
use constant TEMP_USER_ROLENAME => 'Visitor';


#my $self = { auth_id => 0, auth_name => '', debug => 0 };

#sub new {
#	my $class = shift;
#	my ($auth_id, $auth_name) = @_;
#
#	$self = bless { auth_id => $auth_id, auth_name => $auth_name, debug => 0 }, $class;
#	return $self;
#}

# Authenticate user, given credentials (principle + password)
# details = authenticate( username, password );
#
# return hash with details (or undef if not authenticated)
# - userid	userid (query what happens for temp users, userid = 2, tempuser set to userstemp.id)
#		for Active Directory this can be undef, meaning the credentials are not associated with a Jobflow userid
# - username	username (can be present even if userid is not)
# - fname	first name
# - lname	last name
# - email	email address
# - rolename	role name (depends on userid)
# - roleid	role id   (depends on userid)
# - tempuser	0 (if userid from users table) or > 0 if id from userstemp 
# - enabled	0 or 1, (value if users.status)
#
# Returns undef if authentication failed.
#
sub authenticate {
	my $class = shift;
	my ( $username, $password ) = @_;

	my $vars;	# Hash to return if successful (undef if not logged in)

	die "Password is undef" if ! defined $password;

	print "Got username $username, password $password\n" if ( $class->{debug} );

	# Lookup encrypted password in Users table
	my $cryptpassword  = crypt( $password, "ab");

	# try and authenticate from regular table - if no user found *then* authenticate from USERSTEMP
	my ( $userid, $status ) = ECMDBUser::db_Login( $username, $cryptpassword );
	if ( $userid > 0 ) {

		print "Got userid $userid from users, status $status\n" if ( $class->{debug} );

		my @user = ECMDBUser::getUser( $userid );
		die "Expected 1 row from ECMDBUser::getUser( $userid )" unless ( ( scalar @user ) == 1 );
		my $h = $user[0]; # hash { id, status, opcoid, companyid, fname, lname, username, email, employeeid, role, rolename, rec_timestamp }

		$vars = {
			userid		=> $userid,
			username	=> $username,
			fname		=> $h->{fname},
			lname		=> $h->{lname},
			email		=> $h->{email},
			rolename	=> $h->{rolename},
			roleid		=> $h->{role},
			tempuser	=> 0,
			enabled		=> $status,
		};

		print "User first name " . $h->{fname} . "\n" if ( $class->{debug} );

	} else {

		print "Looking at tempusers, username $username, crypted $cryptpassword\n" if ( $class->{debug} );

		my $email;
		# lets see if maybe its a temp user trying to login ..
		( $userid, $email ) = ECMDBUser::db_LoginTemp( $username, $cryptpassword );

		print "ECMDBUser::db_LoginTemp returned $userid, $email\n" if ( $class->{debug} );

		if ( $userid > 0 ) { 
			$vars = {
				username	=> $username,
				userid		=> TEMP_USER_ID,
				fname		=> "Temp",
				lname		=> "User",
				email		=> $email,
				rolename	=> TEMP_USER_ROLENAME,
				roleid		=> TEMP_USER_ROLEID,
				tempuser	=> $userid,
				enabled		=> 1,
			};
		} else {
			# Log authentication failure
			$class->auth_failure( $class->{auth_id}, $username, "No match" );
		}
	}	
	return $vars;
}

1;

