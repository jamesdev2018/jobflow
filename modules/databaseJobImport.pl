#!/usr/local/bin/perl
package ECMDBJobImport;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use Carp qw/carp cluck/;
use Time::HiRes qw/time/;
use Data::Dumper;
use JSON;

# Home grown
use lib 'modules';

use Validate;

require "config.pl";			# getConfig
# eg  my $rowscount		= Config::getConfig("jobrowslimit") || 1000;




#######################################
##  Dealing with the dynamic fields  ##
#######################################

sub add_import_field {
	my ($title, $description, $fieldname, $type, $ismandatory, $mediatypeid, $customerid, $parentid) = @_;
	
	my $id = db_insert("job_import_fields", { title				=> $title,
											description			=> $description,
											fieldname			=> $fieldname,
											fieldtype			=> $type,
											is_mandatory			=> $ismandatory,
											limiter_mediatypeid	=> $mediatypeid,
											limiter_customerid	=> $customerid,
											parentid				=> $parentid,
											is_mappable			=> 1,
											}
						);
	return $id;
}


sub update_import_field {
	my ($id, $field_info) = @_;
	# Valid keys for field_info: title, description, fieldname, fieldtype,
	#							is_mandatory, mediatypeid, customerid, parentid
	
	db_update("job_import_fields", $id, $field_info);
}


sub remove_import_field {
	my ($id) = @_;
	
	db_remove("job_import_fields", $id);
}


## Get field information by id
sub get_import_field {
	my ($id) = @_;
	
	my @rows = db_list("job_import_fields",
								[ 'id', 'title', 'description', 'fieldtype', 'is_mandatory', 'is_mappable' ],
								[
									{ id => $id }
								],
								'index_hint',
						);
	return $rows[0];
}


## Get field information from a search on mediatypeid, customerid and/or title
sub get_import_fields {
	my ($mediatypeid, $customerid, $title) = @_;
	
	my @rows = db_list("job_import_fields",	
								[ 'id', 'field_key', 'title', 'description', 'fieldtype', 'is_mandatory', 'is_mappable' ],
								[	
									{ limiter_mediatypeid => $mediatypeid, title => $title },
									{ limiter_mediatypeid => 'NULL', limiter_customerid => 'NULL', title => $title },
									{ limiter_customerid => $customerid, title => $title },
								],
								'index_hint',
						);
	return @rows;
}


## Get field information for children with a parent of given id
sub get_import_field_children {
	my ($parentid) = @_;
	
	my @rows = db_list("job_import_fields", [ 'id', 'title', 'description', 'fieldtype', 'is_mandatory', 'is_mappable' ],
										[ { parentid => $parentid } ]);
	return @rows;
}









################################################
##  Dealing with the auto-substitution lists  ##
################################################

sub add_substitution {
	my ($column, $text, $replacement, $customerid, $clientid, $importid) = @_;
	
	my $id = db_insert("job_import_substitutions", {	fieldid			=> $column,
													text				=> $text,
													replacement		=> $replacement,
													limit_customerid	=> $customerid,
													limit_clientid	=> $clientid,
													limit_importid	=> $importid ,
												}
						);
	return $id;
}


sub update_substitution {
	my ($id, $substitution_info) = @_;
	# Valid keys for substitution_info: column, text, replacement, customerid,
	#									clientid, importid }
	
	db_update("job_import_substitutions", $id, $substitution_info );
}


sub remove_substitution {
	my ($id) = @_;
	
	db_remove("job_import_substitutions", $id);
}


sub get_substitution {
	my ($fieldid, $text) = @_;
	
	my @substitutions = db_list('job_import_substitutions', [ 'replacement' ], [ { fieldid => $fieldid, text => $text } ], 'id desc');
	
	return @substitutions ? $substitutions[0]->{replacement} : $text;
}


sub get_substitutions {
	my ($customerid, $clientid, $importid) = @_;
	
	my @substitutions = db_list('job_import_substitutions', [ 'id', 'fieldid', 'text', 'replacement' ], [ { customerid => $customerid, clientid => $clientid } ], 'id desc');
	
	return @substitutions;
}

sub apply_substitutions {
	my ($ruleid, $customerid, $clientid) = @_;
	
	
	$Config::dba->process_sql("UPDATE job_import_records jir
							INNER JOIN job_import_record_data jird ON jir.id=jird.recordid
							INNER JOIN job_import_substitutions jis ON jird.fieldid=jis.fieldid AND jird.content=jis.text
							SET jird.content=jis.replacement
							WHERE jir.customerid=? AND jir.clientid=? AND jis.id=? AND jird.content=jis.text;",
							[ $customerid, $clientid, $ruleid ]);
}




## TODO:: Prevent rulesets from duplicating, since we don't check existing ones here
sub add_ruleset {
	my ($customerid, $clientid, $importid, $requirements, $replacements) = @_;
	
	my $reqs = '';
	foreach my $requirement (@$requirements) {
		$reqs .= $requirement->{fieldid} . '=' . $requirement->{value} . "\n";
	}
	
	my $reps = '';
	foreach my $replacement (@$replacements) {
		$reps .= $replacement->{fieldid} . '=' . $replacement->{value} . "\n";
	}
	
	my $id = db_insert('job_import_rulesets', {	customerid => $customerid,
												clientid => $clientid,
												requirements => $reqs,
												replacements => $reps,
											});
	
	return $id;
}


sub remove_ruleset {
	my ($id) = @_;
	
	db_remove('job_import_rulesets', $id);
}

sub get_rulesets {
	my ($customerid, $clientid, $importid) = @_;
	
	my @rules = db_list('job_import_rulesets', [ 'id', 'requirements', 'replacements' ],
								[ { customerid => 'NULL', clientid => 'NULL' },
								  { customerid => $customerid, clientid => 'NULL', },
								  { customerid => $customerid, clientid => $clientid } ],
								'id desc');
	
	my @rulesets;
	foreach my $rule (@rules) {
		my @requirements;
		foreach my $reqs (split(/\n/, $rule->{requirements})) {
			my ($fieldid, $value) = $reqs =~ m/^([0-9]+)=(.*)$/;
			push(@requirements, { fieldid => $fieldid, value => $value || '' });
		}
		
		my @replacements;
		foreach my $reps (split(/\n/, $rule->{replacements})) {
			my ($fieldid, $val) = $reps =~ m/^([0-9]+)=(.*)$/;
			$val = ";$val" if $val !~ m/;/;
			my ($id, $value) = $val =~ m/^([^;]*);(.*)$/;
			push(@replacements, { fieldid => $fieldid, id => $id || '', value => $value || '' });
		}
		
		push(@rulesets, { id => $rule->{id}, requirements => \@requirements, replacements => \@replacements });
	}
	
	return @rulesets;
}








########################################
##  Dealing with spreadsheet imports  ##
########################################

## Translate a customer name and/or mediatype name into their respective ids
sub resolve_ids {
	my ($customer, $mediatype) = @_;
	
	my ($customerid) = db_list('customers', [ 'id '], [ { customer => $customer } ]);
	my ($mediatypeid) = db_list('mediatypes', [ 'id' ], [ { type => $mediatype } ]);
	
	return ($customerid->{id}, $mediatypeid->{id});
}


sub get_mapping_types {
	my @maps = db_list("job_import_mapping_types",
						[ 'id', 'name', 'mappinginfo' ]);
	
	return @maps;
}


## Create a header to link spreadsheet records (rows) under
sub create_spreadsheet_import {
	my ($filename, $sheetname, $parentid, $importerid, $column_pairings) = @_;
	
	
	# Auto-increment version number
		my @existing = db_list("job_import_spreadsheets", [ 'id', 'version' ], [ { filename => $filename, sheetname => $sheetname } ]);
		@existing = sort { $b->{id} <=> $a->{id} } @existing;
		my $version = $existing[0]->{version} || '1';
	
	# Stringify column pairings (field, type, columns (column, rowindex))
		my $col_pairs = '';
		foreach my $pair (@$column_pairings) {
			#field=type[rowindex.column,rowindex.column,rowindex.column]
			$col_pairs .= $pair->{field} . '=' . $pair->{type} . '[' .
							join(', ', map { $_->{rowindex} . '.' . $_->{column} } @{ $pair->{columns} }) .
						  "]\n";
		}
	
	# Create the spreadsheet entry
		my $id = db_insert("job_import_spreadsheets", {	version			=> $version,
														filename			=> $filename,
														sheetname		=> $sheetname,
														parentid			=> $parentid,
														importerid		=> $importerid,
														column_pairings	=> $col_pairs,
													}
							);
	return $id;
}


## Create a spreadsheet record (row)
sub add_spreadsheet_record {
	my ($importid, $search_ids, $data) = @_;
	# Valid & mandatory keys for search_ids: customerid, clientid, campaignid, countryid, mediatypeid
	# Valid keys for data: fieldid (each entry, hash keys are: content, modified_content )
	
	# Create the initial record
		my $id = db_insert("job_import_records", {	import_parent	=> $importid,
													opcoid			=> $search_ids->{opcoid},
													customerid		=> $search_ids->{customerid},
													clientid			=> $search_ids->{clientid},
													campaignid		=> $search_ids->{campaignid},
													countryid		=> $search_ids->{countryid},
													mediatypeid		=> $search_ids->{mediatypeid},
												}
							);
	
	
	# Create entries for the data values for this record
		foreach my $fieldid (keys %$data) {
			next if !defined $data->{$fieldid}->{content};		# Don't bother adding an empty field
			
			db_insert("job_import_record_data", {	recordid			=> $id,
												fieldid			=> $fieldid,
												content			=> $data->{$fieldid}->{modified_content} || $data->{$fieldid}->{content},
												original_content	=> $data->{$fieldid}->{content},
												content_id		=> $data->{$fieldid}->{contentid},
												comment			=> $data->{$fieldid}->{comment},
												}
						);
		}
	
	return $id;
}


sub update_spreadsheet_record {
	my ($recordid, $data) = @_;
	
	# Get a list of the dynamic field entries to update
	my @fields = db_list('job_import_record_data', [ 'id', 'fieldid' ], [{ recordid => $recordid }]);
	
	# Update the fields
	foreach my $field (@fields) {
		if($data->{$field->{fieldid}}) {
			db_update('job_import_record_data', $field->{id}, {	content		=> $data->{$field->{fieldid}}->{text},
																content_id	=> $data->{$field->{fieldid}}->{id} || '',
																comment		=> $data->{$field->{fieldid}}->{comment} || '',
															}
					);
		}
		$data->{$field->{fieldid}} = undef;
	}
	

	# Add any new fields
	foreach my $fieldid (keys %$data) {
		next if !$data->{$fieldid};
		
		db_insert('job_import_record_data', {		recordid => $recordid,
											    fieldid	 => $fieldid,
											    content	 => $data->{$fieldid}->{text},
											    original_content => $data->{$fieldid}->{text},
											    content_id	=> $data->{$fieldid}->{id} || '',
											    comment	=> $data->{$fieldid}->{comment} || '',
										}
						);
	}
}


sub get_spreadsheet_record {
	my ($id) = @_;
	
	my @records = db_list("job_import_records", [ 'id', 'status' ], [ { id => $id } ]);
	my $record = $records[0];
	
	my @fields = db_list([ "job_import_records jir",
							"job_import_record_data jird", "jird.recordid=jir.id",
							"job_import_fields jif", "jif.id=jird.fieldid",
						],
						[ 'jif.id', 'jif.title', 'jir.import_parent', 'jird.content',
							'jird.original_content', 'jird.content_id', 'jird.comment', ],
						[ { 'jir.id' => $id } ]);
						
	foreach my $field (@fields) {
		$record->{$field->{id}} = $field;
	}
	
	return $record;
}


## Remove a single record (along with its dependent data)
sub remove_spreadsheet_record {
	my ($id) = @_;
	
	my @med = db_list([ 'job_import_records' ], [ 'mediachoiceid' ], [ { id => $id } ]);
	return 0 if scalar(@med) && $med[0]->{mediachoiceid};
	
	db_remove('job_import_record_data', { recordid => $id });
	db_remove('job_import_records', $id);
	return 1;
}


## Remove all records (inc. dependent data & parent spreadsheets) according to opcoid/mediatypeid/customerid/etc.
sub remove_spreadsheet_records {
	my ($options) = @_;
	
	my @records = db_list('job_import_records', [ 'id', 'import_parent' ], [ $options ]);
	
	## Get rid of the record data
		foreach my $record (@records) { remove_spreadsheet_record($record->{id}); }
	
	## Get rid of the parent spreadsheets
		my @uniq;
		foreach my $parent (map { $_->{import_parent} } @records) {
			my $found = 0;
			foreach my $exists (@uniq) { if($exists eq $parent) { $found = 1; last; } }
			push(@uniq, $parent) if !$found;
		}
	
		foreach my $spreadsheet (@uniq) { db_remove('job_import_spreadsheets', $spreadsheet); }
}

sub get_spreadsheet_mapping {
	my ($filename) = @_;
	
	my ($mapping) = db_list('job_import_spreadsheets', [ 'column_pairings' ], [ { filename => $filename } ], 'rec_timestamp desc');
	
	my @map;
	foreach my $pairings (split(/\n/, $mapping->{column_pairings} || '')) {
		my ($fieldid, $type, $columns) = $pairings =~ m/^([0-9]+)=([0-9]+)\[(.*)\]$/;
		my @cols;
		foreach my $col (split(/,/, $columns)) {
			my ($test, $rowindex, $column) = $col =~ m/^(([0-9]+)\.)?([0-9]+)$/;
			push(@cols, { column => $column || $col, rowindex => $rowindex || 0 });
		}
		
		push(@map, { field => $fieldid, type => $type, columns => \@cols, origin => $pairings });
	}
	
	return @map;
}

sub update_spreadsheet_mapping {
	die "Not implemented yet";
}


## options => { customer, client, campaign, country, mediatype }
sub get_records {
	my ($options) = @_;
	
	## Get a list of the matching records
	my @records = db_list("job_import_records", [ 'id', 'status', 'mediachoiceid', 'jobid', 'jobnumber', 'import_parent' ], [ $options ]);
	
	## Set-up some manual fields
	my $fieldlist = { mediachoiceid =>	{ title => 'Mediachoice ID', index_hint => -1, is_mappable => 0, is_visible => 1, id => -1 },
					 jobnumber =>		{ title => 'Job Number', index_hint => -2, is_mappable => 0, is_visible => 1, id => -2 },
					};
	
	## Retrieve all of the dynamic fields
	foreach my $record (@records) {
		$record->{mediachoiceid} ||= '';
		$record->{jobid} ||= '';
		$record->{jobnumber} ||= '';
		
		my $mcid = $record->{mediachoiceid} . '/' . $record->{jobid}; if($mcid eq '/') { $mcid = ''; }
		$record->{'Mediachoice ID'}	= { content => $mcid, original_content => $mcid };
		$record->{'Job Number'}		= { content => $record->{jobnumber}, original_content => $record->{jobnumber} };
		
		my @fields = db_list([ "job_import_records jir",
								"job_import_record_data jird", "jird.recordid=jir.id",
								"job_import_fields jif", "jif.id=jird.fieldid",
							],
							[ 'jif.id', 'jif.title', 'jird.content', 'jird.original_content', 'jird.content_id',
							  'jird.comment', 'jif.index_hint', 'jif.is_mappable', 'jif.is_visible',
							  'exists (SELECT 1 FROM job_import_validations jiv WHERE jiv.fieldid=jif.id) as requires_validation' ],
							[ { 'jird.recordid' => $record->{id}, 'jif.is_visible' => 'auto' },
							  { 'jird.recordid' => $record->{id}, 'jif.is_visible' => 'yes'  },
							]);
		foreach my $field (@fields) {
			$record->{$field->{title}} = $field;
		
			## Record the fields we're using
			$fieldlist->{$field->{title}} = { id => $field->{id},
											 title => $field->{title},
											 index_hint => $field->{index_hint},
											 is_mappable => $field->{is_mappable},
											 is_visible => $field->{is_visible} } if !$fieldlist->{$field->{title}};
		}
	}
	
	## Add fields that have to be shown
	my @extra_fields = db_list([ "job_import_fields jif" ],
							  [ 'id', 'title', 'index_hint', 'is_mappable', 'is_visible' ],
							  [ { 'is_visible' => 'yes' } ]
							 );
	foreach my $field (@extra_fields) { $fieldlist->{$field->{title}} = $field if !defined $fieldlist->{$field->{title}}; }
	
	my @f_list;
	foreach my $field (sort { $fieldlist->{$a}->{index_hint} <=> $fieldlist->{$b}->{index_hint} } keys %$fieldlist) {
		push(@f_list, $fieldlist->{$field});
	}
	
	return (\@records, \@f_list);
}

sub get_spreadsheets_by_user {
	my ($userid) = @_;
	
	my @records = db_list([ 'job_import_spreadsheets jis',
							'job_import_records jir', 'jir.import_parent=jis.id',
						 ],
						 [ 'jis.id', 'jis.filename', 'jis.version', 'jir.opcoid', 'jir.customerid',
						   'jir.clientid', 'jir.campaignid', 'jir.countryid', 'jir.mediatypeid',
						   'jis.rec_timestamp'
						  ],
						  [ { 'jis.importerid' => $userid } ],
						  'jis.rec_timestamp DESC',
						  'jis.id'
						 );
						 
	return @records;
}







################################
##  Dealing with validations  ##
################################
sub get_validations_from_field {
	my ($fieldid) = @_;
	
	my @validations = db_list([ 'job_import_validations jiv',
						  ],
						  [ 'jiv.dependencies', 'jiv.data_source', 'jiv.api_uri', 'jiv.table_name',
						  	'jiv.source_id_field', 'jiv.source_data_field', 'jiv.listid',
						  	'jiv.cache_age_limit',
						  ],
						  [ { 'jiv.fieldid' => $fieldid, enabled => 1 } ]
						 );
						 
						 
	return @validations;
}


## Order validation lists by field so we hopefully don't use anything that expects a field to have its ID before it has its own
sub order_validations {
	my @field_ids = @_;
	
	## Get a list of the field dependencies for each validation
		my $deplist;
		foreach my $fieldid (@field_ids) {
			my @deps = db_list([ 'job_import_validations jiv' ],
											[ 'jiv.dependencies' ],
											[ { 'jiv.fieldid' => $fieldid } ]);
			
			## Combine all the dependency lists for this field
			my @temp;
			foreach my $dep (@deps) { push(@temp, $dep->{dependencies} =~ m/\$([0-9]+)=/g); }
			$deplist->{$fieldid} = \@temp;
		}
	
	my $safety = 3;
	my @new_order;
	while(keys %$deplist && $safety--) {
		foreach my $fieldid (keys %$deplist) {
			## Have we got all the dependencies?
			my $deps_met = 1;
			foreach my $dep (@{ $deplist->{$fieldid} }) {
				my $found = 0;
				foreach my $exist_dep (@new_order) { if($dep eq $exist_dep) { $found = 1; last; } }
				$deps_met = 0 if $found == 0;
				last if !$found;
			}
			
			## If the dependencies are met, add it to the queue
			if($deps_met) {
				push(@new_order, $fieldid);
				delete $deplist->{$fieldid};
				$safety = 3;
			}
		}
	}
	
	## List of field IDs that hopefully don't depend on other fields to have IDs before they've been processed
	return (@new_order, keys %$deplist);
}


sub get_validations_from_spreadsheet_record {
	my ($recordid, $fieldid) = @_;
	
	my @validations = db_list([ 'job_import_records jir',
							'job_import_record_data jird', 'jird.recordid=jir.id',
							#'job_import_fields jif', 'jif.id=jird.fieldid',
							'job_import_validations jiv', 'jiv.fieldid=jird.fieldid',
						  ],
						  [ 'jird.id AS dataid', 'jird.content', #'jif.id fieldid', 'jif.title',
						  	'jiv.dependencies', 'jiv.data_source', 'jiv.api_uri', 'jiv.table_name',
						  	'jiv.source_id_field', 'jiv.source_data_field', 'jiv.listid'
						  ],
						  [ { 'jir.id' => $recordid, 'jird.fieldid' => $fieldid } ]
						 );
						 
						 
	return @validations;
}




sub build_vlist_from_list {
	my ($listid) = @_;
	
	my @data = db_list('job_import_validation_lists', [ "value as id", "text", "isdefault" ], [ { listid => $listid } ]);
	
	return @data;
}

sub build_vlist_from_table {
	my ($table, $id_field, $data_field) = @_;
	
	my @data = db_list($table, [ "$id_field AS id", "$data_field AS text" ]);
	
	return @data;
}





#### Column ordering/layout details. Loading support for custom values (width, visibility, etc.)
## columnid(data:value,data:value)/columnid/columnid(data:value) etc.
sub get_column_layout {
	my ($userid, $search_limits) = @_;
	
	my @column_data = db_list('job_import_column_layouts jicl',
								[ 'layout', ],
								[ $search_limits ]
							);
	my $layout_data = $column_data[0]->{layout} || '';
	
	my @columns;
	foreach my $column (split(/\//, $layout_data)) {
		my ($id, $waste, $options) = $column =~ m/^(-?[0-9]*)(\((.*)\))?$/;
		my $col_data = { id => $id };
		
		foreach my $option (split(/,/, $options || '') ) {
			my ($name, $value) = split(/:/, $option);
			$col_data->{$name} = $value;
		}
		
		push(@columns, $col_data);
	}
	
	return @columns;
}

sub save_column_layout {
	my ($userid, $search_limits, $layout_data) = @_;
	
	$search_limits->{userid} = $userid;
	my @exists = db_list('job_import_column_layouts licl', [ 'id', 'layout' ], [ $search_limits ]);
	
	if(scalar @exists) {
		db_update('job_import_column_layouts', $exists[0]->{id}, { layout => $layout_data });
	} else {
		$search_limits->{layout} = $layout_data;
		my $id = db_insert('job_import_column_layouts', $search_limits);
		die "Inserted; got value $id";
	}
}







####################################################
####################################################
##                                                ##
## !! You shouldn't need to change any of this !! ##
##                                                ##
####################################################
####################################################


########
## DB-access macros to make the boilerplate for db manipulation less tedious

## Separate a hash into two arrays (keys & values), but keeping the order synchronised
sub separate_list {
	my ($fieldlist) = @_;
	my (@keys, @values);
	
	foreach my $field (keys %$fieldlist)
	{
		next if !defined $fieldlist->{$field};
		push(@keys, $field);
		push(@values, $fieldlist->{$field});
	}
	
	return (\@keys, \@values);
}

## Create a row with a given hash (describing field=>value relations)
sub db_insert {
	my ($table, $info) = @_;
	
	my ($keys, $values) = separate_list($info);
	return if !@$keys;
	my $sql = "INSERT INTO $table (" . join(', ', @$keys) . ') VALUES (' . ('?, ' x (scalar(@$keys) - 1)) . '?)';
	
	$Config::dba->process_sql($sql, $values);
	my ($id) = $Config::dba->process_oneline_sql("SELECT LAST_INSERT_ID()");
	return $id;
}

## Return an array from a database query
## :: $tables  >> Either: 1) A table name
##                        2) Reference to an array consisting of a table name followed by a list of
##                           tables to inner join of the form (tablename, 'ON' query)
## :: $columns >> Reference to an array of column names to use
## :: $limits  >> Array reference of hashes of WHERE restrictions, in the form
##						[ { column=>value AND column=>value } OR { column=>value AND column=>value } ]
sub db_list {
	my ($tables, $columns, $limits, $sort, $group) = @_;
	
	## Get the tables
		my $tablestring = "";
		if(ref $tables eq 'ARRAY') {
			## If $tables is an array reference, form the table/INNER JOIN part of the query
			$tablestring = "FROM " . shift(@$tables) . ' ';
			while(@$tables) {
				$tablestring .= 'INNER JOIN ' . shift(@$tables) . ' ON ' . shift(@$tables) . ' ';
			}
		} else {
			## Otherwise, it's just a table name
			$tablestring = "FROM $tables ";
		}
	
	## Build the WHERE clause
		my $where_string = "";
		my @value_list;
		foreach my $limit (@{ $limits || [] }) {
			my ($keys, $values) = separate_list($limit);
			
			my @where_list;
			for(my $i = 0; $i < scalar(@$keys); $i++) {
				if(@{ $values }[$i] eq 'NULL') {
					push(@where_list, @{ $keys }[$i] . " IS NULL");
				} elsif(defined @{ $values }[$i]) {
					push(@where_list, @{ $keys }[$i] . "=?");
					push(@value_list, @{ $values }[$i]);
				}
			}
			$where_string .= ($where_string ? ' OR ' : '') . '(' . join(' AND ', @where_list) . ')' if @where_list;
		}
	
	## Execute the SQL
		my $sql = "SELECT " . join(', ', @$columns) . " $tablestring" .
					($where_string ? " WHERE $where_string" : '') .
					($group ? " GROUP BY $group" : '').
					($sort ? " ORDER BY $sort" : '');
		my $rows = $Config::dba->hashed_process_sql($sql, \@value_list);
		
	return $rows ? @$rows : ();
}

sub db_update {
	my ($tables, $id, $info) = @_;
	
	## Get the tables
		my $tablestring = "";
		if(ref $tables eq 'ARRAY') {
			## If $tables is an array reference, form the table/INNER JOIN part of the query
			$tablestring = shift(@$tables) . ' ';
			while(@$tables) {
				$tablestring .= 'INNER JOIN ' . shift(@$tables) . ' ON ' . shift(@$tables) . ' ';
			}
		} else {
			## Otherwise, it's just a table name
			$tablestring = "$tables ";
		}
	
	my ($keys, $values) = separate_list($info);
	return if !@$keys;
	
	my $idstring = "id=?";
	$id = { id => $id } if(ref $id ne 'HASH');
	my ($idkeys, $idvalues) = separate_list($id);
	$idstring = join('=?', $idkeys) . '=?';
	
	my $sql = "UPDATE $tablestring SET " . join('=?, ', @$keys) . '=? WHERE ' . join('=? AND ', @$idkeys) . '=?';
	$Config::dba->process_sql($sql, [ @$values, @$idvalues ]);
}

sub db_remove {
	my ($table, $id) = @_;
	
	my $sql;
	if(ref $id eq 'HASH') {
		my ($field) = keys %$id;
		$id = $id->{$field};
		
		$sql = "DELETE FROM $table WHERE $field=?";
	} else {
		$sql = "DELETE FROM $table WHERE id=?";
	}
	
	$Config::dba->process_sql($sql, [ $id ]);
}
































sub get_job_import_fields {
	my @rows = db_list("job_import_fields", [	'id', 'limiter_mediatypeid', 'limiter_customerid', 
												'title', 'field_key', 'description', 'meta_key', 'fieldtype', 'fieldname', 
												'is_mandatory', 'index_hint', 'is_visible', 'is_mappable' ],
												[],
												'field_key, title'
						);
	return @rows;
}

sub get_job_import_field {
	my ($id) = @_;

	my @rows = db_list("job_import_fields", [   'id', 'limiter_mediatypeid', 'limiter_customerid',
												'title', 'description', 'meta_key', 'fieldtype', 'fieldname',
												'is_mandatory', 'index_hint', 'is_visible', 'is_mappable' ], [ { id => $id }]
						);
	return $rows[0];
}

sub db_save_job_import_field {
	my ($title, $desc, $metakey, $fieldtype, $fieldname, $indexhint, $ismandatory, $ismappable, $isvisible, $mediatypeid, $customerid) = @_;

	$Config::dba->process_sql("INSERT INTO job_import_fields
											(title, description, meta_key, fieldtype,
											fieldname, index_hint, is_mandatory, is_mappable, is_visible)
											VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
							[ $title, $desc, $metakey, $fieldtype, $fieldname, $indexhint, $ismandatory, $ismappable, $isvisible ]);
	
	my ($id) = $Config::dba->process_oneline_sql("SELECT LAST_INSERT_ID()");
	
	return $id;
}

sub db_update_job_import_field {
	my ($id, $title, $desc, $metakey, $fieldtype, $fieldname, $indexhint, $ismandatory, $ismappable, $isvisible, $mediatypeid, $customerid) = @_;
	
	$Config::dba->process_sql("UPDATE job_import_fields SET title=?, description=?, meta_key=?,
									fieldtype=?, fieldname=?, index_hint=?, is_mandatory=?, is_mappable=?, is_visible=?
								WHERE id=?", [ $title, $desc, $metakey, $fieldtype, $fieldname, $indexhint, $ismandatory, $ismappable, $isvisible, $id ]);
	return $id;
}

sub get_job_import_substitutions
{
	my $rows;

	eval {
		$rows = $Config::dba->hashed_process_sql("SELECT jis.id as id, jif.title, jis.fieldid as fieldid, jis.text as text, jis.replacement as replacement ".
												" FROM job_import_substitutions jis ".
												" INNER JOIN job_import_fields jif on jif.id = jis.fieldid ");

		die "No data returned by Job import substitutions" unless ( ( defined $rows ) && ( scalar @{$rows} ) > 0 );

	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		die "Error in get_job_import_substitutions: $error";
	}

	return $rows ? @$rows : ();;
}

sub get_job_import_substitution
{
	my ($id) = @_;
	my $rows;

	eval {
		die "Invalid Id value for Job import substitution" unless ($id gt 0);		

		$rows = $Config::dba->hashed_process_sql("SELECT jis.id as id, jif.title, jis.fieldid as fieldid, jis.text as text, jis.replacement as replacement ".
												" FROM job_import_substitutions jis ".
												" INNER JOIN job_import_fields jif on jif.id = jis.fieldid ".
												" WHERE jis.id = ?", [$id]);

		die "No data returned by Job import substitution" unless ( ( defined $rows ) && ( scalar @{$rows} ) > 0 );

	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		die "Error in get_job_import_substitution: $error";
	}

	return $rows ? @$rows : ();;
}

sub db_update_job_import_substitution {
    my ($id, $replacement) = @_;

	$Config::dba->process_sql("UPDATE job_import_substitutions SET replacement = ? WHERE id=? ",
                                        [ $replacement, $id ]);

    return 1;
}
