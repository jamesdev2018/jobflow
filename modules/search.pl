#!/usr/local/bin/perl -w 
package Search;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use Template;
use JSON;
use Data::Dumper;
use Cwd;

# Home grown

use lib 'modules';

use Validate;
use SessionConstants;

require "config.pl";
require "databaseCustomerUser.pl"; # package ECMDBCustomerUser, getUserCustomerIdsByOpco
require "databaseSearch.pl"; # package ECMDBSearch, db_checkSearchContent, db_getCustomerName, db_getRemotCustomerID, getOpcoNameFromId, canUserSeeOpcoid
require "JFElastic.pm";

use constant OPCO_MULTIPLE_LOCATIONS => 17;
use constant BLANK => '(blank)'; # Substitute for '' in filter values

use constant QP_SEARCH_TYPE_ID => 'searchTypeId';
use constant QP_SEARCH_CONTENT => 'searchContent';
use constant QP_ARCHIVED => 'archived';

# Call from js/custom.js, dashboardSearch
# jobflow.pl?action=dashboard&subaction=search

###################################################################################
# SearchJobs: To check job details present in DB.
# Called by jobflow.pl, for action searchjobs
# Call from js/custom.js, dashboardSearch
# jobflow.pl?action=dashboard&subaction=searchjobs
#
###################################################################################

sub SearchJobs {
	my ( $query, $session ) = @_;

	my %viz_at_other; 								# key is opconame, value is count of jobs from search at that location (which are visible)
	my %jobs_at_other;								# key is opconame, value is count of jobs from search at that location (which are invisible)
	my %cust_not_viz;								# key is "opconame => customer name(customer id)", value is count
	my %other_errors;								# key is error text, value is count
	my %JobsSearchResults;
	my @history;
	my $t0 = 0;										# Start time
	my $queryTime = 0;								# Time for initial query
	my $searchTime = 0;								# Time for complete search (and various lookups)
	my $userid = $session->param( SessionConstants::SESS_USERID );			# Users userid
	my $user_opcoid = $session->param( SessionConstants::SESS_OPCOID );		# Users current opcoid
	my $searchLimit = Config::getConfig("searchlimit") || 2000;				# Limit number of rows returned by search
	my $multiple_job_search_limit = Config::getConfig("multiple_job_search_limit") || 30;				# Limit number of rows returned by multiple job(s) search.
	my $count = 0;
	my $searchTypeId = $query->param( QP_SEARCH_TYPE_ID );
	my $searchContents = $query->param( QP_SEARCH_CONTENT );
	my $archived = $query->param( QP_ARCHIVED ) || 0;            # Set to 1 to include archived jobs

	$searchContents =~ s/,+/,/g if ($searchTypeId == 1 );
	$searchContents =~ s/^,|,$//g if ($searchTypeId == 1 );
	if ( $searchTypeId == 1 ) {
		foreach my $job (split(/,/, $searchContents)) {
    		Validate::job_number($job) if($job ne "");
    	}
	}

	my $start_time = time();

	my $vars = { userid => $userid, opcoid => $user_opcoid, archived => $archived, searchLimit => $searchLimit }; 
	$vars->{tag_env} = $ENV{'TAG_ENV'} || "DEV";

	eval {

		die "Undefined search type id" unless defined $searchTypeId;
		die "Invalid search type id '$searchTypeId'" unless ( looks_like_number( $searchTypeId ) );
		die "Missing search content" unless ( defined $searchContents );

		# push( @history, "SearchJobs: searchTypeId '$searchTypeId', contents '$searchContents'" );

		$vars->{originalSearchContents} = $searchContents; # Copy as Elastic search changes the searchContents

		# Replacing the search content with job numbers because search is done in elastic search 
		# and that job number list we pass to db to get the jobs html details
		# For search content using the server session stored in $session->param(QP_SEARCH_CONTENT)

		# If the search type uses Elastic search, then the search will return a list of job numbers
		# In this case, type 7 is Brief
		if ( $searchTypeId == 7 ) {
			# push( @history, "Calling JFElastic::searchBrief( '$searchContents', opcoid $vars->{opcoid} )" );

			my @joblist = JFElastic::searchBrief( $searchContents, $vars->{opcoid} );
			die "No jobs found by elastic search of Comments" unless ( scalar @joblist );
			my %unique_jobs;
			foreach my $job ( @joblist ) {
    			$job    =~ s/^\s+|\s+$//g; #Removing leading and trailing whitespace from Job_number
    			$unique_jobs{$job} = 1;
			}
			my @joblistUniq		= sort keys %unique_jobs;
			# eg my @joblist = JFElastic::searchBrief( "and", 179 );
			$searchContents = join (",", @joblist);
			push( @history, "Elastic search found " . (scalar @joblistUniq) . " job(s)" );
		}

		# Could be clever here and allow exact or wildcard match, but leave for now

		$t0 = time();

		# per search type validation done in databaseSearch and db_checkSearchContent can throw exception
		my ( $entity, $rows, $sql )	= ECMDBSearch::db_checkSearchContent( $searchTypeId, $searchContents, $searchLimit );
		$vars->{sql} = $sql if ( $vars->{tag_env} ne 'LIVE' );

		push( @history, "Searching for $entity '$vars->{originalSearchContents}' ..." ); # Do after rather than before as search returns entity type

		# Handle no rows slightly differently when searching by job number
		$count = scalar( @{ $rows } ) if ( defined $rows );
		if ( $count == 0 ) {
			die "Job '$searchContents' not found" if ( $searchTypeId == 1 );
			die "No jobs found, for $entity '$searchContents'";
		}
		$queryTime = time() - $t0;
		if ( $searchTypeId == 1 && $count > $multiple_job_search_limit ){
			my $jobs_len =  $#{$rows};
			splice(@$rows, $multiple_job_search_limit, $jobs_len); #Removed other jobs based on $multiple_job_search_limit.
			push( @history, "Multiple job(s) Search limit reached, $multiple_job_search_limit rows returned" );
		} elsif ( $count == $searchLimit ) {
			push( @history, "Search limit reached, $searchLimit rows returned" );
		} else {
			push( @history, "Search returned $count rows" );
		}

		#################  Job Visibility   ############################

		my $customer_ids = ECMDBCustomerUser::getUserCustomerIdsByOpco( $userid, $user_opcoid );
		die "User has no visible customers" unless ( ( defined $customer_ids ) && ( scalar @$customer_ids ) );

		my $opco_ids = ECMDBSearch::getUsersOpcos( $userid );
		die "User has no visible opcos" unless ( ( defined $opco_ids ) && ( scalar @$opco_ids ) );

		my $users_opconame = ECMDBSearch::getOpcoNameFromId( $user_opcoid ); # Name of current opco

		my @joblist;
		foreach my $job ( @{ $rows } ) {

			my $opcoid 				= $job->{ opcoid };
			my $location 			= $job->{ location };
			my $job_number 			= $job->{ job_number };
			my $customerid 			= $job->{ customerid };
			my $jobArchiveStatus 	= $job->{ status };
			eval {
				my $job_visible = 0;

				# Get the customer name first as we need it in the error messages
				die "Customer id is undefined" if ( ! defined $customerid ) || ( $customerid eq '' );

				# Can we see customer ie visible at any of the opcos we can see
				# in scalar context grep returns number of elements that match, http://perldoc.perl.org/functions/grep.html
				my $customer_visible = grep {$_ eq $customerid }  @$customer_ids;
				if ( ! $customer_visible ) {
					
					my ( $customerName ) = ECMDBSearch::db_getCustomerName( $customerid );
					die "Failed to get customer name for id '$customerid'" unless defined $customerName;

					my $opconame = ECMDBSearch::getOpcoNameFromId( $opcoid );
					die "opcoid '$opcoid' is invalid" unless ( defined $opconame );
					my $key = "Customer $opconame => $customerName not visible, please contact Jobflow admin";

					updateJobsSearchResultsHash($key, $job_number, \%JobsSearchResults );
				#	die "Customer $key not visible, please contact Jobflow admin" if ( $searchTypeId == 1 );
					$cust_not_viz{ $key }++;

				} else {
					# Customer is visible, now check home/remote location is visible

					if ( ( $user_opcoid == $opcoid ) || ( ( defined $location ) && ( $user_opcoid == $location ) ) ) {
						# User can see job at home location, or remote location
						if ( $jobArchiveStatus == 6 && $vars->{archived} == 0 ) {
							#Dont push to joblist. Need to enable 'Show Archive' Flag.
						} else {
							push( @joblist, $job_number );
						}
						$job_visible = 1;

					} elsif ( ( defined $location ) && ( $location == OPCO_MULTIPLE_LOCATIONS ) ) {
						if ( ECMDBSearch::hasJobShardAtLocation( $job_number, $user_opcoid ) ) {
							if ( $jobArchiveStatus == 6 && $vars->{archived} == 0 ) {
								#Dont push to joblist. Need to enable 'Show Archive' Flag.
							} else {
								push( @joblist, $job_number );
							}
							$job_visible = 1;
						}
					}

					if ( $job_visible == 0 ) {

						# The user cant see this job with their current opcoid, so determine if they can
						# change to another Opco where they can see the jobs (using either opcoid or location).

						my @locations;		# List of opconames visible or not
						my @visible_locations;	# Subset which are visible
						my $visible_opcoid;
						my $visible_opco_location;

						my $opconame= ECMDBSearch::getOpcoNameFromId( $opcoid );
						push( @locations, "opco $opconame" );

						my $visible_at = ( grep { $_ eq $opcoid } @$opco_ids  );
						if ( $visible_at ) {
							push( @locations, "opco $opconame" );
							push( @visible_locations, $opconame );
							$visible_opcoid = $opcoid;
							$viz_at_other{ $opconame }++ unless ( $searchTypeId == 1 );
						}

						my $location_opconame;
						my @other_locations;
						if ( ! defined $location ) { 
						   	# nothing
						} elsif ( $location == OPCO_MULTIPLE_LOCATIONS ) {
							# get shard locations ..
							my $shard_locations = ECMDBSearch::getShardLocations($job_number);
							@other_locations = @{$shard_locations};
						} elsif ( $location != $opcoid ) { 
							push( @other_locations, $location ); 
						}
						foreach my $location (@other_locations) {
							$visible_at = ( grep { $_ eq $location }  @$opco_ids  );
							if ( $visible_at ) {
								$location_opconame = ECMDBSearch::getOpcoNameFromId( $location );
								if ( ! defined $location_opconame ) {
									my $error = "location $location is invalid";
									$other_errors{ $error }++;
								} else {
									push( @locations, "location $location_opconame" );
									push( @visible_locations, $location_opconame );
									$visible_opco_location = $location;
									$viz_at_other{ $location_opconame }++ unless ( $searchTypeId == 1 );
								}
							};
						}

						# If the job is not visible to the user irrespective of their available opcos
						# then either throw an error (if searching for a job number) or log a summary
						# of the invisible jobs, recording a count against the opconame

						if ( $searchTypeId == 1 ) {
							# For search by job number just display an error, for other search types, the info is in %viz_at_other
	
							my $opco_customers = ECMDBCustomerUser::getUserCustomerIdsByOpco( $userid, $visible_opcoid );
							my $location_customers = ECMDBCustomerUser::getUserCustomerIdsByOpco( $userid, $visible_opco_location);
							my $opco_customer_visible = grep { $_ eq $customerid } @$opco_customers ;	
							my $location_customer_visible = grep { $_ eq $customerid } @$location_customers ;	
							my ( $customerName ) = ECMDBSearch::db_getCustomerName( $customerid );
							if($opco_customer_visible || $location_customer_visible) {
								die "Job exists at " . join( ", ", @locations ) . ", customer $customerName" if ( ( scalar @visible_locations ) == 0 );
#								push( @history, "\n".$job_number." job is visible at " . join( ", ", @visible_locations ) . ", customer $customerName \n" );
								my $key = " Visible at " . join( ", ", @visible_locations ) . ", customer $customerName \n";
								updateJobsSearchResultsHash($key, $job_number, \%JobsSearchResults );
							} else {
#								push( @history, "\n".$job_number." you do not have access to customer $customerName at " . join( ", ", @visible_locations ) . ", please contact Jobflow admin \n" );
								my $key = "you do not have access to customer $customerName at " . join( ", ", @visible_locations ) . ", please contact Jobflow admin";
								updateJobsSearchResultsHash($key, $job_number, \%JobsSearchResults );
							}

						} else {
							# Types other than job number

							if ( ( scalar @visible_locations ) == 0 ) {
								# Otherwise record the presence of jobs which match at other opcos
								foreach my $loc ( @locations ) {
									$jobs_at_other{ $loc } ++;
								}
							}
						}
					}
				} # endif (customer not visible)

				# After all that, check whether the job is archived or not.
				warn "SearchJobs: jobArchiveStatus is undef" unless defined $jobArchiveStatus;

				#die "Search job $job_number is archived... please enable 'Show Archived' checkbox " if ( ( $jobArchiveStatus == 6 ) && ( $vars->{archived} == 0 ) );
				if ( ( $jobArchiveStatus == 6 ) && ( $vars->{archived} == 0 ) ){
					my $key = "Archived... please enable 'Show Archived' checkbox from Filter.";
					updateJobsSearchResultsHash($key, $job_number, \%JobsSearchResults );
					$searchContents	=~	s/$job_number//;
					$count++;
				}
			};
			if ( $@ ) {
				my $error = $@;
				$error =~ s/ at modules.*//gs;
				die $error if ( $searchTypeId == 1 ); # Any error for search by job throw to outer level
				$other_errors{ $error }++;
			}
		} # end of foreach

		$searchTime = time() - $t0;

		my $jobcount = scalar( @joblist );
		if ( $jobcount == 0 ) {
			die "No jobs visible at current Opco" if ( $searchTypeId != 1 );
		} elsif ( $jobcount == 1 && $searchTypeId != 1 ) {
			push( @history, " Displaying 1 job" );
		} elsif($searchTypeId != 1) {
			push( @history, " Displaying $jobcount jobs" );
		}
		$vars->{jobs} = \@joblist; 
	};
	if ( $@ ){
		my $error = $@;
		$error =~ s/ at modules.*//gs;
		$vars->{error} = $error;
	}

	push( @{$vars->{history}}, @history ); # Append local history to the one used in Dashboard

	$vars->{viz_at_other} = \%viz_at_other;
	$vars->{jobs_at_other} = \%jobs_at_other; 
	$vars->{cust_not_viz} = \%cust_not_viz;
	$vars->{other_errors} = \%other_errors;
	$vars->{search_type_id} = $searchTypeId;
	$vars->{search_type_contents} = $searchContents;
	$vars->{query_time} = $queryTime; 
	$vars->{search_time} = (time() - $t0);
	$vars->{JobsSearchResults} = \%JobsSearchResults;
	$vars->{jobhashcount} = $count;
	# Convert to JSON and emit

	$vars->{elapsedtime} = time() - $start_time;

	my $json = encode_json ( $vars );
	print $json;
	return $json;

} #SearchJobs

sub updateJobsSearchResultsHash {
	my $key						= $_[0];
	my $value					= $_[1];
	my $JobsSearchResults_ref	= $_[2];

	if ( exists $JobsSearchResults_ref->{$key}){
		$JobsSearchResults_ref->{$key} .= ", ".$value;
	} else {
		$JobsSearchResults_ref->{$key} .= $value;
	}
}

1;

