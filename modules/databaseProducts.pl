#!/usr/local/bin/perl
package ECMDBProducts;

use strict;
use warnings;

use Carp qw/carp/;

use lib 'modules';

use Validate;

require 'config.pl';
require 'general.pl';
require 'databaseJob.pl';
##############################################################################
#       assignProduct()
# 	assign a product to a user
##############################################################################
sub assignProduct
{
	my ($assigneeid, $status, $lineitemid) = @_;
	
	return 0 if !General::is_valid_id($assigneeid);
	return 0 if !General::is_valid_id($status);
	return 0 if !General::is_valid_id($lineitemid);
	
	$Config::dba->process_sql("UPDATE products SET userid=?, status=?
						WHERE id=?", [ $assigneeid, $status, $lineitemid ]);
	
	return 1;
}

##############################################################################
##       unassignProduct()
##   unassign a product from an user
##############################################################################
sub unassignProduct
{
    my ($lineitemid) = @_;

    return 0 if !General::is_valid_id($lineitemid);

    $Config::dba->process_sql("UPDATE products SET userid=0, status=0
    					WHERE id=?", [ $lineitemid ]);
	
	return 1;
}

##############################################################################
# getProductAssigneeId
#
##############################################################################
sub getProductAssigneeId
{
    my ($job_number, $lineitemid) = @_;

    return 0 if !General::is_valid_id($job_number);
	return 0 if !General::is_valid_id($lineitemid);

    my ($userid) = $Config::dba->process_oneline_sql("SELECT userid FROM products
    					WHERE job_number=? AND id=?", [ $job_number, $lineitemid ]);
	
    return $userid || 0;
}

##################################################################################
# getCurrentJobVersion
#
##################################################################################
sub getCurrentJobVersion
{
    my ($job_number, $lineitemid) = @_;

    return 0 if !General::is_valid_id($job_number);
    return 0 if !General::is_valid_id($lineitemid);

    my ($currentjobversion) = $Config::dba->process_oneline_sql("SELECT JOBVERSION FROM products
                        WHERE job_number=? AND id=?", [ $job_number, $lineitemid ]);

    return $currentjobversion || 0;
}

####################################################################################
# getProductName
#
#################################################################################### 
sub getProductName
{
    my ($job_number, $lineitemid) = @_;

    return 0 if !General::is_valid_id($job_number);
    return 0 if !General::is_valid_id($lineitemid);

    my ($productname) = $Config::dba->process_oneline_sql("SELECT PRODUCTNAME FROM products
                        WHERE job_number=? AND id=?", [ $job_number, $lineitemid ]);

    return $productname || 0;
}

###################################################################################
# getMaxJobVersion
#
###################################################################################
sub getMaxJobVersion
{
    my ($job_number) = @_;

    return 0 if !General::is_valid_id($job_number);

    my ($maxjobversion) = $Config::dba->process_oneline_sql("SELECT MAX(JOBVERSION) FROM products
                        WHERE job_number=? ", [ $job_number ]);

    return $maxjobversion || 0;
}

##############################################################################
#       getProductStatus()
# 	return the status of a product
#	0	None
# 	1	Pending (Assigned)
# 	2	In Progress
# 	3	Paused
# 	4	Completed
##############################################################################
sub getProductStatus
{
	my ($lineitemid) = @_;

	# return 0 if !General::is_valid_id($lineitemid);
	
	my ($status) = $Config::dba->process_oneline_sql("SELECT status FROM products
						WHERE id=?", [ $lineitemid ]);
	
	return $status;
}

##############################################################################
##   getProductAssignee()
##   get current assignee of the product
##############################################################################
sub getProductAssignee
{
    my ($lineitemid) = @_;

    # return 0 if !General::is_valid_id($lineitemid);

    my ($userid) = $Config::dba->process_oneline_sql("SELECT userid FROM products
    					WHERE id=?", [ $lineitemid ]);
	
    return $userid;
}

##############################################################################
###   getCurrentHouseRevision()
###   
##############################################################################
sub getCurrentHouseRevision
{
    my ($lineitemid) = @_;

    return 0 if !General::is_valid_id($lineitemid);

    my ($revision) = $Config::dba->process_oneline_sql("SELECT houserevision FROM products
    					WHERE id=?", [ $lineitemid ]);

    return $revision;
}

##############################################################################
#	setProductStatus()
#	update the prodict status ( 0,1,2,3,4 )
##############################################################################
sub setProductStatus 
{
	my ($status, $lineitemid) = @_;

	# return 0 if !General::is_valid_id($lineitemid);
	return 0 if !General::is_valid_id($status);

	$Config::dba->process_sql("UPDATE products SET status=? 
                    WHERE id=?", [ $status, $lineitemid ]);
	
	return 1;
}

##############################################################################
#	startProduct()
#	A user starts working on a product
##############################################################################
sub startProduct {
	my ($lineitemid, $userid, $job_number, $opcoid, $customerid, $location,
			$houserevision, $checklistid) = @_;
			
	return 0 if !General::is_valid_id($lineitemid);
	return 0 if !General::is_valid_id($userid);
	return 0 if !General::is_valid_id($job_number);
	return 0 if !General::is_valid_id($opcoid);
	return 0 if !General::is_valid_id($customerid);
	return 0 if !General::is_valid_id($location);
	
	$houserevision = 0 if !$houserevision;
	return 0 if !General::is_valid_number($houserevision);
	
	$checklistid = 0 if !$checklistid;
	return 0 if !General::is_valid_number($checklistid);
	
	
	$Config::dba->process_sql("INSERT INTO productstimecapture 
						(productid, userid, job_number, opcoid, customerid,
						 location, houserevision, starttime,
						 ischecklist, checklistid)
						VALUES (?, ?, ?, ?, ?, ?, ?, now(), ?, ?)",
						[ $lineitemid, $userid, $job_number, $opcoid, $customerid,
						  $location, $houserevision,
						  $checklistid ? 1 : 0, $checklistid ]);
	
	return 1;
}

##############################################################################
# addRevisionReason
#
##############################################################################
sub addRevisionReason{
	my ($restartreasonid, $lineitemid, $userid, $job_number, $opcoid, $customerid) = @_;
	
	return 0 if !General::is_valid_id($restartreasonid);
	return 0 if !General::is_valid_id($lineitemid);
	return 0 if !General::is_valid_id($userid);
	return 0 if !General::is_valid_id($job_number);
	return 0 if !General::is_valid_id($opcoid);
	return 0 if !General::is_valid_id($customerid);

	my ($productstimecaptureid) = $Config::dba->process_oneline_sql("SELECT MAX(ID) FROM productstimecapture WHERE productid=? AND USERID = ? AND JOB_NUMBER = ? ", [ $lineitemid, $userid, $job_number ]);
	
			$productstimecaptureid = 0 if ( ! defined $productstimecaptureid );

    $Config::dba->process_sql("INSERT INTO productrevisionreason
    					(reasonid, PRODUCTSTIMECAPTUREID, lineitemid, userid, job_number, opcoid,
    					 customerid, rec_timestamp)
					VALUES (?, ?, ?, ?, ?, ?, ?, now())",
					[ $restartreasonid, $productstimecaptureid, $lineitemid, $userid, $job_number, 
					$opcoid, $customerid ]);
	
	return 1;
}

##############################################################################
#	stopProduct()
#	A user stops working on a product
# 	Only UPATE the endtime IF it is NULL - don't repeatedly UPDATE the
# 	same end time again and again
# 	In effect You can only startTime ONCE and stopTime ONCE per 'task'
#	If a (ANY) user has started a task - there will be a row with ENDTIME IS NULL
#	Stopped = 1 if user stop a product
#	So that is a check isProductStarted() ? func?
##############################################################################
sub stopProduct 
{
	my ($lineitemid, $userid) = @_;
	
	return 0 if !General::is_valid_id($lineitemid);
	return 0 if !General::is_valid_id($userid);
	
	
	$Config::dba->process_sql("UPDATE productstimecapture SET endtime=now()
						WHERE productid=? AND userid=? AND ischecklist=0
							AND endtime IS NULL",
						[ $lineitemid, $userid ]);
						
	$Config::dba->process_sql("UPDATE productstimecapture SET stopped=1
						WHERE productid=? AND userid=? AND ischecklist=0
						ORDER BY id DESC
						LIMIT 1", [ $lineitemid, $userid ]);
	
	$Config::dba->process_sql("UPDATE products SET userid=0 WHERE id=?",
						[ $lineitemid ]);
	
	return 1;
}

##############################################################################
# updateRejectReason
#
##############################################################################
sub updateRejectReason 
{
	my ($lineitemid, $userid, $rejectreasonid) = @_;
	
	return 0 if !General::is_valid_id($lineitemid);
	return 0 if !General::is_valid_id($userid);
	return 0 if !General::is_valid_id($rejectreasonid);
	
	$Config::dba->process_sql("UPDATE productstimecapture SET rejectreasonid=?
						WHERE productid=? AND userid=? AND ischecklist=0
						ORDER BY id DESC
						LIMIT 1", [ $rejectreasonid, $lineitemid, $userid ]);
						
	$Config::dba->process_sql("UPDATE products SET rejectreasonid=?
						WHERE id=?", [ $rejectreasonid, $lineitemid ]);
	
	updateQcStatus( $lineitemid, 3 );
	
    return 1;
}

##############################################################################
# updateQcStatus
#
##############################################################################
sub updateQcStatus
{
	my ($lineitemid, $status) = @_;
	
	return 0 if !General::is_valid_id($lineitemid);
	return 0 if !General::is_valid_id($status);
	
    $Config::dba->process_sql("UPDATE products SET qcstatus=?
    						WHERE id=?", [ $status, $lineitemid ]);
	
    return 1;
}

##############################################################################
##   pauseProduct()
##   A user pause working on a product
##   Only UPATE the endtime IF it is NULL - don't repeatedly UPDATE the
##   same end time again and again
##   In effect You can only startTime ONCE and stopTime ONCE per 'task'
##   If a (ANY) user has started a task - there will be a row with ENDTIME IS NULL
##   So that is a check isProductStarted() ? func?
###############################################################################
sub pauseProduct 
{
	my ($lineitemid, $userid) = @_;
	
	# return 0 if !General::is_valid_id($lineitemid);
	return 0 if !General::is_valid_id($userid);

    $Config::dba->process_sql("UPDATE productstimecapture SET endtime=now()
    					WHERE productid=? AND userid=? AND ischecklist=0
    					AND endtime IS NULL", [ $lineitemid, $userid ]);
	
    return 1;
}

#############################################################################
# updateJobAssignmentHistory
#
############################################################################
sub updateJobAssignmentHistory
{
    my ($userid, $jobid, $lineitemid) = @_;
	
	return 0 if !General::is_valid_id($userid);
	# return 0 if !General::is_valid_id($jobid);
	# return 0 if !General::is_valid_id($lineitemid);
	
	$Config::dba->process_sql("UPDATE tracker3plus SET operatorstatus='A', assignee=?
						WHERE job_number=?", [ $userid, $jobid ]);
	
	$Config::dba->process_sql("UPDATE assignmenthistory
						SET status='A', requested_timestamp=now()
						WHERE job_number=? AND status in ('W')
							AND assigneeid=? AND lineitemid = ?
						ORDER BY id DESC
						LIMIT 1", [ $jobid, $userid, $lineitemid ]);
    
    return 1;
}

##############################################################################
###   hasWorkedOnBefore()
###   
##############################################################################
sub hasWorkedOnBefore
{
	my ($lineitemid, $userid) = @_;

	#return 0 if !General::is_valid_id($lineitemid);
	# return 0 if !General::is_valid_id($userid);
	
    my ($stopped) = $Config::dba->process_oneline_sql("SELECT 1 FROM productstimecapture
    					WHERE productid=?  AND stopped=1 AND ischecklist=0
    					LIMIT 1",
    					[ $lineitemid ]);

    return $stopped || 0;
}

##############################################################################
# getLastStoppedTime
#
##############################################################################
sub getLastStoppedTime
{
	my ($lineitemid, $userid) = @_;
	
#	return 0 if !General::is_valid_id($lineitemid);
	# return 0 if !General::is_valid_id($userid);
	
    my ($time) = $Config::dba->process_oneline_sql("SELECT TIME_TO_SEC(TIMEDIFF(endtime, starttime))
    					FROM productstimecapture 
					WHERE productid=? AND userid=? AND ischecklist=0 AND endtime IS NOT NULL
					ORDER BY id DESC
					LIMIT 1", [ $lineitemid, $userid ]);
	
    return $time;
}

##############################################################################
# getElapsedTime
#
##############################################################################
sub getElapsedTime
{
	my ($lineitemid, $userid) = @_;
	
	#return 0 if !General::is_valid_id($lineitemid);
	# return 0 if !General::is_valid_id($userid);
	
    my ($elapsedtime) = $Config::dba->process_oneline_sql("SELECT
    					SUM(TIME_TO_SEC(TIMEDIFF(COALESCE(endtime, now()), starttime)))
    					FROM productstimecapture
    					WHERE productid=? AND userid=? AND ischecklist=0",
    					[ $lineitemid, $userid ]);
	
    return $elapsedtime || 0;
}

##############################################################################
##   viewProductHistory()
##   
##############################################################################
sub viewProductHistory
{
    my ($lineitemid) = @_;
	
	return 0 if !General::is_valid_id($lineitemid);

	my $rows = $Config::dba->hashed_process_sql("SELECT ptc.job_number,
						CONCAT(u.fname, ' ', u.lname) AS username, p.productname,
						ptc.houserevision, ptc.starttime, ptc.endtime
						FROM productstimecapture ptc
						INNER JOIN users u ON ptc.userid = u.id
						INNER JOIN products p ON ptc.productid = p.ID
						WHERE ptc.ischecklist = 0
							AND ptc.productid = ?
						ORDER BY ptc.id DESC ", [ $lineitemid ]);
	
    return $rows ? @$rows : ();
}


##############################################################################
# isProductWorkedOnByUser
#
##############################################################################
sub isProductWorkedOnByUser
{
    my ($jobid, $userid) = @_;

    return 1 if !General::is_valid_id($jobid);
    return 1 if !General::is_valid_id($userid);

	## status == 2 implies 'In progress'
	my ($isworkedon) = $Config::dba->process_oneline_sql("SELECT 1 FROM products
						WHERE userid=? AND status=2", [ $userid ]);
	
    return $isworkedon || 0;
}

##############################################################################
# isProductBeingWorkedOn
#
##############################################################################
sub isProductBeingWorkedOn
{
    my ($job_number, $lineitemid) = @_;

    return 1 if !General::is_valid_id($job_number);
	return 1 if !General::is_valid_id($lineitemid);

    my ($isworkedon) = $Config::dba->process_oneline_sql("SELECT 1 FROM assignmenthistory
						WHERE job_number=? AND lineitemid=? AND status IN ('W')",
    						[ $job_number, $lineitemid ]);
    	
    return $isworkedon || 0;
}

##############################################################################
# isProductAssignedWorkedOn
#
##############################################################################
sub isProductAssignedWorkedOn
{
    my ($job_number, $lineitemid) = @_;

    return 1 if !General::is_valid_id($job_number);
    return 1 if !General::is_valid_id($lineitemid);

	my ($isworkedon) = $Config::dba->process_oneline_sql("SELECT 1 FROM products
						WHERE job_number=? AND id=? AND status IN (2,3) ",
						[ $job_number, $lineitemid ]);
	
    return $isworkedon || 0;
}

##############################################################################
# isProductAssigned
#
##############################################################################
sub isProductAssigned
{
    my ($job_number, $lineitemid) = @_;

    return 1 if !General::is_valid_id($job_number);
    return 1 if !General::is_valid_id($lineitemid);

	## status == 1 implies 'Pending'
    my ($isworkedon) = $Config::dba->process_oneline_sql("SELECT 1 FROM products
						WHERE job_number=? AND id=? AND status=1",
						[ $job_number, $lineitemid ]);
    	
    return $isworkedon || 0;
}

##############################################################################
# updateProductAssign
#
##############################################################################
sub updateProductAssign
{
    my ($assigneeid, $job_number, $lineitemid) = @_;

    return 0 if !General::is_valid_id($lineitemid);
    return 0 if !General::is_valid_id($job_number);
    return 0 if !General::is_valid_id($assigneeid);

    # $Config::dba->process_sql("DELETE FROM assignmenthistory
	#					WHERE job_number=? AND lineitemid=? AND assigneeid=? AND status='A'",
	#					[ $job_number, $lineitemid, $assigneeid ]);

	$Config::dba->process_sql("UPDATE assignmenthistory
                        SET status='C', requested_timestamp=now()
                        WHERE job_number=? AND status in ('A')
                            AND assigneeid=? AND lineitemid = ?
                        ORDER BY id DESC
                        LIMIT 1", [ $job_number, $assigneeid, $lineitemid ]);

    return 1;
}

#############################################################################
# userCurrenProductDetails
#
#############################################################################
sub userCurrenProductDetails
{
    my ($userid) = @_;

    return 0 if !General::is_valid_id($userid);
	
	 #my ($productname, $job_number, $productid) = $Config::dba->process_oneline_sql("SELECT productname, job_number, id FROM products WHERE userid=? AND status=2", [ $userid ]);
	 my ($productname, $job_number, $productid) = $Config::dba->process_oneline_sql(
			"SELECT productname, job_number, id FROM products 
			WHERE userid=? AND status=2", [ $userid ]);
	
    return ($productid, $productname, $job_number);
}


##############################################################################
# db_productDetails
# Get product details
#
# TODO: Correct capitalisation in column variables
##############################################################################
sub db_productDetails {
    my ($job_number) = @_; 
    
    #return 0 if !General::is_valid_id($job_number);

    my ($details) = $Config::dba->hashed_process_sql("SELECT p.ROUTEVERSION, 
						p.JOBVERSION, p.PRODUCTID, p.PRODUCTNAME, p.QUANTITY, p.ESTAMOUNT,
						p.PRODUCTIONREMARKS, p.ISDELETED, p.ID, p.ISACTIVE, ps.status,
						p.STATUS AS PRODUCTSTATUS, CONCAT(COALESCE(u.FNAME,'None'),
						' ', COALESCE(u.LNAME, '')) AS USERNAME, pqcs.status AS QCSTATUS 
						FROM products p
						LEFT JOIN productstatus ps ON ps.id = p.status
						LEFT JOIN users u ON u.id = p.userid
						LEFT JOIN tracker3plus t3p ON t3p.job_number = p.job_number
						LEFT JOIN productqcstatus pqcs ON pqcs.id = p.qcstatus
						WHERE p.job_number=? AND p.ISDELETED = 0
						ORDER BY p.id", [ $job_number ]);
	
	foreach (@{ $details || [] })
	{
		$_->{ACTUAL} 				= getProductTimeWorked($_->{ID});
		$_->{JOBVERSIONONEBRIEF} 	= getJobversionOneBrief($job_number); }
	
	return $details || [];
}

################################################################################
# getRejectReasons
#
################################################################################
sub getRejectReasons {
    my @rejectreasons;

	my $rows = $Config::dba->hashed_process_sql("SELECT ID, COMMENT FROM commentslist
						WHERE ctype='r' ");

    return $rows ? @$rows : ();
}

##############################################################################
# db_currentProductDetails
# Get current product's details
#
# TODO: Correct capitalisation in column variables
##############################################################################
sub db_currentProductDetails {
	my ($job_number, $lineitemid) = @_;
	
	#return 0 if !General::is_valid_id($job_number);
	#return 0 if !General::is_valid_id($lineitemid);
	
	my ($details) = $Config::dba->hashed_process_sql("SELECT p.ROUTEVERSION,
						p.JOBVERSION, p.PRODUCTID, p.PRODUCTNAME, p.QUANTITY,
						p.PRODUCTIONREMARKS, p.ISDELETED, p.ID, ps.status,
						CONCAT(COALESCE(u.FNAME,'None'), ' ',
						COALESCE(u.LNAME, '')) AS USERNAME, tp.DUEDATE,
						p.DIMENSIONNAME, p.ESTAMOUNT, u.USERNAME AS LOGINNAME
						FROM products p
						LEFT JOIN productstatus ps ON ps.id = p.status
						LEFT JOIN users u ON u.id = p.userid
						LEFT JOIN tracker3plus tp ON p.job_number = tp.job_number
						WHERE p.job_number=? AND p.id=?
						ORDER BY p.id ", [ $job_number, $lineitemid ]);
	
	foreach (@{ $details || [] })
	{
		$_->{ACTUAL} = getProductTimeWorked($_->{ID});
	}
	
    return $details || [];
}

##############################################################################
# getProductRestartReasons
# Get Product Restart Reasons
##############################################################################
sub getProductRestartReasons{
    my ($reasons) = $Config::dba->hashed_process_sql("SELECT id, reason FROM productrestartreasons");

    return $reasons || [];
}

##############################################################################
## getProductTimeWorked
## Get Actual time for each product
##############################################################################
sub getProductTimeWorked{
	my ($lineitemid) = @_;
	my($hh, $mm, $ss) = "";

	my ($actualtime) = $Config::dba->process_oneline_sql( "SELECT SUM(TIME_TO_SEC(TIMEDIFF(endtime, starttime)))
						FROM productstimecapture
						WHERE productid = ?  AND ischecklist = 0
						AND endtime IS NOT NULL", [ $lineitemid ]);
						
	$actualtime ||= 0;
	
	$hh = int($actualtime / (60 * 60));
	$mm = int(($actualtime / 60) % 60);
	$ss = int($actualtime % 60);

	$hh = "0$hh" if $hh < 10;
	$mm = "0$mm" if $mm < 10;
	$ss = "0$ss" if $ss < 10;
	
	return "$hh:$mm:$ss";
}

##############################################################################
#      isProductsEnabled 
#	   check if enableproducts flag is switched on for customerID
##############################################################################
sub isProductsEnabled{
    my ($customerid) = @_;

	return 0 if !General::is_valid_id($customerid);

	my ($enabled) = $Config::dba->process_oneline_sql("SELECT enableproducts
						FROM CUSTOMERS WHERE id=?", [ $customerid ]);
						
    return $enabled || 0;
}


##############################################################################
#      isQCProductTime
#	   check if product has QC time recorded against it
##############################################################################
sub isQCProductTime{
    my ($jobs_ref) = @_;
	
	foreach my $job_number (@$jobs_ref)
	{
		next if !General::is_valid_id($job_number);
		
		my ($approved) = $Config::dba->process_oneline_sql("SELECT qcstatus FROM products
						WHERE qcstatus=2 AND job_number=?
						LIMIT 1", [ $job_number ]);
		return 1 if $approved;
	}
	
	return 0;
}

##############################################################################
# getCurrentJobversion
#
#############################################################################
sub getCurrentJobversion{
    my ($lineitemid) = @_;
    
    return 0 if !General::is_valid_id($lineitemid);
    
    my ($version) = $Config::dba->process_oneline_sql("SELECT jobversion FROM products
						WHERE id=?", [ $lineitemid ]);
	
    return $version || 0;
}

#############################################################################
# updateQcStatusForAllProducts
#
#############################################################################
sub updateQcStatusForAllProducts{
	my ($job_number, $status, $current_jobversion) = @_;
	
	return 0 if !General::is_valid_id($job_number);
	# return 0 if !General::is_valid_id($status);
	return 0 if !General::is_valid_id($current_jobversion);	## Has to be a positive integer, like an ID
	
	$Config::dba->process_sql("UPDATE products SET qcstatus=?
						WHERE job_number=? AND jobversion=?",
						[ $status, $job_number, $current_jobversion ]);
	
	return 1;
}

##############################################################################
# getCustomerUsersProducts
#
##############################################################################
sub getCustomerUsersProducts {
	my ( $customerid, $jobnumber, $jobopcoid, $location ) = @_;

	my $users;
	eval {
		Validate::customerid( $customerid );
		Validate::job_number( $jobnumber );
		Validate::opcoid( $jobopcoid );
		Validate::opcoid( $location );

		my ($shardlocations) = $Config::dba->process_oneline_sql(
			"SELECT GROUP_CONCAT(DISTINCT(LOCATION)) AS shardlocations FROM shards WHERE JOB_NUMBER = ?
				AND LOCATION NOT IN (?, ?) 
				GROUP BY JOB_NUMBER ", [ $jobnumber, $jobopcoid, $location ]);

		my $locations = "$jobopcoid, $location";
		$locations = $locations . ", $shardlocations" unless ( ( ! defined $shardlocations ) || ( $shardlocations eq "null" ) || ( $shardlocations eq "" ) );

		$users = $Config::dba->hashed_process_sql(
			"SELECT DISTINCT cu.userid, u.username, u.fname, u.lname
			FROM customeruser cu
			LEFT OUTER JOIN users u ON u.id=cu.userid AND u.STATUS=1 
			WHERE cu.customerid IN ( SELECT ID FROM CUSTOMERS WHERE customer = (select customer FROM customers WHERE id = ?)
				AND opcoid IN ( $locations )) 
			ORDER BY u.username ", [ $customerid ] );
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$jobnumber = '' unless defined $jobnumber;
		$jobopcoid = '' unless defined $jobopcoid;
		$location = '' unless defined $location;
		carp "getCustomerUsersProducts: job '$jobnumber', jobopcoid '$jobopcoid', location '$location'  $error";
	}
	return $users ? @$users : ();
}

############################################################################
# db_updateJobQcStatus
#
############################################################################
sub db_updateJobQcStatus
{
    my ($jobnumber, $qcstatus) = @_;

	$Config::dba->process_sql("UPDATE TRACKER3PLUS SET qcstatus=? WHERE job_number=?",
                    [ $qcstatus, $jobnumber ]);
}

############################################################################
# isAnyProductRejected
#
############################################################################
sub isAnyProductRejected
{
    my ($job_number) = @_;

    my ($productrejectcount) = $Config::dba->process_oneline_sql("SELECT COUNT(ID) FROM products
                        WHERE JOB_NUMBER=? AND QCSTATUS = 3 ",
                            [ $job_number ]);

    return $productrejectcount || 0;
}

############################################################################
# ViewTimeCapture
#
############################################################################
sub ViewTimeCapture
{
    my ($jobnumber) = @_;

    return 0 if !General::is_valid_id($jobnumber);

    my $rows = $Config::dba->hashed_process_sql("SELECT ptc.job_number,
                        CONCAT(u.fname, ' ', u.lname) AS username, p.productname,
                        ptc.houserevision, ptc.starttime, ptc.endtime
                        FROM productstimecapture ptc
                        INNER JOIN users u ON ptc.userid = u.id
                        INNER JOIN products p ON ptc.productid = p.ID
                        WHERE ptc.ischecklist = 0
                            AND ptc.JOB_NUMBER = ?
                        ORDER BY ptc.id DESC ", [ $jobnumber ]);

    return $rows ? @$rows : ();
}

############################################################################
# getJobversionOneBrief: To get the CMD brief comment of version one.
############################################################################
sub getJobversionOneBrief {
    my ($job_number) = @_;

    my ($jobversionOneBrief) = $Config::dba->process_oneline_sql("SELECT brief FROM tracker3plus
                        WHERE JOB_NUMBER=? ",
                            [ $job_number ]);

    return $jobversionOneBrief;
}

#################################################################
# getQcOpcoid
#
#################################################################
sub getQcOpcoid {

	my ($qcopcoid) = $Config::dba->process_oneline_sql("SELECT OPCOID FROM operatingcompany WHERE OPCONAME = 'QC_MONITOR';");

	return $qcopcoid;
}

#################################################################
# getJobLastLocation
#
#################################################################
sub getJobLastLocation {
	my ($jobnumber) = @_;

	Validate::job_number( $jobnumber );

	my ($location) = $Config::dba->process_oneline_sql("SELECT LAST_LOCATION FROM TRACKER3PLUS WHERE JOB_NUMBER=? ",[ $jobnumber ]);

	return $location;
}

#################################################################
# updateJobLocation
#
#################################################################
sub updateJobLocation {
	my ($jobnumber, $location) = @_;

	Validate::job_number( $jobnumber );
	return 0 if !General::is_valid_id($location);

	my ($lastlocation) = $Config::dba->process_oneline_sql("SELECT LOCATION FROM TRACKER3PLUS WHERE JOB_NUMBER=? ",[ $jobnumber ]);

	$Config::dba->process_sql("UPDATE TRACKER3PLUS SET LOCATION=?, LAST_LOCATION=?
	WHERE JOB_NUMBER=?", [ $location, $lastlocation, $jobnumber ]);

	return 1;
}

#################################################################
# setProductQcStatus
#
#################################################################
sub setProductQcStatus
{
    my ( $status, $lineitemid ) = @_;
    
    return 0 if !General::is_valid_id( $status );
	Validate::lineItem( $lineitemid );
    $Config::dba->process_sql("UPDATE products SET QCSTATUS=?
                    WHERE id=?", [ $status, $lineitemid ]);

    return 1;
}

#################################################################
#To get all 'QC status' product's of a job.
#################################################################
sub db_getJobProductQCStatus {
    my ( $jobnumber ) = @_;

	Validate::job_number( $jobnumber );
    my $rows = $Config::dba->hashed_process_sql("SELECT distinct qcstatus, JOBVERSION FROM products where job_number = ? ", [ $jobnumber ] );

    return $rows ? @$rows : ();
}

#################################################################
#To get 'QC Status' of current product using lineitemid.
#################################################################
sub db_getCurrentProductQCStatus {
    my ( $lineItemID ) = @_;

    Validate::lineItem( $lineItemID );
    my ( $currentProductQCStatus ) = $Config::dba->process_oneline_sql("SELECT qcstatus FROM products WHERE id =? ",[ $lineItemID ] );

    return $currentProductQCStatus;
}

# db_getLatestVersionProducts
#
#################################################################
sub db_getLatestVersionProducts {
    my ($job_number) = @_;

       my ($details) = $Config::dba->hashed_process_sql("SELECT p.ROUTEVERSION,
                        p.JOBVERSION, p.PRODUCTID, p.PRODUCTNAME, p.QUANTITY, p.ESTAMOUNT,
                        p.PRODUCTIONREMARKS, p.ISDELETED, p.ID, p.ISACTIVE, ps.status,
                        p.STATUS AS PRODUCTSTATUS, CONCAT(COALESCE(u.FNAME,'None'),
                        ' ', COALESCE(u.LNAME, '')) AS USERNAME, pqcs.status AS QCSTATUS
                        FROM products p
                        LEFT JOIN productstatus ps ON ps.id = p.status
                        LEFT JOIN users u ON u.id = p.userid
                        LEFT JOIN tracker3plus t3p ON t3p.job_number = p.job_number
                        LEFT JOIN productqcstatus pqcs ON pqcs.id = p.qcstatus
                        WHERE p.job_number=? AND p.ISDELETED = 0
						AND p.JOBVERSION = ( SELECT MAX(JOBVERSION) FROM products WHERE job_number=? )
                        ORDER BY p.id", [ $job_number, $job_number ]);

    foreach (@{ $details || [] })
    {
        $_->{ACTUAL}                = getProductTimeWorked($_->{ID});
        $_->{JOBVERSIONONEBRIEF}    = getJobversionOneBrief($job_number); }

    return $details || [];
}

#################################################################
# db_updateTaskState
#
#################################################################
sub db_updateTaskState
{
    my ($productid, $isactive_to_update) = @_;
	my $vars;

	eval {
		return 0 if !General::is_valid_id($productid);

    	$Config::dba->process_sql("UPDATE products SET ISACTIVE=?
                    				WHERE id=?", [ $isactive_to_update, $productid ]);
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		die "db_excludeProduct: ".$error;
	}

    return 1;
}

#################################################################
# db_getLastStateOfProduct
#
#################################################################
sub db_getLastStateOfProduct {
    my ($productid) = @_;
	return 0 if !General::is_valid_id($productid);

    my ($laststate) = $Config::dba->process_oneline_sql("SELECT ISACTIVE FROM products WHERE id=? ",[ $productid ]);

    return $laststate;
}


################################################################
#
#getActiveProductdetails
################################################################

sub getActiveProductdetails {

    my ($userid) = @_;
    Validate::userid( $userid );

    my ( $lineitemid, $job_number )  = $Config::dba->process_oneline_sql("SELECT ID,JOB_NUMBER FROM products WHERE USERID =? AND STATUS=2 ", [ $userid ]);

    return ($lineitemid, $job_number);

}

################################################################
# db_getMaxVersion
#
################################################################
sub db_getMaxVersion {
	my ($jobnumber) = @_;
	Validate::job_number( $jobnumber );

	my ($version) = $Config::dba->process_oneline_sql("SELECT MAX(JOBVERSION) FROM products WHERE JOB_NUMBER=? ",[ $jobnumber ]);

	return $version || 1;
}
###############################################################
# get count of products which is work by users
###############################################################
sub db_getAssignedProductCount {
	my ($jobnumber) = @_;
	Validate::job_number( $jobnumber );
	my $version	=	db_getMaxVersion($jobnumber);
	my ($count)	= $Config::dba->process_oneline_sql( "SELECT count(*) FROM products where job_number = ? AND JOBVERSION = ? AND status!=0", [ $jobnumber,$version ]);
	return $count;
}
##############################################################################
# db_latestProductDetails
# Get latest versions product details
##############################################################################
sub db_latestProductDetails {
    my ($job_number) = @_;

	my $version	=	db_getMaxVersion($job_number);
    my ($details) = $Config::dba->hashed_process_sql("SELECT p.ROUTEVERSION,
                        p.JOBVERSION, p.PRODUCTID, p.PRODUCTNAME, p.QUANTITY, p.ESTAMOUNT,
                        p.PRODUCTIONREMARKS, p.ISDELETED, p.ID, p.ISACTIVE, ps.status,
                        p.STATUS AS PRODUCTSTATUS, CONCAT(COALESCE(u.FNAME,'None'),
                        ' ', COALESCE(u.LNAME, '')) AS USERNAME, pqcs.status AS QCSTATUS,t3p.brief AS JOBVERSIONONEBRIEF
                        FROM products p
                        LEFT JOIN productstatus ps ON ps.id = p.status
                        LEFT JOIN users u ON u.id = p.userid
                        LEFT JOIN tracker3plus t3p ON t3p.job_number = p.job_number
                        LEFT JOIN productqcstatus pqcs ON pqcs.id = p.qcstatus
                        WHERE p.job_number=? AND p.JOBVERSION=? AND p.ISDELETED = 0
                        ORDER BY p.id", [ $job_number,$version ]);

    foreach (@{ $details || [] })
    {
        $_->{ACTUAL}                = getProductTimeWorked($_->{ID});
    }

    return $details || [];
}

############################################################################
# Added by Suganya
# PD-3592 - To validate the given job is working by job level or task level
############################################################################
sub checkJobLevel {
    my ($job_number)   =   @_;
    my $level = 0;
    eval {
        my ($count)     = db_getAssignedProductCount($job_number);
        my ($assignee,$qcstatus)  = ECMDBJob::get_assigneeid_and_qcstatus($job_number);
        #level 0 means neither JOB or TASK level.
        #level 1 mentioned as JOB Level
        #Level 2 mentioned as TASK level
        if(($count==0 && $assignee!=0) || ($count==0 && $assignee==0 && ( $qcstatus==1 || $qcstatus==2 || $qcstatus==3 ) ))
        {
            $level = 1;
        }
        elsif($count > 0)
        {
            $level = 2;
        }
    };
    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        die "checkJobLevel : ".$error;
    }
        return $level || 0;

}

1;
