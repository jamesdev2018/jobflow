#!/usr/local/bin/perl
package ECMDBUser;

use warnings;
use strict;

use Scalar::Util qw( looks_like_number );

use CGI::Carp qw/cluck/;

# Home grown
use lib 'modules';

use Validate;

require "config.pl";

##############################################################################
#      db login 
#
##############################################################################
sub db_Login {
	my ($username, $password) = @_;

	my $userid;
	my $status;

	eval {
		die "Username is undef" unless defined $username;
		die "Password is undef" unless defined $password;

		($userid, $status) = $Config::dba->process_oneline_sql(
				"SELECT id, status FROM users WHERE username=? AND password=?",
				[ $username, $password]);
		if ( ! $userid ) {
			$userid = 0;
		} elsif ( $status == 1 ) {
			$Config::dba->process_oneline_sql(
				"UPDATE users SET update_time=now() WHERE id=?", [ $userid ]);
		}
	};
	if ( $@ ) {
		cluck "db_Login exception, username " . ( $username ||'undef' ) . ' ' . $@;
		$userid = 0;
	}
	return ( $userid, $status );
}

sub db_update_time {
	my ( $userid ) = @_;

	eval {
		Validate::userid( $userid );

		$Config::dba->process_oneline_sql( "UPDATE users SET update_time=now() WHERE id=?", [ $userid ]);
	};
	if ( $@ ) {
		cluck "db_update_time exception, userid " . ( $userid || 0 ) . ' ' . $@;
	}
}

##############################################################################
#      db login TEMP
#	process a login from the USERSTEMP table (for temporary logins)
##############################################################################
sub db_LoginTemp {
	my ($username, $password) = @_;

	my $userid;
	my $email;
	eval {
		die "Username is undef" unless defined $username;
		die "Password is undef" unless defined $password;

		( $userid, $email ) = $Config::dba->process_oneline_sql(
			"SELECT id, email FROM userstemp WHERE ( username = ? ) AND ( password = ? ) AND ( status = 1 ) AND ( expire_timestamp > now() )",
				[ $username, $password ] );
	};
	if ( $@ ) {
		$username = 'undef' unless defined $username;
		cluck "db_LoginTemp exception, username $username, $@";
		$userid = 0;
	}
	
	return ( $userid || 0, $email ||'' );
}

##############################################################################
#	get the NOTE columns from the USERSTEMP table (for temporary logins)
##############################################################################
sub db_GetTempUserNote {
	my ($userid) = @_;

	my $note;
	eval {
		Validate::userid( $userid );
	
		($note) = $Config::dba->process_oneline_sql("SELECT note FROM userstemp WHERE id=?", [ $userid ]);
	};
	if ( $@ ) {
		cluck "db_GetTempUserNote: userid: " . ( $userid || 0 ) . ' ' . $@;
	
	}
	return $note;
}


##############################################################################
#      db logout
#
##############################################################################
sub db_Logout {
	my ($userid) = @_;

	eval {
		Validate::userid( $userid );
		#When user logged out update the resource status as 4 which means Jobflow is logged out but Workstation is On	
		$Config::dba->process_sql("UPDATE users SET update_time=NULL,RESOURCESTATUSID=4 WHERE id=?", [ $userid ]);
	};
	if ( $@ ) {
		cluck "db_Logout: userid: " . ( $userid || 0 ) . ' ' . $@;
		return 0;
	}	
	return 1;
}


#############################################################################
### db_updateLAST_LOGGEDIN:  To update the latest user login time.
#############################################################################

sub db_updateLAST_LOGGEDIN {

	my ( $userID ) = @_;

	eval {
		Validate::userid( $userID );
		$Config::dba->process_sql("UPDATE users SET LAST_LOGGEDIN=now(), RESOURCESTATUSID=2  WHERE id=?", [ $userID ]);
	};
	if ( $@ ) {
		cluck "db_updateLAST_LOGGEDIN: userID: " . ( $userID || 0 ) . ' ' . $@;
		return 0;
	}
	return 1;
}



##############################################################################
#      db update session
#
##############################################################################
sub db_UpdateSession {
	my ($userid, $sessionid) = @_;

	eval {
		Validate::userid( $userid );
		die "Invalid Session" unless defined $sessionid;
	
		# die "Session not a session" unless General::is_valid_hash($sessionid);
		# Should check for ref to CGI::Session package TODO
	
		$Config::dba->process_sql( "UPDATE users SET update_time=now(), sessionid=? WHERE id=?", [ $sessionid, $userid ]);
	};
	if ( $@ ) {
		cluck "db_UpdateSession: userid: " . ( $userid || 0 ) . ' ' . $@;
		return 0;
	}
	return 1;
}

##############################################################################
#      db getUsername 
#
##############################################################################
sub db_getUsername {
	my ($userid) = @_;

	my $username;
	eval {
		Validate::userid( $userid );
		($username) = $Config::dba->process_oneline_sql("SELECT username FROM users WHERE id=?", [ $userid ]);
	};
	if ( $@ ) {
		cluck "db_getUsername: userid: " . ( $userid || 0 ) . ' ' . $@;
	}
	return $username;
}

##############################################################################
#      db_getRole 
#
##############################################################################
sub db_getRole {
	my ($userid) = @_;

	my $role;
	eval {
		Validate::userid( $userid );
		($role) = $Config::dba->process_oneline_sql("SELECT role FROM users WHERE id=?", [ $userid ]);
	};
	if ( $@ ) {
		cluck "db_getrole: userid: " . ( $userid || 0 ) . ' ' . $@;
	}
	return $role;
}

##############################################################################
#      Get the users' e-mail address 
#
##############################################################################
sub db_getEmail {
	my ($userid) = @_;

	my $email;
	eval {
		Validate::userid( $userid );
		($email) = $Config::dba->process_oneline_sql("SELECT email FROM users WHERE id=?", [ $userid ]);
	};
	if ( $@ ) {
		cluck "db_getEmail: userid: " . ( $userid || 0 ) . ' ' . $@;
	}
	return $email;
}

##############################################################################
#      Get a list of users, filtered by the username 
#
##############################################################################
sub db_getUsers {
	my ($name_filter, $type) = @_;


	my $columnselected = "";
	my $users;

	eval {
		$name_filter = '' unless defined $name_filter;
		# Allow pretty well anything for name filter
		#die "Non Alpha numeric name filter '$name_filter'" if ( $name_filter ne '' ) && !Validate::isAlphaNumeric($name_filter);
	
		die "Type is undef" unless defined $type;
		die "Type is not numeric" unless ( looks_like_number( $type ) );

		if ( $type == 1 ) {
			$columnselected = "u.lname";
		} elsif ( $type == 2 ) {
			$columnselected = "u.fname";
		} elsif ( $type == 3 ) {
			$columnselected = "u.username";
		} elsif ( $type == 4 ) {
			$columnselected = "u.email";
		} elsif ( $type == 5 ) {
			$columnselected = "u.employeeid";
		} else {
			die "Invalid type $type"; 
		}

		my @values;
		my $sql = "SELECT u.id, oc.opconame, oc.opcoid, c.name AS companyname, u.fname, u.lname, u.username, u.email, r.role, u.rec_timestamp FROM users u " .
			" LEFT OUTER JOIN operatingcompany oc ON oc.opcoid=u.opcoid " .
			" LEFT OUTER JOIN companies c ON c.id=u.companyid " .
			" LEFT OUTER JOIN roles r ON u.role=r.id " .
			" WHERE u.status != 0"; 
		if ( $name_filter ne '' ) {
			$name_filter = '%' . $name_filter . '%';
			$sql .= " AND $columnselected LIKE ?";
			push( @values, $name_filter );
		}
		$users = $Config::dba->hashed_process_sql( $sql, \@values );
	};
	if ( $@ ) {
		cluck "db_getUsers: " . $@; # Stack trace but dont rethrow exception
	}
	return $users ? @$users : ();
}


##############################################################################
#      Get a list of users, filtered by the surname
#
##############################################################################
sub db_getUsersBySurname {
	my ($lname_filter) = @_;
	
	## Allow empty entries
	return () if $lname_filter && !Validate::isAlphaNumeric($lname_filter);

	my $sql = "SELECT u.id, oc.opconame, oc.opcoid, c.name AS companyname, u.fname, u.lname, u.username, u.email, r.role, u.rec_timestamp FROM users u " .
			" LEFT OUTER JOIN operatingcompany oc ON oc.opcoid=u.opcoid " .
			" LEFT OUTER JOIN companies c ON c.id=u.companyid " .
			"  LEFT OUTER JOIN roles r ON u.role=r.id " .
			" WHERE u.status != 0 ";
	my $users;
	my @values;
	
	eval {
		if ( defined $lname_filter ) {
			die "Invalid username filter" unless Validate::isAlphaNumeric($lname_filter);
			$sql .= "AND u.lname LIKE ?";
			push( @values, '%' . $lname_filter . '%' );
		}
		$users = $Config::dba->hashed_process_sql( $sql, \@values );
	};
	if ( $@ ) {
		cluck "db_getUsersBySurname: sql: $sql, " . $@;
	}
	return $users ? @$users : ();
}

##############################################################################
#      getUser 
#
##############################################################################
sub getUser {
	my ($userid) = @_;

	my $users;
	eval {
		Validate::userid( $userid );	
		$users = $Config::dba->hashed_process_sql(
				"SELECT u.id, u.status, u.opcoid, u.companyid, u.fname, u.lname, u.username, u.email, " .
				" 	u.employeeid, u.role, r.role AS rolename, u.rec_timestamp " .
				" FROM users u 	INNER JOIN roles r ON u.role=r.id WHERE u.id=?", 
				[ $userid ]);
	};
	if ( $@ ) {
		cluck "getUser: userid " . ( $userid || 0 ) . ' ' . $@;
	}
	return $users ? @$users : ();
}

# getUserSettings, setUserSettings, db_applyColumnManager and db_updateColumnManager moved to databaseColumnManager

# getUserFilterSettings and getFilterData, db_saveFilter, db_renameFilter, db_deleteFilter moved to databaseUserFilter.pl

##############################################################################
#      addUser 
#
##############################################################################
sub addUser {
	my ( $opcoid, $companyid, $fname, $lname, $username, $password, $email, $employeeid, $roleid, $options ) = @_;

	my $userid;

	die "Invalid options arg" if ( ( defined $options ) && ( ref ( $options ) ne "HASH" ) );
	$options = { } unless ( defined $options );

	my $userstatus = $options->{userstatus};
	$userstatus = 1 unless defined $userstatus; # 1 means active, 0 means disabled and 2 I don't know

	my $deputyemail = $options->{deputyemail};

	my $broadcast		= $options->{broadcast};
	my $catalogue		= $options->{catalogue};
	my $directmarketing	= $options->{directmarketing};
	my $interactive		= $options->{interactive};
	my $literature		= $options->{literature};
	my $outdoor		= $options->{outdoor};
	my $packaging		= $options->{packaging};
	my $pointofsale		= $options->{pointofsale};
	my $press		= $options->{press};
	my $artworker		= $options->{artworker};
	my $creative		= $options->{creative};
	my $design		= $options->{design};
	my $junior		= $options->{junior};
	my $retouching		= $options->{retouching};
	my $department		= $options->{department} || 0;  # NOT NULL column
	my $cost_center		= $options->{cost_center};
	my $contracted_hours	= $options->{contracted_hours};
	my $workhoursstarts	= $options->{workhoursstarts}.":". $options->{workhoursstarts_min} ;
	my $workhoursends	= $options->{workhoursends}.":".$options->{workhoursends_min};

	eval {
		# If role is Vendor, dont validate opcoid though should be undef anyway
		if($roleid != 2)
		{
			Validate::opcoid( $opcoid ) if($opcoid);
		}

		Validate::companyid( $companyid ) if($companyid);
		Validate::username( $username );

		Validate::firstname( $fname );
		Validate::lastname( $lname );
		Validate::email( $email );
		Validate::employeeid( $employeeid );
		Validate::roleid( $roleid );

		die "Invalid password" if !defined $password;
		$password = crypt($password, "ab");

		# TODO validate userstatus ... number tween 0 and 3 ?

		# Check row doesnt already exist for username
		my ( $user_exists ) = $Config::dba->process_oneline_sql( "SELECT id FROM users WHERE username = ?", [ $username ] );
		die "User exists with user name '$username'" if defined $user_exists;
	
		$Config::dba->process_sql("INSERT INTO users ( status, opcoid, companyid, fname, lname, username, password, email, deputyemail, " .
				" broadcast, catalogue, directmarketing, interactive, literature, outdoor, packaging, pointofsale, press, " .
				" artworker, creative, design, junior, retouching, " .
				" employeeid, role, dept, cost_center, contracted_hours, workhoursstarts, workhoursends, " .
				" rec_timestamp, update_time ) " .
				" VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, " .
				"          ?, ?, ?, ?, ?, ?, ?, ?, ?, " .
				"          ?, ?, ?, ?, ?, " .
				"          ?, ?, ?, ?, ?, ?, ?, " .
				"	   now(), now() )",
				[ $userstatus, $opcoid, $companyid, $fname, $lname, $username, $password, $email, $deputyemail, 
					$broadcast, $catalogue, $directmarketing, $interactive, $literature, $outdoor, $packaging, $pointofsale, $press,
					$artworker, $creative, $design, $junior, $retouching,
					$employeeid, $roleid, $department, $cost_center, $contracted_hours, $workhoursstarts, $workhoursends ] );

		( $userid ) = $Config::dba->process_oneline_sql( "SELECT last_insert_id()" );

	};
	if ( $@ ) {
		my $error	= $@;
		if($error =~ /(.*) at (.*)/ig){
			$error=$1;
		}
		die "$1";
	}
	return ( 1, $userid );
}

##############################################################################
#      addUserTemp
#
##############################################################################
sub addUserTemp {
	my ($username, $password, $note, $email ) = @_;

	my $userid;
	eval {
		Validate::username( $username );
		Validate::password( $password );
		die "Invalid note" unless defined $note;

		$Config::dba->process_sql(
				"INSERT INTO userstemp (status, username, password, note, email, rec_timestamp, expire_timestamp) " .
				" VALUES (1, ?, ?, ?, ?, now(), DATE_ADD(now(), INTERVAL 7 DAY))",
				[ $username, $password, $note, $email ]);
		($userid) = $Config::dba->process_oneline_sql( "SELECT last_insert_id()" );
	};
	if ( $@ ) {
		cluck "addUserTemp: $@";
		return ( 0, 0 );
	}
	return (1, $userid);
}

##############################################################################
#      updateUser 
#
# nb companyid is not supplied, only used when adding a vendor
##############################################################################
sub updateUser {
	my ( $userid, $opcoid, $fname, $lname, $username, $email, $employeeid, $roleid, $options ) = @_;

	die "Invalid options arg" if ( ( defined $options ) && ( ref ( $options ) ne "HASH" ) );
	$options = { } unless ( defined $options );

	my $userstatus = $options->{userstatus};
	$userstatus = 1 unless defined $userstatus; # 1 means active, 0 means disabled and 2 I don't know

	my $deputyemail		= $options->{deputyemail};

	my $broadcast		= $options->{broadcast};
	my $catalogue		= $options->{catalogue};
	my $directmarketing	= $options->{directmarketing};
	my $interactive		= $options->{interactive};
	my $literature		= $options->{literature};
	my $outdoor		= $options->{outdoor};
	my $packaging		= $options->{packaging};
	my $pointofsale		= $options->{pointofsale};
	my $press		= $options->{press};
	my $artworker		= $options->{artworker};
	my $creative		= $options->{creative};
	my $design		= $options->{design};
	my $junior		= $options->{junior};
	my $retouching		= $options->{retouching};
	my $department		= $options->{department} || 0;  # NOT NULL column
	my $cost_center		= $options->{cost_center};
	my $contracted_hours	= $options->{contracted_hours};
	my $workhoursstarts	= $options->{workhoursstarts}.":".$options->{workhoursstarts_min};
	my $workhoursends	= $options->{workhoursends}.":".$options->{workhoursends_min};

	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid ) if defined $opcoid;
		Validate::firstname( $fname );
		Validate::lastname( $lname );
		Validate::email( $email );
		Validate::employeeid( $employeeid );
		Validate::roleid( $roleid );
		Validate::username( $username );


		# TODO validate userstatus ... number tween 0 and 3 ?

		# both username and userid supplied (and userid alread checked as valid)
		# User can change username but not if there already is another user entry with the same name

		# Check whether user exist for username and if so is it a different userid
		my ( $user_exists ) = $Config::dba->process_oneline_sql( "SELECT id FROM users WHERE username = ?", [ $username ] );
		die "Other user exists with user name '$username'" if  ( ( defined $user_exists ) && ( $user_exists != $userid ) );;
	
		$Config::dba->process_sql(
			"UPDATE users SET " .
				"status = ?, opcoid=?, fname=?, lname=?, username=?, email=?, deputyemail=?, " .
				" broadcast=?, catalogue=?, directmarketing=?, interactive=?, literature=?, outdoor=?, packaging=?, pointofsale=?, press=?, " .
				" artworker=?, creative=?, design=?, junior=?, retouching=?, " .
				" employeeid=?, role=?, dept=?, cost_center=?, contracted_hours=?, workhoursstarts=?, workhoursends=?, " .
				" update_time=now()  WHERE id = ?",

				[ $userstatus, $opcoid, $fname, $lname, $username, $email, $deputyemail,
					$broadcast, $catalogue, $directmarketing, $interactive, $literature, $outdoor, $packaging, $pointofsale, $press,
					$artworker, $creative, $design, $junior, $retouching,
					$employeeid, $roleid, $department, $cost_center, $contracted_hours, $workhoursstarts, $workhoursends,
					$userid ] );
		my ( $user_exists_planner ) = $Config::dba->process_oneline_sql( "SELECT id FROM user_calendar_mapping WHERE userid = ?", [ $userid ] );
		
		if ( $user_exists_planner ) {
			$Config::dba->process_sql( "UPDATE user_calendar_mapping SET OPCOID = ? WHERE userid = ? ", [ $opcoid, $userid ] );
		}
		#
	};
	if ( $@ ) {
		cluck "updateUser: $@";
		return 0;
	}
	return 1;
}

# Update table userskillsmapping for this user, holds list of user skills
# Arguments
# - userid
# - userSkillsMap - array of hash { skillid, skilloption } or undef (in which case populate from user_skills)

sub updateUserSkillsMap {
	my ( $userid, $userSkillsMap ) = @_;

	if ( defined $userSkillsMap ) {
		die "updateUserSkillsMap: userSkillsMap is neither undef or Array" unless ( ref ( $userSkillsMap ) eq "ARRAY" ); 
		foreach my $skill ( @$userSkillsMap ) { # each $skill is hash ref { skillid, skilloption }

			my ( $option ) = $Config::dba->process_oneline_sql( "SELECT options FROM userskillsmapping WHERE userid = ? AND skillid = ?", [ $userid, $skill->{skillid} ] );

			if ( ! defined $option ) {
				$Config::dba->process_sql(
					"INSERT INTO userskillsmapping ( userid, skillid, options, rec_timestamp ) VALUES ( ?, ?, ?, now() )",
					[ $userid, $skill->{skillid}, $skill->{skilloption} ] );

			} elsif ( ( ! defined $skill->{skilloption} ) || ( $option != $skill->{skilloption} ) ) {
				$Config::dba->process_sql(
					"UPDATE userskillsmapping SET options = ?, rec_timestamp = now() WHERE userid = ? AND skillid = ?", 
					[ $skill->{skilloption}, $userid, $skill->{skillid} ] ); 
			}
		}
	} else {
		# Initialise all options to 0
        	$Config::dba->process_sql(
			"INSERT INTO userskillsmapping ( userid, skillid, options, rec_timestamp ) SELECT ?, id, 0, now() FROM user_skills", [ $userid ] );
	}
}

# Update table usersmediatypes for this user, holds list of user skills
# Arguments
# - userid
# - userMediaTypesMap - array of hash { mediatypeid, mediaoption } or undef (in which case populate from media_types)

sub updateUserMediaTypes {
	my ( $userid, $userMediaTypesMap ) = @_;

	if ( defined $userMediaTypesMap ) {
		die "updateUserMediaTypes: userMediaTypesMap is neither undef or Array" unless ( ref ( $userMediaTypesMap ) eq "ARRAY" );

		foreach my $mediatype ( @$userMediaTypesMap ) { # each $mediatype is hash ref { mediatypeid, mediaoption }

			my ( $option ) = $Config::dba->process_oneline_sql( 
				"SELECT options FROM usermediatypes WHERE userid = ? AND mediatypeid = ?", [ $userid, $mediatype->{mediatypeid} ] );
			if ( ! defined $option ) {
				$Config::dba->process_sql(
					"INSERT INTO usermediatypes ( userid, mediatypeid, options, rec_timestamp ) VALUES ( ?, ?, ?, now() )",
					[ $userid, $mediatype->{mediatypeid}, $mediatype->{mediaoption} ] );

			} elsif ( ( ! defined $mediatype->{mediaoption} ) || ( $option != $mediatype->{mediaoption} ) ) {
				$Config::dba->process_sql(
					"UPDATE usermediatypes SET options = ?, rec_timestamp = now() WHERE userid = ? AND mediatypeid = ?", 
					[ $mediatype->{mediaoption}, $userid, $mediatype->{mediatypeid} ] ); 
			}
		}
	} else {
		# Initialise all options to 0
        	$Config::dba->process_sql(
			"INSERT INTO usermediatypes ( userid, mediatypeid, options, rec_timestamp ) SELECT ?, id, 0, now() FROM mediatypes", [ $userid ] );
	}
}

##############################################################################
#      updateOpCoId
#
##############################################################################
sub updateOpCoId {
	my ($userid, $opcoid) = @_;

	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );
	
		$Config::dba->process_sql( 
			"UPDATE users SET opcoid=? WHERE id=?",
			[ $opcoid, $userid ]);
	};
	if ( $@ ) {
		cluck "updateOpCoId: $@";
		return 0;
	}
	return 1;
}

##############################################################################
#      updateRole
#
##############################################################################
sub updateRole {
	my ($userid, $roleid) = @_;

	eval {
		Validate::userid( $userid );	
		Validate::roleid( $roleid );
		$Config::dba->process_sql("UPDATE users SET role=? WHERE id=?", [ $roleid, $userid ]);
	};
	if ( $@ ) {
		cluck "updateRole: $@";
		return 0;
	}
	return 1;
}

##############################################################################
#      userExists 
#
##############################################################################
sub userExists {
	my ($username) = @_;

	my $exists = 0;
	eval {
		Validate::username( $username );
		($exists) = $Config::dba->process_oneline_sql(
				"SELECT 1 FROM users WHERE username=? LIMIT 1", [ $username ]);
	};
	if ( $@ ) {
		cluck "userExists: username: " . ( $username || "" ) . ' ' . $@;
		$exists = 0;
	}
	return $exists || 0;
}

##############################################################################
#      userExistsById 
#
##############################################################################
sub userExistsById {
	my ($userid) = @_;

	my $exists;
	eval {
		Validate::userid( $userid );	
		($exists) = $Config::dba->process_oneline_sql("SELECT 1 FROM users WHERE id=?", [ $userid ]);
	};
	if ( $@ ) {
		cluck "userExistsById: userid: " . ( $userid || 0 ) . ' ' . $@;
		$exists = 0;
	}
	return $exists || 0;
}

##############################################################################
#      isUserAssignedToOpco 
#
##############################################################################
sub isUserAssignedToOpco {
	my ($userid, $opcoid) = @_;

	my $exists;
	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );
		($exists) = $Config::dba->process_oneline_sql(
			"SELECT 1 FROM useropcos WHERE userid=? AND opcoid=?",
			[ $userid, $opcoid ]);
	};
	if ( $@ ) {
		cluck "isUserAssignedToOpco: $@";
		$exists = 0;
	}
	return $exists || 0;
}

# isUserAssignedToCustomer moved to databaseCustomerUser.pl


##############################################################################
#		addUserOpco
#		add this opco to this user (used in modules/useredit.pl)
#
##############################################################################
sub addUserOpco {
	my ($userid, $opcoid) = @_;

	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );
	
		$Config::dba->process_sql("INSERT INTO useropcos (userid, opcoid)
						VALUES (?, ?)", [ $userid, $opcoid ]);
	};
	if ( $@ ) {
		cluck "addUserOpco: $@";
		return 0;
	}
	return 1;
}



# getCustomerUsers, addCustomerUser, removeCustomerUser moved to databaseCustomerUser.pl

##############################################################################
#      deleteUser 
#		a logical delete - status 0 is deleted, and 1 is active
#
##############################################################################
sub deleteUser {
	my ($userid) = @_;

	eval {
		Validate::userid( $userid );
		$Config::dba->process_sql("UPDATE users SET status=0 WHERE id=?", [ $userid ]);
	};
	if ( $@ ) {
		cluck "deleteUser: userid " . ( $userid || 0 ) . ' ' . $@;
		return 0;
	}
	return 1;
}

##############################################################################
#       getRoles
#      
#		TODO: This probably should be elsewhere
##############################################################################
sub getRoles {

	my $roles;
	eval {
		$roles = $Config::dba->hashed_process_sql("SELECT id, role FROM roles ORDER BY role");
	};
	if ( $@ ) {
		cluck "getRoles: $@";
	}
	return $roles ? @$roles : ();
}

# inappropriately named getOpcoOperators moved to databaseCustomerUser.pl

##############################################################################
# db_getOpcoid
#
##############################################################################
sub db_getOpcoid {
	my ($userid) = @_;

	my $opcoid;
	eval {
		Validate::userid( $userid );
		($opcoid) = $Config::dba->process_oneline_sql("SELECT opcoid FROM users WHERE id=?", [ $userid ]);
	};
	if ( $@ ) {
		cluck "db_getOpcoid: userid " . ( $userid || 0 ) . ' ' . $@;
	}
	return $opcoid;
}
##############################################################################
# db_getlastname
#
##############################################################################
sub db_getlastname {
	my ($userid) = @_;

	my $lname;
	eval {
		Validate::userid( $userid );
		($lname) = $Config::dba->process_oneline_sql("SELECT lname FROM users WHERE id=?", [ $userid ]);
	};
	if ( $@ ) {
		cluck "db_getlastname: $@";
	}
	return $lname;
}

##############################################################################
#     db_getUserOpcoRoles 
#
##############################################################################
sub db_getUserOpcoRoles {
	my ($userid, $opcoid) = @_;

	return () unless $opcoid;
	
	my $roles;
	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );
		$roles = $Config::dba->hashed_process_sql(
			"SELECT ur.roleid, r.role FROM userroles ur INNER JOIN roles r ON ur.roleid=r.id WHERE ur.userid=? AND ur.opcoid=?",
			[ $userid, $opcoid ] );
	};
	if ( $@ ) {
		cluck "db_getUserOpcoRoles: $@";
	}
	return $roles ? @$roles : ();
}

##############################################################################
#     getRoleName 
#
##############################################################################
sub getRoleName{
	my ($roleid) = @_;

	my $rolename;
	eval {
		Validate::roleid($roleid);
		($rolename) = $Config::dba->process_oneline_sql(
			"SELECT role FROM roles WHERE id=?", [ $roleid ]);
	};
	if ( $@ ) {
		cluck "getRoleName: roleid " . ( $roleid || 0 ) . ' ' . $@;
	}
	return $rolename || "";
}

##############################################################################
#      db_interactive 
# TODO: Move to databaseMediaTypes.pl
##############################################################################
sub db_interactive {
	my ($userid) = @_;

	my $interactive;
	eval {
		Validate::userid( $userid );

		($interactive) = $Config::dba->process_oneline_sql(
			"SELECT um.options FROM usermediatypes um 
			INNER JOIN mediatypes m ON m.id=um.mediatypeid 
			WHERE  m.type='Interactive' AND  um.userid=?", [ $userid ]);
	};
	if ( $@ ) {
		cluck "db_interactive: userid " . ( $userid || 0 ) . ' ' . $@;
	}
	return ( $interactive || 0 );
}

##############################################################################
###  db_getUserId
##
#############################################################################
sub db_getUserId {
	my ($username) = @_;
	my $userid;
	eval {
		Validate::username( $username );
		($userid) = $Config::dba->process_oneline_sql("SELECT id FROM users WHERE username=?", [ $username ] );
	};
	if ( $@ ) {
		cluck "db_getUserId: username: " . ( $username || '' ) . ' ' . $@;
	}
	return $userid || 0;
}

#############################################################################
####  db_Isuser
## TODO: replicates userExists (above)
############################################################################
sub db_Isuser {
	my ($username) = @_;

	my $userid;
	eval {
		Validate::username( $username );
		($userid) = $Config::dba->process_oneline_sql("SELECT 1 FROM users  WHERE USERNAME=? ",[ $username ]);
	};
	if ( $@ ) {
		cluck "db_Isuser: username: " . ( $username || '' ) . ' ' . $@;
	}
	return $userid || 0;
}

############################################################################
### db_Isgroup
## TODO: Move to databaseMessageGroup. (used in modules/jobs.pl)
###########################################################################
sub db_Isgroup {
	my ($username) = @_;

	my $groupid;
	eval {
		Validate::username( $username );
		($groupid) = $Config::dba->process_oneline_sql("SELECT 1 FROM messagegroup  WHERE GROUPNAME=? ",[ $username ]);
	};
	if ( $@ ) {
		cluck "db_Isgroup: username: " . ( $username || '' ) . ' ' . $@;
	}
  	return $groupid || 0;
}

############################################################################
### db_getGroupUserId
## TODO: Move to databaseMessageGroup. (used in modules/jobs.pl)
###########################################################################
sub db_getGroupUserId {
  	my ($groupname) = @_;

	my @userid;
	eval {

		die "Invalid groupname" if !Validate::isAlphaNumeric($groupname);
		my ($userid) = $Config::dba->hashed_process_sql("SELECT USERID  FROM messagegroup  WHERE GROUPNAME=? ",[ $groupname ]);
		foreach my $userID_hash (@$userid) {
			my $groupuserid = $userID_hash->{'USERID'};
			push( @userid, $groupuserid );
		}
	};
	if ( $@ ) {
		cluck "db_getGroupUserId: groupname: " . ( $groupname || '' ) . ' ' . $@;
	}
  	return @userid;
}

#############################################################################

sub db_update_autorefresh_duration {
	my ($userid, $auto_refresh) = @_;

	eval {
		Validate::userid( $userid );
		die "Invalid auto_refresh" unless ( ( defined $auto_refresh ) && ( looks_like_number( $auto_refresh ) ) );

		$Config::dba->process_sql("UPDATE users SET auto_refresh=? WHERE id=?", [ $auto_refresh, $userid ]);
	};
	if ( $@ ) {
		cluck "db_update_autorefresh_duration: $@";
		return 0;
	}
	return 1;
}

sub db_get_autorefresh_duration {
	my ( $userid ) = @_;

	my $auto_refresh;
	eval {
		Validate::userid( $userid );
		
		###
		## TODO: PD-2804 [[ Temporarily disable auto-refresh until database issues are resolved ]]
		#($auto_refresh) = $Config::dba->process_oneline_sql("SELECT auto_refresh FROM users WHERE id=?",[ $userid ]);
	};
	if ( $@ ) {
		cluck "db_get_autorefresh_duration: userid " . ( $userid || 0 ) . ' ' . $@;
	}
	return ( $auto_refresh || 0 );
}

sub db_authorise_action_group {
	my ( $actiongroup, $roleid ) = @_;

	my $valid = 0;
	eval {
		die "actiongroup not defined" unless defined $actiongroup;
		Validate::roleid($roleid);

		($valid) = $Config::dba->process_oneline_sql("SELECT 1 FROM permissions WHERE entity=? AND roleid=?", [ $actiongroup, $roleid ]);
	};
	if ( $@ ) {
		my $error = $@;
		$roleid = 'undef' unless defined $roleid;
		$actiongroup = 'undef' unless defined $actiongroup;
		cluck "db_authorise_action_group: actiongroup: $actiongroup, roleid $roleid, $error";
		$valid = 0;
	}
	return $valid;
}

##############################################################################
#      Get a list of users, filtered by the User ID 
# TODO: replicates getUser although it also returns opconame, companyname
# and uses LEFT OUTER JOINS instead of INNER JOINS (so could have null names)
##############################################################################

sub db_getUsersByUserID {

    my ( $userID ) = @_;
    my $users	= undef;

    eval {

        my $sql = "SELECT u.id, oc.opconame, oc.opcoid, c.name AS companyname, u.fname, u.lname, u.username, u.email, r.role, u.rec_timestamp FROM users u " .
            " LEFT OUTER JOIN operatingcompany oc ON oc.opcoid=u.opcoid " .
            " LEFT OUTER JOIN companies c ON c.id=u.companyid " .
            " LEFT OUTER JOIN roles r ON u.role=r.id " .
            " WHERE u.status != 0 AND u.id=?";
        $users = $Config::dba->hashed_process_sql( $sql, [ $userID ] );
    };
    if ( $@ ) {
        cluck "db_getUsersByUserID: " . $@; # Stack trace but dont rethrow exception
    }
    return $users ? @$users : ();
} #db_getUsersByUserID

#############################################################################
#db_getResourceContractualHours: To get the Resource contractural hours
#############################################################################

sub db_getResourceContractualHours {

	my $userID						= $_[0];
	my $selectedDate				= $_[1];

    my $workHrsEnd       			= undef;
	my $workHrsStart     			= undef;
	my $adjContractualHours 		= undef;
    my $adjContractualHrsEnd       	= undef;
	my $adjContractualHrsStart     	= undef;
    my $costcode                    = undef;
    eval {

		Validate::userid( $userID );

        ( $workHrsStart, $workHrsEnd, $costcode ) = $Config::dba->process_oneline_sql("SELECT  WORKHOURSSTARTS, WORKHOURSENDS,COST_CENTER FROM USERS WHERE id=?", [ $userID ]);

		( $adjContractualHours) = $Config::dba->process_oneline_sql( "SELECT CASE
																		WHEN DATEDIFF(\'$selectedDate\', ACH.REC_TIMESTAMP) >= 1
																			THEN '' 
																		ELSE 
																			CONCAT(ACH.ADJUSTEDHOURSSTARTS,  ',', ACH.ADJUSTEDHOURSENDS)
        																END AS ADJUSTEDCONSTRACTUALHOURS
																	FROM adjustedcontractualhours ACH 
																	WHERE userid = ? ;", [ $userID ]);
		( $adjContractualHrsStart, $adjContractualHrsEnd ) = ( split( /,/, $adjContractualHours ) ) [ 0, 1 ];
	};

    if ( $@ ) {
        my $error = $@;
        $userID = 'undef' unless defined $userID;
        cluck "db_getResourceContractualHours: $error for userID $userID";
      	return 0; 
    }

    return $workHrsStart, $workHrsEnd, $costcode, $adjContractualHrsStart, $adjContractualHrsEnd;
} #db_getResourceContractualHours

#############################################################################
#db_editResourceContractualHours: To update the resource contractual hours.
#############################################################################

sub db_editResourceContractualHours {

	my $userID							= $_[0];
	my $adjResourceContractualHrsStart	= $_[1];
	my $adjResourceContractualHrsEnds	= $_[2];

	eval {
        Validate::userid( $userID );

		my ( $chekcUsrIDExitst ) = $Config::dba->process_oneline_sql("SELECT  1 FROM adjustedcontractualhours WHERE USERID=?", [ $userID ]);

		if ( $chekcUsrIDExitst == 1 ) {
        	$Config::dba->process_sql( "UPDATE adjustedcontractualhours 
										SET ADJUSTEDHOURSSTARTS = ?, ADJUSTEDHOURSENDS = ?, REC_TIMESTAMP = now() 
										WHERE USERID=?", [ $adjResourceContractualHrsStart, $adjResourceContractualHrsEnds, $userID ] );
		} else {
			$Config::dba->process_sql("	INSERT INTO adjustedcontractualhours( USERID, ADJUSTEDHOURSSTARTS, ADJUSTEDHOURSENDS, REC_TIMESTAMP ) 
												VALUES( ?, ?, ?, now() )", [ $userID, $adjResourceContractualHrsStart, $adjResourceContractualHrsEnds ]);
		}
    };

    if ( $@ ) {
        cluck "db_editResourceContractualHours: userid: " . ( $userID || 0 ) . ' ' . $@;
        return 0;
    }
    return 1;
	
} #db_editResourceContractualHours

##############################################################################
#db_getmessagedays
#############################################################################
sub db_getMessagedays {

	my ( $userid ) = @_;
        Validate::userid( $userid );        
        my ($view_messagedays) = $Config::dba->process_oneline_sql("SELECT nummessagedays FROM users WHERE id=?",[ $userid ]);
        return ( $view_messagedays );
}
#############################################################################
#db_update_message_day
#############################################################################
sub db_update_message_day {

	 my ($userid, $view_messageday) = @_;

        eval {
                Validate::userid( $userid );
                die "Invalid message_day" unless ( ( defined $view_messageday ) && ( looks_like_number( $view_messageday ) ) );

                $Config::dba->process_sql("UPDATE users SET nummessagedays=? WHERE id=?", [ $view_messageday, $userid ]);
        };
        if ( $@ ) {
                cluck "db_update_message_day: $@";
                return 0;
        }
        return 1;
}

sub db_getStatusNoteFlag {
    my ( $userid ) = @_;

    my $status_note_flag;
    eval {
        Validate::userid( $userid );

        ($status_note_flag) = $Config::dba->process_oneline_sql("SELECT status_note_flag FROM users WHERE id=?",[ $userid ]);
    };
    if ( $@ ) {
        cluck "db_get_autorefresh_duration: userid " . ( $userid || 0 ) . ' ' . $@;
    }
    return ( $status_note_flag || 0 );
}

#############################################################################
#db_update_message_day
#############################################################################
sub db_updateStatusNoteFlag {

	my ($userid, $currStatusNoteFlag ) = @_;

	eval {
		Validate::userid( $userid );
		die "Invalid message_day" unless ( ( defined $currStatusNoteFlag ) && ( looks_like_number( $currStatusNoteFlag ) ) );

		$Config::dba->process_sql("UPDATE users SET status_note_flag = ? WHERE id = ?", [ $currStatusNoteFlag, $userid ]);
	};
	if ( $@ ) {
		cluck "db_update_message_day: $@";
		return 0;
	}
}


#get users which that are assigned to Smoke_and_mirrors, Smoke_at_Tag, SMoke_and_mirrors_New_York
sub db_getUserListBasedOpCo { 
	my ($opcoid)	=	@_;
	my $users;
	eval {
		$users = $Config::dba->hashed_process_sql(
									"SELECT CONCAT(fname,' ',lname,', ',username) as user_name 
									FROM users WHERE status = 1 AND id IN (SELECT distinct userid FROM useropcos WHERE opcoid = ? )", 
									[ $opcoid ]);
	};
    if ( $@ ) {
        cluck "db_getUserListBasedOpCo: " . $@; # Stack trace but dont rethrow exception
    }
    return $users ? @$users : ();
}

##############################################################################
#      end of file 
#
##############################################################################
1;

