#!/usr/local/bin/perl
package Companies;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use Template;

use lib 'modules';

require 'databaseProducts.pl';
require "databaseCompanies.pl";
require "general.pl";

##############################################################################
# the template object 
##############################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";

my %companyTypes = (
	Vendor 		=> '1',
	Printer 	=> '2',
	Other 		=> '3',
);

##############################################################################
#       ListCompanies
#
##############################################################################
sub ListCompanies {
	my $query 			= $_[0];
	my $session 		= $_[1];
 	my $sidemenu   = $query->param( "sidemenu" );
    my $sidesubmenu= $query->param( "sidesubmenu" );

	my @companies		= getCompanies();
	
	my $vars = {
		companies		=> \@companies,
        sidemenu    => $sidemenu,
        sidesubmenu => $sidesubmenu,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/listcompanies.html', $vars )
    		|| die $tt->error(), "\n";
}


##############################################################################
#		EditCompany
#
##############################################################################
sub EditCompany {
	my $query 			= $_[0];
	my $session 		= $_[1];
	my $id				= $query->param( "id" );
	
	my ( @company, @companyTypes );
	my ( $typeid, $name, $address1, $address2, $towncity, $postcode, $telnum, $formPosted, %error );

	# validate input
	if ( $id && (! looks_like_number( $id )) ) {
		General::NoAccess();
		return;
	}

	$formPosted 		= $query->param( "formPosted" );

	if ( $formPosted eq "yes" ) { 	#get from form
		$typeid 			= $query->param( "typeid" );
		$name 				= General::htmlEditFormat( $query->param( "name" ) );
		$address1			= General::htmlEditFormat( $query->param( "address1" ) );
		$address2			= General::htmlEditFormat( $query->param( "address2" ) );
		$towncity			= General::htmlEditFormat( $query->param( "towncity" ) );
		$postcode			= General::htmlEditFormat( $query->param( "postcode" ) );
		$telnum				= General::htmlEditFormat( $query->param( "telnum" ) );
	} else {
		my @company			= getCompany( $id );
		
		if ( scalar( @company ) == 0 ) {
			General::NoAccess();
			return;
		}
		
		$typeid				= $company[0]{ typeid };
		$name				= $company[0]{ name };
		$address1			= $company[0]{ address1 };
		$address2			= $company[0]{ address2 };
		$towncity			= $company[0]{ towncity };
		$postcode			= $company[0]{ postcode };
		$telnum				= $company[0]{ telnum };
	}

	if ( $formPosted eq "yes" ) { #update the Operating Company 
		#validate
		%error = validateCompany( $typeid, $name, $address1, $address2, $towncity, $postcode, $telnum );

		# all ok? Update
		if ( $error{ oktogo } == 1 ) {
			ECMDBCompanies::updateCompany( $id, $typeid, $name, $address1, $address2, $towncity, $postcode, $telnum );
		}
	}
	
	#get company types
	@companyTypes = getCompanyTypes();
	
	my $vars = {
		companyTypes	=> \@companyTypes,
		id				=> $id,
 		typeid 			=> $typeid, 
 		name			=> $name, 
 		address1		=> $address1, 
 		address2		=> $address2, 
 		towncity		=> $towncity, 
 		postcode		=> $postcode, 
 		telnum			=> $telnum, 
		error 			=> \%error,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/editcompany.html', $vars )
    		|| die $tt->error(), "\n";
}


##############################################################################
#		AddCompany
#
##############################################################################
sub AddCompany {
	my $query		= $_[0];
	my $session		= $_[1];

	my $formPosted		= $query->param( "formPosted" ) ||'';
	my $typeid		= $query->param( "typeid" );
	my $name		= General::htmlEditFormat( $query->param( "name" ) );
	my $address1		= General::htmlEditFormat( $query->param( "address1" ) );
	my $address2		= General::htmlEditFormat( $query->param( "address2" ) );
	my $towncity		= General::htmlEditFormat( $query->param( "towncity" ) );
	my $postcode		= General::htmlEditFormat( $query->param( "postcode" ) );
	my $telnum		= General::htmlEditFormat( $query->param( "telnum" ) );
	my ( %error, $vars, @companyTypes );
 	my $sidemenu		= $query->param( "sidemenu" );
	my $sidesubmenu		= $query->param( "sidesubmenu" );

	if ( $formPosted eq "yes" ) { #update the Operating Company 
		#validate
		%error = validateCompany( $typeid, $name, $address1, $address2, $towncity, $postcode, $telnum );

		# all ok? Update
		if ( $error{ oktogo } == 1 ) {
			ECMDBCompanies::addCompany($typeid, $name, $address1, $address2, $towncity, $postcode, $telnum );

			General::setHeaderVars( $vars, $session );

			$tt->process( 'ecm_html/companyaddedsuccess.html', $vars )
    			|| die $tt->error(), "\n";
			return;

		}
	}

	#get company types
	@companyTypes = getCompanyTypes();
	
	$vars = {
		companyTypes	=> \@companyTypes,
 		typeid 		=> $typeid, 
 		name		=> $name, 
 		address1	=> $address1, 
 		address2	=> $address2, 
 		towncity	=> $towncity, 
 		postcode	=> $postcode, 
 		telnum		=> $telnum, 
		error 		=> \%error,
		sidemenu	=> $sidemenu,
		sidesubmenu	=> $sidesubmenu,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/addcompany.html', $vars )
    		|| die $tt->error(), "\n";

}

##############################################################################
#       validateCompany
#
##############################################################################
sub validateCompany {
	my %error;
 
	$error{ oktogo } = 1;
 	my ( $typeid, $name, $address1, $address2, $towncity, $postcode, $telnum );
 	$typeid 	= $_[0]; 
 	$name 		= $_[1];
 	$address1 	= $_[2];
 	$address2 	= $_[3];
 	$towncity 	= $_[4];
 	$postcode 	= $_[5];
 	$telnum 	= $_[6];

	# validate input
	if ( $typeid && (! looks_like_number( $typeid )) ) {
		General::NoAccess();
		return;
	}
	if ( $name && (! General::isAlphaNumeric( $name )) ) {
		$error{ name } = "Name must be letters and numbers only.";
		$error{ oktogo } = 0;
	}
	if ( $address1 && (! General::isAlphaNumeric( $address1 )) ) {
		print "address1: $address1 error";
		$error{ address1 } = "Address must be letters and numbers only.";
		$error{ oktogo } = 0;
	}
	if ( $address2 && (! General::isAlphaNumeric( $address2 )) ) {
		$error{ address2 } = "Address must be letters and numbers only.";
		$error{ oktogo } = 0;
	}
	if ( $towncity && (! General::isAlphaNumeric( $towncity )) ) {
		$error{ towncity } = "Town/City must be letters and numbers only.";
		$error{ oktogo } = 0;
	}
	if ( $postcode && (! General::isAlphaNumeric( $postcode )) ) {
		$error{ postcode } = "Postcode must be letters and numbers only.";
		$error{ oktogo } = 0;
	}
	if ( $telnum && (! General::isAlphaNumeric( $telnum )) ) {
		$error{ telnum } = "Telnum must be letters and numbers only.";
		$error{ oktogo } = 0;
	}
	
	if ( length( $name ) < 1 ) {
		$error{ name } = "Company name is required.";
		$error{ oktogo } = 0;
	}
	
	if ( length( $name ) < 3 ) {
		$error{ name } = "Company name must be at least 3 characters.";
		$error{ oktogo } = 0;
	}
	

	return %error;
}


##############################################################################
#       getCompany ()
#
##############################################################################
sub getCompany {
	my $id			= $_[0];
	
	if ( $id && (! looks_like_number( $id )) ) {
		General::NoAccess();
		return;
	}
	
	return ECMDBCompanies::getCompany( $id );
}

##############################################################################
#       getCompanies()
#
##############################################################################
sub getCompanies {
	my $companyType = $_[0];

	if ( $companyType && (! looks_like_number( $companyType )) ) {
		General::NoAccess();
		return;
	}

	return ECMDBCompanies::getCompanies( $companyType );
}

##############################################################################
#       getAllVendors()
#
##############################################################################
sub getAllVendors {
	my $companyType	= $companyTypes{'Vendor'};

	return ECMDBCompanies::getCompanies( $companyType );
}


##############################################################################
#       getCompanyTypes()
#
##############################################################################
sub getCompanyTypes {
	return ECMDBCompanies::getCompanyTypes();
}

##############################################################################
##      isProductsEnabled()
##
##############################################################################
sub isProductsEnabled {
    my $customerid = $_[0];

	return 0 if $customerid && !looks_like_number($customerid);

    return ECMDBProducts::isProductsEnabled( $customerid );
}





##############################################################################
##############################################################################
1;
