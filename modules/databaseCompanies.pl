#!/usr/local/bin/perl
package ECMDBCompanies;

use strict;
use Scalar::Util qw( looks_like_number );

use lib 'modules';

require "config.pl";

my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");

my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");


##############################################################################
#       getOperatingCompanies
#      
##############################################################################
sub getCompaniesXXX {
	
	my @opcos;
	my ( $opcoid, $opconame );

	my $rows = $Config::dba->hashed_process_sql("SELECT opcoid, opconame FROM OPERATINGCOMPANY ORDER BY opconame LIMIT 100 ");
	return $rows ? @$rows : ();
}

##############################################################################
#       getCompany
#      
##############################################################################
sub getCompany {
	my @company;
	
	my $id = $_[0];
	my ( $typeid, $name, $address1, $address2, $towncity, $postcode, $telnum );
	
	# validate input
	if ( $id && (! looks_like_number( $id )) ) {
		return @company;
	}
	
	my $rows = $Config::dba->hashed_process_sql("SELECT 	typeid, name, address1, address2, towncity, postcode, telnum  
				FROM 	COMPANIES 
				WHERE 	id = ?", [ $id ]);
	return $rows ? @$rows : ();
}

##############################################################################
#		updateCompany
#      
##############################################################################
sub updateCompany {
	my ( $id, $typeid, $name, $address1, $address2, $towncity, $postcode, $telnum );

	$id 				= $_[0];
	$typeid 			= $_[1];
	$name 				= $_[2]; 
	$address1 			= $_[3]; 
	$address2 			= $_[4]; 
	$towncity	 		= $_[5]; 
	$postcode 			= $_[6]; 
	$telnum 			= $_[7]; 

	# validate input
	if ( $id && (! looks_like_number( $id )) ) {
		return 0;
	}
	
	$Config::dba->process_sql("UPDATE COMPANIES SET 
					typeid		= ?,
					name		= ?,
					address1	= ?,
					address2	= ?,
					towncity	= ?,
					postcode	= ?,
					telnum		= ?
				WHERE id = ?", [ $typeid, $name, $address1, $address2, $towncity, $postcode, $telnum, $id ]);

	return 1;
}

##############################################################################
#       addCompany
#      
##############################################################################
sub addCompany {

	my ( $typeid, $name, $address1, $address2, $towncity, $postcode, $telnum );

	$typeid 	= $_[0];
	$name 		= $_[1];
	$address1 	= $_[2];
	$address2 	= $_[3];
	$towncity 	= $_[4];
	$postcode 	= $_[5];
	$telnum 	= $_[6];

	# validate input
	if ( $typeid && (! looks_like_number( $typeid )) ) {
		return 0;
	}

	$Config::dba->process_sql("INSERT INTO COMPANIES ( typeid, name, address1, address2, towncity, postcode, telnum, rec_timestamp ) 
		VALUES ( ?, ?, ?, ?, ?, ?, ?, now() )",
		[ $typeid, $name, $address1, $address2, $towncity, $postcode, $telnum ]);

	return 1;
}

##############################################################################
#       getCompaniesXXXX
#      
# 		todo! why is this overwritten here????
# 	deprecate this asap when poss.
##############################################################################
sub getCompanies {
	my $companyType = $_[0];

	my $companyClause = '';
	
	$companyClause = " WHERE typeid = $companyType " if ( defined $companyType ) && ($companyType != 0);
	
	my $rows = $Config::dba->hashed_process_sql("SELECT id, name 
				FROM COMPANIES 
				$companyClause
				ORDER BY NAME ");
	return $rows ? @$rows : ();
}

##############################################################################
#       getCompanyTypes()
#
##############################################################################
sub getCompanyTypes {
	my $rows = $Config::dba->hashed_process_sql("SELECT id, type FROM COMPANYTYPE");
	return $rows ? @$rows : ();
}


##############################################################################
#      db_getVendors 
# This returns a list of users who are vendors (role=2) and also the company 
# they are associated with. 
#
##############################################################################
sub db_getVendors {
	my $opcoid = $_[0];

	my @users;
	my ( $id, $companyname, $fname, $lname, $username, $email, $rec_timestamp );

	# validate input
	if ( $opcoid && (! looks_like_number( $opcoid )) ) {
		return @users;
	}

	my $rows = $Config::dba->hashed_process_sql("SELECT 	u.id, c.name AS companyname, u.fname, u.lname, u.username, u.email, u.rec_timestamp AS timestamp
				FROM 
					USERS u
				INNER JOIN COMPANIES c
					ON c.id = u.companyid
				INNER JOIN OPERATINGCOMPANYVENDOR ocv 
					ON ocv.companyid = c.id 
				WHERE u.status != 0
				AND u.role = 2
				AND ocv.opcoid = ? 
				ORDER BY c.name ", [ $opcoid ]);
	return $rows ? @$rows : ();
}

##############################################################################
#       end of file
#      
##############################################################################

1;
