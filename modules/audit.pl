#!/usr/local/bin/perl
package Audit;

use strict;
use warnings;

use Template;

use lib 'modules';

require "databaseAudit.pl";
require "general.pl";

##################################################################
# the template object 
##################################################################
my $tt = Template->new({
	RELATIVE => 1,
}) || die "$Template::ERROR\n";

##############################################################################
#       auditLog ()
#
##############################################################################
sub auditLog {
	my ( $userid, $tablename, $rowid, $changelog );

	$userid 	= $_[0];
	$tablename 	= $_[1];
	$rowid		= $_[2];
	$changelog	= $_[3];

	ECMDBAudit::db_auditLog( $userid, $tablename, $rowid, $changelog );
}



##############################################################################
#       auditLogWS ()
#
##############################################################################
sub auditLogWS {
	my $query 	= $_[0];

	my $jobid 	= $query->param( "jobid" );	

	ECMDBAudit::db_auditLog( 2, "TRACKER3PLUS", $jobid, "Test message" );
}


##############################################################################
#       ShowLogs ()
#
##############################################################################
sub ShowLogs {
	my $query 	= $_[0];
	my $session = $_[1];
	my $sidemenu = $query->param( "sidemenu" );
	my $sidesubmenu = $query->param( "sidesubmenu" );
	my $roleid = $session->param( 'roleid' );

	#get logs from do
	my @logs;

	@logs = ECMDBAudit::db_getLogs();
	
	#display logs

	my $vars = {
		logs 	=> 	\@logs,
		sidemenu    => $sidemenu,
		sidesubmenu => $sidesubmenu,
		roleid       =>$roleid,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process('ecm_html/logs.html', $vars )
    		|| die $tt->error(), "\n";
}

##############################################################################
##############################################################################
1;
