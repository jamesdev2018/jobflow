package ECMDBMasters;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );

# Home grown
use lib 'modules';

require "config.pl";

##############################################################################
#      db_getCustomerPermissions - seems to return just a role name for userid
#
##############################################################################
sub db_getCustomerPermissions {
	my ( $userid ) = @_;

	my ( $rolename ) = $Config::dba->process_oneline_sql(
		"SELECT r.role FROM ROLES AS r INNER JOIN USERS AS u ON u.role=r.id WHERE u.id=?", [ $userid ] );

	return $rolename;
}

##############################################################################
#      db_getVisibleCustomers - get distinct list of customer names for userid/opcoid combination
#	Each of the customer ids on customeruser is inherenty associated with a specific opcoid.
#
##############################################################################
sub db_getVisibleCustomers {
	my ( $userid, $opcoid ) = @_;
	
	my $rows = $Config::dba->hashed_process_sql( 
		"SELECT DISTINCT customer FROM CUSTOMERS AS c
		  INNER JOIN CUSTOMERUSER AS cu ON cu.customerid=c.id
		  INNER JOIN OPERATINGCOMPANY AS oc ON oc.opcoid=c.opcoid
		  WHERE cu.userid = ? AND oc.opcoid = ?", [ $userid, $opcoid ] );

	my @customers;
	if ( defined $rows ) {
		foreach my $r ( @$rows ) {
			push( @customers, $r->{customer} );
		}
	}
	return \@customers;
}

##############################################################################
#      db_getUserPrimaryOpco - get opco name given userid
#
##############################################################################
sub db_getUserPrimaryOpco {
	my ( $userid ) = @_;
	
	my ( $opcoid, $opconame ) = $Config::dba->process_oneline_sql( 
		"SELECT u.opcoid, oc.opconame FROM USERS AS u INNER JOIN OPERATINGCOMPANY AS oc ON oc.opcoid=u.opcoid WHERE u.id = ?",
		[ $userid ] );

	return ( $opcoid, $opconame );
}

1;
