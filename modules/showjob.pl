#!/usr/local/bin/perl
package ShowJob;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use File::Path;
use File::Copy;
use LWP::Simple;
use JSON qw( decode_json );
use Net::FTP::File;
use URI::Escape;
use URI::URL;

use Template;
use Data::UUID;
use XML::Simple;
use Data::Dumper;
use Date::Simple ('date', 'today');
use JSON;
use POSIX qw/strftime/;
use Time::Local;
use Encode qw(decode encode);
use IO::Compress::Zip qw(zip $ZipError);
use Cwd;
use POSIX qw( strftime );
use Image::Size;
use Carp qw/carp/;

# Home grown
use lib 'modules';

use XMLResponse qw ( :statuses );
use CTSConstants;
use Validate;



require "config.pl";
require "general.pl";
require "jobpath.pl";
require "twist.pl"; # getTwistServer
require 'databaseJobBundle.pl';

require "databaseJob.pl";
require "databaseOpcos.pl";
require "databaseUser.pl";
require "databaseImageworkflow.pl";
require 'databaseMessages.pl';
require 'databaseProducts.pl';
require 'databaseAudit.pl';

require "sendjobs.pl";
require "jf_wsapi.pl"; # JF_WSAPI package
require "Watchlist.pl";
require "digitalassets.pl";
require "imageworkflow.pl";
require "streamhelper.pl";
require 'databaseCustomerUser.pl';

my %preflightstatuses = (
	InProgress 		=> '1',
	Passed 			=> '2',
	Warning			=> '3',
	Failed			=> '4',
	None			=> '5',
	Dispatched		=> '6',
);

my %qcstatuses = (
	0	 		=> 'None',
	1 			=> 'QC Required',
	2			=> 'QC Approved',
	3			=> 'QC Rejected',
);

my %qcnotify = (
    0           => 'None',
    1           => 'QCREQUIRE',
    2           => 'QCAPPROVE',
    3           => 'QCREJECT',
);

my $cmdurl			= Config::getConfig("cmdurl");
my $mongooseServer	= Config::getConfig("mongooseserver" );
my $upload_dir		= Config::getConfig("uploaddirectory")||"";     #directory
my $ffmpegCmd		= Config::getConfig("ffmpegCmd" );
my $smtp_gateway	= Config::getConfig("smtp_gateway");
my $documentAssetsFolder = Config::getConfig("documentAssetsFolder" );
my $digitAssetsFolder = Config::getConfig("digitalAssetsFolder" );
my $digital_convert_formats = Config::getConfig("digital_convert")||"";
my $document_upload_url = Config::getConfig("document_upload_url");
my $digital_upload_url 	= Config::getConfig("digital_upload_url");
my $broadcast_opcos		= Config::getConfig("broadcast_opcos"); #opcoids for Smoke_and_mirrors, Smoke_at_Tag, SMoke_and_mirrors_New_York

my $assetUploadTmpDir = $upload_dir;

##################################################################
# the template object 
##################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";


##############################################################################
#       ShowJob
#
##############################################################################
sub Showjob {

	my ( $query, $session ) = @_;

	my $action          = $query->param("action");
	my $subaction		= $query->param( "subaction" ) || "";
	my $mode			= $query->param( "mode" ) ||'';
	my $myjob			= $query->param( "jobid" );
	my $fileuploaded	= $query->param( "fileuploaded" ) || 'no';
	my $dispatchdate	= $query->param( "dispatchdatetime" ) ||'';

	my $opcoid			= $session->param( 'opcoid' );
	my $roleid			= $session->param( 'roleid' );
	my $userid			= $session->param( 'userid' );	
	my $username		= $session->param( 'username' );
	my $relaxroles		= $session->param( 'relaxroles' );

	my @productDetails; 
	my $commentsData;
	my @commentslist;
	my @taskComments;
	my $active_comments;
	my $docMaxSize;
	my $digitalMaxSize;
	my @documentDetail;
	my $docExts_ref;
	my $documentUploadExts;
	my @filedetails;
	my $digitalExts_ref ;
	my $digitStaticExts_ref;
	my $digitalUploadExts;
	my $digitalStaticUploadExts;
	my $digitalAssetReviewFlag;
	my $assetDownloadFlag;
	my $getcampaignflag;
	my $updatecampaignflag;
	$session->param( 'highlightjob', $myjob ) if defined $myjob;

	my $errorMessage	= 0; # Set to 1 if error
	my $savemsg			= "Job saved OK";
	my $sendToClient	= "off";
	my $uploadmsg		= "File Uploaded successfully";

	my $subactions = {
		'showcomments' => \&showComments,
	};
	foreach my $key ( keys %$subactions ) {
		my $qpv = $query->param( $key ) ||'';
		if ( $qpv =~ /yes/i ) {
			my $func = $subactions->{ $key };
			eval {
				$func->($query, $session);
			};
			if ( $@ ) {
				my $error = $@;
				$error =~ s/ at .*//gs;
				carp "ShowJob::$key: error, $error";
			}
			return;
		}
	}

	# Here if no implicit subaction handled (ie one of a set of query names which is set, such as findAssets)

	my $vars;

	eval {
		Validate::job_number( $myjob );

		## PD-3159 - Previously for action checkcampaign they have passed the job number in new param 'jobnum' which was null when the other action like split was called. So it was not hanlded proper.
		## Now i have changed the 'jobnum' param to 'jobid' in ajax so that it is common as well as made this update call only when action 'checkcampaign' is done. Other times this update is not required.
		## Also i have moved the declaration part 'campaignflag' from above to inside here so that i happens only for this action.

		my ( $jobPath, $jobopcoid, $customerid ) = JobPath::getJobPath( $myjob ); # jobPath doesnt have trailing /

		my $customer		= ECMDBJob::db_getCustomer( $myjob ); # SELECT agency FROM TRACKER3 WHERE job_number=?

		my $useassetrepo	= ECMDBOpcos::db_useassetrepositorycustomer( $customerid ); # Used still ??

		my @job = ECMDBJob::db_getJob( $myjob );

		my ( $isAutoCheckin, $isenabledelete, $isenableproducts, $isenable_one2edit ) = $Config::dba->process_oneline_sql(
			"SELECT autocheckin, enabledelete, enableproducts, enable_one2edit FROM customers WHERE id = ?", [ $customerid ] );
		my $task_level	=	ECMDBProducts::checkJobLevel( $myjob ); # to identify the given job is working by job/task level

		$isAutoCheckin ||= 0;  # This column is NULLable, dont know why

		#my $isenableproducts	= ECMDBProducts::isProductsEnabled( $customerid );
		#my $isAutoCheckin		= ECMDBOpcos::isAutoCheckin( $opcoid, $customer );
		#my $isenabledelete		= ECMDBJob::isDeleteEnabled( $customerid, $jobopcoid );

		my ( $publishing, $jobFolderEnabled, $isJobAssignment, $isLocalUpload, $isThumbView, $isqcenabled, $viewhires, $creativeenabled ) = $Config::dba->process_oneline_sql( 
			"SELECT publishing, jobfolderenabled, jobassignment, enablelocalupload, thumbview, qcenabled, viewhighres, creative FROM operatingcompany WHERE opcoid = ?", [ $jobopcoid ] );

		# All but a few columns on operatingcompany are NULLable, the above ones are not.
		my $imageworkflow	= ECMDBOpcos::db_isOpcoImageWorkFlow( $jobopcoid ); # SELECT COUNT(1) FROM imageworkflow WHERE opcoid = ?

		my $userinteractive	= ECMDBUser::db_interactive($userid);
		my $openFolderPath 	= ECMDBJob::db_openFolderPath( $myjob );
		my $viewrenderedpdf	= ECMDBJob::db_viewrenderpdf( $myjob );
		my @languages		= ECMDBJob::db_languagebycountry( $myjob );

		my @qcstatuses	= ECMDBJob::db_getQCStatuses();

		my @jobNotes	= ECMDBJob::getJobNotes( $myjob );

		my $workedon	= ECMDBJob::isJobBeingWorkedOn( $myjob );
		my $workedonproducts = ECMDBJob::isJobBeingWorkedOnForProdcuts( $myjob );

		my $isCheckedOut = ECMDBJob::isCheckedOut( $myjob );

		my $qcstatus = $job[0]->{"qcstatus"};
		my $creative = $job[0]->{"creative"};
		my $max_version = ECMDBProducts::db_getMaxVersion( $myjob );

		my $ESCompareID         = ECMDBJob::db_getESCompareIDForJob( $myjob );
		my $canoperatorupload = 0;            # upload not done from here, see upload.pl
		my $rnd_no = $session->param( 'rnd_no' ) || '';

		if ( $fileuploaded eq "yes" ) { #post has been done ?? could use if mode eq "json"

			if ( $isqcenabled ) {
                                #when QC Status drop-down is disabled in client-side split window, then param-qcstatus value will not be present in query
                                #so we are not updating the qc status & do nothing scope will be executed.
				$qcstatus = $query->param( "qcstatus" );

				die "Invalid QC status" if defined $qcstatus && ! looks_like_number( $qcstatus );

				if (! defined $qcstatus){
					#do nothing if not provided.									
				}elsif ( $qcstatus == 1 ) { # Required

					ECMDBJob::db_lockJob ( $myjob, 1 );
					ECMDBJob::db_showAssets ( $myjob, 0 );

					# set job status to 'C' and unassign job
					ECMDBJob::markJobComplete( $myjob, $userid ); 

				} elsif ( $qcstatus == 2 ) { # Approved

					ECMDBJob::db_lockJob ( $myjob, 1 );
					ECMDBJob::db_showAssets ( $myjob, 1 );

					# unassign job
					ECMDBJob::unassignJob( $myjob );

					{ # - already obtained $isAutoCheckin above, could remove this
						no warnings 'once';
						my ( $isAutoCheckin ) = $Config::dba->process_oneline_sql( "SELECT autocheckin FROM customers WHERE id = ?", [ $customerid ] );
					}

					# Handle auto checkin 

					my $isCheckedOut = ECMDBJob::isCheckedOut( $myjob );	
					if ( $isAutoCheckin == 1 && $isCheckedOut ) { # if autocheckin enabled AND job is checked out
						requestJobCheckIn( $myjob );
						print "Checking in requested for: $myjob, cust: $customer<br>" unless ( $mode eq "json" );
					}

				} elsif ( $qcstatus == 3 ) { # Rejected

					ECMDBJob::db_lockJob ( $myjob, 0 );		# if qc is Rej - UNLOCK	
					ECMDBJob::db_showAssets ( $myjob, 0 );
				}

				# ONLY update the QC status if it is defined and not 0

				if ( ( defined $qcstatus ) && ( looks_like_number( $qcstatus ) ) ) { 

					ECMDBJob::db_updateJob( $myjob, $qcstatus ); # Use scalar form of 2nd arg as its QC status
					ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $myjob, "Job saved and updated: QC Status: $qcstatuses{ $qcstatus } ($qcstatus)" );

					##Watchlist does not have None status. So 0 is not allowed
					Watchlist::Watchlistnotification( $userid, $opcoid, "Job saved and updated: QC Status: $qcstatuses{ $qcstatus } ($qcstatus)", $myjob, $qcnotify{$qcstatus} ) if ( $qcstatus != 0 );
				}

			} # end if qcenabled

			# Dispatch date

			my ( $existing_dispatch_date ) = $Config::dba->process_oneline_sql(
					"SELECT dispatchdate FROM tracker3plus WHERE job_number = ?", [ $myjob ] );

			if ( $dispatchdate ) { 
				if ( ( ! defined $existing_dispatch_date ) || ( $existing_dispatch_date ne $dispatchdate ) ) {
					ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $myjob, "Dispatch date set to '$dispatchdate'" );
					ECMDBJob::db_updateJob( $myjob, { dispatchdate => $dispatchdate } );
				}
			} else { # Has the user cleared the existing dispatch date ?
				if ( defined $existing_dispatch_date ) {
					ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $myjob, "Dispatch date cleared, was '$existing_dispatch_date'" );
					ECMDBJob::db_updateJob( $myjob, { dispatchdate => 'NULL' } );  # would use undef otherwise
				}
			}

			# The Creative, Colour Manage and Optimise flags exist on the Upload dialog, not here


			# reset the Adgate status
			ECMDBJob::db_updateAdgateStatus( $myjob, "" );

		} # end if ( $fileuploaded eq "yes" ) meaning the form was POSTed

		# addcomment (used in ecm_html/showjob.html only)

		my $addcomment = $query->param( "addcomment" ) || 'no';
		if ( $addcomment eq "yes" ) {
			my $reason;
			my $comment   = $query->param("comment") ||'';
			my $commentid = $query->param("commentid") || 0;
			my $commenttype	= ECMDBJob::getCommentType( $commentid );
			if ($commenttype eq "R") { $reason = "Rejected"; } else { $reason = "Advisory"; }
			$comment .= " (".$reason.")";
			my $tmp = ECMDBJob::addComment( $myjob, $userid, $commentid, $comment );

			my $sendToClient = $query->param( "sendToClient" )||'off';
			if ( $sendToClient eq "on" ) {
				# write confirm xml to archant - that we have received and are processing this job
				# get the agency ref - get the oba - needed to build filename
				my $urnnumber 	= ECMDBJob::getAgencyRef( $myjob );
				my $obanumber 	= ECMDBJob::getOBANumber( $myjob );
				my $commenttext	= ECMDBJob::getCommentText( $commentid );
				my $filename;
				if ( $commenttype eq "R" ) {
					$filename = XMLResponse::write_status( "/temp/archantnotify/$obanumber-$urnnumber-rejected.xml", 
									$urnnumber, $obanumber, XMLResponse::STATUS_REJECTED, "$commenttext # $comment" );
				} else {
					$filename = XMLResponse::write_status( "/temp/archantnotify/$obanumber-$urnnumber-advisory.xml", 
									$urnnumber, $obanumber, XMLResponse::STATUS_ADVISORY, "$commenttext # $comment" );
				}

				SendJobs::sendArchantStatusUpdate( $filename );
			}
		}

		# get job notes *after* we add a comment
		@jobNotes	= ECMDBJob::getJobNotes( $myjob );
		#my @logs	= ECMDBAudit::db_getLogs( $myjob );

		my $displaymsg = $savemsg;

		if($subaction eq "Brief")
		{
			#$commentsData    = ECMDBJob::db_getCommetsData( $myjob );
			@commentslist = ECMDBJob::db_getCommentsList();

	$job[0]->{brief} =~ s/\n/<br>/g if exists $job[0]->{brief};     #FORMAT THE BRIEF SO NEWLINES WORK PROPERLY IN THE BROWSER
	
	@taskComments = ECMDBJob::getJobCommentHeadersFormatted( $myjob );
	$job[0]->{taskcomments} = \@taskComments;

	$active_comments = ECMDBJob::getJobCommentActiveCount($myjob, 0);
	$job[0]->{task_active_comments} = $active_comments;
	@productDetails  = ECMDBProducts::db_productDetails( $myjob );

	$vars = {
		myjob           => \@job,
		commentslist    => \@commentslist,
		userid          => $userid,
		roleid          => $roleid,
		productDetails  => \@productDetails,
		isenabledelete  => $isenabledelete,
		tasklevel		=> $task_level
	}
}
elsif($subaction eq "Document")
{
	( $docMaxSize, $digitalMaxSize ) = ECMDBJob::db_getDocMaxSize( $customerid );  # changed databaseJob to return array of scalars
	@documentDetail  = ECMDBJob::db_documentsDetails( $myjob );
	$docExts_ref     = ECMDBJob::db_getFileExtensions( "document_extension", "docext");
	$documentUploadExts      = getFileExtension( $docExts_ref, "docext" );
	$vars = {
		parentopcoid        => $jobopcoid,
		documentDetails     => \@documentDetail,
		docMaxSize          => $docMaxSize,
		documentUploadExts  => $documentUploadExts,
		documentuploadurl	=> $document_upload_url,
		rnd_no				=> $rnd_no,
		sessionid			=> $session->id,
		roleid          	=> $roleid,
		isenable_one2edit	=> $isenable_one2edit,
		tasklevel			=> $task_level
	}
}
elsif($subaction eq "Digital" || $subaction eq "DigitalCampaign")
{
	($docMaxSize, $digitalMaxSize ) = ECMDBJob::db_getDocMaxSize( $customerid );  # changed databaseJob to return array of scalars
	@filedetails         = ECMDBJob::db_filedetails( $myjob );
	$digitalExts_ref     = ECMDBJob::db_getFileExtensions( "digital_extension", "digitalext");
	$digitStaticExts_ref = ECMDBJob::db_getFileExtensions( "digital_static_extension", "staticext");
	$digitalUploadExts           = getFileExtension( $digitalExts_ref, "digitalext" );
	$digitalStaticUploadExts     = getFileExtension( $digitStaticExts_ref, "staticext" );
	$digitalAssetReviewFlag = ECMDBJob::db_getdigitalAssetCount( $myjob );
	$assetDownloadFlag   = ECMDBJob::db_getAssetDownloadFlag( $myjob );
	#Update campaign approval when user click on "Campaign Approval" checkbox.
		if($action eq "checkcampaign"){
			my $campaignflag    = $query->param( "campaignflag" ) || 0; 
			my $updatecampaignflag  = ECMDBJob::db_updateCampaignFlag( $myjob,$campaignflag );
		}
	$getcampaignflag     = ECMDBJob::db_getcampaignFlag( $myjob );
	$vars = {
		filedetails         => \@filedetails,
		userinteractive     => $userinteractive,
		digitalMaxSize      => $digitalMaxSize,
		digitalAssetReviewFlag  => $digitalAssetReviewFlag,
		assetDownlaodFlag   => $assetDownloadFlag,
		updatecampaignflag => $getcampaignflag,
		digitalUploadExts   => $digitalUploadExts,
		digitalStaticUploadExts => $digitalStaticUploadExts,
		digital_upload_url => $digital_upload_url,
		rnd_no              => $rnd_no,
		sessionid           => $session->id,
		tasklevel			=> $task_level
	}
		}
		elsif($subaction eq "Products")
		{
			@productDetails  = ECMDBProducts::db_productDetails( $myjob );
			$vars = {
				productDetails  => \@productDetails,
				isenableproducts=> $isenableproducts,		
				jobnumber    	=> $myjob,		
				tasklevel		=> $task_level
			};
		}
		else
		{
			# Default if no explicit or implicit subaction

			@productDetails  		= 	ECMDBProducts::db_productDetails( $myjob );

			#broadcast_opcos from jobflow_settings table.
			my @broadcast_opcoid	=	split(",",$broadcast_opcos);
			#If the session OpCo id exist in broadcast_opcos give the access to edit the producer in details page.
			my $access_to_edit_producer	=	grep { $_ eq $opcoid } @broadcast_opcoid; 
			my @opco_based_userlist;
			if($access_to_edit_producer)			
			{
				# PD-3564 : take the userlist based on session opco
				@opco_based_userlist	=	ECMDBUser::db_getUserListBasedOpCo($opcoid );
			}
				$vars = {
					myjob 			=> \@job,
					relaxroles		=> $relaxroles,
					#qcstatuses		=> \@qcstatuses,
					errorMessage		=> $errorMessage,
					isqcenabled		=> $isqcenabled,
					creativeenabled		=> $creativeenabled,
					viewhires		=> $viewhires,
					qcstatus		=> $qcstatus,
					roleid			=> $roleid,
					username		=> $username,
					displaymsg			=> $displaymsg,
					jobPath				=> $jobPath,
					jobFolderEnabled  	=> $jobFolderEnabled,
					openFolderPath		=> $openFolderPath,
					jobNotes			=> \@jobNotes,
					userid				=> $userid,	
					commentslist		=> \@commentslist,
					canoperatorupload	=> $canoperatorupload,
					creative			=> $creative,
					publishing			=> $publishing,
					isJobAssignment		=> $isJobAssignment,
					opcoid				=> $opcoid,
					parentopcoid		=> $jobopcoid,
					useassetrepo		=> $useassetrepo,
					isthumbview			=> $isThumbView,
					cmdurl				=> $cmdurl,
					userinteractive		=> $userinteractive,
					viewrenderedpdf		=> $viewrenderedpdf,
					isLocalUpload		=> $isLocalUpload,
					mongooseserver		=> $mongooseServer,
					escomparedocid		=> $ESCompareID,
					languages			=> \@languages,
					productDetails          => \@productDetails,
					access_to_edit_producer	=> $access_to_edit_producer,
					opco_based_userlist	=> \@opco_based_userlist,
					maxversion => $max_version,
					tasklevel			=> $task_level
				};
		}
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => $error, jobid => $myjob||'', errorMessage => $error };
		carp "ShowJob: error, $error";
	}

	if ( $mode eq "json" ){
 		my $json = encode_json ( $vars ); 
		print $json;
	 	return $json;
	}

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/showjob.html', $vars )
			|| die $tt->error(), "\n";

} # end Showjob

##############################################################################
#       updateJobStatuses
#
##############################################################################
sub updateJobStatuses {
	my ( $jobid, $userid ) = @_;

	#ka - todo: - set assetscreated = 0 / we reset this flag each time a user uploads a new file
	ECMDBJob::db_updateAssetsCreated ( $jobid, 0 ); 
	#on each new upload we reset the compare
	ECMDBJob::db_updateCompare ( $jobid, 0 ); 

	# we lock the job as whenever a user uploads a file - lets Twist again!
	ECMDBJob::db_lockJob ( $jobid, 1 );
	
	# touch the job with this user
	ECMDBJob::db_touchJob ( $jobid, $userid );
	ECMDBAudit::updateHistory( $userid, $jobid );

	# file uploaded so we set job to 'uploaded'
	# change preflight status if uploaded OK
	ECMDBJob::db_updateJobPreflightStatus( $jobid, $preflightstatuses{ InProgress } );

	# mark the OPERATOR STATUS AS 'P' (Processing) in both T3P and AssetHistory
	ECMDBJob::setOperatorJobProcessing( $userid, $jobid );

}

###########################################################################
### getFileExtension: 
###########################################################################
sub getFileExtension {
	my ( $ext_arrayRef, $columnName ) = @_;

	my @ext_array;

	if ( $ext_arrayRef ) {
        my @ext_arrayHash = @$ext_arrayRef;
        foreach my $ext_hashRef ( @ext_arrayHash ) {
            push( @ext_array, $ext_hashRef->{$columnName}  );
        }
    }

    my $extensions = join(",", @ext_array );
    $extensions  =~ s/^,//;
	return $extensions;
} #getFileExtension


##############################################################################
# requestJobCheckIn(), clone of requestJobCheckIn in jobs.pl
##############################################################################
sub requestJobCheckIn {
	my ( $jobid ) = @_;

	my $fromopcoid = ECMDBJob::db_getJobRemoteOpcoid ( $jobid );
	my $domainname = ECMDBOpcos::db_getDomainName( $fromopcoid );
	
	#  job in - CGI call

	my ( $var, $curlCommand ) = JF_WSAPI::jobflowCheckinOther( $domainname, { jobid => $jobid } );

	print "requestJobCheckIn curl: $curlCommand<br>, response '$var'<br>\n";

}


#added by vimalanand for digital upload.
sub showComments {
		my ( $query, $session ) = @_; 

        my $jobid                       = $query->param( "jobid" );
        my $mode                    	= $query->param( "mode" ) || '';
		my $rowid						= $query->param("rowid");

		my $vars;

		eval {
			Validate::job_number( $jobid );
			# die "Invalid rowid" unless ( $rowid ); can it be undef ??

        	my ($commentList, $username, $datetime) = ECMDBJob::getCommentList( $jobid, $rowid );
        	my $customerid                  = ECMDBJob::db_getCustomerIdJob( $jobid );

			$vars = {
            	commentList  => $commentList,
				username     => $username,
				datetime     => $datetime,
	        };
		};
		if ( $@ ) {
			my $error = $@;
			$error =~ s/ at .*//gs;
			$vars = { error => $error };
		}
        if ( $mode eq "json") {
                my $json = encode_json ( $vars );
                print $json;
                return $json;
        }
}

sub getDocumentFolder { 
	return Config::getConfig("documentAssetsFolder" ) || "SUPPORTING_DOCUMENTS"
}

##############################################################################
#sub getFileNameAndExt: To get the file prefix and extension. (cloned from jobs.pl)
###############################################################################

sub getFileNameAndExt {
my ( $fileName )	= @_;

my $fileNamePrefix	= undef;
my $fileExt			= undef; 

if ( $fileName =~ /^(.*)\.(.*)$/ ) {
$fileNamePrefix = $1;
$fileExt 		= $2;
}
return ( $fileNamePrefix, $fileExt );
}

sub removetempdocuments {
	my ( $query, $session ) = @_;
	my $jobid 	= $query->param("job_number");
	my $userid	= $session->param("userid");	
	#system("rm -rf /temp/".$job_number."_upload/SUPPORTING_DOCUMENTS/".$userid."/*")
	my $docTempDir              = sprintf("%s/%s_%s/%s/%s", "$upload_dir", "$jobid", "upload", "$documentAssetsFolder","$userid");
	eval{
        	if ( -d "$docTempDir" ) {
            		rmtree( "$docTempDir" );
        	}
    	};

    	if( $@ ){
		print $@;
	}
}

1;
