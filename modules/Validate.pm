#!/usr/local/bin/perl

package Validate;

use strict;
use warnings;

use Carp qw /croak confess/;
use Scalar::Util qw( looks_like_number );

sub id {  # General case of an id being an integer number
	my ( $id ) = @_;
	confess "Id is undef" unless defined $id;
	confess "Id $id invalid" if ( $id == 0 );
}

sub userid {
	my ( $userid ) = @_;
	confess "Userid is undef" unless defined $userid;
	confess "Userid $userid invalid" if ( $userid == 0 );
}

sub username {
	my ( $username ) = @_;
	confess "Username is undef" unless defined $username;
	confess "Invalid username $username" unless isValidUsername( $username );
	confess "Username must be longer than 3 characters" if ( length( $username ) < 4 );
	confess "Username too long" if  ( length( $username ) > 30 );
}

sub firstname {
	my ( $firstname ) = @_;
	confess "First name is undef" unless defined $firstname;
	confess "Invalid first name $firstname" if !isAlphaNumeric($firstname);
	confess "First name must be longer than 2 characters" if ( length( $firstname ) < 2 );
	confess "First name too long" if  ( length( $firstname ) > 20 );
}

sub lastname {
	my ( $lastname ) = @_;
	confess "Lastname is undef" unless defined $lastname;
	confess "Invalid last name $lastname" if !isAlphaNumeric($lastname);
	confess "Last name must be longer than 2 characters" if ( length( $lastname ) < 2 );
	confess "Last name too long" if  ( length( $lastname ) > 20 );
}

sub password {
	my ( $password ) = @_;
	die "Invalid password" unless ( defined $password );
	die "Password too short" if ( length( $password ) < 4 );
	die "Password too long" if ( length( $password ) > 30 );
}

sub opcoid {
	my ( $opcoid ) = @_;
	confess "opcoid is undef" unless defined $opcoid;
	confess "opcoid $opcoid invalid" if ! looks_like_number( $opcoid );
	confess "opcoid $opcoid invalid" if ( $opcoid == 0 );  # As of JF 3.1, opcoid 0 is still valid in some circumstances
}

sub opcoName {
	my ( $opconame ) = @_;
	confess "Opco name is undef" unless defined $opconame;
	confess "Opco name '$opconame' invalid" if ( $opconame eq '' ); # Could be more specific
}

sub companyid {
	my ( $companyid ) = @_;
	confess "companyid is undef" unless defined $companyid;
	confess "companyid $companyid invalid" if ! looks_like_number( $companyid );
	confess "companyid $companyid invalid" if ( $companyid == 0 ); # As of JF 3.1, companyid 0 is still valid in some circumstances
}

sub customerid {
	my ( $customerid ) = @_;
	confess "customerid is undef" unless defined $customerid;
	confess "customerid $customerid invalid" if ( ! looks_like_number( $customerid ) ||  ( $customerid == 0 ) );
}

sub customerName {
	my ( $customername ) = @_;
	die "customer name is undef" unless defined $customername;
	die "customer name '$customername' invalid" if ( $customername eq '' ); # Could be more specific
}

sub job_number {
	my ( $job_number ) = @_;
	confess "job number is undef" unless defined $job_number;
	confess "job number $job_number invalid" if ( ! looks_like_number( $job_number ) || ( $job_number == 0 ) );
}

sub email {
	my ( $email ) = @_;
	confess "email is undef" unless defined $email;
	confess "email $email is invalid" if ( isValidEmail( $email ) == 0 );
}

sub roleid {
	my ( $role_id ) = @_;
	confess "Role id is undef" unless ( defined $role_id );
	confess "Role id $role_id is invalid" unless ( looks_like_number( $role_id ) && ( $role_id > 0 ) );
}

sub employeeid {
	my ( $employee_id ) = @_;
	confess "Employee id is undef" unless ( defined $employee_id );
	confess "Employee id $employee_id is invalid" unless isAlphaNumeric( $employee_id );
}

sub mediaId {
	my ( $media_id ) = @_;
	die "Media Id is undef" unless defined $media_id;
	die "Media id '$media_id' is invalid" if ( ! looks_like_number( $media_id ) );
}

# and some general functions that just return 0 or 1

sub isValidId {
	my ( $id ) = @_;
	return 0 if ( !defined $id ) || ( ! looks_like_number( $id ) );
	return 1;
}

# isAlphaNumeric and isValidEmail lifted from modules/general.pl

##############################################################################
#       isAlphaNumeric ()
##############################################################################
sub isAlphaNumeric {
	my ($text) = @_;

	#allowing commas for multiple search
	return 0 if !defined $text;	
	return 0 if $text !~ m/^[a-zA-Z0-9. ,_-]+$/; # Old users user name created with "." . For that reason . added in regex
	
	return 1;
}

##############################################################################
#       isValidEmail ()
##############################################################################
sub isValidEmail {
	my ($email)	= @_;
	
	return 0 if !$email;
	return 0 if $email !~ m/^[-\w_.]+\@[-\w_.]+\.[a-zA-Z]{2,}$/;
	
	return 1;
}

sub isValidUsername {
	my ( $username ) = @_;

	return 0 unless defined $username;
	return 0 unless isAlphaNumeric( $username ); # TODO check existing usernames and issue of what Active Directory allows
	return 0 if ( length( $username ) < 4 || length( $username ) > 25); # TODO username is CHAR(30) so could be longer
	return 1;
}

sub lineItem {
    my ( $lineItem_id ) = @_;
    die "Invalid Line item Id is undef" unless defined $lineItem_id;
    die "Line item id '$lineItem_id' is invalid" if ( ! looks_like_number( $lineItem_id ) );
}

1;

