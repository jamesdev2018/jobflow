#!/usr/local/bin/perl
package Watchlist;

use strict;
use warnings;
use JSON;
use Scalar::Util qw( looks_like_number );

# Module contains Watchlistnotification, used in jobs.pl, sendjobs.pl
# to avoid including jobs.pl in other modules.

# Please do NOT include jobs.pl or users.pl

use lib 'modules';

use CTSConstants;

require "config.pl";
require "databaseJob.pl";
require "databaseUser.pl";
require "databaseUserNotification.pl";
require "databaseMessages.pl";
require "databaseAudit.pl";
require "Mailer.pl";

# Watchlistnotification lifted from jobs.pl (as it is used in module sendjobs.pl)

sub Watchlistnotification {

	my $userid 					= $_[0];
	my $opcoid					= $_[1];
	my $msg						= $_[2];
	my $jobid					= $_[3]; #jobid is either a scalar (single job number) or an array of ref (multiple jobs)
	my $event					= $_[4];
	my $fromemail 				= Config::getConfig("fromemail") || 'gateway.dhl.com';
	my $smtp_gateway 			= Config::getConfig("smtp_gateway") || 'jobflow@tagworldwide.com';

	my @jobLists;
	my %users;	

	if ( ref( $jobid ) =~ /^ARRAY$/i ) {
		@jobLists	= @$jobid;
	} else {
		push( @jobLists, $jobid );
	}
	
	foreach my $jobNumber ( @jobLists ) {
		my @tousers = ECMDBJob::watchuserslist($jobNumber);
		foreach my $userID ( @tousers ) {
			next if ( $userID =~ /^$userid$/ ); #dont add entry for current user.
			push( @{$users{$userID}}, $jobNumber );
		}
	}


	foreach my $touserid ( keys %users ) {
		my ( $status, $message, $email ) = ECMDBUserNotification::Getusernotification( $touserid, $opcoid, $event );
		if ( ( defined $status ) && ( $status == 1 ) ) {
			if ( $message == 1 ) {
				my $touser 					= ECMDBUser::db_getEmail($touserid);
				my $recipient_ids			= undef;
				$recipient_ids				= sprintf( "%s,%s", "$userid", "$touserid" ) if ( $userid ne "" ); #We are maintaining 'sent' message for each user.

				foreach my $jobid ( @jobLists ) {
					if ( grep {$_ eq $jobid }  @{ $users { $touserid } } ) {
						my $completeMsg	= "$jobid " . "$msg";
						my $lastInsertID = ECMDBMessages::db_insertMessage( $jobid, $userid, $touserid, $completeMsg, $recipient_ids );
						if ($lastInsertID) {
							my $rowid = $lastInsertID;
							my $tousername = ECMDBUser::db_getUsername($touserid);
							ECMDBAudit::db_auditLog( $userid, "messages", $rowid, "Message sent to $tousername" );
							ECMDBJob::db_recordJobAction($jobid, $touserid, CTSConstants::WATCHLIST_NOTIFICATION);
						}
					}
				}
			}

			if ( $email == 1 ) {
				my $n						= 80;
				my $status 					= 0;
				my $touser 					= ECMDBUser::db_getEmail($touserid);
				my $jobLists				= join( ',', @{ $users { $touserid } } ); #To get all job number list. 
				my $mailBody				= "$msg " . "$jobLists";
				my $mailSubject				= "Job Flow: WATCHLIST STATUS FOR ";

				if ( $jobLists !~ /,/ ) {
					$mailSubject		.= 'JOB: ' . $jobLists;
				} else {
					$mailSubject		.= 'JOBS: ' . $jobLists;
   					# n is maximum subject length, so say 80 chars
					$mailSubject = substr( $mailSubject , 0, $n - 3 ) . '...' if ( length( $mailSubject ) > $n );
				}

				$status = Mailer::send_mail ( $fromemail, $touser, $mailBody, "$mailSubject", { host => $smtp_gateway } );
				foreach my $jobid ( @jobLists ) {
					ECMDBJob::db_recordJobAction($jobid, $touserid, CTSConstants::WATCHLIST_NOTIFICATION);
				}
			}
		}
	}
	return \%users;
} #Watchlistnotification

# addwatchlist and stopwatchlist moved from databaseJob.pl

##############################################################################
#Adding jobnumbers to watch list 
##############################################################################
sub addwatchlist
{
	my ( $query, $session ) = @_;

    my @jobslist	= $query->multi_param( "jobslist[]" );

	my $userid		= $session->param( 'userid' );
	my $opcoid		= $session->param( 'opcoid' );

	my $joblist		= join(",",@jobslist);
    my @jobs		= split(/\,/, $joblist);
	my $mode		= "json";
	my @jobexsists;
	my @history;
	my $vars;

	eval {
		foreach my $jobid (@jobs) {

			next if ( ( ! defined $jobid ) || ( $jobid eq '' ) );
			if ( !looks_like_number($jobid) ) {
				push( @history, "Ignoring '$jobid', not a valid job number" );
				next;
			}

			# column opcoid is the only nullable column in jobnotification
			# PD-3238 removed opcoid from where condition to avoid duplicate of job number for same user

			my ( $alreadypresent )= $Config::dba->process_oneline_sql(
					"SELECT SENDFLG FROM jobnotification WHERE USERID=? AND job_number=?", [ $userid, $jobid ]);
			if ( ! defined $alreadypresent ) {
				$Config::dba->process_sql(
					"INSERT INTO jobnotification (job_number, USERID, OPCOID, SENDFLG) VALUES (?, ?, ?, 1)", [ $jobid, $userid, $opcoid ]);
			} else {
				push( @jobexsists, $jobid ); # Job already in watchlist
			}
		}
		$vars = { jobexsists => \@jobexsists, history => \@history, dummy => 1 };
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => $error, history => \@history };
	}
	if ( $mode eq "json" ){
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

##############################################################################
#Deleting jobnumbers from watch list 
##############################################################################
sub stopwatchlist {
	my ( $query, $session ) = @_;

	my @jobslist    = $query->multi_param( 'jobslist[]' );
	my $joblist     = join(",",@jobslist);
	my @jobs        = split(/\,/, $joblist);
	my $userid      = $session->param( 'userid' );

	my $vars;
	my $mode		= "json";
	my @history;

	eval {
		foreach my $jobid (@jobs) {
			next unless ( ( defined $jobid ) && ( $jobid ne '' ) );
			if ( !looks_like_number($jobid) ) {
				push( @history, "Ignored job '$jobid', not a valid job number" );
			} else {
				$Config::dba->process_sql( "DELETE FROM jobnotification WHERE userid=? AND job_number=?", [ $userid, $jobid ] );
				push( @history, "Removed job '$jobid'" );
			}
        }
		$vars = { history => \@history, dummy => 1, };
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => $error, history => \@history };
	}
	if ( $mode eq "json" ) {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

1;

