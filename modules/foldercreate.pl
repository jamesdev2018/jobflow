#!/usr/local/bin/perl

use warnings;
use strict;

sub create_default_structure
{
	#workflow => TRACKER3.workflow
	#pathtype => TRACKER3.pathtype
	#rootpath => TRACKER3.rootpath
	#dirparamlist =>
	#			customer_firstletter=?&customer=?&client=?&campaign=?&jobtitle=?&jobid_ub=?&workflow=?&jobid=?&outputs=?
	#		TRACKER3.agency
	#		TRACKER3.client
	#		TRACKER3.project
	#		TRACKER3.headline
	#		TRACKER3.workflow
	#		TRACKER3.job_number
	my ($workflow, $pathtype, $rootpath, $jobno, $agency, $client, $project, $publication, $headline, $broadcastVersioning) = @_;
	
	my %user;
	$user{customer_firstletter} = substr($agency || "", 0, 1);
	$user{customer} = $agency || "";
	$user{client} = $client || "";
	$user{campaign} = $project || "";
	$user{jobtitle} = $headline || "";
	$user{workflow} = uc($workflow || "");
	$user{workflow} =~ s/\./_/g;
	$user{publication} = $publication || "";
	$user{jobid} = $jobno;
	$user{jobid_pub} = $user{jobid} . "_" . $user{publication};
	
	my $root = $rootpath;
	if($pathtype eq '4')
	{
		$root = "/prod/SMOKE_AND_MIRRORS/";
		#$root = "/prod/dev/SMOKE_AND_MIRRORS/" if $typeDEV eq "D";
		$root = $rootpath if $rootpath;
	}
	elsif($pathtype eq '0' || $rootpath eq '')
	{
		$root = "/gpfs/STUDIOS/TAG_HQ/";
		#$root = "/home/zed/prod_test_dev/client/" if $typeDEV eq "D";
		#$root = "/home/zed/prod_test_stg/client/" if $typeDEV eq "S";
	
		#$root = "/prod" if $servername eq "5.0.5.35";
	}

	#Additional checks
	foreach my $argument (split(/ /, qq/customer client campaign jobtitle jobid/))
	{
		die "Invalid arguments - $argument ('$user{$argument}') contains invalid characters\n" if $user{$argument} !~ /^[a-zA-Z0-9\._]+$/;
	}


	# Build campaign path
	my $dircount = 0;

	my (@extras, @main);
		
	# create extra paths based on workflow
	@extras = qw/CLIENT_ORIGINAL OUTPUT_FILE DOCUMENT SCANS_LOGOS/;
	@main = qw/customer_firstletter customer client campaign jobtitle jobid_pub outputs/;

	foreach my $extras_path (@extras)
	{
		$user{outputs} = $extras_path;
		
		my $campaign_path = $root;
		foreach my $path (@main)
		{
			$campaign_path .= $user{$path} . '/';
		}
		
		$dircount += recursive_mkdir($campaign_path);
	}

	# Finished succesfully
	print "Directory structure " . ($dircount > 0 ? "created" : "exists") . "\n";
}



# Walk components, creating the path as needed (perl equivalent of 'mkdir -p')
sub recursive_mkdir
{
	my ($path) = @_;
	
	my $createcount = 0;
	my $curpath = "";
	
	foreach my $curdir (split(/\//, $path))
	{
		$curpath .= $curdir . '/';
		
		if(!-e $curpath)
		{
			umask 0;
			print "Creating '$curpath'\n";
			mkdir($curpath, 0777) || print "Couldn't create '$curpath' ($!, $@)";
			$createcount++;
		}
		elsif(!-d $curpath)
		{
			print "'$curpath' exists as not a directory on the filesystem\n";
		}
	}

	return $createcount;
}

1;
