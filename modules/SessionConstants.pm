#!/usr/local/bin/perl -w

package SessionConstants;

use strict;
use warnings;

use constant SESS_LOGGEDIN => 'loggedin';
use constant SESS_USERID   => 'userid';
use constant SESS_USERNAME => 'username';
use constant SESS_POSTLOGIN => 'postlogin';
use constant SESS_FNAME => 'fname';
use constant SESS_LNAME => 'lname';
use constant SESS_EMAIL => 'email';
use constant SESS_ROLENAME => 'rolename';
use constant SESS_ROLEID => 'roleid';
use constant SESS_TEMPUSER => 'tempuser'; # id from userstemp table
use constant SESS_AUTH_NAME => 'authname';
use constant SESS_AUTH_ID => 'authid';
use constant SESS_OPCOID => 'opcoid';
use constant SESS_OPCONAME => 'opconame';
use constant SESS_RELAXROLES => 'relaxroles';
use constant SESS_USERSETTINGS => 'usersettings'; # Column Manager settings (column_id, column_name, width, visibility)
use constant SESS_USERFILTERSETTINGS => 'userfiltersettings'; # List of filter names
use constant SESS_FULLNAME => 'fullname';
use constant SESS_ISDEVELOPER => 'isdeveloper';
use constant SESS_OPMYJOBS => 'opmyjobs';
use constant SESS_SAM_ACCOUNT_NAME => 'samaccountname';
use constant SESS_USER_PRINCIPAL_NAME => 'userprincipalname';
use constant SESS_ASSOC_USERIDS => 'assoc_userids';
use constant SESS_EMPLOYEE_ID => 'employeeid';
use constant SESS_EMPLOYEE_ID2 => 'employeeid2';
use constant SESS_CREATE_CONFIRM => 'create_confirm';
use constant SESS_PRINCIPAL => 'principal';  # actual pricipal supplied by user ie UPN or SAM
use constant SESS_DISTINGUISHED_NAME => 'dn'; # LDAP path with CN, OU and DC components

1;

