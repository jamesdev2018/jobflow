#!/usr/local/bin/perl
package ECMDBKanbanboard;

use strict;
use warnings;

# use Switch; # doesnt use switch statement
use Scalar::Util qw( looks_like_number );
use Carp qw/carp cluck/;
use Data::Dumper;
use URI::Escape;

# Home grown

use lib 'modules';

use Validate;

require "config.pl";
require "databaseOpcos.pl";
require "databaseUserFilter.pl";

use constant UNASSIGNED_LABEL_NAME => "Unassigned"; #mainly used for kanban board to display unassigned jobs.same constant used in kanbanboard.pl
use constant NOT_ASSIGNED_LABEL_NAME => "NONE"; #used for display the unassigned department and teams jobs in kanban

use constant BLANK => '(blank)'; # To present a blank value in the filters

#update the value for dropped job 
sub updateJobValues
{
    my ($job_number,$job_type,$value) = @_;
	my $status;
	my $column_name;
    my $id;
	my $table_name;
	if($job_type == 1) #Department
	{
		$column_name="departmentvalue";
		$table_name="tracker3plus";
		($id) = $Config::dba->process_oneline_sql("SELECT id FROM DEPARTMENTS WHERE name=? ",[$value]);
	}
    if($job_type == 2) #Team
    {
        $column_name="TEAMVALUE";
        $table_name="tracker3plus";
        ($id) = $Config::dba->process_oneline_sql("SELECT id FROM TEAMS WHERE name=? ",[$value]);
    }
    if($job_type == 4) #Operator Status
    {
        $column_name="operatorstatus";
        $table_name="tracker3plus";
        ($id) = $Config::dba->process_oneline_sql("SELECT id FROM OPERATORSTATUS WHERE status=? ",[$value]);
    }
	if($job_type == 5) #Advisory
	{
		$column_name="QUERYTYPE";
		$table_name="tracker3plus";
		($id) = $Config::dba->process_oneline_sql("SELECT id FROM querytypes WHERE querytype=? ",[$value]);
	}
    if($job_type == 6) #QC STATUS
    {
		$column_name="qcstatus";
		$table_name="tracker3plus";
		($id) = $Config::dba->process_oneline_sql("SELECT id FROM QCSTATUS WHERE status=? ",[$value]);
    }
    if($job_type == 7) #Booked By
	{
		$column_name="bookedby";
		$table_name="tracker3";
		$id	=	$value;
    }
    if($job_type == 8) #Producer
	{
		$column_name="producer";
		$table_name="tracker3";
		$value = '' if $value eq BLANK; # map '(blank)' to ''
		$id	= $value;
    }
=cc
    if($job_type == 9) #Assignee
	{
		$column_name="assignee";
		$table_name="tracker3plus";
		($id)	=	$Config::dba->process_oneline_sql("SELECT id FROM users WHERE username=? ",[$value]);
    }
=cut

	if($job_type != 9) # for assignee handled in jobs.pl => AssignJobs()
	{
		$Config::dba->process_sql("UPDATE $table_name SET $column_name='$id' WHERE job_number=?", [ $job_number ]);
	}
}
#get all order by list
sub db_getOrderbyList {
	my $rows = $Config::dba->hashed_process_sql("SELECT id,name from swimlane_orderby_list");
	return $rows ? @$rows : ();
}
#get all card details list
sub db_getCardList {
	my ($traffic_flag) = @_;
	my $rows = $Config::dba->hashed_process_sql("SELECT id,name from swimlane_card_details_list where traffic_light=?", [ $traffic_flag ]);
	return $rows ? @$rows : ();
}

# Copy of db_userlist from databaseJob.pl, saves including a big module for one function
# used by db_getSwimLaneOrderByValues below.

sub db_userlist {
    my ($opcoid) = @_;

    my ( $id, $username, @userslist );
    my $rows = $Config::dba->hashed_process_sql(
		"SELECT u.id AS id, u.username AS username FROM (SELECT DISTINCT(assignee), opcoid, location " .
		" FROM tracker3plus t3p GROUP BY assignee, opcoid, location) t3p " .
		" INNER JOIN users u ON t3p.assignee=u.id " .
		" WHERE t3p.opcoid=? OR t3p.location=? " .
		" GROUP BY username ORDER BY username ASC", [ $opcoid, $opcoid ]);

    foreach my $row (@$rows) {
        push(@userslist, { id => "ASS_" . ($row->{id} || "0"),   name => ($row->{username}) || "-", "userid" => ($row->{id} || "0") });
	}
    return \@userslist;
}


sub db_getSwimLaneOrderByValues {
	my ($orderby_id,$opcoid,$userid,$filter_id)	=	@_;

	# warn "db_getSwimLaneOrderByValues: orderbyid $orderby_id, opcoid $opcoid, userid $userid, filterid $filter_id\n";

	my ($lane_list,$card_list)	=	$Config::dba->process_oneline_sql(
		"SELECT laneid_and_status,card_detail_list FROM swimlane_config WHERE orderby_id = ? AND opcoid =? AND user_id = ? AND filter_id=?  AND status = 1",
		[$orderby_id,$opcoid,$userid,$filter_id]);

	# warn "db_getswimlaneorderbyvalues: lane_list $lane_list\n" if defined $lane_list;
 	# warn "db_getswimlaneorderbyvalues: card list $card_list\n" if defined $card_list;

	my @list;

	if ( ( ! defined $orderby_id ) || ( $orderby_id eq '' ) ) { # The initial call when we start from a new filter, wont define orderby_id
		# Do nothing

	} elsif( $orderby_id ==	1)	#Department
	{
		@list	= 	db_getSwimLane_Team_and_Department_Data($lane_list,$opcoid,"Department");
	}
	elsif($orderby_id  ==	2) #Team
	{
		@list	=  db_getSwimLane_Team_and_Department_Data($lane_list,$opcoid,"Team"); 
	}
	elsif($orderby_id	==	3) #Job Status
	{
		@list	=	db_getSwimlaneData($lane_list,"status","swimlane_job_status"); #arg 1 => swimlane data, arg2 => column name of table , arg3 => table_name
	}
	elsif($orderby_id  ==  4) #Operator Status
	{
		@list	=	db_getSwimlaneData($lane_list,"status","OPERATORSTATUS");
	}
	elsif($orderby_id  ==  5) #Advisory
	{
		@list	=	db_getSwimlaneData($lane_list,"querytype","querytypes");
	}
	elsif($orderby_id  ==  6) #QC Status
	{
		@list	=	db_getSwimlaneData($lane_list,"status","QCSTATUS");
	}
	elsif($orderby_id  ==  7) #Booked By
	{
		@list	=	db_getBookedByList( $lane_list,$opcoid );
	}
	elsif($orderby_id  ==  8) #Producer
	{
		@list	=	db_getProducerList( $lane_list,$opcoid );
	}
	elsif($orderby_id  ==  9) #Assigned To
	{
		if($lane_list ne "")
		{
			@list	=	db_getSwimlaneDataForAssignee($lane_list ,  $opcoid );
		}
		else
		{
			@list = @{ db_userlist($opcoid) };
			@list = map { { id => $_->{userid}, name => $_->{name} } } @list;
			push(@list, {id => UNASSIGNED_LABEL_NAME, name => UNASSIGNED_LABEL_NAME});
		}
		
	}
	return \@list;
}
sub db_getSwimLane_Team_and_Department_Data {
    my ($swimlane_lane_id,$opcoid,$type) =   @_;

    my $lane_name;
    my @lane_details;
    my $lane_id;
    my @lanes;
    my $rows;
	my %index;
    if($swimlane_lane_id ne "")
    {
        my @lane_list = split(",",$swimlane_lane_id);
        foreach $lane_name (@lane_list)
        {
            @lane_details   =   split("~",$lane_name);
            $lane_id        =   $lane_details[0];
            push(@lanes,  $lane_id);
        }
        $swimlane_lane_id = "'".join("','",@lanes)."'";
		if($type	eq	"Team")
		{
        	$rows = $Config::dba->hashed_process_sql("SELECT id,name FROM (
SELECT id,name from teams WHERE id in ($swimlane_lane_id)
UNION
SELECT d1.id,d1.name FROM teams d1 LEFT OUTER JOIN teamdetails d2 ON d1.id = d2.teamid and d2.opcoid=? WHERE d2.checked=1 AND d1.id NOT IN ($swimlane_lane_id)
) as b ORDER BY FIELD(id,$swimlane_lane_id) ", [$opcoid]);
		}
		if($type	eq	"Department")
		{
			$rows = $Config::dba->hashed_process_sql("SELECT id,name FROM (
SELECT id,name from departments WHERE id in ($swimlane_lane_id)
UNION
SELECT d1.id,d1.name FROM departments d1 LEFT OUTER JOIN departmentdetails d2 ON d1.id = d2.departmentid and d2.opcoid=? WHERE d2.checked=1 AND d1.id NOT IN ($swimlane_lane_id)
) as b ORDER BY FIELD(id,$swimlane_lane_id) ", [$opcoid]);
		}

    }
    else
    {		
		if($type    eq  "Team")
		{
        	$rows = $Config::dba->hashed_process_sql("SELECT d1.id,d1.name FROM teams d1 LEFT OUTER JOIN teamdetails d2 ON d1.id = d2.teamid and d2.opcoid=? WHERE d2.checked=1 order by d1.name asc",[ $opcoid ]);
		}
		if($type	eq	"Department")
		{
	    	$rows = $Config::dba->hashed_process_sql("SELECT d1.id,d1.name FROM departments d1 LEFT OUTER JOIN departmentdetails d2 ON d1.id = d2.departmentid and d2.opcoid=? WHERE d2.checked=1 order by d1.name asc",[ $opcoid ] );
		}
		push(@$rows, {id => NOT_ASSIGNED_LABEL_NAME, name => NOT_ASSIGNED_LABEL_NAME});
    }
    return $rows ? @$rows : ();
}

sub db_getJobStatus {
    my $rows = $Config::dba->hashed_process_sql("SELECT id,status FROM swimlane_job_status");
    return $rows ? @$rows : ();
}
sub db_getFilterID {
	my ($filtername,$userid,$opcoid) = @_;
	my ($filterid,$filterownerid)	=	$Config::dba->process_oneline_sql("SELECT id,userid  FROM userfiltersettings
                                                                    WHERE filterdesc=? AND opcoid=? AND userid=?", [ $filtername,$opcoid,$userid ]);
	return $filterid,$filterownerid;
}
sub db_updateSwimLaneBoard {
	my ($filterid,$userid,$opcoid,$orderby_id,$lane_values,$card_list_value,$deleted_user_list,$saved_user_list,$filtername) = @_;
	my @ids; 
	my $username;

	#Update prvious record status as 0
	$Config::dba->process_oneline_sql("UPDATE swimlane_config SET status=0 WHERE user_id=? AND opcoid=? AND filter_id=?",[$userid,$opcoid,$filterid]);
	#Insert the row
	$Config::dba->process_sql("INSERT INTO swimlane_config (orderby_id,filter_id,laneid_and_status,card_detail_list,user_id,opcoid,rec_timestamp) VALUES (?, ?, ?, ?, ?, ?, now())", [ $orderby_id,$filterid,$lane_values,$card_list_value,$userid,$opcoid  ]);
}
sub db_getFilterSettingsValue {
	my ($added_user_list,$deleted_user_list,$filtername,$userid,$opcoid) = @_;
	my @name;
	my $username;
	my $added_user_ids="";
	my $deleted_user_ids="";
	
	my ($status,$sharedfilter_id)   =   $Config::dba->process_oneline_sql("SELECT status,id FROM sharedfilters WHERE STATUS = 1 AND SHAREDFILTERNAME = ? AND OWNERUSERID =? AND OPCOID =?", [$filtername,$userid,$opcoid]);		
	my $sharedusers_flag	=	"No";
	my $sharedfilterid		= 	"null"; #the same value passed in userfilter.pl
	if ( ( defined $status ) && ( $status == 1 ) )
	{
		$sharedusers_flag	=	"Yes";
		$sharedfilterid		=	$sharedfilter_id;
	}
	if($added_user_list)
	{
        @name    =   split(",",$added_user_list);
        $username = "'".join("','",@name)."'";
        ($added_user_ids) = $Config::dba->process_oneline_sql("SELECT group_concat(id) from USERS where username IN ($username)");
	}
	if($deleted_user_list)
	{
        @name    =   split(",",$deleted_user_list);
        $username = "'".join("','",@name)."'";
        ($deleted_user_ids) = $Config::dba->process_oneline_sql("SELECT group_concat(id) from USERS where username IN ($username)");
	}
	my ($shared_users_list)	=	$Config::dba->process_oneline_sql("SELECT group_concat(SHAREDUSERID) as id  FROM sharedfilterusers sfu INNER JOIN sharedfilters sf ON sf.ID=sfu.SHAREDFILTERID    WHERE sf.STATUS=1 AND sfu.STATUS=1 AND sf.OPCOID=? AND sf.SHAREDFILTERNAME = ? AND sf.OWNERUSERID=?", [$opcoid,$filtername,$userid]);
	return $added_user_ids,$deleted_user_ids,$sharedusers_flag,$sharedfilterid,$shared_users_list;

}
sub db_getBoxValues	{

	my ($userid,$opcoid,$filter_id) = @_;

	my ($card_detail_list)  =   $Config::dba->process_oneline_sql("SELECT card_detail_list  FROM swimlane_config  WHERE  user_id=? AND opcoid=? AND filter_id = ?  AND status=1", [ $userid,$opcoid,$filter_id ]);

	return $card_detail_list;
}
sub db_getCardListType {
	my ($id)	= @_;

	my ($name)	=	$Config::dba->process_oneline_sql("SELECT name  FROM swimlane_card_details_list WHERE id=? ", [ $id ]);
	return $name;
}
sub getSwimLaneType {
	my ($userid,$opcoid,$filter_id) = @_;
	my ($lane_type)	= $Config::dba->process_oneline_sql("select orderby_id from swimlane_config WHERE user_id = ? AND opcoid = ? AND filter_id=? AND status=1" , [ $userid,$opcoid,$filter_id ] );
	return $lane_type;
}
sub db_getLaneColumns {
	my ($userid,$opcoid,$filterid) = @_;
	my ($lane_list,$orderby_id,$card_detail_list)  =   $Config::dba->process_oneline_sql("SELECT laneid_and_status,orderby_id,card_detail_list FROM swimlane_config  WHERE  user_id=? AND opcoid=? AND status=1 AND filter_id=?", [ $userid,$opcoid,$filterid ]);	
	return $lane_list,$orderby_id,$card_detail_list;
}
sub db_getLaneColumnName {
	my ($id,$orderbyid) = @_;
	my $column_name;
	my $table_name;
	my $name;

	if($orderbyid == 1) #Department
	{
		$column_name = "name";
		$table_name	 = "departments";
	}
	if($orderbyid == 2) #Team
	{
		$column_name = "name";
		$table_name	 = "teams";
	}
	if($orderbyid == 3) #Job Status
	{
		$column_name = "status";
		$table_name	 = "swimlane_job_status";
	}
	if($orderbyid == 4) #Operator Status
	{
		$column_name = "status";
		$table_name	 = "operatorstatus";
	}
	if($orderbyid == 5) #Advisory
	{
		$column_name = "querytype";
		$table_name	 = "querytypes";
	}
	if($orderbyid == 6) #QC Status
	{
		$column_name = "status";
		$table_name	 = "qcstatus";
	}
	if($orderbyid == 9) #Assiged To
	{
		$column_name = "username";
		$table_name	 = "users";
	}

	($name)	=	$Config::dba->process_oneline_sql("SELECT $column_name from $table_name WHERE id=?",[$id]);

	if($orderbyid == 9)
	{
		if($id eq UNASSIGNED_LABEL_NAME)
		{
			$name	=	UNASSIGNED_LABEL_NAME;
		}
	}

	return $name;

}
sub db_checkSwimlaneConfig {
	my ($filter_id,$userid,$opcoid) = @_;
	my ($exist) =	$Config::dba->process_oneline_sql("SELECT 1 FROM swimlane_config WHERE user_id=? AND opcoid=? AND filter_id=? AND status=1", [$userid,$opcoid,$filter_id]);
	return $exist;
}
sub db_sharedUserAccess {
	my ($filter_name,$userid,$opcoid) = @_;
	my ($exist) =	$Config::dba->process_oneline_sql("SELECT 1 from sharedfilters sf INNER JOIN sharedfilterusers sfu ON sfu.SHAREDFILTERID = sf.ID WHERE sf.SHAREDFILTERNAME = ? AND sfu.SHAREDUSERID = ? AND sf.opcoid = ? AND sfu.status = 1",[$filter_name,$userid,$opcoid]);
	return $exist;
}
sub db_getBookedByList {
	my ($swimlane_lane_id,$opcoid) = @_;
    my $lane_name;
    my @lane_details;
    my $lane_id;
    my @lanes;
    my $rows;

    if($swimlane_lane_id ne "")
    {
        my @lane_list = split(",",$swimlane_lane_id);
        foreach $lane_name (@lane_list)
        {
            @lane_details   =   split("~",$lane_name);
            $lane_id        =   $lane_details[0];
            push(@lanes,  $lane_id);
        }
        $swimlane_lane_id = "'".join("','",@lanes)."'";
        $rows   =   $Config::dba->hashed_process_sql("SELECT bookedby from (SELECT DISTINCT t3.bookedby FROM tracker3 t3 WHERE t3.bookedby IN ($swimlane_lane_id) ORDER BY FIELD(t3.bookedby,$swimlane_lane_id)) as row1
UNION
SELECT bookedby from (SELECT DISTINCT t3.bookedby FROM tracker3 t3 INNER JOIN tracker3plus t3p ON t3p.job_number=t3.job_number WHERE (t3p.opcoid=? or t3p.location=?) AND (t3.bookedby NOT IN ($swimlane_lane_id) AND t3.bookedby!=' ')) as row2",[$opcoid,$opcoid]);

    }
	else
	{
		$rows	=	$Config::dba->hashed_process_sql("SELECT DISTINCT t3.bookedby FROM tracker3 t3 INNER JOIN tracker3plus t3p ON t3p.job_number=t3.job_number WHERE (t3p.opcoid=? or t3p.location=?) AND t3.bookedby!=''",[$opcoid,$opcoid]);
	}
	my @bookedby;

    if ( defined $rows ) {
        foreach my $res ( @$rows ) {
            push ( @bookedby, { id => $res->{bookedby}, name => $res->{bookedby} || "-" } );
        }
    }
    return @bookedby;
}

###########################################################
# db_getProducerList
# swimlane_lane_id	=> swimlane lane id
# opcoid			=> operating company id
###########################################################
sub db_getProducerList {
    my ($swimlane_lane_id,$opcoid) = @_;

    my $lane_name;
    my @lane_details;
    my $lane_id;
    my @lanes;
    my $rows;

	my %producers;

	my $broadcast_opcos = Config::getConfig("broadcast_opcos") || ''; # List of opcoids, for Smoke_and_mirrors etc
	die "No broadcast opcos" unless ( ( defined $broadcast_opcos ) && ( $broadcast_opcos ne '' ) );
	
	$rows = $Config::dba->hashed_process_sql(
				"SELECT DISTINCT t3.producer FROM tracker3 t3 INNER JOIN tracker3plus t3p ON t3p.job_number=t3.job_number " .
				"WHERE (t3p.opcoid IN ( $broadcast_opcos ) ) AND ( t3.producer != '' ) " );

	if ( defined $rows ) {
		foreach my $res ( @$rows ) {
			next if ( $res->{producer} =~ /^\s*$/ ); # Ignore any blank string
			$producers{ $res->{producer} } = 1;
		}
	}

	# Then add any from supplied $swimlane_lane_id
    if ( ( ! defined $swimlane_lane_id ) || ( $swimlane_lane_id eq "" ) ) {
		# warn "db_getProducerList: swimlane id undef\n";

	} else {
		# warn "db_getProducerList: swimlane id $swimlane_lane_id\n";
        my @lane_list = split(",",$swimlane_lane_id);
        foreach $lane_name (@lane_list) {
            my ( $lane_id ) = split("~",$lane_name); # lane_name is laneid ~ number, so just want laneid 
			if ( ! exists $producers{ $lane_id } ) {
				# warn "db_getProducerList: adding $lane_id\n";
				$producers{ $lane_id } = 1;
			}
        }
	}

	# Convert %producers to @producer_list

	my @producer_list;

	push ( @producer_list, { name => BLANK, id => BLANK } ); # Add blank here as we exclude it from the query
	foreach my $producer ( sort keys %producers ) {
		next if (( $producer eq 'undef' ) || ( $producer eq BLANK ));
		push ( @producer_list, { name => $producer, id => $producer } );
	}
	push ( @producer_list, { name => 'undef', id => 'undef' } );    # and undef in case there are jobs with NULL in t3.producer (its nullable with default '-')

	return @producer_list;
}


sub db_getSharedUser {
	my ($filtername) = @_;
    my $rows = $Config::dba->hashed_process_sql("SELECT CONCAT(FNAME,' ',LNAME) as user_name,u.id,sfu.id,u.username from sharedfilters sf INNER JOIN sharedfilterusers sfu ON sfu.SHAREDFILTERID = sf.ID INNER JOIN users u ON u.id=sfu.SHAREDUSERID WHERE sf.SHAREDFILTERNAME = ? AND sfu.status = 1",[$filtername]);
    return $rows ? @$rows : ();

}
sub db_getSwimlaneData {
		
	my ($swimlane_lane_id,$column_name,$table_name)	=	@_;

	my $lane_name;	
	my @lane_details;
	my $lane_id;
	my @lanes;
	my $rows;

	if($swimlane_lane_id ne "")
	{
		my @lane_list = split(",",$swimlane_lane_id);
		foreach $lane_name (@lane_list)
		{
			@lane_details   =   split("~",$lane_name);
			$lane_id        =   $lane_details[0];
			push(@lanes,  $lane_id);
		}
		$swimlane_lane_id = "'".join("','",@lanes)."'";
    	$rows = $Config::dba->hashed_process_sql("select id,$column_name as name from (SELECT id,$column_name FROM $table_name WHERE id IN ($swimlane_lane_id) 
 UNION SELECT id,$column_name FROM $table_name WHERE id NOT IN 
 ($swimlane_lane_id) ) as b ORDER BY FIELD(id,$swimlane_lane_id)");
                                              
	}
	else
	{
		$rows = $Config::dba->hashed_process_sql("SELECT id,$column_name as name from $table_name ORDER BY $column_name asc");
	}
    return $rows ? @$rows : ();
}

sub db_getSwimlaneDataForAssignee {

	my ($swimlane_lane_id,$opcoid) =   @_;
    my $lane_name;
    my @lane_details;
    my $lane_id;
    my @lanes;
    my $rows;
	if($swimlane_lane_id ne "")
	{
        my @lane_list = split(",",$swimlane_lane_id);
        foreach $lane_name (@lane_list)
        {
            @lane_details   =   split("~",$lane_name);
            $lane_id        =   $lane_details[0];
            push(@lanes,  $lane_id);
        }
		my %index;
		@index{@lanes} = (0..$#lanes);
		my $search = UNASSIGNED_LABEL_NAME;
		my $index = $index{$search};
        $swimlane_lane_id = "'".join("','",@lanes)."'";
		$rows	=	$Config::dba->hashed_process_sql("SELECT id,username as name FROM (
							SELECT id,username from users WHERE id in ($swimlane_lane_id)
							UNION
							SELECT u.id AS id, u.username FROM (SELECT DISTINCT(assignee), opcoid, location FROM tracker3plus t3p GROUP BY assignee, opcoid, location) t3p	INNER JOIN users u ON t3p.assignee=u.id	WHERE (t3p.opcoid=? OR t3p.location=?) AND u.id not in ($swimlane_lane_id) GROUP BY username 
							) as b ORDER BY FIELD(id,$swimlane_lane_id) ",[$opcoid,$opcoid]);

		splice @$rows,$index, 0, {id=>UNASSIGNED_LABEL_NAME,name=>UNASSIGNED_LABEL_NAME}; #get index of unassigned and insert into related position. This for showing the list in last saved order
	}
	return $rows ? @$rows : ();
}

sub db_getSavedFilterDate {
    my ($filtername,$orderbyid,$userid) = @_;
    my $filtertype;

	die "Invalid orderbyid" unless defined $orderbyid;

    if($orderbyid == 1)
    {
        $filtertype = "newdeptFilter";
    }
	if($orderbyid == 2)
	{
		$filtertype = "newteamFilter";
	}
	if($orderbyid == 4)
	{
		$filtertype = "newoperatorstatusFilter";
	}
	if($orderbyid == 5)
	{
		$filtertype = "newquerytypeFilter";
	}
	if($orderbyid == 6)
	{
		$filtertype = "newqcstatusFilter";
	}
	if($orderbyid == 7)
	{
		$filtertype = "newbookFilter";
	}
	if($orderbyid == 8)
	{
		$filtertype = "newproducertypeFilter";
	}
	if($orderbyid == 9)
	{
		 $filtertype = "newAssignFilter";
	}
	my ($filterdata) = $Config::dba->process_oneline_sql("SELECT filterdata FROM userfiltersettings WHERE filterdesc =? AND filtertype=? AND status=0 AND userid=?", [$filtername,$filtertype,$userid]);
	return $filterdata;
}
sub db_getUsersList {
	my ($opcoid,$userid) = @_;
	my $whereClause     = "WHERE FNAME <> \'\' AND LNAME <> \'\'";
    my $bindingColumns  = [];

    if ( $opcoid ){
        $whereClause    = "$whereClause" . " AND opcoid = ?";
        $bindingColumns = [ $opcoid ];
    }
    my $rows = $Config::dba->hashed_process_sql("SELECT (CONCAT(FNAME, ' ', LNAME, ', ', ID)) USERNAME FROM users $whereClause ORDER BY ID",$bindingColumns);
    return $rows ? @$rows : ();

}

sub db_getJobOpcoAndPreflightStatus {
	my ($job_number) = @_;
	my ($opcoid,$preflightstatus) = $Config::dba->process_oneline_sql("SELECT opcoid,preflightstatus FROM TRACKER3PLUS WHERE job_number=?", [ $job_number ]);
	return $opcoid,$preflightstatus;
}
sub db_checkQCenabled {
	my ($opcoid)	=	@_;
	my ($qcenabled)	=	 $Config::dba->process_oneline_sql("SELECT qcenabled FROM operatingcompany WHERE opcoid = ?", [ $opcoid ] );
	return $qcenabled;
}

sub db_getBoardviewBriefComments {
	my $job_number	= $_[0];
	my $rows = $Config::dba->hashed_process_sql("SELECT u.fname, u.lname, c.content,
						DATE_FORMAT(c.rec_timestamp,'\%a, \%d \%b \%Y \%H:\%i:\%s') AS timestamp, c.id
                        FROM comments c
                        INNER JOIN users u ON u.id=c.userid
                        WHERE c.job_number = ? AND c.status IN ( 1 ) ORDER BY c.sequence DESC, c.status asc, c.rec_timestamp desc;",[ $job_number ] );
	foreach my $row ( @$rows ) {
		$row->{content}   = uri_escape_utf8( $row->{content} ) if ( $row->{content} );
	}

	return $rows;
}

sub db_getStatusNote {
	my ($job_number, $mediatype) = @_;
	Validate::job_number( $job_number );
	my $status_note = undef;

	if ( (defined $mediatype ) && ( $mediatype =~ /^BROADCAST$/i ) ) {
		($status_note) = $Config::dba->process_oneline_sql("SELECT SMOKECMD FROM Broadcasttracker3 WHERE JOB_NUMBER=?",
															[ $job_number ]);
	} else {
		($status_note) = $Config::dba->process_oneline_sql("SELECT
															CASE WHEN p.JOBVERSION > 1
																THEN p.PRODUCTIONREMARKS
															ELSE
																t3p.brief
															END AS status_note, p.id from tracker3plus t3p
															LEFT OUTER JOIN products p on t3p.job_number =  p.job_number
															WHERE t3p.job_number = ? order by p.ID DESC LIMIT 1;", [ $job_number ] );
	}

	return $status_note || '-';
}

1;
