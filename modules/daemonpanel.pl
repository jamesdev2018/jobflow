#!/usr/local/bin/perl
package DaemonPanel;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use Template;

# Home grown
use lib 'modules';

require "config.pl";
require "htmlhelper.pl";

# daemonpanel.pl - display state of all daemons, control which can run on which server

##############################################################################
#   listDaemons()
#	show the currently running daemons
#
##############################################################################
sub listDaemons {
	my ( $query, $session ) = @_;

	my $formPosted	= $query->param( "formPosted" ) ||'';

	my $daemonOn = "off";
	my $onoff = 0;
	my $sleepdelay = 1;
	my $currentDaemon = 0;
	my @daemonList;
	my @daemons = DaemonPanel::getDaemons();

	if ($formPosted eq "yes") {
		# for each daemon ....
		foreach my $dae ( @daemons ) {
			$currentDaemon = $dae->{'id'};
			$onoff = 0;
			$daemonOn = $query->param( "d_$currentDaemon" );	
			$sleepdelay = $query->param( "sd_$currentDaemon" );	
			if ($daemonOn eq "on") { $onoff = 1; }
			DaemonPanel::updateDaemonOn( $currentDaemon, $onoff, $sleepdelay );
		}	
		# get the updated list
		@daemons = DaemonPanel::getDaemons();
	}


	my $vars = {
		daemons	=> \@daemons,
	};

	HTMLHelper::setHeaderVars( $vars, $session );

	my $tt = Template->new( { RELATIVE => 1, } ) || die "$Template::ERROR\n";
	$tt->process( 'ecm_html/listDaemons.html', $vars ) || die $tt->error(), "\n";

} # listDaemons


##############################################################################
##  getMaxCheckInLmit
##       how many jobs are currently being checked in?
##		 id=1 -> checkin daemon
###############################################################################
sub getMaxCheckInLimit {

	my ($maxcheckin) = $Config::dba->process_oneline_sql("SELECT intfield1 AS maxcheckinlimit FROM daemonconfig WHERE id=1");
	return $maxcheckin;
}

##############################################################################
##  getMaxCheckOutLimit
##       how many jobs are currently being checked out?
##		 id=2 -> checkout daemon
###############################################################################
sub getMaxCheckOutLimit {

	my ($maxcheckout) = $Config::dba->process_oneline_sql(
		"SELECT intfield1 AS maxcheckoutlimit FROM daemonconfig WHERE id=2" );
	return $maxcheckout;
}

##############################################################################
# update max checkins
##############################################################################
sub updateMaxCheckIns {
	my ($maxcheckins) = @_;

	#ONLY SET BIT FIELDS
	return 0 if $maxcheckins && !looks_like_number($maxcheckins);
	
	$Config::dba->process_sql("UPDATE daemonconfig SET intfield1=? WHERE id=1", [ $maxcheckins ]);

	return $maxcheckins;
}

##############################################################################
# update max checkouts
##############################################################################
sub updateMaxCheckOuts {
	my ($maxcheckouts) = @_;
	
	#ONLY SET BIT FIELDS
	return 0 if $maxcheckouts && !looks_like_number($maxcheckouts);
	
	$Config::dba->process_sql("UPDATE daemonconfig SET intfield1=? WHERE id=2", [ $maxcheckouts ]);

	return $maxcheckouts;
}

##############################################################################
## Get the opcoid using operating company name
###############################################################################
sub getDaemons {

	my $rows = $Config::dba->hashed_process_sql(
		"SELECT id, daemon AS name, heartbeat AS lastseen, onoff, sleepdelay, serverid FROM daemonconfig ORDER BY name, id" );

	return $rows ? @$rows : ();
} 

##############################################################################
# update daemon
##############################################################################
sub updateDaemonOn {
	my ($daemonid, $onoff, $sleepdelay) = @_;
	

	#ONLY SET BIT FIELDS
	return 0 if ($onoff != 0) && ($onoff != 1);
	return 0 if $daemonid && !looks_like_number($daemonid);
	return 0 if $sleepdelay && !looks_like_number($sleepdelay);
	
	$Config::dba->process_sql( "UPDATE DAEMONCONFIG SET onoff=?, sleepdelay=? WHERE id=?", [ $onoff, $sleepdelay, $daemonid ] );
	
	return 1;
}

1;

