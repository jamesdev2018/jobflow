#!/usr/local/bin/perl
package Reports;

use strict;
use warnings;
use Scalar::Util qw( looks_like_number );

use Data::Dumper;

use Template;
use Data::UUID;
use XML::Simple;
use Data::Dumper;
use File::Copy;
use File::Basename;
use File::Path qw(make_path remove_tree);
use Cwd;

use lib 'modules';

require "databaseJob.pl";
require "databaseOpcos.pl";
require "databaseReport.pl";
require "users.pl";
require "audit.pl";
require "Mailer.pl";
require "config.pl";
require "general.pl";



##################################################################
# from config 
##################################################################

my $report_dir		= Config::getConfig("reportdirectory");
my $compiler_path	= Config::getConfig("reportcompiler");


#USED FOR FILLING THE REPORTS
my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");
my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");
my $jobflowServer 	= Config::getConfig("jobflowserver");

##################################################################
# globals 
##################################################################

my $applicationurl      = $jobflowServer . "/jobflow.pl";

my $zipcmd		= "/usr/bin/zip";
my $unzipcmd 		= "/usr/bin/unzip";

my %preflightstatuses = (
		InProgress 	=> '1',
        Passed 		=> '2',
        Warning		=> '3',
        Failed		=> '4',
);

my %assetSubPath = (
	bw				=>		'BW_ASSET/',
	bwcropped		=>		'BW_CRP_ASSET/',
	lowrescropped	=>		'LR_CRP_ASSET/',
	lowres			=>		'LR_ASSET/',
	proofonly		=>		'PROOFING_ASSET/',
	rgb				=>		'RGB_ASSET/',
	rgbcropped		=>		'RGB_CRP_ASSET/',
	rgbjpeg			=>		'RGB_JPG_ASSET/',
	hires			=>		'OUTPUT_FILE/',
);


##################################################################
# the template object 
##################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";







##############################################################################
# SHOW A LIST OF REPORTS AVAILABLE
# ---> $query
# ---> $session
##############################################################################
sub ListReports
{
	my ($query, $session) = @_;
   	my $sidemenu   = $query->param( "sidemenu" );
	my $sidesubmenu= $query->param( "sidesubmenu" );
	my $roleid     = $session->param( 'roleid' );

	my @reports = ECMDBReports::getReports();
	
	my $vars = {
		reports	=> \@reports,
		sidemenu => $sidemenu,
		sidesubmenu => $sidesubmenu,	
		roleid => $roleid,
	};

	General::setHeaderVars( $vars, $session );
	
	$tt->process('ecm_html/listreports.html', $vars) or die $tt->error(), "\n";
}



##############################################################################
# DISPLAY A REPORT
# ---> $query
# ---> $session
##############################################################################
sub ShowReport
{
	my ($query, $session) = @_;
	
	
	my $reportid = $query->param("reportid");
	my $reportstyle = $query->param("reportstyle");
	my $formposted = $query->param("formposted");
	
	my $report = ECMDBReports::getReport($reportid);
	$reportstyle = "pdf" if $reportstyle !~ m/^(pdf|html|htm|xml|xls|xlsx)$/;
	
	
	my ($imglist, $parameters) = getDependencyList("$report_dir/" . $report->{sourcefile} . ".jrxml");
	my $paramlist;
	my $retval = "";
	
	if((@$parameters || $report->{formats}) && ($formposted ne "yes"))
	{
		my $vars = {
			title		=> $report->{title},
			parameters	=> $parameters,
			reportid		=> $reportid,
			needsparameters	=> 1,
			exports		=> $report->{formats},
			xmlexport	=> $report->{formats} =~ m/xml/i ? 1 : 0,
			xlsexport	=> (($report->{formats} =~ m/xls/i) && ($report->{formats} !~ m/xlsx/i)) ? 1 : 0,
			xlsxexport	=> $report->{formats} =~ m/xls/i ? 1 : 0,
			htmexport	=> $report->{formats} =~ m/html?/i ? 1 : 0,
			};
		
		General::setHeaderVars( $vars, $session );
	
		$tt->process('ecm_html/showreport.html', $vars) or die $tt->error(), "\n";
		return;
	}
	elsif(@$parameters)
	{
		foreach(@$parameters)
		{
			my $val = $query->param($_->{name});
			$val =~ s/\"/\\"/g;
			
			$paramlist .= $_->{name} . " \"$val\" ";
		}
		$retval = fillReport($report->{sourcefile}, ".$reportstyle", $paramlist);
	}
	else
	{
		$retval = fillReport($report->{sourcefile}, ".$reportstyle");
	}
	
	if($retval)
	{
		die "Couldn't process report: $retval";
	}
	
	
	my $vars = {
		title				=> $report->{title},
		reportid				=> $reportid,
		reportstyle			=> $reportstyle,
		needsparameters		=> 0,
		};

	General::setHeaderVars( $vars, $session );
	
	$tt->process('ecm_html/showreport.html', $vars) or die $tt->error(), "\n";
}


##############################################################################
# ADD A NEW REPORT
# ---> $query
# ---> $session
##############################################################################
sub AddReport
{
	my ($query, $session) = @_;
	
	my $title = General::htmlEditFormat($query->param("title"));
	my $userid = $session->param("userid");
	my $formposted = $query->param("formposted") ||'';
	my $filename = $query->param("template");
	my $formats = ($query->param("htmexport") ? "htm " : "") . 
				  ($query->param("xmlexport") ? "xml " : "") . 
				  ($query->param("xlsexport") ? "xls " : "") . 
				  ($query->param("xlsxexport") ? "xlsx " : "");
	
	my %errors;
   	my $sidemenu   = $query->param( "sidemenu" );
    my $sidesubmenu= $query->param( "sidesubmenu" );
    my $roleid     = $session->param( 'roleid' );

	my $vars = { title => $title };
	
	if($formposted eq "yes")
	{
		if($title eq "")
		{
			$errors{title} = "No title supplied";
		}
		elsif($filename eq "")
		{
			$errors{file} = "No report was supplied";
		}
		elsif($filename !~ m/.*.jrxml$/)
		{
			$errors{file} = "Filename is not a JasperReport XML file (*.jrxml)";
		}
		else
		{
			my $retval = uploadReport($query, $session);
			my $basename = $retval->{basename};
			if($retval->{error} == 0)
			{
				my ($imglist, $paramlist) = getDependencyList("$report_dir/$basename.jrxml");
		
				my $reportid = ECMDBReports::addReport($title, $formats, $basename, $userid);
				
				
				if(@$imglist)
				{
					$errors{firsttime} = "This report refers to external images that need uploading.";
					
					$vars = {
						reportid		=> $reportid,
						title		=> $title,
						images		=> $imglist,
						error		=> \%errors,
						};
				
				
					#SUCCESS!!!
					General::setHeaderVars( $vars, $session );

					$tt->process('ecm_html/editreport.html', $vars) or die $tt->error(), "\n";
				}
				else
				{
					ListReports($query, $session);
				}
				return;
			}
			else
			{
				#AN ERROR OCCURRED, CLEAN UP THE FILE (ESPECIALLY IN CASE OF REPORT NON-COMPILATION)
				unlink "$report_dir/$basename.jrxml";
				$errors{compile} = $retval->{errormessage};
			}
		}
	}
	
	$vars = {
		error		=> \%errors,
		title		=> $title,
		sidemenu    => $sidemenu,
		sidesubmenu => $sidesubmenu,
		roleid       =>$roleid,
	};

	General::setHeaderVars( $vars, $session );
	
	$tt->process('ecm_html/addreport.html', $vars) or die $tt->error(), "\n";
}



##############################################################################
# EDIT A GIVEN REPORT
# ---> $query
# ---> $session
##############################################################################
sub EditReport
{
	my ($query, $session) = @_;
	
	my $reportid = $query->param("reportid");
	my $formposted = $query->param("formposted");
	my $formats = ($query->param("htmexport") ? "htm " : "") . 
				  ($query->param("xmlexport") ? "xml " : "") . 
				  ($query->param("xlsexport") ? "xls " : "") .
				  ($query->param("xlsxexport") ? "xlsx " : "");
	
	my $report = ECMDBReports::getReport($reportid);
	my $imglist; my $paramlist;
	
	
	
	#CALCULATE DEPENDENCIES, ETC.
	($imglist, $paramlist) = getDependencyList("$report_dir/" . $report->{sourcefile} . ".jrxml");
	
	
	#UPLOAD ANY NEW IMAGES POSTED
	my $forceRefresh = "false";
	foreach(@$imglist)
	{
		my $retval = uploadImage($query, $session, $reportid, $_);
		if($retval ne "") { $forceRefresh = "true"; $_ = $retval; }
	}
	if(@$imglist)
	{
		modifyDependencyList("$report_dir/" . $report->{sourcefile} . ".jrxml", $imglist);
		
		
		#NO NEED TO CHECK FOR ERRORS, SINCE WE SHOULDN'T GET HERE
		#IF WE DIDN'T SUCCEED COMPILATION FIRST TIME AROUND
		compileReport($report->{sourcefile});
		
		#IF THE IMAGES HAVE CHANGED, WE WANT TO DELETE THE PDF TO FORCE A REFILL
		if($forceRefresh)
		{
			unlink ("$report_dir/" . $report->{sourcefile} . ".pdf");
		}
	}
	
	
	if($formposted eq "yes")
	{
		ECMDBReports::editReport($reportid, $report->{title}, $formats);
		ListReports($query, $session);
		return;
	}
	
	my $vars = {
		reportid		=> $reportid,
		title		=> $report->{title},
		images		=> $imglist,
		xmlexport	=> $report->{formats} =~ m/xml/i ? 1 : 0,
		xlsexport	=> (($report->{formats} =~ m/xls/i) && ($report->{formats} !~ m/xlsx/i)) ? 1 : 0,
		htmexport	=> $report->{formats} =~ m/html?/i ? 1 : 0,
		};

	General::setHeaderVars( $vars, $session );

	$tt->process('ecm_html/editreport.html', $vars) or die $tt->error(), "\n";
}



##############################################################################
# DELETE A GIVEN REPORT
# ---> $query
# ---> $session
##############################################################################
sub DeleteReport
{
	my ($query, $session) = @_;
	my $reportid = $query->param("reportid");
	
	my $report = ECMDBReports::getReport($reportid);
	my ($imglist, $paramlist) = getDependencyList("$report_dir/" . $report->{sourcefile} . ".jrxml");
	
	
	#DELETE THE REPORT
	system("rm -r $report_dir/" . $report->{sourcefile} . ".*");
	
	#DELETE IMAGES RELATED TO THE REPORT
	foreach(@$imglist)
	{
		unlink("$report_dir/" . $_->{path});
	}
	
	return ECMDBReports::deleteReport($reportid);
}



##############################################################################
# UPLOAD THE ACTUAL REPORT FILE TO THE SERVER
# ---> $query
# ---> $session
# <--- %hash{error, errormessage, basename}
##############################################################################
sub uploadReport
{
	my ($query, $session) = @_;
	my $srcname = $query->param("template");
	
	my %returnvalue = ( error => 0, basename => "" );
	
	my ($filename, $filedir, $fileext) = fileparse($srcname, '\..*');
	my $path =  $report_dir . "/";
	my $basename = $filename;
	
	if($basename)
	{
		my $uploadHandle = $query->upload("template");
		
		my $index = "";
		while(-f "$path$basename$index.jrxml") { $index++; }
		$basename = "$basename$index";
		$returnvalue{basename} = $basename;
		
		$srcname = "$path$basename.jrxml";
		
		make_path($path);
		open(UPLOADFILE, ">$srcname") or die $!;
		binmode UPLOADFILE;
		while(<$uploadHandle>) { print UPLOADFILE; }
		close UPLOADFILE;
	}
	else
	{
		$returnvalue{error} = 1;
		$returnvalue{errormessage} = "No sourcename supplied".
		return \%returnvalue;
	}
	
	
	#COMPILE THE REPORT
	if(my $retval = compileReport($basename))
	{
		$returnvalue{error} = 1;
		$returnvalue{errormessage} = "Couldn't compile report: $retval";
	}
	
	return \%returnvalue;
}




##############################################################################
# COMPILE A GIVEN jrxml REPORT INTO A MORE EFFICIENT FORMAT
# ---> $basename
# <--- $compilerresponse
##############################################################################
sub compileReport
{
	my ($basename) = @_;
	
	make_path($report_dir);
	my $jarname = $compiler_path;
	my $srcname = "$report_dir/$basename.jrxml";
	my $compilename = "$report_dir/$basename.compiled";
	
	#print "Compiling report: 'java -jar $jarname compile $srcname $compilename'<br>\n";
	return qx{java -jar $jarname compile $srcname $compilename};
}



##############################################################################
# FILL IN A COMPILED REPORT WITH A SET OF PARAMETERS USING THE CURRENTLY
# ACTIVE DATABASE SETTINGS
# ---> $basename
# ---> $resultextension
# ---> \@parameterlist(name, value)
# <--- $compilerresponse
##############################################################################
sub fillReport
{
	my ($basename, $ext, $paramlist) = @_;
	$ext = ".pdf" unless $ext =~ m/^\.(pdf|htm|html|xml|xls|xlsx)$/;
	
	make_path($report_dir);
	my $jarname = $compiler_path;
	my $srcname = "$report_dir/$basename.compiled";
	my $pdfname = "$report_dir/$basename$ext";
	
	my $dbpass = $db_password;
	$dbpass =~ s/([\$\\\"\'\|\#\,\.\<\>\?\!])/\\$1/g;
	
	my $serverdetails = "jdbc:mysql://$data_source:3306/$database?zeroDateTimeBehavior=convertToNull $db_username $dbpass";
	
	#print "Filling report: 'java -jar $jarname report $srcname $pdfname $serverdetails $paramlist'<br>\n";
	return qx{ java -jar $jarname report $srcname $pdfname $serverdetails $paramlist };
}


##############################################################################
# UPLOAD AN IMAGE TO THE SERVER AND RETURN ITS NEW PATH
# ---> $query
# ---> $session
# ---> $reportid
# ---> \%imagedata{name, path, newpath}
# <--- \%imagedata{name, path, newpath}
##############################################################################
sub uploadImage
{
	my ($query, $session, $reportid, $imagedata) = @_;
	
	
	#FILE WASN'T ACTUALLY UPLOADED
	unless($query->param($imagedata->{name}))
	{
		$imagedata->{newpath} = "";
		return "";
	}
	
	my ($newfilename, $newfiledir, $newfileext) = fileparse($query->param($imagedata->{name}), '\..*');
	my ($filename, $filedir, $fileext) = fileparse($imagedata->{path}, '\..*');
	my $uploadhandle = $query->upload($imagedata->{name});
	
	
	#GET A UNIQUE VERSION OF THE FILENAME
	my $oldbasename = "$filename$fileext";
	my $basename = "$reportid-$newfilename$newfileext";
	
	
	#SAVE THE FILE
	make_path($report_dir);
	open(UPLOADFILE, ">$report_dir/$basename") or die $!;
	binmode UPLOADFILE;
	while(<$uploadhandle>) { print UPLOADFILE; }
	close UPLOADFILE;
	
	
	#IF THE FILENAME CHANGES, DELETE THE OLD FILE (HOUSECLEANING)
	if($oldbasename ne $basename) { unlink("$report_dir/$oldbasename"); }
	
	
	$imagedata->{newpath} = $basename;
	return $imagedata;
}



##############################################################################
# RETURN A LIST OF ALL IMAGES AND PARAMETERS REFERENCES BY THE REPORT
# ---> $reportfilename
# <--- array(\@array(\%imageinfo), \@array(\%parameterinfo)))
##############################################################################
sub getDependencyList
{
	my ($report) = @_;
	my @imglist;
	my @paramlist;
	my $matchlevel = 0;;

	open INFILE, "<$report" or die "Couldn't read '$report' ($1)";
	
	
	while(my $line = <INFILE>)
	{
		#GRAB IMAGE FILENAMES
		if($line =~ m/^\s*<imageExpression>\s*<!\[CDATA\[(.*)\]\]><\/imageExpression>\s*$/ && ($matchlevel == 0))
		{
			$line = $1;
			my ($filename, $filedir, $fileext) = fileparse($line, "\..*");
			
			$line = General::htmlSafe($line);
			$filename = General::htmlSafe($filename);
			$fileext = General::htmlSafe($fileext);
			
			my %imageinfo = (
				name			=>	"img" . @imglist,
				path			=>	$line,
				shortpath	=>	"$filename$fileext",
				);
			push(@imglist, \%imageinfo);
		}
		
		#DON'T MATCH ON SUBREPORTS, SINCE THEY AREN'T PROMPTABLE ANYWAY
		elsif($line =~ m/^\s*<subDataset\s.*?([\/]?)>\s*$/)
		{
			#DON'T INCREASE THE MATCHLEVEL IF THE EXPRESSION TERMINATES ITSELF
			$matchlevel++ unless $1 eq '/';
		}
		elsif($line =~ m/^\s*<\/subDataset>\s*$/)
		{
			$matchlevel--;
		}
		
		elsif(($line =~ m/^\s*<parameter.*isForPrompting=\"false\".*?([\/]?)>.*$/)) {}
		#GRAB PARAMETERS
		elsif(($line =~ m/^\s*<parameter.*name=\"(\S*)\".*class=\"java.[^.]*.(\S*)\".*?([\/]?)>*$/) && ($matchlevel == 0))
		{
			my $parametername = $1;
			my $parameterclass = $2;
			my $selfterminating = $3 eq '/';
			
			my $parameterdescription = "";
			my $parameterdefault = "";
			
			if(!$selfterminating)
			{
				#SEARCH FOR PARAMETERS; LIMIT THE LOOP JUST FOR SAFETY
				my $loops = 1000;
				while($loops-- > 0)
				{
					$line = <INFILE>;
					if($line		=~ m/^\s*<parameterDescription>\s*<!\[CDATA\[(.*)\]\]><\/parameterDescription>\s*$/)		{ $parameterdescription = $1	; }
					elsif($line	=~ m/^\s*<defaultValueExpression>\s*<!\[CDATA\[(.*)\]\]><\/defaultValueExpression>\s*$/)	{ $parameterdefault = $1; }
					elsif($line	=~ m/^\s*<\/parameter>\s*$/)		{	$loops = 0; }
				}
			
			}
			
			#IF THE PARAMETER DOESN'T HAVE A DESCRIPTION, USE ITS NAME
			unless($parameterdescription) { $parameterdescription = $parametername; }
			$parameterdefault =~ s/\"//g;			#REMOVE ANY QUOTES [USUALLY THERE FOR iREPORT'S BENEFIT, NOT OURS]
			
			$parametername = General::htmlSafe($parametername);
			$parameterdefault = General::htmlSafe($parameterdefault);
			$parameterdescription = General::htmlSafe($parameterdescription);
			$parameterclass = General::htmlSafe($parameterclass);
			
			my %parameter = (
					name				=> $parametername,
					defaultvalue		=> $parameterdefault,
					description		=> $parameterdescription,
					class			=> $parameterclass,
					);
			push(@paramlist, \%parameter);
		}
	}

	return (\@imglist, \@paramlist);
}





##############################################################################
# CHANGE THE PATHNAMES OF ALL FILES REFERRED TO IN A GIVEN REPORT
# BASED ON A PROVIDED SET OF PATHS
# ---> $reportname
# ---> \@array(%imageinfo{})
##############################################################################
sub modifyDependencyList
{
	my ($report, $imglist) = @_;
	
	my @secondlist = @$imglist;		#BYVAL THE LIST
	
	open INFILE, "<$report" or die $1;
	open OUTFILE, ">$report.new" or die $1;
	
	while(<INFILE>)
	{
		my $line = $_;
		if($line =~ m/imageExpression/)
		{
			my $newimage = shift(@secondlist)->{newpath};
			if($newimage ne "")
			{
				$line =~ s/^(.*)CDATA\[\".*\"\]\]><\/imageExpression>$/$1CDATA\[\"$newimage\"\]\]><\/imageExpression>/;
			}
		}
		print OUTFILE $line;
	}
	
	close OUTFILE;
	close INFILE;
	
	move("$report.new", $report) or die $!;
}
