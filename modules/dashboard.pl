#!/usr/local/bin/perl
package Dashboard;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use JSON qw( decode_json );
use Template;
use Data::UUID;
use XML::Simple;
use Data::Dumper;
use Date::Simple ('date', 'today');
use JSON;
use POSIX qw/strftime/;
use Time::Local;
use POSIX qw( strftime );
# Home grown
use lib 'modules';
use Validate;
use Carp qw/carp cluck/;
use CTSConstants;


require "databasedashboard.pl";
require "databaseJob.pl";
require "databaseOpcos.pl";
require "databaseCustomerUser.pl";
require "databaseUser.pl";
require "config.pl";
require "general.pl";

my $longQueryPeriod = Config::getConfig("longqueryperiod") || 10; # Log SQL query in db_getJobs if > this period in seconds
my $rowscount       = Config::getConfig("jobrowslimit") || 1000;
my $dashboardAccessibleUsers    = Config::getConfig("dashboardAccessibleUsers");


##################################################################
# the template object 
##################################################################
my $tt = Template->new( {
        RELATIVE => 1,
} ) || die "$Template::ERROR\n";



###########################################################################
### dashboardCalendar: To get the all days events
###########################################################################
sub calendar {

	my $query               = $_[0];
    my $session             = $_[1];
    my $mode                = $query->param("mode");
    my $opcoid              = $query->param("opcoid");
    my $selectedUserSkillID = $query->param("selectedUserSkillID");
	my $calendarEvents_ref 	= ECMDBDashboard::db_getCalendarEvents( $opcoid, $selectedUserSkillID );
	my $vars = {
        event    			=> $calendarEvents_ref,
    };
    if ( $mode eq "json") {
        my $json = encode_json ( $vars );
        print $json;
        #open(DATA,"+>>calendar.json") || die "Couldn't open file file.txt, $!";
        #print DATA $json;
        #close(DATA);
        return $json;
    }

} #dashboardCalendar


##############################################################################
##       Add Event to Calendar
###############################################################################
sub calendarEdit {

	my $query       		= $_[0];
    my $session     		= $_[1];
    my $type				= $query->param('type');
    my $mode				= $query->param( "mode" );
    my $users      			= $query->param( 'users' );
	my $userid      		= $session->param('userid');
    my $opcoID      		= $query->param( 'opcoid' );
	my $eventID				= $query->param( 'eventID' );
	my $eventEnds			= $query->param( 'eventEnds' );
	my $allDayFlag			= $query->param( 'allDayFlag' );
	my $eventTitle			= $query->param( 'eventTitle' );
	my $eventHolder			= $query->param( 'eventHolder' );
	my $eventStarts			= $query->param( 'eventStarts' );
    my $deleteEventID		= $query->param('deleteEventID');
	my $calendarEventLists 	= undef;
	my $projectDesc			= $query->param( 'projectDesc' );
	my $projectComment		= $query->param( 'comment' );
	my $updateComment	= undef;
	if ( $type =~ /^get_events_list$/ig ) {
		($calendarEventLists,$updateComment) = ECMDBDashboard::db_getcalendarEventLists($eventID);
	} elsif ( $type =~ /^add_event$/ig ) {
		my @usersList	= split( ';', $users );
		foreach my $user ( @usersList ) {	
			if ( $user ne "" ) {
				my ( $userFullName, $loginName )	= ( split ( ', ', $user ) )[ 0, 1 ];
				my $userID     = ECMDBUser::db_getUserId($loginName);
				ECMDBDashboard::db_addcalendarevent( $userID, $eventTitle, $eventStarts, $eventEnds, $allDayFlag, $userFullName, $opcoID, $projectDesc, $projectComment );
			} 
		}
	} elsif ( $type =~ /^update$/ig ) {
    	ECMDBDashboard::db_updatedcalendarevent( $eventTitle, $eventStarts, $eventEnds, $allDayFlag, $eventID, $projectDesc, $projectComment );
	} elsif ( $type =~ /^remove$/ig ) {
    	ECMDBDashboard::db_dashboardcalendardelete( $deleteEventID );
	}


=comm

	foreach my $user ( @users ) {

		if($user =~/,/g) {
        	@guser = split(', ',$user); #seperate the first,lastname and username
            $gusername=$guser[1]; #getting the firstname, $guser[1] lastname
            # to get username  @guser = split(', ' , $user); $gusername=$guser[1]; $gusername=$guser[0] will display both fname lname
            $flname = $guser[0];
        } else {
               	$gusername=$user;
               }
		if($type == "add_event"){
	        ECMDBDashboard::db_addcalendarevent( $who, $title, $start, $end, $allday,$flname, $opcoID );
		}
		if($type == "update") {
	        ECMDBDashboard::db_updatedcalendarevent($title,$start,$end,$allday,$eventid);
		}
   	}
  	if($type == "remove"){
		ECMDBDashboard::db_dashboardcalendardelete($delete_event);
	}
=cut
	my $vars = {
		eventID  			=> $eventID,
		eventEnds  			=> $eventEnds,
		allDayFlag  		=> $allDayFlag,
		eventTitle  		=> $eventTitle,
		eventHolder  		=> $eventHolder,
		eventStarts  		=> $eventStarts,
		calendarEventLists  => $calendarEventLists,
		projectDesc			=> $projectDesc,
		comment				=> $projectComment,
		updateComment	=> $updateComment,
    };

    if ( $mode eq "json" ) {
	    my $json = encode_json ( $vars );
        print $json;
        return $json;
	}
} #calendarEdit

##########################################################################################
# dasshboardFilter: To filter resource
##########################################################################################
sub dasshboardFilter() {

	my $query				= $_[0];
    my $session             = $_[1];
    my $type                = $query->param('type');
    my $mode                = $query->param( "mode" );	
    my $opcoID              = $query->param('opcoID');
    my $fromId				= $query->param( "fromId" );	
	my $userid				= $session->param( 'userid' );
	my $flag				= "Yes";

	my @media				= ();
	my @agencies			= ();
	my @userskills			= ();
	my @departmentList		= ();

	@media                  = ECMDBJob::db_getmedia( $opcoID, $fromId );
	@userskills  			= ECMDBOpcos::getUserskills();
	@agencies   			= ECMDBDashboard::db_getCustomers($opcoID);
	@departmentList			= ECMDBDashboard::db_getDepartments( $opcoID );

	my $vars = {
        mediaLists  		=> \@media,
        userSkillLists  	=> \@userskills,
        customerLists  		=> \@agencies,
        departmentList 		=> \@departmentList,
    };

    if ( $mode eq "json" ) {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
} #dasshboardFilter
#############################################################################
# getUserJobs
#
#############################################################################
sub getUserJobs{

    my ($userid) = @_;

    my $userJobs_ref = $Config::dba->hashed_process_sql("SELECT Job_number from assignmenthistory where STATUS IN ( 'A', 'W') and ASSIGNEEID = ( ? )",[ $userid ]);

    my @userJobs_arrayref   = ();
    my @userJobs            = ();

    if ( $userJobs_ref ) {
        @userJobs_arrayref = @$userJobs_ref;
    }

    foreach my $job_hashref ( @userJobs_arrayref ) {
        push ( @userJobs, $job_hashref->{'Job_number'} );
    }

    if ( @userJobs ) {
        return  join(',', @userJobs);
    } else {
        return -1;
    }
}
#############################################################################
#       validStringFilter()
#
##############################################################################
sub validMysqlStringFilter
{
    my ($filter) = @_;

    return $filter =~ m/['"]/ ? 0 : 1;
}

sub newDashboard {

    my $query   = $_[0];
    my $session = $_[1];

    my ($fromId, $op, $pending, $inprogress, $rejected, $waitingforQC, $overschedule, $actdboxmedia, $actopcoid, $estimatedHrs, $actualHrs);
    my ( $pendingEstimateHrs, $inprogressEstimateHrs, $rejectedEstimateHrs, $waitingforQCEstimateHrs, $totalJobsEstHrs, $overscheduleEstimateHrs );
    my (@useropcos, @media);

    my $userid                  = $session->param( 'userid' );
    my $username                = ECMDBUser::db_getUsername( $userid );
    my $mode                    = $query->param( "mode" ) || '';
    my $dboxcust                = $session->param( "dboxcust" );
    my $dboxmedia               = $session->param( "dboxmedia");
    my $dboxdept                = $session->param( "actdboxdept");
    my $dboxop                  = $session->param( "dboxop");
    my $sidemenu                = $query->param( "sidemenu" );
    my $roleid                  = $session->param( 'roleid' );
    my $box                     = $query->param( "box" );
    my $totalJobs               = 0;
    my $selectedSkill           = $query->param( "selectedUserSkill" );
    my $cust                    = $query->param( "selectedCust" );
    my $dept                    = $query->param( "selectedDept" );
    my $media                   = $query->param( "selectedMedia" );
    my $selectedDay             = $query->param( "selectedDay" );
    my $selectedDropDownOpcoID  = $query->param( "selectedDropDownOpcoID" );

    if ( $roleid !~  /$dashboardAccessibleUsers/ ) {
        General::NoAccess( );
        return;
    } else {

        if ( $query->param( "opcoid" ) )
        {
            $op = $query->param( "opcoid" );
        }
        else
        {
            $op = $session->param( "opcoid" );
        }
        ##Since graph is designed for seven days it has been defaulted to 7.
        if ( !looks_like_number( $fromId ) ) {
            #$fromId = 365;
            $fromId = "";
        }

        @useropcos              = ECMDBOpcos::db_getUserOpcos( $userid );
        @media                  = ECMDBJob::db_getmedia( $op, $fromId );
        $op                     = $selectedDropDownOpcoID       if ( $selectedDropDownOpcoID ne "" );

        ( $pending, $pendingEstimateHrs )           = ECMDBDashboard::boxcountshow($op, $cust, $dept, $media, "U,A",$selectedDay);
        ( $rejected, $rejectedEstimateHrs )         = ECMDBDashboard::boxcountshow($op, $cust, $dept, $media, "R",$selectedDay);
        ( $inprogress, $inprogressEstimateHrs )     = ECMDBDashboard::boxcountshow($op, $cust, $dept, $media, "W,P",$selectedDay);
        ( $waitingforQC, $waitingforQCEstimateHrs ) = ECMDBDashboard::boxcountshow($op, $cust, $dept, $media, "1",$selectedDay);
        ( $overschedule, $overscheduleEstimateHrs ) = ECMDBDashboard::boxcountshow($op, $cust, $dept, $media, "Over",$selectedDay);
        ( $totalJobs, $totalJobsEstHrs )            = ECMDBDashboard::boxcountshow($op, $cust, $dept, $media, "Totjobs",$selectedDay);

        my $db_resources        = undef;
        my $userSkills          = undef;
        my $totalResource       = undef;
        my @userskills          = ();
        $estimatedHrs           = 0;
        $actualHrs              = 0;

        if ( $op ) {
            ($totalResource, $db_resources)     = ECMDBDashboard::db_avlresources($op,$selectedSkill,$selectedDay);
            @userskills  = ECMDBOpcos::getUserskills();

            #$estimatedHrs = GraphCalc::graphCalc($op,$cust,$media,"estimated");
            #$actualHrs    = GraphCalc::graphCalc($op,$cust,$media,"actual");
        }

        my $relaxroles          = $session->param( 'relaxroles' );

        $pendingEstimateHrs     = formatDashboardEstHrs( $pendingEstimateHrs );
        $rejectedEstimateHrs    = formatDashboardEstHrs( $rejectedEstimateHrs );
        $inprogressEstimateHrs  = formatDashboardEstHrs( $inprogressEstimateHrs );
        $waitingforQCEstimateHrs= formatDashboardEstHrs( $waitingforQCEstimateHrs );
        $overscheduleEstimateHrs= formatDashboardEstHrs( $overscheduleEstimateHrs );
        $totalJobsEstHrs        = formatDashboardEstHrs( $totalJobsEstHrs );
        ECMDBJob::db_recordJobAction(0, $userid, CTSConstants::DASHBOARD );
        my $vars = {
                pendingEstimateHrs      => $pendingEstimateHrs,
                inprogressEstimateHrs   => $inprogressEstimateHrs,
                rejectedEstimateHrs     => $rejectedEstimateHrs,
                waitingforQCEstimateHrs => $waitingforQCEstimateHrs,
                overscheduleEstimateHrs => $overscheduleEstimateHrs,
                useropcos               => \@useropcos,
                opcoid                  => $op,
                medias                  => \@media,
                pending                 => $pending,
                inprogress              => $inprogress,
                rejected                => $rejected,
                waitingforQC            => $waitingforQC,
                overschedule            => $overschedule,
                actdboxmedia            => $media || "All" ,
                actopcoid               => $op,
                sidemenu                => $sidemenu,
                resourcescnt            => $totalResource,
                resources               => $db_resources,
                estimatedHrs            =>$estimatedHrs,
                actualHrs               =>$actualHrs,
                roleid                  =>$roleid,
                relaxroles              => $relaxroles,
                userSkills              => \@userskills,
                totalJobs               => $totalJobs,
                username                => $username,
                totalJobsEstHrs         => $totalJobsEstHrs,
        };

        if ( $mode eq "json")
        {
            my $json = encode_json ( $vars );
            print $json;
            return $json;
        }
		$vars->{ resource_dashboard } = 1; # So we can drag in newcustom.js in header.html and exclude custom.js

        General::setHeaderVars( $vars, $session );
        $tt->process( 'ecm_html/newdashboard.html', $vars )
                || die $tt->error(), "\n";
    }
}
##############################################################################
##      To update the Resources based on the selected Skill value. 
###############################################################################
sub updateResource {
    my $query               = $_[0];            
    my $session             = $_[1];            
    my $selectedSkill       = $query->param( "selectedUserSkill" );
    my $mode                = $query->param( "mode" );
    my $opcoid              = $query->param( "opcoid" );
    my $selectedDay         = $query->param( "selectedDay" );
#   my $current_date        = strftime('%Y-%m-%d', localtime());

    my $cust                    = $query->param( "selectedCust" );
    my $dept                    = $query->param( "selectedDept" );
    my $media                   = $query->param( "selectedMedia" );
    my ($availableResource, $db_resources)  = ECMDBDashboard::db_avlresources( $opcoid, $selectedSkill,$selectedDay );
    my ( $pending, $pendingEstimateHrs )           = ECMDBDashboard::boxcountshow($opcoid, $cust, $dept, $media, "U,A",$selectedDay);
    my ( $rejected, $rejectedEstimateHrs )         = ECMDBDashboard::boxcountshow($opcoid, $cust, $dept, $media, "R",$selectedDay);
    my ( $inprogress, $inprogressEstimateHrs )     = ECMDBDashboard::boxcountshow($opcoid, $cust, $dept, $media, "W,P",$selectedDay);
    my ( $waitingforQC, $waitingforQCEstimateHrs ) = ECMDBDashboard::boxcountshow($opcoid, $cust, $dept, $media, "1",$selectedDay);
    my ( $overschedule, $overscheduleEstimateHrs ) = ECMDBDashboard::boxcountshow($opcoid, $cust, $dept, $media, "Over",$selectedDay);
    my ( $totalJobs, $totalJobsEstHrs )            = ECMDBDashboard::boxcountshow($opcoid, $cust, $dept, $media, "Totjobs",$selectedDay);
        $pendingEstimateHrs     = formatDashboardEstHrs( $pendingEstimateHrs );
        $rejectedEstimateHrs    = formatDashboardEstHrs( $rejectedEstimateHrs );
        $inprogressEstimateHrs  = formatDashboardEstHrs( $inprogressEstimateHrs );
        $waitingforQCEstimateHrs= formatDashboardEstHrs( $waitingforQCEstimateHrs );
        $overscheduleEstimateHrs= formatDashboardEstHrs( $overscheduleEstimateHrs );
        $totalJobsEstHrs        = formatDashboardEstHrs(  $totalJobsEstHrs );
    my $vars = { 
        resources =>$db_resources,
        pendingEstimateHrs      => $pendingEstimateHrs,
        inprogressEstimateHrs   => $inprogressEstimateHrs,
        rejectedEstimateHrs     => $rejectedEstimateHrs,
        waitingforQCEstimateHrs => $waitingforQCEstimateHrs,
        overscheduleEstimateHrs => $overscheduleEstimateHrs,
        totalJobs               => $totalJobs,
        pending  =>$pending,
        inprogress              => $inprogress,
        rejected                => $rejected,
        waitingforQC            => $waitingforQC,
        overschedule            => $overschedule,
        totalJobsEstHrs         => $totalJobsEstHrs,
    };  

    if ( $mode eq "json" ) {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
} #updateResource

###################################################################################
#formatDashboardEstHrs: To format the estimate hours with decimal points
###################################################################################
sub formatDashboardEstHrs {

    my $estimateHours   = $_[0];

    if ( defined $estimateHours ) { 
        $estimateHours      = sprintf("%.2f", $estimateHours );
    } else {
        $estimateHours = '0.00'; 
    }   

    return $estimateHours;
} #formatDashboardEstHrs

1;
