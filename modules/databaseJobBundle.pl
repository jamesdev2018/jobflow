#!/usr/local/bin/perl
package ECMDBJobBundle;

use strict;
use warnings;

use Carp qw/carp/;

use lib 'modules';

use Validate;
use Net::FTP::File;
use Scalar::Util qw( looks_like_number );

require 'config.pl';

############################################################################
# getJobAssetTypes( job, customerid )
# returns hash, where the keys are asset type names and the values are anon hash with keys enabled and checked
# So uses table asset_types to construct query to see if the asset types are enabled and/or checked
# (so uses columns enabled, checked from asset_types) when building query against customer c and tracker3plus t3p
############################################################################
sub getJobAssetTypes
{
    my ( $jobnumber, $customerid ) = @_;

    my %assettype_hash;   # return hash, key is asset type, value anon hash { with keys checked, enabled }

    eval {
        Validate::job_number( $jobnumber );
        Validate::customerid( $customerid );

		# retrieve all asset types
		my $assettypes = $Config::dba->hashed_process_sql( "SELECT asset_type, name, enabled, checked FROM asset_types ORDER BY asset_type" );
		die "No asset types defined" unless ( defined $assettypes );

		my @cols;
		foreach my $t (@{ $assettypes } ) {
			push( @cols, $t->{ enabled } . ' AS ' . $t->{ asset_type } . '_enabled' );
			push( @cols, $t->{ checked } . ' AS ' . $t->{ asset_type } . '_checked' );
		}

		# retrieve enabled and checked columns wrt job number from tracker3plus and customers table
		my $data = $Config::dba->hashed_process_sql(
					'SELECT t.job_number, ' . join( ',', @cols ) . ' FROM  tracker3 t ' .
					' INNER JOIN tracker3plus tp  ON t.job_number = tp.job_number ' .
					' INNER JOIN customers cu  ON tp.customerid = cu.id ' .
					' WHERE  t.job_number = ?', [ $jobnumber ] );

		die "No data returned in query for asset types available" unless ( ( defined $data ) && ( scalar @{$data} ) > 0 );
		$data = $data->[ 0 ];  # Only interested in 1st row, which is a hash

		foreach my $t (@{ $assettypes } ) {
			my $a = $t->{ asset_type };
			$assettype_hash{ $a }{ checked } = $data->{ $a . '_checked' } || 0;
			$assettype_hash{ $a }{ enabled } = $data->{ $a . '_enabled' } || 0;
			$assettype_hash{ $a }{ label } = $t->{ name };
		}
    };
    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        $jobnumber = 'undef' unless defined $jobnumber;
        $customerid = 'undef' unless defined $customerid;
        die "getJobAssetTypes: job $jobnumber, cust $customerid, $error";
    }
    return \%assettype_hash;
}

############################################################################
# create( job, asset types, expiry period in days )
# adds a row to table job_bundle
# expiry period is either integer or date string
# returning list ( bundle id, bundle UUID )
############################################################################
sub create
{
	my ($jobnumber, $creator, $expiry_dt, $asset_types, $uuidstr, $job_filename) = @_;
	my $id;

	eval {
		Validate::job_number( $jobnumber );
		Validate::userid( $creator );
		$expiry_dt = 1 unless defined $expiry_dt;
		$job_filename = "" unless defined $job_filename;

		if ( looks_like_number( $expiry_dt ) ) {
			$Config::dba->process_sql(
                        "INSERT INTO job_bundle (job_number, creator, rec_timestamp, expiry_dt, asset_types, bundle_UUID, job_filename) ".
                        " VALUES(?, ?, now(), DATE_ADD(now(), INTERVAL $expiry_dt DAY), ?, ?, ?)",
                        [ $jobnumber, $creator, $asset_types, $uuidstr, $job_filename]);
		} else {
			$Config::dba->process_sql(
						"INSERT INTO job_bundle ".
                        " (job_number, creator, rec_timestamp, expiry_dt, asset_types, bundle_UUID, job_filename) ".
						" VALUES(?, ?, now(), ?, ?, ?, ?)",
                        [ $jobnumber, $creator, $expiry_dt, $asset_types, $uuidstr, $job_filename]);
		}

		($id) = $Config::dba->process_oneline_sql("SELECT last_insert_id()", undef, 1 );
		
		# throw error if id has not been created
		if(!$id){
			die "create: Job bundle has not been created for job: $jobnumber";
		}
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$jobnumber = 'undef' unless defined $jobnumber;
		$creator = 'undef' unless defined $creator;
        die "Error in create for job: $jobnumber, creator:$creator, $error";
    }

	return ( $id, $uuidstr );
}

############################################################################
# addRecipient( bundle_id, email, username, password )
# adds a row to table job_bundle_recipients
############################################################################
sub addRecipient
{
	my ($bundle_id, $email, $username, $password) = @_;

	eval {
		die "Invalid bundle uuid" unless defined $bundle_id;
		Validate::email( $email );

    	$Config::dba->process_sql(
							"INSERT INTO job_bundle_recipients ".
							"(bundle_id, email, username, password) ".
                        	"VALUES(?, ?, ?, ?)",
                        [$bundle_id, $email, $username, $password]);
	};
	if ( $@ ) {
		my $error = $@;
        $error =~ s/ at .*//gs;
		$bundle_id = 'undef' unless defined $bundle_id;
		$email = 'undef' unless defined $email;
        die "Error addRecipient: bundle id: $bundle_id, email: $email, $error";
    }

    return;
}

############################################################################
# getRecipientId( bundle_id, email )
# queries table job_bundle_recipients, returns recipient id or throw exception
############################################################################
sub getRecipientId
{
	my ($bundle_id, $email) = @_;
	my $id;

	eval {
		die "Invalid bundle uuid" unless defined $bundle_id;
		Validate::email( $email );

    	($id) = $Config::dba->process_oneline_sql(
							"SELECT id FROM job_bundle_recipients ".
                        	" WHERE bundle_id = ? AND email = ?", 
							[ $bundle_id, $email ]);

		die "Recipient id does not exist for '$email'" if ( ! $id );	# throw error if recipient id does not exist
	};
	if ( $@ ) {
		my $error = $@;
        $error =~ s/ at .*//gs;
		$bundle_id = 'undef' unless defined $bundle_id;
        $email = 'undef' unless defined $email;
		die "Error in getRecipientId: bundle - $bundle_id, email - $email, $error";
    }

    return $id;
}

############################################################################
# getBundle( bundle_id )
# throws exception if bundle doesnt exist or has expired
# returns ( job, asset types, creator, expiry date )
############################################################################
sub getBundle
{
	my ($bundle_id) = @_;
	my $rows;

	eval {
		die "Invalid bundle uuid" unless defined $bundle_id;

        $rows = $Config::dba->hashed_process_sql(
								"SELECT id, job_number, creator, expiry_dt, asset_types, bundle_UUID, job_filename ".
								" FROM job_bundle ".
                            " WHERE bundle_UUID = ? AND expiry_dt >= CURDATE()", [ $bundle_id ]);

		die "No data returned in query for get bundle" unless ( ( defined $rows ) && ( scalar @{$rows} ) > 0 );

		$rows = ${ $rows }[0];

		die "Invalid bundle id:  '$bundle_id'" unless defined $rows;
    };
    if ( $@ ) {
		my $error = $@;
        $error =~ s/ at .*//gs;
		$bundle_id = 'undef' unless defined $bundle_id;
        die "Error in getBundle for bundle: $bundle_id, $error";
    }
		
	return $rows;
}

############################################################################
# getAssetPath( job, jobpath, asset type )
# obtains the full filename for the asset, for use in the getjobasset script
#  returns hash ref { suffix, target, file_type, path, pattern, name }
# - path is the absolute directory path of the asset directory
# - pattern if defined, is a pattern to match files
# - name if defined is the file name (so wont be present if pattern is defined)
############################################################################
sub getAssetPath {
    my ( $jobnumber, $jobpath, $asset_type ) = @_;
	my $rows;

    eval {
        Validate::job_number( $jobnumber );
        die "Invalid Job path" unless defined $jobpath;
        die "Invalid Asset type" unless defined $asset_type;

		# get suffix, target, file_type to frame the path
		$rows = $Config::dba->hashed_process_sql(
                            " SELECT suffix, target, file_type FROM asset_types WHERE asset_type = ?", [ $asset_type ] );
        die "Invalid Asset type '$asset_type'" unless defined $rows;
        $rows = ${ $rows }[0];   # Should only be one row for a specific asset type

        my $file_type = $rows->{file_type} || 'undef';   # So blank '' will appear as 'undef'
        if ( $file_type eq 'all' ) {
             $rows->{pattern} = '*';   # Any file in the directory
        } elsif ( $file_type eq 'split' ) {
             $rows->{pattern} = '\_(R|L)HP\.pdf$';  # Any filename ending in either _RHP.pdf  or _LHP.pdf
        } elsif ( ( $file_type eq 'pdf' ) || ( $file_type eq 'zip' ) ) {
            $rows->{name} = $jobnumber . '.' .  $file_type;   # So either .pdf or .zip
        } else {
            die "Unexpected file_type '$file_type' for asset type '$asset_type'";
        }
        $rows->{path} = $jobpath . '/' .   $rows->{suffix};
    };
    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        die "Error in getAssetPath, $error";
    }
    return $rows;
}


############################################################################
# addFileToBundle( bundle_id, asset_type, filename )
# Add file to job_bundle_files table
############################################################################
sub addFileToBundle
{
	my ($bundle_id, $asset_type, $filename) = @_;
	my $id = undef;

	eval {
		Validate::id( $bundle_id );
		die "addFileToBundle: Invalid asset type" unless defined $asset_type;
		die "addFileToBundle: Invalid file name" unless defined $filename;

		$Config::dba->process_sql("INSERT INTO job_bundle_files
                        (bundle_id, asset_type, file_name)VALUES
                        (?, ?, ?)",
                        [ $bundle_id, $asset_type, $filename ]);

		($id) = $Config::dba->process_oneline_sql("SELECT last_insert_id()", undef, 1 );

		# throw error if file has not been added to table
		if(!$id){
			die "addFileToBundle: File has not been added for bundle: $bundle_id";
		}
	};
	if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        die "Error in addFileToBundle for bundle $bundle_id, $error";
    }

	return;
}

############################################################################
# checkedAssetsExist( job, jobpath, assets )
# checks to see whether the assets (list of asset types) exist under the job path
# returns hash (key is asset type, value is anon hash)
############################################################################
sub checkedAssetsExist {
    my ( $jobnumber, $jobpath, $opcoid, $assets_ref ) = @_;

    my %checkedassets;
    my $ftp;

    eval {
		# Validation
		Validate::job_number( $jobnumber );
        die "Invalid job path" unless defined $jobpath;
        die "Invalid job asset types" unless (defined $assets_ref) && (( ref $assets_ref ) eq "HASH" );

		# ftp initialisation
		my ( $ftpserver, $ftpusername, $ftppassword ) = db_getFTPDetails( $opcoid );
        die "FTP server not defined for opco $opcoid" unless $ftpserver;

		# Here comes my ftp connection
		$ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to FTP server '$ftpserver' : $@";
        $ftp->login( $ftpusername, $ftppassword ) or die "Cannot login to FTP server '$ftpserver' as '$ftpusername'", $ftp->message;
        $ftp->binary();

		# Yahoo.. i got my ftp connected, check jobpath directory exists and has at least 1 sub directory
		$ftp->cwd( $jobpath ) or die "Job path directory does not exist for job $jobnumber, " . $ftp->message;
        my @toplist = $ftp->ls();
        die "Job path directory is empty" unless ( scalar @toplist );
		
		foreach my $asset_type ( keys %$assets_ref ) {
			# initialise output hash - if assets found in below checks then asset_exists for asset will be 1 otherwise 0
			$checkedassets{ $asset_type } = { asset_exists => 0 };
			next unless ( ( $assets_ref->{ $asset_type }{ checked } || 0 ) == 1 );
            eval {          
                my $details = getAssetPath( $jobnumber, $jobpath, $asset_type ); 
				# Returns hash ref {  suffix, target, file_type, path, pattern, name } or throws exception
				my $found = 0;
                my $asset_dir = $details->{ path };
        
                if ( $ftp->exists( $asset_dir ) ) { # Check whether the asset directory exists or not
                    $ftp->cwd( $asset_dir ) or die $ftp->message . " to $asset_dir";
                    my @list = $ftp->ls();
                    die "Asset directory is empty" unless ( scalar @list );
        
                    my $file_type = $details->{ file_type } || 'undef';
                    my @files; # List of files found that match
        
                    if ( $file_type eq "all" ) {    # for type all 
                        foreach my $file ( @list ) {
                            $found = 1;
                            push( @files, $file ); 
                        } 
        
                    } elsif ( $file_type eq "split" ) { # for type split
                        foreach my $file ( @list ) { 
                             next unless $file =~ /^$jobnumber/;  
                             next unless $file =~ /\_(R|L)HP\.pdf$/;
                             push( @files, $file );
                         } 
                         $found = 1 if (( scalar @files ) == 2 );   # Expect 2 files

                    } elsif ( ( $file_type eq 'pdf' ) || ( $file_type eq 'zip' ) ) {    # for other file types
                        my $fn = $jobnumber . "." . $file_type; # high_res is handled by the general case
                        foreach my $file ( @list ) {
                            next unless ( $file eq $fn );
                            $found = 1;
                            push( @files, $file );
                        }

                    } else {
                        die "Unexpected ftp_type '$file_type'";
                    }
                    $checkedassets{ $asset_type }{ asset_exists } = $found;
                    $checkedassets{ $asset_type }{ files } = \@files;
                } # endif $ftp->exists
            };
            if ( $@ ) {
                my $error = $@;
                $error =~ s/ at .*//gs;
                $checkedassets{ $asset_type }{ error } = $error;
            }

        } # end foreach asset_type
        $ftp->quit();   # close ftp connection
        $ftp = undef;
    };
    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        $ftp->quit() if ( defined $ftp ); # close ftp connection
        die "Error in checkedAssetsExist, $error";
    }
    return \%checkedassets;
}

##############################################################################
# deleteBundle
#
##############################################################################
sub deleteBundle
{
	my $bundleid = @_;

	eval {
		Validate::id( $bundleid );
		my $row = getBundle( $bundleid );

		if ( defined $row ) {
			# delete job bundle files
			$Config::dba->process_sql("DELETE FROM job_bundle_files WHERE bundle_id = ?", [ $bundleid ]);

			# delete job bundle recipients
			$Config::dba->process_sql("DELETE FROM job_bundle_recipients WHERE bundle_id = ?", [ $bundleid ]);

			# delete job bundle history
			$Config::dba->process_sql("DELETE FROM job_bundle_history WHERE bundle_id = ?", [ $bundleid ]);

			# delete job bundle - last
			$Config::dba->process_sql("DELETE FROM job_bundle WHERE id = ?", [ $bundleid ]);
		}
	};
	if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        die "Error in deleteBundle, $error";
    }

	return 1;
}

##############################################################################
# db_getFTPDetails
#
##############################################################################
sub db_getFTPDetails
{
    my ($opcoid) = @_;
	Validate::opcoid( $opcoid );

    my ($ftpserver, $ftpusername, $ftppassword) = $Config::dba->process_oneline_sql("SELECT ftpserver, ftpusername, ftppassword FROM OPERATINGCOMPANY
                                                                            WHERE opcoid=?", [ $opcoid ]);

    return ($ftpserver, $ftpusername, $ftppassword);
}

##############################################################################
# getAssetTypeName
#
##############################################################################
sub getAssetTypeName
{
    my ($asset_type) = @_;

	die "getAssetTypeName: Invalid asset type" unless defined $asset_type;

    my ($assetname) = $Config::dba->process_oneline_sql("SELECT name FROM asset_types WHERE asset_type = ?", [ $asset_type ]);

    return ($assetname);
}

##############################################################################
# recordJobBundleHistory
#
##############################################################################
sub recordJobBundleHistory
{
    my ($bundle_id, $asset_type, $email) = @_;
    my $id;

    eval {
		Validate::id( $bundle_id );
        Validate::email( $email );
		die "recordJobBundleHistory: Invalid asset type" unless defined $asset_type;

		$Config::dba->process_sql(
				"INSERT INTO job_bundle_history ".
				" (bundle_id, asset_type, email, datetime) ".
				" VALUES(?, ?, ?, now())",
				[ $bundle_id, $asset_type, $email ]);

        ($id) = $Config::dba->process_oneline_sql("SELECT last_insert_id()", undef, 1 );

		if(!$id){
            die "recordJobBundleHistory: Job bundle history has not been recorded for bundle: $bundle_id";
        }
    };
    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        $bundle_id = 'undef' unless defined $bundle_id;
        die "Error in recordJobBundleHistory for bundle: $bundle_id, $error";
    }

    return $id;
}

##############################################################################
# getUserEmail
#
##############################################################################
sub getUserEmail
{
	my ($userid) = @_;

    die "getUserEmail: Invalid user" unless defined $userid;

    my ($email) = $Config::dba->process_oneline_sql("SELECT email FROM users WHERE id = ?", [ $userid ]);

    return ($email);
}

1;
