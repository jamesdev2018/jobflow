#!/usr/local/bin/perl
package CompareJobs;

use strict;
use warnings;

use JSON;
use Data::Dumper;
use File::Basename;
use Template;
use Scalar::Util qw( looks_like_number );

use lib 'modules';
use lib "Utils";

use DBAgent;
use CTSConstants;

# Please do not add jobs.pl

require "config.pl";
require "databaseJob.pl";
require "databaseUser.pl";
require "databaseOpcos.pl";
require "jobpath.pl";
require "htmlhelper.pl";
require "escomparejobs.pl";
require 'audit.pl';

our $tag_env = $ENV{"TAG_ENV"} || "DEV"; # One of DEV, Staging or Live
our $debug = 0;

##################################################################
# the template object 
##################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";

# Handler for action "contentcompare"

sub actionHandler {
	my ( $query, $session ) = @_;

	my $subaction = $query->param( "subaction" ) || "";
	my $results;

	# warn "comparejobs:actionHandler: sub $subaction\n";

	eval {
		if ( $subaction eq "contentcompareweb" ) {
			$results = contentCompareWeb( $session->param( 'userid' ), $session->param( 'username' ) );

 		} elsif ( $subaction eq "getversions" ) {
			my @jobs = $query->multi_param( 'jobs' );
			$results = ESCompareJobs::getVersions( $session->param( 'userid' ), \@jobs );

 		} elsif ( $subaction eq "compare" ) {
			my $job1 = $query->param( 'job1' );
			my $job2 = $query->param( 'job2' );
			my $version1 = $query->param( 'version1' ); # versionid rather than major version
			my $version2 = $query->param( 'version2' );
			$results = compare( $session->param( 'userid' ), $job1, $version1, $job2, $version2 );

		} else {
			die "Unrecognised subaction '$subaction' for action contentcompare";
		}
	};
	if ( $@ ) {
		my $error = $@;
		my $vars = { error => $error, params => $query->Vars };
		$results = encode_json( $vars );
	}
	print "$results\n" if ( defined $results );
	
}

# content compare is a separate web application, all this call does is log usage and return the URL
# although the user could easily bookmark it.

sub contentCompareWeb {
	my ( $userid, $username ) = @_;

	my $vars;
	eval {
		my $url = Config::getConfig( "contentcompareurl" );

		die "URL undefined for contentcompareurl" unless defined $url;

		my $job = undef; # Use undef for job number as it is integer, so "" would be invalid
		ECMDBJob::db_recordJobAction( $job, $userid, CTSConstants::CONTENT_COMPARE_WEB );
		Audit::auditLog( $userid, "TRACKER3PLUS", $job, "$userid ($username) using Content Compare Web" ); 

		$vars = { url => $url };
	};
	if ( $@ ) {
		$vars = { error => $@ };
	}
	my $json = encode_json ( $vars );
	return $json;
}

# compare - compare 2 versions of a job or 2 versions of 2 different jobs
# handler for "compare" sub action, called when user clicks on [ Compare ]

sub compare {
	my ( $userid, $job1, $version1, $job2, $version2 ) = @_;

	my $vars;
	my $alreadyExists = 0;

	warn "compare: userid $userid, j1 $job1, v1 $version1, j2 $job2, v2 $version2\n";
	eval {
		die "Job 1 is invalid" unless ( ( defined $job1 ) && looks_like_number( $job1 ) );
		die "Job 2 is invalid" unless ( ( defined $job2 ) && looks_like_number( $job2 ) );

		# This will have to change, to allow versionid to be "" for archived jobs

		die "Version 1 is invalid" unless ( ( defined $version1 ) && ( ( $version1 eq "" ) || looks_like_number( $version1 ) ) );
		die "Version 2 is invalid" unless ( ( defined $version2 ) && ( ( $version2 eq "" ) || looks_like_number( $version2 ) ) );

		my $v1_txt;
		my $v2_txt;
		if ( $version1 eq "" ) {
			$v1_txt = "High-Res";
			$version1 = undef;
		} else {
			$v1_txt = "v $version1";
		}
		if ( $version2 eq "" ) {
			$v2_txt = "High-Res";
			$version2 = undef;
		} else {
			$v2_txt = "v $version2";
		}

		if ( ECMDBJob::isCompareOnQueue( $userid, $job1, $version1, $job2, $version2 ) ) {
			$alreadyExists = 1;
		} else {
			ECMDBJob::enQueueCompare( $userid, $job1, $version1, $job2, $version2 );
			ECMDBJob::db_recordJobAction( $job1, $userid, CTSConstants::CONTENT_COMPARE_BATCH );
			Audit::auditLog( $userid, "TRACKER3PLUS", $job1, "$userid has initiatied Content Compare for $job1 $v1_txt and $job2 $v2_txt" );
		}

		$vars = { already => $alreadyExists, job1 => $job1, version1 => $version1, job2 => $job2, version2 => $version2 };
	};
	if ( $@ ) {
		$vars = { error => $@ };
	}

	my $json = encode_json ( $vars );
	return $json;
}

sub test_contentCompareWeb {

	my $userid = 330; my $username = "streanor";
	my $results;
	eval {
		$results = contentCompareWeb( $userid, $username );
		print "contentCompareWeb returned " . Dumper( $results ) . "\n";
	};
	if ( $@ ) {
		print "contentCompareWeb failed, exception $@\n";
	}
	return 0;
}

sub test_CompareJobs {

	my $userid = 330; my $username = "streanor";
	my $results;
	my $menuation;
	my $formPosted;
	my $mode = "json";
	my $jobs = [ "104000004" ];
	my $compJobID;
	my $selectedversions;

	# No selections, but no formPosted either, just returns available versions

	eval {
		$results = CompareJobs( $userid, $menuation, $formPosted, $mode, $jobs, $compJobID, $selectedversions );
		print "\nCompareJobs returned " . Dumper( $results ) . "\n";
	};
	if ( $@ ) {
		print "\nCompareJobs failed, exception $@\n";
	}

	# Selected 2 versions of same job

	$menuation = "yes";
	$formPosted = "yes";
	$selectedversions = [ "1", "2" ];
	eval {
		$results = CompareJobs( $userid, $menuation, $formPosted, $mode, $jobs, $compJobID, $selectedversions );
		print "\nCompareJobs returned " . Dumper( $results ) . "\n";
	};
	if ( $@ ) {
		print "\nCompareJobs failed, exception $@\n";
	}

	# Selected one version + other job

	$selectedversions = [ "2" ];
	$compJobID = "104000013";
	eval {
		$results = CompareJobs( $userid, $menuation, $formPosted, $mode, $jobs, $compJobID, $selectedversions );
		print "\nCompareJobs returned " . Dumper( $results ) . "\n";
	};
	if ( $@ ) {
		print "\nCompareJobs failed, exception $@\n";
	}

	return 0;
}

sub test_compare {
	my $userid = 330; 
	my $results;
	my $job1 = '104000004';
	my $version1 = '1';
	my $job2 = '104000004';
	my $version2 = '2';
	$debug = 1;

	eval {
		$results = compare( $userid, $job1, $version1, $job2, $version2 );
		print "compare returned " . Dumper( $results ) . "\n";
	};
	if ( $@ ) {
		print "compare failed, exception $@\n";
	}
	return 0;
}
#exit  __PACKAGE__->test_contentCompareWeb() unless caller();
#exit  __PACKAGE__->test_CompareJobs() unless caller();
#exit  __PACKAGE__->test_compare() unless caller();

1;

