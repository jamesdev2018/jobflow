package ECMDBRepoJob;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );

use lib 'modules';

require "config.pl";

my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");

my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");

##############################################################################
#      db getOpcoid 
#
##############################################################################
sub db_getBrokenJobs
{
	my $dbh 	= DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql 	= qq{
		SELECT t.job_number, t.checkoutstatus, loc_oc.opcoid, loc_oc.opconame, loc_oc.domainname, loc_oc.wippath, orig_oc.opcoid, orig_oc.opconame, orig_oc.domainname, orig_oc.wippath, isinrepo
		FROM (
			SELECT t3p.job_number, t3p.checkoutstatus, t3p.opcoid, 
				IF(t3p.location = 16, t3p.opcoid, IF(t3p.location <> 15, t3p.location, IF(rciq.opcoid, rciq.opcoid, IF(rcoq.opcoid, rcoq.opcoid, t3p.opcoid)))) AS curloc,
				((t3p.repocheckedin = 1) OR (t3p.repocheckedout = 1)) AS isinrepo FROM TRACKER3PLUS AS t3p
				
				LEFT OUTER JOIN REPOCHECKINQUEUE AS rciq ON rciq.job_number=t3p.job_number
				LEFT OUTER JOIN REPOCHECKOUTQUEUE AS rcoq ON rcoq.job_number=t3p.job_number
			) AS t
		INNER JOIN OPERATINGCOMPANY AS loc_oc ON loc_oc.opcoid=curloc
		INNER JOIN OPERATINGCOMPANY AS orig_oc ON orig_oc.opcoid=t.opcoid
		WHERE (t.checkoutstatus % 2) <> 0
			OR loc_oc.opcoid = 15
			OR loc_oc.opcoid = 16
		ORDER BY orig_oc.opconame, t.checkoutstatus
	};
	my $sth 	= $dbh->prepare( $sql );
	$sth->execute();
	
	my ($jobnumber, $checkoutstatus, $locopcoid, $locopconame, $locip, $locwip, $origopcoid, $origopconame, $origip, $origwip, $isinrepo);
	$sth->bind_columns(\$jobnumber, \$checkoutstatus, \$locopcoid, \$locopconame, \$locip, \$locwip, \$origopcoid, \$origopconame, \$origip, \$origwip, \$isinrepo);
	
	my @jobs;
	while($sth->fetch())
	{
		push(@jobs, { job_number => $jobnumber,
					  checkoutstatus => $checkoutstatus,
					  location => { opcoid => $locopcoid,
					  				opconame => $locopconame,
					  				ip => $locip,
					  				wip => $locwip,
					  			},
					  origin =>   { opcoid => $origopcoid,
					  				opconame => $origopconame,
					  				ip => $origip,
					  				wip => $origwip,
					  			},
					  });
	}

	$sth->finish();
	$dbh->disconnect();

	return @jobs;
}

sub db_set_checked_in
{
	my ($jobid, $location) = @_;
	
	$location = 16 unless $location;
	
	my $dbh 	= DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	
	#DELETE FROM THE THREE QUEUES
	my $sql 	= qq{ DELETE FROM REPOCHECKINQUEUE WHERE job_number=? };
	my $sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	$sql 	= qq{ DELETE FROM REPOCHECKOUTQUEUE WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	$sql 	= qq{ DELETE FROM REPOSENDJOBSQUEUE WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	
	#UPDATE THE LOCATION AND CHECKOUTSTATUS
	$sql 	= qq{ UPDATE TRACKER3PLUS SET location=?, checkoutstatus=4, repocheckedin=1, repocheckedout=1 WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($location, $jobid);
	
	$sql = qq{ UPDATE SHARDS SET location=? WHERE job_number=? };
	$sth = $dbh->prepare($sql);
	$sth->execute($location, $jobid);
	
	
	
	$sth->finish();
	$dbh->disconnect();
}

sub db_set_checked_out
{
	my ($jobid, $location) = @_;
	
	my $dbh 	= DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	
	#DELETE FROM THE THREE QUEUES
	my $sql 	= qq{ DELETE FROM REPOCHECKINQUEUE WHERE job_number=? };
	my $sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	$sql 	= qq{ DELETE FROM REPOCHECKOUTQUEUE WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	$sql 	= qq{ DELETE FROM REPOSENDJOBSQUEUE WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	
	#UPDATE THE LOCATION AND CHECKOUTSTATUS
	$sql 	= qq{ UPDATE TRACKER3PLUS SET location=?, checkoutstatus=2 WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($location, $jobid);
	
	$sql = qq{ UPDATE SHARDS SET location=? WHERE job_number=? };
	$sth = $dbh->prepare($sql);
	$sth->execute($location, $jobid);
	
	$sth->finish();
	$dbh->disconnect();
}



sub db_checkin
{
	my ($jobid, $location) = @_;
	
	my $dbh 	= DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	
	#DELETE FROM THE THREE QUEUES
	my $sql 	= qq{ DELETE FROM REPOCHECKINQUEUE WHERE job_number=? };
	my $sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	$sql 	= qq{ DELETE FROM REPOCHECKOUTQUEUE WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	$sql 	= qq{ DELETE FROM REPOSENDJOBSQUEUE WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	
	#UPDATE THE LOCATION AND CHECKOUTSTATUS
	$sql 	= qq{ INSERT INTO REPOCHECKINQUEUE (JOB_NUMBER, OPCOID, USERID, CHECKINGIN, REC_TIMESTAMP) VALUES
					(?, ?, 266, 0, now()) };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid, $location);
	
	
	
	$sth->finish();
	$dbh->disconnect();
}



sub db_checkout
{
	my ($jobid, $location) = @_;
	
	my $dbh 	= DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	
	#DELETE FROM THE THREE QUEUES
	my $sql 	= qq{ DELETE FROM REPOCHECKINQUEUE WHERE job_number=? };
	my $sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	$sql 	= qq{ DELETE FROM REPOCHECKOUTQUEUE WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	$sql 	= qq{ DELETE FROM REPOSENDJOBSQUEUE WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	
	#UPDATE THE LOCATION AND CHECKOUTSTATUS
	$sql 	= qq{ INSERT INTO REPOCHECKOUTQUEUE (JOB_NUMBER, OPCOID, USERID, CHECKINGOUT, REC_TIMESTAMP) VALUES
					(?, ?, 266, 0, now()) };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid, $location);
	
	
	$sth->finish();
	$dbh->disconnect();
}


sub db_get_job_info
{
	my ($jobid) = @_;
	
	my $dbh 	= DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql 	= qq{
		SELECT t.job_number, t.checkoutstatus, loc_oc.opcoid, orig_oc.opcoid
        FROM (
                SELECT t.job_number, t.checkoutstatus, IF(t.curloc = 16, t.opcoid, IF(t.curloc <> 15, t.curloc, IF(rciq.opcoid, rciq.opcoid, IF(rcoq.opcoid, rcoq.opcoid, t.opcoid)))) AS curloc, t.opcoid, isinrepo
                FROM (
                        SELECT t3p.job_number, t3p.checkoutstatus, t3p.location AS curloc, t3p.opcoid, ((t3p.repocheckedin = 1) OR (t3p.repocheckedout = 1)) AS isinrepo
                        FROM TRACKER3PLUS AS t3p
                        WHERE t3p.job_number = ?
                ) AS t
                LEFT OUTER JOIN REPOCHECKINQUEUE AS rciq ON rciq.job_number=t.job_number
                LEFT OUTER JOIN REPOCHECKOUTQUEUE AS rcoq ON rcoq.job_number=t.job_number
                WHERE t.curloc <= 16
        ) AS t
        INNER JOIN OPERATINGCOMPANY AS loc_oc ON loc_oc.opcoid=t.curloc
        INNER JOIN OPERATINGCOMPANY AS orig_oc ON orig_oc.opcoid=t.opcoid
        ORDER BY t.job_number, orig_oc.opconame, t.checkoutstatus
	};
	my $sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	my ($jobnumber, $checkoutstatus, $locopcoid, $origopcoid);
	$sth->bind_columns(\$jobnumber, \$checkoutstatus, \$locopcoid, \$origopcoid);
	
	my @jobs;
	$sth->fetch();

	$sth->finish();
	$dbh->disconnect();

	return { job_number => $jobnumber,
			  checkoutstatus => $checkoutstatus,
			  curopcoid => $locopcoid,
			  originopcoid => $origopcoid,
			 };
}

sub db_non_repo
{
	my ($jobid, $location) = @_;
	
	my $dbh 	= DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	
	#DELETE FROM THE THREE QUEUES
	my $sql 	= qq{ DELETE FROM REPOCHECKINQUEUE WHERE job_number=? };
	my $sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	$sql 	= qq{ DELETE FROM REPOCHECKOUTQUEUE WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	$sql 	= qq{ DELETE FROM REPOSENDJOBSQUEUE WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	
	#UPDATE THE LOCATION AND CHECKOUTSTATUS
	$sql 	= qq{ UPDATE TRACKER3PLUS SET location=opcoid, checkoutstatus=0, repocheckedin=0, repocheckedout=0, rec_timestamp=now() WHERE job_number=? };
	$sth 	= $dbh->prepare( $sql );
	$sth->execute($jobid);
	
	
	$sth->finish();
	$dbh->disconnect();
}

1;
