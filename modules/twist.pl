#!/usr/local/bin/perl
package Twist;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use XML::Simple;

# Home grown
use lib 'modules';

#use XMLResponse qw ( :statuses );
use Validate;

require "Mailer.pl";

sub send_support_email {
	my ( $body, $subject ) = @_;

	my $fromemail		= Config::getConfig("fromemail");
	my $supportemail	= Config::getConfig("supportemail");
	my $smtp_gateway	= Config::getConfig("smtp_gateway");

	Mailer::send_mail ( $fromemail, $supportemail, $body, $subject, { host => $smtp_gateway } );
}

##############################################################################
#       getTwistServer
#	Returns
#	""  - error in parsing XML or only server1 is up OR neither is up
#	"1" - (both servers up and server1.load <= server2.load)
#	"2" - (tag_env==DEV) OR (both servers up and server1.load > server2.load) OR (just server2 is up)
##############################################################################
sub getTwistServer {

	my $environment		= Config::getConfig("environment");
	warn "getTwistServer: getConfig('environment') returned undef" if ( ! defined $environment );

	return 2 if ( $environment eq "DEV" );

	my $twistServer1	= Config::getConfig("twistServer1" );
	my $twistKey1		= Config::getConfig("twistKey1" );
	my $twistServer2	= Config::getConfig("twistServer2" );
	my $twistKey2		= Config::getConfig("twistKey2" );

	my $twistcurl1 = `curl -s --connect-timeout 1 $twistServer1/twist/twistServer`;
	my $twistcurl2 = `curl -s --connect-timeout 1 $twistServer2/twist/twistServer`;

	my $xml = new XML::Simple;

	my $twistServerExt = "";
	my $twistserver1; # Nb the slightly differently names variables ie $twistServer1 v $twistserver1
	my $twistserver2;

	eval {
		$twistserver1 = $xml->XMLin( $twistcurl1, ForceArray => 1 );
		$twistserver2 = $xml->XMLin( $twistcurl2, ForceArray => 1 );
	};
	if ( $@ ) {
		return "";	
	}

	my $status1	= $twistserver1->{'TwistServer'}->{$twistKey1}->{'status'} ||'';
	my $status2	= $twistserver2->{'TwistServer'}->{$twistKey2}->{'status'} ||'';
	my $load1	= $twistserver1->{'TwistServer'}->{$twistKey1}->{'load'} ||0;
	my $load2	= $twistserver2->{'TwistServer'}->{$twistKey2}->{'load'} ||0;

	if ( $status1 eq "Up" && $status2 eq "Up" ) { #load balance if both servers are up
		if ( $load1 > $load2 ) {
			return "2";
		} else {
			return "";			
		}
	} else { #one or both are down
	
		if ( $status1 eq "Up" ) { # number 1 is still up
			return "";
		} else {
			# email support that an error has occured.
	
			my $body .= "Error occured in twist.pl::getTwistServer()\n";
			$body .= "Twist Server 1 appears to be down.\n";
			$body .= "\n\n";
			$body .= "Team JobFlow\n\n";
	
			#send_support_email ( $body, "Job Flow: Error (Twist Server 1 Down)" );
		}
		
		if ( $status2 eq "Up" ) { # number 1 is still up
			return "2";
		} else {
			# email support that an error has occured.
	
			my $body .= "Error occured in twist.pl::getTwistServer()\n";
			$body .= "Twist Server 2 appears to be down.\n";
			$body .= "\n\n";
			$body .= "Team JobFlow\n\n";
	
			#send_support_email ( $body, "Job Flow: Error (Twist Server 2 Down)" );
			
		}
		return ""; # if neither are up we send to 1
	}

	return "";
}

1;
