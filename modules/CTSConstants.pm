#!/usr/local/bin/perl

package CTSConstants;

use strict;
use warnings;

# Constants to use in call to ECMDBJob::db_recordJobAction

# from modules/sendjobs.pl
use constant ES_COMPARE => "ES compare"; # Doing a compare using Dalim ES
use constant ES_REVIEW => "ES review"; # Viewing an existing comparison in Dalim ES

# from modules/sendjobs.pl
use constant EMAIL_VENDOR => "email";			# Send by Vendor
use constant EMAIL_MEDIA_OWNER => "emailMediaOwner";	# Send by Media Owner
use constant EMAIL_CLIENT => "emailClient";		# Send by Email Client
use constant EMAIL_DIGITAL => "emailDigital";		# Send by Email Digital link
use constant SEND_BY_FTP => "ftp";			# Send by FTP
use constant SEND_BY_ADSEND => "adsend";		# Send by AdSend

# from modules/comparejobs.pl

use constant CONTENT_COMPARE_WEB => "Content Compare Web"; # Launching the Web interface to Content Compare (doesnt supply job number)
use constant CONTENT_COMPARE_BATCH => "compare";	# Queue a Content Compare Batch request

# from modules/databaseJob.pl (YET to change modules/databaseJob.pl to use the constants)

use constant DEPARTMENTS => "DEPARTMENTS";		# Used twice
use constant QUERYTYPEUPDATE => "QUERYTYPEUPDATE";
use constant JOB_BRIEF_COMMENTS => "job brief comments";
use constant GOOGLE_DOC_REPLACEMENT => "Google Doc Replacement";

# from stream/documentdownload.pl 

use constant DOC_DOWNLOAD => "docdownload";

# from stream/filedownload.pl

use constant FILE_DOWNLOAD => "filedownload";

# from stream/viewrenderedpdf.pl 

use constant RENDERED_PDF => "renderedpdf";

# from stream/jobbag.pl

use constant JOBBAG => 'jobbag';

# from modules/databaseOpcos.pl (YET to change modules/databaseOpcos.pl to use the constants)

use constant CHECKLIST => "Checklist";			# Showchecklistedit, supplies job number 0

# from modules/jobs.pl (YET to change modules/jobs.pl to use the constants)

use constant COLOUR_MANAGE => "colourmanage";		# Appears twice ... ShowJob and Fileupload
use constant TTP => "ttp";				# Appears twice ... ShowJob and requestTTPUpload
use constant LOCAL_UPLOAD => "localupload";		# in Fileupload
use constant HTTP_UPLOAD => "httpupload";		# in Fileupload
use constant OUTSOURCE => "outsource";			# Appears twice ... SetLocation and processRepoSendJobs
use constant MESSAGE => "message";			# in SendMessage
use constant OPEN_JOB => "openjob";			# in recordOpenJob
use constant DIGITAL_UPLOAD => "digitalupload";		# in digitalUpload
use constant WATCHLIST_NOTIFICATION => "watchlistnotification"; # Appears twice ...  # in Watchlist::Watchlistnotification ( email + message )
use constant DOC_UPLOAD => "docupload";			# in documentUpload
use constant DIGITAL_URL_TO_CMD => "digitalurltocmd";	# in renderPdf
use constant SEND_TO_CMD => "sendtocmd";		# in sendRenderedPdfToCMD
use constant SAVE_FORM => "saveform";			# in forms.pl

# from modules/users.pl (YET to change modules/users.pl to use the constants)

use constant CM_UPDATE => "cmupdate"; # Appears twice ... ApplyColumnManager and UpdateColumnManager
use constant SAVE_FILTER => "savefilter";		# in SaveFilter
use constant RENAME_FILTER => "renamefilter";		# in RenameFilter
use constant DELETE_FILTER => "deletefilter";		# in DeleteFilter
use constant JOBBUNDLE_DOWNLOAD => "jobbundledownload"; # for CDN file download

# from modules/dashboard.pl (YET to change modules/dashboard.pl to use the constants)

use constant DASHBOARD => "DASHBOARD"; #To track the dashboard actions in dashboard page.

use constant VIEWINDEXPAGE =>"Create Digital URL";
use constant SENDEMAILTOCLIENTS =>"Client Email";

# TODO fix PD-3531 (so 2 constants for ONE2EDIT)

# from modules/one2edit.pl (either create Master from INDD or Create Version from Master)

use constant ONE2EDIT_MASTER => 'one2edit_master';
use constant ONE2EDIT_VERSION => 'one2edit_version';

1;
