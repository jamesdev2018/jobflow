#!/usr/local/bin/perl
package General;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use HTML::Entities;
use JSON;
use Template;

# Home grown

use lib 'modules';

require "htmlhelper.pl";

#
# PLEASE NOTE
# This is a lightweight module, so please do not put anything in here that uses
# Users.pl, Opcos.pl or any other modules that requires jobs.pl
# Function SupportPage is now in its own support.pl
#

##############################################################################
# from config 
##############################################################################

##############################################################################
# the template object 
##############################################################################
my $tt = Template->new({
	RELATIVE => 1,
}) || die "$Template::ERROR\n";

# setHeaderVars - set variables in vars hash for any variable that appears
# in ecm_html/header.html, configSettings div

# PS do not add anything sensitive to this list as the user can easily inspect the html
 
sub setHeaderVars {
	my ( $vars, $session ) = @_;

	HTMLHelper::setHeaderVars( $vars, $session );
}

##############################################################################
#       NoAccess ()
#
##############################################################################
sub NoAccess {
	my ($message, $roleid) = @_;

	$roleid = 8 unless defined $roleid; # Default to Visitor

	my $vars 	= {
		roleid => $roleid,
		rolename => 'Visitor',
		message => $message,
	};
	
	$tt->process('ecm_html/noaccess.html', $vars )
    		|| die $tt->error(), "\n";
}

##############################################################################
#       Maintainence ()
#
##############################################################################
sub Maintainence {
	my $vars 	= {
	};
	
	$tt->process('ecm_html/maintainence.html', $vars )
    		|| die $tt->error(), "\n";
}

# Moved function Checklist to Checklist.pl

# Moved NewsSummary, ListNews, EditNews, validateNews to news.pl

##############################################################################
#       htmlEditFormat ()
#		Strip HTML Tags
##############################################################################
sub htmlEditFormat {
	my $text	= $_[0];

	if ( defined $text ) {
		$text		=~ s|<.+?>+||g; 	#enhanced
		#$text 		=~ s/[^A-Za-z0-9 ]*/ /g;
	}
	return $text;
}

sub htmlSafe
{
	my ($text) = @_;
	encode_entities($text);
	
	return $text;
}

##############################################################################
#       isAlphaNumeric ()
##############################################################################
sub isAlphaNumeric {
	my ($text) = @_;

	#allowing commas for multiple search
	return 0 if !defined $text;	
	return 0 if $text !~ m/^[a-zA-Z0-9 ,_-]+$/;
	
	return 1;
}

##############################################################################
#       isValidEmail ()
##############################################################################
sub isValidEmail {
	my ($email)	= @_;
	
	return 0 if !$email;
	return 0 if $email !~ m/^[-\w_.]+\@[-\w_.]+\.[a-zA-Z]{2,}$/;
	
	return 1;
}

## Only allow numeric IDs greater than 0
sub is_valid_id
{
	my ($id) = @_;
	my ($package, $filename, $line) = caller();
	my ($package2, $filename2, $line2) = caller(1);
	my $position = "$package:$line, from $package2:$line2";
	
	die "No ID supplied ($position)" if !defined $id;
	die "Supplied ID '$id' is not numeric ($position)" if !looks_like_number($id);
	die "ID '$id' is a negative value ($position)" if $id < 0;
	
	return 1;
}

sub is_valid_hash
{
	my ($hash) = @_;
	my ($package, $filename, $line) = caller();
	my ($package2, $filename2, $line2) = caller(1);
	my $position = "$package:$line, from $package2:$line2";
	
	die "No hash supplied ($position)" if !$hash;
	die "Supplied hash '$hash' contains invalid characters ($position)" if $hash !~ m/^[a-fA-F0-9]+$/;
	die "Supplied hash '$hash' is too long ($position)" if length($hash) > 32;
	die "Supplied hash '$hash' is too short ($position)" if length($hash) < 32;
	
	return 1;
}

sub is_valid_number
{
	my ($number) = @_;
	my ($package, $filename, $line) = caller();
	my ($package2, $filename2, $line2) = caller(1);
	my $position = "$package:$line, from $package2:$line2";
	
	die "No number supplied ($package:$line)" if !defined $number;
	die "Supplied number '$number' is not actually a number ($package:$line)" if !looks_like_number($number);
	
	return 1;
}

# Perl trim function to remove whitespace from the start and end of the string
sub trim($)
{
	my $string = shift;
	if ( defined $string ) {
		$string =~ s/^\s+//;
		$string =~ s/\s+$//;
	}
	return $string;
}
# Left trim function to remove leading whitespace
sub ltrim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	return $string;
}
# Right trim function to remove trailing whitespace
sub rtrim($)
{
	my $string = shift;
	$string =~ s/\s+$//;
	return $string;
}
sub writelog
{
        my $msg = $_[0];
        my $cmd = "echo \" ### $msg ###  \n\" >> DEBUGFILE";
        system($cmd);
}

sub adios {	
	my $string = "Was really good working with you all, wishing you all the best for the future. Khalid";
	exit(1);
}
##############################################################################
##############################################################################
1;
