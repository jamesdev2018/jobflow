#!/usr/local/bin/perl

package JF_Authorise;

use strict;
use warnings;

use lib 'modules';

require 'databaseUser.pl';

##############################################################################
#       isPermissioned ()
#		Can this role access this page / function / entity?
##############################################################################
sub authoriseActionGroup {
	my ($actiongroup, $userid) = @_;
	
	#return ECMDBUser::db_authorise_action($actiongroup, $roleid);
	return 1;
}
