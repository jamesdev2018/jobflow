#!/usr/local/bin/perl
package Mailer;

use strict;
use warnings;

use Net::SMTP;
use Time::Local;
use MIME::Base64;
use File::Basename;

# Home grown

# Please do not include code to get the default from a config function,
# keep this clean and common across Jobflow, Daemons, Masters and JRS

use constant DEFAULT_SMTP_GATEWAY => "gateway.dhl.com";

sub get_default_smtp_host {
	return DEFAULT_SMTP_GATEWAY;
}

######################################################################
#	send_mail
#
#	Arguments
#	- from address, string
#	- list of to address, either comma separated string or array of addresses
#	- body of mail, string, non blank
#	- subject, string, non blank
#	- options (optional Hash ref) { { debug, dieonerror, attachments, cc, html }
#		- debug
#		- dieonerror, on error throw exception otherwise return 0
#		- attachments, either filename, hash { filename, name, ... } or array of hash
#		- cc, string or array of string (addresses to CC)
#		- html, string (HTML alternative), body is plain text
#
#	Returns - 1, indicates success otherwise throws error
#
#	Throws exceptions on error
#
######################################################################
sub send_mail {
 
	my ( $from, $to, $body, $subject, $options ) = @_;

	my @to_addr;
	my @cc_addr; # List of addresses to CC to - see options
	my $smtp;
	my $debug = 0;
	my $die_on_error = 0;
	my $attachments;
	my $cc;
	my $html;

	my $smtp_host = get_default_smtp_host();

	if ( defined $options ) {
		$debug = $options->{debug} || 0;
		$die_on_error = $options->{dieonerror} || 0;
		$html = $options->{html} || ""; # Alternative HTML body text
		$attachments = $options->{attachments};
		$cc = $options->{cc};
		$smtp_host = $options->{host} if ( defined $options->{host} );
	}

	if ( $html ) {
		$html = '<body>' . $html . '</body>' if ( $html !~ /<body>/ );
		$html = '<html><head><meta http-equiv="Content-Type" content="text/html; charset="utf-8"></head>' . $html . '</html>' if ( $html !~ /<html>/ );
	}

	eval {

		die "No from address" unless ( ( defined $from ) && ( $from ne '' ) );

		die "No to address" unless ( defined $to );
		my $to_ref = ref ( $to );
		if ( $to_ref eq '' ) {
			@to_addr = split( ',', $to );
		} elsif ( $to_ref eq 'ARRAY' ) {
			@to_addr = @$to;
		} else {
			die "To arg must be string or array, ref type is $to_ref";
		}

		die "No body" unless ( ( defined $body ) && ( ref ( $body ) eq "" ) );
		die "No subject" unless ( ( defined $subject ) && ( ref ( $subject ) eq "" ) );

		if ( defined $cc ) {
			if ( ref( $cc ) eq '' ) {
				@cc_addr = split( ',', $cc );
			} elsif ( ref( $cc ) eq 'ARRAY' ) {
				@cc_addr = @$cc;
			} else {
				die "CC option must be string or array";
			}
		}

		# Check the attachments exist before starting the email
		# Expect a hash or array of hash

		my @attach_list;
		if ( defined $attachments ) {
			if ( ref( $attachments ) eq '' ) { # Just an attachment filename
				push( @attach_list, { filename => $attachments, contenttype => 'application/text' } );
			} elsif ( ref( $attachments ) eq 'HASH' ) {
				push( @attach_list, $attachments );
			} elsif ( ref( $attachments ) eq 'ARRAY' ) {
				@attach_list = @$attachments;
			} else {
				die "Attachments arg must be string, array or hash";
			}
		}
		foreach my $a ( @attach_list ) {
			my $fn = $a->{filename};
			if ( ( ! -e $fn ) || ( ! -r $fn ) ) {
				die "Could not read attachment '$fn'";
			}
		}
		if ( $debug ) {
			print "From: $from\n";
			print "To: " . join( ",", @to_addr ) . "\n";
			print "Subject: $subject\n";
			print "Body: $body\n";
			if ( defined $options ) {
				foreach my $key ( keys %$options ) {
					print " - $key " . $options->{$key} . "\n";
				}
			}
			foreach my $a ( @attach_list ) {
				print "Attachment " . $a->{filename} . "\n";
			}
		}

		# Open an SMTP session

		$smtp = Net::SMTP->new( $smtp_host,
				'Debug' => $debug,	# set to 1 to turn on debug messages
				'Hello' => 'jobflow.tagworldwide.com',
		);

		# die rather than return 0, indicative of perl module missing
		die "SMTP ERROR: Unable to open smtp session, gateway '$smtp_host'" if ( ! defined( $smtp ) || ! $smtp );

		# Pass the 'from' email address, exit if error
		if ( ! $smtp->mail( $from ) ) {
			die "Failed from, " . $smtp->message();
		}
			
		# Pass the recipient address(es)
		foreach( @to_addr ) {
			if ( ! $smtp->recipient( $_ ) ) {
				die "Failed to, " . $smtp->message();
			}
		}
		# This class doesnt make much of a distinction between to and cc
		foreach( @cc_addr ) {
			if ( ! $smtp->recipient( $_, { Notify => ['NEVER'], SkipBad => 1 } ) ) {
				die "Failed cc, " . $smtp->message();
			}
		}

		my $boundary_mixed = "boundary-mixed-1234";
		my $boundary_alt = "boundary-alt-5678";

		# Send the message

		$smtp->data();
		$smtp->datasend( "From: $from\n" );
		$smtp->datasend( "To: " . join(';', @to_addr) . "\n" );
		$smtp->datasend( "Cc: " . join(';', @cc_addr) . "\n" ) if ( scalar @cc_addr ) > 0;

		$smtp->datasend("Subject: $subject\n");
		$smtp->datasend("MIME-Version: 1.0\n");

		# If using alternation (text v html) with attachments, we start with the multipart/mixed envelope
		# and include the multipart/alternative inside.
		
		my $att_count = scalar( @attach_list );
		if ( $att_count > 0 ) {
			$smtp->datasend("Content-type: multipart/mixed;\n\tboundary=\"$boundary_mixed\"\n");
			$smtp->datasend("\n");
			$smtp->datasend("--$boundary_mixed\n");
		}

		if ( ! $html ) { # Just plain text body
			$smtp->datasend("Content-type: text/plain\n");
			$smtp->datasend("\n $body \n\n");

		} else {  # Plain text body + html alternative
			$smtp->datasend("Content-type: multipart/alternative;\n\tboundary=\"$boundary_alt\"\n");
			$smtp->datasend("\n");
			$smtp->datasend("--$boundary_alt\n");
			$smtp->datasend("Content-type: text/plain\n");
			$smtp->datasend("\n $body \n\n");
			$smtp->datasend("--$boundary_alt\n");
			$smtp->datasend("Content-type: text/html\n");
			$smtp->datasend("\n $html \n\n");
			$smtp->datasend("--$boundary_alt--\n");
		}

		if ( $att_count > 0 ) {
			$smtp->datasend("--$boundary_mixed\n"); # end of the text part

			for ( my $i = 0; $i < $att_count; $i++ ) {
				my $a				= $attach_list[ $i ]; # So we can trap last attachment
				my $fn				= $a->{filename};
				my $bn				= basename( $fn );
				my $content_id		= $a->{contentid} || '';
				my $content_type	= $a->{contenttype} || "application/text";
				my $content_disp	= $a->{contentdisposition} || "attachment";

				# open and read file, ...
				my ( $data, $bytesread, $buffer, $total ) = ( "", 0, "", 0 );
				open( DATA, "<", $fn ) || die "Could not open < $fn: $!";
				binmode( DATA );
				while ( ( $bytesread = sysread( DATA, $buffer, 1024 ) ) == 1024 ) {
					$total += $bytesread;
					$data .= $buffer;
				}
				if ($bytesread) {
					$data .= $buffer;
					$total += $bytesread;
				}
				close(DATA);
				print "Attachment $fn, length $total\n" if ( $debug );

				$smtp->datasend("Content-Type: $content_type; name=\"$bn\"\n");
				$smtp->datasend("Content-Transfer-Encoding: base64\n");
				$smtp->datasend("Content-Disposition: $content_disp; filename=\"$bn\"\n");
				$smtp->datasend("Content-ID:$content_id\n") if ( $content_id ne '' );
				$smtp->datasend("\n");
				$smtp->datasend( encode_base64($data) );

				# Last attachment is special case, has '--' after signature
				if ( $i == ( $att_count - 1 ) ) {
					$smtp->datasend("--$boundary_mixed--\n");
				} else {
					$smtp->datasend("--$boundary_mixed\n");
				}
			}
		}

		$smtp->dataend();
		$smtp->quit;
		$smtp = undef;
	};
	if ( $@ ) {
		my $error = $@;
		$smtp->quit if ( defined $smtp );
		die $error if ( $die_on_error );
		return 0;
	}
	return 1;
}

# must have this at the end for a require
1;

