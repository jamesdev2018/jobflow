#!/usr/local/bin/perl
package ECMDBSearch;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use Carp qw/carp cluck/;
use Time::HiRes qw/time/;
use Encode qw(decode encode);

# Home grown

use lib 'modules';

use Validate;

require "config.pl"; # Code uses $Config::dba
#require "databaseAudit.pl"; # just for db_recordJobAction TODO is it worth doing this to share common copy ? NO

# This is used by both db_checkSearchContent below and db_searchJobs in databaseJoblist.pl
# Validates $searchTypeID is 1..5, then validates $searchContents according to $searchTypeID
# Arguments
# - searchTypeID
# - searchContents
# Returns (column, entity, use_like)
# - column eg t3p.job_number (t3p is the table alias used in the query), used in WHERE clause
# - entity, descriptive text, eg "Job number"
# - use_like, 0 or 1, 0 for exact match, 1 for LIKE match
# Throws exception if validation fails.

sub checkSearchTypeAndContents {
	my ( $searchTypeID, $searchContents ) = @_;

	my $column = "";
	my $where = "";
	my $entity = "";
	my $use_like = 1;
	my $use_in = 0;

	if ( $searchTypeID  == 1 ) {
		die "Job number must be numeric '$searchContents'" if ((! defined $searchContents));# || !looks_like_number( $searchContents ));
		$column = "t3p.job_number";
		$entity = "Job number";
		$use_like = 0;
		$use_in = 1 if (!looks_like_number( $searchContents ));
	} elsif ( $searchTypeID == 2 )   {
		$column = "t.agencyref";
		$entity = "Legacy";
	} elsif ( $searchTypeID == 3 )   {
		$column = "t.businessunitref";
		$entity = "Business Unit Ref";
	} elsif ( $searchTypeID == 4 )   {
		$column = "t.agencyref";
		$entity = "Agency Ref";  # ?? like 2 ??
	} elsif ( $searchTypeID == 5 )   {
		die "Media choice id must be numeric '$searchContents'" if ((! defined $searchContents) || !looks_like_number( $searchContents ));
		$column = "t.MEDIACHOICEID";
		$entity = "Media Choice Id";
		$use_like = 0;
	} elsif ( $searchTypeID == 6 )   {
		$column = "btp.EDITCODE";
		$entity = "Edit Code";
	} elsif ( $searchTypeID == 7 )   {
		$column = "t3p.job_number";
		$use_in = 1;
		$use_like = 0;
		$entity = "Brief comments";
	}else {
		die "Invalid Search Type ID '$searchTypeID'";
	}
	return ( $column, $entity, $use_like, $use_in );
}

#########################################################################################
# db_checkSearchContent: To check whether the search job number exist.
#########################################################################################
sub db_checkSearchContent {
	my ( $searchTypeID, $searchContents, $searchLimit ) = @_;

	# Both job number and media choice id are numeric and require an exact match
	# The other types can be wildcarded (so if the use uses *, we use LIKE

	my ( $column, $entity, $use_like, $use_in ) = checkSearchTypeAndContents( $searchTypeID, $searchContents );

	# Always start with tracker3plus INNER JOIN tracker3, a job is not considered to exist
	# unless it exsist on both tables.

	my $sql = "SELECT t3p.job_number, t3p.status, t3p.opcoid, t3p.customerid, t3p.location FROM tracker3plus t3p 
			INNER JOIN tracker3 t ON t3p.job_number = t.job_number ";

	my $where;
	if ( $use_like ) {
		# In theory we could be clever here, and allow user to use * to mean wildcard, but the main dashboard call
		# doesnt expect to handle *
		# $searchContents =~ s/\%/\\\%/g;	# Replace any real % with \%
		# $searchContents =~ s/\*/\%/g;	# Change * to %
		$searchContents = '%' . $searchContents . '%';
		$where = "$column LIKE ?";

	} elsif ( $use_in ) {
		## As discussed with Brian we can't put '?' for IN CLAUSE. BEcause it is forming the data as '(\'67000236\',\'66001691\',\'66000858\') instead of ('67000236','66001691','66000858')
		$searchContents = "('".join("','", split(/,/, $searchContents)). "')";
		$where = "$column IN $searchContents";

	} else {
		$where = "$column = ?";
	}

	$sql .= " INNER JOIN Broadcasttracker3 btp on t3p.job_number = btp.job_number " if ( $where =~ /^btp\./ );
	$sql .= " WHERE $where ";
	$sql .= " LIMIT $searchLimit " if ( defined $searchLimit );

	my $rows;
	if ( $use_in ) {
		$rows = $Config::dba->hashed_process_sql( $sql );
	} else {
		$rows = $Config::dba->hashed_process_sql( $sql, [ $searchContents ] );
	}

	return $entity, $rows, $sql;

}# db_checkSearchContent

#######################################################################################
# db_getCustomerName: To get customer name.
########################################################################################
sub db_getCustomerName {

	my $customerID	= $_[0];

	my ( $customerName )	= $Config::dba->process_onerow_sql("SELECT customer FROM customers WHERE id = ?", [ $customerID ] );
	return $customerName
}#db_getCustomerName

#######################################################################################
# db_getRemotCustomerID: To get the remote customer id.
########################################################################################
sub db_getRemotCustomerID {
	my $custName  		= $_[0];
	my $locationID  	= $_[1];

    my ( $remoteCustID )    = $Config::dba->process_onerow_sql("SELECT id FROM customers WHERE customer = ? AND opcoid = ? ", [ $custName, $locationID ] );
    return $remoteCustID
} #db_getRemotCustomerID

#######################################################################################
# db_getRemotCustomerID: To get operationg company name.
#######################################################################################
sub getOpcoNameFromId {
	my ( $opcoid ) = @_;
	my ( $opconame ) = $Config::dba->process_onerow_sql( "SELECT opconame FROM operatingcompany WHERE opcoid = ?", [ $opcoid ] );
	return $opconame;
}#getOpcoNameFromId

#######################################################################################
# canUserSeeOpcoid: To check permission.
########################################################################################
sub canUserSeeOpcoid {
	my ( $userid, $opcoid ) = @_;
	my ( $cansee ) = $Config::dba->process_onerow_sql( "SELECT 1 FROM useropcos WHERE userid = ? AND opcoid = ?", [ $userid, $opcoid ] );
	return $cansee || 0;
}#canUserSeeOpcoid

#Get list of opcoids that user can see

sub getUsersOpcos {
    my ( $userid ) = @_;

    my @opcoids = $Config::dba->process_onerow_sql( "SELECT opcoid FROM useropcos WHERE userid=?", [ $userid ] );
    return \@opcoids;
}

sub hasJobShardAtLocation {
    my ( $job_number, $opcoid ) = @_;

    my ( $canSeeShard ) = $Config::dba->process_onerow_sql( "SELECT 1 FROM shards WHERE location=? AND job_number=?", [ $opcoid, $job_number ] );
    $canSeeShard = 0 unless defined $canSeeShard;

    return $canSeeShard;
} 
sub getShardLocations {
	my ( $job_number ) = @_;
	my @locations = $Config::dba->process_onerow_sql("SELECT DISTINCT location FROM shards WHERE job_number=? AND (location not in (0) OR location is not null)", [ $job_number ] );
	return \@locations;
}
1;
