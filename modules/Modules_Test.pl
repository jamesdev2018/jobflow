#!/usr/local/bin/perl
use strict;
use warnings;

use lib 'modules';
use lib 'modules/Utils';
use lib '../modules';
use lib '../modules/Utils';


use Test::More;

my $test_opcoid = '382';
my $test_customerid = '3821';
my $test_jobid = '38238201';
my $test_userid = '99382';
my $test_vendorid = '3823';
my $test_commentid = '3824';


## UPDATE SHORTHAND
sub ut3		{ DBAgent::process_oneline_sql("UPDATE TRACKER3 SET $_[0]=? WHERE job_number=?", [ $_[1], $test_jobid ]); }
sub ut3p		{ DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET $_[0]=? WHERE job_number=?", [ $_[1], $test_jobid ]); }
sub ucust	{ DBAgent::process_oneline_sql("UPDATE CUSTOMERS SET $_[0]=? WHERE id=?", [ $_[1], $test_customerid ]); }
sub uah		{ DBAgent::process_oneline_sql("UPDATE ASSIGNMENTHISTORY SET $_[0]=? WHERE job_number=?", [ $_[1], $test_jobid ]); }

## GET SHORTHAND
sub gt3		{ my ($retval) = DBAgent::process_oneline_sql("SELECT $_[0] FROM TRACKER3 WHERE job_number=?", [ $test_jobid ]); return $retval; }
sub gt3p		{ my ($retval) = DBAgent::process_oneline_sql("SELECT $_[0] FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]); return $retval; }
sub gcust	{ my ($retval) = DBAgent::process_oneline_sql("SELECT $_[0] FROM CUSTOMERS WHERE id=?", [ $test_customerid ]); return $retval; }
sub gah		{ my ($retval) = DBAgent::process_oneline_sql("SELECT $_[0] FROM ASSIGNMENTHISTORY WHERE job_number=?", [ $test_jobid ]); return $retval; }

print "" . ("*" x 20) . "\n";
print "    PREPARING TEST ENVIRONMENT...\n";
print "" . ("*" x 20) . "\n";

#TEST 1
require_ok('DBAgent');

require_ok('config.pl');
require_ok('databaseJob.pl');
require_ok('databaseUser.pl');

#DBAgent::enable_debug_messages();

#TEST 2
#DBAgent::new_connection('jobflow_web', "ecm", "172.27.96.8", "production", "production");   # Old Dev
DBAgent::new_connection('jobflow_web', "ecm", "172.27.93.115", "production", "production"); # DEV on Linux MySQL

is(DBAgent::process_oneline_sql("SELECT 1"), 1, 'Testing database connection');


DBAgent::process_sql("DELETE FROM CUSTOMERS WHERE id=?", [ $test_customerid ]);
DBAgent::process_sql("DELETE FROM OPERATINGCOMPANY WHERE opcoid=?", [ $test_opcoid ]);
DBAgent::process_sql("DELETE FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
DBAgent::process_sql("DELETE FROM TRACKER3 WHERE job_number=?", [ $test_jobid ]);

#TEST 3
DBAgent::process_sql("INSERT IGNORE INTO OPERATINGCOMPANY (opcoid, opconame, rootpath, wippath, mountpath, preflightin, preflightpassed, preflightfail, preflightwarning,
													ftpserver, ftpusername, ftppassword, bwdef, bwcroppeddef, lowrescroppeddef, lowresdef, hiresdef, proofonlydef,
													rgbdef, rgbcroppeddef, rgbjpegdef, hirescroppedbleedboxdef, hirescroppedtrimboxdef, colourmanagedef, colourmanage_isactivedef,
													optimisedef, optimise_isactivedef, pdfcomparedef, filenameconfig, pathtype, jobfolderenabled, ttpfolder, preflight_profile,
													qcenabled, filenameoverride, ftpenabled, jobassignment, viewhighres, creative, publishing, domainname, jobpoolassign,
													ttpupload, remotetempfolder, remotettpfolder, usejobrepository, useassetrepository, repositorysync, syncstart, syncend,
													adgatesend, thumbview, estimateon)
						VALUES (?, 'TEST_OPCO', '/test/root/', '/test/wip/', '/test/mount/', 'preflightin', 'preflightpassed', 'preflightfail', 'preflightwarning',
								'ftpserver', 'ftpusername', 'ftppassword', 1, 1, 1, 1, 1, 1,
								1, 1, 1, 1, 1, 1, 1,
								1, 1, 1, 'test_filename', 1, 1, 1, 1,
								1, 1, 1, 1, 1, 1, 1, 'test_domain', 1,
								1, '/test/remote/temp/', '/test/remote/ttp/', 1, 1, 1, 1, 1,
								1, 1, 1)", [ $test_opcoid ]);


my ($opconame) = DBAgent::process_oneline_sql("SELECT opconame FROM OPERATINGCOMPANY WHERE opcoid=?", [ $test_opcoid ]);
is($opconame, 'TEST_OPCO', 'Manually inserting test opco');

#TEST 4
DBAgent::process_sql("INSERT IGNORE INTO CUSTOMERS (id, opcoid, customer, filename, jobpoolassign, bwdef, bwcroppeddef, lowrescroppeddef, lowresdef, hiresdef, proofonlydef, rgbdef,
													rgbcroppeddef, rgbjpegdef, hirescroppedbleedboxdef, hirescroppedtrimboxdef, autocheckin, useagencyrefasfilename, usejobrepository,
													repoautoarchive, useassetrepository, repositorysync, syncstart, syncend, enhancedftp, estimate, estimateon, pdfcomparedef,
													additionalfiletypes, bw_asset, bw_crp_asset, lr_asset, lr_crp_asset, rgb_asset, rgb_crp_asset, rgb_jpg_asset, hires_crp_bleedbox_asset,
													hires_crp_trimbox_asset, aux_files, split_pages, open_assets, high_res, rec_timestamp, nearline_threshold, offline_threshold)
							VALUES (?, ?, 'test_agency', '', 0, 0, 0, 0, 0, 1, 0, 0,
									0, 0, 0, 0, 0, NULL, 0,
									0, 0, 0, 0, 0, 0, 0.00, 0, 0,
									0, 0, 0, 0, 0, 0, 0, 0, 0,
									0, 0, 0, 0, 1, '2015-01-01 01:01:01', 0, 0)", [ $test_customerid, $test_opcoid ]);



#TEST 4
DBAgent::process_sql("INSERT IGNORE INTO TRACKER3 (job_number, agency, client, project, letter, publication, size, format, jpeg, cmyk, dpi, proofing, icc_curve, crop, path, apdf, ucr,
													headline, lowrespath, releasepath, workflow, szpercent, proofcurve, formatsize, szwidth, szheight, bleedt, bleedb, bleedl, bleedr,
													safetyt, safetyb, safetyl, safetyr, trim, matdate, coverdate, postdate, opcoid, pnums, agencyref, gmg, approvalstatus, szwidthdps,
													szheightdps, fprelname, pubcollect, collectproof, rootpath, pathtype, version, businessunitref, colour)
							VALUES (?, 'test_agency', 'test_client', 'test_project', 'T', 'test_publication', NULL, 'PDF', NULL, 0, 382, 'TEST_SPEC', NULL, 0, 'temp/path', NULL, 320,
									'TBC', 'test/lowres/38200001ver1.pdf', 'FAIL', 'TEST', 0, NULL, 200, 300, 400, NULL, NULL, NULL, NULL,
									NULL, NULL, NULL, NULL, NULL, '01/05/2015', '02/05/2015', '03/05/2015', 382, 123, NULL, 0, 0, NULL,
									NULL, NULL, NULL, NULL, '/test/rootpath/', 1, 1, NULL, 'TESTCOLOUR')", [ $test_jobid ]);
my ($t3id) = DBAgent::process_oneline_sql("SELECT agency FROM TRACKER3 WHERE job_number=?", [ $test_jobid ]);
is($t3id, 'test_agency', 'Manually inserting test job into Tracker3');


DBAgent::process_sql("INSERT IGNORE INTO TRACKER3PLUS (job_number, opcoid, customerid, location, checkoutstatus, locked, locked_time, assetscreated, status, preflightstatus, twist, preflight,
														bw, bwcropped, lowrescropped, lowres, hires, proofonly, rgb, rgbcropped, rgbjpeg, hirescroppedbleedbox, hirescroppedtrimbox, colourmanage,
														colourmanage_isactive, optimise, optimise_isactive, pdfcompare, edited, comparefolder, compareready, comparerequested, sendtotwist, lastuser,
														ziprequired, qcstatus, assignee, duedate, estimate, operatorstatus, creative, brief, lastsynced, repocheckedin, repocheckedout, adgatestatus,
														rec_timestamp, mod_timestamp)
							VALUES (?, ?, ?, ?, 2, 0, '2015-05-01 01:01:01', 0, 1, 1, 1, 1,
									0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
									0, 1, 1, 0, 0, '666', 0, 0, 0, ?,
									0, 3, 0, '2015-05-02 01:01:01', 0.00, 'R', 0, '', '2015-05-03 01:01:01', 1, 1, '',
									'2015-01-04 01:01:01', '2015-01-05 01:01:01')", [ $test_jobid, $test_opcoid, $test_customerid, $test_opcoid, $test_userid ]);
my ($t3pid) = DBAgent::process_oneline_sql("SELECT lastuser FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($t3pid, $test_userid, 'Manually inserting test job into Tracker3Plus');



DBAgent::process_sql("INSERT IGNORE INTO USERS (id, status, opcoid, companyid, fname, lname, username, password, email, deputyemail,
										broadcast, catalogue, directmarketing, interactive, literature, outdoor, packaging,
										pointofsale, press, artworker, creative, design, junior, retouching,
										employeeid, role, rec_timestamp, update_time, sessionid)
						VALUES (?, 1, ?, 5, 'test', 'user', 'testuser', '', 'testuser\@noreply.com', 'testuser_deputy\@noreply.com',
								1, 1, 1, 1, 1, 1, 1,
								1, 1, 1, 1, 1, 1, 1,
								9333, 2, '2015-01-01 01:01:01', NULL, NULL)", [ $test_userid, $test_opcoid ]);
my $username = ECMDBUser::db_getUsername($test_userid);
is($username, 'testuser', 'Manually inserting test user');

DBAgent::process_sql("INSERT IGNORE INTO OUTSOURCEDESTINATIONS (fromopcoid, toopcoid, userid, ttpfolder, status, rec_timestamp)
						VALUES (?, ?, ?, '/test/ttp_folder', 1, '2015-01-01 01:01:01')", [ $test_opcoid, $test_opcoid, $test_userid ]);

my ($folder) = DBAgent::process_oneline_sql("SELECT ttpfolder FROM OUTSOURCEDESTINATIONS WHERE fromopcoid=? AND toopcoid=? AND userid=?", [ $test_opcoid, $test_opcoid, $test_userid ]);
is($folder, '/test/ttp_folder', 'Manually inserting an outsource destination');














print "" . ("*" x 20) . "\n";
print "    TESTING MODULES...\n";
print "" . ("*" x 20) . "\n";

####
my $retval;

$retval = ECMDBJob::db_getOpcoid($test_userid);
is($retval, $test_opcoid, 'Checking the user\'s OPCO ID is valid');


$retval = ECMDBJob::db_getJobOpcoid($test_jobid);
is($retval, $test_opcoid, 'Checking the job\'s OPCO ID is valid');


$retval = ECMDBJob::db_getLocation($test_jobid);
is($retval, $test_opcoid, 'Checking that the job\'s location is valid');


$retval = ECMDBJob::db_getJobRemoteOpcoid($test_jobid);
is($retval, $test_opcoid, 'Checking that the job\'s location is valid (??)');


$retval = ECMDBJob::db_getCustomerid($test_jobid);
is($retval, $test_customerid, 'Finding a customer that matches the job\'s attributes');


$retval = ECMDBJob::db_getCustomerIdJob($test_jobid);
is($retval, $test_customerid, 'Checking the job\'s assigned customer is valid');


$retval = ECMDBJob::db_getCustomer($test_jobid);
is($retval, 'test_agency', 'Checking the job\'s customer name is valid');


$retval = ECMDBJob::db_getJobPathInfo($test_jobid);
is(verify_hashlist($retval, "jobnumber rootpath letter agency client project proofing ucr workflow opcoid opco"), 1, 'Verifying job path info structure');
is($retval->{jobnumber}, $test_jobid, 'Checking the job path returns the correct job');
is($retval->{agency}, 'test_agency', 'Checking the job path returns the correct customer agency');


$retval = ECMDBJob::db_getJobLocation($test_jobid);
is(verify_hashlist($retval, "current_opcoid current_opco current_opcoid current_opcowip origin_opcoid origin_opco origin_opcoip origin_opcowip isinrepo jobid"), 1, 'Verifying job location info structure');
is($retval->{jobid}, $test_jobid, 'Checking the job location returns the correct job');
is($retval->{current_opcoid}, $test_opcoid, 'Checking the job location returns the correct opco id');
is($retval->{origin_opcoid}, $test_opcoid, 'Checking the job location returns the correct origin opco id');


$retval = ECMDBJob::db_getTTPFolder($test_opcoid, $test_opcoid);
is($retval, '/test/ttp_folder', 'Checking TTP folder destinations still work');


$retval = ECMDBJob::db_getDestinationDirectory($test_opcoid);
is($retval, '/test/root/preflightin', 'Checking destination directory returns correctly');

# removed test for db_getJobs

ECMDBJob::db_setActiveFlag($test_jobid, 'OPTIMISE', 1);
($retval) = DBAgent::process_oneline_sql("SELECT optimise_isactive FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 1, 'Setting optimise_isactive flag');
ECMDBJob::db_setActiveFlag($test_jobid, 'OPTIMISE', 0);
($retval) = DBAgent::process_oneline_sql("SELECT optimise_isactive FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 0, 'Clearing optimise_isactive flag');


ECMDBJob::db_setCreative($test_jobid, 1);
($retval) = DBAgent::process_oneline_sql("SELECT creative FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 1, 'Setting creative flag');
ECMDBJob::db_setCreative($test_jobid, 0);
($retval) = DBAgent::process_oneline_sql("SELECT creative FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 0, 'Clearing creative flag');

ECMDBJob::db_updateEstimate($test_jobid, '5.20');
($retval) = DBAgent::process_oneline_sql("SELECT estimate FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, '5.20', 'Changing estimation value (5.20)');
ECMDBJob::db_updateEstimate($test_jobid, '4.50');
($retval) = DBAgent::process_oneline_sql("SELECT estimate FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, '4.50', 'Changing estimation value (4.50)');


my @aretval = ECMDBJob::db_getJob($test_jobid, $test_opcoid);
is(scalar(@aretval), 1, 'Ensuring getJob only returns one job');
is(verify_hashlist($aretval[0], 'job_number locked islocked lockedtime assetscreated statusid preflightstatus agency client project publication twist bw bwcropped lowrescropped lowres hires
								proofonly rgb rgbcropped rgbjpeg hirescroppedbleedbox hirescroppedtrimbox colourmanage colourmanage_isactive optimise optimise_isactive bwdef
								bwcroppeddef lowrescroppeddef lowresdef hiresdef proofonlydef rgbdef rgbcroppeddef rgbjpegdef hirescroppedbleedboxdef hirescroppedtrimboxdef
								colourmanagedef colourmanage_isactivedef optimisedef optimise_isactivedef pdfcomparedef compareready colour agencyref pathtype lastuser qcstatus assignee duedate estimate
								operatorstatus creative actiontype brief jobsize'), 1, 'Checking structure returned by getJob');
is($aretval[0]->{job_number}, $test_jobid, 'Ensuring correct job is returned (1/2)');
is($aretval[0]->{agency}, 'test_agency', 'Ensuring correct job is returned (2/2)');


isnt(ECMDBJob::updateHistory($test_jobid, $test_userid), 0, 'Updating history');
isnt(ECMDBJob::updateVendorHistory($test_jobid, $test_userid, $test_vendorid), 0, 'Updating vendor history');


is(ECMDBJob::db_updateJob($test_jobid, '1'), 1, 'Updating Job status (1/2)');
($retval) = DBAgent::process_oneline_sql("SELECT qcstatus FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, '1', 'Checking qc status (1/2)');
is(ECMDBJob::db_updateJob($test_jobid, '2'), 1, 'Updating Job status (2/2)');
($retval) = DBAgent::process_oneline_sql("SELECT qcstatus FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, '2', 'Checking qc status (2/2)');


isnt(ECMDBJob::db_lockJob($test_jobid, 1), 0, 'Locking job (1/2)');
($retval) = DBAgent::process_oneline_sql("SELECT locked FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 1, 'Testing if job is locked (1/2)');
isnt(ECMDBJob::db_lockJob($test_jobid, 0), 0, 'Unlocking job (2/2)');
($retval) = DBAgent::process_oneline_sql("SELECT locked FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 0, 'Testing if job is unlocked (2/2)');

isnt(ECMDBJob::db_showAssets($test_jobid, 1), 0, 'Updating job assets (1/2)');
($retval) = DBAgent::process_oneline_sql("SELECT assetscreated FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 1, 'Testing asset change (1/2)');
isnt(ECMDBJob::db_showAssets($test_jobid, 0), 0, 'Updating job assets (2/2)');
($retval) = DBAgent::process_oneline_sql("SELECT assetscreated FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 0, 'Testing asset change (2/2)');



ECMDBJob::db_touchJob($test_jobid, 0);
($retval) = DBAgent::process_oneline_sql("SELECT lastuser FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 0, 'Testing last username (1/2)');
ECMDBJob::db_touchJob($test_jobid, $test_userid);
($retval) = DBAgent::process_oneline_sql("SELECT lastuser FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, $test_userid, 'Testing last username (2/2)');



is(ECMDBJob::db_updateAssetsCreated($test_jobid, 0), 1, 'Updating job assets (1/2)');
($retval) = DBAgent::process_oneline_sql("SELECT assetscreated FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 0, 'Testing assets created (1/2)');
is(ECMDBJob::db_updateAssetsCreated($test_jobid, 1), 1, 'Updating job assets (2/2)');
($retval) = DBAgent::process_oneline_sql("SELECT assetscreated FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 1, 'Testing assets created (2/2)');


is(ECMDBJob::db_updateCompare($test_jobid, 0), 1, 'Updating job compare (1/2)');
($retval) = DBAgent::process_oneline_sql("SELECT compareready FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 0, 'Testing job compare (1/2)');
is(ECMDBJob::db_updateCompare($test_jobid, 1), 1, 'Updating job compare (2/2)');
($retval) = DBAgent::process_oneline_sql("SELECT compareready FROM TRACKER3PLUS WHERE job_number=?", [ $test_jobid ]);
is($retval, 1, 'Testing job compare (2/2)');

is(ECMDBJob::db_jobExists($test_jobid), 1, 'Testing job exists');
is(ECMDBJob::db_jobExists("999$test_jobid"), 0, 'Testing job doesn\'t exist');

($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET preflightstatus=0 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobPassed($test_jobid), 0, 'Checking preflight status (1/7)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET preflightstatus=1 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobPassed($test_jobid), 0, 'Checking preflight status (2/7)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET preflightstatus=2 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobPassed($test_jobid), 1, 'Checking preflight status (3/7)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET preflightstatus=3 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobPassed($test_jobid), 0, 'Checking preflight status (4/7)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET preflightstatus=4 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobPassed($test_jobid), 0, 'Checking preflight status (5/7)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET preflightstatus=5 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobPassed($test_jobid), 0, 'Checking preflight status (6/7)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET preflightstatus=6 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobPassed($test_jobid), 1, 'Checking preflight status (7/7)');

($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET qcstatus=0 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobQCApproved($test_jobid), 0, 'Checking approved status (1/1)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET qcstatus=1 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobQCApproved($test_jobid), 0, 'Checking approved status (2/2)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET qcstatus=2 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobQCApproved($test_jobid), 1, 'Checking approved status (3/3)');


($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET qcstatus=0 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobQCRequired($test_jobid), 0, 'Checking required status (1/1)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET qcstatus=1 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobQCRequired($test_jobid), 1, 'Checking required status (2/2)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET qcstatus=2 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_jobQCRequired($test_jobid), 0, 'Checking required status (3/3)');

($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET assetscreated=0 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_assetsCreated($test_jobid), 0, 'Checking assets were created (1/1)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET assetscreated=1 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_assetsCreated($test_jobid), 1, 'Checking assets were created (2/2)');

($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET qcstatus=0 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_getQCStatus($test_jobid), 0, 'Checking QC status (1/1)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET qcstatus=1 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_getQCStatus($test_jobid), 1, 'Checking QC status (2/2)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3PLUS SET qcstatus=2 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_getQCStatus($test_jobid), 2, 'Checking QC status (3/3)');


($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3 SET pathtype=5 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_isLegacy($test_jobid), 0, 'Checking if legacy job (1/1)');
($retval) = DBAgent::process_oneline_sql("UPDATE TRACKER3 SET pathtype=10 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_isLegacy($test_jobid), 1, 'Checking if legacy job (2/2)');






check('Checking enhanced FTP status',	sub { ucust("enhancedftp", $_[0]); },	sub { return ECMDBJob::isEnhancedFTP($test_jobid); },	{ 0 => 0, 1 => 1 });
check('Checking Agency ref',				sub { ut3("agencyref", $_[0]); },		sub { return ECMDBJob::getAgencyRef($test_jobid); },		{ 0 => 0, 1 => 1 });
check('Checking job version',			sub { ut3("version", $_[0]); },			sub { return ECMDBJob::getVersion($test_jobid); },		{ 0 => 0, 1 => 1, 2 => 2 });
check('Checking OBA Number',				sub { ut3("size", $_[0]); },				sub { return ECMDBJob::getOBANumber($test_jobid); },		{ 0 => 0, 5 => 5 });

check('Checking Preflight status',		sub { ECMDBJob::db_updateJobPreflightStatus($test_jobid, $_[0]); }, sub { return gt3p("preflightstatus"); }, { 1 => 1, 2 => 2, 3 => 3, 4 => 4 });


is(scalar(ECMDBJob::db_getPreflight()), 15, 'Checking preflight list');
is(scalar(ECMDBJob::db_getPreflightStatusValues()), 7, 'Checking preflight status value list');
is(scalar(ECMDBJob::db_getJobstatus()), 6, 'Checking job status list');
is(scalar(ECMDBJob::db_getQCStatuses()), 4, 'Checking QC status list');

# TODO - test throws exception
# my @clients = ECMDBJob::db_getClients($test_opcoid, 'test_agency', '36500');
# is(scalar @clients, 1, 'Checking client list');


my @agencies = ECMDBJob::db_getAgencies($test_opcoid);
is(scalar @agencies, 1, 'Checking agency list');


@agencies = ECMDBJob::db_getAgencies2($test_opcoid, '36500');

is(scalar @agencies, 1, 'Checking agency filterable list (1 / 2)');
@agencies = ECMDBJob::db_getAgencies2($test_opcoid, '0');
is(scalar @agencies, 0, 'Checking agency filterable list (2 / 2)');

# Commented out db_getProject (as it throws an SQL error at present and isnt used)
#my @projects = ECMDBJob::db_getProjects($test_opcoid, '36500');
#is(scalar @projects, 1, 'Checking project list (1 / 2)');
#@projects = ECMDBJob::db_getProjects($test_opcoid, '0');
#is(scalar @projects, 0, 'Checking project list (2 / 2)');


is(ECMDBJob::db_jobPath($test_jobid), '/test/rootpath/T/test_agency/test_client/test_project/TBC/38238201_test_publication', 'Checking job path');
is(ECMDBJob::db_openFolderPath($test_jobid), '//test_domain/test/mount//T/test_agency/test_client/test_project/TBC/38238201_test_publication', 'Checking open job path');
is(ECMDBJob::db_jobShortPath($test_jobid), '/T/test_agency/test_client/test_project/TBC/38238201_test_publication', 'Checking short job path');

my @jobs = ECMDBJob::db_getLockedJobs();
is(scalar(@jobs) > 0, 1, 'Checking locked jobs list');
is(verify_hashlist($jobs[0], 'jobid, status, lockedtime'), 1, 'Checking locked jobs list structure');

is(ECMDBJob::db_getFTPDetails($test_opcoid), 'ftpserver ftpusername ftppassword', 'Checking FTP details');

is(ECMDBJob::db_getOperatingCompanyFromJob($test_jobid), $test_opcoid, 'Checking opco is correct for job');

is(ECMDBJob::db_getFilenameTemplate($test_opcoid), 'test_filename', 'Checking filename template');

check('Updating job location',			sub { ECMDBJob::setLocation($test_jobid, '', $_[0]); },	sub { return gt3p("location"); },		{ 555 => '555', $test_opcoid => $test_opcoid });
check('Updating checkout status',		sub { ECMDBJob::setCheckoutStatus($test_jobid, $_[0]); }, sub { return gt3p("checkoutstatus"); }, { 3 => 3, 1 => 1, 2 => 2, 0 => 0 });


ECMDBJob::enQueueOutsource($test_jobid, $test_opcoid, $test_userid);
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM OUTSOURCEQUEUE WHERE job_number=? AND opcoid=? AND userid=?", [ $test_jobid, $test_opcoid, $test_userid ]);
is($retval, 1, 'Checking outsource enqueueing');
DBAgent::process_oneline_sql("DELETE FROM OUTSOURCEQUEUE WHERE job_number=? AND opcoid=? AND userid=?", [ $test_jobid, $test_opcoid, $test_userid ]);





######SEND JOB
is(ECMDBJob::enQueueRepoSendJob($test_jobid, '', $test_opcoid, 16, $test_userid), 0, 'Trying to enqueue an job that shouldn\'t be (1 / 2)');
is(ECMDBJob::isJobOnRepoSendJobsQueue($test_jobid), 0, 'Checking if job is not on queue (1 / 2)');
is(ECMDBJob::enQueueRepoSendJob($test_jobid, '', $test_opcoid, 15, $test_userid), 0, 'Trying to enqueue an job that shouldn\'t be (2 / 2)');
is(ECMDBJob::isJobOnRepoSendJobsQueue($test_jobid), 0, 'Checking if job is not on queue (1 / 2)');
ut3p('location', $test_opcoid);
isnt(ECMDBJob::enQueueRepoSendJob($test_jobid, '', $test_opcoid, 555, $test_userid), 0, 'Enqueueing job onto the send jobs queue');

######CHECKIN
isnt(ECMDBJob::enQueueRepoCheckIn($test_jobid, '', $test_opcoid, $test_userid), 0, 'Enqueueing checkin');
is(ECMDBJob::isJobOnCheckInQueue($test_jobid), 1, 'Testing if we think the job was queued for checkin');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOCHECKINQUEUE WHERE job_number=? AND opcoid=? AND userid=?", [ $test_jobid, $test_opcoid, $test_userid ]);
is($retval, 1, 'Testing if the job was actually queued for checkin');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOCHECKINQUEUEHISTORY WHERE job_number=? AND opcoid=? AND userid=? AND rec_timestamp>DATE_SUB(now(), INTERVAL 1 MINUTE)", [ $test_jobid, $test_opcoid, $test_userid ]);
is($retval, 1, 'Ensuring history was updated for checkin');
isnt(ECMDBJob::db_setRepoCheckedIn($test_jobid), 0, 'Setting checked in flag');
is(gt3p('repocheckedin'), 1, 'Verifying flag');
@aretval = ECMDBJob::getCheckInQueue();
ok(scalar @aretval > 0, 'Checking checkin queue length');
is(verify_hashlist($aretval[0], 'job_number customer opconame toopconame username checkingin rec_timestamp priority timeonqueue jobsize'), 1, 'Checking queue structure');
isnt(ECMDBJob::changeJobQueuePriority($test_jobid, 77), 0, 'Changing job priority');
($retval) = DBAgent::process_oneline_sql("SELECT priority FROM REPOCHECKINQUEUE WHERE job_number=?", [ $test_jobid ]);
is($retval, 77, 'Checking repocheckinqueue priority');
($retval) = DBAgent::process_oneline_sql("SELECT priority FROM REPOSENDJOBSQUEUE WHERE job_number=?", [ $test_jobid ]);
is($retval, 77, 'Checking reposendjobsqueue priority');
isnt(ECMDBJob::delayJobQueueSubmit($test_jobid, 5), 0, 'Delaying job');
is(ECMDBJob::db_removeFromCheckInQueue($test_jobid), $test_jobid, 'Removing from checkin queue');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOCHECKINQUEUE WHERE job_number=? AND opcoid=? AND userid=?", [ $test_jobid, $test_opcoid, $test_userid ]);
is($retval, 1, 'Testing if the job was not removed from checkin queue');
DBAgent::process_oneline_sql("UPDATE REPOCHECKINQUEUE SET checkingin=1 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_removeFromCheckInQueue($test_jobid), $test_jobid, 'Removing from checkin queue');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOCHECKINQUEUE WHERE job_number=? AND opcoid=? AND userid=?", [ $test_jobid, $test_opcoid, $test_userid ]);
is($retval, undef, 'Testing if the job was from checkin queue');
@aretval = ECMDBJob::getCheckInQueueHistory('2001-01-01', '2050-01-01');
ok(scalar @aretval > 0, 'Checking checkin history length');
is(verify_hashlist($aretval[0], 'job_number, opcoid, opconame, customerid, customer, userid, username, rec_timestamp'), 1, 'Checking list structure');

######CHECKOUT
isnt(ECMDBJob::enQueueRepoCheckOut($test_jobid, '', $test_opcoid, $test_userid), 0, 'Enqueueing checkout');
is(ECMDBJob::isJobOnCheckOutQueue($test_jobid), 1, 'Testing if we think the job was queued for checkout');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOCHECKOUTQUEUE WHERE job_number=? AND opcoid=? AND userid=?", [ $test_jobid, $test_opcoid, $test_userid ]);
is($retval, 1, 'Testing if job was actually queued for checkout');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOCHECKOUTQUEUEHISTORY WHERE job_number=? AND opcoid=? AND userid=? AND rec_timestamp>DATE_SUB(now(), INTERVAL 1 MINUTE)", [ $test_jobid, $test_opcoid, $test_userid ]);
is($retval, 1, 'Ensuring history was updated for checkout');
isnt(ECMDBJob::db_setRepoCheckedOut($test_jobid), 0, 'Setting checked out flag');
is(gt3p('repocheckedout'), 1, 'Verifying flag');
@aretval = ECMDBJob::getCheckOutQueue();
ok(scalar @aretval > 0, 'Checking checkout queue length');
is(verify_hashlist($aretval[0], 'job_number customer toopconame username checkingout rec_timestamp priority timeonqueue jobsize'), 1, 'Checking queue structure');
isnt(ECMDBJob::changeJobCOQueuePriority($test_jobid, 44), 0, 'Changing checkout priority');
($retval) = DBAgent::process_oneline_sql("SELECT priority FROM REPOCHECKOUTQUEUE WHERE job_number=?", [ $test_jobid ]);
is($retval, 44, 'Verifying priority');
isnt(ECMDBJob::delayJobCOQueueSubmit($test_jobid, 3), 0, 'Delaying job');
is(ECMDBJob::db_removeFromCheckOutQueue($test_jobid), $test_jobid, 'Removing from checkout queue');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOCHECKOUTQUEUE WHERE job_number=? AND opcoid=? AND userid=?", [ $test_jobid, $test_opcoid, $test_userid ]);
is($retval, 1, 'Testing if the job was not removed from checkout queue');
DBAgent::process_oneline_sql("UPDATE REPOCHECKOUTQUEUE SET checkingout=1 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_removeFromCheckOutQueue($test_jobid), $test_jobid, 'Removing from checkout queue');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOCHECKOUTQUEUE WHERE job_number=? AND opcoid=? AND userid=?", [ $test_jobid, $test_opcoid, $test_userid ]);
is($retval, undef, 'Testing if the job was from checkout queue');
DBAgent::process_oneline_sql("DELETE FROM REPOCHECKOUTQUEUEHISTORY WHERE job_number=? AND opcoid=? AND userid=?", [ $test_jobid, $test_opcoid, $test_userid ]);

######SYNC
isnt(ECMDBJob::requestSync($test_jobid, $test_customerid, $test_opcoid, $test_userid), 0, 'Adding job to sync queue');
is(ECMDBJob::isJobOnRepoSyncQueue($test_jobid), 1, 'Testing if seen on sync queue');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOSYNCQUEUEHISTORY WHERE job_number=? AND customerid=? AND opcoid=? AND userid=? AND rec_timestamp>DATE_SUB(now(), INTERVAL 1 MINUTE)", [ $test_jobid, $test_customerid, 
$test_opcoid, $test_userid ]);
is(ECMDBJob::db_removeFromSyncQueue($test_jobid), $test_jobid, 'Removing from sync queue');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOSYNCQUEUE WHERE job_number=?", [ $test_jobid ]);
isnt($retval, undef, 'Testing to see if the sync hasn\'t been removed');
DBAgent::process_oneline_sql("UPDATE REPOSYNCQUEUE SET syncing=1 WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::db_removeFromSyncQueue($test_jobid), $test_jobid, 'Removing from sync queue');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOSYNCQUEUE WHERE job_number=?", [ $test_jobid ]);
is($retval, undef, 'Testing to see if the sync has been removed');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOSENDJOBSQUEUE WHERE job_number=? AND opcoid=? AND userid=?", [ $test_jobid, 555, $test_userid ]);
is($retval, 1, 'Checking send jobs queue');
is(ECMDBJob::isJobOnRepoSendJobsQueue($test_jobid), 1, 'Checking if job is on queue');
DBAgent::process_oneline_sql("DELETE FROM REPOSENDJOBSQUEUE WHERE job_number=? AND fromopcoid=? AND userid=?", [ $test_jobid, $test_opcoid, $test_userid ]);


######AUTOARCHIVE
isnt(ECMDBJob::enQueueRepoAutoArchive($test_jobid, $test_opcoid, $test_userid, 3), 0, 'Enqueueing onto auto archive queue');
is(ECMDBJob::isJobOnRepoAutoArchiveQueue($test_jobid), 1, 'Testing if we think the job was queued');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM REPOAUTOARCHIVEQUEUE WHERE job_number=?", [ $test_jobid ]);
is($retval, 1, 'Testing if job was actually queued');
DBAgent::process_oneline_sql("DELETE FROM REPOAUTOARCHIVEQUEUE WHERE job_number=?", [ $test_jobid ]);






check('Testing if job is archived',		sub { ut3p('status', $_[0]); },			sub { return ECMDBJob::isJobArchived($test_jobid); },	{ 1 => 0, 5 => 0, 6 => 1, 7 => 0, 1 => 0 });

isnt(ECMDBJob::db_updateAdgateStatus($test_jobid, 2), 0, 'Updating adgate status');
is(gt3p('adgatestatus'), 2, 'Checking adgate status (1 / 2)');
isnt(ECMDBJob::db_updateAdgateStatus($test_jobid, 1), 0, 'Updating adgate status');
is(gt3p('adgatestatus'), 1, 'Checking adgate status (2 / 2)');

is(ECMDBJob::logOutsourceHistory($test_jobid, $test_userid, $test_opcoid), 1, 'Logging outsource history');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM OUTSOURCEHISTORY WHERE job_number=? AND userid=? AND outsourceopcoid=?", [ $test_jobid, $test_userid, $test_opcoid ]);
is($retval, 1, 'Checking history was logged');

check('Testing checkout status',			sub { ut3p('checkoutstatus', $_[0]); },	sub { return ECMDBJob::getCheckedOutStatus($test_jobid); },	{ 0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4 });


($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM OUTSOURCEQUEUE WHERE job_number=?", [ $test_jobid ]);
is(ECMDBJob::isJobOnOutsourceQueue($test_jobid), $retval || 0, 'Checking if job is/isn\'t on the outsource queue');

check('Testing isCheckedOut status',		sub { ut3p('checkoutstatus', $_[0]); },	sub { return ECMDBJob::isCheckedOut($test_jobid); },		{ 0 => 0, 1 => 0, 2 => 1, 3 => 0, 4 => 0 });
check('Testing in transit status',		sub { ut3p('checkoutstatus', $_[0]); },	sub { return ECMDBJob::isJobInTransit($test_jobid); },	{ 0 => 0, 1 => 1, 2 => 0, 3 => 1, 4 => 0 });
check('Testing is creative status',		sub { ut3p('creative', $_[0]); },		sub { return ECMDBJob::isCreative($test_jobid); },		{ 0 => 0, 1 => 1 });

is(ECMDBJob::enQueueFtp($test_jobid, 'filename', '0', $test_userid, '', 'test message', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 1, 'Enqueueing FTP');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM FTPQUEUE WHERE job_number=?", [ $test_jobid ]);
is($retval, 1, 'Ensuring enqueued on FTP');
DBAgent::process_oneline_sql("DELETE FROM FTPQUEUE WHERE job_number=?", [ $test_jobid ]);

is(ECMDBJob::enQueueAdgate($test_jobid, $test_userid, 'account', 'filename', '', '', '', '', 0, '', '', 0, 0, 0, 0, '', '2015-01-01'), 1, 'Enqueueing Adgate');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM ADGATEQUEUE WHERE job_number=?", [ $test_jobid ]);
is($retval, 1, 'Ensuring enqueued on Adgate');
DBAgent::process_oneline_sql("DELETE FROM ADGATEQUEUE WHERE job_number=?", [ $test_jobid ]);


DBAgent::process_oneline_sql("INSERT IGNORE INTO assignmenthistory (status, job_number, assigneeid, assignerid, assigned_timestamp, due_timestamp, requested_timestamp,
																	submittedqc_timestamp, rejected_timestamp)
															VALUES ('C', ?, ?, ?, now(), now(), now(), now(), NULL)", [ $test_jobid, $test_userid, $test_userid ]);
isnt(ECMDBJob::unlockJob($test_jobid), 0, 'Unlocking job');
is(gt3p('operatorstatus'), 'U', 'Testing that job was unlocked');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM ASSIGNMENTHISTORY WHERE job_number=? AND status IN ('A', 'W')", [ $test_jobid ]);
is($retval, undef, 'Testing assignmenthistory was wiped');
DBAgent::process_oneline_sql("DELETE FROM assignmenthistory WHERE job_number=?", [ $test_jobid ]);

isnt(ECMDBJob::db_setTwistServer($test_jobid, 2), 0, 'Setting twist server');
is(gt3p('twist'), 2, 'Checking twist server');

isnt(ECMDBJob::db_setEstimate($test_jobid, 5.7), 0, 'Setting estimate');
is(gt3p('estimate'), '5.70', 'Checking estimate');


@aretval = ECMDBJob::db_getActivity($test_jobid);
isnt($aretval[0], 0, 'Getting job activity');
is(verify_hashlist($aretval[0], 'jobid opconame lastuser username modified assetPath preflightstatus locked lockedtime'), 1, 'Verifying hash structure');

isnt(ECMDBJob::db_requestCompare($test_jobid, 1), 0, 'Requesting compare');
is(gt3p('comparerequested'), 1, 'Testing request');

isnt(ECMDBJob::db_sendToTwist($test_jobid, 1), 0, 'Sending to twist');
is(gt3p('sendtotwist'), 1, 'Testing send');

@aretval = ECMDBJob::db_getJobFields($test_jobid);
isnt($aretval[0], 0, 'Getting job fields');
is(verify_hashlist($aretval[0], 'agency, client, project, letter, publication, size, format, jpeg, cmyk, dpi, proofing, icc_curve, crop, path, apdf, ucr, headline, lowrespath, releasepath,
								workflow, szpercent, proofcurve, formatsize, szwidth, szheight, bleedt, bleedb, bleedl, bleedr, safetyt, safetyb, safetyl, safetyr, trim, matdate, coverdate,
								postdate, opcoid, pnums, agencyref, gmg, approvalstatus, szwidthdps, szheightdps, fprelname, pubcollect, collectproof, rootpath, pathtype, version,
								businessref, colour'), 1, 'Verifying structure');


isnt(ECMDBJob::assignJob($test_jobid, $test_userid, 0, $test_userid, 0), 0, 'Assigning job to user');
$retval = gt3p('duedate');
my ($testval) = DBAgent::process_oneline_sql("SELECT now()");
is($retval, $testval, 'Checking non-published date');
isnt(ECMDBJob::assignJob($test_jobid, $test_userid, 5, $test_userid, 1), 0, 'Assigning job to user');
$retval = gt3p('duedate');
isnt($retval, '2015-04-01', 'Checking published date');
($retval) = DBAgent::process_oneline_sql("SELECT COUNT(1) FROM ASSIGNMENTHISTORY WHERE job_number=? AND assigned_timestamp > DATE_SUB(now(), INTERVAL 10 SECOND)", [ $test_jobid ]);
is($retval, 2, 'Testing assignment history');


isnt(ECMDBJob::reassignJob($test_jobid, $test_userid, 6, $test_userid, 1), 0, 'Assigning job to user');
$retval = gt3p('duedate');
($testval) = DBAgent::process_oneline_sql("SELECT now()");
isnt($retval, '2015-04-01', 'Checking reassigned non-published date');
isnt(ECMDBJob::reassignJob($test_jobid, $test_userid, 4, $test_userid, 0), 0, 'Assigning job to user');
$retval = gt3p('duedate');
isnt($retval, '2015-04-01', 'Checking reassigned published date');
($retval) = DBAgent::process_oneline_sql("SELECT COUNT(1) FROM ASSIGNMENTHISTORY WHERE job_number=? AND assigned_timestamp > DATE_SUB(now(), INTERVAL 10 SECOND)", [ $test_jobid ]);
is($retval, 2, 'Testing reassigned assignment history');

ut3p('operatorstatus', 'W');
isnt(ECMDBJob::unWorkJob($test_jobid, $test_userid), 0, 'Unworking job');
is(gt3p('operatorstatus'), 'A', 'Checking operator status');


is(ECMDBJob::setCurrentOperatorJob($test_userid, $test_jobid), 1, 'Setting operator job');
is(gah('status'), 'W', 'Checking status set to W');
is(ECMDBJob::getCurrentOperatorJob($test_userid), $test_jobid, 'Checking operator job');
is(ECMDBJob::getAssigneeId($test_jobid), $test_userid, 'Checking assignee id');


isnt(ECMDBJob::unassignJob($test_jobid), 0, 'Unassigning job');
is(gt3p('assignee'), 0, 'Checking assignee');

is(ECMDBJob::setOperatorJobProcessing($test_userid, $test_jobid), 1, 'Setting operator job to processing');
is(gt3p('operatorstatus'), 'P', 'Testing job processing');
is(gah('status'), 'P', 'Testing job history processing');

uah('status', 'W');
is(ECMDBJob::rejectJob($test_jobid, $test_userid, $test_commentid, 'Reason'), 1, 'Rejecting job');
is(gt3p('operatorstatus'), 'R', 'Testing job rejected');
is(gah('status'), 'R', 'Testing job history rejected');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM JOBNOTES WHERE job_number=?", [ $test_jobid ]);
is($retval, 1, 'Testing comment');

uah('status', 'W');
is(ECMDBJob::completeJob($test_jobid, $test_userid), 1, 'Testing complete job');
is(gt3p('operatorstatus'), 'C', 'Checking job completed');
is(gah('status'), 'C', 'Checking job history completed');

uah('status', 'A');
uah('due_timestamp', '2001-01-01');
is(ECMDBJob::getNextAssignedJob($test_userid), $test_jobid, 'Getting next assigned job');


ut3p('operatorstatus', 'U');
ut3p('location', $test_opcoid);
ut3p('qcstatus', 0);
is(ECMDBJob::getJobFromPool($test_opcoid), $test_jobid, 'Getting job from pool');

check('Testing if job is assigned', 		sub { uah('status', $_[0]); },		sub { return ECMDBJob::isJobAssigned($test_jobid); }, 		{ 'A' => 1, 'R' => 0, 'P' => 0, 'W' => 1 });
check('Testing if job being worked on',	sub { uah('status', $_[0]); },		sub { return ECMDBJob::isJobBeingWorkedOn($test_jobid); }, 	{ 'A' => 0, 'R' => 0, 'P' => 0, 'W' => 1 });

uah('assigneeid', $test_userid);
uah('status', 'W');
is(ECMDBJob::markJobComplete($test_jobid, $test_userid), 1, 'Marking job complete');
is(gt3p('assignee'), 0, 'Checking unassigned');
is(gah('status'), 'C', 'Checking history status completed');

is(ECMDBJob::addComment($test_jobid, $test_userid, '5', 'Comment'), 1, 'Adding comment');

@aretval = ECMDBJob::getJobNotes($test_jobid);
isnt($aretval[0], 0, 'Getting job notes');
is(verify_hashlist($aretval[0], 'notetext timestamp username comment'), 1, 'Verifying job note structure');

@aretval = ECMDBJob::db_getCommentsList();
isnt($aretval[0], 0, 'Getting comments list');
is(verify_hashlist($aretval[0], 'id comment ctype'), 1, 'Verifying comment list structure');

is(ECMDBJob::getCommentText($test_commentid), undef, 'Checking comment');

check('Getting comment types',			sub { },								,	sub { return ECMDBJob::getCommentType($_[0]); },			{ 1 => 'R', 19 => 'A' });

DBAgent::process_oneline_sql("DELETE FROM ASSIGNMENTHISTORY WHERE job_number=?", [ $test_jobid ]);

check('Testing inProgress status',		sub { ut3p('preflightstatus', $_[0]); },	sub { return ECMDBJob::inProgress($test_jobid); },		{ 0 => 0, 1 => 1, 2 => 0 });


is(ECMDBJob::recordStart($test_jobid, $test_opcoid, $test_customerid, 'test_agency', 1, $test_userid), 1, 'Recording start');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM TIMECAPTURE WHERE job_number=? AND userid=?", [ $test_jobid, $test_userid ]);
is($retval, 1, 'Checking recording has started');

is(ECMDBJob::recordStop($test_jobid, $test_userid), 1, 'Recording stop');
($retval) = DBAgent::process_oneline_sql("SELECT endtime FROM TIMECAPTURE WHERE job_number=? AND userid=?", [ $test_jobid, $test_userid ]);
isnt($retval, undef, 'Checking recording has stopped');


is(ECMDBJob::validStringFilter('abcdefABCDEF0123456789'), 1, 'Checking valid filter');
isnt(ECMDBJob::validStringFilter('-=&`./^^\\'), 1, 'Checking invalid filter');

is(ECMDBJob::db_recordJobAction($test_jobid, $test_userid, 'action', $test_opcoid), 1, 'Recording job action');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM JOBEVENTS WHERE job_number=? AND action=?", [ $test_jobid, 'action' ]);
is($retval, 1, 'Testing job action record');


#ECMDBJob::db_recordLibrarySyncAction($test_jobid, $test_opcoid, 'test_agency', 'testuser', 5)
DBAgent::process_oneline_sql("DELETE FROM LIBRARYSYNCEVENTS WHERE nodeid=?", [ $test_jobid ]);
eval { isnt(ECMDBJob::db_recordLibrarySyncAction('aaaa', $test_opcoid, 'test_agency', 'testuser', 5), 1, 'Testing Library syncs action nodeid'); };
is($@, "nodeid_not_number\n", 'Testing Library syncs action nodeid fails');
eval { isnt(ECMDBJob::db_recordLibrarySyncAction($test_jobid, $test_opcoid, 'test_agency', 'testuser', 'aaa'), 1, 'Testing Library syncs action size'); };
is($@, "size_not_number\n", 'Testing Library syncs action size fails');
eval { isnt(ECMDBJob::db_recordLibrarySyncAction($test_jobid, $test_opcoid, 'test_agency', 'testuser', 5), 1, 'Testing Library syncs action opco'); };
is($@, "opco_not_found\n", 'Testing Library syncs action opco fails');
eval { isnt(ECMDBJob::db_recordLibrarySyncAction($test_jobid, 'TEST_OPCO', 'aaaa--', 'testuser', 5), 1, 'Testing Library syncs action customer'); };
is($@, "customer_not_found\n", 'Testing Library syncs action customer fails');
eval { isnt(ECMDBJob::db_recordLibrarySyncAction($test_jobid, 'TEST_OPCO', 'test_agency', 'asd', 5), 1, 'Testing Library syncs action user'); };
is($@, "user_not_found\n", 'Testing Library syncs action user fails');

$@ = undef;
eval { is(ECMDBJob::db_recordLibrarySyncAction($test_jobid, 'TEST_OPCO', 'test_agency', 'testuser', 500), 1, 'Testing Library syncs action succeeded'); };
is($@, '', 'Testing Library syncs action didn\'t fail');

($retval) = DBAgent::process_oneline_sql("SELECT COUNT(1) FROM LIBRARYSYNCEVENTS WHERE nodeid=?", [ $test_jobid ]);
is($retval, 1, 'Testing only one event was recorded');


isnt(ECMDBJob::db_setJobSize($test_jobid, 900), 0, 'Setting job size');
is(gt3p('comparefolder'), '900', 'Verifying size');

isnt(ECMDBJob::db_updateJobStatus($test_jobid, 1, 1, 1, 1), 0, 'Updating job status');
is(gt3p('preflightstatus'), 1, 'Checking preflight status was updated');
is(gt3p('qcstatus'), 1, 'Checking qcstatus was updated');
is(gt3p('assetscreated'), 1, 'Checking assetscreated was updated');
is(gt3p('hires'), 1, 'Checking hires was updated');

is(ECMDBJob::getPublisherName($test_jobid), gt3('publication'), 'Getting publisher name');
is(ECMDBJob::db_getWorkflow($test_jobid), gt3('workflow'), 'Getting workflow');


is(ECMDBJob::updateMaxCheckIns(4), 4, 'Updating max checkins');
is(ECMDBJob::getMaxCheckInLimit(), 4, 'Checking max checkin limit');
is(ECMDBJob::updateMaxCheckOuts(3), 3, 'Updating max checkouts');
is(ECMDBJob::getMaxCheckOutLimit(), 3, 'Checking max checkout limit');


DBAgent::process_oneline_sql("INSERT IGNORE INTO JOBVERSIONS (job_number, assignee, opcoid, customerid, version, filename, rec_timestamp)
												VALUES (?, ?, ?, ?, 1, 'filename', now())", [ $test_jobid, $test_userid, $test_opcoid, $test_customerid ]);
@aretval = ECMDBJob::getJobVersions($test_jobid);
ok(scalar(@aretval) > 0, 'Checking job versions');
is(verify_hashlist($aretval[0], 'versionid filename'), 1, 'Verifying structure');

isnt(ECMDBJob::getMaxJobVersion($test_jobid), 0, 'Checking max job version');			#??????????

isnt(ECMDBJob::enQueueCompare($test_userid, $test_jobid, 1, $test_jobid, 2), 0, 'Enqueueing compare');
is(ECMDBJob::isCompareOnQueue($test_userid, $test_jobid, 1, $test_jobid, 2), 1, 'Checking is on queue');
($retval) = DBAgent::process_oneline_sql("SELECT 1 FROM COMPAREQUEUEHISTORY WHERE userid=? AND job_number1=? AND version1=? AND job_number2=? AND version2=?", [ $test_userid, $test_jobid, 1, $test_jobid, 2 ]);
is($retval, 1, 'Checking history was updated');


@aretval = ECMDBJob::getDaemons();
ok(scalar(@aretval) > 0, 'Checking daemons');
is(verify_hashlist($aretval[0], 'id name lastseen onoff sleepdelay serverid'), 1, 'Checking daemon list structure');

is(ECMDBJob::updateDaemonOn(1, 0, 1), 1, 'Turning Daemon 1 off');
($retval, $testval) = DBAgent::process_oneline_sql("SELECT onoff, sleepdelay FROM DAEMONCONFIG WHERE id=?", [ '1' ]);
is($retval, 0, 'Verifying (1 / 2)');
is($testval, 1, 'Verifying (2 / 2)');
is(ECMDBJob::updateDaemonOn(1, 1, 4), 1, 'Turning Daemon 1 on');
($retval, $testval) = DBAgent::process_oneline_sql("SELECT onoff, sleepdelay FROM DAEMONCONFIG WHERE id=?", [ '1' ]);
is($retval, 1, 'Verifying (1 / 2)');
is($testval, 4, 'Verifying (2 / 2)');

is(ECMDBJob::db_getopcoidfromcompany('TEST_OPCO'), $test_opcoid, 'Getting OPCO id from name');

@aretval = ECMDBJob::db_getmedia($test_opcoid, 'test_agency', 3650);
ok(scalar(@aretval) > 0, 'Getting media');
is(verify_hashlist($aretval[0], 'id WORKFLOW'), 1, 'Verifying media structure');

ok(ECMDBJob::boxcountshow($test_opcoid, 'test_agency', undef, undef) > 0, 'Testing boxcountshow');

DBAgent::process_oneline_sql("INSERT IGNORE INTO USEROPCOS (userid, opcoid) VALUES (?, ?)", [ $test_userid, $test_opcoid ]);
ut3p('assignee', $test_userid);
$retval = ECMDBJob::db_avlresources($test_opcoid);
@aretval = @$retval;
ok(scalar(@aretval) > 0, 'Got resource');
is(verify_hashlist($aretval[0], 'username userlogin estimate'), 1, 'Verifying structure');

isnt( GraphCalc::graphCalc($test_opcoid, 'test_agency', undef, 'estimated'), undef, 'graphCalc test');

sub check
{
	my ($name, $usub, $test, $values) = @_;
	
	my $index = 0;
	my $total = scalar(keys(%$values));
	foreach my $key (keys(%$values))
	{
		$usub->($key);
		is($test->($key), $values->{$key}, "$name" . ($total > 1 ? " (" . ++$index . " / $total)" : ""));
	}
}




sub verify_hashlist
{
	my ($hash, $list) = @_;
	
	my $retval = 1;
	
	$list =~ s/,//g;
	$list =~ s/\s+/-/g;
	foreach(split(/-/, $list))
	{
		if(!exists($hash->{$_}))
		{
			fail("\"$_\" was not defined");
			$retval = 0;
		}
	}
	
	return $retval;
}

done_testing();



