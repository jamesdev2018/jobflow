#!/usr/local/bin/perl
package DigitalAssets;

use strict;
use warnings;

use Carp qw/carp/;
use Net::FTP::File;
use Data::Dumper;
use Cwd;
use File::Basename;

# Home grown
use lib 'modules';
use JSON;

use Validate;

require "config.pl";
require "general.pl";

require "databaseJob.pl";
require "databaseOpcos.pl";
require "jobpath.pl";
require "databaseAudit.pl";


# TODO dirname/basename/pwd - use Perl classes Cwd, File::Basename

my $assetUploadTmpDir	= Config::getConfig("uploaddirectory");
my $digitAssetsFolder	= Config::getConfig("digitalAssetsFolder" );
my $documentAssetsFolder	= Config::getConfig("documentAssetsFolder" );
my $tmpfileloc		= Config::getConfig("tempfileloc");
my $movingObjExt	= Config::getConfig("movingObjExt");
my $digital_convert_formats             = Config::getConfig("digital_convert");

sub getDocumentFolder {
	return Config::getConfig("documentAssetsFolder" ) || "SUPPORTING_DOCUMENTS"
}

sub getDigitAssetsFolder {
	return Config::getConfig("digitalAssetsFolder") || "DIGITAL_ASSETS"
}

##########################################################################
#storeDigitalAssetsInGPFS: To store the each user uploaded digital assets in /gpfs location.
##########################################################################
sub storeDigitalAssetsInGPFS {
	my ( $jobid, $digitalAssetsSource, $digitalFileName, $oriAssetName, $docflg ) = @_;
	my $ftp;

	eval {
		Validate::job_number( $jobid );
		die "Asset source not supplied" unless ( $digitalAssetsSource );
		die "Digital filename not supplied" unless ( $digitalFileName );

		my ( $destRootDir, $jobopcoid ) = JobPath::getJobPath( $jobid );

		my ( $assetPrefixName, $assetExt ) = getFileNameAndExt( $digitalFileName );

		##Resuing storing files in GPFS of DIGITAL TO DOCUMENT
		$docflg = 0 unless defined $docflg;
		my $AssetsFolder = "";
		if ( $docflg == 1 ) {
			$AssetsFolder		= getDocumentFolder();
		} else	{
			$AssetsFolder		= getDigitAssetsFolder();
		}
		my $digitalAssetsDest  = sprintf( "%s/%s/%s", $destRootDir, $AssetsFolder, $digitalFileName );

		#get the ftp details
		my ( $ftpServer, $ftpUserName, $ftpPassword ) = ECMDBOpcos::db_getFTPDetails( $jobopcoid );
		die "Invalid FTP server for opcoid '$jobopcoid'" unless ( $ftpServer );

		if ( -s "$digitalAssetsSource" ) {
        
			##Transfer the file to remote server using FTP
			$ftp = Net::FTP->new( $ftpServer, Debug => 0 ) or die "Cannot connect to FTP server $@";
			if ( $ftp ) {
				$ftp->login( $ftpUserName, $ftpPassword ) or die "Cannot login", $ftp->message;
				$ftp->binary();
				my $assetRepoJobFldr = dirname( $destRootDir );
				my $dirExist = $ftp->cwd( "$destRootDir" );
				if ( $dirExist eq "" ) {
					$ftp->mkdir ( "$destRootDir/$AssetsFolder", "true") or die "Directory creation failed, $destRootDir,", $ftp->message;
				} else {
					my $dirExist = $ftp->cwd( "$AssetsFolder" );
					if ( $dirExist eq "" ) {
						$ftp->mkdir ( "$AssetsFolder", "true") or die " digital Directory creation failed, $AssetsFolder,", $ftp->message;
					}
				}
				$ftp->site( "chmod 777 $destRootDir" );
				$ftp->site( "chmod 777 $destRootDir/$AssetsFolder" );
				$ftp->binary();
				$ftp->put( "$digitalAssetsSource", "$digitalAssetsDest" ) or die "put failed, ", $ftp->message;

=com				if ( $assetExt =~ /$digital_convert_formats/i ) {
					my $assetFasetStart = basename( $digitalAssetsSource );
					my ( $assetFastStartPrefixName, $assetFastStartExt ) = getFileNameAndExt( $assetFasetStart );
					my ( $currAssetFastStartPrefixName, $currAssetFastStartExt ) = getFileNameAndExt( $oriAssetName );
					my $tempAssetPath	= getAssetUploadTempPath( $jobid );
					my $assetFastStartSrc 	= sprintf( "%s/%s.%s", "$tempAssetPath", "$assetFastStartPrefixName", "mp4" );
					my $assetFastStartDest  = sprintf( "%s/%s/%s.%s", "$destRootDir", "$digitAssetsFolder", "$assetFastStartPrefixName",  "mp4" );
					my $currAssetFastStart  = sprintf( "%s/%s/%s.%s", "$destRootDir", "$digitAssetsFolder", "$currAssetFastStartPrefixName", "mp4" );

					$ftp->put( "$digitalAssetsDest" );
					if ( $ftp->message !~ /Transfer complete/i ) {
						 die "put failed, $oriAssetName",$ftp->message; 	
					}
					$ftp->copy( "$assetFastStartDest", "$currAssetFastStart" );
					$ftp->site("chmod 777 $currAssetFastStart");
				}
=cut
				if ( $oriAssetName ne "" ) {
					$ftp->copy( "$digitalAssetsDest", "$destRootDir/$AssetsFolder/$oriAssetName" );	
            				$ftp->site("chmod 777 $destRootDir/$AssetsFolder/$oriAssetName");
				}
				$ftp->site("chmod 777 $digitalAssetsDest");
				$ftp->quit;
       				$ftp = undef; 
			}
		}
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "storeDigitalAssetsInGPFS: $error";
		if ( defined $ftp ) {
			$ftp->quit;
			$ftp = undef;
		}
	}

} #storeDigitalAssetsInGPFS


##########################################################################
##deleteDigitAssetFrmGPFS: To remove the Static Asset from /gpfs when QC is set to 'DECLINE'
###########################################################################
sub deleteDigitAssetFrmGPFS {

    my $jobid               = $_[0];
    my $assetName			= $_[1];
    my $assetVersionFlg		= $_[2] || 0 ;
	my $type				= $_[3] || '';
	
	my $ftp;

	eval {

		my ( $jobpath, $jobopcoid ) = JobPath::getJobPath( $jobid ); # jobpath has no trailing slash

		my $assetReopPath	= "";
		if ( $type eq "document" ) {
			$assetReopPath		= sprintf ( "%s/%s", $jobpath, "$documentAssetsFolder" );
		} else {
			$assetReopPath		= sprintf ( "%s/%s", $jobpath, "$digitAssetsFolder" );
		}

		my ( $fileNamePrepix, $fileExt ) = getFileNameAndExt( $assetName );
		#get the ftp details
		my ( $ftpServer, $ftpUserName, $ftpPassword ) = ECMDBOpcos::db_getFTPDetails( $jobopcoid ) ;
		die "Invalid FTP server for opcoid $jobopcoid" unless ( $ftpServer );

        ##Transfer the file to remote server using FTP
		my $ftp = Net::FTP->new( $ftpServer, Debug => 0 ) or die "Cannot connect to FTP server $@";
		if ( $ftp ) {
			$ftp->login( $ftpUserName, $ftpPassword ) or die "Cannot login", $ftp->message;
			if ( $assetVersionFlg == 0 ) {
				my @digitalAssets = $ftp->ls("$assetReopPath");
				chomp( @digitalAssets );
				
				foreach my $assetAbsoluteFile ( @digitalAssets ) {
					my $assetFileName = (split(/\//, $assetAbsoluteFile))[-1];
        			if ( $assetFileName =~ /^($fileNamePrepix)_static\.(.*)$/i ) {
						$ftp->cwd( "$assetReopPath" );
						
						my $isFileExists = $ftp->isfile($assetFileName);

						if( $isFileExists ){
							$ftp->delete( "$assetFileName" ) or die "Asset deletetion failed for $jobid, $assetFileName\n", $ftp->message;
						}
					}
				}
			} else {
				$ftp->cwd( "$assetReopPath" );
				$ftp->delete( "$assetName" ) or die "Asset deletetion failed for $jobid, $assetName\n", $ftp->message;
			}
            $ftp->quit;
			$ftp = undef;
        }
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "deleteDigitalAssetFrmGPFS: $error";
	
		if ( defined $ftp ) {
			$ftp->quit();
		}
	}

} #deleteDigitAssetFrmGPFS

##########################################################################
### deleteAllAssets: Delete all assets from db, '/temp' and '/gpfs'
#############################################################################
sub deleteAllAssets {

	my $query				= $_[0];
	my $session				= $_[1];
	my $jobid				= $query->param( "jobid" );
	my $mode				= $query->param( "mode" );

	if ( $jobid ) {
		my $uploadLoc		= sprintf ("%s/%s_%s", "$assetUploadTmpDir", "$jobid", "assets");		
		if ( -d "$uploadLoc" ) {
			`rm -rf $uploadLoc/*.*`;
		}
		ECMDBJob::db_deleteAllAssets( $jobid );
		eraseAllAssetsFromGPFS( $jobid, $session );
	}
	my $vars = {
        deleteSuccess    => "YES",
    };

    if ( $mode eq "json") {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}#deleteAllAssets

##########################################################################
### eraseAllAssetsFromGPFS: 
###########################################################################
sub eraseAllAssetsFromGPFS {
	my ( $jobid, $session ) = @_;

	my $userid				= $session->param( 'userid' );
	my $userName    		= $session->param( 'username' );

	my ( $jobpath, $jobopcoid ) = JobPath::getJobPath( $jobid );

    my $assetReopPath       = $jobpath . '/' . $digitAssetsFolder;

    #get the ftp details
    my ( $ftpServer, $ftpUserName, $ftpPassword ) = ECMDBOpcos::db_getFTPDetails( $jobopcoid ) ;
	die "Invalid FTP server for opcoid '$jobopcoid'" unless ( $ftpServer );

    ##Transfter the file to remote server using FTP
	my $ftp = Net::FTP->new( $ftpServer, Debug => 0 ) or die "Cannot connect to FTP server $@";

	if ( $ftp ) {
		$ftp->login( $ftpUserName, $ftpPassword ) or die "Cannot login", $ftp->message;
		my @digitalAssets = $ftp->ls("$assetReopPath");
		chomp( @digitalAssets );

		foreach my $assetAbsoluteFile ( @digitalAssets ) {
			my $gpfsAssetName = `basename $assetAbsoluteFile`;
			if ( $gpfsAssetName =~ /^(.*)\.(.*)$/g ) {
				$ftp->delete( "$assetAbsoluteFile" ) or die "Asset deletetion failed for $jobid, $assetAbsoluteFile\n", $ftp->message;
				my $assetName = `basename $assetAbsoluteFile`;
				chomp( $assetName );
				ECMDBAudit::db_auditLog( $userid, "ASSETDELETE", $jobid, "Asset $assetName deleted by $userName" );
			}
        }
	}
	$ftp->quit;
	return 1;
}#eraseAllAssetsFromGPFS


##############################################################################
#sub getFileNameAndExt: To get the file prefix and extension. (cloned from jobs.pl)
###############################################################################

sub getFileNameAndExt {

	my $fileName 		= shift;

	my $fileNamePrefix	= undef;
	my $fileExt			= undef; 

	if ( $fileName =~ /^(.*)\.(.*)$/ ) {
		$fileNamePrefix = $1;
		$fileExt 		= $2;
	}
	return ( $fileNamePrefix, $fileExt );
}

##############################################################################
# reviewDigitalAsset: used to get asset from /gpfs, and asset details from DB. (called by jobflow.pl)
##############################################################################

sub reviewDigitalAsset {
	my ( $query, $session ) = @_;

	my $mode                = $query->param( "mode" ) ||'';
	my $jobid               = $query->param("jobid");

	my $path				= undef;
	my $digitalAssetSrc		= "";	
	my $movingObjFlag		= undef;

	my ( $jobpath, $jobopcoid ) = JobPath::getJobPath( $jobid ); # jobpath has no trailing slash
	my @assetDetail = ECMDBJob::db_getfilename($jobid, "ShowAsset");

    my $assetReopPath		= $jobpath . '/' . $digitAssetsFolder;

	my $tmpPath = getAssetUploadTempPath( $jobid );

	$tmpfileloc = sprintf( "%s/%s", "$tmpfileloc", "$jobid" );
	system ( "cd ../; mkdir -p $tmpfileloc; chmod 777 $tmpfileloc" );

	my $curDir = getcwd;
	my $dirName = dirname( $curDir );
	my $tempAssetDir = $dirName . '/' . $tmpfileloc;
	chdir( $tempAssetDir );

    my ( $ftpServer, $ftpUserName, $ftpPassword ) = ECMDBOpcos::db_getFTPDetails( $jobopcoid ) ;
	die "Invalid FTP server for opcoid $jobopcoid" unless ( $ftpServer );

    my $ftp = Net::FTP->new( $ftpServer, Debug => 0 ) or die "Cannot connect to FTP server $@";

    if ( $ftp ) {

        $ftp->login( $ftpUserName, $ftpPassword ) or die "Cannot login", $ftp->message;
        $ftp->cwd( "$assetReopPath" );
        $ftp->binary();
		foreach my $asset_ref ( @assetDetail ) {
        	my $digitalAssetName = $asset_ref->{filename};
        	$ftp->get( "$assetReopPath/$digitalAssetName" ) or die "Asset download fails for $digitalAssetName\n", $ftp->message;
    	}
        $ftp->close();
    }
	chdir( $curDir );

	foreach my $asset_ref ( @assetDetail ) {

		my $digitalAssetName = $asset_ref->{filename};
		chmod( 0777, "$tempAssetDir/$digitalAssetName" );

		my ( $assetPrefix, $assetExt ) =  getFileNameAndExt( $digitalAssetName );
		if ( $assetExt =~ /^zip$/i ) {

			my $cmdUnzip = "cd ../; cd $tmpfileloc; unzip -uq $digitalAssetName \"$assetPrefix/*\"; chmod -R 777 $digitalAssetName $assetPrefix;";
			system ( "$cmdUnzip" );

			if ( $? == -1 ) {
				die " unzip failed!!! for $cmdUnzip\n";
			}

			my $listHtmlFile  = `cd ../; ls -ltr $tmpfileloc/$assetPrefix/*html`;
			chomp($listHtmlFile);
			$digitalAssetSrc = (split( " ", $listHtmlFile, 99))[8];

			my @movingObjFiles = `cd ../; cd $tmpfileloc/$assetPrefix/; ls -ltrR`;

			if ( $digitalAssetSrc ne "" ) {
				$movingObjFlag = (grep(/$movingObjExt/, @movingObjFiles )) ? 1: 0; 
			}

		} else {
			$digitalAssetSrc = sprintf( "%s/%s", "$tmpfileloc", "$digitalAssetName" );
		}

		$movingObjFlag = 1 if ( $assetExt =~ /$movingObjExt/i );
		$digitalAssetSrc = sprintf ( "../%s", "$digitalAssetSrc" );
		$asset_ref->{filename} = sprintf ( "%s,%s,%s", "$digitalAssetName", "$movingObjFlag", "$digitalAssetSrc" );
	}

	my $vars  = {
        digitalAssetDet     => \@assetDetail,
        job_number          => $jobid,
	};

	if ( lc($mode) eq "json" ) {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
} #reviewDigitalAsset

sub getAssetUploadTempPath {

	my $jobID	= $_[0];
	return sprintf("%s/%s_%s", "$assetUploadTmpDir", "$jobID", "assets");
}

sub constructAssetDetails {
	my ( $query, $session ) = @_;

    my $jobid               = $query->param( "jobid" );
    my $mode                = $query->param( "mode" ) || '';

  	my $userid      		= $session->param( 'userid' );

	my @filedetails         = ECMDBJob::db_filedetails($jobid);
    my $userinteractive     = ECMDBUser::db_interactive($userid);

	my $vars = {
        filedetails     	=>\@filedetails,
        userinteractive 	=>$userinteractive,
    };

    if ( $mode eq "json") {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

1;

