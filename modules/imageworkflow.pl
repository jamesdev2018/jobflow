#!/usr/local/bin/perl
package Imageworkflow;

use strict;
use warnings;

use Template;
use Scalar::Util qw( looks_like_number );
use Carp qw/carp/;

use lib 'modules';

require "databaseJob.pl";
require "databaseOpcos.pl";
require "databaseImageworkflow.pl";
require "opcos.pl";
require "jf_wsapi.pl"; # JF_WSAPI package


##############################################################################
# the template object 
##############################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";


##############################################################################
#		EditImageworkflow
#
##############################################################################
sub EditImageworkflow {
	my $query 			= $_[0];
	my $session 		= $_[1];
	my $opcoid			= $query->param( "opcoid" );	
	my $tabFormPosted   = $query->param( "tabFormPosted" );
	
	my ($bookingform, $xmppath, $tmppath, $batchserver, $autocheckout, $autocheckouttime, $autoOPCOID, $ingestionhost, $ingestionpath, $ingestionprotocol, $ingestionusername, $ingestionopassword, $mastersync, $autocheckin);
	

	if ( $tabFormPosted eq "yes" ) { 	#get from form
	    $bookingform  = General::htmlEditFormat( $query->param( "bookingform" ));
		$xmppath      		= $query->param( "xmppath" );
		$tmppath      		= $query->param( "tmppath" );
		$batchserver  		= $query->param( "batchserver" );
		$autocheckout  		= $query->param( "Autocheckout" );
		$autocheckouttime   = $query->param( "Autocheckouttime" );
		$autoOPCOID  		= $query->param( "AutocheckoutOPCOID" );
		$ingestionhost = $query->param( "ingestionhost" );
        $ingestionpath = $query->param( "ingestionpath" );
        $ingestionprotocol = $query->param( "ingestionprotocol" );
		$ingestionusername = $query->param( "ingestionusername" );
		$ingestionopassword = $query->param( "ingestionpassword" );
		$mastersync = $query->param( "Mastersync" );
		$autocheckin = $query->param( "Autocheckin" );
		
		ECMDBImageworkflow::updateImageworkflow( $opcoid, $bookingform, $xmppath, $tmppath, $batchserver, $autocheckout, $autocheckouttime, $autoOPCOID , $ingestionhost, $ingestionpath, $ingestionprotocol, $ingestionusername, $ingestionopassword, $mastersync, $autocheckin );
	}
	
	
	#my $vars = {
	#	opcoid			=> $opcoid,
	#	bookingform	    => $bookingform,
	#};

	#$tt->process( 'ecm_html/editoperatingcompany.html', $vars )
    #		|| die $tt->error(), "\n";
	
	Opcos::EditOperatingCompany( $query, $session );

}


##############################################################################
#		BookImage
#		Called by jobflow.pl, for action bookimage (imageworkflow/add)
##############################################################################
sub BookImage {
    
	my ( $query, $session ) = @_;

	my $opcoid			= $query->param( "opcoid" );
	my $formPosted		= $query->param( "formPosted" );
	my $formUpdated		= $query->param( "formUpdated" );
	my $job_number      = $query->param( "job" );

	my $isImageFTP      = ECMDBImageworkflow::isImageFTP( $opcoid );
	
	my @opco			= Opcos::getOperatingCompany( $opcoid );
	my $opconame		= $opco[0]{ opconame };
    my $myform          = "<table>";

	if ($formPosted eq 'yes'){
	
	    my $imageUniqueID	= $query->param( "imageUniqueID" );
		my $description	    = $query->param( "description" );
		my $type	        = $query->param( "type" );
		my $userComment	    = $query->param( "userComment" );
		my $identifier      = $query->param( "identifier" );
		my $coverage        = $query->param( "coverage" );
		my $level1          = $query->param( "level1" );
		my $level2          = $query->param( "level2" );
		my $level3          = $query->param( "level3" );
		my $level4          = $query->param( "level4" );
		my $level5          = $query->param( "level5" );
		my $level6          = $query->param( "level6" );
		my $suffix          = $query->param( "suffix" );
		my $cameraOperator  = $query->param( "cameraOperator" );
		
		#capitalise
		$level1 = uc($level1);
		$level2 = uc($level2);
		$level3 = uc($level3);
		$level4 = uc($level4);
		$level5 = uc($level5);
		$level6 = uc($level6);
		
		#replace spaces with underscore
        $level1=~s/ /_/g;
		$level2=~s/ /_/g;
		$level3=~s/ /_/g;
		$level4=~s/ /_/g;
		$level5=~s/ /_/g;
		$level6=~s/ /_/g;
		
		
		#Validation
		my $validateMessage = "";
		
		if((not defined $imageUniqueID) or ($imageUniqueID eq '')){
		  $validateMessage = "SKU Number required<br/>";
		} else {
		
			my $checkSKU = ECMDBImageworkflow::checkSKU( $imageUniqueID );
			
			if(($checkSKU == 1) && ($suffix eq '')){
			   $validateMessage = "SKU Number exists please use an SKU Appendix<br/>";
			}
		}
		
		if($identifier eq ''){
		   $validateMessage = $validateMessage."Billing number required<br/>"; 
		}

		if($validateMessage ne ''){	
			my @bookingform_	= ECMDBImageworkflow::getBookingForm( $opcoid );
			my @operators_		= ECMDBImageworkflow::getCameraOperators( $opcoid );
			my $vars = {
				isImageFTP       => $isImageFTP,
				job_number       => $job_number,
				imageUniqueID    => $imageUniqueID,
				suffix           => $suffix,
				description      => $description,
				type             => $type,
				opcoid	          => $opcoid,
				opconame	      => $opconame,
				bookingform      => \@bookingform_,
				identifier       => $identifier,
				coverage         => $coverage,
				operators        => \@operators_,
				userComment      => $userComment,
				validateMessage  => $validateMessage,
				myform           => $myform,
			};

			General::setHeaderVars( $vars, $session );

			$tt->process( 'ecm_html/bookimage.html', $vars );
			return;
		}

		my $jobid = ECMDBImageworkflow::createJob($opcoid, $imageUniqueID, $level1, $level2, $suffix, $cameraOperator, $coverage, $userComment);
		CreateXMP($opcoid, $imageUniqueID, $description, $type, $userComment, $identifier, $coverage, $level1, $level2, $level3, $level4, $level5, $level6, $jobid);
		return 'dashboard'; # Force display of Job dashboard page
	
	} elsif(($formUpdated eq 'yes') && (defined $job_number && $job_number ne "")){
	     
		#FTP ingested image
		my $description     = $query->param( "description" );
		my $identifier      = $query->param( "identifier" );
		my $coverage        = $query->param( "coverage" );
		my $userComment     = $query->param( "userComment" );
		ECMDBImageworkflow::updateJob($job_number, $opcoid, $userComment, $identifier, $coverage);
		return 'dashboard'; # Force display of Job dashboard page
	}

	my @bookingform		= ECMDBImageworkflow::getBookingForm( $opcoid );
	my @operators		= ECMDBImageworkflow::getCameraOperators( $opcoid );

	foreach my $row(@bookingform) {

		my $id         = $row->{'id'};
		my $label      = $row->{'label'};
		my $fieldname  = $row->{'fieldname'};
		my $fieldvalue = $row->{'fieldvalue'};
		my $parentid   = $row->{'parentid'};
	   
		if($fieldvalue eq ""){
			$myform = $myform . "<tr><td width='150'>$label</td><td><input type='text' name='$fieldname' autocomplete='on' /></td></tr>";
		}

		### fieldvalue requires drop down
 
		elsif($fieldvalue ne "" and $parentid == 0){
			$myform = $myform . "<tr><td>$label</td><td><select name='$fieldname'><option value='$fieldvalue'>$fieldvalue</option>";
			### any children?
			foreach my $child(@bookingform) {
				my $childfieldname  = $child->{'fieldname'};
				my $childfieldvalue = $child->{'fieldvalue'};
				my $childparentid   = $child->{'parentid'};
				if($childfieldvalue ne "" and $childparentid != 0 and $id == $childparentid){
					$myform = $myform . "<option value='$childfieldvalue'>$childfieldvalue</option>";
				}
			}
			$myform = $myform . "</select></td></tr>";
		}
	}

	$myform = $myform . "</table>";

	my $vars = {
		 isImageFTP   => $isImageFTP,
		 job_number   => $job_number,
		 opcoid	      => $opcoid,
		 opconame	  => $opconame,
		 bookingform  => \@bookingform,
		 operators    => \@operators,
		 myform       => $myform,
	};
	
	#Image via FTP form 
	if (($isImageFTP) && (defined $job_number && $job_number ne "")){
		my $imageUniqueID = ECMDBImageworkflow::getSKU($job_number);
		if ($imageUniqueID ne ""){
			$vars -> {imageUniqueID} = $imageUniqueID;
		} 

		my $userComment = ECMDBImageworkflow::getUserComment($job_number, $opcoid);
		if ($userComment ne ""){
			$vars -> {userComment} = $userComment;
		}
      
		my $identifier = ECMDBImageworkflow::getIdentifier($job_number, $opcoid);
		if ($identifier ne ""){
			$vars -> {identifier} = $identifier;
		}
 
		my $description = ECMDBImageworkflow::getDescription($job_number, $opcoid);
		if ($description ne ""){
			$vars -> {description} = $description;
		}		 
	}

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/bookimage.html', $vars );
}


##############################################################################
#		CreateXMP
#
##############################################################################
sub CreateXMP {
   
   my $opcoID	        = $_[0];
   my $imageUniqueID	= $_[1];
   my $description	    = $_[2];
   my $type	            = $_[3];
   my $userComment	    = $_[4];
   my $identifier       = $_[5];
   my $coverage         = $_[6];
   my $level1           = $_[7];
   my $level2           = $_[8];
   my $level3           = $_[9];
   my $level4           = $_[10];
   my $level5           = $_[11];
   my $level6           = $_[12];
   my $jobid            = $_[13];
   
   my $xmpPath = ECMDBImageworkflow::db_getXMPPath($opcoID);
   my $xmpFile = $imageUniqueID.".xmp";
   my $jobPath = ECMDBJob::db_jobPath( $jobid );
   
   $xmpFile = $xmpPath.$xmpFile;
   
   unless (-d $xmpPath) {
    mkdir $xmpPath;
   }
   
   unless(open FILE, '>'.$xmpFile) {
	die "Unable to create $xmpFile";
   }
   
   my $mastersPath = $level1.'/';
   
   if ($level2 ne ''){
       $mastersPath = $mastersPath.$level2.'/';
   }
   if ($level3 ne ''){
       $mastersPath = $mastersPath.$level3.'/';
   }
   if ($level4 ne ''){
       $mastersPath = $mastersPath.$level4.'/';
   }
   if ($level5 ne ''){
       $mastersPath = $mastersPath.$level5.'/';
   }
   if ($level6 ne ''){
       $mastersPath = $mastersPath.$level6.'/';
   }
   
   
   
   #pre-attributes
   print FILE "<?xpacket begin='?' id='W5M0MpCehiHzreSzNTczkc9d'?><x:xmpmeta xmlns:x='adobe:ns:meta/'><rdf:RDF xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' xmlns:dc='http://purl.org/dc/elements/1.1/'><rdf:Description rdf:about=''
xmlns:exif='http://ns.adobe.com/exif/1.0/'>";

   print FILE "<dc:description><rdf:Alt><rdf:li xml:lang='x-default'>$description</rdf:li></rdf:Alt></dc:description>";
   
   print FILE "<exif:CatalogSets>$mastersPath</exif:CatalogSets>";
   
   print FILE "<dc:identifier><rdf:Alt><rdf:li xml:lang='x-default'>$identifier</rdf:li></rdf:Alt></dc:identifier>";
   
   print FILE "<dc:type><rdf:Alt><rdf:li xml:lang='x-default'>$type</rdf:li></rdf:Alt></dc:type>";
   
   print FILE "<dc:coverage><rdf:Alt><rdf:li xml:lang='x-default'>$coverage</rdf:li></rdf:Alt></dc:coverage>";
   
   print FILE "<dc:relation><rdf:Alt><rdf:li xml:lang='x-default'>$jobPath</rdf:li></rdf:Alt></dc:relation>";
   
   print FILE "<exif:ImageUniqueID>$imageUniqueID</exif:ImageUniqueID>";
   
   print FILE "<exif:UserComment>$userComment</exif:UserComment>";
   
   #post-attributes
   print FILE "</rdf:Description></rdf:RDF></x:xmpmeta><?xpacket end='r'?>";
   
   close FILE;
   chmod 0755, $xmpFile;
}

sub copyWipToMastersAndSync {
	my ( $query, $session, $jobid, $opcoid, $checkoutopcoid, $history ) = @_;

	eval {
		my $studioServer      = ECMDBOpcos::db_getDomainName($opcoid);
		my $wipPath           = ECMDBOpcos::db_getOpcoWIP($opcoid);
		my $jobPath           = ECMDBJob::db_jobPath($jobid);
		my $mastersHotPath    = ECMDBImageworkflow::db_getMastersHotPath($opcoid);
		my $customerID        = ECMDBImageworkflow::db_getCustomerID($opcoid);
		my $masterCustomerID  = ECMDBImageworkflow::db_getMasterCustomerID($opcoid);
		my $sessionID         = $session->id;   
		my $imageJobPath      = $wipPath.'/'.$jobPath.'/IMAGE';

		my ( $listCurl, $dirlistCGI ) = JF_WSAPI::fileopsOther( $studioServer, { function => 'dirlist', source => $imageJobPath, destination => $imageJobPath } );

		push( @$history, "fileops request: $dirlistCGI" );
		push( @$history, "fileops response: $listCurl" );

		# Find first file in list
		my $file;
		my $size;

		die "fileops response: Missing [[ or  ]]" if ( ( $listCurl !~ /\[\[/ ) || ( $listCurl !~ /\]\]/ ) );

		$listCurl =~ s/^DIR\<br\>\[\[//g; # Remove preamble
		$listCurl =~ s/\]\]$//g; # Remove post amble
		$listCurl =~ s/\|$//g; # Remove trailing |
		push( @$history, "string now '$listCurl'" );
		my @list = split /\|/, $listCurl;
		foreach my $e ( @list ) {
			push( @$history, "entry '$e'" );
			if ( $e =~ /^\^F/ ) {
				$e =~ s/^\^F//g;
				( $file, $size ) = split /,/, $e;
				push( @$history, "Found file '$file', size '$size'" );
				last;
			}
		}
		die "No file found in dirlist" if ( ! defined $file );

		# exif data on batchserver
		my ( $masterPath, $exifCGI ) = JF_WSAPI::exifToolOther( $studioServer, 
			{ tag => 'CatalogSets', source => "$imageJobPath/$file", hotpath => $mastersHotPath } );

		push( @$history, "exiftool request: $exifCGI" );
		push( @$history, "exiftool response: $masterPath" );

		# copy on batchserver
		my ( $copyCurl, $copyCGI ) = JF_WSAPI::fileopsOther( $studioServer, 
				{ function => 'copytype', source => "$imageJobPath/$file", destination => "$mastersHotPath/$masterPath$file" } );

		push( @$history, "fileops request: $copyCGI" );
		push( @$history, "fileops response: $copyCurl" );

		# TODO should add some sense checking of exiftool response here

		# Masters checkin on webserver (and sync to target opco after check-in)

		# Sets up session details used by Masters required for use with tsid.
		Masters::SupplyCredentials( $session, $query ); 

		my ( $checkinCurl, $checkinCGI ) = JF_WSAPI::mastersOther( $studioServer, 
				{ action => 'checkinfile', sourcedir => $masterPath, targetdir => $masterPath, sourcefile => $file,
				  customerid => $masterCustomerID, job => $jobid, sync => $checkoutopcoid,
				  tsid => "" . $session->id, username => 'ImageWorkflow', sessionid => 1 } );

		push( @$history, "Request for Masters checkin, $checkinCGI" );
		push( @$history, "Response from Masters checkin, $checkinCurl" );

		ECMDBImageworkflow::updateCurlhistory( $jobid, $checkinCGI );

		if (index($checkinCurl, 'SUCCESS') != -1) {
				my $idx = index($checkinCurl, ".");
				my $nodeID = substr $checkinCurl, $idx+1;
				my $idx_ = index($nodeID, ".");
				$nodeID = substr $nodeID, 0, $idx_;
				push( @$history, "Response from Masters, nodeid $nodeID" );
		}
	};
	if ( $@ ) {
		my $error =$@;
		push( @$history, "Masters Checkin and sync failed, $error" );
	}
}

# code lifted from Jobs.pl, completeJob().

sub completeImageWorkflowJob {
	my ( $query, $session, $jobid, $opcoid, $history ) = @_;

	my @local_history;
	$history = \@local_history unless ( defined $history );

	my $userid = $session->param( 'userid' );

	eval {
		my $checkoutopcoid  = ECMDBImageworkflow::db_Autocheckoutdetails($opcoid, "CHECKOUTOPCOID");
		my $location = ECMDBJob::db_getLocation($jobid);

		push( @$history, "Checkout opcoid $checkoutopcoid, location $location" );

		#Autocheckin
		if(ECMDBImageworkflow::db_isAutocheckin($opcoid)){

			# unassign job
			push( @$history, "Unassign job" );
			ECMDBJob::unassignJob( $jobid );

			if ($location != $opcoid){
				#need to check the job back in
				push( @$history, "Checkin job back in" );
				ECMDBImageworkflow::imageCheckin($jobid, $opcoid, $userid);		   
			}

	   		if ($location == $checkoutopcoid){
	   			#need to check the job back in
				push( @$history, "Checkin job back in (2)" );
				ECMDBImageworkflow::imageCheckin($jobid, $opcoid, $userid);
			}

			#copy wip to masters and sync
			if ( ECMDBImageworkflow::db_isMastersync( $opcoid ) ) {

				push( @$history, "Copy WIP to Masters and Sync" );
				copyWipToMastersAndSync( $query, $session, $jobid, $opcoid, $checkoutopcoid, $history );

	    		} # endif isMastersSync
		} # endif db_isAutocheckin
	};
	if ( $@ ) {
		my $error = $@;
		push( @$history, "completeImageWorkflowJob error, $error" );
		die "Failed in completeImageWorkflowJob";
	}
}

sub getSupportedFileTypes {

	my @fileTypes = ("png","jpg","tiff","gif","psd");

	my $imageWorkflowFileTypes = Config::getConfig("imageworkflowfiletypes"); # Overide this default list if present
	@fileTypes = split( ',', $imageWorkflowFileTypes ) if ( defined $imageWorkflowFileTypes );

	return @fileTypes;
}

sub isSupportedFileType {
	my ( $fileType ) = @_;

	return 0 unless ( ( defined $fileType ) && ( $fileType ne '' ) );

	my @extList = getSupportedFileTypes();

	my $supported = 0;
	foreach my $t ( @extList ) {
		if ( $t eq $fileType ) {
			$supported = 1;
			last;
		}
	}
	return $supported;
}

sub fileUploadImageWorkflowJob {
	my ( $query, $jobid, $opcoid, $myfile, $upload_dir, $history ) = @_;

	my @local_history;
	$history = \@local_history unless ( defined $history );
	my $ftp;

	eval {
		my $domainname  = ECMDBOpcos::db_getDomainName( $opcoid );
		my $sku         = ECMDBJob::getAgencyRef( $jobid );
		my $batchserver = ECMDBImageworkflow::db_getBatchserver( $opcoid );
		my $batchuser   = ECMDBImageworkflow::db_getBatchuser( $opcoid );
		my $batchpass   = ECMDBImageworkflow::db_getBatchpassword( $opcoid );

		# XMP metadata merge 
		my $exifTool = "/usr/local/bin/exiftool";
		my $xmppath = ECMDBImageworkflow::db_getXMPPath( $opcoid );

		push( @$history, "Running exiftool" );
		system("$exifTool $upload_dir$myfile -tagsfromfile $xmppath$sku.xmp");
 
		#create CMD folder structure
		push( @$history, "Creating CMD structure" );
		my $repoUrlOther = Config::getConfig("repourlother"); # repourl with placeholder for domain
		my $url = $repoUrlOther =~ s/\%domain\%/$domainname/g;  # use jrs_webservice but to potentially different domain...
		my $curlString = "curl -sS '$url?action=checkout&port=6123&ip=".$domainname."&jobid=".$jobid."'";
		my $folderCurl = `$curlString`;
  
		# Send file to batch box
		# move original file into CMD job structure		   
		# thumb creation
 
		push( @$history, "FTPing file to batch box" );
		$ftp = Net::FTP->new( $batchserver, Debug => 0 ) or die "Cannot connect to FTP server, $batchserver, $@";

		$ftp->login( $batchuser, $batchpass ) or die "Cannot login", $ftp->message;
		$ftp->binary();
		$ftp->put( "$upload_dir$myfile", "$myfile" ) or die "put failed, ", $ftp->message;
		$ftp->quit;
		$ftp = undef;

		# Clear assignee
		ECMDBImageworkflow::db_clearAssignee($opcoid, $jobid);
	};
	if ( $@ ) {
		my $error = $@;
		carp "fileUploadImageWorkflowJob, $error";
		$error =~ s/ at .*//gs;
		push( @$history, "Error in fileUploadImageWorkflowJob, $error" );
		$ftp->quit() if ( $ftp );
		die $error;
	}
}

1;
