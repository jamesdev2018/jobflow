#!/usr/local/bin/perl

package JFAuthFactory;

use strict;
use warnings;

use Carp qw /carp confess cluck/;
use Cwd;

use lib 'modules';

use JFAuthBase;
use JFAuthLocal;
use JFAuthAD;

# The caller in processLogin or whatever, gets a principle, password and authsys id
#
# SQL select auth_name, auth_class, auth_table from auth_systems where auth_id = ?
#
# my $class = JFAuthFactory::createInstance( $auth_id, $auth_name, $auth_class_name, $auth_table );
# my $details = $class->authenticate( $principle, $password );
# $details is undef if auth fails
#

sub createInstance {
	my ( $auth_id, $auth_name, $auth_class_name, $auth_table ) = @_;

	my $valid_args = 0;
	die "Missing auth_id argument" unless defined $auth_id;
	die "Missing auth_name argument" unless defined $auth_name;
	die "Missing auth_class_name argument" unless defined $auth_class_name;
	$valid_args = 1;

	my $class;

	eval {
		# Create class using auth_class
		$class = $auth_class_name->new( $auth_id, $auth_name, $auth_table );

		# Check that class is sub class of JFAuthBase
		die 'Class $auth_class_name is not sub class of JFAuthBase' unless $class->isa( 'JFAuthBase' ); 
	};
	if ( $@ ) {
		my $error = $@;
		$auth_table = '' unless defined $auth_table;
		$auth_name = 'undef' unless defined $auth_name;
		$auth_class_name = 'undef' unless defined $auth_class_name;
		$auth_table = 'undef' unless defined $auth_table;
		carp "JFAuthFactory::createInstance, authid: $auth_id, name: '$auth_name', class name: '$auth_class_name', table: '$auth_table'";
		if ( $error =~ /Can't locate/m ) {
			carp "Cwd is " . getcwd;
			carp "Dump of \@INC";
			foreach my $k ( @INC ) {
				carp " - '$k";
			}
		}
		die "JFAuthFactory: createInstance failed, error $error";
	}
	return $class;
}

1;

