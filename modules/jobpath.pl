#!/usr/local/bin/perl
package JobPath;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use Carp qw/carp/;

# *** Please do not require any other modules in this lightweight module ***

use lib 'modules';

use Validate;

require "config.pl";

our $assetrepository = "/gpfs/TIER2/TAG_REPOSITORY_JOB_ASSETS/";

# This is a light weight module, specifically to return the Job path in its various forms
# for use where a module needs the Job path but doesnt want to include jobs.pl, which takes several seconds to compile.

# getAssetRepositoryJobPath (copy of Jobs::getAssetRepositoryJobPath)

sub getAssetRepositoryJobPath {
	my $jobid	= $_[0];

	my $one		= substr ( $jobid, 0, 2 );
	my $two		= substr ( $jobid, 2, 2 );
	my $three	= substr ( $jobid, 4, 2 );

	return "$assetrepository$one/$two/$three/$jobid/";
} 

# db_jobPath (copy from databaseJobs.pl)

sub db_jobPath {

	my ($jobid) = @_;

	my $path;
	eval {
			Validate::job_number( $jobid );
	
			my ($rootpath, $letter, $agency, $client, $project, $headline, $publication, $workflow, $opcoid, $pathtype)
					 = $Config::dba->process_oneline_sql(
							"SELECT t.rootpath, t.letter, t.agency, t.client, t.project, t.headline,
									t.publication, t.workflow, t.opcoid, o.pathtype FROM TRACKER3 t
								INNER JOIN OPERATINGCOMPANY o ON o.opcoid=t.opcoid
								WHERE t.job_number=?", [ $jobid ]);
			
			## HACK FOR ZURICH								
			$rootpath = "/gpfs/STUDIOS/TAG_ZURICH_NAC/PRODUCTION/" if $opcoid eq "10722";

			die "Rootpath undefined" unless ( $rootpath ); # Possible issue with JOIN
	
			$path = $rootpath;
			if($pathtype == 1 && length($letter))	{ $path .= "$letter/$agency/$client/$project/$headline/"; }
			elsif($pathtype == 1)					{ $path .= "$agency/$client/$project/$headline/"; }
			elsif($pathtype == 3)					{ $path .= "$agency/$client/$project/$headline/$workflow/"; }
			
			$path .= $jobid . "_";
			$path .= $publication if length($publication) && (lc($publication) ne "null");
			};
	if ( $@ ) {
		$jobid = "" unless defined $jobid;
		carp "db_jobPath: job '$jobid', $@";
		$path = "";
	}	
	return $path;

}

# getJobPath - get the basic job path
# Returns the job path without a trailing slash)
# Inputs
# - job
# Returns
# - jobpath without trailing /
# - opcoid ( home/origin opcoid )
# - customerid
# - location ( remote opcoid )
# - remoteCustomerId (customerid for job at location)
# Throws exceptions if invalid job, customerid passed

sub getJobPath {
	my ( $job ) = @_;

	my $path = '';
	my $opcoid = 0;
	my $customerId = 0;			# Customerid from tracker3plus.customerid
	my $remoteCustomerId = 0;	# Customerid corresponding to location rather than opcoid
	my $customer_name = '';
	my $useassetrepository = 0;

	my $location = 0;

	eval {

		Validate::job_number( $job );

		( $opcoid, $customerId, $location ) = $Config::dba->process_oneline_sql("SELECT opcoid, customerid, location FROM TRACKER3PLUS WHERE job_number=?", [ $job ]);
		die "Invalid job number $job" unless ( defined $customerId );
		die "Job $job has customerid 0" if ( $customerId == 0 );

		# my $useassetrepository = ECMDBOpcos::db_useassetrepositorycustomer( $customerId ); # another simple select ... and combine with customer name lookup
		( $customer_name, $useassetrepository ) = $Config::dba->process_oneline_sql( 
				"SELECT customer, useassetrepository FROM customers WHERE id = ?", [ $customerId ] );
		die "assetrepository is NULL for customer $customerId" unless defined $useassetrepository;
		die "Invalid customer name for customerid  $customerId" unless $customerId; # defined and <> 0

		if ( $useassetrepository == 1 ) {
			$path = getAssetRepositoryJobPath( $job );
		} else {
			$path = db_jobPath( $job, 0 );
		}
		$path =~ s/\/$//g; # Remove trailing slash (inconsistency between the 2 calls above)

		# There are a few rows in tracker3plus with location = NULL but only 1 in 2016, and some in 2013
		# There are circa 130 with customerid = 0 and opcoid = 0 (so will be trapped by the SQL query for useassetrepository above)

		# If the location is defined but one of CHECK-IN QUEUE (13), CHECK-OUT QUEUE (14), IN TRANSIT (15), REPOSITORY (16)
		# then treat as if at home location, so remote users cant see the job.

		$remoteCustomerId = $customerId;
		if ( (defined $location) && ( $opcoid != $location ) && ( $location !~ /^(13|14|15|16|17)$/ ) )  { 
			# Hopefully this is a more efficient version of ECMDBOpcos::getCustomerId2 but without doing a query that scans both tracker3 and tracker3plus.
			# In a very small number of cases, tracker3.agency does differ from customers.customer (3 jobs in 2016)
			# Steve Lawley pointed out that if the customer name differs across different opcos, you can never check the job out
			# so the customer name in customers is King.
			( $remoteCustomerId ) = $Config::dba->process_oneline_sql( "SELECT id FROM customers WHERE customer = ? AND opcoid = ?", [ $customer_name, $location ] );  
			if ( ! defined $remoteCustomerId ) {
				# die "Invalid remote customerid for '$customer_name', location '$location'";
				my ( $location_name ) = $Config::dba->process_oneline_sql( "SELECT opconame FROM operatingcompany WHERE opcoid = ?", [ $location ] );
				die "Jobs location $location is invalid" if ( ! defined $location_name );
				die "Can't find customer id for customer name '$customer_name' @ '$location_name' ($location)";
			}
		}

	};
	if ( $@ ) {
		my $error = $@;
		$job = '' unless defined $job;
		$opcoid = '' unless defined $opcoid;
		$customerId = '' unless defined $customerId;
		carp "getJobPath: job '$job', opcoid $opcoid, cust $customerId, $error";
		$error =~ s/ at .*//gs;
		die "getJobPath: job '$job', $error";
	}
	return ( $path, $opcoid, $customerId, $location, $remoteCustomerId, $customer_name );
}

# getJobVersionPath - get the path to the versions folder for a job
# Arguments
# - job
# Returns
# - path, opcoid, customerid, location, remoteCustomerId

sub getJobVersionPath {
	my ( $job ) = @_;

	my ( $path, $opcoid, $customerid, $location, $remoteCustomerId ) = getJobPath( $job );
	$path = $path . '/JOB_ASSETS/VERSIONS/';

	return ( $path, $opcoid, $customerid, $location, $remoteCustomerId );
}

# getJobOutputFilePath - get the path to the versions folder for a job
# Arguments
# - job
# Returns
# - path, opcoid, customerid, location, remoteCustomerId

sub getJobOutputFilePath {
	my ( $job ) = @_;

	my ( $path, $opcoid, $customerid, $location, $remoteCustomerId ) = getJobPath( $job );
	$path = $path . '/OUTPUT_FILE/';

	return $path;
}

1;


