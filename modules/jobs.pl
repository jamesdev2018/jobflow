﻿#!/usr/local/bin/perl
package Jobs;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use File::Path;
use File::Copy;
use LWP::Simple;
use JSON qw( decode_json );
use Net::FTP::File;
use URI::Escape;
use URI::URL;
use Data::Dumper;
use Template;
use Data::UUID;
use XML::Simple;
use Data::Dumper;
use Date::Simple ('date', 'today');
use JSON;
use POSIX qw/strftime/;
use Time::Local;
use Encode qw(decode encode);
use IO::Compress::Zip qw(zip $ZipError);
use Cwd;
use POSIX qw( strftime );
use Image::Size;
use Carp qw/carp/;

# Home grown
use lib 'modules';

use XMLResponse qw ( :statuses );
use CTSConstants;
use Validate;
use JFElastic;

require "databaseJob.pl";
require "databaseOpcos.pl";
require "databaseCustomerUser.pl";
require "databaseUser.pl";
require "databaseImageworkflow.pl";
require 'databaseMessages.pl';
require 'databaseProducts.pl';
require 'databasedashboard.pl';
require 'databasegroups.pl';
require "audit.pl";
require "Mailer.pl";
require "config.pl";
require "general.pl";
require "sendjobs.pl";
require "Checklist.pl";
require "jf_wsapi.pl"; # JF_WSAPI package
require "Watchlist.pl";
require "graphcalc.pl"; # graphCalc for new dashboard
require "daemonpanel.pl"; # getDaemons, updateDaemonOn etc
require "digitalassets.pl";
require "showjob.pl";
require "streamhelper.pl"; # createKey
require "databaseSearch.pl"; # Search
require "jobpath.pl";
require "databaseKanbanboard.pl"; # To get brief comment in BoardView.

use constant OPCO_MULTIPLE_LOCATIONS => 17;
use constant DEFAULT_SMTP_GATEWAY => 'gateway.dhl.com';
use constant DEFAULT_FROM_EMAIL => 'jobflow@tagworldwide.com';
use constant DEFAULT_SUPPORT_EMAIL => 'kakram@tagworldwide.com';
use constant DEFAULT_SUPPORT_EMAIL2 => 'feroz.khan@tagworldwide.com';

##################################################################
# from config // Khalid has changed this comment again 16:24
##################################################################

#my $jobflowServer = Config::getConfig("jobflowserver");
#my $applicationurl = $jobflowServer . "/jobflow.pl";

my $zipcmd 		= "/usr/bin/zip";
my $unzipcmd	= "/usr/bin/unzip";

my $assetrepository	= "/gpfs/TIER2/TAG_REPOSITORY_JOB_ASSETS/";

my %preflightstatuses = (
	InProgress 		=> '1',
	Passed 			=> '2',
	Warning			=> '3',
	Failed			=> '4',
	None			=> '5',
	Dispatched		=> '6',
);

my %qcstatuses = (
	0	 		=> 'None',
	1 			=> 'QC Required',
	2			=> 'QC Approved',
	3			=> 'QC Rejected',
);

my %qcnotify = (
    0           => 'None',
    1           => 'QCREQUIRE',
    2           => 'QCAPPROVE',
    3           => 'QCREJECT',
);

my %productstatuses = (
    None		=> '0',
    Pending		=> '1',
    InProgress	=> '2',
    Paused		=> '3',
    Completed	=> '4',
);

##################################################################
# the template object 
##################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";

# Maybe this should be in General module

sub elapsedToHHMMSS {
	my ( $elapsed ) = @_;

	my $timehms = "00:00:00";
	if ( defined $elapsed ) {
		my $hh =  int( $elapsed / ( 60*60 ) );
		my $mm = int( ( $elapsed / 60) % 60 );
		my $ss = int( $elapsed % 60 );
		$timehms = sprintf( "%02d:%02d:%02d", $hh, $mm, $ss );
	}
	return $timehms;
}

##############################################################################
#       Dashboard
#
##############################################################################
sub Dashboard {
	my ( $query, $session ) = @_;
	my $params = $query->Vars;
	warn "Jobs::Dashboard: " . Dumper( $params ) . "\n";
	die "Jobs::Dashboard no longer valid\n"; # Trace any calls to this 
}

# removed GetPersistentFilterLists as not referenced anywhere

# moved Applypopup to broadcast.pl

# getPubEmailFromPubName, getPubEmailFromJobid moved to modules/sendjobs.pl

# Showjob, Fileupload, updateJobStatuses moved to showjob.pl

# sendStatusUpdate, Approve moved to modules/sendjobs.pl

# getTwistServer moved to twist.pl

##############################################################################
#       getAssetRepositoryJobPath
#
##############################################################################
sub getAssetRepositoryJobPath {
	my $jobid	= $_[0];
	my $path;

	my $one		= substr ( $jobid, 0, 2);
	my $two		= substr ( $jobid, 2, 2);
	my $three	= substr ( $jobid, 4, 2);

	$path	= "$assetrepository$one/$two/$three/$jobid/";
	return $path

} # end getAssetRepositoryJobPath

# ViewLowRes, getLowResZip, GetHiRes, email_user, chooseDestination, SelectVendor -- moved to modules/sendjobs.pl

##############################################################################
#   ViewActivity()
#	Who is doing what?
#
##############################################################################
sub ViewActivity {
	my $query 		= $_[0];
	my $session 		= $_[1];
	my $sidemenu   = $query->param( "sidemenu" );
	my $sidesubmenu= $query->param( "sidesubmenu" );
	my $roleid     = $session->param( 'roleid' );

	my @activity 		= getActivity();

	my $vars = {
		activity	=> \@activity,
		sidemenu    => $sidemenu,
		sidesubmenu => $sidesubmenu,	
		roleid      =>$roleid,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/activity.html', $vars )
			|| die $tt->error(), "\n";
	
}

##############################################################################
#   ChangeLocation()
#	Allows changing of opcoid location - related to 'checkout' button
#
##############################################################################
sub ChangeLocation {
	my $query 		= $_[0];
	my $session 		= $_[1];
	my @jobs		= $query->param( "jobs" );
	my @cancheckout;	#array of jobs that 'can' be checked out 
	my @cantcheckout;	#array of jobs that 'cant' be checked out 
	my $checkedoutstatus;
	my $opcoid 		= $session->param( 'opcoid' );
	my $oktogo		= 1;
	my $message		= "";
	my $customerid		= 0;
	my $isJobRepository	= 0;
	
	
	my @locations	= ECMDBOpcos::getRemoteOperatingCompanies( $opcoid );

	for my $job ( @jobs ) {
		$checkedoutstatus 	= ECMDBJob::getCheckedOutStatus( $job );
		$customerid		= ECMDBJob::db_getCustomerIdJob( $job );
		
		if ( $checkedoutstatus == 0 || $checkedoutstatus == 4 ) { # its checked in
			push ( @cancheckout, $job );
		} else { #checkedout (2), checkingout (1), checkingin (3)
			push ( @cantcheckout, $job );
		}

		#if job is assigned - can't checkout
		if ( ECMDBJob::isJobAssigned( $job ) ) {
			push ( @cantcheckout, $job );
			$oktogo = 0;
			$message = "Assigned/Worked on jobs cannot be checked out";
		}

		# check if this jobs customer uses the Repository
		$isJobRepository	= ECMDBOpcos::customerJobRepositoryEnabled( $customerid );
		if ( $isJobRepository == 1 ) {
			$oktogo = 0;
			$message = "The customer for this job uses the Repository - cannot Checkout using this method";
			push ( @cantcheckout, $job );
		}
	}

	my $vars = {
		jobs			=> \@jobs,
		locations		=> \@locations,
		cancheckout		=> \@cancheckout,
		cantcheckout		=> \@cantcheckout,
		oktogo			=> $oktogo,
		message			=> $message, 
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/changeLocation.html', $vars )
			|| die $tt->error(), "\n";
}


##############################################################################
#   SetLocation()
#	change the location of the selected Jobs
#
##############################################################################
sub SetLocation {
	my $query 			= $_[0];
	my $session 			= $_[1];
	my $checkedoutstatus;
	my $errorMessage		= "";
	my $errors			= 0;

	my $str		= $query->param( 'jobs' );
	my @array	= split(",", $str);
	
	my $shard	= $query->param( 'shard'  ) || '';
	my $location 	= $query->param( 'opcoid' );

	foreach my $job ( @array ) {
		ECMDBJob::setLocation ( $job, $shard, $location );
		
		$checkedoutstatus = ECMDBJob::getCheckedOutStatus( $job, $shard );
		
		#only check out if status is 0 (never checked out) or 4 (checked in)
		if ( $checkedoutstatus == 0 || $checkedoutstatus == 4 ) {
			#change checkout status of each job to 'Sending' (from '')
			ECMDBJob::setCheckoutStatus ( $job, $shard, 1 ); # 1 is 'Checking Out'
			
			if (! ECMDBJob::isJobOnOutsourceQueue( $job, $shard ) ) {
				#put job on the outsource queue (job, remoteopcoid)
				ECMDBJob::enQueueOutsource ( $job, $shard, $location, $session->param( 'userid' ) );
				
				#record in the outsourcehistory that we have checked this job out JOB / USERID / OUTSOURCE OPCOID
				ECMDBAudit::logOutsourceHistory( $job, $session->param( 'userid' ), $location  );
				ECMDBJob::db_recordJobAction($job, $session->param('userid'), CTSConstants::OUTSOURCE, $location);
			}
		} else {
			$errorMessage .= "Job $job has already been checked out<br/>";
			$errors = $errors + 1;
		}
	}

	my $vars = {
		jobs		=> $str,
		errorMessage	=> $errorMessage,
		errors		=> $errors,
	};

	General::setHeaderVars( $vars, $session );
	
	$tt->process( 'ecm_html/setLocation.html', $vars )
			|| die $tt->error(), "\n";
	
}


##############################################################################
#   CheckIn()
#	Allow user to check jobs back in from remote
#
##############################################################################
sub CheckIn {
	my $query 	= $_[0];
	my $session 	= $_[1];
	my @cancheckin;		#array of jobs that 'can' be checked in 
	my @cantcheckin;	#array of jobs that 'cant' be checked in 
	my $checkedoutstatus;
	my $oktogo	= 1;
	my $message	= "";
	my $customerid		= 0;
	my $isJobRepository	= 0;

	my @jobs		= $query->param( "jobs" );


	for my $job ( @jobs ) {
		$checkedoutstatus 	= ECMDBJob::getCheckedOutStatus( $job );
		$customerid		= ECMDBJob::db_getCustomerIdJob( $job );
		$checkedoutstatus = ECMDBJob::getCheckedOutStatus( $job );
		
		if ( $checkedoutstatus == 2 ) { # its checked out (2)
			push ( @cancheckin, $job );
		} else { #any other status - cannot checkin
			push ( @cantcheckin, $job );
		}
		
		#if job is assigned - can't checkout
		if ( ECMDBJob::isJobAssigned( $job ) ) {
			$oktogo = 0;
			$message = "Assigned/Worked on jobs cannot be checked in";
		}
		
		# check if this jobs customer uses the Repository
		$isJobRepository	= ECMDBOpcos::customerJobRepositoryEnabled( $customerid );
		if ( $isJobRepository == 1 ) {
			$oktogo = 0;
			$message = "The customer for this job uses the Repository - cannot Checkin using this method";
			push ( @cantcheckin, $job );
		}
		
	}

	my $vars = {
		jobs		=> \@jobs,
		cancheckin	=> \@cancheckin,
		cantcheckin	=> \@cantcheckin,
		oktogo		=> $oktogo,
		message		=> $message,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/checkIn.html', $vars )
			|| die $tt->error(), "\n";
}


##############################################################################
#   processCheckIn()
#	Do the actual check in - via a CGI call to remote.
#
##############################################################################
sub processCheckIn {
	my $query 	= $_[0];
	my $session 	= $_[1];
	my $fromopcoid;	# remember for the checkin FROM is remote
	my $toopcoid;   # remember for the checkin TO is local
	my $domainname;
	my $ttpfolder = "";

	my @jobs		= split( ',', $query->param( "jobs" ) );

	$domainname = "Tag61TTP002.tag.internal"; 	#dehli live

	foreach my $job ( @jobs ) {
		
		# get the toopcoid;
		$toopcoid = ECMDBJob::db_getJobOpcoid ( $job );
		$fromopcoid = ECMDBJob::db_getJobRemoteOpcoid ( $job );
		print "fromopcoid: $fromopcoid<br>";
		print "Requesting a checkin in from $fromopcoid to $toopcoid<br>";

		#ka todo - delete the stuff below
		if ( $toopcoid == 10565 ) { # checking back into Diageo HQ
			$domainname = "172.27.242.101";
		}
		if ( $toopcoid == 11111 && $fromopcoid == 8590 ) { # checking back into London Hub (DEV) 
			$domainname = "5.0.2.222";
		}
		if ( $toopcoid == 11112 && $fromopcoid == 8590 ) { # checking back into London WL TEST (DEV) 
			$domainname = "5.0.2.222";
		}

		$domainname = ECMDBOpcos::db_getDomainName( $fromopcoid );
		requestJobCheckIn( $domainname, $job );
	}

	my $vars = {
		jobs		=> \@jobs,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/checkInComplete.html', $vars )
			|| die $tt->error(), "\n";
}


##############################################################################
# requestJobCheckIn()
##############################################################################
sub requestJobCheckIn {
	my $domainname	= $_[0];
	my $jobid	= $_[1];

	#  job in - CGI call

	my ( $var, $curlCommand ) = JF_WSAPI::jobflowCheckinOther( $domainname, { jobid => $jobid } );

	print "requestJobCheckIn curl: $curlCommand<br>, response '$var'<br>\n";

}

##############################################################################
# requestTTPUpload()
##############################################################################
sub requestTTPUpload {
	my $domainname	= $_[0];
	my $jobid	= $_[1];

	eval {
		my ( $var, $curlCommand ) = JF_WSAPI::ttpUploadOther( $domainname, { jobid => $jobid } );

		Audit::auditLog( "-1", "TRACKER3PLUS", $jobid, "Job: $jobid was uploaded via TTP" );
		ECMDBJob::db_recordJobAction($jobid, "-1", CTSConstants::TTP );
	};
	if ( $@ ) {
		my $error = $@;
		Audit::auditLog( "-1", "TRACKER3PLUS", $jobid, "Job: $jobid failed upload via TTP, $error" );
	}
}



##############################################################################
#   RepoCheckIn()
#	Allow user to check jobs into the repository
#
##############################################################################
sub RepoCheckIn {
	my $query 	= $_[0];
	my $session 	= $_[1];
	my @cancheckin;		#array of jobs that 'can' be checked in 
	my @cantcheckin;	#array of jobs that 'cant' be checked in 
	my $checkedoutstatus;
	my $oktogo	= 1;
	my $message	= "";
	my $customerid	= 0;
	my $joblocation	= 0;

	my @jobs	= ();
	my $menuation      = $query->param( "menuation" );

	if( $menuation eq "Yes" )
	{
		@jobs = split( ',', $query->param( "jobslist" ) );
	}
	else
	{
		@jobs	= $query->param( "jobs" );
	}

	for my $job ( @jobs ) {
		$customerid	= ECMDBJob::db_getCustomerIdJob( $job );
		$joblocation	= ECMDBJob::db_getJobRemoteOpcoid( $job );

		if ( ECMDBOpcos::customerJobRepositoryEnabled( $customerid ) && ($joblocation != 15) && ($joblocation != 16)) { # its checked out (2)
			push ( @cancheckin, { job_number => $job, shards => [ ECMDBJob::db_getJobShards($job) ] } );
		} else { #any other status - cannot checkin
			$oktogo = 0;
			$message = "The customer is not repo enabled for job: $job. ";
			if ( $joblocation == 15 ) {
				$message .= "The job $job is already in TRANSIT";
			}
			if ( $joblocation == 16 ) {
				$message .= "The job $job is already checked in";
			}
			push ( @cantcheckin, $job );
		}
	}

	my $vars = {
		jobs		=> \@jobs,
		cancheckin	=> \@cancheckin,
		cantcheckin	=> \@cantcheckin,
		oktogo		=> $oktogo,
		message		=> $message,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/repoCheckIn.html', $vars )
			|| die $tt->error(), "\n";
}

sub RestoreJobs {
	my $query 	= $_[0];
	my $session 	= $_[1];

	my $message	= "The following jobs will be restored shortly:<br/><br/>";
	my $warning	= "The following jobs are not archived and cannot be restored:<br/><br/>";

	my $success	= 0;
	my $failure	= 0;
	my @jobs;

	my $userid	 = $session->param( 'userid' );
	my $menuation    = $query->param( "menuation" );
	my $email        = ECMDBUser::db_getEmail ( $userid );
	
	if ( $menuation eq "Yes" ) {
		@jobs = split( ',', $query->param( "jobslist" ) );
	} else {
		@jobs   = $query->param( "jobs" );
	}

	foreach my $id ( @jobs ) {

		if ( ECMDBJob::isJobArchived( $id ) ) {
			$message = $message.$id."<br/>";
			eval {
				my ( $var, $curlCommand ) = JF_WSAPI::archiveJob( { action => 'restore', job => $id, uemail => $email } );
				$success = 1;
			};
			if ( $@ ) {
				$warning = $warning.$id."<br/>";
			}
		} else {
			$warning = $warning.$id."<br/>";
			$failure = 1;
		}
	}

	my $vars = {
		jobs            => \@jobs,
		message	 	=> $message,
		warning	 	=> $warning,
		success		=> $success,
		failure		=> $failure,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/RestoreJobs.html', $vars )
			|| die $tt->error(), "\n";

}



##############################################################################
#   RepoCheckOut()
#	Allow user to check jobs OUT of the repository
#
##############################################################################
sub RepoCheckOut {
	my $query 	= $_[0];
	my $session 	= $_[1];
	my @cancheckout;	#array of jobs that 'can' be checked out 
	my @cantcheckout;	#array of jobs that 'cant' be checked out 
	my $checkedoutstatus;
	my $opcoid 		= $session->param( 'opcoid' );
	my $oktogo	= 1;
	my $message	= "";
	my $customerid	= 0;
	my $joblocation	= 0;
	my @jobs	= ();
	my $menuation      = $query->param( "menuation" );

	if( $menuation eq "Yes" )
	{
		@jobs = split( ',', $query->param( "jobslist" ) );
	}
	else
	{
		@jobs	= $query->param( "jobs" );
	}

	# list of opcos to which we can send a job
	my @locations	= ECMDBOpcos::getRepoDestinations( $opcoid );

	for my $job ( @jobs ) {
		$customerid	= ECMDBJob::db_getCustomerIdJob( $job );
		$joblocation	= ECMDBJob::db_getJobRemoteOpcoid( $job );

		if ( ECMDBOpcos::customerJobRepositoryEnabled( $customerid ) && ($joblocation != 15) ) { # its checked out (2)
			push ( @cancheckout, { job_number => $job, shards => [ ECMDBJob::db_getJobShards($job) ] } );
		} else { #any other status - cannot checkin
			$oktogo = 0;
			$message = "The customer is not repo enabled for job: $job. ";
			if ( $joblocation == 15 ) {
				$message .= "The job $job is already in TRANSIT";
			}
			push ( @cantcheckout, $job );
		}
	}

	my $vars = {
		jobs		=> \@jobs,
		locations	=> \@locations,
		cancheckout	=> \@cancheckout,
		cantcheckout	=> \@cantcheckout,
		oktogo		=> $oktogo,
		message		=> $message,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/repoCheckOut.html', $vars )
			|| die $tt->error(), "\n";
}

##############################################################################
#   RepoSendJobs()
#	Allow an operartor to send jobs from the repository OR a Studio - without first 
# 	checking a Job into the Repo - as they are generally too thick to understand the two
#	step process ... 
#
##############################################################################
sub RepoSendJobs {
	my $query 	= $_[0];
	my $session 	= $_[1];
	my @cancheckout;	#array of jobs that 'can' be checked out 
	my @cantcheckout;	#array of jobs that 'cant' be checked out 
	my $checkedoutstatus;
	my $customerid	= 0;
	my $joblocation	= 0;
	my $opcoid 	= $session->param( 'opcoid' );
	my $oktogo	= 1;
	my $message	= "";
	my @jobs	= ();
	my $menuation      = $query->param( "menuation" );

	

	if( $menuation eq "Yes" )
	{
		@jobs = split( ',', $query->param( "jobslist" ) );
	}
	else
	{
		@jobs	= $query->param( "jobs" );
	}
	# list of opcos to which we can send a job
	my @locations	= ECMDBOpcos::getRepoDestinations( $opcoid );

	for my $job ( @jobs ) {
		
		$customerid	= ECMDBJob::db_getCustomerIdJob( $job );
		$joblocation	= ECMDBJob::db_getJobRemoteOpcoid( $job );
		my @shards = ECMDBJob::db_getJobShards($job);

		if ( ECMDBJob::isJobArchived( $job ) ) {
			$oktogo = 0;
			$message = "The job '$job' is ARCHIVED - can't Send. ";
			push(@cantcheckout, $job);
		}	
		elsif ( ECMDBJob::getJobQcMonitorStatus( $job ) eq "1" ) {
            $oktogo = 0;
            $message = "The job '$job' is in QC and cannot be sent.";
            push(@cantcheckout, $job);
        }
		elsif ( ECMDBJob::getCheckedOutStatus ( $job ) eq "0" ) {
			$oktogo = 0;
			$message = "The job '$job' predates the repository system. To check it out, please reprint the job ticket.";
			push(@cantcheckout, $job);
		}
		elsif ( !ECMDBOpcos::customerJobRepositoryEnabled( $customerid ) )
		{
			$oktogo = 0;
			$message = "The job '$job' belongs to a customer that is not enabled to use the job repository.";
			push(@cantcheckout, $job);
		}
		elsif ( ECMDBJob::isJobOnRepoSendJobsQueue ( $job ) ) {
			$oktogo 	= 0;
			$message 	= "The job: $job is already on the 'Send Jobs' queue. ";
			push ( @cantcheckout, $job );
		}
		elsif ( ECMDBJob::getAssigneeId( $job ) > 0 ) { # job is assigned
			$oktogo 	= 0;
			$message 	= "The job: $job is assigned to someone - can't Send.";
			push ( @cantcheckout, $job );
		}
		elsif ( ECMDBJob::isJobAssigned( $job ) || ECMDBJob::isJobBeingWorkedOn( $job ) ) { # job is assigned
			$oktogo 	= 0;
			$message 	= "The job: $job is assigned and being worked upon- can't Send.";
			push ( @cantcheckout, $job );
		}
		elsif ( ECMDBJob::inProgress( $job )  ) { # is job in 'Progress'? 
			$oktogo 	= 0;
			$message 	= "The job: $job is currently 'In Preflight Progress' - can't Send.";
			push ( @cantcheckout, $job );
		}
		elsif ( ECMDBOpcos::customerJobRepositoryEnabled( $customerid ) && ( $joblocation != 15 ) ) { # its checked out (2)
			push ( @cancheckout, { job_number => $job, shards => [ ECMDBJob::db_getJobShards($job) ] } );
		}
		elsif ( scalar ( @shards ) > 0 ) {
        	foreach my $shard (@shards) {
				my $curr_location = ECMDBJob::db_getLocation($job, $shard);
            	if( $curr_location && ( ( $curr_location <= 16 ) || ( $curr_location == $opcoid ) ) ) {
					$message    .= "Not sending $job/$shard, as the destination ($opcoid) is the same as the current location ($curr_location)\n";
					push ( @cantcheckout, $job );
            	}
        	}
		} else { #any other status - cannot checkin
			$oktogo 	= 0;
			$message 	= "The customer is not repo enabled for job: $job. ";
			if ( $joblocation == 15 ) {
				$message .= "The job $job is already in TRANSIT";
			}
			push ( @cantcheckout, $job );
		}
	}

	my $cantcheckout_jobs		= join( ", ", @cantcheckout );
	my $vars = {
		jobs		=> \@jobs,
		locations	=> \@locations,
		cancheckout	=> \@cancheckout,
		cantcheckout_jobs	=> $cantcheckout_jobs,
		oktogo		=> $oktogo,
		message		=> $message,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/repoSendJobs.html', $vars )
			|| die $tt->error(), "\n";
}

##############################################################################
#   processRepoCheckIn()
#	Add the jobs to the database to be processed for checkin by batch.
#
##############################################################################
sub processRepoCheckIn {
	my $query 	= $_[0];
	my $session 	= $_[1];

	my $opcoid 	= $session->param( 'opcoid' );
	my $userid 	= $session->param( 'userid' );
	my $location	= 0;

	my @jobs	= split( ',', $query->param( "jobs" ) );
	my @paramnames = $query->param;


	foreach my $job ( @jobs ) {
		## Get a list of the job shards that we're going to send (if applicable)
		my @shards;
		foreach my $parameter (@paramnames) { push(@shards, $1) if $parameter =~ m/^$job-(.*)$/; }
		## If no shards are selected, send them all (assuming the job is even split)
		@shards = ECMDBJob::db_getJobShards($job) if !@shards;
		@shards = ('') if !@shards;
		
		## Queue the shards, or else just queue the actual job
		foreach my $shard (@shards)
		{
			my $location = ECMDBJob::db_getLocation($job, $shard);
			ECMDBJob::enQueueRepoCheckIn($job, $shard, $location, $userid);
			
			## Audit the send for logs and reporting
			Audit::auditLog( $userid, "TRACKER3PLUS", $job, "Job: $job/$shard has been queued for checkin by $userid");
		}
	}

	my $vars = {
		jobs	=> \@jobs,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/repoCheckInRequested.html', $vars )
			|| die $tt->error(), "\n";
}


##############################################################################
#   processRepoCheckOut()
#	Add the jobs to the database to be processed for CHECKOUT by batch.
#
##############################################################################
sub processRepoCheckOut {
	my $query 	= $_[0];
	my $session 	= $_[1];
	
	my $userid 		= $session->param( 'userid' );
	my @jobs		= split( ',', $query->param( "jobs" ) );
	my $destinationOpcoid	= $query->param( "opcoid" ) || 0;
	my @paramnames = $query->param;


	foreach my $job ( @jobs ) {
		## Get a list of the job shards that we're going to send (if applicable)
		my @shards;
		foreach my $parameter (@paramnames) { push(@shards, $1) if $parameter =~ m/^$job-(.*)$/; }
		
		## If no shards are selected, send them all (assuming the job is even split)
		@shards = ECMDBJob::db_getJobShards($job) if !@shards;
		@shards = ('') if !@shards;
		
		## Queue the shards, or else just queue the actual job
		foreach my $shard (@shards)
		{
			ECMDBJob::enQueueRepoCheckOut($job, $shard, $destinationOpcoid, $userid);
			
			## Audit the send for logs and reporting
			Audit::auditLog($session->param('userid'), "TRACKER3PLUS", $job, "Job: $job/$shard has been queued for checkOUT by $userid");
		}
	}


	my $vars = {
		jobs		=> \@jobs,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/repoCheckOutRequested.html', $vars )
			|| die $tt->error(), "\n";
}


##############################################################################
#   processRepoSendJobs()
#	Add the jobs to the database to be processed for CHECKIN followed 
#	by CHECKOUT by batch.
#	Essentially we make the Checkin and then Checkout as an atomic operation 
#	as far as the user is concerned.
#
##############################################################################
sub processRepoSendJobs {
	my $query 	= $_[0];
	my $session 	= $_[1];

	my $userid 		= $session->param( 'userid' );
	my @jobs                = split( ',', $query->param( "jobs" ) );
	my $destinationOpcoid	= $query->param( "opcoid" );
	
	my @paramnames = $query->param;
	my $message; 

	foreach my $job (@jobs)
	{
		## Get a list of the job shards that we're going to send (if applicable)
		my @shards;
		foreach my $parameter (@paramnames) { push(@shards, $1) if $parameter =~ m/^$job-(.*)$/; }
		## If no shards are selected, send them all (assuming the job is even split)
		@shards = ECMDBJob::db_getJobShards($job) if !@shards;
		@shards = ('') if !@shards;
		
		
		## Queue the shards, or else just queue the actual job
		my $jobsent = 0;
		foreach my $shard (@shards)
		{
			my $location = ECMDBJob::db_getLocation($job, $shard);
			if($location && (($location <= 16) || ($location == $destinationOpcoid)))
			{
				$message	= "--> Not sending $job/$shard, as the destination ($destinationOpcoid) is the same as the current location ($location)!<br>";
			}
			else
			{
				ECMDBJob::enQueueRepoSendJob($job, $shard, $location, $destinationOpcoid, $userid);
			
				## Audit the send for logs and reporting
				Audit::auditLog($session->param('userid'), "TRACKER3PLUS", $job, "Job: $job/$shard has been queued for Sending to $destinationOpcoid by $userid");
				$jobsent = 1;
			}
		}
		
		## Only record the action per job, not per fragment
		ECMDBJob::db_recordJobAction($job, $session->param('userid'), CTSConstants::OUTSOURCE, $destinationOpcoid) if $jobsent;
		ECMDBGroups::db_deptstatusupdate("0", $job);##After Send Jobs to studio the department has to be default to None	
		ECMDBGroups::db_teamstatusupdate("0", $job);##After Send Jobs to studio the team has to be default to None	
	}

	my $joblist		= join( ', ', @jobs );
	my $vars = {
		message					=> $message,
		requested_jobslists		=> $joblist,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/repoSendJobsRequested.html', $vars ) || die $tt->error(), "\n";
}

# listDeamons moved to daemonpanel.pl

# moved AlterEstimate, processNewEstimate to estimate.pl

##############################################################################
#   UnlockJob()
#	change the lock status of the selected Jobs
#
##############################################################################
sub UnlockJobs {
	my $query	= $_[0];
	my $session 	= $_[1];

	#my @jobs		= $query->param( "jobs" );
	my $menuation      = $query->param( "menuation" );

	my @jobs;
	if( $menuation eq "Yes" ) {
		@jobs = split( ',', $query->param( "jobslist" ) );
	} else {
		@jobs	= $query->param( "jobs" );
	}
	
	foreach my $job ( @jobs ) {
		ECMDBJob::unlockJob ( $job );

		#audit this		
		Audit::auditLog( $session->param( 'userid' ), "TRACKER3PLUS", $job, "Job: $job has been unlocked manually" );
	}

	# dashboard is displayed by postaction in jobflow.pl
}

# moved AssignProduct, UnassignProduct, StartProduct, AddRevisionReason, GetProductRestartReasons, finishProductAssignToDept, PauseProduct, StopProduct, ViewProductHistory to products.pl

##############################################################################
#   AssignJobs()
#	Assign these jobs to an operator
#	The operators are retrieved from the CUSTOMER attached to the LOCATION.
#	These MUST BE SET UP!!!
#
##############################################################################
sub AssignJobs {
	my ( $query, $session ) = @_;

	my $formPosted	= $query->param( "formPosted" ) ||'';
	my $menuation	= $query->param( "menuation" );
	my $mode	= $query->param( "mode" ) ||'';

	my @jobs;
	if ( $formPosted eq "yes" ) {
		@jobs	= split( ',', $query->param( "jobs" ));
	} elsif( $menuation eq "Yes" ) {
		@jobs = split( ',', $query->param( "jobslist" ) );
	} else {
		@jobs	= $query->param( "jobs" );
	}
	my $myopcoid 		= $session->param( 'opcoid' );
	my $userid 			= $session->param( 'userid' );
	my $username		= $session->param( 'username' );

	my @history;
	my @users;		# Get list of users [ { userid, username, fname, lname, role } ] who can see this specific customerid

	my $assigneeid      = 0;
	my $assigneename	= "";
	my  $lineitemid 	= "";

	my $count = 0;
	my $firstOpcoid = 0; 
	my $firstCustomerid = 0;
	my $firstRemoteCustomerId = 0;
	my $vars;

	eval {
		if ( $formPosted ne "yes" ) {

			push( @history, "Checking the jobs" );

			die "Mo jobs specified" unless (scalar @jobs);
			for my $jobid ( @jobs ) {
				$count++;
				eval {
					my ( $path, $opcoid, $customerId, $location, $remoteCustomerId, $customer_name ) = JobPath::getJobPath( $jobid );

					push( @history, "Job $jobid, opcoid $opcoid, customerid $customerId, location $location, remotecust $remoteCustomerId, name $customer_name" );

					# check No 1 - the opcoid of the person who is logged in MUST be the same as the opcoid of the first job
					die "You cannot assign jobs that are at a remote location unless you are logged in at that location." unless ( $location == $myopcoid );

					# check No 2 - all the jobs MUST be from the same remote Customer id
					# as the remote customer id is used to decide the list of users to assign to.
					if ( $count == 1 ) {
						$firstRemoteCustomerId = $remoteCustomerId;
					} elsif ( $firstRemoteCustomerId != $remoteCustomerId ) {
						die "Job has remote customer id $remoteCustomerId, differs from 1st job $firstRemoteCustomerId";
					}

					# check No 3 Can the user doing the assign see the remote customer
					push( @history, "Calling assert_customer_visible user $userid, custid $remoteCustomerId" );
					eval {
						StreamHelper::assert_customer_visible( $userid, "", $remoteCustomerId );
					};
					if ( $@ ) {
						warn $@;
						die "Customer $customer_name (remoteCustomerId) not visible for this user";
					}
					my ($level)	=	ECMDBProducts::checkJobLevel($jobid);	#if level is 2, the job is working by task level
					die "You can not assign a job that is using Tasks." if($level == 2);

					# check number 4 - are any of these jobs in state 'W'
					if ( ECMDBJob::isJobBeingWorkedOn( $jobid ) ) {
						die "When assigning jobs, the jobs must not be in state 'W' (being Worked on).";
					}

					# check number 5 - are any of these jobs in QC Approved or Rejected? If so - can't assign.
					if ( ECMDBJob::db_jobQCApproved( $jobid ) || ECMDBJob::db_jobQCRequired( $jobid ) ) {
						die "When assigning jobs, the jobs must not be in state QC Required or QC Approved";
					}

					# check number 6 - are any of these jobs in transit? Checking-out / Checking in? If so - can't assign
					if ( ECMDBJob::isJobInTransit( $jobid ) ) {
						die "When assigning jobs, the jobs must not be in Transit (Checking Out/Checking In)";
					}

					# check number 7 - products assigned to anyone
					my ( $isassigned, $productid, $productname, $productstatus ) = ECMDBJob::isJobAssignedToOthers( $jobid );
					# check Job is Paused or Job has multiple assigned products
	      		  	if ($isassigned > 1 || ((defined $productstatus) && ($productstatus==3)) ) { # $productstatus can be undefined
    	        		die "Products has been assigned to other users for this job $jobid";
        			}
				};
				if ( $@ ) {
					my $error = $@;
					warn $error;
					if ($error =~ /getJobPath/ ) {
						$error =~ s/getJobPath: job '$jobid', //gs;
					}
					$error =~ s/ at m.*//gs;
					die "Job: $jobid, Error: $error";
				}
			} # end for jobs

			# Get list of users [ { userid, username, fname, lname, role } ] who can see this specific customerid
			@users = ECMDBCustomerUser::getCustomerUsers( $firstRemoteCustomerId );

			$vars = {
				jobs			=> \@jobs,
				users			=> \@users,
				assigneeid		=> $assigneeid,
				formPosted		=> $formPosted,
				ajaxassignjob	=> 1,
				history			=> \@history,
			};

		} else {
			push( @history, "Doing the job assign" );
	

			my $assigneename = $query->param( "assigneeid" );
			die "Assignee name not specified" unless (( defined $assigneename ) && ( $assigneename ne '' ));
		    $assigneeid = ECMDBUser::db_getUserId( $assigneename );
			die "Assignee name '$assigneename' not recognised" unless $assigneeid > 0;

			my @assignJobLists;
			my @reassignJobLists;
			my $timetoadd		= 0;
			my $defaultHours	= 4;
			my $watchingUsrJobMapping_ref;
			my $reAssignWatchingUsrJobMapping_ref;
	
			# Assign jobs to selected user

			for my $jobid ( @jobs ) {

				my ( $path, $opcoid, $customerId, $location, $remoteCustomerId, $customer_name ) = JobPath::getJobPath( $jobid );

				$timetoadd = 0;
				push( @history, "Assigning $jobid to $assigneename, time to add: $timetoadd minutes" );

				# Check whether the job has single/multiple assigned product	
				my ( $isassigned, $productid, $productname, $productstatus ) = ECMDBJob::isJobAssignedToOthers( $jobid );

				my $ispublishing = ECMDBOpcos::isPublishing( $opcoid );
				# warn "ispublishing $ispublishing\n";
					
				my ($level)	=	ECMDBProducts::checkJobLevel($jobid);	#if level is 2, the job is working by task level
				die "You can not assign a job that is using Tasks." if($level == 2);

				# if this job is not already assiged to ANY user
				if ( ! ECMDBJob::isJobAssigned( $jobid ) ) {

					push( @history, "Job $jobid not assigned, assigning to $assigneename( $assigneeid )" );

					ECMDBJob::assignJob( $jobid, $assigneeid, $timetoadd, $userid, $ispublishing,  $lineitemid, $isassigned, $productid, $productstatus );

					# log assign
					Audit::auditLog( $userid, "TRACKER3", $jobid, "Job $jobid has been assigned to $assigneename by $username" );

					push ( @assignJobLists, $jobid );

					# check the job has single assigned product and status not in "Paused"	
					if( ( $isassigned == 1 ) && ( $productstatus != 3 ) ) {
						Audit::auditLog( $userid, "PRODUCTS", $jobid, "Product: $productname (Job: $jobid) has been assigned to user: $assigneename by $username" );
					}
				
				} else { # if it IS assigned to a user then REASSIGN it to this user (even if it is the same user) - ignore jobs that are being worked on

					if ( ! ECMDBJob::isJobBeingWorkedOn( $jobid ) ) {

						push( @history, "Job $jobid is assigned, but not being worked on, assigning to $assigneename( $assigneeid )" );

						ECMDBJob::reassignJob( $jobid, $assigneeid, $timetoadd, $userid, $ispublishing, $isassigned, $productid, $productstatus );
						push ( @reassignJobLists, $jobid );

						Audit::auditLog( $userid, "TRACKER3", $jobid, "Job $jobid has been re-assigned to $assigneename by $username" );

						# check the job has single assigned product and status not in "Paused"	
						if ( ($isassigned == 1) && ((defined $productstatus) && ($productstatus != 3)) ) {
							Audit::auditLog( $userid, "PRODUCTS", $jobid, "Product: $productname (Job: $jobid) has been assigned to user: $assigneename by $username" );
						}
					}
				}
			} # end for

			my $mailBody;

			# Calling Watchlistnotification function with list of 'Assign' job(s).
			if ( @assignJobLists ) {
				$watchingUsrJobMapping_ref = Watchlist::Watchlistnotification( 
						$userid, $myopcoid, "Job(s) has been assigned to $assigneename by $username", \@assignJobLists, "ASSIGNED" );
				$mailBody = $mailBody . "Job(s) " . join( ', ', @assignJobLists ) . " has been assigned ";
			}

			# Calling Watchlistnotification function with list of 're-Assign' job(s).
			if ( @reassignJobLists ) {
				$reAssignWatchingUsrJobMapping_ref = Watchlist::Watchlistnotification( 
						$userid, $myopcoid, "Job(s) has been re-assigned to $assigneename by $username", \@reassignJobLists, "ASSIGNED");
				if ( $mailBody ) {
					$mailBody	= $mailBody . " and Job(s) " . join( ', ', @reassignJobLists ) . " has been re-assigned ";
				} else {
					$mailBody	= $mailBody . "Job(s) " . join( ', ', @reassignJobLists ) . " has been re-assigned ";
				}
			}

			$mailBody = $mailBody . " to $assigneename by $username";
			my $touserid = ECMDBUser::db_getEmail($assigneeid);
			my $smtp_gateway = Config::getConfig("smtp_gateway") || DEFAULT_SMTP_GATEWAY;
			my $fromemail = Config::getConfig("fromemail") || DEFAULT_FROM_EMAIL;
			my $triggermail	= Mailer::send_mail ( $fromemail, $touserid, "$mailBody", "Jobflow assignment.", { host => $smtp_gateway } );

			# defaultHours is for all the jobs, whereas ispublishing depends on jobs opcoid (which may vary)
			#if ( $ispublishing == 1 ) {
			#	$defaultHours = 0;
			#}
	
			$vars = {
				jobs			=> \@jobs,
				users			=> \@users,
				assigneeid		=> $assigneeid,
				defaultHours	=> $defaultHours,
				formPosted		=> $formPosted,
				ajaxassignjob	=> 1,
				watchingUsrJobMapping => $watchingUsrJobMapping_ref,
				reAssignWatchingUsrJobMapping => $reAssignWatchingUsrJobMapping_ref,
				history			=> \@history,
			};

		} # end if formPosted

	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at m.*//gs;
		$vars = { error => $error, history => \@history };
	}

	if ( $mode eq "json" ){
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/assignJobs.html', $vars )
			|| die $tt->error(), "\n";
}

##############################################################################
#   RequestJob()
#	Operator Requests a Job
#
##############################################################################
sub RequestJob {
	my ( $query, $session ) = @_;

	my $mode                = $query->param( "mode" ) ||'';

	my $userid		= $session->param( 'userid' );
	my $location		= $session->param( 'opcoid' );
	my $username		= $session->param( 'username' ); 

	my $jobopcoid;		# the opcoid of the job (could be different from location ...)
	my $customerid		= 0;
	my $jobid		= 0;
	my $agencyref		= "";
	my $version		= 1;
	my $message		= "";
	my $timetoadd		= 4; #default is 4 hours
	my $maxjobsallowed	= 5;
	my $tmp;
	my $parentopcoid	= 0;
	my $ispublishing	= 0;
	my $reqJob		= 1;

	# FIRST - check to see if this operator is already working on a job
	$jobid = ECMDBJob::getCurrentOperatorJob( $userid );
	if ( $jobid > 0 ) {
		$agencyref = ECMDBJob::getAgencyRef( $jobid );
		$message = "You are already working on job: $jobid / $agencyref";
	} 

	#get the id of most recent job that has been assigned to this operator (if any)
	if ( $jobid == 0 ) {	#operator is not working on a job --> get one
		$jobid = ECMDBJob::getNextAssignedJob( $userid );
		if ( $jobid > 0 ) {
			# put this job in the jobworkqueue table
			$tmp = ECMDBJob::setCurrentOperatorJob( $userid, $jobid );
			# mark the job in assignmenthistory table
			# Archant notify
			Audit::auditLog( $userid, "TRACKER3", $jobid, "Assigned Job $jobid has been requested by $username: Work Started" );
			$jobopcoid = ECMDBJob::db_getJobOpcoid( $jobid );
			$customerid = ECMDBJob::db_getCustomerid( $jobid );

			##Added by Feroz - Extra log stashes
			Audit::auditLog( $userid, "TRACKER3", $jobid, "LOG START HERE" );
			Audit::auditLog( $userid, "TRACKER3", $jobid, "LOG1:jobopcoid $jobopcoid customerid $customerid" );

			if ( $jobopcoid == 11463 || $jobopcoid == 15555551 ) { # Archant
				notifyInProgress( $jobid );	# tell Archant job is now ib progress
				$agencyref = ECMDBJob::getAgencyRef( $jobid );
				$version = ECMDBJob::getVersion( $jobid );
			}

			#Image workflow
			if (ECMDBOpcos::db_isOpcoImageWorkFlow( $jobopcoid )){
			    $agencyref = ECMDBJob::getAgencyRef( $jobid );
			}

			# set the message text
			$message = "You have been given $jobid / $agencyref from the assigned list";
			Audit::auditLog( $userid, "TRACKER3", $jobid, 
				"LOG2:DB PASSED VALUES jobid $jobid  jobopcoid $jobopcoid customerid $customerid agencyref $agencyref version $version userid $userid" );

			##Added by Feroz : If all values are right just record in db. Else record in db along with email
			if ($jobid > 0 && $jobopcoid > 0 && $customerid > 0 && $agencyref > 0 && $version > 0 && $userid > 0)
			{
				Audit::auditLog( $userid, "TRACKER3", $jobid, "LOG3:recordStart Passed values are correct" );
			}
			elsif($jobopcoid == 11463 || $jobopcoid == 15555551) ##It has to be done only for archant
			{
				Audit::auditLog( $userid, "TRACKER3", $jobid, "LOG3:recordStart Passed values are incorrect" );
				my $body = "Value is missing for any one of the following ";
				$body .= "jobid $jobid  jobopcoid $jobopcoid customerid $customerid agencyref $agencyref version $version userid $userid";
				my $smtp_gateway = Config::getConfig("smtp_gateway") || DEFAULT_SMTP_GATEWAY;
				my $fromemail = Config::getConfig("fromemail") || DEFAULT_FROM_EMAIL;
				my $supportemail2 = Config::getConfig("supportemail2") || DEFAULT_SUPPORT_EMAIL2;
				Mailer::send_mail ( $fromemail, $supportemail2, $body, "Job Flow: recordStart Value is NULL for JOB-$jobid", { host => $smtp_gateway } );

				if ( ! $jobopcoid )
				{
					$jobopcoid = "NULL";	
				}

				if ( ! $customerid )
				{
					$customerid = "NULL";	
				}

				if ( ! $agencyref )
				{
					$agencyref = "NULL";	
				}

				if ( ! $version )
				{
					$version = "NULL";	
				}

				if ( ! $userid )
				{
					$userid = "NULL";	
				}
			}

			ECMDBJob::recordStart( $jobid, $jobopcoid, $customerid, $agencyref, $version, $userid );
			Audit::auditLog( $userid, "TRACKER3", $jobid, "LOG END HERE" );
		}
		else { # no assigned jobs - get one from the pool
			#print "No job found in assigned for user $userid - get one from pool<br>";
			#logic for getting a job from the pool ....
			#1 - LOCATION OF JOB MUST BE SAME AS THAT OF LOGGED IN OPERATOR.
			#2 - OPERATORSTATUS OF JOB MUST BE 'U' - UNASSIGNED
			#3 - Get the oldest job from eligible jobs (which are those NOT currently assigned).
			#4 - if QC is Enabled - exclude jobs that in QC state: Required, Approved
			#5 - the 'jobpoolassign' flag in the OPCO for the job must be set to '1'
			$jobid = ECMDBJob::getJobFromPool( $location );

			if ( $jobid > 0 ) {
				$parentopcoid 	= ECMDBJob::db_getJobOpcoid( $jobid );
				$ispublishing	= ECMDBOpcos::isPublishing( $parentopcoid );
				if ( $ispublishing == 1 ) { $timetoadd = 0; }
				# assign the job (as if the Studio Manager had done so) - then set the current operator job
				$tmp = ECMDBJob::assignJob( $jobid, $userid, $timetoadd, $userid, $ispublishing );
				$tmp = ECMDBJob::setCurrentOperatorJob( $userid, $jobid );
				Audit::auditLog( $userid, "TRACKER3", $jobid, "Pool Job $jobid has been requested by $username: Work Started" );
				$jobopcoid = ECMDBJob::db_getJobOpcoid( $jobid );
				$customerid = ECMDBJob::db_getCustomerid( $jobid );

				##Added by Feroz - Extra log stashes
				Audit::auditLog( $userid, "TRACKER3", $jobid, "LOG START HERE 2" );
				Audit::auditLog( $userid, "TRACKER3", $jobid, "LOG1:jobopcoid $jobopcoid customerid $customerid" );

				# Archant notify
				$jobopcoid = ECMDBJob::db_getJobOpcoid( $jobid );
				if ( $jobopcoid == 11463 || $jobopcoid == 15555551 ) { # Archant
					notifyInProgress( $jobid );	
					$agencyref = ECMDBJob::getAgencyRef( $jobid );
					$version = ECMDBJob::getVersion( $jobid );
				}
				# set the message text
				$message 	= "You have been given $jobid / $agencyref from the pool";
				Audit::auditLog( $userid, "TRACKER3", $jobid, "LOG2:DB PASSED VALUES jobid $jobid  jobopcoid $jobopcoid customerid $customerid agencyref $agencyref version $version userid $userid" );

				##Added by Feroz : If all values are right just record in db. Else record in db along with email
				if ($jobid > 0 && $jobopcoid > 0 && $customerid > 0 && $agencyref > 0 && $version > 0 && $userid > 0)
				{
					Audit::auditLog( $userid, "TRACKER3", $jobid, "LOG3:recordStart Passed values are correct" );
				}
				elsif($jobopcoid == 11463 || $jobopcoid == 15555551) ##It has to be done only for archant
				{
					Audit::auditLog( $userid, "TRACKER3", $jobid, "LOG3:recordStart Passed values are incorrect" );
					my $body = "Value is missing for any one of the following ";
					$body .= "jobid $jobid  jobopcoid $jobopcoid customerid $customerid agencyref $agencyref version $version userid $userid";
					my $smtp_gateway = Config::getConfig("smtp_gateway") || DEFAULT_SMTP_GATEWAY;
					my $fromemail = Config::getConfig("fromemail") || DEFAULT_FROM_EMAIL;
					my $supportemail2 = Config::getConfig("supportemail2") || DEFAULT_SUPPORT_EMAIL2;
					Mailer::send_mail ( $fromemail, $supportemail2, $body, "Job Flow: recordStart Value is NULL for JOB-$jobid", { host => $smtp_gateway } );
					if ( ! $jobopcoid )
					{
						$jobopcoid = "NULL";
					}

					if ( ! $customerid )
					{
						$customerid = "NULL";
					}

					if ( ! $agencyref )
					{
						$agencyref = "NULL";
					}

					if ( ! $version )
					{
						$version = "NULL";
					}

					if ( ! $userid )
					{
						$userid = "NULL";
					}

				  }


				ECMDBJob::recordStart( $jobid, $jobopcoid, $customerid, $agencyref, $version, $userid );
				Audit::auditLog( $userid, "TRACKER3", $jobid, "LOG END HERE 2" );
			} else {
				$message = "There is no job available at the moment, please speak to your supervisor.";
				Audit::auditLog( $userid, "USERS", undef, "User $username requested a job, but none was available" );
			}
		}		
	}

	my $postValue ="requestJob";
	#get the job details and present then to the operator
		
	my $vars = {
		message		=> $message,
		jobid		=> $jobid,
		postValue	=> $postValue,
		reqJob		=> $reqJob,
	};
	
	if ( $mode eq "json" ){
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/requestJob.html', $vars )
			|| die $tt->error(), "\n";
}

# CompareJobs() moved to modules/comparejobs.pl


##############################################################################
#  TakeJob 
# 	Operator wishes to suspend current Job and Take another one
##############################################################################
sub TakeJob {
	my ( $query, $session ) = @_;

	my $menuation		= $query->param( "menuation" ) || '';
        my $mode		= $query->param( "mode" ) ||'';

	my $userid		= $session->param( 'userid' );
	my $username            = $session->param( 'username' );

	my @jobs;
	if ( $menuation eq "Yes" ) {
		@jobs = split( ',', $query->param( "jobslist" )||'' );
	} else {
		@jobs   = $query->param( "jobs" );
	}
	my $jobid		= $jobs[0];
	my $vars;

	eval {
		Validate::userid( $userid );
		Validate::job_number( $jobid );

		# FIRST - check to see if this operator is already working on a job
		my $currentjob = ECMDBJob::getCurrentOperatorJob( $userid );
		die "Can't take a Job that you are already working on" if ( ( defined $currentjob ) && ( $currentjob == $jobid ) );

		# get the assignne of the selected job - if it is not same as this bloke - nogo
		my $assigneeid	= ECMDBJob::getAssigneeId( $jobid );
         die "You are not assigned this job: $jobid - hence you can't 'Take' it. Sorry."if ( ( ! defined $assigneeid ) || ( $assigneeid != $userid ) );

		#PD-3592 : Check the job is working by job level/task level
		my ($level) =   ECMDBProducts::checkJobLevel($jobid); #if level is 2, the job is working by task level
		die "You can not take a job that is using Tasks." if($level == 2);

		# if validation is good - we let user take the job 
        
		# get the agency ref of the new job
		my $jobopcoid	= ECMDBJob::db_getJobOpcoid( $jobid ); # the parent opcoid of the job
		my $agencyref 	= "";	
		my $version	= 1;
		my $customerid	= ECMDBJob::db_getCustomerIdJob( $jobid );
        
		# stop user working on current job - set it to 'A' FROM 'W'
		ECMDBJob::unWorkJob( $currentjob, $userid ); # every else about the job stays the same, just goes from W --> A

		if ( $jobopcoid == 11463 || $jobopcoid == 15555551 ) { # Archant
			$agencyref = ECMDBJob::getAgencyRef( $jobid );
			$version = ECMDBJob::getVersion( $jobid );
		}

		# stop the clock
		ECMDBJob::recordStop( $currentjob, $userid );

		# start user working on 'taken' job - set it to 'W' FROM 'A'
		ECMDBJob::setCurrentOperatorJob( $userid, $jobid ); # want it to go from A --> W

		# start clock
		ECMDBJob::recordStart( $jobid, $jobopcoid, $customerid, $agencyref, $version, $userid );
		Audit::auditLog( $userid, "TRACKER3PLUS", $jobid, "$userid has taken Job: $jobid and put on hold job: $currentjob" );

		$vars = {
			message		=> "You have taken Job: $jobid and put on hold job: $currentjob",
			jobid		=> $jobid,
			gogobaby	=> 1,
			takejob		=> 1,
		}	
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = {
			jobid           => $jobid,
			takejob         => 1,
			message		=> $error,
			gogobaby        => 0,
		}
	}

	if ( $mode eq "json" ){
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/takejob.html', $vars )
			|| die $tt->error(), "\n";
}

##############################################################################
#   notify In-Progress to Archant
#   used by RequestJob
##############################################################################
sub notifyInProgress {
	my $myjob = $_[0];

	# write confirm xml to archant - that we have received and are processing this job
	# get the agency ref - get the oba - needed to build filename
	
	my $urnnumber 	= ECMDBJob::getAgencyRef( $myjob );
	my $obanumber 	= ECMDBJob::getOBANumber( $myjob );
	my $filename;
	$filename = XMLResponse::write_status("/temp/archantnotify/$obanumber-$urnnumber-inprogress.xml", $urnnumber, $obanumber, XMLResponse::STATUS_IN_PROGRESS, "In Progress");
	print "filename: $filename<br>";
	# ftp the file over to archant

	#sendStatusUpdate( $filename ); ##This function is removed
    SendJobs::sendArchantStatusUpdate( $filename ); 
}

##############################################################################
#   rejectJob
#   operator rejects a job
#
##############################################################################
sub rejectJob {
	my $query 	= $_[0];
	my $session 	= $_[1];

	my $jobid		= $query->param("jobid");
	my $userid		= $session->param( 'userid' );
	my $username		= ECMDBUser::db_getUsername( $userid ); 
	my $rejectionReason	= $query->param("rejectionReason");
	my $commentid		= $query->param("commentid");
	my $message		= "";
    	my $mode        = $query->param( "mode" );
	my $opcoid		= $session->param( 'opcoid' );


	ECMDBJob::rejectJob( $jobid, $userid, $commentid, $rejectionReason );
	# stop the timer - he/she has stopped working on it.
	print "Job: $jobid <br>";
	print "user: $userid <br>";

	ECMDBJob::recordStop( $jobid, $userid );
	Audit::auditLog( $userid, "TRACKER3", $jobid, "Job $jobid has been Rejected by $username" );

	my $success = Watchlist::Watchlistnotification( $userid, $opcoid, "Job $jobid has been Rejected by $username", $jobid, "REJECT" );

	my $postValue ="rejectJob";
	my $vars = {
		message		=> $message,
		jobid		=> $jobid,
		postValue	=> $postValue,
	};
        if ( $mode eq "json" ){
                my $json = encode_json ( $vars );
                print $json;
                return $json;
        }

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/rejectJob.html', $vars )
			|| die $tt->error(), "\n";
}


##############################################################################
#   RejectJobsAcctMan
#   An Account Manager wishes to reject Job(s)
#
##############################################################################
sub RejectJobsAcctMan {

	my ( $query, $session ) = @_;

	my $oktogo	= 1;
	my $message	= "";

	my $formPosted		= $query->param( "formPosted" ) ||'';
	my $menuation		= $query->param( "menuation" ) ||'';
	my $rejectionReason	= $query->param("rejectionReason");
	my $commentid		= $query->param("commentid");
	my $mode                = $query->param( "mode" ) ||'';
	my $job_number		= $query->param( "jobslist" );
	my $sendToClient	= $query->param("sendToClient") ||'';

	my $userid		= $session->param( 'userid' );
	my $username		= $session->param( 'username' );
 	my $opcoid		= $session->param( 'opcoid' );

	my @jobs;
	if ( $formPosted eq "yes" ) {
		@jobs	= split( ',', $query->param( "jobs" ));
	        $job_number = $query->param( "jobs" );
	} elsif( $menuation eq "Yes" ) {
		@jobs = split( ',', $query->param( "jobslist" ) );
	} else {
		@jobs = $query->param( "jobs" );
	}

	my $vars;
	eval {
		my @commentslist		= ECMDBJob::db_getCommentsList();
		my $parentopcoid        = ECMDBJob::db_getJobOpcoid( $job_number );
		my $userJobnumberMapping_ref;

		if ($formPosted eq "yes") {
			$oktogo 	= 0;
			$userJobnumberMapping_ref	= Watchlist::Watchlistnotification( $userid, $opcoid, "Job(s) has been Rejected by $username", \@jobs, "REJECT" );
			for my $jobid ( @jobs ) {
				# QC Reject this job
				ECMDBJob::recordStop( $jobid, $userid );
				ECMDBJob::db_updateJob( $jobid, 3 ); # 3 - QC Rejected
				ECMDBJob::rejectJob( $jobid, $userid, $commentid, $rejectionReason );
				ECMDBJob::db_lockJob( $jobid, 0 ); #unlock the job
				ECMDBJob::db_showAssets ( $jobid, 0 ); 

				my $rowid = ECMDBJob::getNotesID($jobid);
				Audit::auditLog( $userid, "JOBNOTES", $rowid, "Job $jobid has been Rejected by $username" );

				##Bring the rejection details inside the comment box. So the comment details which are done in client side is merged with reject in server side
				#my $existingdata = ECMDBJob::getcommentsbyJobid($jobid);
				my $commenttext = ECMDBJob::getCommentText( $commentid );
				my $data = "REASON FOR REJECT:".$commenttext . "<BR>COMMENT:" . $rejectionReason;		
				#my $time = strftime "%a, %e %b %Y %H:%M:%S", localtime;
				#my $newdata = '<h5><a href="#" style="color: red;">' .$username. ' Rejected the job - ' .$time. ' GMT</a></h5>';
				#$newdata .= '<div><p>' .$actualdata.'</p></div>';
				#my $data = $existingdata . $newdata;
				#ECMDBJob::addtinycomment( $query, $session, $data, $jobid);
				ECMDBJob::addJobComment($session->param('userid'), $jobid, $query->param('lineitemid') || 0 , $data,0,0,1);
			}
		}

		if ( $sendToClient eq "on" ) {
			# write confirm xml to archant - that we have received and are processing this job
			# get the agency ref - get the oba - needed to build filename
			my $urnnumber   = ECMDBJob::getAgencyRef( $job_number );
			my $obanumber   = ECMDBJob::getOBANumber( $job_number );
			my $commenttext = ECMDBJob::getCommentText( $commentid );
			my $commenttype = ECMDBJob::getCommentType( $commentid );
			my $filename;

			if ( $commenttype eq "R" ) {
				$filename = XMLResponse::write_status( "/temp/archantnotify/$obanumber-$urnnumber-rejected.xml", 
					$urnnumber, $obanumber, XMLResponse::STATUS_REJECTED, "$commenttext # $rejectionReason" );
			} else {
				$filename = XMLResponse::write_status( "/temp/archantnotify/$obanumber-$urnnumber-advisory.xml", 
					$urnnumber, $obanumber, XMLResponse::STATUS_ADVISORY, "$commenttext # $rejectionReason" );
			}
            		SendJobs::sendArchantStatusUpdate( $filename );
		}

		$vars = {
			jobs					=> \@jobs,
			message					=> $message,
			oktogo					=> $oktogo,
			formPosted				=> $formPosted,
			commentslist			=> \@commentslist,
			rejectJob				=>1,
			parentopcoid			=> $parentopcoid,
			userJobnumberMapping	=> $userJobnumberMapping_ref,
		};
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = {
			message => $error,
		}
	}
        if ( $mode eq "json" ){
                my $json = encode_json ( $vars );
                print $json;
                return $json;
        }

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/rejectjobsacctmanager.html', $vars )
			|| die $tt->error(), "\n";
}


##############################################################################
#   completeJob
#   operator completes a job
#
##############################################################################
sub completeJob {
	my $query 	= $_[0];
	my $session 	= $_[1];

	my $nodeID	= 0; # Masters nodeid checked in (Image Masters only)
	my $agencyref	= ""; # Used in JSON response
	my $message	= ""; # Used in JSON response
	my $imageworkflow = 0;
	my $vars	= {}; # Used to form JSON response (or pass params to template)
	my $username	= "";
	my $opcoid	= 0;
	my $qcenabled	= 0;
	my $customerid	= 0;
	my @history;

	my $jobid	= $query->param("jobid");
	my $mode	= $query->param( "mode" ) || "";
	my $action	= $query->param( "action" ) || "completeJob";
	my $userid	= $session->param( 'userid' );

	eval {
		die "jobid param not supplied" unless ( defined $jobid );

		$agencyref	= ECMDBJob::getAgencyRef( $jobid );
		$username	= ECMDBUser::db_getUsername( $userid );
		$opcoid		= ECMDBJob::db_getJobOpcoid( $jobid );
		$customerid	= ECMDBOpcos::getCustomerId( $jobid );
		$qcenabled	= ECMDBOpcos::db_isQCEnabled( $opcoid );
		$imageworkflow  = ECMDBOpcos::db_isOpcoImageWorkFlow( $opcoid );

		ECMDBJob::completeJob( $jobid, $userid );
		if ( $qcenabled == 1 ) {
			push( @history, "Setting QC status to required" );
			ECMDBJob::db_updateJob( $jobid, 1 ); # set QC status to 'Required' (1)
		}

		# only request a sync if the CUSTOMER has sync enabled AND repo is switched on
		if ( isJobRepositoryEnabled( $jobid ) && ECMDBOpcos::db_isRepoSyncEnabled ( $customerid ) ) {
			push( @history, "Request a sync" );
			ECMDBJob::requestSync( $jobid, $customerid, $opcoid, $userid);  # send custid also for better join
		}

		push( @history, "Stop recording" );
		ECMDBJob::recordStop( $jobid, $userid );

		$message = "Job $jobid";
		$message .= " / $agencyref" if ( ( defined $agencyref ) && ( $agencyref ne '' ) );
		$message .= " marked as 'Complete' - please request another Job";

		push( @history, "Audit log" );
		Audit::auditLog( $userid, "TRACKER3", $jobid, "Job $jobid has been Completed by $username" );

		my $userJobnumberMapping_ref = Watchlist::Watchlistnotification( $userid, $opcoid, "Job $jobid has been Completed by $username - QC REQUIRED", $jobid, $qcnotify{1} );

		# Image Workflow~ not FTP ingested images
		Imageworkflow::completeImageWorkflowJob( $query, $session, $jobid, $opcoid, \@history ) if ( $imageworkflow == 1 );

		$vars = {
			jobid           			=> $jobid,
			username					=> $username,
			qcenabled					=> $qcenabled,
			message						=> $message,
			agencyref					=> $agencyref,
			postValue					=> $action,
			opcoid						=> $opcoid,
			customerid					=> $customerid,
			nodeid						=> $nodeID,
			imageworkflow				=> $imageworkflow,
			history						=> \@history,
			userJobnumberMapping		=> $userJobnumberMapping_ref,
		};
	};
	if ( $@ ) {
		my $error = $@;
		$vars = {
			error		=> $error,
			jobid           => $jobid,
			username	=> $username,
			qcenabled	=> $qcenabled,
			message		=> $message,
			agencyref	=> $agencyref,
			postValue	=> $action,
			opcoid		=> $opcoid,
			customerid	=> $customerid,
			nodeid		=> $nodeID,
			imageworkflow	=> $imageworkflow,
			history         => \@history,
		}
	}

	if ( $mode eq "json" ){
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/completeJob.html', $vars )
			|| die $tt->error(), "\n";
}

##############################################################################
#       getDestinationDirectory()
#
##############################################################################
sub getDestinationDirectory {
	my $opcoid = $_[0];

	return ECMDBOpcos::db_getDestinationDirectory( $opcoid );
}

##############################################################################
##       getLocalUploadSourceDirectory()
##
###############################################################################
sub getLocalUploadSourceDirectory {
    my $opcoid = $_[0];

    return ECMDBOpcos::db_getLocalUploadSourceDirectory( $opcoid );
}


##############################################################################
##       getLocalUploadDestDirectory()
##
###############################################################################
sub getLocalUploadDestDirectory {
    my $opcoid = $_[0];

    return ECMDBOpcos::db_getLocalUploadDestDirectory( $opcoid );
}


##############################################################################
#	used in viewPreflight
##############################################################################
sub getFTPDetails {
	my $opcoid = $_[0];

	return ECMDBOpcos::db_getFTPDetails( $opcoid );
}


##############################################################################
#       jobPassed()
#
##############################################################################
sub jobPassed {
	my $jobid = $_[0];

	return ECMDBJob::db_jobPassed( $jobid );
}

##############################################################################
#       jobQCApproved()
#
##############################################################################
sub jobQCApproved {
	my $jobid = $_[0];

	return ECMDBJob::db_jobQCApproved( $jobid );
}

##############################################################################
#       jobQCRequired()
#
##############################################################################
sub jobQCRequired {
	my $jobid = $_[0];

	return ECMDBJob::db_jobQCRequired( $jobid );
}
##############################################################################
#       getVendors()
#
##############################################################################
sub getVendors {
	my $opcoid = $_[0];
	return ECMDBCompanies::db_getVendors( $opcoid );
}

##############################################################################
#       getOperatingCompanyFromJob()
#
##############################################################################
sub getOperatingCompanyFromJob {
	my $jobid = $_[0];

	return ECMDBJob::db_getOperatingCompanyFromJob( $jobid );
}

# ViewPreflight -- moved to modules/sendjobs.pl

##############################################################################
#       ViewHistory()
#
##############################################################################
sub ViewHistory {
	my ( $query, $session ) = @_;

	my $jobid	= $query->param( "jobid" );
	my $mode	= $query->param( "mode" ) ||'';
	my $userid	= $query->param( "userid" );
	my $from	= $query->param( "from" );
	my $to		= $query->param( "to" );
	
	#get job history
	my @logs = ECMDBAudit::db_getLogs( $jobid, $userid, $from, $to );

	my $vars = {
		jobid 		=> $jobid,
		logs 		=> \@logs,
		ViewHistory	=> 1,
	};

	if ( $mode eq "json" ){
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/viewjobhistory.html', $vars )
			|| die $tt->error(), "\n";
}

##############################################################################
##       GetUsers()
##
###############################################################################
sub GetUsers {
    my $query       = $_[0];
    my $session         = $_[1];

    my $jobno       = $query->param( "jobno" );
    my $mode        = $query->param( "mode" );
    my $opcoid		= $query->param( "opcoid" );

    my @users        = ECMDBMessages::db_getUsersForLookup( $opcoid );
	my $calendarEventLists = ECMDBDashboard::db_getcalendarEventLists();
    my $vars = {
        jobno       			=> $jobno,
        userlist    			=> \@users,
        GetUsers 				=> 1,
        calendarEventLists    	=> $calendarEventLists,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }   
}
##############################################################################
###       GetGroupUsers()
###
################################################################################
sub GetGroupUsers{

	my $query                   = $_[0];
    my $session                 = $_[1];

    my $mode                    = $query->param( "mode" ) || '';
    my $type                    = $query->param( "type" );
    my $jobno                   = $query->param( "jobno" );
    my $message_ID              = $query->param( "message_ID" );
    my $user_ID          		= $session->param( 'userid' );
    my $recipientLists_ref      = undef;

    if ( ( defined $type ) && ( $type =~ /^reply$/i ) ) {
        $recipientLists_ref = ECMDBMessages::db_getRecipientLists( $message_ID, $user_ID );
    } 
	my @users       = ECMDBMessages::db_getGroupUsersForLookup();

    my $vars = {
        jobno               => $jobno,
        userlist            => \@users,
        GetUsers            => 1,
        recipientLists  	=> $recipientLists_ref,
		message_ID			=> $message_ID,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }	
}

###############################################################################
##       SendMessage
##
##############################################################################
sub SendMessage {
	
	my ($query, $session) = @_;

    my $jobno               = $query->param( "jobid" );
    my $msgcontent          = $query->param( "msgcontent" );
    my $recipient_ids       = $query->param( "recipient_ids" );
    my $reply_message_ID	= $query->param( "reply_message_ID" ); #To reply.
    my $fromuserid          = $session->param( 'userid' );
	my $lastInsertedID		= undef;

    my $commentid   = $query->param( "commentid" );
    if($commentid)
    {
        $msgcontent = ECMDBJob::getComment($commentid);
    }

	$reply_message_ID	=~ s/^null$|^undefined$//i;
	my $parentConversationID    = ECMDBMessages::db_getMessageParentConversationID( $reply_message_ID );
    $msgcontent  =~ s/(<([^>]+)>)//ig; #To remove html tags.
    foreach my $recipientUserID (split(/,/, $recipient_ids )) {
        ## Parse out the username if it's in "realname, username" format

		if ( $recipientUserID =~ /(.*)grp$/i ) {
			$recipientUserID	= $1;
			my @groupRecipientsUserIDs_ref = ECMDBMessages::db_getGroupMsgRecipientIDs( $recipientUserID );
			
            foreach my $recipientID_ref ( @groupRecipientsUserIDs_ref ) {
				$recipientUserID	= $recipientID_ref->{'USERID'};
				$lastInsertedID	= SendGroupMessage( $jobno, $recipientUserID, $msgcontent, $fromuserid, $recipient_ids, $parentConversationID );
				$parentConversationID	= $lastInsertedID if ( $parentConversationID eq "" );
            }
        } else {
			$lastInsertedID	= SendGroupMessage( $jobno, $recipientUserID, $msgcontent, $fromuserid, $recipient_ids, $parentConversationID );
			$parentConversationID	= $lastInsertedID if ( $parentConversationID eq "" );
		}
    }

	print encode_json({ jobno => $jobno, SendMessage => 1 }) if $query->param('mode') eq 'json';
}
###############################################################################
####     SendGroupMessage
####
################################################################################

sub SendGroupMessage {

	my $jobno                   = $_[0];
    my $touserid                = $_[1];
    my $msgcontent              = $_[2];
    my $fromuserid              = $_[3];
    my $recipient_ids           = $_[4];
    my $group_conversation_id   = $_[5];
	my $lastInsertID;

    eval {
        $lastInsertID = ECMDBMessages::db_insertMessage( $jobno, $fromuserid, $touserid, $msgcontent, $recipient_ids, $group_conversation_id );

        if ( $lastInsertID ){
            my $rowid       = $lastInsertID;
            my $username    = ECMDBMessages::db_getUserName($touserid);

            Audit::auditLog( $fromuserid, "messages", $rowid, "Message sent to $username" ) if ( $touserid ne $fromuserid );
            ECMDBJob::db_recordJobAction($jobno, $fromuserid, CTSConstants::MESSAGE );
        }
    };
    if ($@) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        return encode_json({ error => $error });
    }	

	return $lastInsertID;
}

###############################################################################
###      Msgjoblocation
###
###############################################################################
sub Msgjoblocation
{

    my $query               = $_[0];
    my $session             = $_[1];
    my $jobnum              = $query->param( "jobnumber" );
    my $sessopcoid          = $session->param( 'opcoid' );
    my $userid              = $session->param( 'userid' );
    my $mode                = $query->param( "mode" );
    my $jobnumopcoid        = ECMDBJob::db_getOperatingCompanyFromJob( $jobnum );
    my $jobnumlocation      = ECMDBJob::db_getLocation( $jobnum );
    my $opconame            = ECMDBOpcos::getOpcoName( $jobnumopcoid );
    my $retval;
    
    if($sessopcoid == $jobnumopcoid || $sessopcoid == $jobnumlocation)
    {
        $retval = 1;
    }
    else
    {
        $retval = 0;
    } 
    my $vars = {
        retresponse => $retval,
        opconame    => $opconame, 
            };
    if($mode eq "json"){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
        
    }

}


###############################################################################
###       GetMessageCount
###
###############################################################################
sub GetMessageCount {
    my $query       = $_[0];
    my $session     = $_[1];   
   
    my $userid  = $session->param( 'userid' );
    my $op  = $session->param( 'opcoid' );
    my $mode    = $query->param( "mode" );

	my $viewmessagedays = ECMDBUser::db_getMessagedays( $userid );
    my $messagecount 	= ECMDBMessages::db_getMessageCount($userid, $viewmessagedays);
	my $watchlistcnt 	= ECMDBJob::db_getWatchlistcount($userid, $op);
	my $jobflowURL		= Config::getConfig("jobflowserver") || '';
    my $jobno 			= ECMDBMessages::db_getLatestJobnumber($userid);

    my $vars = {
        messagecount     => $messagecount,
        GetMessageCount  => 1,
		watchlistcnt	 => $watchlistcnt,
		jobflowURL       => $jobflowURL,
		jobno           => $jobno,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

###############################################################################
####     GetMessageList
####
################################################################################
sub GetMessageList {

	my ( $query, $session ) = @_;

    my $mode    			= $query->param( "mode" ) || '';	# SHould be json
	my $omitSentMessage    	= $query->param( "omitSentMessage" ) || ''; # yes or no
	my $job					= $query->param( "job" ) || '';   # Non blank if Related used
	my $selectedMsgID		= $query->param( "selectedMsgID" ) || ''; # Not sure its used
	my $userid  			= $session->param( 'userid' );
	my $viewmessagedays		= 0;
	my $vars;

	eval {
		$viewmessagedays                 = ECMDBUser::db_getMessagedays( $userid );

		my @messagelist = ECMDBMessages::db_getMessageList( $userid, $job, $omitSentMessage, $viewmessagedays);

		# Note we format the string - which possibly truncates it, before URI encoding
		# as doing the other way round could truncate a UTF8 sequence

		my $messagelist_ref = format_message_content( \@messagelist );

		# TODO preserve $selectedMsgID

		$vars = {
			messagelist    				=> $messagelist_ref,
			GetMessageList  			=> 1,
			omitSentMessage				=> $omitSentMessage,
			selectedMsgID				=> $selectedMsgID,
		};
	};
	if ( $@ ) {
		my $error	= $@;
		$error		=~ s/ at(.*)//;
		$vars 		= { error => $error, 	};
	}
	if ( $mode eq "json" ) {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

###############################################################################
#####     GetMessageView
#####
#################################################################################
sub GetMessageView {
    my $query       = $_[0];
    my $session     = $_[1];
    
    my $mode    		= $query->param( "mode" );
	my $status    		= $query->param( "status" );
    my $messageid    	= $query->param( "messageid" );	
    my $sentByselfFlag	= $query->param( "sentByselfFlag" );	

	if ( $status eq 0 ) {
		my $updateStatus = ECMDBMessages::db_updateMessage($messageid);
	}

	my $parentConversationID	= ECMDBMessages::db_getMessageParentConversationID( $messageid );
    my @messageview = ECMDBMessages::db_getMessageView( $parentConversationID, $sentByselfFlag );

	foreach ( @messageview ) { 
		$_->{content} = uri_escape_utf8( $_->{content} ); # URI Escape the content as it may contain UTF8 chars
	} 

    my $vars = {
        messageview     => \@messageview,
        GetMessageView  => 1,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##################################################################################
######     DeleteMessage
######
##################################################################################
sub DeleteMessage {

    my $query       = $_[0];
    my $session     = $_[1];

    my $mode    			= $query->param( "mode" ) || '';
    my $status    			= $query->param( "status" ) || '';
    my $messageid    		= $query->param( "messageids" ) || '';
    my $omitSentMessage    	= $query->param( "omitSentMessage" ) || '';
	my $userid  			= $session->param( 'userid' );

	my $viewmessagedays     = 0;
	my $vars;

	eval {

		$viewmessagedays = ECMDBUser::db_getMessagedays( $userid );

		my $deleteStatus = ECMDBMessages::db_deleteMessage($messageid);

		my @messagelist = ECMDBMessages::db_getMessageList( $userid, "", $omitSentMessage, $viewmessagedays );

		my $messagelist_ref = format_message_content(\@messagelist);

		$vars = {
			messagelist    => $messagelist_ref,
			DeleteMessage  => 1,
		};
	};
	if ( $@ ) {
		my $error = $@;
		warn $error . "\n";
		$error =~ s/ at .*//gs;
		$vars = { error => $error };
	}
    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

# DownloadAsset, getCustomerFilename, getOperatingCompanyFilename -- moved to modules/sendjobs.pl

##########################################################################
#      isJobRepositoryEnabled 
#	used in CompleteJob
##########################################################################
sub isJobRepositoryEnabled {

	my ( $isEnabled, $job_number, $customerId );

	$job_number	= $_[0];
	$customerId	= 0;
	$isEnabled	= 0;	# assume not enabled by default
 		
	# for this job - is it enabled in the customer? 
	$customerId	= ECMDBOpcos::getCustomerId( $job_number );

	# if we have a customer
	if ( $customerId > 0 ) {
		$isEnabled =  ECMDBOpcos::customerJobRepositoryEnabled( $customerId );
	}
	return $isEnabled;

} # isJobRepositoryEnabled

# getFilename, getFilenameTemplate, getJobFields now exist in modules/sendjobs.pl

#############################################################################
#       getActivity()
#	used in ViewActivity
##############################################################################
sub getActivity {
	return ECMDBJob::db_getActivity( );
}

##############################################################################
# CheckInQueue
# 	allow admin to view / admin the checkin Queue
##############################################################################
sub CheckInQueue {
	my ( $query, $session ) = @_;

	my $formPosted		= $query->param("formPosted") ||'';

	#my $shardname		= $query->param("shardname");

	my $change		= $query->param("change") ||'';
	my $priority		= $query->param("priority") ||'';
	my $delay		= $query->param("delay") ||'';
	my $checkindelay	= $query->param("checkindelay") ||'';
	my $maxcheckinsval	= $query->param("maxcheckinsval") || '';
	my $checkins		= $query->param("checkins");

	my $sidemenu		= $query->param( "sidemenu" );
	my $sidesubmenu		= $query->param( "sidesubmenu" );

	my $roleid		= $session->param( 'roleid' );

	my $job			= 0;

	my @resetjobs;
	{
		$CGI::LIST_CONTEXT_WARN = 0;
        @resetjobs = $query->param("resetjobs");
	}

	my $resetcheckboxselected = scalar( @resetjobs );

	my @jobs;
	{
		# Avoid the CGI::param called in list context error message
		# see http://www.perlmonks.org/?node_id=1140831
		# We might be using a version of CGI from before the feature was added.
		no warnings qw( once );

		# We realize that $cgi->param can return more than one value and our code handles that correctly.
		$CGI::LIST_CONTEXT_WARN = 0;
		@jobs = $query->param("jobs");
	}

	my $userid      = $session->param( 'userid' );
	my $username    = ECMDBUser::db_getUsername( $userid );

	if ( $formPosted eq "yes" ) {
		if ( $change ne '' ) {
			for $job ( @jobs ) {
				ECMDBJob::changeJobQueuePriority( $job, $priority );
			}
		}

		if ( ( $delay ne '' ) && ( length( $delay ) > 0 ) ) {
			for $job ( @jobs ) {
				ECMDBJob::delayJobQueueSubmit( $job, $checkindelay );
			}
		}

		if ( length( $checkins ) > 0 ) {
			if ( looks_like_number( $maxcheckinsval ) ) {
				DaemonPanel::updateMaxCheckIns( $maxcheckinsval );
			}
		}


		if ( $resetcheckboxselected ) {
			for $job ( @resetjobs ) {
    	       	ECMDBJob::resetCheckInJob( $job );
				Audit::auditLog( $userid, "repocheckinqueue", $job, "Job: $job has been reset by user: $username (CheckIn reset)" );
            }
		}
	}

	my @checkinqueue = ECMDBJob::getCheckInQueue();
	my $maxcheckins = DaemonPanel::getMaxCheckInLimit();

	my $vars = {
		checkinqueue 	=> \@checkinqueue,
		priority 	=> $priority,
		maxcheckins	=> $maxcheckins,
		sidemenu	=> $sidemenu,
		sidesubmenu	=> $sidesubmenu,
		roleid		=> $roleid,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/checkinqueue.html', $vars )
			|| die $tt->error(), "\n";
}

##############################################################################
# CheckOutQueue
# 	allow admin to view / admin the checkout Queue
##############################################################################
sub CheckOutQueue {
	my ( $query, $session ) = @_;

	my $formPosted			= $query->param("formPosted") ||'';
	my $priority			= $query->param("priority") ||'';
	my $change			= $query->param("change") ||'';
	my $delay			= $query->param("delay") ||'';
	my $checkoutdelay		= $query->param("checkoutdelay") ||'';
	my $maxcheckoutsval		= $query->param("maxcheckoutsval") ||'';
	my $checkouts			= $query->param("checkouts") ||'';
	my $job = 0;

	my @jobs;
	{
		# We might be using a version of CGI from before the feature was added.
		no warnings qw( once );

		# We realize that $cgi->param can return more than one value and our code handles that correctly.
		$CGI::LIST_CONTEXT_WARN = 0;
		@jobs = $query->param("jobs");
	}

	my @resetjobs;
    {
        $CGI::LIST_CONTEXT_WARN = 0;
        @resetjobs = $query->param("resetjobs");
    }

    my $resetcheckboxselected = scalar( @resetjobs );
	my $userid      = $session->param( 'userid' );
	my $username    = ECMDBUser::db_getUsername( $userid );


	if ( $formPosted eq "yes" ) {
		if ( length( $change) > 0 ) {
			for my $job ( @jobs ) {
				ECMDBJob::changeJobCOQueuePriority( $job, $priority );
			}
		}

		if ( length( $delay ) > 0 ) {
			for my $job ( @jobs ) {
				ECMDBJob::delayJobCOQueueSubmit( $job, $checkoutdelay );
			}
		}

		if ( length( $checkouts ) > 0 ) {
			if ( looks_like_number( $maxcheckoutsval ) ) {
				DaemonPanel::updateMaxCheckOuts( $maxcheckoutsval );
			}
		}

		if ( $resetcheckboxselected ) {
            for $job ( @resetjobs ) {
                ECMDBJob::resetCheckOutJob( $job );
				Audit::auditLog( $userid, "repocheckoutqueue", $job, "Job: $job has been reset by user: $username (CheckOut reset)" );
            }
        }
	}

	my @checkoutqueue = ECMDBJob::getCheckOutQueue();
	my $maxcheckouts = DaemonPanel::getMaxCheckOutLimit();

	my $vars = {
		checkoutqueue 	=>	\@checkoutqueue,
		priority 	=>	$priority,
		maxcheckouts	=>	$maxcheckouts,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/checkoutqueue.html', $vars )
			|| die $tt->error(), "\n";
}

##############################################################################
# CheckInQueueHistory
# 	allow admin to view / admin the checkin Queue
##############################################################################
sub CheckInQueueHistory {
	my ( $query, $session ) = @_;

	my $formPosted		= $query->param("formPosted") ||'';
	my $fromyear		= $query->param("fromyear"); 
	my $frommonth		= $query->param("frommonth"); 
	my $fromday		= $query->param("fromday"); 
	my $toyear		= $query->param("toyear"); 
	my $tomonth		= $query->param("tomonth"); 
	my $today		= $query->param("today"); 
	my $sidemenu		= $query->param( "sidemenu" );
	my $sidesubmenu		= $query->param( "sidesubmenu" );

	my $defaultdate		= strftime('%Y-%m-%d', localtime());

	my $roleid		= $session->param( 'roleid' );

	my $fromtime = "00:00:00";
	my $totime = "23:59:59";
	my $fromdate;
	my $todate;

	if ( $formPosted eq "yes" ) {
		$fromdate = $fromyear . "-" . $frommonth . "-" . $fromday . " " . $fromtime;
		$todate = $toyear . "-" . $tomonth . "-" . $today . " " . $totime;
	} else {
		$fromdate = get_yesterday( $defaultdate ) . " " . $fromtime;
		$todate = $defaultdate . " " . $totime;
	}
	print "from: $fromdate, to: $todate<br>";

	my @checkinqueuehistory = ECMDBJob::getCheckInQueueHistory( $fromdate, $todate );

	my $vars = {
		checkinqueuehistory 	=> \@checkinqueuehistory,
		fromyear		=> $fromyear,
		frommonth		=> $frommonth,
		fromday			=> $fromday,
		toyear			=> $toyear,
		tomonth			=> $tomonth,
		today			=> $today,
		sidemenu		=> $sidemenu,
		sidesubmenu		=> $sidesubmenu,
		roleid			=> $roleid,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/checkinqueuehistory.html', $vars )
			|| die $tt->error(), "\n";
} # CheckInQueueHistory


##############################################################################
# get_yesterday
#	what was yesterday ?
##############################################################################
sub get_yesterday {
   my $date = shift;
   my ($year, $month, $day) = split/-/,$date;
   my $time = timegm(0, 0, 12, $day, $month-1, $year);
   return strftime "%Y-%m-%d", gmtime($time - 24*60*60);
}

# AdGateDeliver - moved to modules/sendjobs.pl

sub recordOpenJob
{
	my ($query, $session) = @_;
	
	my $jobid = $query->param("jobid");
	my $userid = $session->param("userid");
	
	ECMDBJob::db_recordJobAction($jobid, $userid, CTSConstants::OPEN_JOB );
}

sub addJobComment {

    my ($query, $session) = @_;
    my $mode            		= $query->param('mode');
    my $userid          		= $session->param('userid');
    my $comment         		= $query->param('comment');
    my $sequence        		= $query->param('sequence');
    my $lineitemid      		= $query->param('lineitemid');
    my $typeOfView      		= $query->param('typeOfView');
    my $job_number      		= $query->param('job_number');

    my $vars            		= undef;
    my $isenabledelete  		= undef;
    my $last_inserted_commentId	= undef;
    
    #my @selected_jobs   = $query->param('job_number[]'); #To get the array of selected jobs.
    my $selected_jobs   = $query->param('job_number'); #To get the array of selected jobs.
    my @headers         = "";
 
    eval {
        foreach my $jobnumber ( split( /,/, $selected_jobs ) ) {
            my $parentopcoid    	= ECMDBJob::db_getJobOpcoid( $jobnumber );
            my $customerid      	= ECMDBJob::db_getCustomerIdJob( $jobnumber );
            $last_inserted_commentId 	= ECMDBJob::addJobComment( $userid, $jobnumber, $lineitemid, $comment );
            ## Index in Elastic Search server
            $comment =~ s/\{//;
            JFElastic::indexBrief( $parentopcoid, $jobnumber, $comment );

            ## Headers returns an array sorted in (sequence-)descending order,
            ## so our freshly-added one should be at the top
			if ( $typeOfView	=~ /^ListView$/i ) {
            	$isenabledelete  =  ECMDBJob::isDeleteEnabled( $customerid, $parentopcoid );
            	@headers = ECMDBJob::getJobCommentHeaders( $jobnumber, $lineitemid );
			}
        }

		if ( $typeOfView    =~ /^ListView$/i ) {
        	$vars	= {
            	headers         => $headers[0],
            	enabledelete    => $isenabledelete,
        	};
		} else {
			my $boardViewBriefCommentsDet   = ECMDBKanbanboard::db_getBoardviewBriefComments( $job_number );
			$vars	= {
				commentid					=> $last_inserted_commentId,
        		boardViewBriefCommentsDet   => \@$boardViewBriefCommentsDet,
			};
		}
    };
    if ( $@ ) {
		my $error   = $@;
		$error  =~ s/ at .*//;
		$vars    = { error   => $error, }; 
	}

    if ( $mode eq "json") {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
} #addJobComment

sub updateJobComment
{
	my ($query, $session) = @_;

	my $userid 				= $session->param('userid');
	my $job_number 			= $query->param('job_number');
	my $lineitemid 			= $query->param('lineitemid');
	my $sequence 			= $query->param('sequence');
	my $comment 			= $query->param('comment');
	my $commentID			= $query->param('id');
	my $mode				= $query->param('mode');

	ECMDBJob::addJobComment($userid, $job_number, $lineitemid, $comment, $commentID, $sequence);
	my $currentCommentID	= ECMDBJob::updateJobComment( $commentID, $job_number );

	my $vars = {
		currentCommentID  => $currentCommentID,
	};

	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

# moved storeFile, deleteFile, closeDialog, eraseDocument to showjob.pl  

# moved digitalUpload, showversion, rollback, erase, showComments, documentUpload, getFileNameAndExt,storeDocuments, deleteDocuments, docCloseDialog to showjob.pl 

# moved reviewDigitalAsset to modules/digitalasset.pl 

##############################################################################
#       Delete HTML files from server Folder (action=delete_cache)
##############################################################################

sub deleteCache {
	my $query               = $_[0];

	my $job_number 			= $query->param( "jobNumber" );
	my $curDir = `pwd`;
    chomp ( $curDir );
    my $dirName = `dirname $curDir`;
    chomp ( $dirName );

	my $tmpfileloc = Config::getConfig("tempfileloc"); 
	if ( ( defined $tmpfileloc ) && ( -d "$dirName/$tmpfileloc/$job_number" ) ) {
		rmtree( "$dirName/$tmpfileloc/$job_number" );
	}
}

##############################################################################
#    renderPdf
##############################################################################
sub renderPdf {

	my $query               = $_[0];
	my $session             = $_[1];
	my $mode                = $query->param( "mode" );
	my $jobid               = $query->param("jobid");
	my $chkStaticFile		= $query->param("chkStaticFile");
	my $userid  			= $session->param( 'userid' );
	my $uploadUserName      = ECMDBUser::db_getUsername($userid);
	my $renderReturn		= undef;


	my $chkStaticFileCount = ECMDBJob::db_getStaticFileCount( $jobid );
	if ( $chkStaticFileCount > 0 && $chkStaticFile == 1 ) {
		$renderReturn   = "3";		#Added by Anbu for static file check before convert to digital URL	
	}
	else{
		my $qcApprovedFileCount	= ECMDBJob::db_getQCApprovedFileCounts( $jobid );
		if ( $qcApprovedFileCount > 0 ) {

			$renderReturn = ECMDBJob::db_renderPdf( $jobid, $userid );
			if ( $renderReturn ne "" ) {
				my $assetDownloadFlag	= $query->param("assetDownloadFlag");
				Audit::auditLog( $userid, "DIGITAL", $jobid, "Create Digital Url $jobid.pdf by $uploadUserName" );
				ECMDBJob::db_recordJobAction($jobid, $userid, CTSConstants::DIGITAL_URL_TO_CMD );
				ECMDBJob::db_updateViewRenderedPdf( $jobid, $assetDownloadFlag );	
			}
		} else {
			$renderReturn	= "2";
		}
	}

	my $vars = {
        renderReturn       => $renderReturn,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}#renderPdf

##############################################################################
#    sendRenderedPdfToCMD (action=sendRenderedPdfToCMD)
##############################################################################
sub sendRenderedPdfToCMD {
	my ( $query, $session ) = @_;

	my $mode			= $query->param( "mode" ) ||'';
	my $jobid			= $query->param("jobid");

	my $userid			= $session->param( 'userid' );
	my $uploadUserName	= $session->param( 'username' );
	
	my $returnVal = 0;
	my $ftp;
	my $vars;

	my $renderToCMDPdfPath = Config::getConfig("renderToCMDPdfPath" );

	eval {

		my $assetrepository 	= getAssetRepositoryJobPath( $jobid );
		my $renderSrcPdfName 	= sprintf( "%s%s/%s.pdf", "$assetrepository", "RENDERED_PDF", "$jobid" );
		my $renderToCMDPdfName 	= sprintf( "%s%s.pdf", "$renderToCMDPdfPath", "$jobid" );

		my $jobopcoid           = ECMDBJob::db_getJobOpcoid( $jobid );
		my ( $ftpServer, $ftpUserName, $ftpPassword ) = Jobs::getFTPDetails( $jobopcoid ) ;
		$ftp = Net::FTP->new( $ftpServer, Debug => 0 ) or die "Cannot connect to FTP server: $ftpServer, $@";

		if ( $ftp ) {
			$ftp->login( $ftpUserName, $ftpPassword ) or die "Cannot login", $ftp->message;
			$ftp->binary();
			my $retVal = $ftp->copy( "$renderSrcPdfName", "$renderToCMDPdfName" );

			if ( ( defined $retVal ) && looks_like_number( $retVal ) && ( $retVal == 1 ) ) {
				$ftp->site( "chmod 777 $renderToCMDPdfName" ) or die "change permission failed for $renderToCMDPdfName\n", $ftp->message;
				ECMDBJob::db_recordJobAction($jobid, $userid, CTSConstants::SEND_TO_CMD );
				Audit::auditLog( $userid, "DIGITAL", $jobid, "Digital URL to CMD $renderToCMDPdfName by $uploadUserName" );
				$returnVal = 1;
			} else {
				Audit::auditLog( $userid, "DIGITAL", $jobid, "Failed Digital URL to CMD $renderToCMDPdfName by $uploadUserName" );	
			}
		}
		$ftp->close();
		$ftp = undef;

		$vars = {
			renderSendToCMDReturnVal	=> $returnVal,
		};
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//g;
		$vars = { error => $error, renderSendToCMDReturnVal => 0 };
		$ftp->close() if ( defined $ftp );
	}
	if ( $mode eq "json" ) {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
	
} #sendRenderedPdfToCMD

##############################################################################
#    AJAX QC changes for modal box
##############################################################################

sub QCstatus {
    	my $query               = $_[0];
   	 	my $session             = $_[1];
        my ($job_number,$filename,$fileid,$qcid,$width,$height,$value);
        $job_number = $query->param( "job_number" );
        $filename = $query->param( "filename" );
        $fileid   = $query->param( "fileid" );
		$qcid   = $query->param( "qcid" );
		$width   = $query->param( "width" );
		$height   = $query->param( "height" );
		$value   = $query->param( "value" );
        my $output = ECMDBJob::db_updateqc($job_number, $filename, $fileid, $width, $height, $value);
		my $staticFlag = ECMDBJob::db_getStaticFlag( $job_number, $filename );

		if ( $value =~ /^QC DECLINED$/i && $staticFlag == 2 ) {
			 ECMDBJob::db_updateStaticUpload( $job_number, $filename, "1" );  #3rd Argument one(1) indicates the Static file need to be upload.
			 DigitalAssets::deleteDigitAssetFrmGPFS( $job_number, $filename, 0 );
		}

        my $vars = {
                                value    => $value,
                                fileid  => $fileid,
                                qcid    => $qcid,
                        };


     my $json = encode_json ( $vars );
     print $json;
         return $json;

}

# moved deleteAsset, storeDocuments, staticFileUploaded to showjob.pl

# moved constructAssetDetails, getAssetUploadTempPath moved to digitalassets.pl

# moved recursiveBackFileName  to showjob.pl
# moved staticFileUploaded, storeDigitalAssetsInGPFS to digitalassets.pl

##########################################################################
## ShowTaskProcessDialog
##########################################################################
sub ShowTaskProcessDialog{
	my $query   = $_[0];
	my $session = $_[1];

	my $mode = $query->param( "mode" );
	my $opcoid      = $session->param('opcoid');
	my $roleid      = $session->param( 'roleid' );
	my $lineitemid  = $query->param("lineitemid");
	my $jobnumber   = $query->param("jobnumber");
	
	my $currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );

	# We need the customer id of current user logged opco. So session opcoid is passed to pick that customer id.
    # Otherwise job level custid in tracker3 table always maintains the custid which comes from CMD.
    # It is not getting update when job is moved from one location to another. Checklist will be set based on opco/customer wise wise in Admin->checklist.
	my $customerid  = ECMDBOpcos::getCustomerId( $jobnumber, $opcoid );

	my $location = ECMDBJob::db_getLocation($jobnumber);
	my $jobopcoid   = ECMDBJob::db_getJobOpcoid( $jobnumber );

	my @users       = ECMDBProducts::getCustomerUsersProducts( $customerid, $jobnumber, $jobopcoid, $location );
	my $mediatypeId  = Checklist::getMediatypeId( $jobnumber );

	my $userid      = $session->param( 'userid' );
	my $currentuserid = ECMDBProducts::getProductAssignee( $lineitemid ) || 0;
	my $hasworkedonbefore = ECMDBProducts::hasWorkedOnBefore( $lineitemid, $userid );
	my @currentProductDetails = ECMDBProducts::db_currentProductDetails($jobnumber, $lineitemid);
	my $elapsedtime = ECMDBProducts::getElapsedTime( $lineitemid, $currentuserid );

	my $laststoppedtime;
	if($hasworkedonbefore > 0){
		$laststoppedtime = $elapsedtime;
	} else {
        	$laststoppedtime = ECMDBProducts::getLastStoppedTime( $lineitemid, $currentuserid );
	}
	my $laststoppedtimehms = elapsedToHHMMSS( $laststoppedtime );

	my @productDetails = ECMDBProducts::db_productDetails($jobnumber);
	my @comments = (ECMDBJob::getJobCommentHeaders($jobnumber, $lineitemid));

	my $vars = {
		roleid          =>  $roleid,
		lineitemid		=> $lineitemid,
		jobnumber		=> $jobnumber,
		status => $currentProductStatus,
		users       => \@users,
		currentuserid => $currentuserid,
		userid => $userid,
		hasworkedonbefore => $hasworkedonbefore,
		currentProductDetails => \@currentProductDetails,
		elapsedtime => $elapsedtime,
		laststoppedtimehms => $laststoppedtimehms,
		productDetails => \@productDetails,
		mediatypeId => $mediatypeId,
		customerid => $customerid,
		opcoid => $opcoid,
		comments => \@comments,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}  #ShowTaskProcessDialog

# moved viewTimeCapture to products.pl

# moved uploadSelectedAllAssets to showjob.pl

# moved deleteDigitAssetFrmGPFS, deleteAllAssets, eraseAllAssetsFromGPFS to digitalassets.pl

# moved getFileExtension to showjob.pl 

###############################################################################
###       Message Group
###
###############################################################################
sub SendGroupList {

    my $query                   	= $_[0];
    my $session                 	= $_[1];
            
    my $mode                        = $query->param( "mode" );
    my $editval						= $query->param( "edit" );			
    my $groupname                   = $query->param( "groupname" );
    my $fromuserid                  = $session->param( 'userid' );
	my $msgGroupUserID				= undef;
    my $msgRecipientUserIDs			= $query->param( "msgRecipientUserIds" );
    my @msgRecipientUserIDs			= split(',', $msgRecipientUserIDs, 999 );

    my @username;
    my $uname;
    my $username;
    my $touserid;
    my $checkStatus="";
    my $deleteStatus="";

    if ( $editval ) {
       $checkStatus = 0;
    } else {
         $checkStatus  = ECMDBMessages::db_checkGroupname($groupname);
	}

    if ( $checkStatus != 1 ) {

		if( $groupname eq $editval ) {
			$deleteStatus=ECMDBMessages::db_deletegroup($groupname);
        } else {
			$deleteStatus=ECMDBMessages::db_deletegroup($editval);
		}

		if ( $editval ) {
			$msgGroupUserID	= ECMDBMessages::db_getMsgGroupID( $groupname );	
		} else {
			$msgGroupUserID	= ECMDBMessages::db_createMsgGrpName( $fromuserid, $groupname );
		}

    	foreach my $recipientUserID ( @msgRecipientUserIDs ) {
	     	my $insertStatus = ECMDBMessages::db_insertMessagegroup( $groupname, $recipientUserID, $fromuserid, $msgGroupUserID );
    	}
    }
   
    my $vars = 
    {
        MessageGroup  => 1,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}
###############################################################################
####       Check Group Name
####
################################################################################
sub CheckGroupName	{
	my $query   			= $_[0];
	my $session 			= $_[1];
	my $mode                = $query->param( "mode" );
	my $groupname           = $query->param( "groupname" );
	my $checkStatus         = ECMDBMessages::db_checkGroupname($groupname);

	my $vars = {
		CheckGroup =>$checkStatus,
	};

	if ( $mode eq "json" ) {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

##############################################################################
##      To update the Resources based on the selected Skill value. 
###############################################################################
sub updateResource {
	my $query				= $_[0];
	my $session				= $_[1];
	my $selectedSkill		= $query->param( "selectedUserSkill" );
	my $mode				= $query->param( "mode" );
	my $opcoid				= $query->param( "opcoid" );
	my $selectedDay			= $query->param( "selectedDay" );
#	my $current_date 		= strftime('%Y-%m-%d', localtime());

	my $cust                    = $query->param( "selectedCust" );
    my $dept                    = $query->param( "selectedDept" );
    my $media                   = $query->param( "selectedMedia" );
	my ($availableResource, $db_resources)	= ECMDBJob::db_avlresources( $opcoid, $selectedSkill,$selectedDay );
	my ( $pending, $pendingEstimateHrs )           = ECMDBJob::boxcountshow($opcoid, $cust, $dept, $media, "U,A",$selectedDay);
   	my ( $rejected, $rejectedEstimateHrs )         = ECMDBJob::boxcountshow($opcoid, $cust, $dept, $media, "R",$selectedDay);
    my ( $inprogress, $inprogressEstimateHrs )     = ECMDBJob::boxcountshow($opcoid, $cust, $dept, $media, "W,P",$selectedDay);
    my ( $waitingforQC, $waitingforQCEstimateHrs ) = ECMDBJob::boxcountshow($opcoid, $cust, $dept, $media, "1",$selectedDay);
    my ( $overschedule, $overscheduleEstimateHrs ) = ECMDBJob::boxcountshow($opcoid, $cust, $dept, $media, "Over",$selectedDay);
    my ( $totalJobs, $totalJobsEstHrs ) 		   = ECMDBJob::boxcountshow($opcoid, $cust, $dept, $media, "Totjobs",$selectedDay);
		$pendingEstimateHrs     = formatDashboardEstHrs( $pendingEstimateHrs );
        $rejectedEstimateHrs    = formatDashboardEstHrs( $rejectedEstimateHrs );
        $inprogressEstimateHrs  = formatDashboardEstHrs( $inprogressEstimateHrs );
        $waitingforQCEstimateHrs= formatDashboardEstHrs( $waitingforQCEstimateHrs );
        $overscheduleEstimateHrs= formatDashboardEstHrs( $overscheduleEstimateHrs );
		$totalJobsEstHrs        = formatDashboardEstHrs(  $totalJobsEstHrs );
	my $vars = {
		resources =>$db_resources,
		pendingEstimateHrs      => $pendingEstimateHrs,
        inprogressEstimateHrs   => $inprogressEstimateHrs,
        rejectedEstimateHrs     => $rejectedEstimateHrs,
        waitingforQCEstimateHrs => $waitingforQCEstimateHrs,
        overscheduleEstimateHrs => $overscheduleEstimateHrs,
		totalJobs               => $totalJobs,
		pending  =>$pending,
		inprogress              => $inprogress,
        rejected                => $rejected,
        waitingforQC            => $waitingforQC,
        overschedule            => $overschedule,
		totalJobsEstHrs			=> $totalJobsEstHrs,
	};

	if ( $mode eq "json" ) {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
} #updateResource

sub get_language_list
{
	my ($query, $session) = @_;
	
    my $uploaderid  = $session->param( 'userid' );
	my $countryid   = $query->param('cid');
    my $jobid       = $query->param( "jobid" );

    ECMDBJob::db_recordJobAction($jobid, $uploaderid, CTSConstants::GOOGLE_DOC_REPLACEMENT );
	
	## TODO : Find the correct place for this
	#db_recordJobAction($jobid, $uploaderid, CTSConstants::GOOGLE_DOC_REPLACEMENT );
	
	my @languages = ECMDBJob::db_getlanguagelist($countryid);
	
	if($query->param('mode') eq 'json')
	{
		print encode_json({ languagelist => \@languages });
    }
}

# Moved update_broadcast to broadcast.pl

##############################################################################
####       GetGroup()
####
#################################################################################
sub GetGroup{
    my $query       = $_[0];
    my $session         = $_[1];

    my $mode        = $query->param( "mode" ) ||'';
    my $owner_id		= $session->param( "userid" );
    my @groups        = ECMDBMessages::db_getGroupName($owner_id);
    my $vars = {
        grouplist    => \@groups,
        GetUsers => 1,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}


##############################################################################
#####       DeleteGroupName()
#####
##################################################################################
sub DeleteGroupName{
    my $query       		= $_[0];
    my $session         	= $_[1];

    my $mode    			= $query->param( "mode" ) ||'';
    my $groupname			= $query->param( "groupname" );			

    my $deletegroup      	= ECMDBMessages::db_deleteGroupName($groupname);

    my $vars = {
        grouplist    => $deletegroup,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

###################################################################################
###    viewGroupDetails()
####
###################################################################################
sub viewGroupDetails
{

  my $query		= $_[0];
  my $session		= $_[1];
  my $groupname         = $query->param( "groupname" );
  my $mode              = $query->param( "mode" );
  my $owner_id		= $session->param( "userid" );
  my @groupdetails 	= ECMDBMessages::db_getGroupDetails($groupname,$owner_id);
  my @users        = ECMDBMessages::db_getUsersForLookup();
  my $vars = 
  {

        groupdetails	=> \@groupdetails,
	userlist	=> \@users,
  };
  if ( $mode eq "json" )
  {
	my $json = encode_json ( $vars );
	print $json;
	return $json;
  }
}

# get_session_details moved to users.pl

# format_message_content used in GetMessageList

sub format_message_content {
	my @messagelist = @{$_[0]};
	foreach (@messagelist) {
		$_->{content} =~ s/<.*?>//g;
		$_->{content} =~ s/\n.+$/.../s;
		$_->{content} = substr($_->{content}, 0, 60) . (length($_->{content}) > 60 ? '...' : '');
		$_->{content} = uri_escape_utf8( $_->{content} ); # URI Escape the content as it may contain UTF8 chars
	}
	return \@messagelist;
}

###################################################################################
#SearchJobs: To check job details present in DB.
###################################################################################

sub SearchJobs {
	my ( $session, $searchTypeID, $searchContents, $jobArchivedFlg ) = @_;
	my %viz_at_other;# key is opconame, value is count of jobs from search at that location (which are visible)
	my %jobs_at_other;# key is opconame, value is count of jobs from search at that location (which are invisible)
	my %cust_not_viz;# key is "opconame => customer name(customer id)", value is count
	my %other_errors;# key is error text, value is count
	my @history;
	my $vars;
	my $t0 = 0;# Start time
	my $queryTime = 0;# Time for initial query
	my $searchTime = 0;# Time for complete search (and various lookups)
	my $userid = $session->param('userid');	# Users userid
	my $user_opcoid = $session->param('opcoid');# Users current opcoid
	my $searchLimit = Config::getConfig("searchlimit") || 2000; # Limit number of rows returned by search
	my $copySql = "";
	eval {
		die "Invalid search type id '$searchTypeID'" unless ( defined $searchTypeID ) && ( looks_like_number( $searchTypeID ) );
		die "Missing search content" unless ( defined $searchContents );
		# per search type validation done in databaseSearch and db_checkSearchContent can throw exception
		# Could be clever here and allow exact or wildcard match, but leave for now
		$t0 = time();
		my ( $entity, $rows, $sql )	= ECMDBSearch::db_checkSearchContent( $searchTypeID, $searchContents, $searchLimit );
		# warn $sql;
		$copySql = $sql;
		if ( $searchTypeID == 7 )
		{
			my $error = $session->param('searchContent');
			push( @history, "Searching for $entity '$error' ..." ); # Do after rather than before as search returns entity type
		}
		else
		{
			push( @history, "Searching for $entity '$searchContents' ..." ); # Do after rather than before as search returns entity type
		}

		# Handle no rows slightly differently when searching by job number
		if ((  ! defined $rows ) || ( scalar( @{ $rows } ) == 0 ) ) {
			die "Job '$searchContents' not found" if ( $searchTypeID == 1 );
			if ( $searchTypeID == 7 )
			{
				my $error = $session->param('searchContent');
				die "No jobs found, for $entity '$error'";
			}
			else
			{
				die "No jobs found, for $entity '$searchContents'";
			}
		}
		$queryTime = time() - $t0;
		# warn "Rows returned " . scalar( @{ $rows } );
		if ( scalar( @{ $rows } ) == $searchLimit ) {
			push( @history, "Search limit reached, $searchLimit rows returned" );
		}

		#################  Job Visibility   ############################
		my $customer_ids = ECMDBCustomerUser::getUserCustomerIdsByOpco( $userid, $user_opcoid );
		die "User has no visible customers" unless ( ( defined $customer_ids ) && ( scalar @$customer_ids ) );
		my $opco_ids = ECMDBSearch::getUsersOpcos( $userid );
		die "User has no visible opcos" unless ( ( defined $opco_ids ) && ( scalar @$opco_ids ) );
		my $users_opconame = ECMDBSearch::getOpcoNameFromId( $user_opcoid ); # Name of current opco
		my @joblist;
		foreach my $job ( @{ $rows } ) {
			my $opcoid                              = $job->{ opcoid };
			my $location                    = $job->{ location };
			my $job_number                  = $job->{ job_number };
			my $customerid                  = $job->{ customerid };
			my $jobArchiveStatus    = $job->{ status };
			eval {
				my $job_visible = 0;
				# Get the customer name first as we need it in the error messages
				die "Customer id is undefined" if ( ! defined $customerid ) || ( $customerid eq '' );
				# Can we see customer ie visible at any of the opcos we can see
				# in scalar context grep returns number of elements that match, http://perldoc.perl.org/functions/grep.html
				my $customer_visible = grep {$_ eq $customerid }  @$customer_ids ;
				if ( ! $customer_visible ) {
					my ( $customerName ) = ECMDBSearch::db_getCustomerName( $customerid );
					die "Failed to get customer name for id '$customerid'" unless defined $customerName;
					my $opconame = ECMDBSearch::getOpcoNameFromId( $opcoid );
					die "opcoid '$opcoid' is invalid" unless ( defined $opconame );
					my $key = "$opconame => $customerName";
					die "Customer $key not visible, please contact Jobflow admin" if ( $searchTypeID == 1 );
					$cust_not_viz{ $key }++;
				} else {
					# Customer is visible, now check home/remote location is visible
					if ( ( $user_opcoid == $opcoid ) || ( ( defined $location ) && ( $user_opcoid == $location ) ) ) {
						push( @joblist, $job_number );  # User can see job at home location, or remote location
						$job_visible = 1;
					} elsif ( ( defined $location ) && ( $location == OPCO_MULTIPLE_LOCATIONS ) ) {
						if ( ECMDBSearch::hasJobShardAtLocation( $job_number, $user_opcoid ) ) {
							push( @joblist, $job_number );
							$job_visible = 1;
						}
					}
					if ( $job_visible == 0 ) {
						# The user cant see this job with their current opcoid, so determine if they can
						# change to another Opco where they can see the jobs (using either opcoid or location).
						my @locations;          # List of opconames visible or not
						my @visible_locations;  # Subset which are visible
						my $visible_opcoid;
						my $visible_opco_location;
						my $opconame= ECMDBSearch::getOpcoNameFromId( $opcoid );
						push( @locations, "opco $opconame" );
						#my $visible_at = ( grep( /^$opcoid$/, @$opco_ids ) );
						my $visible_at = ( grep { $_ eq $opcoid } @$opco_ids  );
						if ( $visible_at ) {
							push( @locations, "opco $opconame" );
							push( @visible_locations, $opconame );
							$visible_opcoid	=	$opcoid;
							$viz_at_other{ $opconame }++ unless ( $searchTypeID == 1 );
						};
						my $location_opconame;
						my @other_locations;
						if ( ! defined $location ) { 
						   	# nothing
						} elsif ( $location == OPCO_MULTIPLE_LOCATIONS ) {
							# get shard locations ..
							my $shard_locations = ECMDBSearch::getShardLocations($job_number);
							@other_locations = @{$shard_locations};
						} elsif ( $location != $opcoid ) { 
							push( @other_locations, $location ); 
						}
						foreach my $location (@other_locations)
						{
							#$visible_at = ( grep( /^$location$/, @$opco_ids ) );
							$visible_at = ( grep { $_ eq $location }  @$opco_ids  );
							if ( $visible_at ) {
								$location_opconame = ECMDBSearch::getOpcoNameFromId( $location );

								if ( ! defined $location_opconame ) {
									my $error = "location $location is invalid";
									$other_errors{ $error }++;
								} else {
									push( @locations, "location $location_opconame" );
									push( @visible_locations, $location_opconame );
									$visible_opco_location	=	$location;
									$viz_at_other{ $location_opconame }++ unless ( $searchTypeID == 1 );
								}
							};
						}		
						# If the job is not visible to the user irrespective of their available opcos
						# then either throw an error (if searching for a job number) or log a summary
						# of the invisible jobs, recording a count against the opconame
						if ( $searchTypeID == 1 ) {
							# For search by job number just display an error, for other search types, the info is in %viz_at_other
							my $opco_customers = ECMDBCustomerUser::getUserCustomerIdsByOpco( $userid, $visible_opcoid );
							my $location_customers = ECMDBCustomerUser::getUserCustomerIdsByOpco( $userid, $visible_opco_location);
							my $opco_customer_visible = grep { $_ eq $customerid } @$opco_customers ;	
							my $location_customer_visible = grep { $_ eq $customerid } @$location_customers ;	
							my ( $customerName ) = ECMDBSearch::db_getCustomerName( $customerid );
							if($opco_customer_visible || $location_customer_visible)
							{
								die "Job exists at " . join( ", ", @locations ) . ", customer $customerName" if ( ( scalar @visible_locations ) == 0 );
								push( @history, "Job is visible at " . join( ", ", @visible_locations ) . ", customer $customerName" );
							}
							else
							{
								push (@history,"You do not have access to customer $customerName at " . join( ", ", @visible_locations ) . ", please contact Jobflow admin");
							}

						} else {
							# Types other than job number

							if ( ( scalar @visible_locations ) == 0 ) {
								# Otherwise record the presence of jobs which match at other opcos
								foreach my $loc ( @locations ) {
									$jobs_at_other{ $loc } ++;
								}
							}
						}
					} #Job_visible end loop
				} # endif (customer not visible)
				#To check whether the job is archived or not.
				die "Search job $job_number is archived... please enable the filter Show Archived" if ( $jobArchiveStatus == 6 && $jobArchivedFlg =~ /^false$|^0$/i );
			};
			if ( $@ ) {
				my $error = $@;
				$error =~ s/ at modules.*//gs;
				die $error if ( $searchTypeID == 1 ); # Any error for search by job throw to outer level
				$other_errors{ $error }++;
			}
		} # end of foreach

		$searchTime = time() - $t0;

		my $jobcount = scalar( @joblist );
		if ( $jobcount == 0 ) {
			die "No jobs visible at current Opco" if ( $searchTypeID != 1 );
		} elsif ( $jobcount == 1 ) {
			push( @history, "Displaying 1 job" );
		} else {
			push( @history, "Displaying $jobcount jobs" );
		}
		$vars = { jobs => \@joblist, history => \@history, viz_at_other => \%viz_at_other, jobs_at_other => \%jobs_at_other, 
				cust_not_viz => \%cust_not_viz, other_errors => \%other_errors,
				search_type_id => $searchTypeID, search_type_contents => $searchContents,
				# sql => $copySql,
				query_time => $queryTime, search_time => $searchTime, };

	}; #first eval block end
	if( $@ ){
		my $error = $@;
		$error =~ s/ at modules.*//gs;
		$searchTime = time() - $t0; 
		$vars = { error => $error, history => \@history, viz_at_other => \%viz_at_other, jobs_at_other => \%jobs_at_other, 
			cust_not_viz => \%cust_not_viz, other_errors => \%other_errors,
			search_type_id => $searchTypeID, search_type_contents => $searchContents,
			# sql => $copySql,
			query_time => $queryTime, search_time => $searchTime };
	}
	print encode_json ( $vars );
} #SearchJobs

# Moved updateStatusNotes to broadcast.pl
1;
