#!/usr/local/bin/perl

use warnings;
use strict;

package Utilities;

use File::Path;
#use Path::Class;
use utf8;
use Encode;

use Logger;
use constant LOG_TAG => "UTL";

use Data::Dumper;

#------------------------------------------------------------------
#	Recursively create a directory path
#------------------------------------------------------------------
sub mkdirpath
{
	my ($path) = @_;
	
	my $existingroot = "";
	my $curdir = ($path =~ m/^[^\\\/]/ ? "." : "");	#DETERMINE IF THE PATH IS RELATIVE OR ABSOLUTE
	my $newdir = "";
	
	#FOR EACH DIRECTORY (SLASH-DELIMITED ENTRY)
	my $inflimit = 1000;
	while($path =~ m/^([^\\\/]*)[\\\/](.*)$/)
	{
		$inflimit--;
		die "mkdirpath: Infinite loop, path $path" if ( $inflimit == 0 );

		$newdir = $1;
		$path = $2;
		
		#IGNORE DOUBLE-SLASHES
		next unless $1;
		
		#MAKE THE DIRECTORY IF APPLICABLE
		$curdir .= "/" . $newdir;
		if(-d $curdir)
		{
			$existingroot = $curdir;
		}
		else
		{
			print_message(LOGLEVEL_TRACE, "Forming new path '$curdir'\n", LOG_TAG);
			mkdir $curdir;
		}
		chmod 0755, $curdir;
	}
	
	#FINAL MAKE
	$curdir .= "/" . $path;
	if(-d $curdir)
	{
		$existingroot = $curdir if $path;
	}
	else
	{
		print_message(LOGLEVEL_TRACE, "Final make '$curdir'\n", LOG_TAG);
		mkdir $curdir;
	}
	chmod 0755, $curdir;
	
	print_message(LOGLEVEL_TRACE, "Added directories onto '$existingroot'", LOG_TAG);
	return $existingroot;
}


## --<<< >>>--
## --<<< Trawl through a directory and delete any files/folders that match filters >>>--
sub cleandirpath
{
	my ($directorypath, $safemode, @deletefilter) = @_;
	
	#####
	##--- Make sure we can delete the contents of the directory
		$safemode = 1 unless defined $safemode;
		$directorypath = add_slash($directorypath, 'END');
		print_message(LOGLEVEL_FAILURE, "Path isn't deemed safe enough to delete from", LOG_TAG) if !is_safe_path($directorypath);
	
	
	#####
	##--- Get a list of the files in the directory
		opendir DIR, $directorypath or print_message(LOGLEVEL_FAILURE, "Couldn't open directory '$directorypath' ($!)", LOG_TAG);
		my @files = grep { !/^\.{1,2}$/ } readdir(DIR);
		$_ = decode('utf8', $_) foreach(@files);	
		closedir DIR;
		
	
	#####
	##--- Delete files that match a filter
		my $failure = 0;
		foreach my $file (@files)
		{
			my $filename = "$directorypath$file";
			next if !-e $filename;
			print_message(LOGLEVEL_TRACE, "Testing '$directorypath$file'", LOG_TAG);
			
			## Descend into directories (We delete empty folders regardless, and filters will block deleting important files)
			if(-d $filename)
			{
				if(cleandirpath("$filename/", $safemode, @deletefilter))
				{
					print_message(LOGLEVEL_WARNING, "Can't delete '$filename/'; couldn't delete some of its contents\n", LOG_TAG);
					$failure++;
				}
				
				next;
			}
			
			
			## See if the file matches any filters, so that we know we can delete it
			my $match = "";
			foreach my $filter (@deletefilter) { if($file =~ m/$filter/) { $match = $filter; last; } }
			if(!$match)
			{
				print_message(LOGLEVEL_WARNING, "Can't delete '$filename' - exists and doesn't match any filters\n", LOG_TAG);
				$failure++;
				next;
			}
			
			print_message(LOGLEVEL_TRACE, "'$filename' matches filter '$match'; deleting", LOG_TAG);
			unlink $filename unless $safemode;
		}
		
	## Remove the source directory if it's empty
		print_message(LOGLEVEL_DEBUG, "Deleting root directory '$directorypath' [$failure failures]\n", LOG_TAG);
		rmdir $directorypath if !$failure && !$safemode;
		
		return $failure;
}



#TRAVERSES UP A PATH, DELETING EACH DIRECTORY ONLY IF IT'S EMPTY
sub cleanDirPathQuick
{
	my ($directorypath, $root) = @_;
	$directorypath .= "/" unless $directorypath =~ m/\/$/;
	
	$root ||= "";
	$root .= "/" unless $root =~ m/\/$/;

	my $inflimit = 1000;	
	while($directorypath ne "/")
	{
		$inflimit--;
		die "cleanDirPathQuick: Infinite loop, path $directorypath" if ( $inflimit == 0 );

		$directorypath =~ m/^(\/?.*[^\\])\/(.*?)$/;
		my $dp = $1;
		my $path = "$1/";
		
		if($path eq $root) { print_message(LOGLEVEL_TRACE, "Not deleting '$root'", LOG_TAG); last; }
		
		#OPEN THE DIRECTORY IF WE CAN
		if(opendir(my $dirhandle, $path))
		{
			#DOES IT HAVE FILES IN IT?
			my $isempty = (scalar(grep { -e $path.$_ } readdir($dirhandle)) - 2) == 0;
			closedir($dirhandle);
			
			#IF IT DOES, IT'S NOT EMPTY, WE CAN'T DELETE ANY MORE
			if(!$isempty) { print_message(LOGLEVEL_TRACE, "Folders no longer empty at '$path'; leaving alone.", LOG_TAG); last; }
			rmdir $path;
		}
		
		
		$directorypath = $dp;
	}
}

#----------------------------------------------------------------
#	getCheckSum
#----------------------------------------------------------------

sub getCheckSum{
	my ($src, $ckmethod) = @_;
	
	#DEFAULT TO OLD CHECKSUM IF NOT TOLD OTHERWISE
	$ckmethod = "cksum" if !defined $ckmethod;
	my $checksum = 0;
	
	#SINGLE QUOTES NEED A SPECIAL ESCAPING, BUT ALL ELSE SHOULD BE ALRIGHT
	$src = escape_single_quotes( $src );

	#STANDARD CHECKSUM
	if($ckmethod eq "cksum")
	{
		my $cmd = `cksum '$src'`;
		chomp($cmd);
		
		$checksum = $1 if $cmd =~ m/^([0-9a-fA-F]*) .*$/;
	}
	#MD5 CHECKSUM
	elsif($ckmethod eq "md5")
	{
		my $md5util = `which md5 2>/dev/null`; chomp($md5util);
		my $md5sumutil = `which md5sum 2>/dev/null`; chomp($md5sumutil);
		
		#IF md5sum DOESN'T EXIST, try md5
		print_message(LOGLEVEL_TRACE, "md5sum Utility is: $md5sumutil", LOG_TAG);
		print_message(LOGLEVEL_TRACE, "md5 Utility is: $md5util", LOG_TAG);
		my $cmd = $md5sumutil ? "$md5sumutil '$src'" : "$md5util -q '$src'";
		$cmd .= " 2>/dev/null";
		print_message(LOGLEVEL_TRACE, "Command is: '$cmd'", LOG_TAG);
		$cmd = `$cmd`;
		print_message(LOGLEVEL_TRACE, "Output is: '$cmd'", LOG_TAG);
		
		chomp($cmd);
		$checksum = $1 if $cmd =~ m/^([0-9a-fA-F]{32})/;
	}
	
	return $checksum;
}

#----------------------------------------------------------------
#	getmd5CheckSum(filename) : Alias to getCheckSum(filename, "md5")
#----------------------------------------------------------------
sub getmd5CheckSum
{
	return getCheckSum($_[0], "md5");
}

#----------------------------------------------------------------
#	unlinkFile
#----------------------------------------------------------------
sub unlinkFile{
	my ($src) = @_;
	
	return 0 unless -e $src;
	unlink($src);
	
	# check if success
	if($?)
	{
		# force delete
		system("chmod 777 '$src'; rm '$src'");
		return 1 if -e $src
	}
	
	return 0;
}

#----------------------------------------------------------------
#	dirLength
# 	return number of visible files in dir
#----------------------------------------------------------------
sub dirLength(){
	my ($src) = $@;
	my $cnt = 0;

	opendir(DIRHANDLE, $src) or return -1;
	while(defined (my $filename = readdir(DIRHANDLE)))
	{
	    $cnt++ if $filename !~ /^\./;
	}
	closedir(DIRHANDLE);

	return $cnt;
}


sub make_ordinal
{
	my ($number) = @_;
	
	return $number . "th" if $number >= 10 && $number <= 20;
	
	my $ordinals = { 1 => 'st', 2 => 'nd', 3 => 'rd' };
	$number =~ m/(\d)$/;
	
	return $number . ($ordinals->{$1} || 'th');
}

###################################################################
## prettifyBytecount(bytecount)
## ---------------------------------
## Return a human-readable version of the bytecount
###################################################################
sub human_bytes
{
	my $val = shift || 0;
	
	my $out = "b";
	if($val >= 1024) { $out = "kb"; $val /= 1024; }
	if($val >= 1024) { $out = "mb"; $val /= 1024; }
	if($val >= 1024) { $out = "gb"; $val /= 1024; }
	
	return (int($val * 100) / 100) . $out;
}

sub human_time
{
	use integer;
	my $val = shift || 0;

	return "under a second" if $val <= 0;

	my $out = "";
	my @label = ( " sec", " min", " hr" );
	my @scale = ( 60, 60, 24 );

	for ( my $i = 0; $i < 3; $i++ ) {
		my $part = $val % $scale[ $i ];
		if ( $part == 1 ) {
			$out = $part . $label[ $i ] . ($out ? ", " : "") . $out;
		} elsif ( $part > 1 ) {
			$out = $part . $label[ $i ] . "s" . ($out ? ", " : "") . $out;
		}
		$val /= $scale[ $i ];
	}
	# anything left is days
	if($val > 0) { $out = ($val) . " day" . (($val) != 1 ? "s" : "") . ($out ? ", " : "") . $out; }
	return $out;
}

sub approximate_time
{
	use integer;
	my $val = shift || 0;
	
	return "under 1 sec" if $val <= 0;
	return "a few seconds" if $val <= 15;
	return "under 1 min" if $val < 60;
	
	$val /= 60;
	return "$val min" . ($val != 1 ? 's' : '') if $val < 60;
	
	$val /= 60;
	return "$val hour" . ($val != 1 ? 's' : '') if $val < 24;
	
	return "over a day";
}

sub separate_number
{
	my $val = shift || 0;
	
	
	$val =~ m/^(\d*)(\.\d+)?/;
	my $majorpart = $1;
	my $minorpart = $2;
	
	$majorpart =~ s/(.*)(\d)(\d\d\d)/$1$2,$3/g;
	
	return "$majorpart$minorpart";
}



sub add_slash
{
	my ($text, $positions) = @_;
	
	return "" if !$text;
	
	$positions = uc($positions);
	
	$text = "/$text"	if $positions =~ m/(BEGIN|BOTH)/ && $text !~ m/^\//;
	$text .= "/"			if $positions =~ m/(END|BOTH)/ && $text !~ m/\/$/;
	
	return $text;
}

sub strip_slash
{
	my ($text, $positions) = @_;
	
	return "" if !$text;
	
	$positions = uc($positions);
	
	$text = substr($text, 1)		if $positions =~ m/(BEGIN|BOTH)/ && $text =~ m/^\//;
	$text = substr($text, 0, -1) if $positions =~ m/(END|BOTH)/ && $text =~ m/\/$/;
	$text =~ s/[\/\\]{2,}/\//g if $positions =~ m/(MULTIPLES)/;
	
	return $text;
}


sub combine_paths
{
	my ($root, @subdirs) = @_;
	
	$root = add_slash($root, 'BOTH');
	foreach my $subdir (@subdirs)
	{
		next if !$subdir;
		
		$subdir = add_slash($subdir, 'END') if $subdir !~ m/\.[^\/]*$/;
		$subdir = strip_slash($subdir, 'BEGIN');
		$root .= $subdir;
	}
	
	print_message(LOGLEVEL_TRACE, "Combined " . (scalar(@subdirs) + 1) . " paths into '$root'", LOG_TAG);
	return $root;
}





sub curl
{
	my ($url) = @_;
	
	##--- Get studio information from the JRS
	my $curl = "curl -f -m 0 -sS '$url'";
	print_message(LOGLEVEL_DEBUG, "Executing curl: $curl\n", LOG_TAG);
	
	## Curl Failed
	my $response = "";
	eval { $response = execute($curl); };
	if($@)
	{
		print_message(LOGLEVEL_ERROR, "Couldn't retrieve studio information ($@)", LOG_TAG);
		return 0;
	}
	
	return $response;
}



## --<<< >>>--
## --<<< Single-quoted strings are used with shells, but single quotes don't escape normally >>>--
sub escape_single_quotes
{
	my $filename = shift || "";
	$filename =~ s/'/'\\''/g;
	return $filename;
}


sub is_safe_path
{
	my $filename = shift || "";
	
	## Don't even consider first-level directories
	return 0 if $filename !~ m/^(\/?[^\/]+)\//;
	
	## Make sure the first directory isn't one of the *nix system directories
	my $firstdir = $1;
	return 0 if $firstdir =~ m/^\/?(etc|bin|boot|cgroup|dev|lib|lib64|media|misc|mnt|net|opt|proc|root|sbin|selinux|srv|symlinks|sys|usr|var)/;
	
	## Make sure we've either got group or user permissions
    my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size, $atime,$mtime,$ctime,$blksize,$blocks) = stat($1);
	return 2 if (($uid eq getpwuid($<)) && (($mode & 0600) == 0600)) ||
				(($gid eq getgrgid($()) && (($mode & 0060) == 0060)) ||
				(($mode & 0006) == 0006);
	
	## Still a valid path, just in case we want to raise permissions
	return 1;
}



########
## ONLY EXECUTE COMMANDS IF WE'RE NOT IN HARMLESS MODE. TRACE THE INTENDED COMMAND EITHER WAY
########
sub execute
{
	my $command = escape_single_quotes(join(' ', @_) || "");
	
	
	## I HAD A REASON FOR EXECUTING IN AN EXPLICIT SESSION, BUT I DON'T REMEMBER
	## WHAT IT WAS OFFHAND. I THINK cd DIDN'T WORK PROPERLY IN THE MIGRATION SCRIPT
	## (WHERE THIS WAS TAKEN FROM)
	## N.B. ALSO, MANUALLY STATE THE CHARACTERSET, BECAUSE APACHE SHELLS DEFAULT TO POSIX,
	## WHERE-AS MANUAL SHELLS USE THEIR OWN NATIVE ONES
	print_message(LOGLEVEL_TRACE, "Executing: \"sh -c '$command'\"", LOG_TAG);
	my $retval = `LANG=en_GB.UTF-8 sh -c '$command' 2>/dev/null`;
	
	my $errorcode = $? >> 8;
	my $short = $command; $short =~ s/^(\w+).*$/$1/;
	print_message(LOGLEVEL_FAILURE, "'$short' failed (error code '$?')", LOG_TAG) if $errorcode == 1;
	print_message(LOGLEVEL_FAILURE, "Missing keyword in command", LOG_TAG) if $errorcode == 2;
	print_message(LOGLEVEL_FAILURE, "Permission/execution problem", LOG_TAG) if $errorcode == 126;
	print_message(LOGLEVEL_FAILURE, "Command not found", LOG_TAG) if $errorcode == 127;
	print_message(LOGLEVEL_FAILURE, "Command manually terminated (Ctrl+C)", LOG_TAG) if $errorcode == 130;
	print_message(LOGLEVEL_FAILURE, "Invalid exit status", LOG_TAG) if $errorcode == 255;
	
	chomp($retval);
	$retval =~ s/^ *(.*) *$/$1/g;
	print_message(LOGLEVEL_TRACE, "Returned with value '$retval'", LOG_TAG);
	
	return $retval;
}

sub parse_structure
{
	my ($array, $fieldlist) = @_;
	my @fields = split(/ /, $fieldlist);
	
	my %tree;
	foreach my $element (@$array)
	{
		my $branch = \%tree;
		for(my $i = 0; $i < scalar(@fields); $i++)
		{
			my $name = 'e' . $element->{$fields[$i]};
			$branch->{$name} = ($i + 1 == scalar(@fields)) ? [] : {} if !defined $branch->{$name};
			$branch = $branch->{$name};
		}
		
		push(@$branch, $element);
	}
	
	return dissolve_structure(\%tree);
}

sub dissolve_structure
{
	my ($hash) = @_;
	
	my @array;
	foreach my $key (reverse(sort(keys(%$hash))))
	{
		if(ref $hash->{$key} eq "HASH")		{ push(@array, dissolve_structure($hash->{$key}) ); }
		elsif(ref $hash->{$key} eq "ARRAY")	{ push(@array, $hash->{$key}); }
	}
	
	return \@array;
}

1;
