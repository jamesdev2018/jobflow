#!/usr/local/bin/perl





####	############################################################	##
####	############################################################	##
## A LOGGING SYSTEM THAT SUPPORTS MULTIPLE OUTPUTS (EXPOSURES)	##
##		THAT RESPOND ONLY TO GIVEN LOGGING LEVELS 				##
####	############################################################	##
####	############################################################	##



package Logger;
our $VERSION = 2.9;		#VERSION STRING FOR PRE-5.10 PERL VARIANTS

use strict;
use warnings;
use utf8;
use Encode;

use threads;
use threads::shared;

use constant LOGLEVEL_TRACE		=> 0x01;					#PROGRAMMER STUFF THAT NORMAL PEOPLE SHOULD NEVER SEE
use constant LOGLEVEL_MARK		=> 0x02;					#PROGRAMMER TIMESTAMPS FOR BENCHMARKING CODE SECTIONS
use constant LOGLEVEL_DEBUG		=> 0x04;					#DEBUG INFORMATION FOR USERS WHO HAVE PROBLEMS
use constant LOGLEVEL_TEST		=> 0x08;					#CODE MARKED AS A SPECIFIC, TEMPORARY TEST
use constant LOGLEVEL_INFO		=> 0x10;					#GENERAL INFORMATION FOR EVERYONE
use constant LOGLEVEL_WARNING	=> 0x20;					#ERRORS THAT DON'T REALLY BREAK MUCH
use constant LOGLEVEL_ERROR		=> 0x40;					#ERRORS THAT DO BREAK THINGS, BUT CAN BE RECOVERED FROM
use constant LOGLEVEL_FAILURE	=> 0x80;					#ERRORS THAT DO BREAK THINGS, BUT CAN BE RECOVERED FROM AND WE WANT TO THROW AN EXCEPTION
use constant LOGLEVEL_FATAL		=> 0x100;				#ERRORS THAT CAN'T BE RECOVERED FROM AND WE WANT TO EXIT; USE SPARINGLY
use constant LOGLEVEL_SPECIFIC	=> 0x1000;				#DON'T ALSO REPORT ERRORS MORE IMPORTANT THAN THOSE SPECIFIED
use constant LOGLEVEL_SPECIAL_1	=> 0x2000;
use constant LOGLEVEL_SPECIAL_2	=> 0x4000;
use constant LOGLEVEL_SPECIAL_3	=> 0x8000;
use constant LOGLEVEL_SPECIAL_4	=> 0x10000;
use constant LOGLEVEL_SPECIAL_5	=> 0x20000;


use constant MODE_DEFAULT	=> 0x0;
use constant MODE_NORMAL		=> 0x1;
use constant MODE_ADVANCED	=> 0x2;
use constant MODE_HTML		=> 0x4;
use constant MODE_HOOKED		=> 0x8;
use constant MODE_PLAIN		=> 0x10;

use constant TIMER_NORMAL	=> 0x0;
use constant TIMER_HIRES		=> 0x1;						#HIGH RESOLUTION TIMER


#EXPORT SOME VALUES SO PEOPLE DON'T HAVE TO PUT "Logger::" IN FRONT OF /EVERYTHING/
use base 'Exporter';

our @EXPORT = qw(	print_message
					loadModule requireModule script_path
					LOGLEVEL_TRACE LOGLEVEL_DEBUG LOGLEVEL_TEST LOGLEVEL_INFO LOGLEVEL_WARNING LOGLEVEL_ERROR LOGLEVEL_FAILURE LOGLEVEL_FATAL LOGLEVEL_SPECIFIC
						LOGLEVEL_SPECIAL_1 LOGLEVEL_SPECIAL_2 LOGLEVEL_SPECIAL_3 LOGLEVEL_SPECIAL_4 LOGLEVEL_SPECIAL_5
					MODE_NORMAL MODE_ADVANCED MODE_HTML MODE_HOOKED MODE_PLAIN
					TIMER_NORMAL TIMER_HIRES );

our @EXPORT_OK = ('LOGLEVEL_TRACE', 'LOGLEVEL_DEBUG', 'LOGLEVEL_INFO', 'LOGLEVEL_TEST', 'LOGLEVEL_WARNING', 'LOGLEVEL_ERROR', 'LOGLEVEL_FAILURE', 'LOGLEVEL_FATAL', 'LOGLEVEL_SPECIFIC',
					'LOGLEVEL_SPECIAL_1', 'LOGLEVEL_SPECIAL_2', 'LOGLEVEL_SPECIAL_3', 'LOGLEVEL_SPECIAL_4', 'LOGLEVEL_SPECIAL_5');
our %EXPORT_TAGS = ( loglevels => [ 'LOGLEVEL_TRACE', 'LOGLEVEL_DEBUG', 'LOGLEVEL_TEST', 'LOGLEVEL_INFO', 'LOGLEVEL_WARNING', 'LOGLEVEL_ERROR', 'LOGLEVEL_FAILURE', 'LOGLEVEL_FATAL', 'LOGLEVEL_SPECIFIC' ],
					 logmodes => [ 'MODE_NORMAL', 'MODE_ADVANCED', 'MODE_HTML', 'MODE_HOOKED', 'MODE_PLAIN' ],
					 timermodes => [ 'TIMER_NORMAL', 'TIMER_HIRES' ],
					 moduleLoaders => [ 'loadModule', 'requireModule' ],
					 output	=> [ 'print_message' ]);

					
					
					
					
my $failurecode = 1;										#DEFAULT EXIT VALUE WHEN FATAL ERRORS OCCUR
my $LOG_TAG = "log";										#LOG TAG FOR THIS MODULE
my $LOG_BASE = "#";
my $lastrecordedtime = time();							#TIMESTAMPS FOR LOGLEVEL_MARK RECORDS
my $retval = 0;
my $callback_routine;

my $midmessage = 0;
my $threadlock:shared = 0;


use Scalar::Util qw( openhandle );
use Time::HiRes qw(gettimeofday);
# use Term::ReadKey;										#TO DETERMINE TERMINAL WIDTH, IF THE PACKAGE IS AVAILABLE (BETTER FORMATTING OF LOG OUTPUT)
use Term::ANSIColor;										#LOAD OUTPUT FONT STYLE MODULES. FALLBACK TO NON-COLOUR MODE IF ANSIColor IS NOT AVAILABLE
use Term::Cap;
use POSIX;
use File::Basename;


my ($start_sec, $start_min, $start_hour, $start_mday, $start_mon, $start_year) = localtime();

#SET STDOUT AND STDERR AS DEFAULT EXPOSURES
my @exposures;
my $local_exposure_handles;
share(@exposures);
add_exposure("stdout", LOGLEVEL_INFO | LOGLEVEL_WARNING | LOGLEVEL_SPECIFIC, MODE_ADVANCED, ".*", TIMER_NORMAL);
add_exposure("stderr", LOGLEVEL_ERROR | LOGLEVEL_FAILURE | LOGLEVEL_FATAL | LOGLEVEL_SPECIFIC, MODE_ADVANCED, ".*", TIMER_NORMAL);



END
{
	clear_exposures();
}

sub set_callback_routine
{
	my ($routine) = @_;
	
	$callback_routine = $routine if defined $routine && ref $routine eq "CODE";
}

###################################################################
## set_modifier(modifiers, [outputhandle])
## ---------------------------------
## Applies any colour/bold/underline modifiers to a terminal output
## (defined by outputhandle being STDOUT or STDERR), using
## Term::ANSIColor, falling back to Term::Cap/Posix or just not
## applying anything depending on packages installed.
###################################################################
sub modify_message
{
	my ($mode, $modifiers, $outputhandle, $message) = @_;
	
	return if ($mode & MODE_PLAIN) == MODE_PLAIN;
	if(($mode & MODE_HTML) == MODE_HTML)
	{
		my $styles = "";
		
		$modifiers =~ s/yellow/orange/;
		$modifiers =~ s/cyan/blue/;
		foreach(split(/ /, $modifiers))
		{
			if(lc($_) 	 eq "reset")		{  }
			elsif(lc($_) eq "normal")	{ $styles .= "font-weight: normal; "; }
			elsif(lc($_) eq "bold")		{ $styles .= "font-weight: bold; "; }
			elsif(lc($_) eq "underline")	{ $styles .= "text-decoration: underlined; "; }
			
			elsif(lc($_) =~ m/^(black|red|green|orange|blue|magenta|cyan|white)$/) { $styles .= "color: $1; "; }
			elsif(lc($_) =~ m/^on_(black|red|green|orange|blue|magenta|cyan|white)$/) { $styles .= "background-color: $1; "; }
		}
		
		$message =~ s/>/&gt;/g;
		$message =~ s/</&lt;/g;
		$message =~ s/ /&nbsp;/g;
		$message =~ s/\n/<br>/mg;
		
		return "<span style=\"$styles\">$message</span>";
	}
	else
	{
		my $ANSIcode = "";
		
		$outputhandle = *STDOUT unless defined $outputhandle;
		return $message unless $outputhandle eq *STDOUT || $outputhandle eq *STDERR;
	
		#JUST USE Term::ANSIColor IF WE HAVE IT [MAKE THE PIPE HOT SO STUFF IS IMMEDIATELY APPLIED]
		eval 	{ $ANSIcode = color($modifiers); };
	
		#OTHERWISE, TRY TO USE Term::Cap
		if($@)
		{
			eval
			{
				my $termios = new POSIX::Termios;
				$termios->getattr;

				my $ospeed = $termios->getospeed();
				my $t = Tgetent Term::Cap { TERM => undef, OSPEED => $ospeed };
				my ($normal, $underline, $bold) = map { $t->Tputs($_,1) } qw/me md us/;
			
				foreach (split(/ /, $modifiers))
				{
					if(lc($_) 	 eq "reset")		{ $ANSIcode = $normal; }
					elsif(lc($_) eq "normal")	{ $ANSIcode = $normal; }
					elsif(lc($_) eq "bold")		{ $ANSIcode = $bold; }
					elsif(lc($_) eq "underline")	{ $ANSIcode = $underline; }
				}
			};
			#OTHERWISE WE CAN'T USE FONT MODIFIERS
			return $message if $@;
		}
		
		return ($ANSIcode || "") . ($message || "");
	}
}


sub script_path
{
	my $cp = (caller)[1];
	$cp = "./$cp" unless $cp =~ m/[\/\\]/;
	
	#STRIP THE SCRIPT NAME FROM THE PATH
	$cp =~ m/^(.*[\/\\]+)[^\/\\]*$/;
	$cp = $1;
	
	return $cp;
}

sub logged_error
{
	return $retval;
}



###################################################################
## add_exposure(filename, reportlevel)
## ---------------------------------
## Add an exposure to catch anything described by /reportlevel/.
## Reportlevel is an or'd list of LOGLEVEL_* values. If LOGLEVEL_SPECIFIC
## is specified, only those levels match, otherwise any level equal
## or higher matches those provided.
## Filename is either the file to log the messages to, or STDOUT or
## STDERR
###################################################################
sub add_exposure
{
	my ($filename, $reportlevels, $mode, $filter, $timermode) = @_;
	
	$filename = $filename || $0;
	my $originalname = $filename;
	
	
	##---  UPDATE THE TIME IF THE DATE HAS TICKED OVER
	my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime();
	if(($mday != $start_mday) || ($mon != $start_mon) || ($year != $start_year))
	{
		($start_sec, $start_min, $start_hour, $start_mday, $start_mon, $start_year) = ($sec, $min, $hour, $mday, $mon, $year);
	}
	
	
	
	
	my ($handle, $inode);
	
	if($filename eq "stdout")		{ $handle = *STDOUT; }
	elsif($filename eq "stderr")		{ $handle = *STDERR; }
	elsif(($mode & MODE_HOOKED) == MODE_HOOKED)
	{
		$filename = "";
		$originalname = "";
	}
	else
	{
		##--- ENSURE THE BASE DIRECTORY EXISTS
			#my($logfile, $logdir) = fileparse($filename);
			my $logdir = $filename . "/";
			my $logfile = "";
			
			## CREATE A FOLDER BASED ON THE DATE
			my $date = sprintf("%04d-%02d-%02d", $start_year + 1900, $start_mon + 1, $start_mday);
			$logdir .= "$date/";
			
			## SINGLE QUOTES NEED A SPECIAL ESCAPING, BUT ALL ELSE SHOULD BE ALRIGHT
				my $escaped_logdir = "";
				while($logdir =~ m/^(.*?)'(.*)$/)
				{
					$escaped_logdir .= "$1'\\''";
					$logdir = $2;
				}
				$escaped_logdir = "$escaped_logdir$logdir";
			
			`mkdir -p '$escaped_logdir'` if $escaped_logdir;
		
		
		##--- GET A UNIQUE LOGNAME DEDICATED TO THIS PID
			$logfile .= sprintf("$$\_\_%02d:%02d", $start_hour, $start_min);
			my $logsize = -s "$logdir$logfile" || 0;
			if($logsize >= 5 * 1024 * 1024)
			{
				my $count = 1;
				while(-e "$logdir$logfile" . "_" . ++$count)
				{
					my $logsize = -s "$logdir$logfile\_$count" || 0;
					last if $logsize < 5 * 1024 * 1024;
				};
				$logfile .= "_$count";
			}
			$filename = "$logdir$logfile";
			
			
		##--- OPEN THE FILE FOR WRITING (APPEND OR NEW)
			my $exists = -e $filename;
			open $handle, $exists ? ">>" : ">", $filename or print_message(LOGLEVEL_ERROR, "Couldn't open '$filename' for append access", $LOG_TAG);
			$inode = (stat($filename))[1] || -1;		## TO TEST IF THE LOG FILE HAS BEEN DELETED
			
			## TURN OFF BUFFERING
			my $oldh = select($handle);
			$| = 1;
			select($oldh);
			
			## PRINT A SEPARATOR IF WE'RE MID-LOGFILE
			if($exists)
			{
				my @months = ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec");
				my $time = $mday . ($mday % 10 == 1 ? "st" : $mday % 10 == 2 ? "nd" : $mday % 10 == 3 ? "rd" : "th") . " " . $months[$mon] . ", " . ($year + 1900) . " \@ $hour:$min:$sec";
				my $text = "\n\n\n--------======== NEW LOG [$time] ========--------\n";
				$text =~ s/\n/<br>\n/mg if $mode == MODE_HTML;
				print $handle $text;
				print "File printed to\n";
			}
			else
			{
				print "Opened a new file\n";
			}
	}
	
	
	binmode $handle, ":utf8" if $handle;
	my %exposure:shared = (		file_descriptor	=> $handle ? fileno $handle : undef,
								name		=> $originalname,
								filename=> $filename,
								level	=> $reportlevels || LOGLEVEL_INFO,
								mode		=> $mode || MODE_NORMAL,
								filter	=> $filter || ".*",
								timer	=> $timermode || TIMER_NORMAL,
								inode	=> $inode,
								date		=> "$mday-$mon-$year",
								tlock	=> 1,
						);
	
	$local_exposure_handles->{$originalname} = $handle if $originalname !~ m/^std(out|err)$/;
	
	##--- UPDATE THE EXPOSURE, OR ADD A NEW ONE
	my $found = 0;
	foreach(@exposures)
	{
		if($_->{name} eq $originalname)
		{
			$_ = \%exposure;
			$found = 1;
			last;
		}
	}
	
	push(@exposures, \%exposure) unless $found;
}



sub roll_logfile
{
	my ($filename) = @_; 
	my @fileinfo;
	
	#IGNORE IF WE'RE NOT DEALING WITH A FILE, OR IF THE FILE IS LESS THAN 5MB BIG,
	#OR WAS CREATED ON A DIFFERENT DAY TO TODAY
	return if $filename eq "stdout";
	return if $filename eq "stderr";
	return if $filename = undef;
	
	## GET THE DATE
	my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime();
	
	
	##--- FIND OUR EXPOSURE AND TEST IT
	foreach my $e (@exposures)
	{
		next unless $e->{name} eq $filename;
		my @fileinfo = stat($e->{filename});
		
		my $handle = $e->{handle};
		my $filesize = -s $e->{filename} || 0;
		
		
		## IF THE FILE HAS CHANGED IN ANOTHER THREAD, DUPLICATE THE NEW HANDLE
		## i.e. MAKE SURE WE'RE NOT WRITING TO THE OLD FILE
		if(fileno $local_exposure_handles->{$filename}) {
			if($e->{file_descriptor} ne fileno $local_exposure_handles->{$filename})
			{
				close $local_exposure_handles->{$filename};
				open $local_exposure_handles->{$filename}, ">&=" . $e->{file_descriptor};
			}
		}
		
		## CHECK OUR EXPOSURE STILL EXISTS, IS WITHIN DATE AND FILESIZE
		return if	($filesize < 5 * 1024 * 1024) &&
					($e->{inode} eq ($fileinfo[1] || -1)) &&
					($e->{date} eq "$mday-$mon-$year");
		
		## IT ISN'T - RECREATE THE EXPOSURE
		add_exposure($filename, $e->{level}, $e->{mode}, $e->{filter}, $e->{timer});
		last;
	}
}





###################################################################
## increase_exposure(filename, reportlevel)
## ---------------------------------
## Add LOGLEVEL_* values to a given exposure, specified by filename.
## See add_exposure for a definition of reportlevel
###################################################################
sub increase_exposure
{
	my ($filename, $reportlevels, $mode, $filter, $timer) = @_;
	
	foreach(@exposures)
	{
		if($_->{name} eq $filename)
		{
			$_->{level} |= ($reportlevels || 0);
			$_->{mode} = $mode || $_->{mode};
			$_->{filter} = $filter || $_->{filter};
			$_->{timer} = $timer || $_->{timer};
		}
	}
}


###################################################################
## remove_exposure(filename)
## ---------------------------------
## Remove an exposure from the list. This will prevent it from
## receiving any more messages
###################################################################
sub remove_exposure
{
	my ($filename) = @_;
	
	my @newlist = ();
	foreach (@exposures)
	{
		close $_->{handle} if ($_->{name} eq $filename) && ($_->{name} !~ m/^std(out|err)$/);
		push(@newlist, $_) if $_->{name} ne $filename;
	}
	@exposures = @newlist;
	
	return undef;
}


###################################################################
## clear_exposures()
## ---------------------------------
## Remove all exposures. No messages will be caught or displayed
###################################################################
sub clear_exposures
{
	foreach(@exposures)
	{
		close $_->{handle} if $_->{name} ne "stdout"
								&& $_->{name} ne "stderr"
								&& $_->{handle};
	}
	
	@exposures = ();
}




sub set_log_base
{
	my ($base) = @_;
	
	$LOG_BASE = substr($base || "a", 0, 1);
}




###################################################################
## print_message(loglevel, message, tag)
## ---------------------------------
## Print /message/ to all exposures designed to catch /loglevel/ or
## below. /tag/ can be defined as an optional identifier for easier
## log parsing. FATAL messages will exit immediately, ERROR messages
## will call die (and can subsequently be caught with eval)
###################################################################
sub print_message
{
	my ($loglevel, $message, $tag) = @_;
	
	
		
	lock($threadlock);
	
	#IF NO LOGLEVEL, ASSUME LOGLEVEL_DEBUG
	unless($loglevel && $message && $tag)
	{
		$tag = $message;
		$message = $loglevel;
		$loglevel = LOGLEVEL_DEBUG;
	}
	
	#FIX UP THE MESSAGE AND TAG
	chomp($message);
	my $messagetext = $message;
	#$messagetext =~ s/\W{5,}/ /g;
	$tag = uc(length($tag) > 3 ? substr($tag, 0, 3) : $tag);
	
	#GET MORE DETAILED ERROR INFORMATION WITH STACK TRACE
	$messagetext .= "\n---" if $loglevel == LOGLEVEL_FATAL;
	my ($package, $filename, $line, $subroutine) = caller();
	if($loglevel >= LOGLEVEL_ERROR && $loglevel <= LOGLEVEL_FATAL)
	{
		$messagetext .= "\nError raised in package $package";
		my $c = 0;
		my @info;
		while(($package, $filename, $line, $subroutine) = caller($c++)) { push(@info, { package => $package, filename => $filename, line => $line, subroutine => $subroutine }); }
		
		my $m = scalar(@info) - 1;
		for($c = 0; $c <= $m; $c++)
		{
			my $line = (($c == $m) ? "|-" : "|-");
			my $i = $info[$m - $c];
			## Make sure perl core packages don't take up so much room (we can't do much about them anyway)
			$i->{filename} =~ s/^\.\///;
			if($i->{filename} =~ m/^(\/usr\/local\/perls\/perl-5\.20\..\/lib\/site_perl\/)(5\.20\..)(.*)$/)
			{
				next if $i->{filename} eq $info[$m - ($c + 1)]->{filename};
				$messagetext .= "\n   $line     __perl_core$3";
			}
			else
			{
				$messagetext .= "\n   $line $i->{filename}:$i->{line}" . ($info[$m - ($c - 1)]->{subroutine} ? " ($info[$m - ($c - 1)]->{subroutine})" : "");
			}
		}
		
		if($loglevel == LOGLEVEL_FATAL)
		{
			chomp($!) if $!;
			chomp($?) if $?;
			chomp($@) if $@;
		
		  	$messagetext .= "\nOS return value: $!" if $!;
		  	$messagetext .= "\nSystem call error: $?" if $?;
		  	$messagetext .= "\nEval error: $@" if $@;
		}
	}
	
	
	#GET THE THREAD INDEX IF LOG_BASE IS A HASH
	#VERY USEFUL FOR MULTI-THREADED DEBUGGING!
	my $identifier = $LOG_BASE;
	eval
	{
		require threads;
		threads->import("stringify");

		$identifier = threads->self() || "";
	} if $identifier eq "#";
	$identifier = "/$identifier" if $identifier;
	
	#CREATE OUTPUT MESSAGE
	my $output = " " . ($tag ne "" ? "[$tag" : "[...") . "$identifier]" . (" " x (5 - length($identifier)));
	$output .= ">>>---> " if $loglevel == LOGLEVEL_MARK;		#THIS IS A PRIVATE MARKER TO MAKE THE
															#TIMER STAND OUT
															
	$output .= " Trace: " if $loglevel == LOGLEVEL_TRACE;
	$output .= " Debug: " if $loglevel == LOGLEVEL_DEBUG;
	$output .= '@@TEST@ ' if $loglevel == LOGLEVEL_TEST;
	$output .= " Info:  " if $loglevel == LOGLEVEL_INFO;
	$output .= " WARN:  " if $loglevel == LOGLEVEL_WARNING;
	$output .= "*ERROR* " if $loglevel == LOGLEVEL_ERROR;
	$output .= "**FAIL**" if $loglevel == LOGLEVEL_FAILURE;
	
	$loglevel = LOGLEVEL_TRACE if $loglevel == LOGLEVEL_MARK;
	
	
	
	#FOR EACH OUTPUT CHANNEL (EXPOSURE)
	foreach (@exposures)
	{
		next if $_->{printing};
		$_->{printing} = 1;
		
		my $filter = $_->{filter};
		next if $tag !~ m/$filter/;
		
		my $filename = $_->{name};
		my $special = $_->{originalfilename};
		my $handle = $local_exposure_handles->{$filename} || '';
		$handle = *STDOUT if $filename eq "stdout";
		$handle = *STDERR if $filename eq "stderr";
		
		
		
		my $displaymessage = prettytime(undef, $_->{timer}) . $output;
		my $msgtxt = $messagetext;
		my $indent = " " x length($displaymessage);
		
		#ENSURE THE LOG FILE IS STILL WRITABLE
		if(($handle ne *STDOUT) && ($handle ne *STDERR) && $filename)
		{
			## MAKE SURE ONLY ONE THREAD DOES THIS BIT AT A TIME
			my $lock:shared = 0; lock($lock);
			roll_logfile($filename);
			$handle = $local_exposure_handles->{$_->{name}};
		}
		
		
		#ALLOW SPECIFICALLY ONE LEVEL, OR INCLUDE ANYTHING ABOVE IT
		if(($_->{level} & LOGLEVEL_SPECIFIC) == LOGLEVEL_SPECIFIC)
		{
			if(($_->{level} & $loglevel) != $loglevel)
			{
				$_->{printing} = 0;
				next;
			}
		}
		elsif($loglevel < $_->{level})
		{
			$_->{printing} = 0;
			next;
		}
		
		
		
		#FATAL ERROR HEADING
		if($loglevel == LOGLEVEL_FATAL)
		{
			$displaymessage = modify_message($_->{mode}, "bold red", $handle, "\n********") . 
							  modify_message($_->{mode}, "black bold on_red",		 $handle, "FATAL ERROR") .
							  modify_message($_->{mode}, "reset bold red", $handle, "********\n$displaymessage");
		}
		if(($_->{level} & LOGLEVEL_TRACE) == LOGLEVEL_TRACE)
		{
			my ($package, $filename, $line, $subroutine) = caller;
			$filename =~ s/^(\.\/)//g;
			$msgtxt .= modify_message($_->{mode}, "magenta", $handle, " $filename:$line");
		}
		
		
		
		#WRAP MESSAGES TO STDOUT/STDERR SO THE TERMINAL LOOKS NICE
		if(($handle eq *STDOUT || $handle eq *STDERR) && ($_->{mode} != MODE_HTML) && ($_->{mode} != MODE_PLAIN))
		{
			my $linewidth = get_terminal_width($_->{mode}) - (length($indent));
			my $shortwidth = $linewidth - 5;
			
			$msgtxt =~ s/(.{$shortwidth})([[:cntrl:]]\[\d\dm)/$1\n$2/g;
			$msgtxt =~ s/([^\n]{$linewidth})/$1\n/g;
			$msgtxt =~ s/(\n *)/\n/g;
		}
		
		#INDENT THINGS PROPERLY
		$msgtxt =~ s/(\n)/$1$indent/g;
		
		
		#COLOUR THE MAIN MESSAGE APPROPRIATELY
		if($loglevel == LOGLEVEL_FATAL)
		{ $msgtxt = modify_message($_->{mode}, "red",    $handle, $msgtxt); $displaymessage .= $msgtxt; }
		else
		{ $displaymessage .= $msgtxt; }
		
		if($loglevel == LOGLEVEL_FAILURE)	{ $displaymessage = modify_message($_->{mode}, "red bold",	$handle, $displaymessage); }
		if($loglevel == LOGLEVEL_ERROR)		{ $displaymessage = modify_message($_->{mode}, "red",		$handle, $displaymessage); }
		elsif($loglevel == LOGLEVEL_WARNING)	{ $displaymessage = modify_message($_->{mode}, "yellow", 	$handle, $displaymessage); }
		elsif($loglevel == LOGLEVEL_INFO)	{ $displaymessage = modify_message($_->{mode}, "",       	$handle, $displaymessage); }
		elsif($loglevel == LOGLEVEL_TEST)	{ $displaymessage = modify_message($_->{mode}, "magenta",	$handle, $displaymessage); }
		elsif($loglevel == LOGLEVEL_DEBUG)	{ $displaymessage = modify_message($_->{mode}, "cyan",   	$handle, $displaymessage); }
		elsif($loglevel < LOGLEVEL_DEBUG)	{ $displaymessage = modify_message($_->{mode}, "blue",   	$handle, $displaymessage); }
		
		
		#CLOSE THE FATAL ERROR BOX IF WE'VE GOT A FATAL ERROR
		$displaymessage .= modify_message($_->{mode}, "red", $handle, "\n***************************\n") if $loglevel == LOGLEVEL_FATAL;
		$displaymessage .= modify_message($_->{mode}, "reset", $handle, "\n");
		$displaymessage .= ($_->{mode} == MODE_HTML ? "\n" : "");
		
		if($filename eq "")					{ $callback_routine->($loglevel, $displaymessage) if $callback_routine; }
		elsif($handle) { if(fileno $handle)	{ print $handle $displaymessage; } }
		
		$_->{printing} = 0;
	}
	
	$retval = ($loglevel == LOGLEVEL_FATAL || $loglevel == LOGLEVEL_FAILURE) ? 1 : 0;
	
	#IF WE HAVE A FATAL ERROR, QUIT IMMEDIATELY (WITH A GIVEN FAILURECODE)
	exit $failurecode if $loglevel == LOGLEVEL_FATAL;
	
	#IF IT'S JUST A NORMAL ERROR, die AND LET ANY eval{}s CATCH IT
	die "$message\n" if $loglevel == LOGLEVEL_FAILURE;
}

sub print_mark
{
	my $init = shift || "mark";
	
	if($init eq "init")
	{
		print_message(LOGLEVEL_MARK, "Starting new code segment time check", $LOG_TAG);
	}
	else
	{
		my $delay = int(time() - $lastrecordedtime);
		print_message(LOGLEVEL_MARK, "Code segment took $delay seconds to run", $LOG_TAG);
	}
	
	$lastrecordedtime = time();
}


sub prettytime
{
	my ($localtime, $use_hires) = @_;
	
	my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime($localtime || time()); 
	
	my ($seconds, $microseconds) = gettimeofday();
	$microseconds =~ s/^(...)(.*)$/$1/;
	
	return sprintf("%02d:%02d:%02d.\%d", $hour, $min, $sec, $microseconds) if $use_hires;
	return sprintf("%02d:%02d:%02d", $hour, $min, $sec);
}





####################################################################
## display_data_quality(bytelength, @blockqualities)
## ---------------------------------
## Draws a visual representation of a chunk of data, using an array
## of hashes that include the block position, size and validity
## blockquality{int offset, int size, textboolean validated}
## Think of the old defrag display :)
###################################################################
sub display_data_quality
{
	##DISABLE THIS FUNCTION UNLESS WE WANT IT
	#return;
	
	
	my $linewidth = get_terminal_width() - 24;		#APPROXIMATE WIDTH FOR A FULL-SCREEN TERMINAL LINE
													#TAKING INTO ACCOUNT ROUGHLY THE EXTRA INFO
	
	my ($filesize, @blocklist) = @_;
	my $prevdatapoint = 0;
	
	my $hasgood = "false";
	my $hasbad = "false";
	my $goodsize = 0;
	
	my $buffer = "";
	
	
	for(my $i = 0; $i < $linewidth; $i++)
	{
		my $curdatapoint = int($filesize / $linewidth) * $i;
		
		$hasgood = "false";
		$hasbad = "false";
		$goodsize = 0;
		
		
		my $min = $prevdatapoint;
		my $max = $curdatapoint;
		my $used = "true";
		
		
		while($used eq "true")
		{
			#RUN THROUGH EACH VALID ITEM
			$used = "false";
			foreach (@blocklist)
			{
				if($_->{validated} eq "true")
				{
					#REDUCE MAX
					if(($_->{offset} < $max) && (($_->{offset} + $_->{size}) >= $max))
					{
						$max = $_->{offset};
						$used = "true";
					}
					
					#RAISE MIN
					if(($_->{offset} <= $min) && (($_->{offset} + $_->{size}) > $min))
					{
						$min = $_->{offset} + $_->{size};
						$used = "true";
					}
				}
			}
			
			#IF MAX AND MIN OVERLAP, ESCAPE
			if($min >= $max) { $used = "false"; }
		}
		
		
		if($min >= $max) { $buffer .= "#"; }
		elsif($min == $prevdatapoint && $max == $curdatapoint) { $buffer .= "_"; }
		elsif($min >= $prevdatapoint || $max <= $curdatapoint) { $buffer .= "'"; }
		else { $buffer .= " "; };
		
		$prevdatapoint = $curdatapoint;
	}
	
	$buffer .= " #=" . int($filesize / $linewidth) . " bytes";
	
	$goodsize = 0;
	foreach(@blocklist) { if($_->{validated} eq "true") { $goodsize += $_->{size}; } }
	if($filesize != $goodsize) { $buffer .= "!$goodsize:$filesize!"; }
	else		{	$buffer .= "  :)";	}
	
	$buffer .= "\n";
	
	return $buffer;
}



###################################################################
## prettifyBytecount(bytecount)
## ---------------------------------
## Return a human-readable version of the bytecount
###################################################################
sub prettify_byte_count
{
	my $val = shift || 0;
	
	my $out = "b";
	if($val >= 1024) { $out = "kb"; $val /= 1024; }
	if($val >= 1024) { $out = "mb"; $val /= 1024; }
	if($val >= 1024) { $out = "gb"; $val /= 1024; }
	
	return (int($val * 100) / 100) . $out;
}

sub discern_byte_count
{
	my $text = shift || "0b";
	
	$text =~ m/^(\d+)(\.\d+)?([KkMmGgTtPp]?)[Bb]?$/;
	my $value = ($1 || "1") . ($2 || "");
	my $multiplier = lc($3 || "");
	
	$value *= 1024 if $multiplier eq "k";
	$value *= 1024 * 1024 if $multiplier eq "m";
	$value *= 1024 * 1024 * 1024 if $multiplier eq "g";
	$value *= 1024 * 1024 * 1024 * 1024 if $multiplier eq "t";
	$value *= 1024 * 1024 * 1024 * 1024 * 1024 if $multiplier eq "p";
	
	return int($value);
}

sub prettify_second_count
{
	use integer;
	my $val = shift || 0;

	return "under a second" if $val <= 0;

	my $out = "";
	my @label = ( " sec", " min", " hr" );
	my @scale = ( 60, 60, 24 );

	for ( my $i = 0; $i < 3; $i++ ) {
		my $part = $val % $scale[ $i ];
		if ( $part == 1 ) {
			$out = $part . $label[ $i ] . ($out ? ", " : "") . $out;
		} elsif ( $part > 1 ) {
			$out = $part . $label[ $i ] . "s" . ($out ? ", " : "") . $out;
		}
		$val /= $scale[ $i ];
	}
	# anything left is days
	if($val > 0) { $out = ($val) . " day" . (($val) != 1 ? "s" : "") . ($out ? ", " : "") . $out; }
	return $out;
}

sub prettify_approximate_second_count
{
	use integer;
	my $val = shift || 0;
	
	return "under a second" if $val <= 0;
	return "under a minute" if $val < 60;
	
	$val /= 60;
	return "$val min" . ($val != 1 ? 's' : '') if $val < 60;
	
	$val /= 60;
	return "$val hour" . ($val != 1 ? 's' : '') if $val < 24;
	
	return "over a day";
}

sub prettify_number
{
	my $val = shift || 0;
	
	$val =~ m/^(\d*)(\.\d+)?/;
	my $majorpart = $1;
	my $minorpart = $2;
	
	$majorpart =~ s/(.*)(\d)(\d\d\d)/$1$2,$3/g;
	
	return "$majorpart$minorpart";
}






###################################################################
## set_failure_code(code), get_failure_code() => [code]
## ---------------------------------
## Set and get the return codes emmitted when FATAL errors are
## passed to the logging routine.
###################################################################
sub set_failure_code
{
	$failurecode = shift;
}

sub get_failure_code
{
	return $failurecode;
}


sub get_terminal_width
{
	my ($mode, $cols) = @_;
	
	## Default to 80 columns if we don't have a terminal set
	return 80 if !$ENV{TERM};
	
	$cols = 80 unless defined $cols;
	$mode = (!$^V or $^V lt v5.10.0) ? MODE_NORMAL : MODE_ADVANCED unless $mode;	#POSSIBLE HANGING PROBLEM WITH EARLIER PERLS
	
	return $cols if $mode != MODE_ADVANCED;
	
	$cols = 80; # eval { ($cols) = Term::ReadKey::GetTerminalSize(); };
	return $cols;
}



# must have this at the end for a require
1;
