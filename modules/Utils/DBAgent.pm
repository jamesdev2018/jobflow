#!/usr/local/bin/perl

package DBAgent;
our $VERSION = 1.4;		#VERSION STRING FOR PRE-5.10 PERL VARIANTS

use strict;
use warnings;

use Encode;
use Carp qw(carp cluck croak);

use base 'Exporter';
our @EXPORT = qw( get_row get_column get_default process_sql process_oneline_sql process_onerow_sql hashed_process_sql);

use Scalar::Util 'blessed';

use DBI;

# Home grown

use Logger;

use constant LOG_TAG => "DBA";

#GONNA HAVE TO FIGURE SOMETHING OUT WITH THIS.
#NOTE 2: GONNA HAVE TO FIGURE SOMETHING OUT WITH WHAT?
my $default_connection = "prime";
my $connections = { 	prime => {	database => "",
								data_source => "",
								db_username => "",
								db_password => "",
								db_force => 0,
								connection_timeout => 30,
								dbh => undef,
								last_query_time => 0,
								debug_mode => 0,
							},
		 		}	;
END { kill_connection(); }


sub new
{
	my ($class, $tag, $database, $data_source, $username, $password, $force, $timeout) = @_;
	
	$tag ||= '';
	$default_connection = $tag || $default_connection;
	return $connections->{$tag} if $connections->{$tag};
	
	my $self = {
		database				=> $database,
		data_source			=> $data_source,
		db_username			=> $username,
		db_password 			=> $password,
		db_force				=> $force,
		connection_timeout	=> 300,
		dbh					=> undef,
		last_query_time		=> 0,
	};
	bless $self, $class or print_message(LOGLEVEL_ERROR, "$!", LOG_TAG);
	
	$connections->{$tag} = $self;
	
	return $self;
}

sub enable_debug_messages
{
	my ($isblessed) = blessed($_[0]);
	my $info = $isblessed ? shift : $connections->{$default_connection};
	
	$info->{debug_mode} = 1;
}

sub disable_debug_messages
{
	my ($isblessed) = blessed($_[0]);
	my $info = $isblessed ? shift : $connections->{$default_connection};
	
	$info->{debug_mode} = 0;
}

#CREATE A NEW DATABASE CONNECTION WITH UPDATED SETTINGS
sub new_connection
{
	my $isblessed = blessed($_[0]);
	my $info = $isblessed ? shift : $connections->{$default_connection};
	my ($tag, $db, $dbsrc, $dbusr, $dbpass, $dbforce, $connectiontimeout) = @_;
	
	$info->{database} = $db if defined $db;
	$info->{data_source} = $dbsrc if defined $dbsrc;
	$info->{db_username} = $dbusr if defined $dbusr;
	$info->{db_password} = $dbpass if defined $dbpass;
	$info->{db_force} = $dbforce if defined $dbforce;
	$connectiontimeout = "" unless defined $connectiontimeout;
	$info->{connection_timeout} = $connectiontimeout if ($connectiontimeout ne "");
	
	if($isblessed) { $info->kill_connection(); } else { kill_connection(); }
	$info->{dbh} = DBI->connect("DBI:mysql:database=$info->{database};host=$info->{data_source}",
							$info->{db_username}, $info->{db_password},
							{ RaiseError => $info->{db_force} ? 0 : 1,
							  PrintError => 0,
							  mysql_enable_utf8 => 1,
							  AutoInactiveDestroy => 1,
							}) or return ($info->{db_force} ? 0 : 1);
	
	$info->{last_query_time} = time();
	
	#ENSURE UTF8 CAPABILITY; USEFUL FOR PERL8.8 INSTALLS
	if($isblessed)	{ $info->process_oneline_sql("SET NAMES UTF8"); }
	else 			{ process_oneline_sql("SET NAMES UTF8"); }
	
	return 0;
}

#FINISH A DATABASE CONNECTION
sub kill_connection
{
	my $isblessed = blessed($_[0]);
	my $info = $isblessed ? shift : $connections->{$default_connection};
	
	return unless defined $info->{dbh};
	$info->{dbh}->disconnect;
	$info->{dbh} = undef;
}

sub get_connection_settings
{
	my $isblessed = blessed($_[0]);
	my $info = $isblessed ? shift : $connections->{$default_connection};
	
	return {
				database => $info->{database},
				data_source => $info->{data_source},
				username => $info->{db_username},
				password => $info->{db_password},
				force => $info->{db_force},
				timeout => $info->{connection_timeout},
			};
}

# load_global_settings moved to JRSConfig.pm, MasterConfig.pm etc


#RETURN AN ARRAY (ROWS) OF AN ARRAY (COLUMNS) THAT WERE RETURNED FROM A SQL QUERY
sub process_sql
{
	my $isblessed = blessed($_[0]);
	my $info = $isblessed ? shift : $connections->{$default_connection};
	
	my $returnhash = $_[0] eq "hash";
	shift if $returnhash;
	
	my ($sqlstring, $arguments, $numcolumns) = @_;
	

	#RESET THE CONNECTION IF FIVE MINUTES HAVE PASSED SINCE OUR LAST TRANSACTION
	if((time() - $info->{last_query_time}) > $info->{connection_timeout})
	{
		$info->new_connection() if $isblessed;
		new_connection() unless $isblessed;
	}
	
	my @columnnames;
	my $fields = "";
	my @returncolumns;

	my $dbh = $info->{dbh};

	eval
	{
		#INPUT ARGUMENTS

		my $sth = $dbh->prepare($sqlstring);

		# $sth->{NUM_OF_FIELDS} not set until after execute, along with {NAMES}
		
		if ( ( ! defined $arguments ) || (( scalar @$arguments ) == 0) ) {
			$sth->execute();
		} else {
			$sth->execute(@$arguments);
		}
		if ( $sqlstring =~ /^\s*SELECT\s/ ) {
			#print "SQL: $sqlstring\n";
			#print "Number of columns after " . $sth->{NUM_OF_FIELDS} . "\n";
			#print "First column name: " . $sth->{NAME}->[0] . "\n" if ( exists $sth->{NAME} );
		}
	
		#AUTOGRAB THE COLUMN COUNT Only if standard process_sql call and no 3rd arg

		if ( ( ! defined $numcolumns ) && ! ( $returnhash ) )
		{
			## We can avoid a lot of parsing trouble by just collapsing brackets
			my $inflimit = 1000; # prevent potential infinite loop
			my $tempsql = $sqlstring;
			while ( $tempsql =~ m/^(.*)\([^(]*?\)(.*)$/s ) { 
				$tempsql = "$1$2"; 
				$inflimit--;
				last if $inflimit == 0;
			}
			$tempsql =~ s/\n/ /g;
			
			if($tempsql =~ m/^SELECT\s+(.*?)\s+FROM\s+.*/si)	{ $tempsql = $1; }
			elsif($tempsql =~ m/^SELECT\s+(.*)/si)		{ $tempsql = $1; }
			else { $tempsql = ""; }
			
			if($tempsql)
			{
				foreach (split(/, ?/, $tempsql))
				{
					$_ =~ s/^.* [aA][sS] (.*)$/$1/;
					$_ =~ s/^.*\.(.*)$/$1/;
					push(@columnnames, $_);
				}
				$numcolumns = scalar @columnnames;
			}
		}

		# For process_hashed_sql at least auto detect the column names
		# ps could do for the process_sql case, in future.
		# nb overrides any $numcolumns value passed

		if ( $returnhash ) {
			# Build column names dynamically ...
			$numcolumns = $sth->{NUM_OF_FIELDS} || 0;  # Assume the call was for something other than SELECT ... in which case NUM_OF_FIELDS is undef?
			for ( my $i = 0; $i < $numcolumns; $i++ ) {
				push( @columnnames, $sth->{NAME}->[ $i ] );
			}
		}

		# Maybe the next statement should also have be conditional on $sqlstatement =~ /^\s*SELECT/i;
		# as the first call is SET UTF8, and a SET statement shouldnt return values..
	
		if($numcolumns)
		{
			# ie there is something to return, rather than just execute the statement

			#REDIRECTION STUFF.
			#CREATE AN ARRAY OF AS MANY VARIABLES AS WE NEED (i.e. 1 FOR EACH COLUMN)
			my @vals = (0) x $numcolumns;
			#CREATE AN ARRAY OF REFERENCES TO THE ENTRIES IN THE FIRST ARRAY (i.e. FOR bind_columns)
			my @retvals = ();
			for(my $i = 0; $i < $numcolumns; $i++) { $retvals[$i] = \($vals[$i]); }
			
			#PASS THE ARRAY OF REFERENCES SO bind_columns CAN BIND TO THE ORIGINAL VARIABLES
			#...AND FILL THEM!
			$sth->bind_columns(@retvals);
			
			#GO THROUGH EACH RETURNED RECORD
			@returncolumns = ();
			while($sth->fetch())
			{
				#FILL AN ARRAY WITH THE DATA FILLED IN THE ORIGINAL VARIABLE LIST
				if($returnhash)
				{
					my $rv;
					for(my $colindex = 0; $colindex < $numcolumns; $colindex++)
					{
						$rv->{$columnnames[$colindex]} =${ $retvals[$colindex] };
					}
					push(@returncolumns, $rv);
				}
				else
				{
					my @rv = ();
					push(@rv, $$_) foreach (@retvals);
		
					#PUSH THE NEW ARRAY ONTO THE RETURN VARIABLE
					push(@returncolumns, \@rv);
				}
			}
		}
		
		$sth->finish();
	};
	
	if($@)
	{
		if(!$info->{db_force})
		{
			my $error = "Error: " . ($@ ? "$@ ($!)" : $!) . "\n";
			
			my $message = $sqlstring;
			if($arguments) {
				my @args = @$arguments;
				$message = get_resultant( $sqlstring, \@args );
			}
			$error .= "SQL was $message";
			print_message(LOGLEVEL_FAILURE, $error, LOG_TAG);
		}
		
		my @rv = (0) x ($numcolumns || 0);
		push(@returncolumns, \@rv);
	}
	elsif($info->{debug_mode} || 0)
	{
		my $message = $sqlstring;
		if($arguments) {
			my @args = @$arguments;
			$message = get_resultant( $sqlstring, \@args );
		}
		print_message(LOGLEVEL_INFO, $message . "\n(" . scalar(@returncolumns) . "x [" . join(', ', @columnnames) . "])<br>", LOG_TAG);
	}
	
	return \@returncolumns;		#NEEDS TO BE A REFERENCE SO WE CAN CORRECTLY PASS IT INTO get_row,
								#OR ELSE get_row CAN'T EACH ENTRY ISN'T JUST ANOTHER PARAMETER
}

# Substitute placeholders '?' with actual string values

sub get_resultant {
	my ( $sqlstring, $args ) = @_;

	my $message = $sqlstring;

	my $inflimit = 1000;
	while( $message =~ m/^(.*?)\?(.*)$/s ) {
		$message = $1 . '\'' . shift( @$args ) . '\'' . $2;
		$inflimit--;
		last if $inflimit == 0;
	}

	return $message;
}


#RETURN AN ARRAY (COLUMNS) OF A SQL QUERY THAT WOULD ONLY RETURN A SINGLE ROW
sub process_oneline_sql
{
	my $isblessed = blessed($_[0]);
	my $info = $isblessed ? shift : $connections->{$default_connection};
	my ($sqlstring, $arguments, $numcolumns) = @_;
	
	my $rows;
	$rows = $info->process_sql($sqlstring, $arguments, $numcolumns) if $isblessed;
	$rows = process_sql($sqlstring, $arguments, $numcolumns) unless $isblessed;
	
	my $row = get_row($rows);
	return undef unless defined $row;
	return @$row;
}

sub process_onerow_sql
{
	my $isblessed = blessed($_[0]);
	my $info = $isblessed ? shift : $connections->{$default_connection};
	my ($sqlstring, $arguments, $numcolumns) = @_;
	
	my $rows;
	$rows = $info->process_sql($sqlstring, $arguments, $numcolumns) if $isblessed;
	$rows = process_sql($sqlstring, $arguments, $numcolumns) unless $isblessed;
	
	my @values;
	foreach my $row (@$rows) { push(@values, @{ $row }[0]); }
	return @values;
}

sub hashed_process_sql
{
	my $isblessed = blessed($_[0]);
	my $info = $isblessed ? shift : $connections->{$default_connection};
	
	my @values = ("hash", @_);
	unshift(@values, $info) if $isblessed;
	my $retval = process_sql(@values);
	
	return undef unless ref @{ $retval }[0] eq "HASH";
	return $retval;
}

#GET AN ARRAY (COLUMNS) FROM A GIVEN ARRAY (ROWS) AND INDEX
sub get_row
{
	my ($rows, $index) = @_;
	$index = 0 unless defined $index;
	eval {
		croak "rows undef" unless ( defined $rows );
		croak "index undef" unless ( defined $index );
		croak "index > number of rows" if ( $index > ( scalar @$rows ) );
	};
	if ( $@ ) {
		cluck "DBAgent: get_row: $@";
	}

	return undef unless defined $rows;
	return undef if $index > scalar @$rows;
	my $r = @$rows[$index];
}

#GET A VALUE FROM AN ARRAY (COLUMNS) GIVEN AN INDEX
sub get_column
{
	my ($columns, $index) = @_;
	$index = 0 unless defined $index;
	
	return undef unless defined $columns;
	return undef if $index > scalar @$columns;
	return @$columns[$index];
}

sub get_default
{
	my ($value) = @_;
	my $default = -1;
	
	return $default unless defined $value;
	return $default if $value eq "";
	return $default unless $value >= 0;
	
	return $value;
}

sub make_update_list
{
	my ($values, $filterstring) = @_;
	$filterstring ||= ".*";
	
	my (@columns, @values);
	foreach my $key (keys %$values)
	{
		next if lc($key) !~ m/^($filterstring)$/;
		
		push(@columns, "$key=?");
		push(@values, $values->{$key});
	}
	
	return { columns => \@columns, values => \@values };
}

1;
