#!/usr/local/bin/perl





####	############################################################	##
####	############################################################	##
##   FILEHANDLE OBJECT THAT SUPPORTS AUTOMATICALLY WRITING TO A	##
##	TEMPORARY FILE IN ARBITRARY BLOCKS, WITH THE ADDED ABILITY	##
##	OF COLLABORATIVE WRITING TO A FILE WITH OTHER FILETRANSFER	##
##							OBJECTS								##
####	############################################################	##
####	############################################################	##




package TransferFile;
our $VERSION = 1.3;		#VERSION STRING FOR PRE-5.10 PERL VARIANTS

use strict;
use warnings;
use utf8;
use Encode;

use Logger ':loglevels';		#SIMPLE BUT FLEXIBLE DEBUG LOGGING AND OUTPUT
my $LOG_TAG = "FIL";


use Fcntl qw/ :seek /;
use File::Temp qw/ tempfile tempdir /;
use File::Copy;
use File::Path qw/ make_path remove_tree /;
use Encode qw/ encode /;
use Digest::MD5 qw/ md5_hex /;

use Utilities;


sub DESTROY
{
	my ($self) = @_;
	
	$self->close_file();
}


sub escape_filename
{
	my $filename = shift || "";
	
	#IF A BACKSLASH IS PRESENT, ASSUME ALREADY ESCAPED - THIS SHOULD PREVENT ACCIDENTAL DOUBLE-ESCAPING
	return $filename if $filename =~ m/\\/;
	
	$filename =~ s/\\/\\\\/g;
	$filename =~ s/\`/\\\`/g;
	$filename =~ s/\'/\'\\\'\'/g;
	$filename =~ s/\"/\\\"/g;
	$filename =~ s/\’/\\\’/g;
	
	return $filename;
}

sub escape_single_quotes
{
	my $filename = shift || "";
	
	#SINGLE QUOTES NEED A SPECIAL ESCAPING, BUT ALL ELSE SHOULD BE ALRIGHT
		my $newsrc = "";
		while($filename =~ m/^(.*?)'(.*)$/)
		{
			$newsrc .= "$1'\\''";
			$filename = $2;
		}
		
		return "$newsrc$filename";
}

###################################################################
## new (filename) --> [transferfileobject]
## ---------------------------------
## Create a new version of this class referring to a given file
###################################################################
sub new
{
	my $class = shift;
	
	my $self = {
		_filehandle => "",
		_originalname => shift || "", #escape_filename(shift),
		_tempname => "",
		_tempdir => "",
		_filesize => 0,
		
		_collabmode => "false",
		_base => 0,
		_top => undef,
		};
	bless $self, $class or die "$!\n";
	
	return $self;
}



###################################################################
## get_working_name() -> [filename]
## ---------------------------------
## Get the filename used for reading/writing the file. Useful for
## retrieving the temporary filename creatde in open_for_write();
###################################################################
sub get_working_filename
{
	my ($self) = @_;
	
	my $filename = $self->{_tempname} eq "" ? $self->{_originalname} : $self->{_tempname};
	return $filename;
}


sub set_filename
{
	my ($self, $name) = @_;
	
	$self->{_originalname} = $name;
	$self->get_size();
	
	return 0;
}


###################################################################
## init_collaboration(filesize) -> [filename]
## ---------------------------------
## Open a temporary file for writing to in collaborative mode. This
## forces it to a set size immediately and returns the active
## filename.
###################################################################
sub init_collaboration
{
	my ($self, $filesize, $tempdir) = @_;
	
	close $self->{_filehandle} if $self->is_open();
	
	#LET THE SYSTEM CREATE ITS OWN TEMPORARY FILE
	if($tempdir)
	{
		$self->{_tempdir} = $tempdir;
		$self->{_cleanup_tempdir} = Utilities::mkdirpath($tempdir);
		
		my $tempname = $self->{_originalname};
		$tempname =~ s/^.*\///;
		
		$self->{_tempname} = "$tempdir$tempname.tmp";
		$tempname = $self->{_tempname};
		
		Logger::print_message(LOGLEVEL_INFO, "Temp name is '$tempname'", $LOG_TAG);
		
		## TOUCH THE FILENAME SO WE CAN JUST OPEN IT FOR APPEND ACCESS WITHOUT ISSUES
		## MAKE SURE TO ESCAPE THE ' FIRST SO WE CAN USE IT IN SHELL-SAFE(R) SINGLE QUOTES
		my $safe_tempname = escape_single_quotes($tempname);
		`touch '$safe_tempname'`;
		
		open(my $handle, "+<" . $tempname) or die "$!\n";
		$self->{_filehandle} = $handle;
	}
	else
	{
		(($self->{_filehandle}, $self->{_tempname}) = tempfile()) or die "$!\n";
	}
	
	binmode $self->{_filehandle};
	
	my $oldh = select($self->{_filehandle});
	$| = 1;
	select($oldh);
	
	$self->grow($filesize);
	$self->join_collaboration(0, $filesize);
	Logger::print_message(LOGLEVEL_TRACE, "Opened " . $self->{_tempname} . " for collaborative work", $LOG_TAG);
	
	return $self->{_tempname};
}



###################################################################
## join_collaboration()
## ---------------------------------
## Impose collaboration bounds
###################################################################
sub join_collaboration
{
	my ($self, $base, $top) = @_;
		
	#FILL IN MISSING DATA, BOUND TO ACTUAL FILESIZE
	my $size = $self->get_size();
	$base = 0 		if !defined $base || $base < 0;
	$top = $size		if !defined $top || $top > $size || $top < 0;
	
	$self->{_base} = $base;
	$self->{_top} = $top;
	$self->{_collabmode} = "true";
	
	Logger::print_message(LOGLEVEL_TRACE, "Set limit of " . ($top - $base) . " byte window $base bytes into the file for collaborative work", $LOG_TAG);
}



###################################################################
## finish_collaboration(validate)
## ---------------------------------
## Finish collaboration, and close the file as usual.
###################################################################
sub finish_collaboration
{
	my $self = shift;
	my $validate = shift;
	
	$self->{_collabmode} = "false";
	$self->{_base} = 0;
	$self->{_top} = undef;
	
	Logger::print_message(LOGLEVEL_TRACE, "Finishing collaborative editing and closing file", $LOG_TAG);
	return $self->close_file($validate);
}




###################################################################
## check_exists() -> [fileexists]
## ---------------------------------
## Detemine whether or not the related file exists and has a
## filesize greater than 0
###################################################################
sub check_exists
{
	my $self = shift;
	my $useworking = shift || 0;
	
	if($self->{_originalname} eq "" && !$useworking) { return 0; }
	
	#$self->get_size();
	return -e ($self->{_originalname} || $self->{_tempname}); #;
	#$self->{_filesize} = $self->{_filesize} || 0;
	#Logger::print_message(LOGLEVEL_TRACE, "File '" . ($self->{_originalname} || $self->{_tempname}) . "' has size $self->{_filesize}", $LOG_TAG);
	#return $self->{_filesize} > 0;
}




###################################################################
## check_occupies_space() -> [fileexists]
## ---------------------------------
## Detemine whether or not the related file exists and has a
## filesize greater than 0
###################################################################
sub check_occupies_space
{
	my $self = shift;
	my $useworking = shift || "false";
	
	if($self->{_originalname} eq "" && $useworking ne "true") { return 0; }
	
	$self->{_filesize} = 0;
	eval { $self->get_size(); };
	
	return $self->{_filesize} > 0;
}


###################################################################
## get_size() -> [filesize]
## ---------------------------------
## Get the size (in bytes) of the related file
###################################################################
sub get_size
{
	my $self = shift;
	
	#IF WE'RE IN collab MODE, RETURN THE BLOCK SIZE
	return $self->{_top} - $self->{_base} if $self->{_collabmode} eq "true";
	
	
	#IF THE FILE'S OPEN FOR WRITE, THE -s WILL BE WRONG, SO
	#DETERMINE IT MANUALLY
	if($self->{_filehandle})
	{
		my $maxpos = 0;
		
		#GET THE SIZE ON DISK
		if($self->{_tempname}) { $maxpos = -s $self->{_tempname}; } #---
		
		#GET THE MAXMIMUM READBLE LENGTH SINCE IT WAS OPENED
		my $curpos = tell($self->{_filehandle});
		seek($self->{_filehandle}, 0, SEEK_END);
		$self->{_filesize} = tell($self->{_filehandle});
		seek($self->{_filehandle}, $curpos, SEEK_SET);
		
		#FILESIZE IS WHICHEVER IS BIGGEST
		$self->{_filesize} = $maxpos if $self->{_filesize} < $maxpos;
	}
	#OTHERWISE, JUST GET THE FILESIZE ON DISK
	else
	{
		my $fn = $self->{_originalname} || $self->{_tempname};
		if ( ! defined $fn ) {
			$self->{_filesize} = undef;
		} elsif ( ! -e $fn ) {
			$self->{_filesize} = 0 if ( ! defined $self->{_filesize} );
			Logger::print_message(LOGLEVEL_TRACE, "File '$fn' does not exist", $LOG_TAG);
	
		} else {
			$self->{_filesize} = -s ($self->{_originalname} || $self->{_tempname}); #;
			$self->{_filesize} = $self->{_filesize} || 0;
			Logger::print_message(LOGLEVEL_TRACE, "File '$fn' has size $self->{_filesize}", $LOG_TAG);
		}
	}
	
	
	
	return $self->{_filesize};
}



###################################################################
## is_open() -> [isopen]
## ---------------------------------
## Tests whether the file is currently open for reading/writing
###################################################################
sub is_open
{
	my $self = shift;
	
	return 0 if $self->{_filehandle} eq "";			#FILE NOT OPEN
	return 1 if $self->{_tempname} eq "";			#OPEN FOR READ
	return 2;										#OPEN FOR WRITE
}



###################################################################
## open_for_read() -> [readhandle]
## ---------------------------------
## If possible, open the related file for reading operations. Raise
## an error if the file doesn't exist, the filesize is 0 or the
## file couldn't be opened for reading
###################################################################
sub open_for_read
{
	my ($self, $filename) = @_;
	
	#unless($self->check_exists(1)) { die "File or directory not found\n"; }
	close $self->{_filehandle} if $self->is_open();
	
	my $filehandle;
	$self->{_originalname} = $filename if $filename;
	open($filehandle, $self->{_originalname}) or die "$!\n";
	$self->{_filehandle} = $filehandle;
	binmode $self->{_filehandle};
	
	
	#GOTO THE START OF OUR BLOCK IF WORKING IN collab MODE
	seek($filehandle, $self->{_base}, SEEK_SET) if $self->{_collabmode} eq "true";
	
	Logger::print_message(LOGLEVEL_TRACE, "Opened " . $self->{_originalname} . " for reading", $LOG_TAG);
	return $self->{_filehandle};
}



###################################################################
## open_for_write() -> [writehandle]
## ---------------------------------
## Open a temporary file for writing to, or raises an error
###################################################################
sub open_for_write
{
	my ($self, $filename) = @_;
	
	
	close $self->{_filehandle} if $self->is_open();
	$self->{_originalname} = $filename if $filename;


	#IF WE'RE WORKING IN collab MODE, DON'T USE A TEMPORARY FILE
	if($self->{_collabmode} eq "true")
	{
		open my $filehandle, "+<" . $self->{_originalname} or die "$!\n";
		
		#GOTO THE START OF OUR BLOCK IF WORKING IN collab MODE
		seek($filehandle, $self->{_base}, SEEK_SET) if $self->{_collabmode} eq "true";
		$self->{_filehandle} = $filehandle;
		Logger::print_message(LOGLEVEL_TRACE, "Opened collaborative file " . $self->{_originalname} . " for write access", $LOG_TAG);
	}
	else
	{
		(($self->{_filehandle}, $self->{_tempname}) = tempfile()) or die "$!\n";
		Logger::print_message(LOGLEVEL_TRACE, "Opened temporary file " . $self->{_tempname} . " for write access", $LOG_TAG);
	}


	
	
	binmode $self->{_filehandle};
	
	my $oldh = select($self->{_filehandle});
	$| = 1;
	select($oldh); 
	
	return $self->{_filehandle};
}



###################################################################
## grow(newsize) -> [filesize]
## ---------------------------------------------------------------------
## If newsize is greater than the current file size, grow the filesize
## until it matches. Otherwise, truncate the file.
###################################################################
sub grow
{
	my ($self, $newsize) = @_;
	
	unless($self->is_open())
	{
		$self->open_for_write() or die "Couldn't open file for writing\n";
	}
	
	return $self->{_top} - $self->{_base} if $self->{_collabmode} eq "true";


	#IF THE FILE IS SMALLER THAN THE BLOCK WRITE POSITION,
	#INCREASE THE FILESIZE UNTIL IT IS
	if($newsize > $self->get_size())
	{
		#ONLY ADD BYTES TO THE END OF THE FILE, OR ELSE WE RISK
		#OVERWRITING DATA LATER ON
		my $fh = $self->{_filehandle};
		
		my $curpos = tell($fh);
		seek($fh, $newsize - 1, SEEK_SET);
		print $fh "\0";
		seek($fh, $curpos, SEEK_SET);
	}
	elsif($newsize < $self->get_size())
	{
		truncate $self->{_filehandle}, $newsize;
	}
	
	
	Logger::print_message(LOGLEVEL_TRACE, "Grew file to $newsize bytes", $LOG_TAG);
	
	return $newsize;
}



###################################################################
## get_block([blocksize], [blockstart]) -> (data, bytesread, offset)
## -----------------------------------------------------
## Read blocksize bytes from the related file, trying to open the
## file if not already done so. Blocksize defaults to 1kb,
## blockstart defaults to the current read position
###################################################################
sub get_block
{
	my ($self, $blocksize, $blockstart) = @_;
	my $blockdata;
	
	
	if(!$self->is_open())
	{
		$self->open_for_read() or die "Couldn't open file for reading\n";
	}
	
	
	
	#GET THE VIEWING WINDOW
	my $top = defined $self->{_top} ? $self->{_top} : $self->get_size();
	my $base = $self->{_base};
	my $fh = $self->{_filehandle};
	
	#GET RELATIVE (0-BASED) SEEK POS
	$blockstart = tell($self->{_filehandle}) - $base if !defined $blockstart;
	$blocksize = 1024 if !defined $blocksize;
	
	#SEEK TO ABSOLUTE POSITION IF WE CAN
	die "Trying to read a block outside of viewable limitations ($base + $blockstart + $blocksize) > $top)" if $base + $blockstart + $blocksize > $top;
	seek($fh, $base + $blockstart, SEEK_SET);
	
	
	
	#READ DATA
	my $bytesread = sysread($self->{_filehandle}, $blockdata, $blocksize);
	die "$!\n" if !defined $bytesread;
	
	return ($blockdata, $bytesread, $blockstart);
}



###################################################################
## get_block_checksum([blockstart], [blocksize]) -> (md5)
## -----------------------------------------------------
## Read blocksize bytes from the related file, and return a
## checksum. Try to open the file if not already done so.
## Blocksize defaults to 1kb, blockstart defaults to the
## current read position
###################################################################
sub get_block_checksum
{
	my ($self, $blockstart, $blocksize) = @_;
	my $blockdata;
	
	Logger::print_message(LOGLEVEL_TRACE, "Getting checksum of file for block: $blockstart" . "b -> " . ($blockstart + $blocksize) . "b", $LOG_TAG);
	
	if(!$self->is_open())
	{
		$self->open_for_read() or die "Couldn't open file for reading\n";
	}
	
	#GET THE VIEWING WINDOW
	my $top = defined $self->{_top} ? $self->{_top} : $self->get_size();
	my $base = $self->{_base};
	my $fh = $self->{_filehandle};
	
	#GET RELATIVE (0-BASED) SEEK POS
	$blockstart = tell($self->{_filehandle}) - $base if !defined $blockstart;
	$blocksize = 1024 if !defined $blocksize;
	
	#SEEK TO ABSOLUTE POSITION IF WE CAN
	die "Trying to read a block outside of viewable limitations (" . ($base + $blockstart + $blocksize) . " > $top)" if $base + $blockstart + $blocksize > $top;
	seek($fh, $base + $blockstart, SEEK_SET);
	
	
	
	#READ DATA
	my $bytesread = sysread($self->{_filehandle}, $blockdata, $blocksize) or die "$!\n";
	
	
	return md5_hex($blockdata);
}



###################################################################
## save_block([blockdata], [blocksize], [blockstart]) -> [blockswritten]
## ---------------------------------------------------------------------
## Write blocksize bytes of blockdata to the related file, starting
## at blockstart position in the file. Try to open the file if necessary
## and increase the filesize to compensate for blocks after file end.
## Blockstart defaults to the beginning of the file.
###################################################################
sub save_block
{
	my ($self, $blockdata, $blocksize, $blockstart) = @_;
	
	unless($self->is_open())
	{
		$self->open_for_write() or die "Couldn't open file for writing\n";
	}
	
	
	
	my $top = defined $self->{_top} ? $self->{_top} : $self->get_size();
	my $base = $self->{_base};
	my $fh = $self->{_filehandle};
	
	$blockstart = tell($self->{_filehandle}) - $base if !defined $blockstart;
	$blocksize = length($blockdata) if !defined $blocksize;
	
	die "Trying to write a block outside of strict viewable limitations (" . ($base + $blockstart + $blocksize) . " > $top)" if defined $self->{_top} && $base + $blockstart + $blocksize > $top;
	
	
	
	Logger::print_message(LOGLEVEL_TRACE, "Heating up pipe.", $LOG_TAG);
	my $oldh = select($self->{_filehandle});
	$| = 1;
	select($oldh); 
	
	Logger::print_message(LOGLEVEL_TRACE, "Seeking to correct position.", $LOG_TAG);
	seek($fh, $base + $blockstart, SEEK_SET);
	
	$blockdata = substr($blockdata, 0, $blocksize) if $blocksize < length($blockdata);
	
	
	Logger::print_message(LOGLEVEL_TRACE, "Writing $blocksize bytes to disk in 512kb chunks.", $LOG_TAG);
	while(length($blockdata) > 512 * 1024)
	{
		my $portion = substr($blockdata, 0, 512 * 1024);
		$blockdata = substr($blockdata, 512 * 1024);
		
		print $fh $portion or die "Couldn't write block [$!]\n";
	}
	print $fh $blockdata or die "Couldn't write trailing bytes of block [$!]\n";
	Logger::print_message(LOGLEVEL_TRACE, "Bytes written successfully.", $LOG_TAG);
	
	
	return $blocksize;
}


###################################################################
## close_file()
## ---------------------------------
## Close the filehandle. If opened for writing, move the temporary
## file into the original file's position.
###################################################################
sub close_file
{
	my $self = shift;
	my $validate = shift || "";
	
	return 0 if $self->{_filehandle} eq "";
	
	close $self->{_filehandle};
	
	#IF WE'RE COLLABORATING, DON'T MOVE OR VERIFY ANYTHING
	if($self->{_tempname} && $self->{_collabmode} ne "true")
	{
		#VALIDATE THE FILE AGAINST A CHECKSUM WHERE SUPPLIED
		my $cksum = "";
		if($validate) { $cksum = $self->get_checksum(); }
		if($cksum ne $validate)
		{
			Logger::print_message(LOGLEVEL_WARNING, "Checksum of source file ($validate) does not match checksum of received file ($cksum)", $LOG_TAG);
			return 1;
		}
		Logger::print_message(LOGLEVEL_TRACE, "File passed validation against $cksum", $LOG_TAG) if $validate;
		
		#MOVE THE TEMPORARY FILE TO THE DESTINATION
		if($self->{_originalname})
		{
			$self->{_originalname} =~ m/(.*?)([^\/]*)$/;
			Logger::print_message(LOGLEVEL_TRACE, "Creating directory $1", $LOG_TAG);
			Utilities::mkdirpath($1);
			
			Logger::print_message(LOGLEVEL_TRACE, "Moving '$self->{_tempname}' to '$self->{_originalname}'", $LOG_TAG);
			move($self->{_tempname}, $self->{_originalname}) or die "$!\n";
			chmod 0775, $self->{_originalname};
			$self->{_tempname} = "";
			
			Logger::print_message(LOGLEVEL_TRACE, "File was moved to " . $self->{_originalname}, $LOG_TAG);
		}
	}
	
	Utilities::cleanDirPathQuick($self->{_tempdir}, $self->{_cleanup_tempdir}) if $self->{_tempdir} && $self->{_cleanup_tempdir};
	
	$self->{_filehandle} = "";
	$self->{_collabmode} = "false";
	$self->{_base} = 0;
	$self->{_top} = undef;
	
	Logger::print_message(LOGLEVEL_TRACE, "Closed file", $LOG_TAG);
	
	return 0;
}



###################################################################
## clean()
## ---------------------------------
## Close the filehandle. For use when file transfer has failed and
## we don't want to overwrite any existing file with the
## temporary one
###################################################################
sub clean
{
	my $self = shift;
	
	close $self->{_filehandle};
	$self->{_tempname} = "";
	$self->{_filehandle} = "";
	$self->{_collabmode} = "false";
	$self->{_base} = 0;
	$self->{_top} = undef;
	
	Logger::print_message(LOGLEVEL_TRACE, "Closed file", $LOG_TAG);
}



###################################################################
## get_checksum()
## ---------------------------------
## Generates a checksum for the file
###################################################################
sub get_checksum
{
	my $self = shift;
	
	#NOT QUITE SURE WHAT THIS REGEX DOES, SO I'M KEEPING IT JUST IN CASE
	#my ($file_name) = ( $file =~ /([^\\\/]+)[\\\/]*$/gs );
	
	my $filename = ($self->{_tempname} ? $self->{_tempname} : $self->{_originalname});
	return Utilities::getCheckSum($filename, "md5");
}



###################################################################
## check_block_limits(blockstart, blocksize) -> [isvalid]
## ---------------------------------------------------------------------
## Checks if a block is within the limitations imposed by collaboratio
## mode, or is otherwise within the file's size
###################################################################
sub check_block_limits
{
	my ($self, $blockstart, $blocksize) = @_;
	
	#LIMIT CHECKS
	if($self->{_collabmode} eq "true")
	{
		$blockstart = 0 if $blockstart > 0;
		return 1 if $blockstart + $blocksize > $self->{_collabupperlimit} - $self->{_collablowerlimit};
	}
	else
	{
		return 1 if $blockstart > $self->get_size();
	}
	
	return 0;
}


sub move_file
{
	my ($self, $destination) = @_;
	
	return undef if $self->is_open();
	
	#ENSURE THE DESTINATION PATH EXISTS
	my $path = $destination;
	$path =~ s/(^.*)[\\\/]([^\\\/]*)$/$1/;
	Utilities::mkdirpath($path) if $path;
	
	move $self->{_originalname}, $destination;
	$self->{_originalname} = $destination;
	
	return $self;
}

sub copy_file
{
	my ($self, $destination) = @_;
	
	return undef if $self->is_open();
	
	#ENSURE THE DESTINATION PATH EXISTS
	my $path = $destination;
	$path =~ s/(^.*)[\\\/]([^\\\/]*)$/$1/;
	Utilities::mkdirpath($path) if $path;
	
	copy $self->{_originalname}, $destination;
	#$self->{_originalname} = $destination;
	
	return TransferFile->new($destination);
}

sub destroy_file
{
	my ($self) = @_;
	
	return if $self->is_open();
	
	unlink $self->{_originalname} or die "$! $self->{_originalname}";
}

1;
