#!/usr/local/bin/perl

use warnings;
use strict;

package dirUtils;

use File::Path;
use File::stat;
use Carp qw/carp/;

use utf8;
use Encode;
use Cwd;

use Carp qw(cluck confess longmess shortmess);
use Digest::MD5 qw/ md5_hex /;

use lib 'Utils';

use Logger 1.9;


use constant LOG_TAG => "DUT";
use constant CM_BIN_PATH => "../common/cm/cm_bin";   # was "cm/cm_bin"

our $verbose = 0;

# setDebug intended for use by unit tests

sub setDebug {
	( $verbose ) = @_;
}
 
#------------------------------------------------------------------
#	Recursively create a directory path
#------------------------------------------------------------------
sub mkdirpath
{
	my ($path) = @_;

	die "mkdir path not specified" unless defined $path;
	
	my $existingroot = "";
	my $curdir = ($path =~ m/^[^\\\/]/ ? "." : "");	#DETERMINE IF THE PATH IS RELATIVE OR ABSOLUTE
	my $newdir = "";

	eval {
	
		#FOR EACH DIRECTORY (SLASH-DELIMITED ENTRY)
		my $inflimit = 1000;
		while($path =~ m/^([^\\\/]*)[\\\/](.*)$/)
		{
			$inflimit--;
			die "Infinite loop in mkdirpath, path $path" if ( $inflimit == 0 );

			$newdir = $1;
			$path = $2;
	
			#IGNORE DOUBLE-SLASHES
			next unless $1;
	
			#MAKE THE DIRECTORY IF APPLICABLE
			$curdir .= "/" . $newdir;
			if ( -d $curdir ) {
				$existingroot = $curdir;
			} else 	{
				print_message(LOGLEVEL_TRACE, "Forming new path '$curdir'\n", LOG_TAG);
				mkdir $curdir;
			}
			chmod 0755, $curdir;
		}

		#FINAL MAKE
		$curdir .= "/" . $path;
		if ( -d $curdir) 	{
			$existingroot = $curdir if $path;
		} else {
			print_message(LOGLEVEL_TRACE, "Final make '$curdir'\n", LOG_TAG);
			mkdir $curdir;
		}
		chmod 0755, $curdir;

		print_message(LOGLEVEL_TRACE, "Added directories onto '$existingroot'", LOG_TAG);
	};
	if ( $@ ) {
		print_message(LOGLEVEL_WARNING, "mkdirpath: $@", LOG_TAG );
	}
	return $existingroot;
}

# cleandirpath ( directorypath [, safemode [,deletefilter ] ] )
# deletes a directory tree, ignoring files which match patterns in deletefilter
#
# Arguments
# directorypath - root of the directory tree to clean - must be a directory
# safemode - 0 (delete directories once empty), 1 (leave directories even if empty)
# deletefilter - array of patterns for files to happily delete
#
# Returns: 0 if directory not empty (or error reading directory), 1 if directory is empty
#
sub cleandirpath
{
	my ($directorypath, $safemode, @deletefilter) = @_;

	my $rc = 0; # 0 means havent deleted this directory, 1 means directory has been deleted
	my $dir_empty = 1; # assume true

	$safemode = 1 unless defined $safemode; # 0 means delete empty dirs, 1 means leave them alone
	
	die "cleandirpath: directorypath must be supplied" unless defined $directorypath;

	print "cleandirpath: path: $directorypath, safemode $safemode, verbose $verbose\n" if $verbose;

	my $absdirectorypath = Cwd::abs_path( $directorypath );
	die "cleandirpath: directorypath '$directorypath' has no abs path" unless ( defined $absdirectorypath );
	$absdirectorypath .= "/" if $absdirectorypath !~ m/\/$/;

	# Would have been easier if deletefilter was not an array, so we could have added a $depth arg to trap excessive recursion
	my @splitpath = split '/', $absdirectorypath;
	my $depth = scalar @splitpath;
	die "cleandirpath: '$absdirectorypath' is fewer than two levels deep from root" if ( $depth < 4 );
	die "cleandirpath: '$absdirectorypath' too many levels" if ( $depth > 100 );

	eval {

		# opendir likes unencoded directory name
		my $directory_dec;
		eval {
			$directory_dec = decode('UTF-8', "$absdirectorypath", Encode::FB_CROAK );
			die "decoded directory is blank" if ( ( ! defined $directory_dec ) || ( $directory_dec eq "" ) );
		};
		if ( $@ ) {
			print "cleandirpath: exception decoding directorypath\n" if $verbose;
			$directory_dec = $absdirectorypath;
		}

		my $dirhandle;
		opendir( $dirhandle, $directory_dec ) || die "can't open $directory_dec,  $!";
		print "Opened directory $directory_dec\n" if $verbose;
		my @files = readdir($dirhandle);
		closedir $dirhandle;
	
		foreach my $name ( @files ) {

			next if $name =~ /^\.{1,2}$/;

 			print " Filename: $name\n" if $verbose;
	
			my $name_dec;
			eval {
				$name_dec = decode('UTF-8', "$name", Encode::FB_CROAK );
			};
			if ( $@ ) {
				print "cleandirpath: exception decoding name '$name'\n" if $verbose;
				$name_dec = $name;
			}

			# However, File::stat wants encoded name
			my $filename = File::Spec->catpath( "", $absdirectorypath, $name );

			eval {

				my $stats = stat( $filename ); # Using File::stat here rather than Core::stat
				die "Could not stat '$filename' or file doesnt exist" unless defined $stats;
				die "File '$filename' does not exist" unless -e $stats;

				if ( -d $stats ) {

					print "'$filename' is a directory\n" if $verbose;
					$rc = cleandirpath( "$filename/", $safemode, @deletefilter );
					die "Can't delete '$filename/' - directory not empty\n" if ( $rc == 0 );
					die "Empty sub dir means this dir is NOT empty" if ( $safemode == 1 );

				} elsif ( -f $stats ) {

					print "'$filename' is a file\n" if $verbose;
					my $match;
					foreach my $filter ( @deletefilter ) { 
						# print "... filter '$filter' matching '$filename/'\n" if $verbose;
						if ( "$filename" =~ m/^$filter$/ ) {
							$match = $filter; 
							print "filter '$filter' matches '$filename/'\n" if $verbose;
							last; 
						} 
					}
					die "File $name doesnt match any filter" unless defined $match;
					unlink $filename or die "Failed to unlink file '$filename' $!";

				} else {
					die "$name is neither a directory or file";
				}
			};
			if ( $@ ) {
				my $error = $@;
				$error =~ s/ at .*//gs;
				print "cleandirpath: $error\n" if $verbose;
				$dir_empty = 0;
			}
		} # end FOR loop

		if ( ( $safemode == 0 ) && ( $dir_empty == 1 ) ) {
			print "cleandirpath: Attempting rmdir '$directory_dec'\n" if $verbose;
			rmdir( $absdirectorypath ) || die "can't rmdir '$directory_dec' $! $?";
		}
	};
	if ( $@ ) {
		print "cleandirpath: Exception, directory $directorypath $@\n" if ( $verbose );
		print `ls $directorypath` . "\n" if ( $verbose );
		$dir_empty = 0;
	}
	
	return $dir_empty;
}

#TRAVERSES UP A PATH, DELETING EACH DIRECTORY ONLY IF IT'S EMPTY
# Returns : (nothing)
# Throws an exception if arg 1 is missing

sub cleanDirPathQuick
{
	my ($directorypath, $root) = @_;

	die "directorypath argument missing" unless defined $directorypath;

	$directorypath .= "/" unless $directorypath =~ m/\/$/;

	$root = "" unless defined $root;
	$root .= "/" unless $root =~ m/\/$/;

	my $inflimit = 1000;
	while($directorypath ne "/")
	{
		$inflimit--;
		die "cleanDirPathQuick: Infinite loop, path $directorypath" if ( $inflimit == 0 );
 
		$directorypath =~ m/^(\/?.*[^\\])\/(.*?)$/;
		my $dp = $1;
		my $path = "$1/";

		if($path eq $root) { print_message(LOGLEVEL_TRACE, "Not deleting '$root'", LOG_TAG); last; }
		
		#OPEN THE DIRECTORY IF WE CAN
		my $directory_dec;
		eval {
			$directory_dec = decode('UTF-8', "$path", Encode::FB_CROAK );
		};
		if ( $@ ) {
			$directory_dec = $path;
		}
		if(opendir(my $dirhandle, $directory_dec))
		{
			#DOES IT HAVE FILES IN IT?
			my $isempty = (scalar(grep { -e $path.$_ } readdir($dirhandle)) - 2) == 0;
			closedir($dirhandle);
			
			#IF IT DOES, IT'S NOT EMPTY, WE CAN'T DELETE ANY MORE
			if(!$isempty) { print_message(LOGLEVEL_TRACE, "Folders no longer empty at '$path'; leaving alone.", LOG_TAG); last; }
			rmdir $path;
		}
		
		
		$directorypath = $dp;
	}
}

#----------------------------------------------------------------
#	getCheckSum
#----------------------------------------------------------------

sub getCheckSum{
	my ($src, $ckmethod) = @_;

	die "getCheckSum: Source argument not supplied" unless defined ( $src );
	
	#DEFAULT TO OLD CHECKSUM IF NOT TOLD OTHERWISE
	$ckmethod = "cksum" if !defined $ckmethod;
	my $checksum = 0;
	
	#STANDARD CHECKSUM
	if($ckmethod eq "cksum")
	{
		#SINGLE QUOTES NEED A SPECIAL ESCAPING, BUT ALL ELSE SHOULD BE ALRIGHT
		$src = escape_single_quotes( $src );

		my $cmd = `cksum '$src'`;
		chomp($cmd);
		
		$checksum = $1 if $cmd =~ m/^([0-9a-fA-F]*) .*$/;
	}
	#MD5 CHECKSUM
	elsif($ckmethod eq "md5")
	{
		$checksum = getmd5CheckSum( $src );
	}
	else {
		die "getCheckSum: Unrecognised ckmethod '$ckmethod'";
	}
	
	return $checksum;
}

#----------------------------------------------------------------
#	getmd5CheckSum(filename) : Alias to getCheckSum(filename, "md5")
#	use Digest::md5, see http://perldoc.perl.org/Digest/MD5.html
#----------------------------------------------------------------
sub getmd5CheckSum
{
	my ($filename) = @_;

	return '' unless ( defined $filename );

	if ( open ( my $fh, '<', $filename ) ) {
		binmode ($fh);
		my $md5 = Digest::MD5->new->addfile($fh)->hexdigest;
		close($fh);
		return $md5;
	}
	return '';
}


# Checks whether full filename starts with a protected directory, ie one that we
# should not be doing anything to.
# Inputs:
# - path - full directory name, starting with "/"
# Returns
# - 0 ok,
# - 1, path is protected
sub isSystemPath {
	my $path = $_[0];

	return 1 if ( !defined $path );

	# exclude media from this list as /gpfs/... has abs_path /media/data/gpfs
	return 1 if ( $path =~ /^\/(bin|cgroup|etc|lib|lost\+found|prod|rsync|selinux|symlnks|boot|dev|img|lib64|misc|net|opt|proc|root|sbin|srv|sys|usr|var|xmp)/ );

	return 0;
}

#----------------------------------------------------------------
#	unlinkFile
# Inputs:
# arg 1 - filename of file to delete
# Returns:
# 0 - file deleted
# 1 - failed, even after changing permission to 777
# or dies if filename is "/" or the like.
#----------------------------------------------------------------
sub unlinkFile{

	my ($src) = @_;

	die "dirUtils::unlinkFile: src argument not supplied" unless ( defined $src );
	
	my $err = 0;

	my $absSrc = Cwd::abs_path( $src ); # Convert relative to absolute 
	if ( isSystemPath( $absSrc ) != 0 ) {
		die "dirUtils::unlinkFile: Top level is protected: '$src', abs path '$absSrc'";
	}

	my @dir_parts = split "\/", $absSrc;
	if ( (scalar @dir_parts) < 2 ) { # dir_parts[0] will be "" because of the leading /
		die "dirUtils::unlinkFile: No directories in '$src', abs path '$absSrc'";
	}
	if ( (scalar @dir_parts) < 4 ) { 
		die "dirUtils::unlinkFile: '$src', less than 3 levels, abs path '$absSrc'";
	};

	if (-e $src){
		# try and unlink first
		unlink($src);
		
		# check if success
		if ($? != 0){
			# force delete
			my $cmd = "chmod 777 '$src'; rm '$src'";
			system($cmd);
			
			if (-e $src){
				# unable to delete
				$err = 1;
			}
		}
	}
	
	return $err;
}

#----------------------------------------------------------------
#	dirLength
# 	return number of visible files in dir (or -1 if failed to open directory)
#----------------------------------------------------------------
sub dirLength(){
	my $src = $_[0];
	my $cnt = 0;

	my $src_dec;
	eval {
		$src_dec = decode('UTF-8', "$src", Encode::FB_CROAK );
	};
	if ( $@ ) {
		$src_dec = $src;
	}
	opendir( DIRHANDLE, $src_dec ) or return -1;
	while(defined (my $filename = readdir(DIRHANDLE)))
	{
	    $cnt++ if $filename !~ /^\.{1,2}$/;
	}
	closedir(DIRHANDLE);

	return $cnt;
}

# Change file/directory permissions on file or directory to 0777
# Used in checkin/checkin_main & checkin_dirs to make files
# both readable (to copy) and writeable so that they can be
# deleted afterwards.
# However, as cm_bin is undiscerning, add a check for the path of
# the file being deleted.
# Returns:
# 0 - if ok
# 1 - if change of permission failed or argument filename missing
# or dies if attempt made to change permissions on root directory,
# a sub directory of a system directory or a directory
# that is not at level 3 levels down.
#
sub make_read_write {
	my ($filename) = @_;

	my $rc = 0;

	eval {

		die "filename arg is null" unless defined $filename;

		my $abs_filename = Cwd::abs_path( $filename );
		die "could not get abs filename for '$filename'" if (( ! defined $abs_filename ) || ( $abs_filename eq '' ));

		die "system path '$filename', abs: '$abs_filename'" if ( isSystemPath( $abs_filename ) != 0 );

		my @dir_path = split( "/", $abs_filename );
		die "called for path '$filename' - 1 or less directories deep" if ( (scalar @dir_path) < 2 );

		my $s = stat( $abs_filename ) or die "Could not stat file '$abs_filename', $!";

		my ( $gid ) = getgrgid( $s->gid);
		my ( $uid ) = getpwuid( $s->uid );

		# print "Group: $gid, Owner: $uid\n" );

		if ( -f $s ) {
			if ( $uid eq 'apache' && $gid eq 'apache' ) { # could use -o but that just tests owner
				if ( ($s->mode &07777) != 0777 ) {
					chmod 0777, $abs_filename or die "Failed to chmod 0777 $abs_filename, $!";
				}
			} else {
				# Use cm_bin to change owner/perm
				die "Failed to find " . CM_BIN_PATH unless ( -e CM_BIN_PATH );

				# use single quotes to contain filename, so only need to escape those
				my $cmd = CM_BIN_PATH . " '" . escape_single_quotes( $abs_filename ) . "' > /dev/null 2>&1";
				my $rc = system( $cmd );
				die "$cmd returned $rc" unless ( $rc == 0 );
	
			}
		} elsif ( -d $s ) {
			die "Failed to find " . CM_BIN_PATH unless ( -e CM_BIN_PATH );

			# use single quotes to contain filename, so only need to escape those
			my $cmd = CM_BIN_PATH . " -R  '" . escape_single_quotes( $abs_filename ) . "' > /dev/null 2>&1";
			my $rc = system( $cmd );
			die "$cmd returned $rc" unless ( $rc == 0 );
		} else {
			die "Filename '$filename', is neither file nor directory";
		}
		die "Failed to find " . CM_BIN_PATH unless ( -e CM_BIN_PATH );

		# use single quotes to contain filename, so only need to escape those
		my $cmd = CM_BIN_PATH . " -R  '" . escape_single_quotes( $abs_filename ) . "' > /dev/null 2>&1";
		my $rc = system( $cmd );
		die "$cmd returned $rc" unless ( $rc == 0 );

	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "make_read_write: $error";
		$rc = 1;
	}
	return $rc;
}

# Single-quoted strings are used with shells, but single quotes don't escape normally

sub escape_single_quotes {
	my $filename = shift || "";
	$filename =~ s/'/'\\''/g;
	return $filename;
}

# escape_filename
# Escapes a small subset of characters that would otherwise cause an issue when we shell to 
# use ls, test etc
# Input:
# - filename
# Returns
# - escaped copy of filename
sub escape_filename {

	my ( $filename ) = @_;

	$filename =~ s/([^\\])([\s])/$1\\$2/g; # unescaped spaces
	$filename =~ s/([^\\])([\'\"\&\<\>\-])/$1\\$2/g;
	$filename =~ s/([^\\])([\)\(])/$1\\$2/g; # round brackets

	return $filename;
}

sub add_slash
{
	my ($text, $positions) = @_;
	
	$positions = uc($positions);
	
	$text = "/$text"	if $positions =~ m/(BEGIN|BOTH)/ && $text !~ m/^\//;
	$text .= "/"			if $positions =~ m/(END|BOTH)/ && $text !~ m/\/$/;
	
	return $text;
}

sub clear_slash
{
	my ($text, $positions) = @_;
	
	$positions = uc($positions);
	
	$text = substr($text, 1)		if $positions =~ m/(BEGIN|BOTH)/ && $text =~ m/^\//;
	$text = substr($text, 0, -1) if $positions =~ m/(END|BOTH)/ && $text =~ m/\/$/;
	$text =~ s/[\/\\]{2,}/\//g if $positions =~ m/(MULTIPLES)/;
	
	return $text;
}

1;
