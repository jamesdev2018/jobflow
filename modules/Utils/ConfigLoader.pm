package ConfigLoader;

use warnings;
use strict;

use base 'Exporter';
our @EXPORT = qw( load_file save_file get_setting set_setting );

use Scalar::Util 'blessed';


my $data = { options => {},			# VARIABLES LOADED FROM CONFIG TEXT
			 variables => {},		# VARIABLES USED FOR LOADING
			 rawconfig => [],		# VERBATIM CONFIG TEXT
			 autoreload => 0,		# RELOAD CONFIG SETTING FROM FILE?
		   };

sub new
{
	my $class = shift;
	
	my $self = {
		options => {},
		variables => {},
		rawconfig => [],
		autoreload => 0,
		section => "dev",
		};
	bless $self, $class or die "$!\n";
	
	return $self;
}

sub reload_config
{
	my $info = blessed($_[0]) ? shift : $data;
	
	load_string($info, $info->{rawconfig}) if blessed($info);
	load_string($info->{rawconfig});
}

sub load_file
{
	my $info = blessed($_[0]) ? shift : $data;
	my ($filename, $section) = @_;
	
	open CONFIG, "<$filename" or return 1;
	my $string;
	while(<CONFIG>) { $string .= "$_\n"; }
	close CONFIG;
	
	return load_string($info, $string, $section) if blessed($info);
	return load_string($string, $section, 1);
}

sub load_string
{
	my $info = blessed($_[0]) ? shift : $data;
	my ($nstring, $section, $reload) = @_;
	
	#$section = $info->{section} if !$reload;
	$section = "" unless defined $section;
	$section = lc( $section ); # As we match lc of the current section
	$nstring ||= "";
	
	my $cursection = $section || "dev";
	my $arrayparam;
	
	my $options = $reload ? undef : $info->{options};
	my $rawconfig = $reload ? undef : $info->{rawconfig};
	
	my @sections = ();
	
	## START SLURPING DATA IMMEDIATELY IF IT ISN'T IN A SECTION
	my $load_section = 1;
	
	my $linecount = 0;
	foreach my $line (split(/^/m, $nstring))
	{
		chomp($line);
		next if $line =~ m/^\s*$/;
		$linecount++;
		
		#GET THE DEFAULT SECTION, IF APPLICABLE
		if($line =~ m/^default\s*(\S.*?)\s*$/i && ($linecount == 1))
		{
			my $identifier = $1;
			#ONLY USE IF CALLER ISN'T OVERRIDING THE FILE'S VERSION
			if($identifier =~ m/^ENV:([^:]+):([^:]+)$/)
			{
				$section = lc($ENV{$1} || $2) if $section eq "";
			}
			else
			{
				$section = lc($1) if $section eq "";
			}
		}
		
		#EVALUATE NEW SECTION
		elsif($line =~ m/^\s*\[(.*)\]\s*$/)
		{
			$cursection = lc($1);
			push(@sections, lc($1));
			
			## IF THE SECTION TITLE IS CONTAINED WITHIN ITS OWN [], EVALUATE IT
			if($cursection =~ m/^\[(.*)\]$/)
			{
				$cursection = $1;
				
				foreach my $key (keys %{ $info->{variables} })
				{
					my $val = $info->{variables}->{$key};
					$cursection =~ s/$key/$val/g;
				}
				
				## DISOLVE BYTE SIZES
				$cursection =~ s/(\d+)tb/($1 * 1024) . 'gb'/e;
				$cursection =~ s/(\d+)gb/($1 * 1024) . 'mb'/e;
				$cursection =~ s/(\d+)mb/($1 * 1024) . 'kb'/e;
				$cursection =~ s/(\d+)kb/($1 * 1024)/e;
				
				$load_section = eval $cursection;
			}
			
			## OTHERWISE, JUST MATCH THE SECTION TITLE
			else
			{
				$load_section = $cursection eq $section;
			}
		}
		
		#ADD TO THE CURRENT ARRAY
		elsif($arrayparam)
		{
			if($line =~ m/^\s*(#<<<)/)
			{
				$arrayparam = "";
			}
			elsif($line =~ m/^\s*(.*)\s*/)
			{
				push(@{ $options->{$arrayparam} }, $1);
			}
		}
		
		#LOAD VALUES INTO THE HASH
		elsif($load_section)
		{
			$line =~ m/^([^=#]*)(=?)([+]?)([^#]*)#?(.*)$/;
			
			my $parameter = lc(defined $1 ? $1 : "");
			my $defined = $2 eq '=';
			my $addition = $3 ? 1 : 0;
			my $value = defined $4 ? $4 : "";
			my $comment = $5 || "";
			
			$parameter =~ s/^\s*(\S*?)\s*$/$1/;
			$value =~ s/^\s*(\S.*?)\s*$/$1/;
			$value = 1 if !$defined;
			
			if(($value eq "") && ($comment eq ">>>"))
			{
				$options->{$parameter} = ();
				$arrayparam = $parameter;
			}
			else
			{
				$options->{$parameter} = ($addition ? (defined $options->{$parameter} ? $options->{$parameter} : "") : "") . $value;
			}
			
			$line = "--VALUE--$parameter" if $parameter ne "";
		}
		
		push(@{ $rawconfig }, $line);			#SAVE THE FILE FOR LATER IN CASE WE NEED TO OVERWRITE IT
	}
	
	
	$info->{options} = $options;
	$info->{rawconfig} = $rawconfig;
	$info->{section} = $section;
	$info->{sections} = \@sections;
	
	return 0;
}

sub save_file
{
	my $info = (defined blessed($_[0])) ? shift : $data;
	
	my $options = $info->{options};
	my $rawconfig = $info->{rawconfig};
	
	
	my ($filename) = @_;
	
	open CONFIG, ">$filename" or return 1;
	foreach(@{ $rawconfig })
	{
		if($_ =~ m/^--VALUE--(.+)$/)
		{
			print CONFIG "$1=" . $options->{$1} . "\n" if $options->{$1};
		}
		else
		{
			print CONFIG "$_\n";
		}
	}
	close CONFIG;
	
	return 0;
}

sub get_setting
{
	my $info = blessed($_[0]) ? shift : $data;
	my $options = $info->{options};

	# print "keys " . join( ",", keys %$options ) . "\n";
	
	my @retval;
	foreach my $setting (@_)
	{
		push(@retval, defined $options->{lc($setting)} ? $options->{lc($setting)} : "");
	}
	
	return $retval[0] if scalar @retval == 1;
	return @retval;
}

sub set_setting
{
	my $info = blessed($_[0]) ? shift : $data;
	my $options = $info->{options};
	
	if(ref($_[0]) eq 'ARRAY')
	{
		print "Array\n";
		foreach my $entry (@_)
		{
			my $setting = lc(@$entry[0]);
			$options->{$setting} = @$entry[1];
		}
	}
	elsif(ref($_[0]) eq 'HASH')
	{
		foreach my $hash (@_)
		{
			my $setting = $hash->{name};
			my $value = $hash->{value};
			$setting = lc($setting);
			$options->{$setting} = $value;
		}
	}
	else
	{
		for(my $i = 0; $i < scalar @_; $i += 2)
		{
			my $setting = lc($_[$i]);
			my $value = $_[$i + 1];
			$options->{$setting} = $value;
		}
	}
	
	return 0;
}

sub set_variable
{
	my $info = blessed($_[0]) ? shift : $data;
	my ($variable, $value) = @_;
	
	$variable = lc($variable);
	$info->{variables}->{$variable} = $value;
}

sub set_variables
{
	my $info = blessed($_[0]) ? shift : $data;
	my ($variables) = @_;
	
	foreach my $k (keys %{ $variables }) { $info->{variables}->{$k} = $variables->{$k} unless ref $variables->{$k}; }
}

sub get_variable
{
	my $info = blessed($_[0]) ? shift : $data;
	my ($variable) = @_;
	
	$variable = lc($variable);
	return $info->{variables}->{$variable};
}

sub get_section
{
	my $info = blessed($_[0]) ? shift : $data;
	
	return $info->{section};
}

sub get_sections
{
	my $info = blessed($_[0]) ? shift : $data;
	
	return $info->{sections};
}

return 1;
