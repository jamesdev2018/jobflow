#!/usr/local/bin/perl




####	############################################################	##
####	############################################################	##
##   NON-BLOCKING SOCKETS THAT INHERENTLY HANDLE HEADERS AND 	##
##					CHECKSUMMED DATA BLOCKS	 					##
####	############################################################	##
####	############################################################	##




package TransferSocket;
our $VERSION = 1.2;		#VERSION STRING FOR PRE-5.10 PERL VARIANTS


use strict;
use warnings;

use Encode;
use POSIX qw(:errno_h);
use Fcntl;
use Time::HiRes qw( time usleep );

use Socket;
use IO::Socket::INET;
use IO::Select;
use IO::Handle;
use Sys::Hostname;

use Logger 1.9;		#SIMPLE BUT FLEXIBLE DEBUG LOGGING AND OUTPUT
use constant LOG_TAG => "SOK"; 

use constant STATE_NORMAL	=> 0;
use constant STATE_WARN		=> 1;
use constant STATE_FATAL		=> 2;
use constant STATE_RESET		=> 3;

use constant DEFAULT_TIMEOUT	=> 30;

my $NO_HANDSHAKES = "true";
my $packettypes = { N => " retry", E => "n error", S => " header", H => " data-header", D => " data" };

#EXPORT SOME VALUES SO PEOPLE DON'T HAVE TO PUT "TransferSocket::" IN FRONT OF /EVERYTHING/
use base 'Exporter';
our @EXPORT = qw(	DEFAULT_TIMEOUT );


#SET TO IGNORE PIPE SIGNAL, OR ELSE WE'LL JUST CRASH OUT IF THE SOCKETS
#BREAK, AND NO AMOUNT OF evalS WILL SAVE US
$SIG{PIPE} = 'IGNORE';


sub enable_legacy_mode
{
	my $version = $_[0] || "2.0";
	
	Logger::print_message(LOGLEVEL_DEBUG, "Starting up in legacy mode $version", LOG_TAG);
	
	Logger::print_message(LOGLEVEL_WARNING, " - Disabling handshakes", LOG_TAG) if $version lt "1.1";
	$NO_HANDSHAKES = $version lt "1.1" ? "true" : "false";
}

###################################################################
## new ([autoretry], [timeout], [autodie]) --> [transfersocketobject]
## ---------------------------------
## Create a new version of this class with an optional timeout
## (defaults to 5 seconds).
###################################################################
sub new
{
	my $class = shift;
	
	my $handshake = shift || "";
	my $autodie = "true";
	my $timeout = DEFAULT_TIMEOUT;
	my $autoreconnect = "false";
	
	#IF WE ARE PASSED A TEMPLATE SOCKET, USE THOSE VALUES INSTEAD
	if(((ref $handshake) || "SCALAR") eq "SCALAR")
	{
		$autodie = shift || $autodie;
		$timeout = shift || $timeout;
		$autoreconnect = shift || $autoreconnect;
	}
	else
	{
		$autodie = $handshake->{autodie};
		$timeout = $handshake->{timeout};
		$autoreconnect = $handshake->{autoreconnect};
		$handshake = $handshake->{handshake};
	}
	
	#INITIALISE CLASS VARIABLES
	my $self = {
		packettype	=> "",
		packetdata	=> "",
		packetlen	=> 0,
		
		sendtype		=> "",
		sendbuffer	=> "",
		
		retrycount	=> 5,
		autoreconnect=> $autoreconnect,
		autodie		=> $autodie,
		
		sockstate	=> STATE_NORMAL,
		lastmsg		=> "",
		lastsize		=> 0,
		
		timeout		=> $timeout,
		sock			=> undef,
		
		handshake	=> $NO_HANDSHAKES eq "true" ? undef : $handshake,
		
		
		original_remoteport => 0,
		remoteport	=> 0,
		remotehost	=> "0.0.0.0",
		localport	=> 0,
		
		unique		=> int(rand(100)),
		mode		=> "",
		};
	bless $self, $class or print_message(LOGLEVEL_FAILURE, "Couldn't create new TransferSocket", LOG_TAG);
	
	return $self;
}

sub DESTROY
{
	my ($self) = @_;
	
	$self->close_socket();
}

sub getmode
{
	my ($self) = @_;
	
	return $self->{mode} || "";
}




###################################################################
## listen (localport)
## ---------------------------------
## Start listening on a given port
###################################################################
sub listen
{
	my ($self, $localport) = @_;
	
	$self->close_socket();
	
	#CREATE THE SOCKET, LISTEN
	my $socket = undef;
	my $retrycount = $self->{retrycount};
	
	print_message(LOGLEVEL_FAILURE, "Port is already in use, can't listen", LOG_TAG) if !canlisten($localport, 5);
	
	while($retrycount-- > 0)
	{
		$socket = IO::Socket::INET->new(
			Listen		=> SOMAXCONN,
			LocalPort	=> $localport,
			Proto		=> 'tcp',
			ReuseAddr	=> 1,
			Blocking		=> 0,
			);
		
		last if defined $socket;
		sleep(1);
		print_message(LOGLEVEL_FAILURE, "Don't have permission to listen on port $localport", LOG_TAG) if $@ eq "IO::Socket::INET: Permission denied";
		print_message(LOGLEVEL_WARNING, "Couldn't listen on port $localport ($!/$@); retrying", LOG_TAG);
	}
	if(!defined $socket)
	{
		my $result = `lsof -i:$localport`;
		print_message(LOGLEVEL_WARNING, "Failed to listen on port $localport", LOG_TAG);
		print_message(LOGLEVEL_FAILURE, "Result of lsof is: $result", LOG_TAG);
		return undef;
	}
	
	$self->{autoreconnect} = "false";		#DISABLE RECONNECT - WE'RE LISTENING, NOT CONNECTING
	$self->{localport} = $localport;
	$self->{sock} = $socket;
	$self->{mode} = "Listening";
	print_message(LOGLEVEL_TRACE, "Listening on port $localport", LOG_TAG);
	
	return 0;
}

sub canlisten
{
	my ($localport) = @_;
	
	#my $result = `lsof -i:$localport -sTCP:^ESTABLISHED -sTCP:^LISTEN -sTCP:^SYN_SENT -sTCP:^CLOSE_WAIT | tail -n +2`;
	#my $result = `lsof -i:$localport -sTCP:^ESTABLISHED -sTCP:^SYN_SENT -sTCP:^CLOSE_WAIT | tail -n +2`;
	my $result = `netstat -antlp 2>/dev/null | grep ':$localport' | egrep -ve '(ESTABLISHED|SYN_SENT|.+_WAIT)'`;
	print_message(LOGLEVEL_TRACE, "netstat result for $localport is '$result'", LOG_TAG);
	
	return $result eq "";
}



###################################################################
## accept ([parent])
## ---------------------------------
## Accept a connection from a given parent, or accept the current
## class' connection and close the listener
###################################################################
sub accept
{
	my ($self, $parent) = @_;
	
	#DETERMINE IF WE'RE ACCEPTING THE CONNECTION FROM OURSELVES OR A PARENT
	my $parentsocket = (defined $parent->{sock} ? $parent->{sock} : $self->{sock});
	my $childsocket;
	
	print_message(LOGLEVEL_FAILURE, "No parent socket defined", LOG_TAG) unless defined $parentsocket;
	
	#TRY TO ACCEPT WHILST WE HAVEN'T TIMED OUT
	my $mark = time();
	my $retrycount = $self->{retrycount};
	while(!defined ($childsocket = $parentsocket->accept()))
	{
		if($! == EAGAIN || $! == EWOULDBLOCK || $! == EINPROGRESS || $! == EINTR)
		{
			next unless defined $self->{timeout};
			next if time() <= $mark + $self->{timeout};
			if($retrycount-- > 0)
			{
				print_message(LOGLEVEL_WARNING, "Timed out trying to accept a connection. Retrying $retrycount more times...", LOG_TAG);
				$mark = time();
				next;
			}
			
			print_message(LOGLEVEL_FAILURE, "Timed out waiting for a connection to establish itself", LOG_TAG);
		}
		else
		{
			print_message(LOGLEVEL_FAILURE, "Couldn't accept a new connection ($!)", LOG_TAG);
		}
		return 1;
	}
	
	#TURN OFF BUFFERING AND SET TO BINARY MODE
	my $oldh = select($childsocket);
	$| = 1;
	select($oldh);
	binmode $childsocket;
	
	$self->{autoreconnect} = "false";			#DISABLE RECONNECT; WE'RE ACCEPTING, NOT CONNECTING
	$self->close_socket();						#CLOSE THE LISTENING SOCKET IF IT WAS US, MAKE PERMANENT THE NEW CONNECTION
	$self->{sock} = $childsocket;
	$self->get_remote_address();
	$self->get_remote_port();
	
	
	
	#IF WE'RE WANTED TO HANDSHAKE, THEN DO SO
	$self->{handshake} = $parent->{handshake} if $parent->{handshake};
	if($self->{handshake} && $NO_HANDSHAKES ne "true")
	{
		$self->send_header($self->{handshake});
		print_message(LOGLEVEL_TRACE, "accept: Sent header " . $self->{handshake}, LOG_TAG);
		my ($handshake) = $self->receive_header();
		$handshake = "" unless $handshake;
		print_message(LOGLEVEL_TRACE, "accept: Received header $handshake", LOG_TAG);
		
		if($handshake ne $self->{handshake})
		{
			$self->close_socket();
			print_message(LOGLEVEL_FAILURE, "Spurious handshake '$handshake' did not match the expected '" . $self->{handshake} . "'", LOG_TAG);
			return 1;
		}
		
		Logger::print_message(LOGLEVEL_TRACE, "$handshake (Handshake verified, accepted successfully)", LOG_TAG);
	}
	else
	{
		print_message(LOGLEVEL_TRACE, "accept: Not sending handshake", LOG_TAG);
	}
	
	$self->{mode} = "Accepted";
	
	return 0;
}




###################################################################
## connect (remotehost, reportport)
## ---------------------------------
## Connect to a listening port on a given host
###################################################################
sub connect
{
	my ($self, $remotehost, $remoteport, $reconnecting) = @_;
	
	$self->close_socket();
	
	#CREATE THE SOCKET
	$self->{remotehost} = $remotehost;
	$self->{remoteport} = $remoteport;
	$self->{original_remoteport} = $remoteport;
	
	my $socket;
	my $retrycount = $self->{retrycount};
	#while($retrycount-- > 0)
	my $mark = time();
	my $lastmark = time();
	while(time() < $mark + ($self->{timeout} || 30))
	{
		my $address = sockaddr_in($remoteport, inet_aton($remotehost));
		$socket = new IO::Socket::INET (
			Proto		=> 'tcp',
			Timeout		=> 1,
			Blocking		=> 0,
			);
		
		if($socket->connect($address))
		{
			if($! == EINPROGRESS)
			{
				my $sockets = IO::Select->new($socket);
				if(!$sockets->can_write($self->{timeout} || 30))
				{
					print_message(LOGLEVEL_FAILURE, "Timed out trying to connect to the $remotehost:$remoteport", LOG_TAG);
					return undef;
				}
			}
			
			last if $socket->connected || $! == EISCONN;
		}
		
		#RETRY, BUT GIVE THE SERVER A FEW SECONDS TO RECOVER IN CASE IT'S BEING
		#HIT WITH MORE CONNECTIONS THAN IT CAN HANDLE
		usleep(100);
		if($lastmark < time() - 5)
		{
			print_message(LOGLEVEL_WARNING, "Still trying to connect to $remotehost:$remoteport (mark is " . int(time() - $mark) . " seconds old)", LOG_TAG);
			$lastmark = time();
		}
		#print_message(LOGLEVEL_WARNING, "Couldn't connect to remotehost ($@, $!). " . ($retrycount ? "Retrying $retrycount more times" : "Final attempt") . "...", LOG_TAG);
		#sleep(int(rand(3 * (($self->{retrycount} - $retrycount) + 1))));
	}
	
	if(!defined $socket)
	{
		print_message(LOGLEVEL_FAILURE, "Couldn't create a socket to connect with ($@, $!)", LOG_TAG);
		return undef;
	}
	if(!$socket->connected)
	{
		print_message(LOGLEVEL_FAILURE, "Couldn't connect to the remotehost", LOG_TAG);
		return undef;
	}
	
	
	#TURN OFF BUFFERING AND SET TO BINARY MODE
	my $oldh = select($socket);
	$| = 1;
	select($oldh);
	binmode $socket;
	
	$self->{sock} = $socket;
	$self->get_remote_address();
	$self->get_remote_port();
	
	
	#VALIDATE A HANDSHAKE WITH THE OTHER SOCKET IF REQUIRED
	if($self->{handshake} && $NO_HANDSHAKES ne "true")
	{{
		#SOMETIMES WE CAN AUTO-RECONNECT DURING THE HANDSHAKE. THIS FLAG ENSURES WE DON'T OVER-COMPENSATE
		$self->{midhandshake} = "true";
		
		$self->send_header($self->{handshake});
		last unless $self->{midhandshake};		#RETURN IF A RE-CONNECT BEAT US TO THE HANDSHAKE
		
		print_message(LOGLEVEL_TRACE, "connect: Sent header " . $self->{handshake}, LOG_TAG);
		
		
		my ($handshake) = $self->receive_header();
		last unless $self->{midhandshake};		#RETURN IF A RE-CONNECT BEAT US TO THE HANDSHAKE
		
		$handshake = "" unless $handshake;
		print_message(LOGLEVEL_TRACE, "connect: Received header $handshake", LOG_TAG);
		
		if($handshake ne $self->{handshake})
		{
			print_message(LOGLEVEL_FAILURE, "Connected to a socket whose handshake '$handshake' didn't match what we expected ('" . $self->{handshake} . "')", LOG_TAG);
			return undef;
		}
	
		Logger::print_message(LOGLEVEL_TRACE, "$handshake (Handshake verified, connected successfully)", LOG_TAG);
		
		#WE SUCCESFULLY HANDSHOOK. CLEAR UP.
		delete $self->{midhandshake};
	}}
	
	$self->{mode} = "Connected";
	return 0;
}




###################################################################
## close_socket
## ---------------------------------
## Closes the current socket
###################################################################
sub close_socket
{
	my ($self) = @_;
	
	#ALREADY CLOSED
	return 0 if ($self->{remotehost} || "") eq "0.0.0.0" &&
				($self->{localport} || 0) eq 0 &&
				($self->{remoteport} || 0) eq 0;
	
	#IF WE HAVE A SOCKET OPEN, CLOSE IT
	if(defined $self->{sock})
	{
		$self->{sock}->close();
		$self->{sock} = undef;
	}
	
	$self->{remotehost} = "0.0.0.0";
	$self->{localport} = 0;
	$self->{remoteport} = 0;
	$self->{original_remoteport} = 0;
	$self->{mode} = "";
	
	return 0;
}






###################################################################
## synchronise()
## ---------------------------------
## Waits for the other process to reach a synchronised state
###################################################################
sub synchronise
{
	my ($self) = @_;
	
	$self->send_header("--==SYNC==--");
	$self->receive_header();
	
	return 0;
}







###################################################################
## processdata() --> [packettype]
## ---------------------------------
## Grab the next valid packet into the buffer, returning the
## packet type when done
###################################################################
sub process_data
{
	my ($self) = @_;
	
	return $self->__grab_data();
}





###################################################################
## send_error(message)
## ---------------------------------
## Send an error message to the remote host
###################################################################
sub send_error
{
	my ($self, $message) = @_;
	
	my $packet = "";
	return undef if !$self->__send_data("E", \$message);
	
	$self->{lastsize} = 0;
	
	return 0;
}




###################################################################
## receive_error() --> [errormessage]
## ---------------------------------
## Parse the buffer and retrieve an error message
###################################################################
sub receive_error
{
	my ($self) = @_;
	
	
	#IF THE BUFFER IS EMPTY, GRAB SOMETHING AUTOMATICALLY AND HOPE IT'S AN ERROR PACKET
	$self->__grab_data() if $self->{packettype} eq "";
	if($self->{packettype} ne "E")
	{
		print_message(LOGLEVEL_WARNING, "Trying to process an error packet that is actually '" . $self->{packettype} . "'", LOG_TAG);
		return undef
	}
	
	my $error = $self->{packetdata};
	$self->__reset_data();
	
	return $error;
}



###################################################################
## send_header({values, ...})
## ---------------------------------
## Send a collection of values to the remote host
###################################################################
sub send_header
{
	my ($self, @values) = @_;
	
	
	#CONSTRUCT THE HEADER DATA (IGNORE undefs)
	my $packet = "";
	foreach(@values) { $_ =~ s/\|/§§/g; $packet .= encode_utf8($_) . "|" if defined $_; }
	$packet = substr($packet, 0, -1);
	
	
	print_message(LOGLEVEL_TRACE, "Sent header: '" . join("', '", @values) . "'", LOG_TAG);
	return undef if !$self->__send_data("S", \$packet);
	
	
	#CLEAN UP IN CASE WE DIE
	$self->{lastsize} = 0;
	
	return 0;
}



###################################################################
## receive_header() --> {elementlist, ...}
## ---------------------------------
## Listen for and receive a given header message from the remote
## host, returning it as an array of entries
###################################################################
sub receive_header
{
	my ($self, $timeout) = @_;
	
	
	#IF THE BUFFER IS EMPTY, AUTOMATICALLY GRAB THE NEXT PACKET AND HOPE IT'S A STANDARD HEADER
	if($self->{packettype} eq "")
	{
		return undef if !defined $self->__grab_data($timeout);
		$self->{packetdata} = $self->{packetdata};
	}
	
	if($self->{packettype} ne "S")
	{
		print_message(LOGLEVEL_WARNING,"Received packet type '" . $self->{packettype} . "' when expecting 'S'", LOG_TAG);
		print_message(LOGLEVEL_WARNING,"Data: '" . $self->{packetdata} . "'", LOG_TAG );
		print_message(LOGLEVEL_FAILURE, "Trying to process a standard header packet, but the next queue item is not a standard header", LOG_TAG);
		return undef;
	}
	
	#GRAB THE VALUES FROM THE HEADER
	chomp($self->{packetdata});
	my @headerinfo = split(/\|/, $self->{packetdata});
	foreach(@headerinfo) { $_ = decode_utf8($_); $_ =~ s/§§/\|/g; }
	
	
	$self->__reset_data();
	
	print_message(LOGLEVEL_TRACE, "Received header: '" . join("', '", @headerinfo) . "'", LOG_TAG);
	
	return @headerinfo;
}


sub shift_header
{
	my ($self) = @_;
	
	
	#IF THE BUFFER IS EMPTY, AUTOMATICALLY GRAB THE NEXT PACKET AND HOPE IT'S A STANDARD HEADER
	if($self->{packettype} eq "")
	{
		$self->__grab_data();
		$self->{packetdata} = $self->{packetdata};
	}
	if($self->{packettype} ne "S")
	{
		print_message(LOGLEVEL_FAILURE, "Trying to shift a standard header, but the next queue item is not a standard header ($self->{packettype})", LOG_TAG);
		return undef;
	}
	
	#GRAB THE FIRST VALUE FROM THE HEADER
	chomp($self->{packetdata});
	my $header;
	my $index = index($self->{packetdata}, '|');
	if($index == -1 || $index >= length($self->{packetdata}) - 1)
	{
		$header = $self->{packetdata};
		$self->{packetdata} = "";
	}
	else
	{
		$header = substr($self->{packetdata}, 0, $index);
		$self->{packetdata} = substr($self->{packetdata}, $index + 1);
	}
	$self->{packetlen} = length($self->{packetdata});
	
	
	
	$self->__reset_data() if $self->{packetlen} == 0;
	
	
	return $header;
}




###################################################################
## send_data_block(offset, data, size)
## ---------------------------------
## Send a checksum-verified chunk of data to the remote host
###################################################################
sub send_data_block
{
	my ($self, $blockoffset, $blockdata, $blocksize) = @_;
	
	
	#GENERATE CHECKSUM
	my $csum = unpack "%64A*", $blockdata;
	#$csum = "BADWOLF" if int(rand(10)) > 3;			#BAD CHECKSUM TEST
	$blockoffset = "0" if !defined $blockoffset;
	
	#SEND THE DATA HEADER, THEN THE DATA PACKET
	return undef if !defined $self->__send_data("H", \"$csum|$blockoffset|$blocksize");
	return undef if !defined $self->__send_data("D", \$blockdata);
	
	
	
	#CLEAR LAST RECEIVED SIZE
	$self->{lastsize} = 0;
	
	return 0;
}




###################################################################
## receive_data_block() --> {offset, size, data}
## ---------------------------------
## Listen for and receive a chunk of checksummed data from the
## remote host
###################################################################
sub receive_data_block
{
	my ($self) = @_;
	
	
	#IF THE BUFFER IS EMPTY, GRAB A HEADER AND HOPE IT'S A DATA HEADER
	$self->__grab_data() if $self->{packettype} eq "";
	if($self->{packettype} ne "H")
	{
		print_message(LOGLEVEL_FAILURE, "Trying to process a data packet's header, but the next queue item is not a data header packet", LOG_TAG);
		return undef;
	}
	
	chomp($self->{packetdata});
	my ($cksum, $blockoffset, $blocksize) = split(/\|/, $self->{packetdata});
	
	
	#GRAB THE NEXT PACKET, WHICH SHOULD BE A DATA PACKET
	$self->__grab_data();
	if($self->{packettype} ne "D")
	{
		print_message(LOGLEVEL_FAILURE, "Trying to process a data packet, but the next queue item is not a data packet", LOG_TAG);
		return undef;
	}
	if($self->{packetlen} != $blocksize)		#WE DIDN'T GET ALL OF THE DATA PACKET, BUT PROBABLY EOF'D, SO RETURN
	{
		print_message(LOGLEVEL_FAILURE, "Trying to process a data packet, but the length does not match what was predicted in the data  header", LOG_TAG);
		return undef;
	}
	
	
	my $d = $self->{packetdata};
	#my $data = \($self->{packetdata});
	my $data = \$d;
	print_message(LOGLEVEL_TRACE, "Received " . length($$data) . " bytes of unstructured data", LOG_TAG);
	
	my $csum = unpack "%64A*", $$data;
	if($csum ne $cksum)
	{
		print_message(LOGLEVEL_WARNING, "Packet failed checksum validation", LOG_TAG);
		return undef;
	}
	
	$self->__reset_data();
	
	print_message(LOGLEVEL_TRACE, "$blocksize :: " . length($data), LOG_TAG);
	return ($blockoffset, $blocksize, $data);
}


sub get_local_domain_name
{
    my $host = hostname();
    
    return $host;
}

sub get_local_address
{
	my ($self) = @_;
	
	if(!$self->{sock})
	{
		#USE CACHED VERSIONS IF AVAILABLE
		return $self->{localhost} if $self->{localhost};
		
		my $sock = IO::Socket::INET->new(PeerAddr=> "example.com", PeerPort=> 80, Proto => "tcp");
		
		$self->{localhost} = $sock->sockhost();
		$self->{localport} = $sock->sockport();
	}
	
	else
	{
		$self->{localhost} = $self->{sock}->sockhost();
	}
	
	return $self->{localhost};
}

sub get_local_port
{
	my ($self) = @_;
	
	if(!$self->{sock})
	{
		return $self->{localport} if $self->{localport};
		
		my $sock = IO::Socket::INET->new(PeerAddr => "example.com", PeerPort => 80, Proto => "tcp");
		
		$self->{localhost} = $sock->sockhost();
		$self->{localport} = $sock->sockport();
	}
	else
	{
		$self->{localport} = $self->{sock}->sockport();
	}
	
	return $self->{localport};
}


sub get_remote_address
{
	my ($self) = @_;
	
	return "" if !$self->{sock};
	$self->{remotehost} = $self->{sock}->peerhost();
	return $self->{remotehost};
}

sub get_remote_port
{
	my ($self) = @_;
	
	return "" if !$self->{sock};
	$self->{remoteport} = $self->{sock}->peerport();
	return $self->{remoteport};
}

###################################################################
## get_last_read_size() --> readsize
## ---------------------------------
## Get the bytecount of the last chunk of data read, even when
## the chunkread failed
###################################################################
sub get_last_read_size
{
	my ($self) = @_;
	
	return $self->{lastsize};
}



sub is_connected
{
	my ($self) = @_;
	
	return defined $self->{sock}->connected() ? 1 : 0 if defined $self->{sock};
	return 0;
}





###################################################################
## get_error_message() -> [message]
## ---------------------------------
## Return the status message where the state is STATE_WARN or
## STATE_ERROR
###################################################################
sub get_error_message
{
	my ($self) = @_;
	
	return "" if $self->{sockstate} == STATE_NORMAL;
	chomp($self->{lastmsg});
	
	return $self->{lastmsg};
}



###################################################################
## get_fatal_message() -> [fatalmessage]
## ---------------------------------
## Return the status message where the state is STATE_ERROR
###################################################################
sub get_fatal_message
{
	my ($self) = @_;
	
	return "" if $self->{sockstate} == STATE_NORMAL || $self->{sockstate} == STATE_WARN;
	chomp($self->{lastmsg});
	
	return $self->{lastmsg};
}



sub die_if_fatal_message
{
	my ($self) = @_;
	
	return undef if $self->{sockstate} != STATE_FATAL && $self->{sockstate} != STATE_RESET;
	print_message(LOGLEVEL_FAILURE, "Connection was dropped, but recovered and so the system state needs resetting", LOG_TAG) if $self->{sockstate} == STATE_RESET;
	
	print_message(LOGLEVEL_FATAL, "Socket listening on port " . $self->{localport} . " died (" . $self->{lastmsg} . ")", LOG_TAG) if $self->{remotehost} eq "0.0.0.0";
	print_message(LOGLEVEL_FATAL, "Socket conected to " . $self->{remotehost} . ":" . $self->{remoteport} . " died (" . $self->{lastmsg} . ")", LOG_TAG);
}
















###################################################################
###################################################################
##																##
##=====================[PRIVATE SUBROUTINES]=======================
##																##
##					  ****HERE BE DRAGONS****					##
##																##
###################################################################
###################################################################









###################################################################
## __grab_data() --> [packettype]
## ---------------------------------
## Grab the next packet from the stream and store it in a buffer
###################################################################
sub __grab_data
{
	my ($self, $timeout) = @_;
	my $begin = time();
	
retry:
	#MAKE SURE WE CAN ACTUALLY READ THE SOCKET
	if(!defined $self->{sock} || !$self->is_connected())
	{
		print_message(LOGLEVEL_FAILURE, "__grab_data: Socket is not open for communication", LOG_TAG);
		return undef;
	}
	
	
	#GET THE PACKET HEADER
	my $data;
	my $tempdata;
	my $datalen = 0;
	my $templen = 0;
	my $retrycount = $self->{retrycount};
	
	my $mark = time();
	my $iloops = 0;
	my $waitlist = IO::Select->new($self->{sock});
	while($datalen < 9)
	{
		my $iloops++;
		usleep(100);
		
		if($waitlist->can_read($timeout || 0)) { $templen = $self->{sock}->sysread($tempdata, 9 - $datalen); } else { $templen = 0; $! = EWOULDBLOCK; }
		if(($templen || 0) > 0)
		{
			#EVERYTHING'S FINE
			print_message(LOGLEVEL_TRACE, "Got $templen bytes", LOG_TAG);
			$mark = time();			#RESET THE TIMEOUT
			$data .= $tempdata;
			$datalen += $templen;
			
			last if $datalen == 9;
			next;
		}
		#AN IRRECOVERABLE ERROR HAPPENED (i.e. WE CAN'T JUST WAIT IT OUT)
		elsif($! != EWOULDBLOCK && $! != EAGAIN && $! != EINTR && $! != EINPROGRESS)
		{
			my $err = $!;
			
			#DISCONNECTION
			print_message(LOGLEVEL_FAILURE, "__grab_data: Connection was dropped ($@, $!).", LOG_TAG); # if !defined $self->__reconnect();
			$self->{sock}->close();
			#print_message(LOGLEVEL_INFO, "Connection was dropped, but was re-established ($err)", LOG_TAG);
			return undef;
		}
		## OPTIONAL TIMEOUT FOR NONBLOCKING REQUESTS
		elsif(defined $timeout)
		{
			return undef if time() - $mark > $timeout;
		}
		
		usleep(100);
		print_message(LOGLEVEL_FAILURE, "Connection dropped whilst trying to receive data", LOG_TAG) if !$self->is_connected();
	}
	my $interim = time();
	
	
	#WE DIDN'T RECEIVE THE WHOLE HEADER
	if($datalen != 9)
	{
		usleep(10);
		print_message(LOGLEVEL_FAILURE, "Packet header is truncated", LOG_TAG);
		return undef;
	}
	
	
	
	#PARSE THE HEADER'S DATA [UNPACK THE PACKET TYPE AND SIZE]
	my $packettype = substr($data, 0, 1);
#	goto retry if $packettype eq "Q";
	if($packettype !~ m/[NESHD]/g) { print_message(LOGLEVEL_FAILURE, "Received an invalid packet [$packettype]", LOG_TAG); return undef; }
	my $packetlength = unpack("N", substr($data, 1));
	
	
	print_message(LOGLEVEL_TRACE, "__grab_data: Received a$packettypes->{$packettype} packet", LOG_TAG);
	
	
	#GRAB THE PACKET PAYLOAD (i.e. THE AMOUNT OF DATA DESCRIBED BY THE HEADER)
	$mark = time();
	$retrycount = 0;
	my $bytesleft = $packetlength;
	$datalen = 0; $data = "";
	
	my $loops = 0;
	my $errors = 0;
	while($bytesleft > 0)
	{
		$loops++;
		$tempdata = ' ' x $bytesleft;
		if($waitlist->can_read($timeout || 0)) { $templen = $self->{sock}->sysread($tempdata, $bytesleft); } else { $templen = 0; $! = EWOULDBLOCK; }
		my $err = $!;
		
		if(($templen || 0) > 0)
		{
			$mark = time();
			$data .= $tempdata;
			$datalen += $templen;
			$bytesleft -= $templen;
		}
		elsif($err == EWOULDBLOCK || $err == EAGAIN || $err == EINTR || $err == EINPROGRESS)
		{
			return undef if (time() - $mark > ($timeout || 0)) && defined $timeout;
			#print_message(LOGLEVEL_WARNING, "Would block ($err)", LOG_TAG);
		}
		elsif(defined $err)
		{
			$errors++;
			#SELF-RECOVERING DISCONNECTION
			if(1) #!defined $self->__reconnect())
			{
				$self->{sock}->close();
				print_message(LOGLEVEL_FAILURE, "Connection was dropped '$@, $err'", LOG_TAG);
				return undef;
			}
			else
			{
				print_message(LOGLEVEL_FAILURE, "Connection was dropped, and subsequently re-established whilst trying to read data", LOG_TAG);
				return undef;
			}
			
			#TIMEOUT IF WE TAKE TOO LONG TO RECEIVE ANY DATA
			#next unless defined $self->{timeout};
			print_message(LOGLEVEL_FAILURE, "Connection dropped whilst trying to receive data", LOG_TAG) if !$self->is_connected();
		}
		else
		{
			print_message(LOGLEVEL_WARNING, "No data, but not blocking?", LOG_TAG);
		}
	}
	if($bytesleft > 0)
	{
		print_message(LOGLEVEL_WARNING, "Tried to read more bytes from the stream than were available", LOG_TAG);
		return undef;
	}
	
	
	
	#BUFFER THE DATA AND RETURN THE PACKET TYPE
	$interim = time();
	$self->{packettype} = $packettype;
	$self->{packetdata} = $data;
	$self->{packetlen} = length($data);
	
	my $end = time();
	#print_message(LOGLEVEL_TRACE, "Received in  $loops / $iloops loops ($errors errors) in " . ($end - $begin) . " seconds, frontloaded at " . ($interim - $begin) . " seconds", LOG_TAG);
	
	return $packettype;
}





sub __resend_last_packet
{
	my ($self) = @_;
	
	$self->__send_data($self->{sendtype}, \($self->{senddata}));
}



###################################################################
## new ([timeout]) --> [transfersocketobject]
## ---------------------------------
## Create a new version of this class with an optional timeout
## (defaults to 5 seconds).
###################################################################
sub __send_data
{
	my ($self, $datatype, $data) = @_;
	my $begin = time();
	
	if(!defined $self->{sock})
	{
		print_message(LOGLEVEL_FAILURE, "Socket is not open for writing", LOG_TAG);
		return undef;
	}
	
	#PACK THE PACKET TYPE AND LENGTH INTO A 9-BYTE HEADER
	my $packedlength = pack("N", length($$data));
	$packedlength = $packedlength . (" " x (8 - length($packedlength)));
	my $packet = substr($datatype, 0, 1) . $packedlength . $$data;
	
	
	#SEND THE PACKET
	my $bytesremaining = length($packet);
	my $dataremaining = $packet;
	$self->{lastsentdata} = $packet;
	my $datalen;
	my $mark = time();
	
	my $interim = time();
	
	my $loops = 0;
	my $errors = 0;
	while($bytesremaining > 0)
	{
		$loops++;
		$datalen = $self->{sock}->syswrite($dataremaining, $bytesremaining);
		
		if(defined $datalen)
		{
			$bytesremaining -= $datalen;
			if($datalen + 1 > length($dataremaining))	{ $dataremaining = ""; }
			else											{ $dataremaining = substr($dataremaining, $datalen); }
			
			$mark = time();
			next;
		}
		elsif($! != EWOULDBLOCK && $! != EAGAIN && $! != EINTR && $! != EINPROGRESS)
		{
			$errors++;
			my $err = $!;
			#DISCONNECTION
			if(1) #!defined $self->__reconnect())
			{
				$self->{sock}->close();
				print_message(LOGLEVEL_FAILURE, "Connection dropped whilst writing data (Datatype: '$datatype'; Error: $err)", LOG_TAG);
				return undef;
			}
			else
			{
				print_message(LOGLEVEL_FAILURE, "Connection dropped, but was subsequently re-established while sending data ($err happened)", LOG_TAG);
				return undef;
			}
		}
	}
	
	#RETURN AN ERROR IF WE DIDN'T SEND IT ALL
		print_message(LOGLEVEL_WARNING, "Didn't manage to send all of the data (Datatype: '$datatype')", LOG_TAG) if $bytesremaining;
		my $end = time();
		#print_message(LOGLEVEL_TRACE, "Did $loops loops with $errors errors in " . ($end - $begin) . " seconds, front loaded at " . ($interim - $begin) . "seconds.\n", LOG_TAG);
	
	
	#RETURN THE NUMBER OF BYTES SENT
	return length($packet) - $bytesremaining;
}








###################################################################
## __reset_data()
## ---------------------------------
## Clear the packet buffer
###################################################################
sub __reset_data
{
	my ($self) = @_;
	
	$self->{packettype} = "";
	$self->{packetdata} = "";
	$self->{packetlen} = 0;
}



sub __reconnect()
{
	my ($self) = @_;
	
	
	
	return undef if $self->{autoreconnect} eq "false";
	
	return $self->connect($self->{remotehost}, $self->{remoteport}, "true");
}


1;
