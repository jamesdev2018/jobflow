#!/usr/local/bin/perl





####	############################################################	##
####	############################################################	##
##   A CLASS FOR QUICKLY AND SIMPLY REGISTERING CUSTOM INPUT		##
##			PARAMETER PASSING FROM THE COMMAND LINE				##
####	############################################################	##
####	############################################################	##




package OptionParser;
our $VERSION = 2.0;		#VERSION STRING FOR PRE-5.10 PERL VARIANTS

use strict;
use warnings;

use constant NONE_GROUP => "none";

use Logger qw/:loglevels :logmodes :timermodes/;		#SIMPLE BUT FLEXIBLE DEBUG LOGGING AND OUTPUT
my $LOG_TAG = "OPT";

my $indent = "     ";		#A GIVEN NUMBER OF SPACES FOR A \t SO WE CAN MAINTAIN INDENTATION


sub new
{
	my $class = shift;
	
	my $self = {	};
	bless $self, $class or die "$!\n";
	
	$self->{canrun} = 1;
	
	return $self;
}


sub add_description
{
	my ($self, $description) = @_;
	
	$self->{description} = $description;
}


###################################################################
## add_option(options, description, variable, defvalue, group, optional)
## ---------------------------------
## Adds an option to the checking list. Arguments will check against
## the short and/or long options and either update variable with the new
## parameter or run a given function with the parameter as its first
## argument (depending on whether variable is a reference to a scalar
## or a function). If no value is passed, defvalue is used as the first
## argument
###################################################################
sub add_option
{
	my ($self, $options, $description, $binding, $default, $group, $modifier) = @_;
	
	$options = "" if !defined $options;
	$description = "[No description entered]" if !defined $description || $description eq "";
	$modifier = "" if !defined $modifier;
	$group = NONE_GROUP if !defined $group;
	$binding = "" if !defined $binding;
	
	Logger::print_message(LOGLEVEL_FATAL, "Script error: tried to add an option without a parsable name", $LOG_TAG) if $options eq "";
	Logger::print_message(LOGLEVEL_FATAL, "Script error: tried to set an option without tying it to a scalar variable or subroutine", $LOG_TAG) if $binding && ref($binding) ne "SCALAR" && ref($binding ne "CODE");
	
	
	
	my @opts = sort { length($a) <=> length($b) } split(/ /, $options);
	
	my $mandatory = index($modifier, "mandatory") != -1;
	my $triggerable = index($modifier, "triggerable") != -1;
	my $indexed = index($modifier, "indexed") != -1;
	my $hidden = index($modifier, "hidden") != -1;
	
	my $option = {	options		=> \@opts,
					description	=> $description,
					binding		=> $binding,
					defaultval	=> $default,
					mandatory	=> $mandatory,
					triggerable	=> $triggerable,
					indexed		=> $indexed,
					hidden		=> $hidden,
					
					matched		=> "false",
			  		};
	
	Logger::print_message(LOGLEVEL_TRACE, "Adding option '$options' to group $group", $LOG_TAG);
	
	my $p = $self->{parameters};
	push(@$p, $option);
	$self->{parameters} = $p;
	
	my $groups = $self->{groups}->{$group} || ();
	push(@$groups, $option);
	$self->{groups}->{$group} = $groups;
}



###################################################################
## add_help_option()
## ---------------------------------
## Shorthand for implementing a help-text, with option for overriding
## the default shorthand in case '-h' or '--help' are already used
###################################################################
sub add_help_option
{
	my ($self, $options) = @_;
	
	$options = "? h help" if !defined $options;
	$self->add_option($options, "Display this help text", sub { $self->display_options(); }, undef, undef, "triggerable hidden");
	$self->{helpenabled} = "true";
}



###################################################################
## add_noise_options(), add_quiet_option(), add_verbose_option(),
## add_super_verbose_option()
## ---------------------------------
## Shorthand functions for implementing quiet, verbose and super-
## verbose logging options. add_noise_options() is a quick way
## of adding all three in one go
###################################################################
sub add_ui_options
{
	my ($self) = @_;
	
	$self->add_help_option();
	$self->add_quiet_option();
	$self->add_verbose_option();
	$self->add_super_verbose_option();
	$self->add_extended_verbose_option();
	$self->add_log_option();
	$self->add_debug_log_option();
}

sub add_quiet_option
{
	my ($self, $options) = @_;
	
	$options = "q quiet" if !defined $options;
	$self->add_option($options, "Only report fatal errors",
		sub
		{
			Logger::remove_exposure("stdout");
			Logger::add_exposure("stderr", LOGLEVEL_FATAL | LOGLEVEL_SPECIFIC);
			Logger::print_message(LOGLEVEL_DEBUG, "Quiet output enabled", $LOG_TAG);
		}, undef, "noise", "triggerable");
}

sub add_verbose_option
{
	my ($self, $options) = @_;
	
	$options = "v verbose" if !defined $options;
	$self->add_option($options, "Increase verbosity level to include DEBUG output",
		sub
		{
			Logger::increase_exposure("stdout", LOGLEVEL_TEST | LOGLEVEL_DEBUG);
			Logger::print_message(LOGLEVEL_TRACE, "Verbose output enabled", $LOG_TAG);
		}, undef, "noise", "triggerable");
}

sub add_super_verbose_option
{
	my ($self, $options) = @_;
	
	$options = "vv very-verbose" if !defined $options;
	$self->add_option($options, "Maximum verbosity level, includes DEBUG and TRACE output",
		sub
		{
			Logger::increase_exposure("stdout", LOGLEVEL_TEST | LOGLEVEL_DEBUG | LOGLEVEL_TRACE, undef, undef, TIMER_HIRES);
			Logger::print_message(LOGLEVEL_TRACE, "Super-verbose output enabled", $LOG_TAG);
		}, undef, "noise", "triggerable");
}


sub add_extended_verbose_option
{
	my ($self, $options) = @_;
	
	$options = "vx verbose-extended" if !defined $options;
	$self->add_option($options, "Maximum verbosity level, includes DEBUG and TRACE output, with automatic terminal width detection",
		sub
		{
			Logger::increase_exposure("stderr", undef, MODE_ADVANCED);
			Logger::increase_exposure("stdout", LOGLEVEL_TEST | LOGLEVEL_DEBUG | LOGLEVEL_TRACE, MODE_ADVANCED, undef, TIMER_HIRES);
			Logger::print_message(LOGLEVEL_TRACE, "Extended-verbose output enabled", $LOG_TAG);
		}, undef, "noise", "triggerable");
}

sub add_log_option
{
	my ($self, $options) = @_;
	
	$options = "lf logfile" if !defined $options;
	$self->add_option($options, "Write output to a log file",
		sub
		{
			my ($filename) = @_;
			Logger::add_exposure($filename, LOGLEVEL_INFO);
			Logger::print_message(LOGLEVEL_INFO, "Started writing output to file $filename", $LOG_TAG);
		}, undef, "log", undef);
}

sub add_debug_log_option
{
	my ($self, $options) = @_;
	
	$options = "df debugfile" if !defined $options;
	$self->add_option($options, "Write debug output to a log file",
		sub
		{
			my ($filename) = @_;
			Logger::add_exposure($filename, LOGLEVEL_TRACE);
			Logger::print_message(LOGLEVEL_INFO, "Started writing debug output to file $filename", $LOG_TAG);
		}, undef, "log", undef);
}

sub add_multirun_option
{
	my ($self, $options) = @_;
	
	$options = "m multirun" if !defined $options;
	$self->add_option($options, "Allow this script to run even if an other instance is already active",
		sub
		{
			$self->{canrun} = 1;
		}, undef, undef, "triggerable");
	
	
	$self->{canrun} = -1;
}



###################################################################
## display_options()
## ---------------------------------
## Shows a brief help screen briefly detailing all options that
## are available on the command line
###################################################################
sub display_options
{
	my $self = shift;
	
	
	#DESCRIPTION OF THE SCRIPT
	print "Description: $indent" . $self->{description} . "\n\n" if $self->{description};
	
	
	#BRIEF OVERVIEW OF THE OPTIONS AVAILABLE TO USE
	print "Usage:$indent$0";
	my $groups = $self->{groups};
	while(my ($key, $value) = each(%$groups))
	{
		next if @$value > 1 && @$value[0]->{hidden};
		
		print " ";
		print "(" if @$value > 1 && $key ne NONE_GROUP;
		
		my $val = 0;
		foreach(@$value)
		{
			next if $_->{hidden};
			
			print "" . ($key eq NONE_GROUP ? " " : "|") if $val++ > 0;
			print "[" if !$_->{mandatory};
			my $options = $_->{options};
			my $option = @$options[0];
			print "" . (length($option) <= 2 ? "-" : "--") . $option;
			print "=*" unless $_->{triggerable};
			print "]" if !$_->{mandatory};
		}
		
		print ")" if @$value > 1 && $key ne NONE_GROUP;
	}
	
	print "\n\n";
	
	
	#WRITE THE MORE IN-DEPTH OPTION INFORMATION
	my $params = $self->{parameters};
	my @orderedparams =  sort { $a->{options}[0] cmp $b->{options}[0] } @$params;
	foreach(@orderedparams)
	{
		next if $_->{hidden};
		
		my $text = "$indent";
		my $val = 0;
		my $o = $_->{options};
		foreach my $opt (@$o)
		{
			$text .= ", " if $val++ > 0;
			$text .= Logger::modify_message(Logger::MODE_NORMAL, 
						"bold",
						*STDOUT,
						(length($opt) <= 2 ? "-" : "--") . $opt);
		}
		$text .= Logger::modify_message(Logger::MODE_NORMAL, "reset", *STDOUT, "");
		print $text;
		
		#MAKE SURE THAT EACH LINE WRAPS AROUND THE TERMINAL PROPERLY, INCLUDING INDENTATION
		my $desc = $_->{description};
		my $linewidth = Logger::get_terminal_width(Logger::MODE_NORMAL) - (length($indent) * 2);
		$desc =~ s/(.{$linewidth})/$1\n/g;
		
		#REPAIR INDENTATION
		$desc =~ s/(^|\n)/$1$indent$indent/g;
		
		
		#OUTPUT
		print "\n" . $desc;
		print "\n$indent${indent}Defaults to: " . $_->{defaultval} if $_->{defaultval} && !$_->{triggerable};
		print "\n";
	}
	
	print "\n\n";
	exit;
}



###################################################################
## process_options([arguments])
## ---------------------------------
## Runs through the commandline arguments passed to the script and
## checks against any commands added with add_option. Where
## arguments are passed in without a dash (-, --), update based on
## argument position
###################################################################
sub process_options
{
	my ($self, $arguments) = @_;
	
	#CLONE THE GROUPS, SINCE WE DON'T WANT OUR DELETES TO PROPAGATE
	#PAST THIS SUBROUTINE
	my $selfgroups = $self->{groups};
	my %newgroups = %$selfgroups;
	my $groups = \%newgroups;
	my @args = @ARGV;
	
	@args = (get_arguments($arguments), @ARGV) if defined $arguments;
	
	
	my $argpos = 1;
	for(my $argindex = 0; $argindex < @args; $argindex++)
	{
		my ($command, $parameter) = split("=", $args[$argindex]);
		utf8::decode($command) if $command;
		utf8::decode($parameter) if $parameter;
		
		#WE'RE USING A NAMED OPTION
		if(substr($command, 0, 1) eq "-")
		{
			keys %$groups;	#RESET THE each OPERATOR
			#FOR EACH VALUE IN EACH GROUP
			while(my ($key, $value) = each(%$groups))
			{
				foreach my $option (@$value)
				{
					#FOR EACH ACCEPTIBLE TAG IN AN OPTION ("-h", "-t", "--sample-tag", etc.)
					my $opt = $option->{options};
					foreach my $tag (@$opt)
					{
						my $catch = (length($tag) > 2 ? "--" : "-") . $tag;
						
						#COMMAND MATCHES
						if(lc($command) eq lc($catch))
						{
							#IF OPTION IS TRIGGERABLE, SET THE PARAMETER WITH THE 'DEFAULT' VALUE
							if($option->{triggerable} && !defined $parameter) { $parameter = $option->{defaultval}; }
							$option->{matched} = "true";
							
							my $var = $option->{binding};
							Logger::print_message(LOGLEVEL_FATAL, "Must supply an argument to member " . $option->{options}[0], $LOG_TAG) if !$option->{triggerable} && !defined $parameter;
							__call_binding($option->{binding}, $parameter);
							
							delete $groups->{$key} if $key ne NONE_GROUP;
							goto FOUNDNAMEDTAG;
						}
					}
				}
			}
			
			Logger::print_message(LOGLEVEL_FATAL, "Could not match command '$command'", $LOG_TAG);
FOUNDNAMEDTAG:
		}
		
		#NO NAMED OPTION WAS SUPPLIED. TRY TO DISCERN THE RELEVANT PARAMETER BY INDEX
		else
		{
			my $optindex = 0;
			keys %$groups;
			while(my ($key, $value) = each(%$groups))
			{
				foreach my $option (@$value)
				{
					$optindex++ if $option->{indexed};
					
					#FOUND THE VALUE THAT MATCHES THE POSITION
					if($optindex == $argpos)
					{
						$option->{matched} = "true";
						__call_binding($option->{binding}, $command);
						
						$argpos++;
						delete $groups->{$key} if $key ne NONE_GROUP;
						goto FOUNDUNNAMEDTAG;
					}
				}
			}
			Logger::print_message(LOGLEVEL_FATAL, "Cannot infer the command for parameter '$command'", $LOG_TAG) if $optindex < $argpos;
FOUNDUNNAMEDTAG:
		}
		
		shift @ARGV;
	}
	
	
	
	
	
	keys %$groups;
	while(my ($key, $value) = each(%$groups))
	{
		foreach my $option (@$value)
		{
			if($option->{matched} ne "true")
			{
				#IF IT'S NOT TRIGGERABLE, WE CAN FALL BACK TO A DEFAULT VALUE
				if($option->{defaultval} && !$option->{triggerable} && !$option->{mandatory})
				{
					__call_binding($option->{binding}, $option->{defaultval}) if !$option->{triggerable};
				}
				else
				{
					my $opt = $option->{options}[0];
					my $message = "Mandatory command '" . (length($opt) == 1 || $opt eq "vv" ? "-" : "--") . "$opt' was not provided (Description: " . $option->{description} . ").";
					$message .= " Use the -h command to see a list of available inputs." if $self->{helpenabled} eq "true";
					Logger::print_message(LOGLEVEL_FATAL, $message, $LOG_TAG) if $option->{mandatory};
				}
			}
		}
	}
	
	if($self->{canrun} == -1)
	{	
		#DIE IF ANOTHER INSTANCE IS RUNNING AND WE'RE TESTING FOR THAT (UNLESS WE'VE OVERRIDDEN IT)
		#NOTE THAT THIS IS *NOT PORTABLE* TO NON-*NIX SYSTEMS
		my $instances = "";
		my $curinstance = $0; $curinstance =~ s/^(.*?)(.\/|perl )(.*)/$3/;
		eval
		{
			for(split '^', `ps ax -o "pid,args"`)				#DISPLAY PIDS AND COMMAND PATHS FOR ALL PROCESSES WITH OR WITHOUT AN ASSOCIATED TERMINAL
			{
				if(/^\s*(\d+)\s+(.*?)(.\/|perl )$0.*$/)			#PARSE THE PID IF COMMAND PATH MACHES OUR EXECUTION PATH
				{
					next if $2 =~ m/sudo -u/;					#SUDO AS USER SEEMS TO RUN COMMAND TWICE, SO DON'T GET CONFUSED
					$instances .= "$_\n" if $$ ne $1;			#APPEND PID TO THE LIST FOR LATER
				}
			}
			$self->{canrun} = 0 if $instances && $self->{canrun} == -1;
		};
	
		Logger::print_message(LOGLEVEL_FATAL, "An instance of this script is already running (Use the multi-instance option to override this)\nActive PIDs colliding with $$ $0 are:\n$instances", $LOG_TAG) unless $self->{canrun};
		Logger::print_message(LOGLEVEL_ERROR, "Couldn't determine whether this script is a unique instance ($!).", $LOG_TAG) if $@;
	}
}





###################################################################
## get_arguments(string)
## ---------------------------------
## Take a list of arguments and output an array of entries. Should
## handle most cases of commandline-like argument inputs
###################################################################
sub get_arguments
{
	my $string = $_[0];
	
	my $escape = "false";
	my $inquotes = "false";
	my $lastindex = 0;
	my @values;
	for(my $i = 0; $i < length($string); $i++)
	{
		my $curchar = substr($string, $i, 1);
		
		#ESCAPES: IGNORE THE NEXT CHARACTER
		if($escape eq "true")
		{
			$escape = "false";
			next;
		}
		
		#IF WE'VE HIT A QUOTE
		if($curchar eq '"')
		{
			$inquotes = $inquotes eq "true" ? "false" : "true";
			if($inquotes ne "true")			#ENDING QUOTE. SPLIT
			{
				$i++;
				push(@values, substr($string, $lastindex, $i - $lastindex));
				$lastindex = $i + 1;
			}
			else
			{
				$lastindex++;
			}
		}
		#ESCAPES: IGNORE THE NEXT CHARACTER
		elsif($curchar eq "\\")
		{
			$escape = "true";
		}
		elsif($inquotes eq "true")
		{
			next;
		}
		
		#SPLIT ON ' ' UNLESS WITHIN QUOTES
		elsif($curchar eq " ")
		{
			push(@values, substr($string, $lastindex, $i - $lastindex));
			$lastindex = $i + 1;
		}
	}
	push(@values, substr($string, $lastindex)) if $lastindex < length($string);
	
	return @values;
}



sub __call_binding
{
	my ($binding, $value) = @_;
	
	if(!$binding)					{}
	elsif(ref($binding) eq "CODE")	{ $binding->($value); }
	else								{ $$binding = $value; }
}

1;
