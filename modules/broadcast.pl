#!/usr/local/bin/perl
package Broadcast;

use strict;
use warnings;

use Carp qw/carp/;
use Cwd;
use Data::Dumper;
#use Date::Simple ('date', 'today');
#use Data::UUID;
#use Encode qw(decode encode);
#use File::Path;
#use File::Copy;
use JSON;
use JSON qw( decode_json );
#use LWP::Simple;
#use POSIX qw/strftime/;
use Scalar::Util qw( looks_like_number );
use Template;
#use Time::Local;
use URI::Escape;
#use URI::URL;
#use XML::Simple;

# Home grown

use lib 'modules';

#use CTSConstants;
use Validate;						# job_number

require "databaseJob.pl";			# db_getWorkflow, db_update_broadcast 
require "config.pl";
require "databaseAudit.pl";

# Get the details for the Broadcast Apply popup (removed in 3.3.4)

sub Applypopup {
    my ($query, $session) = @_;

    my $mode        = $query->param( "mode" ) ||'';
    my $jobnumber   = $query->param( 'jobid' );
    my $nobroadcast  = 0;


    ##Condition to check all values are Broadcast or not
    my @jobsplit    = split(/\,/, $jobnumber);

    foreach my $splitjobid (@jobsplit)
    {
        my $mediatype = ECMDBJob::db_getWorkflow($splitjobid);
        if ($mediatype ne "BROADCAST")
        {
            my $vars = {
                nobroadcast=>1,
            };
            if ( $mode eq "json" )
            {
                my $json = encode_json ( $vars );
                print $json;
                return $json;
            }
        }
    }
    # if(length($jobnumber))           { $jobnumber = "('" . ($jobnumber =~ s/,/','/rg) . "') "; } 
    my @countries               = ECMDBJob::db_getcountries();
    #my @languages               = ECMDBJob::db_languagebycountry($jobnumber);
    my @videoratios             = ECMDBJob::db_getidnamebytablename('videoratio','ratio');
    my @videostandards          = ECMDBJob::db_getidnamebytablename('videostandard','standard');
    my @videoframerates         = ECMDBJob::db_getidnamebytablename('videoframerate','ratio');
    my @videoletterboxes        = ECMDBJob::db_getidnamebytablename('videoletterbox','ratio');
    my @audiostandards          = ECMDBJob::db_getidnamebytablename('audiostandard','standard');
	my @producerresponsible  = ECMDBJob::db_getidnamebytablename('producerresponsible','name');

    my $vars = {
            countries           => \@countries,
            videoratios         => \@videoratios,
            videostandards      => \@videostandards,
            videoframerates     => \@videoframerates,
            videoletterboxes    => \@videoletterboxes,
            audiostandards      => \@audiostandards,
            producerresponsible => \@producerresponsible,
            nobroadcast         => $nobroadcast,
    };
    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

#
# The Action->Apply action displays the "Apply to Selection" dialog
# which allows the user to select the Country, Language, Video Aspect Ration, Video Standard, Video Frame Rate
# Video Letter boxing, Audio Spec and Producer Responsible.
# When the user clicks Apply, it calls saveBroadcastDetails which passes popupflag = 1 and uses the popup prefix.
# Has been removed in 3.3.4 as the fields are populated by Atlas (at least in some cases).

sub apply_update {
	my ($query, $session) = @_;

    my $mode        = $query->param( "mode" ) ||'';
 
	my %params = $query->Vars; # Access the query params as a hash
	
	my $values = {	
		country			=> $params{ 'country' },
		language		=> $params{ 'language' },
		videoratio		=> $params{ 'videoratio' },
		videostd		=> $params{ 'videostd' },
		videoframerate	=> $params{ 'videoframerate' },
		videoletterbox	=> $params{ 'videoletterbox' },
		audiostd		=> $params{ 'audiostd' },
		producerresp	=> $params{ 'producerresponsible' },
		popupflag		=> 1,
	 };

	my @history;
	my $vars = { dummy => 1 };
	my $failures = 0;

	# Apply selection is just that, so it applies to 1 or more jobs

	my @jobs = split(/,/, $query->param('job_num') || $query->param('jobnumber') || '');
	foreach my $jobnumber (@jobs) {
		eval {
			Validate::job_number( $jobnumber );
			ECMDBJob::db_update_broadcast( $jobnumber, $values );
			push( @history, "Updated $jobnumber" );
		};
		if ( $@ ) {
			$failures++;
			push( @{$vars->{failures}}, "Failed to update $jobnumber, Error: $@" );
		}
	}
	$vars->{history} = \@history;
	$vars->{failures} = $failures;
	$vars->{error} = "Failed to update $failures job(s)" if ( $failures > 0 );
	
	if  ( $mode eq 'json' ) {
		print encode_json( $vars );
	}
}

# action updatebroadcasttable            => 'broadcast/save',
# Used by js/broadcast.js and js/custom.js
#
# In custom.js, the action is the Save button at the top of the right hand pane
# See function updatebroadcastdata
#

sub update_broadcast {
	my ($query, $session) = @_;

    my $mode        	= $query->param( "mode" ) ||'';
 	my $userid          = $session->param( 'userid' );
	my $username        = $session->param( 'username' );
	# Change way we get values, to avoid CGI::param called in list context errors in apache error log
	# Creating a hash with calls to $query->param, causes CGI errors to be logged
	#my $values = {	
	#	country			=> $query->param("$country"),
	#	language		=> $query->param("$language"),

	my %params = $query->Vars; # Access the query params as a hash

	my $values = {	# keys are the names of columns in broadcasttracker3 (case insensitive)
		acc_manager		=> $params{ 'accmanager' },
		#orderdate		=> $params{ 'orderdate' },
		#agency_jobno	=> $params{ 'agencyno' },
		#country			=> $params{ 'country' },
		#language		=> $params{ 'language' },
		deliverydate	=> $params{ 'deliverydate' },
		scheduledate	=> $params{ 'scheduleDate' },
		#editcode		=> $params{ 'editcode' },
		internal_blockno => $params{ 'internalblockno' },
		#bdccode			=> $params{ 'bccode' },
		#videoratio		=> $params{ 'videoratio' },
		#videostd		=> $params{ 'videostd' },
		#videoframerate	=> $params{ 'videoframerate' },
		#videoletterbox	=> $params{ 'videoletterbox' },
		#audiostd		=> $params{ 'audiostd' },
		smokecmd		=> $params{ 'smokecmd' },
		producerresp	=> $params{ 'producerresponsible' },
	 };

	my $deliverydate = $params{ 'deliverydate' }; # for tracker3 update

	$values->{acc_manager} = '' unless defined $values->{acc_manager};
	$values->{acc_manager} =~ s/^\s+$//; # converts blanks to empty string
	$values->{acc_manager} = '' if ( $values->{acc_manager} eq '<br>' );
				   
	my @history;
	my $vars = { dummy => 1 };
	my $failures = 0;

	# This should be just one job (as thats all the right hand pane can handle)

	my $job_number = $query->param('job_num') || $query->param('jobnumber') || '';
	my $is_producer_changed	=	$query->param( 'is_producer_changed' );
	my $new_producer_name		=	$query->param( 'new_producer_name' );
	my $old_producer_name		=	$query->param( 'old_producer_name' );
	eval {
			Validate::job_number( $job_number );
			ECMDBJob::db_update_broadcast( $job_number, $values );
			ECMDBJob::db_updatedatetime( $job_number, $deliverydate ) if ( defined $deliverydate );
			ECMDBJob::db_updateProducer( $job_number,$new_producer_name ) if ( $is_producer_changed ); 
			ECMDBAudit::db_auditLog( $userid, "TRACKER3", $job_number, "Job $job_number : Producer name '".$old_producer_name."' has been changed to '".$new_producer_name."' by $username" ) if ( $is_producer_changed && ($new_producer_name ne $old_producer_name));
			push( @history, "Updated $job_number" );
	};
	if ( $@ ) {
		$failures++;
		push( @{$vars->{failures}}, "Failed to update $job_number, Error: $@" );
	}
	$vars->{history} = \@history;
	$vars->{failures} = $failures;
	$vars->{error} = "Failed to update $failures job(s)" if ( $failures > 0 );
	
	if  ( $mode eq 'json' ) {
		print encode_json( $vars );
	}
}

######################################################################################
# To Update/Edit Status Notes only for Broadcast Media types jobs only.
# action update_status_notes => 'broadcast/update_status_notes'
# Used by updateStateNotes in js/kanbanboard.js for Broadcast jobs
######################################################################################
sub updateStatusNotes() {
	my ($query, $session) = @_;	

	my $mode				= $query->param( "mode" ) ||'';
	my $job_number			= $query->param( "job_number" );
	my $status_notes		= $query->param( "status_notes" );
	my $vars;
	
	eval{
		ECMDBJob::db_update_broadcast( $job_number, { smokecmd => $status_notes } );		
		$vars = { update_status => 'success' };
	};
	if ( $@ ) {
		my $error = $@;
        $error =~ s/ at modules.*//gs;
		$vars	= { error	=> $error };
	}
	if ( $mode =~ /^json$/i ) {
		print encode_json ( $vars );
	}
}

sub update_datetime {
	my ($query, $session) = @_;
	
	my $dateval = $query->param('dateval') || 0;
	my $joblist	= $query->param('jobnum') || '';
	my $mode    = $query->param('mode') || '';

	my @jobs = split(/,/, $joblist);

	foreach my $jobnumber (@jobs) {	
		ECMDBJob::db_updatedatetime( $jobnumber, $dateval );
	}
	if( $mode eq 'json' ) {
		print encode_json({ success => 1 });
	}
}

1;

