#!/usr/local/bin/perl -w

use warnings;
use strict;

package StreamHelper;

use CGI;
use CGI::Carp qw( fatalsToBrowser carp cluck confess );
use Scalar::Util qw( looks_like_number );
use File::Path;
use File::Basename;

use lib 'modules';
use lib '../modules';

require "config.pl";

use constant STREAM_TEMP_CONFIG => 'stream_temp'; # Name of Jobflow setting
use constant DEFAULT_STREAM_TEMP => '/temp';

our $debug = 0;  # Set to 1 if testing stream scripts from command line, 0 otherwise

# Check stream script is authorised
# Returns userid if successful
# Throws exception if not successful

sub checkAuth {
	my ( $query, $history ) = @_;

	my $userid;
	my $username;
	my $email;
	my @local_history;

	$history = \@local_history if ( ! defined $history );

	# get session id from param sessionid OR cookie (if there is one)
	my $sid = $query->param( "sessionid" ) || $query->cookie( "CGISESSID" ) || '';
	my $env = Config::getConfig( 'environment' ) || 'dev';

	my $debug = $query->param( "debug" ) || 0;
	my $rnd_no = $query->param( "rnd_no" ) || '';
	push ( @$history, "checkAuth, rnd_no '$rnd_no', debug $debug, env '$env'" );
	die "No session" unless ( defined $sid ) && ( $rnd_no );

	# Running via apache (possibly via curl request) rather than command line
	my $isLocal = 0;
	# Apache CGI environment variables - see http://www.cgi101.com/book/ch3/text.html
	my $referer = $ENV{HTTP_REFERER} || '';  # can be blank if direct request or full URL http://jfcdn.staging.tagworldwide.com/jobflow.pl
	my $server_name = $ENV{SERVER_NAME} || ''; # can be IP address "172.27.100.13" or fully qualified domain eg "jfcdn.staging.tagworldwide.com"
 	my $server_address = $ENV{SERVER_ADDRESS} || ''; # Typically just IP address eg "172.27.93.111"
 	my $http_host = $ENV{HTTP_HOST} || ''; # domain name "jfcdn.staging.tagworldwide.com" or IP address
	my $remote_address = $ENV{REMOTE_ADDRESS} || ''; # IP address of request initiator, either user PC/MAc or other web server

	if ( $referer ne '' ) {
		if ( $server_name eq '' ) {
			push( @$history, "REFERER is '$referer', but SERVER_NAME missing" );
		} elsif ( $referer =~ /\/$server_name/ ) {
			# eg referer "http://jfcdn.staging.tagworldwide.com/jobflow.pl" contains "/jfcdn.staging.tagworldwide.com"
			push( @$history, "REFERER is '$referer', matches SERVER_NAME '$server_name'" );
			$isLocal = 1;
		} else {
			push( @$history, "REFERER is '$referer', but SERVER_NAME '$server_name' doesnt match, server address '$server_address'" );
		}

	} else {
		# compare REMOTE_ADDRESS and SERVER_ADDRESS
		if ( $remote_address eq '' ) {
			push( @$history, "REMOTE_ADDRESS and REFERER missing, SERVER_NAME '$server_name', SERVER_ADDRESS '$server_address'" );
		} elsif ( $server_address eq '' ) {
			push( @$history, "SERVER_ADDRESS and REFERER missing, SERVER_NAME '$server_name', REMOTE_ADDRESS '$remote_address'" );
		} elsif (( $server_address ne $remote_address ) && ( $http_host ne $remote_address )) {
			push( @$history, "REMOTE_ADDRESS '$remote_address' doesnt match SERVER_ADDRESS '$server_address' or HTTP_HOST '$http_host'" );
		} else {
			push( @$history, "REMOTE_ADDRESS '$remote_address' matches SERVER_ADDRESS '$server_address' or HTTP_HOST '$http_host'" );
			$isLocal = 1;
		}
	}

	if ( $isLocal ) {

		push( @$history, "local access, sid '$sid'" );

		# Try and access the CGI session directly ...
		my $session = new CGI::Session("driver:File", $sid, { Directory => '/tmp' } );
		die "Session doesnt exist (locally)" unless ( ( defined $session ) && ( $session->id eq $sid ) );

		my $logged_in = $session->param( 'loggedin' ) || 0;
		my $stored_rnd = $session->param( 'rnd_no' ) || '';
		$userid = $session->param( 'userid' ) || 0;
		$username = $session->param( 'username' ) || 0;
		$email = $session->param( 'email' ) || '';
		my $last_access_time		= $session->atime(); # Added for PD-2874: Session time out issue
		my $session_created_time	= $session->ctime();

		push( @$history, "local access, logged_in, $logged_in, stored rnd $stored_rnd, username $username, userid $userid, last access $last_access_time, session created $session_created_time" );
		die "Not logged on (1)" unless ( $logged_in == 1 );
		die "Not logged in (2)" unless defined $rnd_no;
		die "Not logged in (3)" unless defined $stored_rnd;
		die "Not logged in (4)" unless ( "$stored_rnd" eq "$rnd_no" );
		die "Not logged in (5)" unless ( $userid != 0 );

	} else {
		push( @$history, "remote access, server_name $server_name, server address $server_address, referer '$referer'" );

		# check the http referrer and die if its incorrect
		if ( $env eq "PROD" && (length($referer) > 0) ) {
			my $validreferer = Config::getConfig( 'validreferer' ) || '';
			# die "Invalid referer $referer" unless $referer =~ m/$validreferer/i;
			die "Invalid referer" unless $referer =~ m/$validreferer/i;
		}

		{
			no warnings 'once';
			( $userid ) = $Config::dba->process_oneline_sql( "SELECT userid FROM usersessions WHERE sessionid=?", [ $sid ] );
		}
		die "Not logged on (5)" unless ( defined $userid );

		my $ssl_hack = Config::getConfig( 'jf_ssl_hack' ) || 0;
		my $jobflowws_url = Config::getConfig( 'jobflowserver' ) . '/jobflowws.pl';

		# Use internal_jobflowws_url if available especially for Staging/Live
		my $internal_jobflowws_url = Config::getConfig( 'internal_jobflowws_url' );

		if ( defined $internal_jobflowws_url ) {
			$jobflowws_url = $internal_jobflowws_url;
		} elsif ( $referer ne '' ) { # Typically on dev
			$jobflowws_url = $referer;
			$jobflowws_url =~ s/jobflow.pl/jobflowws.pl/g;			
		}

		my $curlReq = "-d action=verifysess -d sessionid=$sid -d rnd_no=$rnd_no \"$jobflowws_url\"";
		$curlReq = " -k $curlReq" if $ssl_hack;

		push( @$history, "curl $curlReq" );

		my $curlResp = `curl -sS $curlReq`;

		push( @$history, "response - $curlResp" );

		if ( $curlResp eq '' ) {
			die "No response from jobflowws call, $curlReq" if $debug;
			die "No response from jobflowws call";
		}
		# can get "<user><userid>0</userid><error>Not logged in (1) at /home/www-data/jobflow.dev.tagworldwide.com/htdocs/jobflowws.pl line 139.</error></user>"

		if ( $curlResp =~ /\<error>([^\<]*)\<\/error/ ) {
			my $error = $1;
			$error =~ s/ at .*//gs;
			die "Error '$error', request '$curlReq', response '$curlResp'" if $debug;
			die $error;
			
		}

		( $userid ) = $curlResp =~ /\<userid>(\d*)\<\/userid>/;
		( $email )=  $curlResp =~ /\<email>([^\<]*)\<\/email>/;
		die "Not logged on (3)" unless defined $userid;

	}

	push( @$history, "streamHelper:checkAuth returned $userid, email: $email" );

	return $userid, $email;
}

# PD-2622 Generate random number to pass to stream scripts
# Only do if displaying HTML as that has the configSettings.
# The stream scripts which get passed sessionid and rnd_no can verify as authenticated

sub createKey {
	my ( $session ) = @_;

	my $rnd_no = int( rand( 1000000 ) ); # use a random number 0..999999
	$session->param( 'rnd_no', $rnd_no );
	$session->flush();

	return $rnd_no; 
}

sub getTempDir {
	my $temp = Config::getConfig( STREAM_TEMP_CONFIG ) || DEFAULT_STREAM_TEMP;
	eval {
		if ( ! -e $temp ) {
			# warn "Creating '$temp'";
			mkpath( $temp, { mode => 0777, verbose => 0 } ) or die "Failed to create '$temp', $!";
		}
	};
	if ( $@ ) {
		warn "Failed to create ... $@";
		$temp = DEFAULT_STREAM_TEMP;
	}
	return $temp;
}

# Assert (or die) whether user can view specific customer for an opco
# The user can view the job if 
# a) they have visibility of the customerid associated with the home opcoid. If they have this, they can see the job where ever it is
# b) they have visibility of the customerid associated with the current location
# c) they have admin role for the opcoid or location
# Inputs
# - userid
# - opcoid home opcoid of job
# - customerid (corresponding to customer name at opcoid)
# - location current opcoid of job
# - remoteCustomerid (corresponding to customer name at location)
# Returns void
# Throws exception if user has no visibility.

sub assert_customer_visible {
	my ( $userid, $opcoid, $customerid, $location, $remoteCustomerid ) = @_;

	# Table customeruser has columns ( customerid, userid, OpCoID_Customer )

    print "assert_customer_visible: userid $userid, opcoid $opcoid, cust $customerid, locn $location, rci $remoteCustomerid<br>\n" if $debug;
	# Has the user visiblity of the home customerid
	my ( $canview ) = $Config::dba->process_oneline_sql( "SELECT 1 FROM customeruser WHERE userid=? AND customerid = ?", [ $userid, $customerid ] );
	print "User has access to customer $customerid<br>\n" if $debug && $canview;

	if ( ! $canview ) {
		# See if user had admin access to the opco for this customer (at home location)
		( $canview ) = $Config::dba->process_oneline_sql( "SELECT 1 FROM userroles WHERE userid=? AND opcoid=? AND roleid=4", [ $userid, $opcoid ] );
		print "User has admin access to home opcoid $opcoid<br>\n" if $debug && $canview;
	}

	if ( ( ! $canview ) && ( defined $remoteCustomerid ) && ( $customerid != $remoteCustomerid ) ) {
		print "Looking at remote customer $remoteCustomerid, location $location<br>\n" if $debug;

		# Can the user view the customer corresponding to the current location (same customer name, just different opcoid)
		( $canview ) = $Config::dba->process_oneline_sql( "SELECT 1 FROM customeruser WHERE userid=? AND customerid = ?", [ $userid, $remoteCustomerid ] );
		print "User has access to remote customer $remoteCustomerid<br>\n" if $debug && $canview;

		if ( ! $canview ) {
			# See if user had admin access to the opco for this customer (at current location)
			( $canview ) = $Config::dba->process_oneline_sql( "SELECT 1 FROM userroles WHERE userid=? AND opcoid=? AND roleid=4", [ $userid, $location ] );
			print "User has admin access to location $location<br>\n" if $debug && $canview;
		}
	}

	die "You do not have access to the customer for this job" unless ( $canview );
}

# Clone of ECMDBOpcos::db_getFTPDetails from databaseOpcos.pl (to save loading that large module)

sub db_getFTPDetails {
    my ($opcoid) = @_;

    return "" if $opcoid && !looks_like_number($opcoid);

    my ($ftpserver, $ftpusername, $ftppassword) = $Config::dba->process_oneline_sql(
			"SELECT ftpserver, ftpusername, ftppassword FROM OPERATINGCOMPANY WHERE opcoid=?", [ $opcoid ] );

    return ($ftpserver, $ftpusername, $ftppassword);
}

# add_suffix function is for adding suffix name to the file name
sub add_suffix {
	my ( $file, $suffix ) = @_;

	return $file if ( ! defined $suffix ) || ( $suffix  eq "" );
	my ( $name, $path, $ext ) = fileparse( $file, '\.[^\.]*');
	$file = $name . '_' . $suffix;
	$file .=  $ext if ( defined $ext ) && ( $ext ne '' );
#	$file = $path .  '/' . $file if ( defined $path ) && ( $path ne '' ) && ( $path ne '/' );
	return $file; 
}

sub add_prefix {
	my ( $file, $prefix ) = @_;
	return $file if ( ! defined $prefix ) || ( $prefix  eq "" );
	my ( $name, $path, $ext ) = fileparse( $file, '\.[^\.]*');
	$file = $prefix . '_' . $name;
	$file .=  $ext if ( defined $ext ) && ( $ext ne '' );
	return $file;
}

1;

