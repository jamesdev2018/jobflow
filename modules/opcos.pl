#!/usr/local/bin/perl
package Opcos;

use strict;
use warnings;
use Template;
use Scalar::Util qw( looks_like_number );
use JSON;
use Encode qw(decode encode);
use Carp qw /carp/;
use Data::Dumper;
use File::Path;
use File::Copy;
use File::Basename;

use lib 'modules';

use Cwd;
use Cwd 'abs_path';

require "companies.pl";
require "databaseOpcos.pl";
require "databaseCustomerUser.pl";
require "general.pl";
require "users.pl";
require "databaseImageworkflow.pl";
require "Checklist.pl";
require "config.pl";
require "customerlogo.pl";

# List of check box fields in customer table, ie those of type tinyint(1) NOT NULL DEFAULT '0'
# Where the column name and query param name as the same, just the column name, otherwise columnname=queryname

our @customer_cbs = ( 'jobpoolassign', 'bwdef', 'bwcroppeddef', 'lowrescroppeddef', 'lowresdef', 'hiresdef', 'proofonlydef', 'rgbdef', 'rgbcroppeddef', 'rgbjpegdef', 
			'hirescroppedbleedboxdef', 'hirescroppedtrimboxdef', 'autocheckin', 'useagencyrefasfilename', 'usejobrepository', 'repoautoarchive',
			'useassetrepository', 'repositorysync', 'enhancedftp', 'estimateon', 'pdfcomparedef', 'pfc_disable',
			'additionalfiletypes', 'bw_asset', 'bw_crp_asset', 'lr_asset', 'lr_crp_asset', 'rgb_crp_asset', 'rgb_asset', 'rgb_jpg_asset',
			'hires_crp_bleedbox_asset', 'hires_crp_trimbox_asset', 'aux_files', 'split_pages', 'open_assets', 'high_res', 'zip_dir',
			'es_compare', 'enableproducts', 'enabledelete', 'enable_one2edit' );

# Non cb fields id, opcoid, customer, filename, syncstart, syncend (held as TINYINT(4) but appearing a numeric values
# estimate, rec_timestamp, nearline_threshold, offline_threshold, docmaxsize, digitalmaxsize, enablejobshards ?, 

##############################################################################
# the template object 
##############################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";


##############################################################################
#       ListOperatingCompanies
#
##############################################################################
sub ListOperatingCompanies {
	my $query 		= $_[0];
	my $session 		= $_[1];

	my $sidemenu   = $query->param( "sidemenu" );
    my $sidesubmenu= $query->param( "sidesubmenu" );

	my @opcos		= getOperatingCompanies();
	
	my $vars = {
		sidemenu    => $sidemenu,
        sidesubmenu => $sidesubmenu,
		opcos 		=> \@opcos,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/listoperatingcompanies.html', $vars )
    		|| die $tt->error(), "\n";
}

##############################################################################
#		AddOperatingCompany
#
##############################################################################
sub AddOperatingCompany {
	my $query 			= $_[0];
	my $session 		= $_[1];

	my ( $opcoid, $opconame, $rootpath, $preflightin, $preflightpassed, $preflightfail, $preflightwarning, $ftpserver, 
				$ftpusername, $ftppassword, $bwdef, $bwcroppeddef, $lowrescroppeddef, $lowresdef, $hiresdef, $proofonlydef, $rgbdef, $rgbcroppeddef, 
				$rgbjpegdef, $colourmanagedef, $colourmanage_isactivedef, $optimisedef, $optimise_isactivedef, $pdfcomparedef, $filenameconfig, $pathtype, $ttpfolder, 
				$preflightprofile, $qcenabled, $filenameoverride, $ftpenabled, $jobassignment, $viewhighres, $creative, $jobpoolassign, $publishing, $domainname );
	my ( $formPosted, $errorMessage, %error );
	my $vars				= {};
	
	$opcoid 		= General::htmlEditFormat( $query->param( "opcoid" ) );
	$opconame 		= General::htmlEditFormat( $query->param( "opconame" ) );
	$rootpath 		= General::htmlEditFormat( $query->param( "rootpath" ) );
	$preflightin 		= General::htmlEditFormat( $query->param( "preflightin" ) );
	$preflightpassed 	= General::htmlEditFormat( $query->param( "preflightpassed" ) );
	$preflightfail	 	= General::htmlEditFormat( $query->param( "preflightfail" ) );
	$preflightwarning 	= General::htmlEditFormat( $query->param( "preflightwarning" ) );
	$ftpserver		= General::htmlEditFormat( $query->param( "ftpserver" ) );
	$ftpusername	 	= General::htmlEditFormat( $query->param( "ftpusername" ) );
	$ftppassword	 	= General::htmlEditFormat( $query->param( "ftppassword" ) );
	$bwdef			= $query->param( "bwdef" );
	$bwcroppeddef	 	= $query->param( "bwcroppeddef" );
	$lowrescroppeddef 	= $query->param( "lowrescroppeddef" );
	$lowresdef		= $query->param( "lowresdef" );
	$hiresdef		= $query->param( "hiresdef" );	
	$proofonlydef	 	= $query->param( "proofonlydef" );
	$rgbdef			= $query->param( "rgbdef" );
	$rgbcroppeddef	 	= $query->param( "rgbcroppeddef" );
	$rgbjpegdef		= $query->param( "rgbjpegdef" );
	$colourmanagedef 	= $query->param( "colourmanagedef" );
	$colourmanage_isactivedef	= $query->param( "colourmanage_isactivedef" );
	$optimisedef		= $query->param( "optimisedef" );
	$optimise_isactivedef	= $query->param( "optimise_isactivedef" );
	$pdfcomparedef	 	= $query->param( "pdfcomparedef" );
	$filenameconfig 	= General::htmlEditFormat( $query->param( "filenameconfig" ) );
	$pathtype		= $query->param( "pathtype" );
	$ttpfolder		= $query->param( "ttpfolder" );
	$preflightprofile	= $query->param( "preflightprofile" );
	$qcenabled		= $query->param( "qcenabled" );
	$filenameoverride	= $query->param( "filenameoverride" );
	$ftpenabled		= $query->param( "ftpenabled" );
	$jobassignment		= $query->param( "jobassignment" );
	$viewhighres		= $query->param( "viewhighres" );
	$creative		= $query->param( "creative" );
	$jobpoolassign		= $query->param( "jobpoolassign" );
	$publishing		= $query->param("publishing");
	$domainname		= $query->param("domainname");
	my $sidemenu		= $query->param( "sidemenu" );
	my $sidesubmenu		= $query->param( "sidesubmenu" );
	$formPosted 		= $query->param( "formPosted" ) ||'';
	$error{ oktogo } 	= 1;

	if ( $formPosted eq "yes" ) {

		%error = validateOperatingCompany( $opcoid, $opconame, $rootpath, $preflightin, $preflightpassed, $preflightfail, $preflightwarning, $ftpserver, 
				$ftpusername, $ftppassword, $bwdef, $bwcroppeddef, $lowrescroppeddef, $lowresdef, $hiresdef, $proofonlydef, $rgbdef, $rgbcroppeddef, 
				$rgbjpegdef, $colourmanagedef, $colourmanage_isactivedef, $optimisedef, $optimise_isactivedef, $pdfcomparedef, $filenameconfig, 
				$pathtype, $ttpfolder, $preflightprofile, $qcenabled, $filenameoverride, $ftpenabled, $ftpenabled, $jobassignment, $viewhighres, $creative,
				$jobpoolassign, $publishing, $domainname );
		
		if ( $error{ oktogo } == 1 ) {
			ECMDBOpcos::addOperatingCompany( $opcoid, $opconame, $rootpath, $preflightin, $preflightpassed, $preflightfail, $preflightwarning, $ftpserver, 
				$ftpusername, $ftppassword, $bwdef, $bwcroppeddef, $lowrescroppeddef, $lowresdef, $hiresdef, $proofonlydef, $rgbdef, $rgbcroppeddef, 
				$rgbjpegdef, $colourmanagedef, $colourmanage_isactivedef, $optimisedef, $optimise_isactivedef, $pdfcomparedef, $filenameconfig, 
				$pathtype, $ttpfolder, $preflightprofile, $qcenabled, $filenameoverride, $ftpenabled, $jobassignment, $viewhighres, $creative, $jobpoolassign,
				$publishing, $domainname );
		
			General::setHeaderVars( $vars, $session );
	
			$tt->process( 'ecm_html/operatingcompanyaddedsuccess.html', $vars )
    				|| die $tt->error(), "\n";
			return;
		}
	}

	$vars = {
		error 				=>	\%error,
		opcoid				=>	$opcoid, 
		opconame			=>	$opconame, 
		rootpath			=>	$rootpath, 
		preflightin			=>	$preflightin, 
		preflightpassed			=>	$preflightpassed, 
		preflightfail			=>	$preflightfail, 
		preflightwarning 		=>	$preflightwarning, 
		ftpserver			=>	$ftpserver, 
		ftpusername			=>	$ftpusername, 
		ftppassword			=>	$ftppassword, 
		bwdef				=>	$bwdef, 
		bwcroppeddef			=>	$bwcroppeddef, 
		lowrescroppeddef		=>	$lowrescroppeddef, 
		lowresdef			=>	$lowresdef, 
		hiresdef			=>	$hiresdef, 		
		proofonlydef			=>	$proofonlydef, 
		rgbdef				=>	$rgbdef, 
		rgbcroppeddef			=>	$rgbcroppeddef, 
		rgbjpegdef			=>	$rgbjpegdef, 
		colourmanagedef			=>  	$colourmanagedef, 
		colourmanage_isactivedef	=>	$colourmanage_isactivedef,
		optimisedef			=>	$optimisedef,
		optimise_isactivedef		=>	$optimise_isactivedef,
		pdfcomparedef			=>	$pdfcomparedef,		 
		filenameconfig			=> 	$filenameconfig,
		pathtype			=>  	$pathtype,
		ttpfolder			=>  	$ttpfolder,
		preflightprofile		=>	$preflightprofile,
		qcenabled			=>	$qcenabled,
		filenameoverride		=>	$filenameoverride,
		ftpenabled			=>	$ftpenabled,
		jobassignment			=>	$jobassignment,
		viewhighres			=>	$viewhighres,
		creative			=>	$creative,
		jobpoolassign			=>	$jobpoolassign,
		publishing			=>	$publishing,
		domainname 			=>	$domainname,
		sidemenu			=>	$sidemenu,
		sidesubmenu			=>	$sidesubmenu,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/addoperatingcompany.html', $vars )
    		|| die $tt->error(), "\n";
}
##############################################################################
#		EditOperatingCompany
#
##############################################################################
sub EditOperatingCompany {
	my $query	= $_[0];
	my $session	= $_[1];

	my $opcoid	= $query->param( "opcoid" );
	my $mode	= $query->param( "mode" ) ||'';

	if ( $mode eq "json" )
	{
		$opcoid = $session->param( "opcoid" );
	}

	my @opco;
	my @vendors;
	my ( $formPosted, %error );
	
	my ( $opconame, $rootpath, $wippath, $mountpath, $preflightin, $preflightpassed, $preflightfail, $preflightwarning, $ftpserver, $ftpusername, $ftppassword,
				$bwdef, $bwcroppeddef, $lowrescroppeddef, $lowresdef, $hiresdef, $proofonlydef, $rgbdef, $rgbcroppeddef, $rgbjpegdef,
				$colourmanagedef, $colourmanage_isactivedef, $optimisedef, $optimise_isactivedef, $pdfcomparedef, $filenameconfig, $updateflag,
				$pathtype, $jobfolderenabled, $ttpfolder, $preflightprofile, $qcenabled, $filenameoverride, $ftpenabled, $jobassignment, $viewhighres, $creative,
				$jobpoolassign, $publishing, $domainname, $ttpupload, $remotetempfolder, $remotettpfolder, $usejobrepository, $useassetrepository, $repositorysync, $syncstart, 
				$syncend, $adgatesend, $thumbview, $estimateon, $Creative, $enablelocalupload, $localuploadsource, $localuploaddestination );

	$formPosted = $query->param( "formPosted" ) ||'';

	if ( $formPosted eq "yes" ) { 	#get from form
		$opcoid 			= General::htmlEditFormat( $query->param( "opcoid" ) );
		$opconame 			= General::htmlEditFormat( $query->param( "opconame" ) );
		$rootpath 			= General::htmlEditFormat( $query->param( "rootpath" ) );
		$wippath 			= General::htmlEditFormat( $query->param( "wippath" ) );
		$mountpath			= General::htmlEditFormat( $query->param( "mountpath" ) );
		$preflightin 			= General::htmlEditFormat( $query->param( "preflightin" ) );
		$preflightpassed 		= General::htmlEditFormat( $query->param( "preflightpassed" ) );
		$preflightfail	 		= General::htmlEditFormat( $query->param( "preflightfail" ) );
		$preflightwarning 		= General::htmlEditFormat( $query->param( "preflightwarning" ) );
		$ftpserver		 	= General::htmlEditFormat( $query->param( "ftpserver" ) );
		$ftpusername	 		= General::htmlEditFormat( $query->param( "ftpusername" ) );
		$ftppassword	 		= General::htmlEditFormat( $query->param( "ftppassword" ) );
		$bwdef				= $query->param( "bwdef" );
		$bwcroppeddef			= $query->param( "bwcroppeddef" ); 
		$lowrescroppeddef		= $query->param( "lowrescroppeddef" ); 
		$lowresdef			= $query->param( "lowresdef" );
		$hiresdef			= $query->param( "hiresdef" );		
		$proofonlydef			= $query->param( "proofonlydef" ); 
		$rgbdef				= $query->param( "rgbdef" );
		$rgbcroppeddef			= $query->param( "rgbcroppeddef" ); 
		$rgbjpegdef			= $query->param( "rgbjpegdef" );
		$colourmanagedef		= $query->param( "colourmanagedef" );
		$colourmanage_isactivedef	= ($query->param( "colourmanage_isactivedef") eq "enabled" ? 1 : 0);
		$optimisedef			= $query->param( "optimisedef");
		$optimise_isactivedef		= ($query->param( "optimise_isactivedef" ) eq "enabled" ? 1 : 0);
		$pdfcomparedef			= $query->param( "pdfcomparedef" );
		$filenameconfig 		= General::htmlEditFormat( $query->param( "filenameconfig" ) );
		$pathtype			= $query->param( "pathtype" );
		$jobfolderenabled               = $query->param( "jobfolderenabled" );
		$ttpfolder			= $query->param( "ttpfolder" );
		$preflightprofile		= $query->param( "preflightprofile" );
		$qcenabled			= $query->param( "qcenabled" );
		$filenameoverride		= $query->param( "filenameoverride" );
		$ftpenabled			= $query->param( "ftpenabled" );
		$jobassignment			= $query->param( "jobassignment" );
		$viewhighres			= $query->param( "viewhighres" );
		$creative			= $query->param( "creative" );
		$jobpoolassign			= $query->param( "jobpoolassign" );
		$publishing			= $query->param("publishing");
		$domainname			= $query->param("domainname");
		$ttpupload			= $query->param("ttpupload");
		$remotetempfolder		= $query->param("remotetempfolder");
		$remotettpfolder		= $query->param("remotettpfolder");
		$usejobrepository		= $query->param("usejobrepository");
		$useassetrepository		= $query->param("useassetrepository");
		$repositorysync			= $query->param("repositorysync");
		$syncstart			= $query->param("syncstart");
		$syncend			= $query->param("syncend");
		$adgatesend			= $query->param("adgatesend");
		$thumbview			= $query->param("thumbview");
		$estimateon			= $query->param("estimateon");
		$enablelocalupload		= $query->param("enablelocalupload");
		$localuploadsource		= General::htmlEditFormat( $query->param( "localuploadsource" ) );
		$localuploaddestination		= General::htmlEditFormat( $query->param( "localuploaddestination" ) );

		# Getting all department list with id to get the query string value using foreach so that it will frame auto even we add more departments
		my @departmentlist		= ECMDBOpcos::db_getdepartments(); 
		ECMDBOpcos::db_deletedeptbyopcoid($opcoid); # All time delete the previous checked list of that opcoid

		my @advisoryLists = ECMDBOpcos::db_getquerytype($opcoid); ##Getting all Query types to pass to HTML

		foreach my $advisory ( @advisoryLists ) {

			my $db_advisoryID 			= $advisory->{'id'};
			my $formAdvisoryColor		= $query->param("advisoryColor_" . "$db_advisoryID");
			my $formAdvisoryColorFlag	= $query->param("advisoryColorCheckbox_" . "$db_advisoryID");

			ECMDBOpcos::db_editAdvisoryColour( $opcoid, $db_advisoryID, $formAdvisoryColor, $formAdvisoryColorFlag );
		}

		foreach my $val (@departmentlist)
		{
			my $deptid = $val->{id};
			my $checkeddeptid = $query->param($deptid); # Getting the HTML checked value dynamically

			if ($checkeddeptid)
			{
				ECMDBOpcos::db_updatedepartmentdetails($opcoid, $checkeddeptid, "1"); # Setting the flag to one only for checked values
			}
		}	

		my @teamlist  = ECMDBOpcos::db_getteams();
		ECMDBOpcos::db_deleteteambyopcoid($opcoid);
		foreach my $val (@teamlist)
		{
			my $teamid = $val->{name};
			my $checkedteamid = $query->param($teamid); # Getting the HTML checked value dynamically
			if ($checkedteamid)
			{
				ECMDBOpcos::db_updateteamdetails($opcoid, $checkedteamid, "1"); # Setting the flag to one only for checked values
			}
		}
		$updateflag = 1;
	} else {

		@opco = getOperatingCompany( $opcoid );

		if ( $mode eq "json" ) {
			# Do nothing
		} else {
			if ( scalar( @opco ) == 0 ) {
				General::NoAccess();
				return;
			}
		}
		$opconame					= $opco[0]{ OPCONAME };
		$rootpath					= $opco[0]{ ROOTPATH }; 
		$wippath					= $opco[0]{ WIPPATH }; 
		$mountpath              	= $opco[0]{ MOUNTPATH };
		$preflightin				= $opco[0]{ PREFLIGHTIN }; 
		$preflightpassed			= $opco[0]{ PREFLIGHTPASSED }; 
		$preflightfail				= $opco[0]{ PREFLIGHTFAIL };
		$preflightwarning			= $opco[0]{ PREFLIGHTWARNING }; 
		$ftpserver					= $opco[0]{ FTPSERVER };
		$ftpusername				= $opco[0]{ FTPUSERNAME }; 
		$ftppassword				= $opco[0]{ FTPPASSWORD }; 
		$bwdef						= $opco[0]{ BWDEF };
		$bwcroppeddef				= $opco[0]{ BWCROPPEDDEF }; 
		$lowrescroppeddef			= $opco[0]{ LOWRESCROPPEDDEF }; 
		$lowresdef					= $opco[0]{ LOWRESDEF };
		$hiresdef					= $opco[0]{ HIRESDEF };
		$proofonlydef				= $opco[0]{ PROOFONLYDEF }; 
		$rgbdef						= $opco[0]{ RGBDEF };
		$rgbcroppeddef				= $opco[0]{ RGBCROPPEDDEF }; 
		$rgbjpegdef					= $opco[0]{ RGBJPEGDEF };
		$colourmanagedef			= $opco[0]{ COLOURMANAGEDEF };
		$colourmanage_isactivedef	= $opco[0]{ COLOURMANAGE_ISACTIVEDEF };
		$optimisedef				= $opco[0]{ OPTIMISEDEF };
		$optimise_isactivedef		= $opco[0]{ OPTIMISE_ISACTIVEDEF };
		$pdfcomparedef				= $opco[0]{ PDFCOMPAREDEF };
		$filenameconfig				= $opco[0]{ FILENAMECONFIG };
		$pathtype					= $opco[0]{ PATHTYPE };
		$jobfolderenabled       	= $opco[0]{ JOBFOLDERENABLED };
		$ttpfolder					= $opco[0]{ TTPFOLDER };
		$preflightprofile			= $opco[0]{ PREFLIGHT_PROFILE };
		$qcenabled					= $opco[0]{ QCENABLED };
		$filenameoverride			= $opco[0]{ FILENAMEOVERRIDE };
		$ftpenabled					= $opco[0]{ FTPENABLED };
		$jobassignment				= $opco[0]{ JOBASSIGNMENT };
		$viewhighres				= $opco[0]{ VIEWHIGHRES };
		$creative					= $opco[0]{ CREATIVE };
		$jobpoolassign				= $opco[0]{ JOBPOOLASSIGN };
		$publishing					= $opco[0]{ PUBLISHING };
		$domainname 				= $opco[0]{ DOMAINNAME };
		$ttpupload 					= $opco[0]{ TTPUPLOAD };
		$remotetempfolder			= $opco[0]{ REMOTETEMPFOLDER };
		$remotettpfolder 			= $opco[0]{ REMOTETTPFOLDER };
		$usejobrepository 			= $opco[0]{ USEJOBREPOSITORY };
		$useassetrepository 		= $opco[0]{ USEASSETREPOSITORY };
		$repositorysync 			= $opco[0]{ REPOSITORYSYNC };
		$syncstart 					= $opco[0]{ SYNCSTART };
		$syncend 					= $opco[0]{ SYNCEND };
		$adgatesend 				= $opco[0]{ ADGATESEND };
		$thumbview          		= $opco[0]{ THUMBVIEW };
		$enablelocalupload     		= $opco[0]{ ENABLELOCALUPLOAD };
		$localuploadsource      	= $opco[0]{ LOCALUPLOADSOURCE };
		$localuploaddestination 	= $opco[0]{ LOCALUPLOADDESTINATION };
		$estimateon             	= $opco[0]{ ESTIMATEON };
		$updateflag					= 1;		
	}

	
	if ( $formPosted eq "yes" ) { #update the Operating Company 
		#validate (only validates opcoid and opconame)
		%error = validateOperatingCompany( $opcoid, $opconame, $rootpath, $wippath, $mountpath, $preflightin, $preflightpassed, $preflightfail, $preflightwarning, $ftpserver, 
				$ftpusername, $ftppassword, $bwdef, $bwcroppeddef, $lowrescroppeddef, $lowresdef, $hiresdef, $proofonlydef, $rgbdef, $rgbcroppeddef, 
				$rgbjpegdef, $colourmanagedef, $colourmanage_isactivedef, $optimisedef, $optimise_isactivedef, $pdfcomparedef, $filenameconfig, $pathtype, $jobfolderenabled,
				$ttpfolder, $preflightprofile, $qcenabled, $filenameoverride, $ftpenabled, $jobassignment, $viewhighres, $creative, $jobpoolassign, $updateflag, $publishing, 
				$domainname, $ttpupload, $remotetempfolder, $remotettpfolder, $usejobrepository, $useassetrepository, $repositorysync, $syncstart, $syncend, 
				$adgatesend, $thumbview, $estimateon, $enablelocalupload, $localuploadsource, $localuploaddestination );

		# all ok? Update
		if ( $error{ oktogo } == 1 ) {
			ECMDBOpcos::updateOperatingCompany( $opcoid, $opconame, $rootpath, $wippath, $mountpath, $preflightin, $preflightpassed, $preflightfail, $preflightwarning, $ftpserver, 
				$ftpusername, $ftppassword, $bwdef, $bwcroppeddef, $lowrescroppeddef, $lowresdef, $hiresdef, $proofonlydef, $rgbdef, $rgbcroppeddef, 
				$rgbjpegdef, $colourmanagedef, $colourmanage_isactivedef, $optimisedef, $optimise_isactivedef, $pdfcomparedef, $filenameconfig, $pathtype, $jobfolderenabled,
				$ttpfolder, $preflightprofile, $qcenabled, $filenameoverride, $ftpenabled, $jobassignment, $viewhighres, $creative, $jobpoolassign, $publishing,
				$domainname, $ttpupload, $remotetempfolder, $remotettpfolder, $usejobrepository, $useassetrepository, $repositorysync, $syncstart, $syncend, 
				$adgatesend, $thumbview, $estimateon, $enablelocalupload, $localuploadsource, $localuploaddestination );
		}
	}

	# get the assigned vendors for this operating company
	@vendors	= getOperatingCompanyVendors( $opcoid );
	@opco		= getOperatingCompanies();
	my @departmentchecklist 	= ECMDBOpcos::db_getdepartmentstatus($opcoid); ##Getting all department updates to pass to HTML
	my @teamchecklist 			= ECMDBOpcos::db_getteamstatus($opcoid); ##Getting all department updates to pass to HTML
	my @querytypechecklist 		= ECMDBOpcos::db_getquerytype($opcoid); ##Getting all Query types to pass to HTML
	my @advisorysColourDetails	= ECMDBOpcos::db_getadvisorysColourDetails( $opcoid ); ##To get all advisory alowing with color based on opcoid.

	my $vars = {
		error 			=> \%error,
		vendors			=> \@vendors,
		opcoid			=> $opcoid,
		opcos			=> \@opco,
		opconame		=> $opconame, 
		rootpath		=> $rootpath, 
		wippath			=> $wippath,
		mountpath               => $mountpath, 
		preflightin		=> $preflightin, 
		preflightpassed		=> $preflightpassed, 
		preflightfail		=> $preflightfail, 
		preflightwarning	=> $preflightwarning, 
		ftpserver		=> $ftpserver, 
		ftpusername		=> $ftpusername, 
		ftppassword		=> $ftppassword, 
		bwdef			=> $bwdef,
		bwcroppeddef		=> $bwcroppeddef, 
		lowrescroppeddef	=> $lowrescroppeddef, 
		lowresdef		=> $lowresdef, 
		hiresdef		=> $hiresdef, 		
		proofonlydef		=> $proofonlydef, 
		rgbdef			=> $rgbdef, 
		rgbcroppeddef		=> $rgbcroppeddef, 
		rgbjpegdef		=> $rgbjpegdef, 
		colourmanagedef		=> $colourmanagedef, 
		colourmanage_isactivedef	=> $colourmanage_isactivedef,
		optimisedef		=> $optimisedef,
		optimise_isactivedef	=> $optimise_isactivedef,
		pdfcomparedef		=> $pdfcomparedef, 
		filenameconfig		=> $filenameconfig, 
		pathtype		=> $pathtype,
		jobfolderenabled	=> $jobfolderenabled,
		ttpfolder		=> $ttpfolder,
		preflightprofile	=> $preflightprofile,
		qcenabled		=> $qcenabled,
		filenameoverride	=> $filenameoverride,
		ftpenabled		=> $ftpenabled,
		jobassignment 		=> $jobassignment,
		viewhighres 		=> $viewhighres,
		creative 		=> $creative,
		jobpoolassign 		=> $jobpoolassign,
		publishing		=> $publishing,
		domainname		=> $domainname,
		ttpupload		=> $ttpupload,
		remotetempfolder	=> $remotetempfolder,
		remotettpfolder		=> $remotettpfolder,
		usejobrepository	=> $usejobrepository,
		useassetrepository	=> $useassetrepository,
		repositorysync		=> $repositorysync,
		syncstart		=> $syncstart,
		syncend			=> $syncend,
		adgatesend		=> $adgatesend,
		thumbview		=> $thumbview,
		estimateon		=> $estimateon,
		enablelocalupload	=> $enablelocalupload,
		localuploadsource	=> $localuploadsource,
		localuploaddestination	=> $localuploaddestination,
		imageworkflow		=> ECMDBOpcos::db_isOpcoImageWorkFlow( $opcoid ),
		bookingform		=> ECMDBImageworkflow::db_isImageBookingForm( $opcoid ),
		xmppath			=> ECMDBImageworkflow::db_getXMPPath( $opcoid ),
		tmppath			=> ECMDBImageworkflow::db_getTMPPath( $opcoid ),
		batchserver		=> ECMDBImageworkflow::db_getBatchserver( $opcoid ),
		batchuser		=> ECMDBImageworkflow::db_getBatchuser( $opcoid ),
		batchpassword		=> ECMDBImageworkflow::db_getBatchpassword( $opcoid ),
		Autocheckout		=> ECMDBImageworkflow::db_Autocheckoutdetails($opcoid, "AUTOCHECKOUT" ),
		Autocheckouttime	=> ECMDBImageworkflow::db_Autocheckoutdetails($opcoid, "AUTOCHECKOUTTIME"),
		AutocheckoutOPCOID	=> ECMDBImageworkflow::db_Autocheckoutdetails($opcoid, "CHECKOUTOPCOID"),
		ingestionhost		=> ECMDBImageworkflow::db_Autocheckoutdetails($opcoid, "Imagehost" ),
		ingestionpath		=> ECMDBImageworkflow::db_Autocheckoutdetails($opcoid, "Imagehostpath" ),
		ingestionprotocol	=> ECMDBImageworkflow::db_Autocheckoutdetails($opcoid, "Imagehostprotocol" ),
		ingestionusername	=> ECMDBImageworkflow::db_Autocheckoutdetails($opcoid, "Imagehostusername" ),
		ingestionpassword	=> ECMDBImageworkflow::db_Autocheckoutdetails($opcoid, "Imagehostpassword" ),
		Mastersync		=> ECMDBImageworkflow::db_isMastersync($opcoid),
		Autocheckin		=> ECMDBImageworkflow::db_isAutocheckin($opcoid),
		departmentchecklist	=>\@departmentchecklist,
		teamchecklist		=>\@teamchecklist,
		querytypechecklist  =>\@querytypechecklist,
		advisorysColourDetails  =>\@advisorysColourDetails,
	};
	if ( $mode eq "json" ) {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/editoperatingcompany.html', $vars )
    		|| die $tt->error(), "\n";
}


##############################################################################
#		EditOperatingCompanyVendors
#
##############################################################################
sub EditOperatingCompanyVendors {
	my $query 			= $_[0];
	my $session 		= $_[1];
	my $opcoid			= $query->param( "opcoid" );

	my ( @opcovendors, @vendors, %selectedVendors, $formPosted );

	# get the assigned vendors for this operating company
	@opcovendors 		= getOperatingCompanyVendorCompanies( $opcoid );
	@vendors 			= getAllVendors();

	$formPosted 	= $query->param( "formPosted" ) ||'';

	if ( $formPosted eq "yes" ) {
		my @selected	= $query->param( "companyid" );
		foreach my $v ( @selected ) {
			$selectedVendors{ $v } = 1;
		}

		updateOperatingCompanyVendors( $opcoid, \@selected );
		
	} else {
		foreach my $v ( @opcovendors ) {
			$selectedVendors{$v->{'id'}} = 1;
		}
	}
	
	my $vars = {
		vendors			=> \@vendors,
		opcoid			=> $opcoid,
		selectedVendors	=> \%selectedVendors,
		formPosted		=> $formPosted,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/editoperatingcompanyvendors.html', $vars )
    		|| die $tt->error(), "\n";

}

##############################################################################
#		updateFTPServer
#
##############################################################################
sub updateFTPServer {
	my ( $query, $session ) = @_;

	my $ftpserver		= $query->param( "ftpserver" );
	my $formPosted 		= $query->param( "formPosted" ) ||'';
	my $sidemenu		= $query->param( "sidemenu" );
	my $sidesubmenu		= $query->param( "sidesubmenu" );

	my %error;

	if ( $formPosted eq "yes" ) {

		%error = validateFTPServerAddress( $ftpserver );

		if ( $error{ oktogo } == 1 ) {
			ECMDBOpcos::db_updateFTPServer( $ftpserver );
		}
	}

	my $vars = {
		ftpserver	=> $ftpserver,
		error 		=> \%error,
		sidemenu    => $sidemenu,
		sidesubmenu => $sidesubmenu,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/updateFTPServer.html', $vars )
    		|| die $tt->error(), "\n";
}

##############################################################################
#       validateFTPServerAddress
#
##############################################################################
sub validateFTPServerAddress {
	my %error;

	my $ftpServerAddress 	= $_[0];
	
	$error{ oktogo } = 1;
	
	if( $ftpServerAddress =~ /(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/ )
	{
		#valid IP address
	} else {
		$error{ ftpserver } = "Not a valid IP address!";
		$error{ oktogo } = 0;
	}

	return %error;

}

##############################################################################
#       :validateOperatingCompany
#
##############################################################################
sub validateOperatingCompany {
	my %error;
	my ( $opcoid, $opconame, $rootpath, $wippath, $mountpath, $preflightin, $preflightpassed, $preflightfail, $preflightwarning, $ftpserver, $ftpusername, $ftppassword,
				$bwdef, $bwcroppeddef, $lowrescroppeddef, $lowresdef, $hiresdef, $proofonlydef, $rgbdef, $rgbcroppeddef, $rgbjpegdef, $colourmanagedef,
				$colourmanage_isactivedef, $optimisedef, $optimise_isactivedef, $pdfcomparedef, $filenameconfig, $pathtype, $jobfolderenabled, $ttpfolder, $preflightprofile,
				$qcenabled, $filenameoverride, $ftpenabled, $jobassigned, $viewhighres, $creative, $jobpoolassign, $updateflag, $publishing, $domainname );

	$opcoid				= $_[0]; 
	$opconame			= $_[1]; 
	$rootpath			= $_[2];
        $wippath                        = $_[3];
        $mountpath                      = $_[4];
	$preflightin			= $_[5];
	$preflightpassed		= $_[6];
	$preflightfail			= $_[7];
	$preflightwarning		= $_[8];
	$ftpserver			= $_[9];
	$ftpusername			= $_[10];
	$ftppassword			= $_[11];
	$bwdef				= $_[12];
	$bwcroppeddef			= $_[13];
	$lowrescroppeddef		= $_[14];
	$lowresdef			= $_[15];
	$hiresdef			= $_[16];	
	$proofonlydef			= $_[17];
	$rgbdef				= $_[18];
	$rgbcroppeddef			= $_[19];
	$rgbjpegdef			= $_[20];
	$colourmanagedef		= $_[21];
	$colourmanage_isactivedef	= $_[22];
	$optimisedef			= $_[23];
	$optimise_isactivedef		= $_[24];
	$pdfcomparedef			= $_[25];
	$filenameconfig			= $_[26];
	$pathtype			= $_[27];
        $jobfolderenabled               = $_[28];
	$ttpfolder			= $_[29];
	$preflightprofile		= $_[30];
	$qcenabled			= $_[31];
	$filenameoverride		= $_[32];
	$ftpenabled			= $_[33];
	$jobassigned			= $_[34];
	$viewhighres			= $_[35];
	$creative			= $_[36];
	$jobpoolassign			= $_[37];
	$updateflag			= $_[38];
	$publishing			= $_[39];
	$domainname 			= $_[40];
	$error{ oktogo } 		= 1;
	
	#validate params
	if ( length( $opcoid ) < 1 ) {
		$error{ opcoid } = "Operating Company Id is required.";
		$error{ oktogo } = 0;
	}
	if ( ! looks_like_number( $opcoid )  ) {
		$error{ opcoid } = "Operating Company Id must be a number";
		$error{ oktogo } = 0;
	}
	#only refuse on an add of opco - not on an update
	if ( looks_like_number( $opcoid ) && opcoidExists ( $opcoid ) && ( $updateflag != 1 ) ) {
		$error{ opcoid } = "Operating Company Id already exists";
		$error{ oktogo } = 0;		
	} 
	if ( length( $opconame ) < 1 ) {
		$error{ opconame } = "Operating Company name is required.";
		$error{ oktogo } = 0;
	}

	return %error;
}

##############################################################################
#       getOperatingCompanies ()
#
##############################################################################
sub getOperatingCompanies {
	return ECMDBOpcos::getOperatingCompanies();
}


##############################################################################
#       getOperatingCompany ()
#
##############################################################################
sub getOperatingCompany {
	my $opcoid			= $_[0];
	
	return ECMDBOpcos::getOperatingCompany( $opcoid );
}


##############################################################################
#       getOperatingCompanyVendorCompanies()
#
##############################################################################
sub getOperatingCompanyVendorCompanies {
	my $opcoid			= $_[0];

	return ECMDBOpcos::getOperatingCompanyVendorCompanies( $opcoid );
}


##############################################################################
#       getOperatingCompanyVendors()
#
##############################################################################
sub getOperatingCompanyVendors {
	my $opcoid			= $_[0];

	return ECMDBOpcos::getOperatingCompanyVendors( $opcoid );
}


##############################################################################
#       opcoidExists()
#
##############################################################################
sub opcoidExists {
	my $opcoid			= $_[0];

	return ECMDBOpcos::opcoidExists( $opcoid );
}


##############################################################################
#       getAllVendors()
#
##############################################################################
sub getAllVendors {

	return Companies::getAllVendors();
}


##############################################################################
#       updateOperatingCompanyVendors()
#
##############################################################################
sub updateOperatingCompanyVendors {
	my ( $opcoid, $selected )		=	@_;

	ECMDBOpcos::updateOperatingCompanyVendors( $opcoid, $selected );

}

##############################################################################
#       ChangeOperatingCompany()
#
##############################################################################
sub ChangeOperatingCompany {
	my $query 			= $_[0];
	my $session 		= $_[1];
	
	my $newOperatingCompany	= $query->param( "opcoid" );
	
	$session->param( 'opcoid',  $newOperatingCompany );
	# when selecting a new opco - reset the search filters
	$session->param( "agencyFilter", "" );
	$session->param( "clientFilter", "" );
	$session->param( "projectFilter", "" );
	$session->param( "preflightFilter", "" );
	
	#Users::updateOpCoId($session->param('userid'), $newOperatingCompany);
	# caller ie jobflow.pl, then calls Jobs::Dashboard( $query, $session );
	
}

##############################################################################
# Customer functions
##############################################################################
sub ListCustomers {
	my $query 		= $_[0];
	my $session 	= $_[1];
 	my $sidemenu	= $query->param( "sidemenu" );
	my $sidesubmenu	= $query->param( "sidesubmenu" );
	my @customers	= ECMDBOpcos::getCustomers();
	
	my $vars = {
		customers			=> \@customers,
		sidemenu    => $sidemenu,
		sidesubmenu => $sidesubmenu,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/listcustomers.html', $vars )
    		|| die $tt->error(), "\n";
	
}


##############################################################################
# Customer functions
##############################################################################
sub AddCustomer {
	my $query	= $_[0];
	my $session 	= $_[1];

	my $formPosted 	= $query->param( "formPosted" ) ||'';
	my $opcoid 	= $query->param( "opcoid" );
	my $customer    = $query->param( "customer" );
	$customer	= General::htmlEditFormat( $customer ) if defined $customer;

	my $message	= "";
 	my $sidemenu	= $query->param( "sidemenu" );
	my $sidesubmenu	= $query->param( "sidesubmenu" );

	my ( %error, $vars, @opcos );

	if ( $formPosted eq "yes" ) { #update the Operating Company 
		#validate
		%error = validateCustomer( $opcoid, $customer );

		# all ok? Update
		if ( $error{ oktogo } == 1 ) {
			ECMDBOpcos::addCustomer( $opcoid, $customer );

			$message = "Customer added OK";
		}
	}

	#get operating companies
	@opcos = getOperatingCompanies();
	$customer = '' unless defined $customer;
	
	$vars = {
		opcos			=> \@opcos,
 		opcoid 			=> $opcoid, 
 		customer		=> uc($customer), 
		error 			=> \%error,
		message			=> $message,
		sidemenu    => $sidemenu,
		sidesubmenu => $sidesubmenu,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/addcustomer.html', $vars )
    		|| die $tt->error(), "\n";

}

##############################################################################
# validateCustomer
##############################################################################
sub validateCustomer {
	my $opcoid		= $_[0];
	my $customername	= $_[1];
	my %error;

	$error{ oktogo } 	= 1;
	
#	- we no longer REQUIRE that a Customer 'belong' to an opco
#	- because we need to add 'virtual' customers to opcos
#	- to allow remote Studios to work on jobs belonging to customers.
#	if (! ECMDBOpcos::customerValid( $opcoid, $customername ) ) {
#		$error{ customer } = "This customer does not belong to this operating company";
#		$error{ oktogo } = 0;
#	}
	
	if ( ECMDBOpcos::customerExists( $opcoid, $customername ) ) {
		$error{ customer } = "This customer already exists for this operating company";
		$error{ oktogo } = 0;		
	}

	return %error;	
}


##############################################################################
# EditCustomer
##############################################################################
sub EditCustomer {
	
	my ( $query, $session ) = @_;
	my $mode 		= $query->param( "mode" ) ||'';
	my $id			= $query->param( "id" );
    my $jobnumber   = $query->param( "jobnumber" );

    if ( ! $id )   
    {
        $id = ECMDBOpcos::getCustomerId($jobnumber);
    }
	my $formPosted				= $query->param( "formPosted" ) ||'';
	my $filename				= $query->param("filename");
	my $syncstart				= $query->param("syncstart");
	my $syncend					= $query->param("syncend");
	my $estimate       			= $query->param("estimate");
	my $documentmaxsize	 		= $query->param("documentmaxsize") || 10;
	my $digitalAssetMaxSize 	= $query->param("digitalUploadSizeLimit") || 10;
	my $mediaid	 				= $query->param("mediaid") || 1; # select/option rather than checkbox
	my ($customerName,$customerlogo_name)			= ECMDBOpcos::getCustomerName( $id );
	my $opcoid      			= ECMDBOpcos::getCustomerOpcoid( $id );
	my $opconame      			= ECMDBOpcos::getOpname( $opcoid );
	my @customerusers			= ECMDBCustomerUser::getCustomerUsers( $id );
	my $docMaxSizes_ref			= ECMDBOpcos::getDocMaxSizes();
	my $digitalSizeLimit_ref	= ECMDBOpcos::getdigitalSizeLimit();
	my $customerLogoName		= $query->param("customerlogo");
	my $customerLogo			= $query->upload("customerlogo");
	my @addresses;			# Customer addresses
	my @fileoptions;		# the file options for this customer.
	my %settings;			# Settings for all the checkboxes (key is column name, value is 0 or 1)
 	my $newlogoname = '';
	my $logouploadlog = '';
	if ( (defined $customerLogoName) && ($customerLogoName ne '') &&  $customerLogoName =~ /^(.*)\.(.*)$/ ) {
		my $logo_repository     = Config::getConfig("brandedlogo_repository") || "/gpfs/SERVER_ADMIN/DIGITAL_JOBFLOW_TEMPLATES";
		($newlogoname,$logouploadlog) = Customerlogo::upload_logo($opcoid,$customerName,$customerLogo,$logo_repository);
	}

	#my $upload_dir          	= Config::getConfig("uploaddirectory");
    #my $dir = getcwd;
	#my $abs_path = abs_path(); 
	if ( $formPosted eq "yes" ) {

		# populate settings from check boxes
		foreach my $col ( @customer_cbs ) {
			my $qpn = $col;
			if ( $col =~ /=/ ) {
				( $col, $qpn ) = $col =~ m/([\w\_]*)=([\w\_]*)/; # left is column name, right is query param name
			}
			my $qpv = $query->param( $qpn ) ||'off';
			my $val = 0; 
			$val = 1 if $qpv eq "on";
			$settings{ $col } = $val;
		}

		# validate emails
		for ( my $i = 1; $i <= 10 ; $i++) {
			my $addr = $query->param( "address$i" );
			if ( ( defined $addr ) && General::isValidEmail( $addr ) ) {
				push ( @addresses, $addr );
			} 
		}

		# update emails
		ECMDBOpcos::updateCustomerAddresses( \$id, \@addresses );

		# update the customer
		ECMDBOpcos::updateCustomer( $id, $filename, $syncstart, $syncend, $estimate, $documentmaxsize, $digitalAssetMaxSize, $mediaid, \%settings, $newlogoname );

		# update the estimatetable - pass opcoid, custid, mediaid and estimation time
		# PD-3500 we are removing estimation, beacuse hereafter we are going to get that value from CMD
		#ECMDBOpcos::updateestimate($opcoid, $id, $mediaid, $estimate);
		#$estimate   = getCustomerDefaultEstimate( $opcoid, $id, $mediaid ) || "0.00";

	} else {
		# Initial entry

		$filename	= ECMDBOpcos::getCustomerFilenameTemplate( $id );

		$settings{ jobpoolassign }	= ECMDBOpcos::getCustomerJobPoolAssign( $id );
#		$settings{ estimateon }		= ECMDBOpcos::getCustomerEstimateOn( $id ); #PD-3500 We no longer need estimation
		$settings{ pdfcomparedef }	= ECMDBOpcos::getCustomerPDFCompareDef( $id );
		$settings{ pfc_disable }	= ECMDBOpcos::getCustomerPFCDisabled( $id );

		$settings{ autocheckin }	= ECMDBOpcos::getCustomerAutoCheckin($id);
		$settings{ useagencyrefasfilename } = ECMDBOpcos::getCustomerAgencyRefAsFilename($id);

		my @repoOptions	= ECMDBOpcos::getCustomerRepositoryOptions( $id );

		$settings{ usejobrepository }	= $repoOptions[0];
		$settings{ repoautoarchive }	= $repoOptions[1];
		$settings{ useassetrepository }	= $repoOptions[2];
		$settings{ repositorysync } 	= $repoOptions[3];
		$syncstart 			= $repoOptions[4];
		$syncend			= $repoOptions[5];
		$settings{ enhancedftp }	= $repoOptions[6];

		@fileoptions	= ECMDBOpcos::getCustomerFileoptions( $id );
		foreach my $k ( 'additionalfiletypes', 'bw_asset', 'bw_crp_asset', 'lr_asset', 'lr_crp_asset', 'rgb_asset', 'rgb_crp_asset', 'rgb_jpg_asset', 
					'hires_crp_bleedbox_asset', 'hires_crp_trimbox_asset', 'aux_files', 'split_pages', 'open_assets', 'high_res',
					'bwdef', 'bwcroppeddef', 'lowrescroppeddef', 'lowresdef', 'hiresdef', 'proofonlydef', 'rgbdef', 'rgbcroppeddef', 'rgbjpegdef',
					'hirescroppedbleedboxdef', 'hirescroppedtrimboxdef', 'docmaxsize', 'digitalmaxsize','zip_dir', 'es_compare', 'enableproducts', 'enabledelete', 'enable_one2edit') {
			$settings{ $k } = $fileoptions[0]{ $k };
		}

		$documentmaxsize	= $fileoptions[0]{docmaxsize};
		$digitalAssetMaxSize	= $fileoptions[0]{digitalmaxsize};
		$mediaid		= $fileoptions[0]{LASTUPDMEDIA};
		$estimate		= getCustomerDefaultEstimate( $opcoid, $id, $mediaid ) || "0.00";

		my @myAdd		= ECMDBOpcos::getCustomerAddresses( $id );

		foreach ( @myAdd ) {
			push ( @addresses, $_->{'email'} );
		}
	}
	
	my @customerftps = ECMDBOpcos::getCustomerFTPs( $id );
	my @mediatypes   = Checklist::getMediatypes();

	my $vars = {
		id			=> $id,
		opcoid			=> $opcoid,
		customerName 		=> $customerName,
		filename		=> $filename,

		customerftps		=> \@customerftps,
		addresses		=> \@addresses,  
		customerusers		=> \@customerusers,

		syncstart		=> $syncstart,
		syncend			=> $syncend,
		estimate                => $estimate,
		mediaid			=> $mediaid,
		mediatypes		=> \@mediatypes,

		maxdocsizes		=> $docMaxSizes_ref,
		documentmaxsize		=> $documentmaxsize,
		digitalSizeList		=> $digitalSizeLimit_ref,
		currDigitalAssetMaxSize	=> $digitalAssetMaxSize,
		customerlogo		=> $customerLogo,
		logouploadlog	=> $logouploadlog,
		customerlogo_name	=> $customerlogo_name,

	};

	# Add the check box settings
	foreach my $col ( @customer_cbs ) {
		my $qpn = $col;
		if ( $col =~ /=/ ) {
			( $col, $qpn ) = $col =~ m/([\w\_]*)=([\w\_]*)/; # left is column name, right is query param name
		}
		$vars-> { $qpn } = $settings{ $col } || 0;
	}

	if ( $mode eq "json" ) {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/editcustomer.html', $vars )
    		|| die $tt->error(), "\n";
	
}



##############################################################################
# AddCustomerFTP
##############################################################################
sub AddCustomerFTP {
	my $query 		= $_[0];
	my $session 	= $_[1];

	my $customerid		=  $query->param( "customerid" );
	my $customerName	= ECMDBOpcos::getCustomerName( $customerid );
	my $label			= $query->param( "label" );
	my $level1			= $query->param( "level1" );
	my $level2			= $query->param( "level2" );
	my $level3			= $query->param( "level3" );
	my $username		= $query->param( "username" );
	my $password		= $query->param( "password" );
	my $domainname		= $query->param( "domainname" );
	my $formPosted		= $query->param( "formPosted" ) ||'';
	my %error;
	my $message			= "";


	if ( $formPosted eq "yes" ) { #update the FTP
		#validate
		%error = validateCustomerFTP( $label, $level1, $level2, $level3, $username, $password, $domainname );

		#	 all ok? Update
		if ( $error{ oktogo } == 1 ) {
			ECMDBOpcos::addCustomerFTP( $customerid, $label, $level1, $level2, $level3, $username, $password, $domainname );
		
			$message = "Customer FTP added OK";
		}
	}

	my $vars = {
		customerid		=> $customerid,
		customerName 	=> $customerName,
		label			=> $label,
		level1			=> $level1,
		level2			=> $level2,
		level3			=> $level3,
		username		=> $username,
		password		=> $password,
		domainname		=> $domainname,
		message			=> $message,
		error 			=> \%error,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/addcustomerftp.html', $vars )
    		|| die $tt->error(), "\n";

}

##############################################################################
# EditCustomerFTP
##############################################################################
sub EditCustomerFTP {
	my $query 	= $_[0];
	my $session 	= $_[1];

	my $ftpid	= $query->param( "ftpid" );
	my $message;
	
	my ( $formPosted, %error, @ftp );
	
	my ( $label, $level1, $level2, $level3, $username, $password, $domainname );

	$formPosted 	= $query->param( "formPosted" ) ||'';

	if ( $formPosted eq "yes" ) { 	#get from form
		$label 				= General::htmlEditFormat( $query->param( "label" ) );
		$level1 			= General::htmlEditFormat( $query->param( "level1" ) );
		$level2 			= General::htmlEditFormat( $query->param( "level2" ) );
		$level3 			= General::htmlEditFormat( $query->param( "level3" ) );
		$username 			= General::htmlEditFormat( $query->param( "username" ) );
		$password 			= General::htmlEditFormat( $query->param( "password" ) );
		$domainname 		= General::htmlEditFormat( $query->param( "domainname" ) );
	} else {					# from DB
		my @ftp				= ECMDBOpcos::getCustomerFTP( $ftpid );

		$label				= $ftp[0]{ label };
		$level1				= $ftp[0]{ level1 }; 
		$level2				= $ftp[0]{ level2 }; 
		$level3				= $ftp[0]{ level3 }; 
		$username			= $ftp[0]{ username }; 
		$password			= $ftp[0]{ password }; 
		$domainname			= $ftp[0]{ domainname }; 
	}

	if ( $formPosted eq "yes" ) { #update the Operating Company 
		#validate
		%error = validateCustomerFTP( $label, $level1, $level2, $level3, $username, $password, $domainname );

		# all ok? Update
		if ( $error{ oktogo } == 1 ) {
			ECMDBOpcos::updateCustomerFTP( $ftpid, $label, $level1, $level2, $level3, $username, $password, $domainname );


			$message = "Customer FTP updated OK";

		}
	}

	my $vars = {
		error 				=>	\%error,
		ftpid				=> $ftpid,
		label				=> $label,
		level1				=> $level1,
		level2				=> $level2,
		level3				=> $level3,
		username			=> $username,
		password			=> $password,
		domainname			=> $domainname,
		message				=> $message,
		
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/editcustomerftp.html', $vars )
    		|| die $tt->error(), "\n";	
}

##############################################################################
# AddCustomerFTP
##############################################################################
sub validateCustomerFTP {

	my $label			= $_[0];
	my $level1			= $_[1];
	my $level2			= $_[2];
	my $level3			= $_[3];
	my $username		= $_[4];
	my $password		= $_[5];
	my $domainname		= $_[6];
	my %error;
	
	$error{ oktogo }	= 1;	
	
	if ( length( $label ) < 1 ) {
		$error{ label } = "Label is required";
		$error{ oktogo } = 0;
	}

	# level 1 is no longer required
	#if ( length( $level1 ) < 1 ) {
	#	$error{ level1 } = "Level1 is required";
	#	$error{ oktogo } = 0;
	#}
	
	if ( length( $username ) < 1 ) {
		$error{ username } = "Username is required";
		$error{ oktogo } = 0;
	}
	if ( length( $password ) < 1 ) {
		$error{ password } = "Password is required";
		$error{ oktogo } = 0;
	}
	if ( length( $domainname) < 1 ) {
		$error{ domainname } = "Domainname is required";
		$error{ oktogo } = 0;
	}

	return %error;

}

##############################################################################
# AddCustomerUser
##############################################################################
sub AddCustomerUser {

	my $query 			= $_[0];
	my $session 		= $_[1];

	my $customerid		= $query->param( "customerid" );
	my $users		= $query->param( "users" );
	my $formPosted		= $query->param( "formPosted" ) ||'';
	my $customerName	= ECMDBOpcos::getCustomerName( $customerid );
	my $opcoid		= ECMDBOpcos::getCustomerOpcoid( $customerid );
	my $opconame		= ECMDBOpcos::getOpcoName( $opcoid );
	my %error;
	my $message			= "";
	my ($usersadded, $usersnotadded);
	$error{ oktogo }	= 1;
	
	print "cust: $customerName<br>";
	print "opcooid: $opcoid<br>";

	if ( $formPosted eq "yes" && $opcoid != 0 ) {
		my @usersArray = split(",", $users);
		#add each user to this customer
		
		#loop over users
		for my $user ( @usersArray ) {
			print "user: $user<br>";
			#if user is not already assigned to this opco add it
			my $isUserAssignedToOpco = ECMDBUser::isUserAssignedToOpco( $user, $opcoid );
			my $userExistsById = ECMDBUser::userExistsById( $user );
			my $isUserAssignedToCustomer = ECMDBCustomerUser::isUserAssignedToCustomer( $user, $customerid );
			print "isUserAssignedToOpco: $isUserAssignedToOpco<br>";
			print "userExistsById: $userExistsById<br>";
			print "isUserAssignedToCustomer: $isUserAssignedToCustomer<br>";

			if ( $isUserAssignedToOpco && $userExistsById && ( !$isUserAssignedToCustomer )) {
				#add this operating company to this user
				ECMDBCustomerUser::addCustomerUser( $customerid, $user );
				
				$message = "Users added to customer";
			} else {
				$usersnotadded .= $user.",";
			}
		}
	}

	my $vars = {
		customerid		=> $customerid,
		customerName 		=> $customerName,
		error 			=> \%error,
		message			=> $message,
		usersadded		=> $usersadded,
		usersnotadded		=> $usersnotadded,
		opconame		=> $opconame,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/addcustomeruser.html', $vars )
    		|| die $tt->error(), "\n";
}


##############################################################################
# RemoveCustomerUser
##############################################################################
sub RemoveCustomerUser {

	my $query 		= $_[0];
	my $session 		= $_[1];

	my $customerid		= $query->param( "customerid" );
	my $users		= $query->param( "users" );
	my $formPosted		= $query->param( "formPosted" ) ||'';
	my $customerName	= ECMDBOpcos::getCustomerName( $customerid );
	my $opcoid		= ECMDBOpcos::getCustomerOpcoid( $customerid );
	my $opconame		= ECMDBOpcos::getOpcoName( $opcoid );
	my %error;
	my $message		= "";
	my ($usersremoved, $usersnotremoved);
	$error{ oktogo }	= 1;
	
	print "cust: $customerName<br>";
	print "opcooid: $opcoid<br>";

	if ( $formPosted eq "yes" && $opcoid != 0 ) {
		my @usersArray = split(",", $users);
		#remove each user to this customer
		
		#loop over users
		for my $user ( @usersArray ) {
			print "user: $user<br>";
			my $userExistsById = ECMDBUser::userExistsById( $user );
			if (! $userExistsById ) {
				$usersnotremoved .= $user.",";
			} else {
				ECMDBCustomerUser::removeCustomerUser( $customerid, $user );
				$usersremoved .= $user.",";
			}
		}
	}

	my $vars = {
		customerid		=> $customerid,
		customerName 		=> $customerName,
		error 			=> \%error,
		message			=> $message,
		usersremoved		=> $usersremoved,
		usersnotremoved		=> $usersnotremoved,
		opconame		=> $opconame,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/removecustomeruser.html', $vars )
    		|| die $tt->error(), "\n";
}
##############################################################################
# getCustomerDefaultEstimate 
##############################################################################
sub getCustomerDefaultEstimate {
        my $numParameters = @_ ;
        my $opcoid              = "";
        my $customerid  = "";
        my $mediaid             = "";
        my $query               = "";
        my $session             = "";
        my $mode                = "";

        ##numParameters to decide whether this function is called through ajax for onchange if it is 2. Else it is called normal by form post
    	if ( $numParameters == 2 )
        {
                $query      = $_[0];
                $session    = $_[1];
                $opcoid     = $query->param( "opcoid" );
                $customerid = $query->param( "custid" );
                $mediaid    = $query->param( "mediaid" );
       			$mode       = $query->param( "mode" );
    	}
        else
        {
        		($opcoid, $customerid, $mediaid)= @_;
        }

        my ( $estimate );

        if ( $customerid && (! looks_like_number( $customerid )) ) {
                return $estimate;
        }
		$estimate=ECMDBOpcos::getCustomerDefaultEstimateDB($opcoid, $customerid, $mediaid);	

    my $vars = {
          estimate=>$estimate,
    };

    if ( $mode eq "json" ){
                my $json = encode_json ( $vars );
                print $json;
                return $json;
    }
    return $estimate;
}
##############################################################################
##############################################################################
1;
