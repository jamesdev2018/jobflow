#!/usr/local/bin/perl -w
package Estimate;

use strict;
use warnings;

use Template;
use Data::Dumper;
use JSON;
use Carp qw/carp/;
use Scalar::Util qw( looks_like_number );

# Home grown

use lib 'modules';

use CTSConstants;
use Validate;

require "databaseJob.pl";
require "databaseOpcos.pl";
require 'databaseAudit.pl';
require 'general.pl';
##################################################################
# the template object 
##################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";

# includes AlterEstimate, updateEstimate, processNewEstimate

sub updateEstimate {
	my ( $query, $session ) = @_;

	my $mode = $query->param( "mode" ) ||'';

	## called when Actions->estimations to auto detect the customer name and media types. 
	## So the only input can get from table row is job number. So reusing getCustomerId to get
	## cust id of that job number as custid is the only input for this function. PD-2140

	my $jobnumber 		=  $query->param( "jobnumber" );
	my $estimatevalue 	=  $query->param( "estimatevalue" );
	my $mediavalue		=  $query->param( "mediavalue" );

	my $vars;

	eval {
		Validate::job_number( $jobnumber );
		die "Missing estimatevalue" unless defined $estimatevalue;
		die "Missing mediavalue" unless defined $mediavalue;

		my $id = ECMDBOpcos::getCustomerId( $jobnumber );
		my $opcoid = ECMDBOpcos::getCustomerOpcoid( $id );
		ECMDBOpcos::updateestimate( $opcoid, $id, $mediavalue, $estimatevalue );

		$vars = { jobnumber => $jobnumber, estimatevalue => $estimatevalue, mediavalue => $mediavalue };
	};
	if ( $@ ) {
		my $error = $@;
		$vars = { jobnumber => $jobnumber||'', estimatevalue => $estimatevalue||'', mediavalue => $mediavalue||'', error => $error };
	}
	my $json = encode_json ( $vars );
	print $json;    
	return $json;
}

##############################################################################
#   AlterEstimate()
#	change the estimate on selected jobs:
#
##############################################################################
sub AlterEstimate {
	my ( $query, $session ) = @_;

	my @jobs;
	my $newEstimate	= $query->param( "newEstimate" );
	my $menuation	= $query->param( "menuation" );

	if( $menuation eq "Yes" ) {
		@jobs = split( ',', $query->param( "estimateJL" ) );
	} else {
		@jobs   = $query->param( "jobs" );
	}

	my $vars = {
		jobs		=> \@jobs,
		newestimate	=> $newEstimate,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/alterEstimate.html', $vars )
			|| die $tt->error(), "\n";

}

##############################################################################
#   processNewEstimate()
#	change the estimate on selected jobs:
#
##############################################################################
sub processNewEstimate {
	my ( $query, $session ) = @_;

	my @jobs		= split( ',', $query->param( "jobs" ) );
	my $newEstimate		= $query->param( "newEstimate" );
	if ( looks_like_number( $newEstimate ) ) {
		foreach my $job ( @jobs ) {
			ECMDBJob::db_updateEstimate ( $job, $newEstimate );

			# audit this		
			ECMDBAudit::db_auditLog( $session->param( 'userid' ), "TRACKER3PLUS", $job, "Job: $job has had estimate changed to : $newEstimate hours" );
		}
	}

	# Dashboard( $query, $session ); -- done by jobflow as follow-on action
}



1;

