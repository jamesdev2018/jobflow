﻿#!/usr/local/bin/perl
package KanbanBoard;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use JSON;
use Encode qw(decode encode);
use Data::Dumper;
use Template;
use Carp qw/carp/;
use URI::Escape;
# Home grown - please do NOT include jobs.pl or anything that includes jobs.pl

use lib 'modules';

use SessionConstants;

require "databaseAudit.pl"; # db_auditLog
require "databaseKanbanboard.pl"; # many
require "databaseMessages.pl";  # db_getUsersForLookup
require "databaseJoblist.pl"; # get_operating_company_flags, db_getFilteredJobs
require "filters.pl"; # package Filters, get_joblist_filters, update_filter_values
require "databaseUserFilter.pl";
require "userfilter.pl";
require "Watchlist.pl";

my %orderbyname = (
    1           => 'is assigned to Department',
    2           => 'is assigned to Team',
    3           => 'Job Status changed to',
    4           => 'Operator Status changed to',
    5           => 'Advisory has been set to',
    6           => 'Qc Status changed to',
    7           => 'Booked By changed to',
    8           => 'Producer changed to',
    9           => 'is assigned to',
);
my %preflightstatusname = (
	1 => "In Progress",
	2 => "Passed",
	3 => "Warning",
	4 => "Failed",
	5 => "None",
	6 => "Dispatched",
	7 => "Creative",
);	

use constant BLANK => '(blank)';

##################################################################
# the template object
##################################################################
my $tt = Template->new( {
    RELATIVE => 1,
} ) || die "$Template::ERROR\n";

use constant KANBAN_UNASSIGNED_LABEL_NAME => "Unassigned"; # Used for kanban board to display unassigned jobs. Same constant used in databaseKanbanBoard.pl
use constant KANBAN_JOB_LIMIT_DEFAULT => 200;
use constant KANBAN_NOT_ASSIGNED_LABLE_NAME => "NONE";

# function lane_f_? helper functions to extract / group data for Kanban (see group_by_lane_type)

sub lane_f_depart {
	my ( $job ) = @_;

	return $job->{name} || KANBAN_NOT_ASSIGNED_LABLE_NAME; # Department
}
sub lane_f_team {
	my ( $job ) = @_;

	return $job->{teamname} || KANBAN_NOT_ASSIGNED_LABLE_NAME; # Team
}

sub lane_f_jobstatus {
	my ( $job ) = @_;

	my $lane_name = '';
	if ( $job->{preflightid} eq '6' ) {
		$lane_name = "Release";	
	} elsif ( $job->{operatorstatus} eq 'U' || $job->{operatorstatus} eq 'A' ) {
		$lane_name = "Pending";
	} elsif ( $job->{operatorstatus} eq 'W' || $job->{operatorstatus} eq 'P' ) {
		$lane_name = "In Progress";
	} elsif ( $job->{qcstatusid} eq '1' ) {
		$lane_name = "Awaiting QC";
	} elsif ( $job->{qcstatusid} eq '2' ) {
		$lane_name ="QC Approved";
	} elsif ( $job->{qcstatusid} eq '3' || $job->{operatorstatus} eq 'R' ) {
		$lane_name ="QC Declined";
	}
	return $lane_name;
}

sub lane_f_opstatus {
	my ( $job ) = @_;

	my $status = $job->{op_status}; # id is in operatorstatus
	$status = '' unless defined $status;
	return $status; # Operator Status
}
sub lane_f_querytype {
	my ( $job ) = @_;

	return $job->{querytype}; # Query type
}
sub lane_f_qcstatus {
	my ( $job ) = @_;

	return $job->{qcstatus}; # QC status
}
sub lane_f_bookedby {
	my ( $job ) = @_;

	return $job->{bookedby}; # Booked by
}
sub lane_f_producer {
	my ( $job ) = @_;

	return 'undef' if ( ! defined $job->{producer} );
	return BLANK if ( $job->{producer} eq '' );
	return $job->{producer}; # Producer
}
sub lane_f_assignee {
	my ( $job ) = @_;

	return $job->{assignee}|| KANBAN_UNASSIGNED_LABEL_NAME;
}

sub get_traffic_light_key {
	my ( $card_list_id ) = @_;

	my $traffic_date_keys = {
		11 => "requiredDate",
		12 => "dispatchdate",
		#13 => "MATDATE",
		13 => "matdate_datetime",
	};

	return $traffic_date_keys->{$card_list_id} || '-';
}

# Helper for Dashboard_view to group data for Kanban board
# Since we are getting the same field for each job (according to lane_type), use a function handle
# to decide which column to use for the grouping, avoiding a big if then else block.
# Inputs
# - jobs
# - lane_type
# Returns
# ref to hash 

sub group_by_lane_type {
	my ( $jobs, $lane_type, $vars, $traffic_date_key ) = @_;
	my $count = 0;
	my $lane_functions = { # Map lane_type to a function handle, to speed up the loop that groups the kanban data
				1 => \&lane_f_depart,
				2 => \&lane_f_team,
				3 => \&lane_f_jobstatus,
				4 => \&lane_f_opstatus,
				5 => \&lane_f_querytype,
				6 => \&lane_f_qcstatus,
				7 => \&lane_f_bookedby,
				8 => \&lane_f_producer,
				9 => \&lane_f_assignee,
	};
	my $lane_func = $lane_functions->{ $lane_type };
	die "Invalid lane type $lane_type" unless defined $lane_func;

	my %kanbanjobs; # Only used in lane_type param is present

	foreach my $row ( @{ $jobs } ) {
		my $lane_name = $lane_func->( $row );
		if ( ! defined $lane_name ) {
			warn "No lane name, lane type $lane_type\n";
		} elsif ( $lane_name ne '' ) {
			my $job_number = $row->{job_number};

			my $traffic_date = $row->{$traffic_date_key};

			if ( ($traffic_date_key eq '-') || (! $traffic_date) ) {
				$traffic_date = "-";
			}

			no warnings 'once';
			my ( @product_assignee_details ) = $Config::dba->process_sql(
					"SELECT CONCAT('Assignee : ',u.username,' : ',p.PRODUCTNAME) as product_details " .
					" FROM products p INNER JOIN users u ON u.id=p.USERID WHERE p.USERID > 0 AND p.JOB_NUMBER = ?",
						[ $job_number ] );

			#To get the totla estimation hours of product(max job version)
			my ( $total_prod_estimation )	= $Config::dba->process_oneline_sql("SELECT coalesce(sum(p.ESTAMOUNT),0) AS total_estimation " . 
											" FROM products p where p.JOB_NUMBER=? AND p.ISACTIVE=1 AND p.STATUS != 4 AND p.ISDELETED !=1 " . 
											" AND p.JOBVERSION = ( SELECT MAX(JOBVERSION) FROM products WHERE job_number=? )", [ $job_number, $job_number ] );;
			# Add status notes
			my $mediatype = $row->{actiontype};
			my $status_note = $row->{status_note} || '-'; # broadcasttracker3.smokecmd for broadcast jobs

			if ( ( ! defined $mediatype ) || ( uc( $mediatype ) ne 'BROADCAST' ) ) {
				$status_note = ECMDBKanbanboard::db_getStatusNote( $job_number, $mediatype ); # get t3p.brief or product comments
			}

			$kanbanjobs{ $lane_name }{ $job_number } = {
					"Job No"			=> $job_number, 
					"job_number"		=> $job_number, 
					"Client"			=> $row->{client} || '-' , 
					"Campaign"			=> $row->{project} || '-', 
					"Customer"			=> $row->{agency} || '-' , 
					"Job Title"			=> $row->{HEADLINE} || '-',
					"Media Owner"		=> $row->{publication} || '-',
					"Format"			=> $row->{formatsize} || '-',
					"Media Choice ID"	=> $row->{mediachoiceid} || '-',
					"Time Length"		=> $row->{timelength} || '-',
					"lane_type"			=> $lane_type, # Numeric 1..9
					"assignee_name"		=> $row->{assignee} || "Unassigned",
					"product_assignee"	=> (@product_assignee_details),
					"media_type"		=> $mediatype,
					"traffic_date"		=> $traffic_date,
					"Country"			=> $row->{country} || '-',
					"total_prod_estimation"			=> $total_prod_estimation || '0',
					"status_note_title"	=> uri_escape_utf8($status_note),
					"Status Note"		=> $status_note,
			};
			$count++;
		}
	} # end foreach row

	return \%kanbanjobs,$count;
}

# Show kanban board

sub kanbanBoard {
	my ($query,$session) = @_;

	my $kanbanjobs	= $query->param("kanbanjobs");
	my $filtername	= $query->param("filtername");	
	my $orderbyid	= $query->param( 'orderbyid' ) || 1; # Beware this orderbyid differs from swimlane orderby_id
	my $sortby		= $query->param( 'sortby' ) || 'desc';

	my $opcoid		= $session->param( 'opcoid' );
	my $userid		= $session->param( 'userid' );

	my $lane;
	my @lanes;
	my $lane_name;
	my @lane_details;
	my $lane_id;
	my $status;
	my %lane_config;
	my $advisory_state;

	my $vars = {
		userid              => $session->param( SessionConstants::SESS_USERID ),  
		roleid              => $session->param( SessionConstants::SESS_ROLEID ),
		rolename            => $session->param( SessionConstants::SESS_ROLENAME ),
		relaxroles          => $session->param( SessionConstants::SESS_RELAXROLES ),
	};

	eval {
		die "Invalid filter" if ( ! defined $filtername ) || ( $filtername =~ /^\s*$/ );

		my ( $filtername, $filter_username, $filter_userid ) = UserFilter::parseQualifiedFilter( $filtername,$session->param( 'username' ), $session->param( 'userid' ) );

		my ( $filter_id, $filterownerid ) = ECMDBKanbanboard::db_getFilterID( $filtername, $filter_userid, $opcoid );
		die "Invalid filter '$filtername'" if ! defined $filter_id;

		my ( $lanes_list, $orderby_id, $selected_card_list) = ECMDBKanbanboard::db_getLaneColumns( $filterownerid, $opcoid, $filter_id );
		my $lane_type = ECMDBKanbanboard::getSwimLaneType( $filterownerid, $opcoid, $filter_id );

		my $opflags = ECMDBJoblist::get_operating_company_flags( $opcoid );

		my $symbol_table = Filters::getMatchConditionSymbols( $opflags, $vars->{roleid}, $vars->{rolename}, $vars->{relaxroles} );

		my $filters = Filters::get_joblist_filters( $opflags );

		my $query_params = $query->Vars;
		my $filter_values = Filters::get_filter_values( $query_params, $filters );

		my $fromId = $filter_values->{fromdate};
		if ( ! defined $fromId ) {
			$fromId = 1;
			# warn "Setting fromId to $fromId\n";
			$filter_values->{fromdate} = $fromId;
		}

		# $filter_values->{archived} = $vars->{archived};  # Set to '1' if user checks 'Show Archived' cb

        my ($card_detail_list)  =   ECMDBKanbanboard::db_getBoxValues( $filterownerid, $opcoid, $filter_id );
        $card_detail_list = '' unless defined $card_detail_list;

        my @list_id =   split("~",$card_detail_list);

        my  $header =   ECMDBKanbanboard::db_getCardListType($list_id[0]);
        my  $line1  =   ECMDBKanbanboard::db_getCardListType($list_id[1]);
        my  $line2  =   ECMDBKanbanboard::db_getCardListType($list_id[2]);
        my  $line3  =   ECMDBKanbanboard::db_getCardListType($list_id[3]);

        my  $line4_id  =   defined $list_id[4] ? $list_id[4] : 0;
		my  $line4  =   ECMDBKanbanboard::db_getCardListType($line4_id);

        my $traffic_date_key = get_traffic_light_key($line4_id);

		$vars->{subaction} = 'kanban';

		my ( $jobs, $sql ) = ECMDBJoblist::db_getFilteredJobs( $userid, $orderbyid, $sortby, $opflags, $vars, $filters, $filter_values, $opcoid, $fromId );
		die "No jobs returned for this filter" unless defined $jobs;

		my $kanbanjobslimit = Config::getConfig("kanbanjobslimit") || KANBAN_JOB_LIMIT_DEFAULT;
	
		# push( @{$vars->{history}}, "Adding kanban data, lane type $lane_type" );
		# if the filtered jobs count is greated than the kanban limit no need to generated the board.
		my $total_filtered_jobs_count	=	0;
		$total_filtered_jobs_count = scalar( @$jobs ) if defined $jobs;

		die "The board view supports up to  $kanbanjobslimit records, but this filter returned $total_filtered_jobs_count records. Please adjust your filter settings."
			if ( $total_filtered_jobs_count > $kanbanjobslimit );

		my ($kanbanjobs,$kanbanjobscount) = group_by_lane_type( $jobs, $lane_type, $vars, $traffic_date_key ); # Returns ref to hash
		#my $kanbanjobcount = keys %$kanbanjobs;
	
		if ( ( defined $lanes_list ) && ( $lanes_list ne "" ) ) {
			my $i=0;
			my @lane_list = split(",",$lanes_list);

			foreach $lane_name (@lane_list) {
				@lane_details	=	split("~",$lane_name);
				$lane_id		=	$lane_details[0];
				$status			=	$lane_details[1];
				$advisory_state		= 	defined $lane_details[2] ? $lane_details[2] : 0;

				#If the value is checked, get the name from table
				if($status == 1) {
					if ($orderby_id!=7 && $orderby_id!=8) {
						$lane = ECMDBKanbanboard::db_getLaneColumnName($lane_id,$orderby_id);
					} else {
						$lane = $lane_id;
					}
					push(@lanes,  $lane);
					$i++;

					if ($orderby_id == 5 && $line4_id != 0) {
						if ($advisory_state == 1) { $lane_config{"$lane"} = "$advisory_state"; }
					}
				}
			}
		}

		$vars = {
			lanes		=>  \@lanes,
			kanbanjobs	=> $kanbanjobs,
			header		=>	$header,
			line1		=>	$line1,
			line2		=>	$line2,
			line3		=>	$line3,
			orderby_id	=> 	$orderby_id,
			filtername	=>	$filtername,
			kanbanjobscount	=>	$kanbanjobscount,
			lane_conf	=> \%lane_config,
			opcoid		=> $opcoid,
			line4		=> $line4
		};

		#print Dumper($kanbanjobs);
    };
	if ( $@ ) {
        my $error = $@;  # make sure roleid is set, as that enables menu items if not set
		warn "kanbanBoard: $error\n";
		$error =~ s/ at .*//gs;
        $vars = { error => $error };
		#print encode_json($vars);
	}
	$tt->process( 'ecm_html/kanbanboard.html', $vars ) || die $tt->error(), "\n";
}

#update the values 
sub updateKanbanboard {
    my ($query,$session) = @_;

    my $job_number 	= $query->param("job_number");
    my $job_value	= $query->param("job_value");
    my $job_type 	= $query->param("job_type");
	my $username	= $session->param( 'username' );
	my $vars;

	eval {
		ECMDBKanbanboard::updateJobValues($job_number,$job_type,$job_value);
		if($job_type != 9) { # For assignee handled in jobs.pl AssignJobs()
			ECMDBAudit::db_auditLog( $session->param( 'userid' ), "TRACKER3PLUS", $job_number, "Job: $job_number $orderbyname{$job_type}  $job_value" );
			# Send a notification for watchlist jobs when user change the team
			Watchlist::Watchlistnotification( $session->param( 'userid' ), $session->param( 'opcoid' ), "Job(s) is assigned to team $job_value ", $job_number, "TEAMS" ) if($job_type == 2);

			# Send notification when user changes advisory
			Watchlist::Watchlistnotification( $session->param( 'userid' ), $session->param( 'opcoid' ), "Job $job_number, Advisory has been set to $job_value by $username", $job_number, "ADVISORY" ) if($job_type == 5);
		}
		$vars = { success => 1, job_type => $job_type, job_value => $job_value, job_number => $job_number };
	};
	if ( $@ ) {
        my $error = $@;  # make sure roleid is set, as that enables menu items if not set
        $vars = { error => $error };
	}
	print encode_json($vars);
}

#Show swim lane confid board
sub swimlaneConfigBoard {
	my ($query,$session) = @_;
	my $vars;
	my $filter_name			=	$query->param("filtername");
	my $opcoid      = $session->param( 'opcoid' );
	my $userid      =   $session->param( 'userid' );
	my $traffic_flag = 0;

	my @orderby_list		=	ECMDBKanbanboard::db_getOrderbyList();
	my @card_list			=	ECMDBKanbanboard::db_getCardList($traffic_flag);
	my ($filter_id,$filterownerid)  =   ECMDBKanbanboard::db_getFilterID($filter_name,$userid,$opcoid);
	my ($lanes_list,$orderby_id,$selected_card_list) = ECMDBKanbanboard::db_getLaneColumns($filterownerid,$opcoid,$filter_id);
	my @shared_users		=	ECMDBUserFilter::getSharedFilterUser($filter_name,$opcoid,$userid);

	$traffic_flag = 1;
	my @traffic_card_list   =   ECMDBKanbanboard::db_getCardList($traffic_flag);

	#print Dumper(@orderby_list);

	$vars	=	{	orderby_list	=>	\@orderby_list ,
					card_list		=>	\@card_list,
					filter_name		=>	$filter_name,
					shared_users	=> \@shared_users,
					card_detail_list=> $selected_card_list,
					orderby_id		=> $orderby_id,
					traffic_card_list => \@traffic_card_list,
				};
	$tt->process( 'ecm_html/swimlaneboard.html', $vars ) || die $tt->error(), "\n";
}
#get users list
sub getSwimLaneUsersList {
	my ($query,$session) = @_;
	my $vars;
	my @users        = ECMDBMessages::db_getUsersForLookup();
	$vars = { users_list      =>  \@users };
	print encode_json($vars);
}

#get the list of named based selected order by columns.

sub getSwimLaneOrderByValues {
	my ($query,$session) = @_;

	my $orderbyid	= $query->param("orderby_id");
	my $filtername	= $query->param("filtername");

	my $opcoid		= $session->param( 'opcoid' );
	my $userid		= $session->param( 'userid' );

    my $lane_name;
    my @lane_details;
    my $lane_id;
    my $status;
    my $lane;
    my @lanes;
	my @advisory_lanes;
	my $advisory_lane_state;
    my $vars = {};

	eval {
		my $savedfilter	=	ECMDBKanbanboard::db_getSavedFilterDate( $filtername, $orderbyid , $userid);

		my ($filter_id,$filterownerid)  =   ECMDBKanbanboard::db_getFilterID( $filtername, $userid, $opcoid );
		die "Filter name '$filtername' does not exist" if ( ! defined $filter_id );

		my ($lanes_list,$orderby_id,$selected_card_list) = ECMDBKanbanboard::db_getLaneColumns( $filterownerid, $opcoid, $filter_id );
		# die "Lane list not defined for filter '$filtername'($filter_id)" unless defined $lanes_list; -- if filter not configured, can be undef
		$lanes_list = '' unless defined $lanes_list;

        my @lane_list = split(",",$lanes_list);
        foreach $lane_name (@lane_list) {
            ( $lane_id, $status, $advisory_lane_state ) =   split("~",$lane_name);

            #If the value is checked, get the name from table
            if ( $status == 1 ) {
				$lane = $lane_id;
				push( @lanes, $lane );

				#This is applicable only for 'Advisory' swim-lanes.
				if ( $orderbyid == 5 ) {
					if (! defined $advisory_lane_state ) {
						$advisory_lane_state = '0';
					}
					push ( @advisory_lanes, $advisory_lane_state );
				}
            }
        }
		my $values = ECMDBKanbanboard::db_getSwimLaneOrderByValues($orderbyid,$opcoid,$filterownerid,$filter_id);

		$vars->{lane_values}	=	$values;			
		$vars->{selected_lane}	=	\@lanes;
		$vars->{selected_orderby_id}	=	$orderby_id;
		$vars->{savedfilterdata}	=	$savedfilter;
		#This is applicable only for 'Advisory' swim-lanes.
		if ( $orderbyid == 5 ) {
			$vars->{selected_advisory_lane}  =   \@advisory_lanes;
		}
		#print encode_json($values);
	};
    if ( $@ ) {
        my $error = $@;  # handle error
		$vars = {error => $error};
	}
	print encode_json( $vars );
}

sub updateSwimLane {
	my ($query,$session) = @_;
	my $orderby_id  =   $query->param("orderby_id");
	my $opcoid      = 	$session->param( 'opcoid' );
	my $userid      = 	$session->param( 'userid' );
	my $lane_values	= 	$query->param("lane_values");
	my $card_list_value = $query->param("card_list_value");
	my $filtername	=	$query->param("filtername");
	my $deleted_user_list = $query->param("deleted_user_list");
	my $added_user_list	=	$query->param("saved_user_list");
	my $vars;
	eval {
		my ($filter_id,$filterownerid)	=	ECMDBKanbanboard::db_getFilterID($filtername,$userid,$opcoid);
		ECMDBKanbanboard::db_updateSwimLaneBoard($filter_id,$filterownerid,$opcoid,$orderby_id,$lane_values,$card_list_value,$deleted_user_list,$added_user_list,$filtername);
		my ($added_user,$deleted_user,$sharedusers_flag,$sharedfilterid,$shared_users_list) = ECMDBKanbanboard::db_getFilterSettingsValue($added_user_list,$deleted_user_list,$filtername,$userid,$opcoid);
		$vars	=	{	added_user => $added_user,
						deleted_user	=> $deleted_user,
						sharedusers_flag	=> $sharedusers_flag,
						sharedfilterid		=> $sharedfilterid,
						shareduserid		=> $shared_users_list
					}
	};
    if ( $@ ) {
        my $error   = $@;  # handle error
        $vars       = {error => $error};
    }
		print encode_json($vars);
}
sub checkFilterOwnerAccess {
	my ($query,$session) = @_;
	my $filter_name         =   $query->param("filtername");
	my $opcoid      =   $session->param( 'opcoid' );
	my $userid      =   $session->param( 'userid' );
	my $vars;
	my $grid_status = 0;
	my $config_status = 0;
	my $shared_user_access = 0;
	my $filter_owner_acces = 0;
	my $filter_exist = 0;
	my $config_button_enabled = 0;
	eval {
		my ($filter_id,$filterownerid)   =   ECMDBKanbanboard::db_getFilterID($filter_name,$userid,$opcoid);

		if($filterownerid eq $userid)
		{
			$config_status	=	1; # If the userid and filterownerid are equal, user is the owner of that filter.
			$filter_owner_acces = 1;
			$config_button_enabled = 1;
		}
		else
		{
			($shared_user_access) = ECMDBKanbanboard::db_sharedUserAccess($filter_name,$userid,$opcoid);
			if($shared_user_access)
			{
				$config_status  =   1;
			}
		}
	
		if($config_status)
		{
			if($filter_owner_acces)
			{
				$filter_exist=	ECMDBKanbanboard::db_checkSwimlaneConfig($filter_id,$userid,$opcoid);
			}
			if($shared_user_access)
			{
				$filter_exist=  ECMDBKanbanboard::db_checkSwimlaneConfig($filter_id,$filterownerid,$opcoid);
			}
			if($filter_exist)
			{
				$grid_status	=	1; #If the grid status is one , the owner configured swim lane for this filter. 
			}
		}
		
		$vars = { grid_status => $grid_status, config_status => $config_status , shared_user_access => $shared_user_access, filter_owner_acces => $filter_owner_acces, config_button_enabled => $config_button_enabled};
		
		print encode_json($vars);
			
	};
    if ( $@ ) {
        my $error   = $@;  # handle error
        $vars       = {error => $error};
        print encode_json($vars);
    }
}
sub checkQCstatus {
	my ($query,$session) = @_;
	my $job_number	=	$query->param("job_number");
	my $roleid		=	$session->param( 'roleid' );	
	my $message		= 	"";
	my $vars;
	eval {
		my ($opcoid,$preflightstatus)		=	ECMDBKanbanboard::db_getJobOpcoAndPreflightStatus($job_number);
		my $isqcenabled		=	ECMDBKanbanboard::db_checkQCenabled($opcoid);
		if($isqcenabled	==	0)
		{
			$message = "QC Disabled";
		}
		elsif ($preflightstatus != 2 && $preflightstatus != 7) #!= Passed and != Creative
		{
			$message = "QC disabled, preflight status ".$preflightstatusname{$preflightstatus};	
		}
		elsif($roleid == 3 || $roleid == 5 || $roleid == 7 || $roleid == 8)
		{
			$message = "User don't have permission to change QC Status. Please contact administrator.";
		}	
		$vars		=	{	message => $message	}; 	
	};
    if ( $@ ) {
        my $error   = $@;  # handle error
        $vars       = {message => $error};
    }
        print encode_json($vars);

}

#############################################################################
#getBoardviewBriefComments: To get active brief only
#############################################################################

sub getBoardviewBriefComments {
	my ( $query, $session )	= @_;
	my $job_number	= $query->param( "job_number" );
 	my $vars;
	eval {
		my $boardViewBriefCommentsDet	= ECMDBKanbanboard::db_getBoardviewBriefComments( $job_number );
		$vars	= { boardViewBriefCommentsDet	=> \@$boardViewBriefCommentsDet, job_number => $job_number };
	};
	if ( $@ ) {
		my $error	= $@;
		$error	=~ s/ at (.*)//gs;
		$vars	= { error => $error };
	}
	print encode_json($vars);
}

1;
