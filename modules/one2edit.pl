#!/usr/local/bin/perl
package One2Edit;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use JSON;
use Data::Dumper;
use CGI;
use LWP::UserAgent ();
use URI::Escape;
use XML::Simple qw(:strict);
use XML::Parser;
# use utf8;
use URI::URL;
use Encode qw(decode encode);;
use File::Path;
use File::Copy;
use Net::FTP::File;
use Data::UUID;
use POSIX; # ceil
use Template;

# Home grown

# Please avoid including the older larger modules such as databaseOpcos.pl, jobs.pl etc

use lib 'modules';

use Validate;
use CTSConstants;

require "config.pl"; # for calls to Config::getConfig and $Config::dba->hashed_process_sql
require "databaseOne2Edit.pl";
require "databaseAudit.pl"; # db_recordJobAction, db_auditLog
require "jobpath.pl";
require "streamhelper.pl"; # StreamHelper::db_getFTPDetails

use constant ONE2EDIT_SESSION_USERNAME => 'one2edit_username';
use constant ONE2EDIT_SESSION_PASSWORD => 'one2edit_password';
use constant ONE2EDIT_SESSION_SAVECRED => 'one2edit_savecred';

use constant MAX_SIZE_IN_MM => 100 * 1000; # 100m in mm
use constant MM_TO_PTS => 72.0 / 25.4; # Divide mm by 25.4 to get to Inches, then x 72 points per inch

our $subactions = { # subaction dispatch table
		'getuser'	=> \&getUser,	# Allocate one2edit credential from pool
		'freeuser'	=> \&freeUser,	# Return one2edit credential back into pool
		'jobdetails'	=> \&jobDetails, # Get job details (for create Version from Master)
		'finddocpath'	=> \&findDocPath,	# Find or Create Document directory path
		'getmasters'	=> \&getMasters,	# Return list of Master documents in Document folder
		'checkversion'	=> \&checkVersion, # See if prior version exists for Job
		'deleteversion' => \&deleteVersion, # Delete version document
		'createversion' => \&createVersion, # Create version document from Master document
		'startworkflow'	=> \&startDocumentWorkflow, # Start Document workflow
		'isdocumentopen' => \&isDocumentOpen,
		'resize'	=> \&documentResize, # Resize document
		'doccommit' 	=> \&documentCommit, # Commit document workflow
		'updateversion'	=> \&updateVersion,	# Update one2edit_version and colour manage settings
		'assetpath'	=> \&getAssetPath, # To create & get a folder for the project
		'assetupload'	=> \&uploadAsset, # To upload the indesign file to one2edit
		'documentlink'	=> \&documentLink, # To get the documentId for the the uploaded asset
		'updatemaster'	=> \&updateMaster, # To insert a row in one2edit_master with job_number, folder_id, documentId, documentName
		'summary'	=> \&summaryData,
		'get_campaign_details' => \&get_campaign_details,
		'get_process_count' => \&get_process_count,
		'get_queue_list' => \&get_queue_list,
		'get_campaign_list' => \&get_campaign_list,
		'contentgroup'	=> \&getContentGroup, # To get the document content group before creating version document
		'getstyles'	=> \&getStyles,  # Read styles from one2edit_job_styles
		'extractstyles' => \&extractStyles, # Extract styles from Master document and update one2edit_job_styles
		'testparse'	=> \&testParse,
		'associate'	=> \&jobAssociate, # Associate a job with a Master document id and job
		'update_styles' => \&updateStyle, # Update styles for versioned job
		'transformstyles' => \&transformStyles, # To import the edited styles to newly created version document
		'getlanguages'	=> \&getLanguages, # Get list of languages
		'updatelanguage' => \&updateLanguage, # Update Language for job (updates table one2edit_job_language)
		'checktasks' => \&check_task_found, #To find whether the job has any tasks present in it that matches the said pattern,
		'getjobworkflow' => \&getJobWorkflow, #To get job workflow details
	};

our $config_done = 0;
our $client_id = 0;

# Set of settings with default values
# The values are updated using the value in the jobflow_settings when getConfig is called.

our $settings = {
		url => 'https://estudio.tagworldwide.com/Api.php',
		clientid => 35,
		asset_folder => 'Automation_Dev',
		project_name => 'estudio Project',
		project_id => 21,
		timeout => 30,
		workflow_folder => 'Jobflow',
		workflow => 'Jobflow_Master',
		workflow_id => 13,
		username => '?',
		password => 'dev',
		content_group => 'Data',
		};

our $ua;

##################################################################
# the template object
##################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";

# This is the action handler for action one2edit
# a) action_handler is called from jobflow.pl for a specific action
#	(so has corresponding entry in modules/legacy for the action too).
# b) It has 2 sub actions, init and details (both of which return a hash ref which this converts to JSON)
# c) It will return the following JSON fields
#	- history (array of string, whatever the callers use it for)
#	 - elapsedtime, time taken for call in seconds
#	- error, exception text if error trapped

sub action_handler {
	my ( $query, $session ) = @_; # args passed by jobflow.pl

	my $subaction 	= $query->param( 'subaction' ) || 'undef';

	my $start_time = time();
	my @history;
	my $vars = { history => \@history }; # ref to hash with just history array

	eval {
		my $userid = $session->param( 'userid' );
		die "Permission denied, temp user" if ( $userid == 2 ); # Don't allow temporary users access
		my $rolename = $session->param( 'rolename' );
		die "Permission denied, role $rolename" unless ( $rolename =~ /(Account Manager|Administrator|Studio Manager)/ ); # Broad check

		my $func = $subactions->{ $subaction };
		die __PACKAGE__ . ": invalid subaction '$subaction'" unless defined $func;

		getConfig();
		$func->( $query, $session, $vars );
	};
	if ( $@ ) {
		my $error = $@;
		#$error =~ s/ at .*//gs; # Strip the traceback if needed
		$vars->{error} = __PACKAGE__ . "::$subaction error, $error";
	}
	$vars->{elapsedtime} = time() - $start_time;

	# Tidy up allocated user if needed, eg subaction called getUser but threw an error or forgot to call freeUser

	my $o2e_username = $session->param( ONE2EDIT_SESSION_USERNAME ) || '';
	my $savecred = $session->param( ONE2EDIT_SESSION_SAVECRED ) || 0;
	if ( ( $o2e_username ne '' ) && ( $savecred == 0 ) ) {
		eval {
			push( @{$vars->{history}}, "action_handler: username $o2e_username, savecred $savecred" );
			freeUser( $query, $session, $vars );
		};
		if ( $@ ) {
			my $error = $@;
			push( @{$vars->{history}}, "getUser: error freeing username $o2e_username" );
			$vars->{error} = $error if ( ! exists $vars->{error} ); # Only set error if not already set
		}
	}
	# In a few cases, eg summaryData, we set the query param mode, so defer getting the value until here
	my $mode = $query->param( 'mode' ) || 'json';
	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
	die $vars->{error} if ( exists $vars->{error} ); # Rely on exception handling in jobflow.pl for non JSON handling
}

# Local function to populate settings hash with values from jobflow_settings

sub getConfig {
	if ( $config_done == 0 ) {
		foreach my $k ( keys %$settings ) {
			next if ( $k =~ /(username|password)/ );
			my $v = Config::getConfig("one2edit_$k");
			$settings->{ $k } = $v if ( defined $v );
		}
	}
	$config_done = 1;
}

# Create instance of UserAgent

sub get_ua {
	if ( ! defined $ua ) {
		$ua = LWP::UserAgent->new;
		$ua->timeout( $settings->{timeout} );
		#$ua->default_header( "Content-Type" =>  "application/x-www-form-urlencoded; charset=UTF-8" );
	}
	return $ua;
}

sub free_ua {

}

# Wrapper for UserAgent::post
# Args: ( url_hash, other args )
#  -- url_hash is hash containing the command and options
#  -- other args is anything else that needs to be passed to post.
 
sub do_post {
	my $content = shift; # 1st arg is hash which gets translated to Content => { hash } for the post

	die "do_post: No argument specified" unless ( defined $content ) && ( ref( $content ) eq 'HASH' );
	die "do_post: No command specified" unless exists $content->{command};

	getConfig(); # Make sure $settings hash is initialised

	#print "do_post remaining " . Dumper( \@_ ) . "\n" if scalar @_;
	unshift( @_, Content => $content ); # Add back hash into 

	my $ua = get_ua();
	my $url = $settings->{ url };

	# TODO, still an issue how we get the users session allocated credentials to use here
	# Could just permanently allocate a specific username for use by Jobflow

	$url .= '?authDomain=local&authUsername=' . $settings->{username} . '&authPassword=' . 
				$settings->{password} . '&clientId=' . $settings->{clientid};

	# print "Using url $url\n";

	#my $response = $ua->post( $url, Content => $content );
	my $response = $ua->post( $url, @_ );

	die $response->status_line unless $response->is_success;
	#print $response->decoded_content;  # or whatever

	# Use XML::Simple for most commands, but save actual XML text (as we may use XML::Parser in some cases)
	my $obj = XMLin( $response->decoded_content, ForceArray => 0, KeyAttr => [], KeepRoot => 1 );
	if ( exists $obj->{error} ) {
		$obj = XMLin( $response->decoded_content, ForceArray => 0, KeyAttr => [], KeepRoot => 1 );
	}
	die "Unexpected response " . Dumper( $obj ) . "\n" unless ( exists $obj->{error} ) || ( exists $obj->{success} );
	$obj->{xml} = $response->decoded_content;

	# Throw an exception if we dont see success as the top level element
	if ( ( exists $obj->{error} ) && ( ! exists $content->{ignoreerror} ) ) {
		my $error = $obj->{error};
		die $content->{command} . ' Error: ' . $error->{code} . " >> " . $error->{message} . "\n";
	}
	return $obj;
}

# Due to the way that we convert the XML to an object, the way that the relationship between folders and folder elements
# needs to be handled consistently, ie a folder can have 0 or more child folders
# a) No child folder		<folders></folders>							folders => {}
# b) One child folder		<folders><folder>...</folder></folders>		folders => { folder => { ... } };
# c) Two or more														folders => { folder => [ { ... } ] };
#
# So return an array ref in all cases.

sub to_array {
	my ( $struct ) = @_;
	#warn "to_array: ref struct is " . ref( $struct ) . "\n";
	return [] unless defined $struct;
	return $struct if ( ref( $struct ) eq 'ARRAY' );
	return [ $struct ];
}

# Subaction handles, one per subaction, each having args ( $query, $session, $vars )
# and returning the results in $vars (action_handler converts this to JSON).

# getUser, checks whether session already has a username and if not tries to allocate the next free one.
# Just leaves the credentials in the session object.
# A call is deemed internal/external by looking at the subaction.
# If a call is made with subaction 'getuser' it is external in which case we set the savecred flag
# If made with another subaction it is internal and we leave the savecred flag as is
# The difference is when we process the freeuser call, if it is an internal call and the savecred flag is
# set, we leave the credentials alone.
# Inputs: $query.subaction, $session.username
# 

sub getUser {
	my ( $query, $session, $vars ) = @_;

	my $o2e_username = $session->param( ONE2EDIT_SESSION_USERNAME );
	my $o2e_password = $session->param( ONE2EDIT_SESSION_PASSWORD );
	my $userid = $session->param('userid');

	my $id; # row in one2edit_idpool

	if ( defined $o2e_username ) { # Username already in session (assume savecred setting set one way or the other with it)
		$settings->{username} = $o2e_username;
		$settings->{password} = $o2e_password;
		push( @{$vars->{history}}, "getUser: existing username $o2e_username" ); 
		return;
	}

	my $subaction = $query->param( 'subaction' ) || 'undef'; # Need to discriminate between internal/external calls

	eval {

		$Config::dba->process_oneline_sql( "UPDATE one2edit_idpool SET expiry_dt = NULL, userid = NULL, sessionid = NULL WHERE expiry_dt < now()" );

		# Find first free credential in table one2edit_idpool
		$Config::dba->process_oneline_sql( "START TRANSACTION" ); # temp disable autocommit

		# Check to see if this userid + sessionid has an allocated row.
		( $id, $o2e_username, $o2e_password ) = $Config::dba->process_oneline_sql(
			"SELECT id, username, password FROM one2edit_idpool WHERE userid = ? AND sessionid = ? LIMIT 1 FOR UPDATE",
			[ $userid, $session->id ] );

		if ( defined $id ) {
			push( @{$vars->{history}}, "getUser: reusing username $o2e_username, id $id" );

			$Config::dba->process_oneline_sql( "UPDATE one2edit_idpool SET expiry_dt = DATE_ADD( now(), INTERVAL 10 MINUTE ) WHERE id = ?", [ $id ] );
		} else {

			( $id, $o2e_username, $o2e_password ) = $Config::dba->process_oneline_sql(
				"SELECT id, username, password FROM one2edit_idpool WHERE expiry_dt IS NULL LIMIT 1 FOR UPDATE" );

			die "Failed to allocate credentials, please try after few minutes. " if ( ! defined $id );

			$Config::dba->process_oneline_sql(
				"UPDATE one2edit_idpool SET expiry_dt = DATE_ADD( now(), INTERVAL 10 MINUTE ), userid = ?, sessionid = ? WHERE id = ?",
				[ $userid, $session->id, $id ] );
			push( @{$vars->{history}}, "getUser: allocated username $o2e_username" );
		}

		$Config::dba->process_oneline_sql( "COMMIT" ); # Must do COMMIT or ROLLBACK after START TRANSACTION

		$session->param( ONE2EDIT_SESSION_USERNAME, $o2e_username );
		$session->param( ONE2EDIT_SESSION_PASSWORD, $o2e_password );

		$settings->{username} = $o2e_username;
		$settings->{password} = $o2e_password;

		$session->param( ONE2EDIT_SESSION_SAVECRED, 1 ) if ( $subaction eq 'getuser' ); # Set save flag if external call
	};
	if ( $@ ) {
		my $error = $@;
		$Config::dba->process_oneline_sql( "ROLLBACK" );
		die "getUser: Error: $error";
	}
}

# freeUser, free One2Edit user credentials
# The One2Edit username is set in the session (but can be overridden by the query param for test purposes)
# If getUser is called by the UI, then session.savecred is set, so correspondingly if freeUser
# is called and session.savecred is set, we only free the user if called by the UI
 
sub freeUser {
	my ( $query, $session, $vars ) = @_;

	my $o2e_username = $query->param( 'username' ) || ''; # Really for test purpose, the session username should be used normally
	if ( $o2e_username eq '' ) {
		$o2e_username = $session->param( ONE2EDIT_SESSION_USERNAME ) || '';
	}
	my $subaction = $query->param( 'subaction' ) || '';

	# If the session savecred flag is set, it means that the credentials where allocated by the getUser subaction
	# so we dont free the credentials until we see the freeuser subaction.

	my $savecred = $session->param( ONE2EDIT_SESSION_SAVECRED ) || 0;
	return if ( $savecred ) && ( $subaction ne 'freeuser' );

	eval {
		if ( defined $o2e_username ) {
			push( @{$vars->{history}}, "freeUser: user '$o2e_username'" );
			$Config::dba->process_oneline_sql(
				"UPDATE one2edit_idpool SET expiry_dt = NULL, userid = NULL, sessionid = NULL WHERE username = ?",
				[ $o2e_username ] );
		} else {
			push( @{$vars->{history}}, "freeUser: sessionid '" . $session->id . "'" );
			$Config::dba->process_oneline_sql(
				"UPDATE one2edit_idpool SET expiry_dt = NULL, userid = NULL, sessionid = NULL WHERE sessionid = ?",
				[ $session->id ] );
		}
		$session->clear( ONE2EDIT_SESSION_USERNAME );
		$session->clear( ONE2EDIT_SESSION_PASSWORD );
		$session->clear( ONE2EDIT_SESSION_SAVECRED );
	};
	if ( $@ ) {
		die "freeUser: Error: $@";
	}
}

# getCampaignDetails, helper function used by jobDetails, getAssetPath, updateMaster

sub getCampaignDetails {
	my ( $job_number, $vars ) = @_;

	eval {
		Validate::job_number( $job_number );

		# Get customer, client, campaign
		my ( $customer, $client, $campaign ) = $Config::dba->process_oneline_sql(
			"SELECT t3.agency, t3.client, t3.project FROM tracker3 t3 WHERE t3.job_number = ?",
			[ $job_number ] );
		die "Invalid job number $job_number" unless defined $customer;

		$vars->{customer} = $customer;
		$vars->{client} = $client;
		$vars->{campaign} = $campaign;
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		die 'getCampaignDetails: ' . $error;
	}
}

# jobDetails, get job details for Create Version from Master
# Inputs
# - job_number
# Returns
# customer, client, campaign
# folder_id, document_id, document_name (if row exists on one2edit_master)
# master_doc_id, version_doc_id (if row exists on one2edit_version)
# colourmanage, colourmanage_isactive (initial colour manage settings)

sub jobDetails {
	my ( $query, $session, $vars ) = @_;

	my $job_number 	= $query->param( 'job_number' );
	
	eval {
		Validate::job_number( $job_number );

		push( @{$vars->{history}}, "In jobDetails function, job_num: $job_number" );

		# Get customer, client, campaign
		getCampaignDetails( $job_number, $vars ); # sets customer, client, campaign in vars 

		# Check one2edit option is enabled or not for this customer

		my ( $customerId ) = $Config::dba->process_oneline_sql("SELECT customerid FROM TRACKER3PLUS WHERE job_number=?", [ $job_number ]);
		my ( $isenable_one2edit ) = $Config::dba->process_oneline_sql( "SELECT enable_one2edit FROM customers WHERE id = ?", [ $customerId ] );

		die "Customer '$vars->{customer} ($customerId)' is not one2edit enabled." if ( $isenable_one2edit == 0 );
 
		# Query one2edit_master and one2edit_version
		my ( $folder_id, $document_id, $document_name ) = $Config::dba->process_oneline_sql(
			"SELECT folder_id, document_id, document_name FROM one2edit_master WHERE job_number = ?",
			[ $job_number ] );

		$vars->{folder_id} = $folder_id;
		$vars->{document_id} = $document_id;
		$vars->{document_name} = $document_name;

 		my ( $master_doc_id, $version_doc_id ) = $Config::dba->process_oneline_sql(
			"SELECT master_doc_id, version_doc_id FROM one2edit_version WHERE job_number = ?",
			[ $job_number ] );

		$vars->{master_doc_id} = $master_doc_id;
		$vars->{version_doc_id} = $version_doc_id;

		# Check Job is associated or not
		my ( $associate_master_doc_id, $associate_master_job_no, $associate_master_doc_name ) = $Config::dba->process_oneline_sql(
				"SELECT master_doc_id, master_job_no, master_doc_name FROM one2edit_associate WHERE job_number = ?",
					[ $job_number ] );
		$vars->{associate_master_job_no} = $associate_master_job_no;
		$vars->{associate_master_doc_id} = $associate_master_doc_id;
		$vars->{associate_master_document_name} = $associate_master_doc_name;

		# Get colour manager settings
 		my ( $colourmanage, $colourmanage_isactive ) = $Config::dba->process_oneline_sql(
			"SELECT t3p.colourmanage, t3p.colourmanage_isactive FROM tracker3plus t3p WHERE t3p.job_number = ?",
			[ $job_number ] );

		$vars->{colourmanage} = $colourmanage;
		$vars->{colourmanage_isactive} = $colourmanage_isactive;

		getConfig(); # Initialise values in $settings->{ ... };
		$vars->{settings} = $settings;

		# Get the job language details
		$vars->{languages} = getJobLanguage( $job_number, $vars );
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = 'jobDetails: ' . $error;
	}
}

# updateVersion, called on completion of the steps used to create a Version
# The table one2edit_version is updated in both deleteVersion and createVersion, so there is no need to do so here.
#
# However, we need to update the colour manage settings on tracker3plus
# Inputs
# - job_number
# - colour_manage, 1 if checked 0 otherwise
#
# Code lifted from upload.pl, sub handle_creative_colourmanage_optimise
#
sub updateVersion {
	my ( $query, $session, $vars ) = @_;

	my $job_number = $query->param( 'job_number' );
	my $colourmanage_isactive = $query->param( 'colour_manage' );
	my $userid = $session->param( 'userid' );

	my @changes;

	eval {
		Validate::job_number( $job_number );
		die "Param colour_manage missing" unless defined $colourmanage_isactive;

		# Get the existing colour manage setting from tracker3plus

		my ( $current_colourmanage, $current_colourmanage_isactive, $current_lastuser ) = $Config::dba->process_oneline_sql(
			"SELECT colourmanage, colourmanage_isactive, lastuser FROM tracker3plus WHERE job_number = ?", [ $job_number ] );
		die "Failed reading current colour settings" unless ( defined $current_colourmanage ); # Fields are NOT NULL

		push( @{$vars->{history}}, "Colour Manager current settings: enabled: $current_colourmanage, checked: $current_colourmanage_isactive" );

		if ( ( $current_colourmanage == 1 ) && ( $current_colourmanage_isactive != $colourmanage_isactive ) ) {
			push( @{$vars->{history}}, "Setting Colour Manager to $colourmanage_isactive" ); 
			$Config::dba->process_oneline_sql(
				"UPDATE tracker3plus SET colourmanage_isactive = ? WHERE job_number = ?", [ $colourmanage_isactive, $job_number ] );

			ECMDBAudit::db_recordJobAction( $job_number, $userid, CTSConstants::COLOUR_MANAGE );
			if ( $colourmanage_isactive == 1 ) {
				push( @changes, "Colour Manager = 'ON'" );
			} else {
				push( @changes, "Colour Manager = 'OFF'" );
			}
		}
		if ( ( ! defined $current_lastuser ) || ( $current_lastuser != $userid ) ) {
			push( @changes, "Last User = " . $session->param( 'username' ) . "($userid)" );
			$Config::dba->process_oneline_sql(
				"UPDATE tracker3plus SET lastuser = ? WHERE job_number = ?", [ $userid, $job_number ] );
		}

		# Record usage of One2Edit in CTS (done ealier in getJobDetails)
		ECMDBAudit::db_recordJobAction( $job_number, $userid, CTSConstants::ONE2EDIT_VERSION );

		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $job_number, "Job was saved with " . join( ', ', @changes ) ) if ( scalar @changes > 0 );
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = 'updateVersion: ' . $error;
		ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_VERSION", $job_number, "Error updating Version: $error" );
	}
}

#
# From hereon, functions are grouped according to the order of the commands used
# see https://support.one2edit.com/Api
#

# -- Asset --

# findAssetProject,converts an asset projet name eg 'estudio Project' into a project id that can be used in asset.upload
# NOT currently used, we just use a jobflow setting for this
 
sub findAssetProject {
	my ( $asset_project_name ) = @_;

	my $asset_project_id;
	eval {

		my $obj = do_post( { command => 'asset.list' } );
		# die "Failed to get asset.list " . $obj->{error}->{message} if ( exists $obj->{error} );
		# $obj->{success}->{assets} is a hash (not sure how this works if the user has access to more than one project)
		my $assets = to_array( $obj->{success}->{assets}->{asset} );
		foreach my $a ( @$assets ) {
			if ( $a->{name} eq $asset_project_name ) {
				$asset_project_id = $a->{project};
				last;
			}
		}
		die "Could not find asset project $asset_project_name\n" unless ( defined $asset_project_id );
		#push( @{$vars->{history}}, "Found asset project $asset_project_name, id $asset_project_id" );
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		die 'findAssetProject: ' . $error;
	}
	return $asset_project_id;
}

# getAssetPath, create the asset-path in One2Edit (move up ...)
# The top level directory is determined by jobflow_setting one2edit_asset_folder, typically Automation_Dev, Automation_staging or just Automation
# Under that the directories are named according to <Customer>, <Client>, <Campaign>
# Selva creating this from one2edit_asset.pl

sub getAssetPath {
	my ( $query, $session, $vars ) = @_;

	my $job_number 	= $query->param( 'job_number' );
	
	eval {
		Validate::job_number( $job_number );

		# Get customer, client, campaign
		getCampaignDetails( $job_number, $vars ); # sets customer, client, campaign in vars 
		my $job_path = $vars->{customer} . "/" . $vars->{client} . "/" . $vars->{campaign};

		#job_number:: 105000030 - "assetpath":"/Automation_Dev/AA_PRODUCTION_COMPANY/AA_PRODUCTION_CLIENT/500_JOBS",

		# Create asset folder path
		my $asset_project_id = $vars->{settings}->{project_id};

		getUser( $query, $session, $vars );

		$job_path = $vars->{settings}->{asset_folder} . '/' . $job_path;

		my $path;
		my $root_path = "/";

		foreach my $folder_name ( split /\//, $job_path ) {

			if (!defined $path) {
				$path = '/' . $folder_name;
			} else {
				$path = $path . '/' . $folder_name;
			}

			my $obj = do_post( { command => 'asset.list', projectId => $asset_project_id, folderIdentifier => $path, ignoreerror => '1' } );
			if ( exists $obj->{error} ) {
				my $error = $obj->{error};
				die "Failed to get asset.list, folder $folder_name, $error->{code}, $error->{message}\n" unless $error->{message} eq 'No such file or directory';
				# Otherwise create the asset folder
				$obj = do_post( { command => 'asset.create', projectId => $asset_project_id, folderIdentifier => $root_path, name => $folder_name } );
				if ( exists $obj->{error} ) {
					my $error = $obj->{error};
					die "Failed to create asset folder, " . $error->{message} . ' code ' . $error->{code} . "\n";
				}
			}

			$root_path = $root_path . '/' . $folder_name;
		}

		$vars->{assetpath} = $path;
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = $error;
	}
}

# uploadAsset, upload Document asset to One2Edit - see one2edit_resize3.pl, upload_asset
#
# The asset to be uploaded exists in the Documents storage area
# a) get opcoid of job (use Jobpath::... )
# b) create temp directory (which needs to be deleted at the end, even on error)
# c) FTP the asset to the temp directory
# d) use do_post to upload the file
# e) log audit event
# f) clean up FTP connection and delete temp directory
#
sub uploadAsset {
	my ( $query, $session, $vars ) = @_;

	my $job_number = $query->param( 'job_number' );
	my $folder_id = $query->param( 'folder_id');
	my $folder_path = $query->param( 'folder_path' );
	my $file = $query->param( 'file' );

	my $userid = $session->param( 'userid' );

	my $ftp;
	my $temp_path;	# temp directory used for all One2Edit asset upload
	my $path;		# will be temp_path qualified by UUID

	eval {
		die "Missing folder_path" unless defined $folder_path;
		die "Missing file" unless defined $file;
		Validate::job_number( $job_number );

		getConfig(); # Initialise settings
		my $asset_project_id = $settings->{project_id};

		my $documentAssetsFolder	= Config::getConfig("documentAssetsFolder" );
		$temp_path = Config::getConfig('one2edit_asset_temp') || '/temp/o2e_asset';

		# Create the UUID string
		my $ug = new Data::UUID; 
		my $uuid	= $ug->create();
		my $uuidstr = $ug->to_string( $uuid );

		$path = $temp_path . '/' . $uuidstr . '/';

		# Remove the file and path if already exist and recreate
		system( "rm -rf $path" ) if (  -e "$path" );
		mkpath( "$path", { mode => 0777, verbose => 0 } );
		chmod( 0777, "$path" );

		chdir($path);

		my ( $jobPath, $jobopcoid, $customerid, $location ) = JobPath::getJobPath( $job_number );
		$jobPath = $jobPath."/".$documentAssetsFolder;

		my ( $ftpServer, $ftpUserName, $ftpPassword ) = StreamHelper::db_getFTPDetails( $jobopcoid );
		die "Invalid FTP Server for opcoid $jobopcoid" unless ( $ftpServer );

		$ftp = Net::FTP->new( $ftpServer, Debug => 0 ) or die "Cannot connect to FTP server $@";
		if ( $ftp ) {
			$ftp->login( $ftpUserName, $ftpPassword ) or die "Cannot login", $ftp->message;
			$ftp->cwd( "$jobPath" );
			$ftp->binary();
			$ftp->get( "$file" ) or die "ftp get failed ", $ftp->message;
			$ftp->quit(); # Close the FTP connection
			$ftp = undef; # Clear so that the error handling doesnt try to close it twice
		}

		chdir($path);

		getUser( $query, $session, $vars );

		my $obj = do_post( { command => 'asset.upload', projectId => $asset_project_id, folderIdentifier => $folder_path, 
				name => $file, overwrite => 'true', data => [ $path."/".$file ] },  #$file_path
				Content_Type => 'multipart/form-data', 
		);

		my $uploaded_asset = $obj->{success}->{asset};

		$vars->{asset_details} = $uploaded_asset;
		#print "Uploaded asset " . Dumper( $uploaded_asset ) . "\n";
		ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_MASTER", $job_number, "Successfully Uploaded the Asset : $folder_path/$file" );
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = 'uploadAsset: ' . $error;

		$ftp->quit() if ( defined $ftp );

		ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_MASTER", $job_number, "Error uploading Asset for job: $error" ) if ( defined $job_number );
	}

	if ( ( defined $path ) && ( $path ne '' )  && ( -e "$path" ) ) {
		system( "rm -rf $path" );
	}
}

sub updateMaster {
	my ( $query, $session, $vars ) = @_;

	my $job_number = $query->param( 'job_number' );
	my $folder_id = $query->param( 'folder_id' );
	my $document_id = $query->param( 'document_id' );
	my $document_name = $query->param( 'document_name' );

	my $userid = $session->param( 'userid' );

	eval {
		Validate::job_number( $job_number );

		die "Param folder_id missing" unless defined $folder_id;
		die "Param document_id missing" unless defined $document_id;
		die "Param document_name missing" unless defined $document_name;

		# Get customer, client, campaign
		getCampaignDetails( $job_number, $vars ); # sets customer, client, campaign in vars 
		my $campaign_path = $vars->{customer} . "/" . $vars->{client} . "/" . $vars->{campaign};
	
		$Config::dba->process_oneline_sql(
			"INSERT INTO one2edit_master ( job_number, folder_id, document_id, document_name, campaign_path ) VALUES ( ?, ?, ?, ?, ?)",
			[ $job_number, $folder_id, $document_id, $document_name, $campaign_path ] );

		ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_MASTER", $job_number, "Created Master doc id $document_id, name $document_name, folder id $folder_id" );
		ECMDBAudit::db_recordJobAction( $job_number, $userid, CTSConstants::ONE2EDIT_MASTER );
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = 'updateMaster: ' . $error;
		ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_MASTER", $job_number, "Error inserting row in one2edit_master: $error" );
	}
}

# -- Client, Content, expand as needed
#

# -- Document --
#

# Handler for subaction finddocpath, return the folder id if the path found/created, otherwise returns an error
# Query params
# - doc_path, string, directory path
# - create,  will create sub directories if they dont exist.
# Returns folder_id
#
sub findDocPath {
	my ( $query, $session, $vars ) = @_;

	my $doc_path = $query->param( 'doc_path' ); # String with / directory delimiters
	my $create = $query->param( 'create' ) || 0; # Create missing sub directories if needed

	my $folder_id;
	my $found_path_array_ref;
	my @found_path;
	my @found_name;
	my $obj;
	my $verb = 'find';
	$verb = 'create' if $create;
	
	eval {
		die "Missing doc_path param" unless ( defined $doc_path );
		$doc_path =~ s/^\///g; # Remove leading /
		$doc_path =~ s/\/$//g; # Remove trailing /
		die "Blank doc_path param" unless ( $doc_path ne '' ); # Have to have at least one directory
	
		my @path = split /\//, $doc_path;

		getUser( $query, $session, $vars );

		foreach my $folder_name ( @path ) {
			if ( defined $folder_id ) {
				push( @{$vars->{history}}, "Looking for folder '$folder_name', in folder id $folder_id" );
				$obj = do_post( { command => 'document.folder.list', depth => 1, id => $folder_id, include => 'name,id,folders' } );
			} else {
				push( @{$vars->{history}}, "Looking for top level folder '$folder_name'" );
				$obj = do_post( { command => 'document.folder.list', depth => 1, include => 'name,id,folders' } );
			}
			my $folders = to_array( $obj->{success}->{folders}->{folder} );
			my $found = 0;
			foreach my $f ( @$folders ) {
				if ( $f->{name} eq $folder_name ) {
					$found = 1;
					$folder_id = $f->{id};
					push( @found_path, { name => $f->{name}, id => $f->{id} } );
					push( @found_name, $f->{name} );
					last;
				}
			}
			if ( ! $found ) {
				if ( ! $create ) {
					if ( defined $folder_id ) {
						die "Could not find folder $folder_name, in folder " . join( '/', @found_name ) . " ($folder_id)";
					} else {
						die "Could not find top level folder $folder_name";
					}
				}
				# Try to create the folder
				if ( defined $folder_id ) {
					push( @{$vars->{history}}, "Create folder $folder_name in folder $folder_id" );
					$obj = do_post( { command => 'document.folder.add', name => $folder_name, id => $folder_id } );
				} else {
					push( @{$vars->{history}}, "Create folder $folder_name at top level" );
					$obj = do_post( { command => 'document.folder.add', name => $folder_name } );
				}
				my $folder = $obj->{success}->{folder};
				$folder_id = $folder->{id};
				push( @{$vars->{history}}, "New folder id $folder_id" );
				push( @found_path, { name => $folder_name, id => $folder_id } );
				push( @found_name, $folder_name );
			}
		}

		$vars->{folder_id} = $folder_id;
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = 'findDocPath: ' . $error;
	}
}

# getMasters - Return list of Master documents in Document folder
# Inputs 
# - folder_id
# Returns
#
sub getMasters {
	my ( $query, $session, $vars ) = @_;

	my $folder_id = $query->param('folder_id') || $query->param('folderId');

	my $userid = $session->param( 'userid' );

	my @files; # Returns array of hash, one element per file

	eval {
		die "Invalid folder_id" unless ( defined $folder_id );

		getUser( $query, $session, $vars ); # Sets $vars->{username} and {password}

		my $obj = do_post( { command => 'document.list', folderId => $folder_id,
				 include => 'id,name,tags,description,version,revision,owner,isVersion' } );

		# Returns <documents><document>*</documents>
		# document contains sub elements id, name, tags, owner, description, version, revision
		# 		
		my $documents = to_array( $obj->{success}->{documents}->{document} );
		my @documentIds = ();
		foreach my $d ( @$documents ) {
			next if ( $d->{isVersion} eq 'true' );
			my $f = { id => $d->{id}, tags => [], owner => {}, };
			push ( @documentIds, $d->{id} );
			foreach my $k ( keys %{$d} ) {
				if ( $k eq 'tags' ) {
					# <tags> if not empty has child element <tag>(0..n) and each <tag> element has <id> and <name>
					my $tags = to_array( $d->{ $k }->{tag} );
					if ( ( defined $tags ) && ( ref $tags eq 'ARRAY' ) ) {
						foreach my $t ( @$tags ) {
							push( @{$f->{tags}}, { id => $t->{id}, name => $t->{name} } );
						}
					}
				} elsif ( $k eq 'owner' ) {
					# <owner> element has children id, name, contact, role, status, lock, online, onlineTime, domain, identifier
					# but we only need name and contact
					my $owner = $d->{owner};
					$f->{owner} = { name => $owner->{name}, id => $owner->{id} };
				} else {
					$f->{ $k } = '';
					$f->{ $k } = $d->{ $k } unless ( ref $d->{ $k } eq 'HASH' );
				}
			}
			push( @files, $f );
		}

		my $docIds = "'".join("','",@documentIds)."'";
		my $rows = $Config::dba->hashed_process_sql( "SELECT document_id, job_number FROM one2edit_master WHERE document_id IN ( $docIds )" );

		my %master_jobs;
		if ( defined $rows ) {
			foreach my $res ( @$rows ) {
				$master_jobs{$res->{document_id}} = $res->{job_number};
			}
		}

		foreach my $f ( @files ) {
			if ( defined $master_jobs{$f->{id}} ) {
				$f->{master_job_number} = $master_jobs{$f->{id}};
			}
		}
		$vars->{files} = \@files;
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = 'getMasters: ' . $error;
	}
}

# Handler for subaction checkversion, check to see if a Version exists for this job number.
# If we have previously created a Version for this job, we record it in one2edit_version
# but that document could have been deleted in One2Edit and another document created

sub checkVersion {
	my ( $query, $session, $vars ) = @_;

	my $job_number = $query->param('job_number');

	eval {
		Validate::job_number( $job_number );

		# Check whether job has prior Version
 		my ( $master_doc_id, $version_doc_id, $version_folder_id ) = $Config::dba->process_oneline_sql(
			"SELECT master_doc_id, version_doc_id, folder_id FROM one2edit_version WHERE job_number = ?",
			[ $job_number ] );

		my $version_doc_id_exists = 0; # Set to 1 if it still exists

		if ( defined $version_doc_id ) {

			getUser( $query, $session, $vars );

			# See if the last recorded Version exists
			my $obj = do_post( { command => 'document.info', id => $version_doc_id, depth => 0, include => 'name,id,folders' } );
			$version_doc_id_exists =  1 if ( exists $obj->{success} );
		}

		$vars->{doc_exists} = $version_doc_id_exists;

		# TODO See if a Version exists with the name <job number>.indd

	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = 'checkVersion: ' . $error;
	}
}

# Handler for subaction deleteversion, Delete the Version document for the job

sub deleteVersion {
	my ( $query, $session, $vars ) = @_;

	my $job_number = $query->param('job_number');
	my $userid = $session->param( 'userid' );

	eval {
		Validate::job_number( $job_number );

		# Check whether job has prior Version
		my ( $master_doc_id, $version_doc_id, $version_folder_id ) = $Config::dba->process_oneline_sql(
			"SELECT master_doc_id, version_doc_id, folder_id FROM one2edit_version WHERE job_number = ?",
			[ $job_number ] );

		if ( defined $version_doc_id ) {
			# See if the last recorded Version exists
			eval {
				getUser( $query, $session, $vars );

				my $obj = do_post( { command => 'document.info', id => $version_doc_id, depth => 0, include => 'name,id,folders' } );
				push( @{$vars->{history}}, "Deleting Version $version_doc_id" );
				$obj = do_post( { command => 'document.delete', id => $version_doc_id } );
				push( @{$vars->{history}}, Dumper( $obj->{success} ) );
				$Config::dba->process_oneline_sql( "DELETE FROM one2edit_version WHERE version_doc_id = ?", [ $version_doc_id ] );

				ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_VERSION", $job_number, "Deleted previous Version $version_doc_id" );
			};
			if ( $@ ) {
				push( @{$vars->{history}}, $@ ); 
			}
		}
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = 'deleteVersion: ' . $error;
		ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_VERSION", $job_number, "Error deleting Version for job: $error" ) if ( defined $job_number );
	}
}

# Create Version of existing Master document

sub createVersion {
	my ( $query, $session, $vars ) = @_;

	my $job_number = $query->param('job_number');
	my $master_doc_id = $query->param('master_doc_id');
	my $folder_id = $query->param('folder_id');
	my $version_doc_name = $query->param('version_doc_name');
	my $userid = $session->param( 'userid' );

	my $version_name;
	my $version_id;

	eval {
		Validate::job_number( $job_number );

		die "Missing master_doc_id param" unless defined $master_doc_id;
		die "Missing folder_id" unless defined $folder_id;

		getUser( $query, $session, $vars );

		$version_name = $job_number . '.indd';

		my $obj = do_post( { command => 'document.copy', mode => 'VERSION',
					id => $master_doc_id,
					folderId => $folder_id,
					documentVersion => $job_number, 
					documentName => $version_name } );
		# If ok, we get <success><document><...>
		# and <document> has child elements id, type, parentId, folderId (hmm 0 if not supplied), name, tags, description, version...

		$version_id = $obj->{success}->{document}->{id};

		# Check whether job has prior Version
		my ( $prior_version_doc_id ) = $Config::dba->process_oneline_sql(
			"SELECT version_doc_id, folder_id FROM one2edit_version WHERE job_number = ?", [ $job_number ] );

		if ( defined $prior_version_doc_id ) { # Previous document instance
			# See if the last recorded Version exists
			$Config::dba->process_oneline_sql( "DELETE FROM one2edit_version WHERE job_number = ?", [ $job_number ] );
		}
		$Config::dba->process_oneline_sql( 
			"INSERT INTO one2edit_version ( job_number, master_doc_id, version_doc_id, folder_id ) VALUES ( ?, ?, ?, ? )",
			[ $job_number, $master_doc_id, $version_id, $folder_id ] );
 
		$vars->{version_id} = $version_id;
		$vars->{version_name} = $version_name;

		ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_VERSION", $job_number, "Created Version $version_id, from Master $master_doc_id ($version_doc_name) " );
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = 'createVersion: ' . $error;
		ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_VERSION", $job_number, "Error creating Version: $error" );
	}
}

# documentLink, create Document from asset
# code from one2edit_asset.pl
# Inputs
# - job_number
# - asset_identifier, ie directory and filename of uploaded INDD file
# Returns
# - document_id

sub documentLink {
	my ( $query, $session, $vars ) = @_;

	my $job_number = $query->param('job_number');
	my $folder_id = $query->param('folder_id'); 	# Document folder id
	my $assetIdentifier = $query->param('asset_identifier');

	my $userid = $session->param( 'userid' );

	eval {
		Validate::job_number( $job_number );

		die "Missing folder_id" unless defined $folder_id;
		die "Missing asset_identifier param" unless defined $assetIdentifier;

		getUser( $query, $session, $vars );

		my $asset_project_id = $settings->{project_id};

		# my $assetIdentifier = $folder_path . '/' . $file; 
		# $assetIdentifier =~ s/^\///; # Remove leading /
 
		push( @{$vars->{history}}, "document.link with folderId => $folder_id, assetProjectId => $asset_project_id, assetIdentifier => $assetIdentifier" );

		my $obj = do_post( { command => 'document.link', folderId => $folder_id, assetProjectId => $asset_project_id, assetIdentifier => $assetIdentifier  } );
		my $document = $obj->{success}->{document};
		push( @{$vars->{history}},  "document.link returns " . Dumper( $document ) );
		my $document_id = $document->{id};
		$vars->{document_id} = $document_id;
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = 'documentLink: ' . $error;
		ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_MASTER", $job_number, "Error on document.link: $error" );
	}
}

# Convert size in mm to points, rounding to nearest int value
# eg my $height_pts = mmTopoints( $version_height );
#
sub mmTopoints {
	my ( $size_mm ) = @_;

	# my $size_pts = round( $size_mm * MM_TO_PTS );
	my $size_pts = ceil( $size_mm * 1000 * MM_TO_PTS ) / 1000; # Use the 1000 factor so we end up with 3 decimal places
	return $size_pts;
}

# resizeDocument, change the width and height of the Version using the szheight and szwidth from tracker3plus

sub documentResize {
	my ( $query, $session, $vars ) = @_;

	my $job_number = $query->param('job_number');
	my $document_id = $query->param('document_id');		# id of Version document to update

	my $userid = $session->param( 'userid' );

	# Document resize
	eval {
		Validate::job_number( $job_number );
		die "Missing param document_id" unless defined $document_id;

		# select szwidth, szheight from tracker3 where requireddate > '2017-06-01 00:00'; 
		# returns many rows with null value
		# both columns are varchar(15) nullable

		# Get size of Version job
		my ( $version_height, $version_width ) = $Config::dba->process_oneline_sql(
			"SELECT szheight, szwidth FROM tracker3 WHERE job_number = ?", [ $job_number ] );

		die "Height or width is NULL\n" if ( ( ! defined $version_height ) || ( ! defined $version_width ) );
		die "Invalid height '$version_height'\n" unless looks_like_number( $version_height );
		die "Invalid width '$version_width'\n" unless looks_like_number( $version_width );
		die "Invalid height '$version_height'\n" if ( $version_height < 1 ) || ( $version_height > MAX_SIZE_IN_MM );
		die "Invalid width '$version_width'\n" if ( $version_width < 1 ) || ( $version_width > MAX_SIZE_IN_MM );

		push( @{$vars->{history}}, "Resize:  Version size in mm: $version_height, $version_width" );

		# Could in theory compare the Version job size with the Master Job size and only resize if they don't match
		# but for the time being just do it.

		my $height_pts = mmTopoints( $version_height );
		my $width_pts = mmTopoints( $version_width );
		die "Failed to convert size from mm to points\n" if ( ( $height_pts == 0 ) || ( $width_pts == 0 ) );

		getUser( $query, $session, $vars );

		push( @{$vars->{history}}, "Resizing document $document_id, using $width_pts x $height_pts pts\n" );
		my $obj = do_post( { command => 'document.resize', id => $document_id, width => $width_pts, height => $height_pts, } );
		push( @{$vars->{history}},  "Resize returned " . Dumper( $obj->{success} ) );

		$vars->{resized} = 1;
		$vars->{height_mm} = $version_height;
		$vars->{width_mm} = $version_width;
		$vars->{height_pts} = $height_pts;
		$vars->{width_pts} = $width_pts;

		ECMDBAudit::db_auditLog( $userid, 
			"ONE2EDIT_VERSION", $job_number, "Resized Version $document_id, $version_height x $version_width mm, $width_pts x $height_pts Points" );
	};
	if ( $@ ) {
		my $error = $@;
		$vars->{error} = 'Resize: ' . $error;
		ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_VERSION", $job_number, "Resize Version $document_id,  error $error" );
	}
}

sub documentCommit {
	my ( $query, $session, $vars ) = @_;

	my $document_id = $query->param('document_id');		# id of Version document to update
	my $toStatus = $query->param('tostatus') || 'DONE';	# One of NEEDSEDIT, EDITED, NEEDSADAPTION, NEEDSREVIEW, REVIEWED, DONE
	my $job_number = $query->param('job_number');		# Needed for audit log

	my $userid = $session->param( 'userid' );

	eval {
		die "Missing param document_id" unless defined $document_id;
		die "Missing param tostatus" unless defined $toStatus;

		getUser( $query, $session, $vars );

		push( @{$vars->{history}}, "Document.commit, documentId $document_id, toStatus '$toStatus'" );
		my $obj = do_post( { command => 'document.workflow.commit', documentId => $document_id, toStatus => $toStatus } );
		push( @{$vars->{history}}, "Commit returned status " . Dumper( $obj->{success}->{status} ) ) ;
	};
	if ( $@ ) {
		my $error = $@;
		$vars->{error} = 'documentCommit: ' . $error;
		ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_VERSION", $job_number, "Error applying Document workflow: $error" );
	}
}

# Document Workflow

sub documentDeleteWorkflow {
	my ( $query, $session, $vars ) = @_;

	my $document_id = $query->param('document_id');	 # id of Version document to update
	my $job_number = $query->param('job_number');			# Needed for audit log

	my $userid = $session->param( 'userid' );

	eval {
		die "Missing param document_id" unless defined $document_id;

		getUser( $query, $session, $vars );

		push( @{$vars->{history}}, "Deleting workflow for document $document_id" );
		my $obj = do_post( { command => 'document.workflow.delete', documentId => $document_id } );
	};
	if ( $@ ) {
		my $error = $@;
		if ( $error !~ /13110/ ) {
			$vars->{error} = 'documentDeleteWorkflow: ' . $error; # Ignore Error: 13110 >> No workflow.
			ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_VERSION", $job_number, "Error deleting workflow for document: $error" );
		}
	}
}

# Start Document workflow
# Inputs
# - document_id
# - workflow_id
# - workflow_name (name corresponding to workflow_id)
# - job_number, for audit purpose

sub startDocumentWorkflow {
	my ( $query, $session, $vars ) = @_;

	my $document_id = $query->param( 'document_id' );
	my $job_number = $query->param( 'job_number' );

	my $workflow = $query->param( 'workflow_name' ); # Workflow name eg Jobflow_Master
	my $workflow_id = $query->param( 'workflow_id' ); # id corresponding to Jobflow_Master workflow
	my $userid = $session->param( 'userid' );

	eval {
		die "Missing document_id param" unless defined $document_id;
		die "Missing workflow_id param" unless defined $workflow_id;
		die "Missing workflow_name param" unless defined $workflow;
		Validate::job_number( $job_number );
		push( @{$vars->{history}}, "Starting Document Workflow for document $document_id, workflow $workflow ($workflow_id)" );

		getUser( $query, $session, $vars );

		my $obj = do_post( { command => 'document.workflow.start', documentId => $document_id, workflowId => $workflow_id } );
		my $jobs = to_array( $obj->{success}->{jobs}->{job} );
		push( @{$vars->{history}}, "Start workflow returned: " . Dumper( $obj->{success} ) );
		# Should have 2 jobs, one with stepData.type = 'CONTENT', the other with 'REVIEW'
		$vars->{jobs} = $jobs;
		$vars->{workflow} = $workflow;
		$vars->{workflow_id} = $workflow_id;
	};
	if ( $@ ) {
		my $error = $@;
		if ( $error =~ /template has no matching groups/ ) {
			$error =~ s/ at .*//gs;
			$error .= " - Please check that the Master document has a content group called $settings->{ content_group }.";
		}
		# $error =~ s/ at .*//gs;
		$vars->{error} = "startDocumentWorkflow: workflow $workflow ($workflow_id) " . $error;
		ECMDBAudit::db_auditLog( $userid, "ONE2EDIT_VERSION", $job_number, "Error starting workflow for document: $error" );
	}
}

sub isDocumentOpen {
	my ( $query, $session, $vars ) = @_;

	my $document_id = $query->param( 'document_id' );
	eval {
		die "Missing document_id param" unless defined $document_id;


		# push( @{$vars->{history}}, "Starting Document Workflow for document $document_id, workflow $workflow ($workflow_id)" );

		getUser( $query, $session, $vars );

		my $obj = do_post( { command => 'document.info', id => $document_id, } );
		my $info = $obj->{success}->{document};
		# push( @{$vars->{history}}, "document.info returned: " . Dumper( $info ) );
		$vars->{isOpen} = $info->{isOpened};
		push( @{$vars->{history}}, "document.info returned: isOpened " . $vars->{isOpen} );
	};
	if ( $@ ) {
		my $error = $@;
		# $error =~ s/ at .*//gs;
		$vars->{error} = "isDocumentOpen " . $error;
	}
}

# Display One2Edit Summary page
# Returns HTML, so set CGI param mode to html to avoid JSON treatment
 
sub summaryData {
	my ( $query, $session, $vars ) = @_;

	$vars->{process_count_days}	= Config::getConfig("one2edit_process_count_days") || 7;
	$tt->process( 'ecm_html/one2edit_summary.html',$vars) || die $tt->error(), "\n";
	$query->param( -name=> 'mode', -value => 'html' );
}

sub get_campaign_details {
	my ( $query, $session, $vars ) = @_;

	my $campaign_path = $query->param("campaign_path");

	eval {
		die "Missing campaign_path" unless defined $campaign_path;
		my ( $customer, $client, $campaign ) = split('/', $campaign_path);

		die "Invalid customer" unless ( defined $customer ) && ( $customer ne '' );
		die "Invalid client" unless ( defined $client ) && ( $client ne '' );
		die "Invalid campaign" unless ( defined $campaign ) && ( $campaign ne '' );

		push( @{$vars->{history}}, "Customer $customer, Client $client, Campaign $campaign" );
		my $job_data = ECMDBOne2Edit::db_getJobsInCampaign( $customer, $client, $campaign );
		push( @{$vars->{history}}, "AFter call to db_getJobsInCampaign" );
		$vars->{job_data} = $job_data;
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = $error;		
	}
}

# Get process count to display on One2Edit Summary page

sub get_process_count {
	my ( $query, $session, $vars )	= @_;

	eval {
		my $days	=	$query->param("days");
		die "Missing days param" unless defined $days;
		die "Invalid days param $days" unless looks_like_number( $days );

		my $process_count = ECMDBOne2Edit::getProcessCount($days);
		$vars->{process_count} = $process_count;
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = "get_process_count: $error";
	}
}

sub get_queue_list {
	my ( $query, $session, $vars ) = @_;

	eval {
		my $limit	= Config::getConfig("one2edit_summary_limit") || 200;
		my $search_type	= $query->param("search_data_type") || "";
		my $search_value = $query->param("search") || "";

		my $queue	= ECMDBOne2Edit::getOne2EditQueueDetails($limit,$search_type,$search_value);
		$vars->{ queue } = $queue;
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = "get_queue_list: $error";
	}
}

sub get_campaign_list {
	my ( $query, $session, $vars )  = @_;

	eval {
		my $limit	=	Config::getConfig("one2edit_summary_limit") || 200;
		my $search_type	= $query->param("search_data_type") || "";
		my $search_value	= $query->param("search") || "";
		my $campaign	= ECMDBOne2Edit::getOne2EditCampaignList($limit,$search_type,$search_value);
		$vars->{ campaign_list } = $campaign;
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = "get_campaign_list: $error";
	}
}

# getContentGroup, determine if document has specific content group, throw error if not
sub getContentGroup {
	my ( $query, $session, $vars ) = @_;

	my $document_id = $query->param( 'master_doc_id' ) || $query->param( 'document_id' );

	my $userid = $session->param( 'userid' );

	# Get list of groups we need to see (need to see at least one of these, unless the list is empty)

	my $groups = {}; # Set of groups found

	eval {
		die "Missing document_id param" unless defined $document_id;

		push( @{$vars->{history}}, "Get Content Groups for document $document_id" );

		getUser( $query, $session, $vars ); # Sets $vars->{username} and {password}

		my $obj = do_post( { command => 'document.group.list', documentId => $document_id } );

		my $group = to_array( $obj->{success}->{groups}->{group} );
		foreach my $d ( @{$group} ) {
			$groups->{ $d->{name} } = $d->{id};
		}
		$vars->{content_groups} = $groups; # return even if there is an error

		my $content_groups = Config::getConfig("one2edit_content_group") || 'Data';
		$vars->{content_groups_needed} = $content_groups;

		my @list = split /,/, $content_groups;
		my @missing;
		my @found;
		foreach my $g ( @list ) {
			if ( exists $groups->{ $g } ) {
				push( @found, $g );
			} else {
				push( @missing, $g );
			}
		}
		if ( scalar @found == 0 ) {
			my $missing_groups = join( ', ', @missing );
			die "Master document $document_id, doesn't have content group(s) '$missing_groups'";
		}
		# push( @{$vars->{history}}, "Get Content Group returned: " . Dumper( $obj->{success} ) );

		# freeUser( $query, $session, $vars );
	};
	if ( $@ ) {
		my $error = $@;
		if ( $error =~ /doesn't have content group/ ) {
			$error =~ s/ at .*//gs;
		}
		$vars->{error} = "getContentGroup: $error";
	}
}

sub check_task_found {
	my ( $query, $session, $vars ) = @_;

	my $job_number = $query->param( 'job_number' );
	my $session_opcoid = $session->param( 'opcoid' );
	my $session_opco = $session->param( 'opconame' );

	my @product_list;

	eval {
		die "Missing job number" unless ( defined $job_number ) && looks_like_number( $job_number );

		my ( $jobPath, $jobopcoid, $customerid, $job_location ) = JobPath::getJobPath( $job_number );

		if ( $session_opcoid != $job_location ) {
			my ( $location_name ) = $Config::dba->process_oneline_sql( "SELECT opconame FROM operatingcompany WHERE opcoid = ?", [ $job_location ] );

			die "Job is at $location_name, please change to $session_opco and retry.\n";
		}

		my $rows = $Config::dba->hashed_process_sql( 
			"SELECT id, productname FROM products WHERE job_number = ? AND isDeleted = 0 AND JOBVERSION = (SELECT MAX(JOBVERSION) FROM products WHERE job_number = ?)", 
			[ $job_number, $job_number] );
		if ( defined $rows ) {
			foreach my $res ( @$rows ) {
				push( @product_list, { id => $res->{ id }, productname => $res->{ productname } } );
			}
		}

		my $product_count = scalar @product_list;

		my $one2edit_send_to_qcmonitor = Config::getConfig( 'one2edit_send_to_qcmonitor' ) || 0;
		my $o2e_artwork_task	= Config::getConfig("one2edit_artwork_task" ) || 'Artwork.*';
		my $o2e_task_warning    = Config::getConfig("one2edit_task_warning" ) || 'Missing Artwork task';
		my $o2e_qc_task			= Config::getConfig("one2edit_qc_task" ) || 'QC Check';

		my $artwork_found_id = 0; # id of product that matches
		my $artwork_found_name = ''; # name of matching product
		my $qc_found_id = 0; # id of product that matches
		my $qc_found_name = ''; # name of matching product

		if ( $product_count > 0 ) {

			my @pattern_list = split /,/, $o2e_artwork_task;
			foreach my $pattern ( @pattern_list ) {
				foreach my $h ( @product_list ) {
					if ( $h->{productname} =~ /$pattern/i ) {
						$artwork_found_id = $h->{id};
						$artwork_found_name = $h->{productname};
						last;
					}
				}
				last if ( $artwork_found_id != 0 );
			}

			@pattern_list = split /,/, $o2e_qc_task;
			foreach my $pattern ( @pattern_list ) {
				foreach my $h ( @product_list ) {
					if ( $h->{productname} =~ /$pattern/i ) {
						$qc_found_id = $h->{id};
						$qc_found_name = $h->{productname};
						last;
					}
				}
				last if ( $qc_found_id != 0 );
			}
		}

		if ( $one2edit_send_to_qcmonitor == 0 ) {
			$vars->{task_warning} = "Send to QC Monitor disabled for One2Edit jobs"; # Not sure this should even be a warning
			push( @{$vars->{history}}, $vars->{task_warning} );

		} elsif ( $product_count == 0 ) {
			$vars->{task_warning} = "This job has no tasks and will not be moved to QC Monitor. Click OK to continue.";
			push( @{$vars->{history}}, $vars->{task_warning} );

		} elsif ( $product_count == 1 ) {
			push( @{$vars->{history}}, "Please ensure that there are at least two tasks, including an Artwork task in the Job" );
			die "Please ensure that there is an Artwork task and another task to QC in the Job\n";

		} else { # product count is 2 or more


			#if ( $artwork_found_id == 0 or $qc_found_id == 0 ) {
			if ( $artwork_found_id == 0 ) {
				$vars->{task_warning} = $o2e_task_warning;
				push( @{$vars->{history}}, "No matching task found" );
			} else {
				$vars->{task_matched} = $artwork_found_name;
				push( @{$vars->{history}}, "Found matching task $artwork_found_name($artwork_found_id), QC Task $qc_found_name($qc_found_id)" );
			}
		}
    };

    if ( $@ ) {
        my $error = $@;
        #if ( $error =~ /Please ensure/ ) {
            $error =~ s/ at module.*//gs;
        #}
        $vars->{error} = $error;
    }
}

# -- Font, Job, Language, PDF, Preflight, Search, Segmentation, Site, System, Tags, Template, User, Workflow (expand as needed)
#
# XML::Parser handlers

sub hdl_start {
	my ($p, $elt, %atts) = @_;

	return if ( $elt eq 'success' );

	my $context = $p->{'Non-Expat-Options'};

	if ( ! ( ( $elt eq 'segments' ) || ( $elt eq 'segment' ) || ( $elt eq 'attribute' ) || ( $elt eq 'tag' ) ) ) {
		# warn "hdl_start: elt $elt -- something else\n";

	} else {
		# Deal with the Output XML later, but save a copy of the attributes for the current element
		# which we can use in the output XML without corrupting the structures used by XML:Parser::expat

		my %atts_copy;  # Close %atts as we need to change this for the <attribute> element
		foreach my $k ( keys %atts ) {
			$atts_copy{ $k } = $atts{ $k };
		}

		# Deal with element specific handling first as this can alter XML output eg style name in attribute element

		my $mode = $context->{mode};
		if ( $elt eq 'attribute' ) {
			my $name = $atts{ name }; # Style type
			my $value = $atts{ value }; # Style name
			$context->{seen} = {} unless exists $context->{seen}; # Track all instances of value

			if ( $mode eq 'read' ) {
				my $pattern = $context->{pattern};

				if ( $name !~ /^(CharStyle|ParaStyle)$/ ) {
					# Something else, note we can't cope with other foreign attributes embedded in the text

				} elsif ( $value !~ /$pattern/ ) { # Attribute name is ok, but attribute value doesnt match pattern
					# Say we come across <attribute name="CharStyle" value="Colours/White"/>
					# just ignore it and carry on grabbing text after it.
					# push( @{$context->{history}}, "hdl_start: Pattern didnt matched '$value'" ) if exists $context->{history};
					#my $composite = $context->{matched};
					#if ( $composite ne '' ) {
					#	if ( $context->{distinct}->{ $composite } ne '' ) {
					#		# warn "hdl_start: attribute (1): name $name value $value  Cancel existing match $composite";
					#		$context->{matched} = '';	# Stop matching previous ParaStyle if there was text seen between
					#	}
					#}
					#$composite = "$name#$value";
					#$context->{seen}->{ $composite } = { count => 1, match => 0 };

				} else { # Both name and value match the pattern

					# Check to see if we need to terminate a prior style match
					my $composite = $context->{matched};
					if ( $composite ne '' ) {
						if ( $context->{distinct}->{ $composite } ne '' ) { # ie there is text between the last style attribute and this on
							# warn "hdl_start: attribute (2): name $name value $value  Cancel existing match $composite";
							$context->{matched} = '';	# Stop matching previous ParaStyle if there was text seen between
						}
					}
					$composite = "$name#$value"; # New composite key
					if ( exists $context->{seen}->{ $composite } ) {
						$context->{seen}->{ $composite }->{count}++; # Ignore this style if we have seen it before
	
					} else {
						# First time we have seen the style
						# push( @{$context->{history}}, "hdl:start: Pattern matched '$value'" ) if exists $context->{history};
						$context->{seen}->{ $composite } = { count => 1, match => 1 };
						# warn "hdl_start: attribute (3): name $name value $value, capturing text";
						$context->{matched} = ''; # Cancel previous style capture if set
						my $composite = "$name#$value"; # Composite key for hash
						if ( exists $context->{distinct}->{ $composite } ) {
							warn "hdl_start: attribute (4): name $name value $value, distinct already has key for this\n";
						} else {
							# warn "hdl_start: creating new entry for $composite";
							$context->{distinct}->{ $composite } = '';
							$context->{matched} = $composite;
						}
					}
				}
			} elsif ( $mode eq 'transform' ) {
				my $composite = "$name#$value";
				if ( exists $context->{translations}->{ $composite } ) {
					my $t = $context->{translations}->{ $composite };
					$context->{replace_flag} = 1; # Suppress any following char content
					$context->{replace_content} = $t->{ style_text };
					# Change in style_name, name attrib is stype type, value attrib is style name
					$atts_copy{ value } = $t->{ style_name };
				}
			}

		} elsif ( $elt eq 'segment' ) {
			$context->{replace_flag} = 0;
			$context->{matched} = ''; # Should have been trapped in hdl_end, so just in case
		}

		# Output XML, had to defer as we can change the style name attribute in the <attribute> element

		$context->{output_xml} .= '<' . $elt;
		foreach my $k ( sort keys %atts_copy ) {
			# warn ">> element $elt, $k, value '" . $atts_copy{ $k } . "'"; 
			$context->{output_xml} .= " $k=\"" . $atts_copy{ "$k" } . "\"";
		}
		if ( $elt =~ /(segment|segments)/ ) {
			$context->{output_xml} .= ">"; # The corresponding close element is written in hdl_end
		} else {
			$context->{output_xml} .= "/>"; # tag and attribute just close, they are not containers
		}
	}
	return;
}

sub hdl_end {
	my ($p, $elt) = @_;

	return if ( $elt eq 'success' );

	my $context = $p->{'Non-Expat-Options'};
	if ( ( $elt eq 'segments' ) || ( $elt eq 'segment' ) ) {
		$context->{output_xml} .= '</' . $elt . ">\n";
		$context->{replace_flag} = 0; # Matters for transform mode
		$context->{matched} = '';	# Matters for read mode
	} elsif ( ( $elt eq 'attribute' ) || ( $elt eq 'tag' ) ) {
		# Already closed it
	} else {
		# warn "hdl_end: elt $elt -- something else\n";
	}
	return;
}

sub hdl_char {
	my ($p, $str) = @_;

	my $text = $p->recognized_string();
	#chomp $text;
	return if ( $text eq '' );

	my $context = $p->{'Non-Expat-Options'};
	my $mode = $context->{mode};
	if ( $mode eq 'read' ) {
		my $composite_key = $context->{matched};
		# warn "hdl_char: read: >> $text << key '$composite_key'\n";
		if ( defined ( $composite_key ) && $composite_key ne '' ) {
			# warn "Appending to style '$composite_key', text $text";
			$context->{distinct}->{ $composite_key } .= $text;
		}
		$context->{output_xml} .= $text;

	} elsif ( $mode eq 'transform' ) {
		# warn "hdl_char: transform: >> $text <<\n";
		if ( $context->{replace_flag} == 0 ) {
			$context->{output_xml} .= $text;
		} else {
			$context->{output_xml} .= $context->{replace_content};
			$context->{replace_content} = '';
		}
	}
	return;
}

sub hdl_def {
	my ($p, $str) = @_;
	# warn "hdl_def: str $str\n";
}

sub hdl_element {
	my ($p, $name, $model) = @_;
	# warn "hdl_element: name $name, model $model\n";
}

sub hdl_attlist {
	my ($p, $name, $elname, $attname, $type, $default, $fixed) = @_;
	# warn "hdl_attlist: name $elname, attname $attname type $type default $default fixed $fixed\n";
}

sub hdl_final {
	# warn "hdl_final called\n";
}

# extractStyles, extract the relevant styles from the documents segment text

sub extractStyles {
	my ( $query, $session, $vars ) = @_;

	my $job_number = $query->param( 'job_number' ); #Job selected in list
	my $document_id = $query->param( 'master_document_id' ) || $query->param( 'document_id' );
	my $debug = $query->param( 'debug' ) || 0;

	# Export text

	my $segments;
	my $segments_xml = '';

	eval {
		die "Missing job_number parameter" unless defined $job_number;
		Validate::job_number( $job_number );

		die "Missing document_id parameter" unless defined $document_id;
		die "Invalid document id $document_id" unless looks_like_number( $document_id );

		push( @{$vars->{history}}, "Get Styles for document $document_id" );

		getUser( $query, $session, $vars ); # Sets $vars->{username} and {password}

		push( @{$vars->{history}}, "Document.export.text" );
		my $obj = do_post( { command => 'document.export.text', id => $document_id } );
		$segments = $obj->{success}->{segments};
		push( @{$vars->{history}}, "Document.export.text returned segments for client " . $segments->{"client-id"} . 
						", document " . $segments->{"document-id"} );
		$segments_xml = $obj->{xml};

		my $style_pattern2match = Config::getConfig("one2edit_style_pattern");

		my $context = {
			pattern => $style_pattern2match,
			output_xml => '', # Transformed output XML string
			depth => 0,
			mode => 'read', # distinguish between read and transform mode
			replace_flag => 0,
			replace_content => '',
			distinct => {},
			matched => '',
			history => $vars->{history},
		};

		my $xml_parser = XML::Parser->new(
			'Non-Expat-Options' => $context,
			ProtocolEncoding => 'UTF-8',
			Handlers => {
			Start	=> \&hdl_start,
			End	=> \&hdl_end,
			Char	=> \&hdl_char,
			Default => \&hdl_def,
			Final => \&hdl_final,
			Element => \&hdl_element,
			Attlist => \&hdl_attlist, }
		);

		$xml_parser->parse( $segments_xml );
		my $styles = $context->{distinct};
		my $seen = $context->{seen};
		my @seen_list;
		if ( defined $seen ) {
			foreach my $k ( sort keys %$seen ) {
				my $s = $seen->{ $k };
				push( @seen_list, { $k => $s } );
			}
		}

		push( @{$vars->{history}}, "Adding/replacing Styles in one2edit_job_styles" );

		# Delete any styles for this job, as we can do this operation more than once
		$Config::dba->process_oneline_sql( "DELETE FROM one2edit_job_styles WHERE job_number = ?", [ $job_number ] );

		foreach my $composite ( keys %$styles ) { # So in this hash, the key is a composite key
			my $text = $styles->{ $composite };
			$text =~ s/&#x([a-fA-F0-9]{2});/pack('C', hex($1))/eg; # Convert &#xC9; to a Unicode char
			my ( $style_type, $style_name ) = split /#/, $composite; # Use style type and name to form composite key
			$Config::dba->process_oneline_sql(
				"INSERT INTO one2edit_job_styles ( " .
				" job_number, style_type, style_name, style_text, original_type, original_name, original_text ) " .
				" VALUES ( ?, ?, ?, ?, ?, ?, ? )",
				[ $job_number, $style_type, $style_name, $text, $style_type, $style_name, $text ] );
		}
		$vars->{styles} = $styles;
		$vars->{output_xml} = $segments_xml if ( $debug );
		$vars->{pattern} = $style_pattern2match;
		$vars->{seen} = \@seen_list; # Ordered seen list
	};
	if ( $@ ) {
		my $error = $@;
		$vars->{error} = "extractStyles: $error";
	}
}

# Internal version of testParse, can be called by unit test

sub testParseInternal {
	my ( $xml, $xml_mode, $pattern, $translations ) = @_;

	my $context;
	eval {
		die "Missing xml parameter" unless ( defined $xml ) && ( $xml ne '' );
		die "Missing xml_mode parameter" unless ( defined $xml_mode );
		die "Invalid xml_mode param, must be 'read' or 'transform'" unless ( $xml_mode =~ /^(read|transform)$/ );
		die "Missing pattern" unless defined $pattern;
		die "Missing translations param" unless ( $xml_mode ne 'transform' ) || ( defined $translations );

		$context = {
			pattern => $pattern,
			output_xml => '', # Transformed output XML string
			depth => 0,
			mode => $xml_mode, # distinguish between read and transform mode
			replace_flag => 0,
			replace_content => '',
			translations => $translations,
			distinct => {},
			matched => '',
		};

		my $xml_parser = XML::Parser->new(
			'Non-Expat-Options' => $context,
			ProtocolEncoding => 'UTF-8',
			Handlers => {
				Start	=> \&hdl_start,
				End	=> \&hdl_end,
				Char	=> \&hdl_char,
				Default => \&hdl_def,
				Final 	=> \&hdl_final,
				Element => \&hdl_element,
				Attlist => \&hdl_attlist,
			}
		);

		$xml_parser->parse( $xml );
		# $vars->{styles} = $context->{distinct};
		# $vars->{output_xml} = $context->{output_xml} if $xml_mode eq 'transform';
	};
	if ( $@ ) {
		my $error = $@;
		die "testParseInternal: $error";
	}
	return $context;
}

# Can be called using subaction testparse

sub testParse {
	my ( $query, $session, $vars ) = @_;

	my $segments_xml = $query->param( 'xml' );
	my $xml_mode = $query->param( 'xml_mode' );
	my $subkey = $query->param( 'subkey' ); # key of style to replace (if mode=transform)
	my $subval = $query->param( 'subval' ); # replacement text for style (if mode=transform)

	eval {
		die "Missing xml parameter" unless ( defined $segments_xml ) && ( $segments_xml ne '' );
		die "Missing xml_mode parameter" unless ( defined $xml_mode );
		die "Invalid xml_mode param, must be 'read' or 'transform'" unless ( $xml_mode =~ /^(read|transform)$/ );

		my $style_pattern2match = Config::getConfig("one2edit_style_pattern");
		my $translations = {};
		$translations ->{ $subkey } = $subval if defined $subkey;
		my $context = testParseInternal( $segments_xml, $xml_mode, $style_pattern2match, $translations );
		$vars->{styles} = $context->{distinct};
		$vars->{output_xml} = $context->{output_xml} if $xml_mode eq 'transform';
	};
	if ( $@ ) {
		my $error = $@;
		$vars->{error} = "testParse: $error";
	}
}

sub jobAssociate {
	my ( $query, $session, $vars ) = @_;

	my $version_job_number	= $query->param("version_job_number");	
	my $master_job_number	= $query->param("master_job_number");	
	my $master_doc_id		= $query->param("master_document_id");
	my $master_doc_name		= $query->param("master_document_name");

	my $userid				= $session->param( 'userid' );

	eval {
		Validate::job_number( $version_job_number );
		die "Missing master_document_id" unless defined $master_doc_id;
		die "Invalid document id" unless looks_like_number( $master_doc_id );
		die "Missing master_document_name" unless defined $master_doc_name; 

		$Config::dba->process_oneline_sql( 
			"INSERT INTO one2edit_associate ( job_number, master_job_no, master_doc_id, master_doc_name, assoc_userid ) VALUES( ?, ?, ?, ?, ? ) " .
			" ON DUPLICATE KEY UPDATE master_job_no = ?, master_doc_id = ?, master_doc_name = ?, assoc_userid = ?", 
			[ $version_job_number, $master_job_number, $master_doc_id, $master_doc_name, $userid, 
					$master_job_number, $master_doc_id, $master_doc_name, $userid ] );
	};
	if ( $@ ) {
		my $error = $@;
		$vars->{error} = "jobAssociate : $error";
	}
}

sub getStyles {
	my ( $query, $session, $vars ) = @_;

	my $version_job_number = $query->param("version_job_number");
	eval {
		die "Missing job_number parameter" unless ( defined $version_job_number );
		Validate::job_number( $version_job_number );

		my $rows = $Config::dba->hashed_process_sql( 
			"SELECT id, job_number, style_type, style_name, style_text, original_type, original_name, original_text " .
			" FROM one2edit_job_styles WHERE job_number = ? ", 
			[ $version_job_number ] );
		my @list;
		if ( defined $rows ) {
			foreach my $files (@$rows) {
				push(@list, {
					id => $files->{id},
					job_number => $files->{job_number},
					style_type => uri_escape_utf8($files->{style_type}),
					style_name => uri_escape_utf8($files->{style_name}),
					style_text => uri_escape_utf8($files->{style_text}),
					original_type => uri_escape_utf8($files->{original_type}),
					original_name => uri_escape_utf8($files->{original_name}),
					original_text => uri_escape_utf8($files->{original_text}),
				});			
			}
		}
		$vars->{style_list}	= \@list;	
	};
	if ( $@ ) {
		my $error = $@;
		$vars->{error} = "getStyles : $error";
	}
}

sub updateStyle {
	my ( $query, $session, $vars ) = @_;

	my $version_job_number = $query->param("version_job_number");	
	my $style_list = $query->param("style_list");

	eval {
		my $decoded_list = decode_json( $style_list );
		$vars->{decoded_list} = $decoded_list;
		foreach my $res ( @$decoded_list  ) { 
			# Update edited styles

			# TODO should not be updating original_text
			# and you might need to update the style_name (say if teh language changes)
				$Config::dba->process_oneline_sql(
					"UPDATE one2edit_job_styles SET style_text = ?, style_name = ?  WHERE id = ?", 
					[ $res->{style_text}, $res->{style_name}, $res->{id} ] );
		}	
	};
	if ( $@ ) {
		my $error = $@;
		$vars->{error} = "updateStyle : $error";
	}
}

sub transformStyles {
	my ( $query, $session, $vars ) = @_;

	my $job_number = $query->param( 'job_number' ); # Job for Version
	my $document_id = $query->param( 'document_id' ); # Version document id (we read/update the segment text in this)

	my $segments;
	my $segments_xml = '';

	eval {
		die "Missing job_number parameter" unless defined $job_number;
		Validate::job_number( $job_number );

		die "Missing document_id parameter" unless defined $document_id;
		die "Invalid document id $document_id" unless looks_like_number( $document_id );

		push( @{$vars->{history}}, "Getting saved styles for job $job_number" );

		my $styles = $Config::dba->hashed_process_sql( 
			"SELECT original_type, original_name, original_text, style_type, style_name, style_text " .
			" FROM one2edit_job_styles WHERE job_number = ? ", 
			[ $job_number ] );
		$vars->{style_list} = $styles;	# Complete list of styles

		# Narrow the list down by excluding those that havent changed. Note that we can change both style_name and text.
		my $translations = {};
		foreach my $s ( @$styles ) {
			if ( ( $s->{original_type} ne $s->{style_type} ) ||
				( $s->{original_name} ne $s->{style_name} ) ||
				( $s->{original_text} ne $s->{style_text} ) ) {
				my $key = $s->{original_type} . '#' . $s->{original_name};
				$s->{ style_text } =~ s/([\x80-\xff])/sprintf "&#x%02x;",ord($1)/eg; # replace Unicode with &#xC9;
					# Getting "cr&#x{e8};me br&#x{fb};l&#x{e9};e"
					# s/([\x80-\xff])/sprintf "\\x{%02x}",ord($1)/eg; gives "\x{e8}"
				$translations->{ $key } = $s;		
			}
		}
		$vars->{translations} = $translations;	# List of styles which are to be changed

		my $translation_count = scalar keys %$translations;
		if ( $translation_count == 0 ) {
			push( @{$vars->{history}}, "No styles have been changed, skipping the transform" );

		} else {
			push( @{$vars->{history}}, "Getting Segment text for document $document_id" );

			getUser( $query, $session, $vars ); # Sets $vars->{username} and {password}

			push( @{$vars->{history}}, "Document.export.text" );
			my $obj = do_post( { command => 'document.export.text', id => $document_id } );
			$segments = $obj->{success}->{segments};
			push( @{$vars->{history}}, "Document.export.text returned segments for client " . $segments->{"client-id"} . 
						", document " . $segments->{"document-id"} );
			$vars->{xml} = $obj->{xml};

			my $style_pattern2match = Config::getConfig("one2edit_style_pattern");

			my $context = {
				pattern => $style_pattern2match,
				output_xml => '', # Transformed output XML string
				depth => 0,
				mode => 'transform', # distinguish between read and transform mode
				replace_flag => 0,
				replace_content => '',
				translations => $translations,
				distinct => {},
				matched => '',
				history => $vars->{history},
			};

			my $xml_parser = XML::Parser->new(
				'Non-Expat-Options' => $context,
				ProtocolEncoding => 'UTF-8',
				Handlers => {
				Start	=> \&hdl_start,
				End	=> \&hdl_end,
				Char	=> \&hdl_char,
				Default => \&hdl_def,
				Final   => \&hdl_final,
				Element => \&hdl_element,
				Attlist => \&hdl_attlist, }
			);

			$xml_parser->parse( $vars->{xml} );

			my $transformed_xml = $context->{output_xml};
			$vars->{transformed_xml} = $transformed_xml;

			push( @{$vars->{history}}, "Updating Segment text for Version document $document_id" );
			$obj = do_post( { command => 'document.import.text', id => $document_id, data => $transformed_xml } );

			# import entry contains importCount, count, warningCount
			push( @{$vars->{history}}, "document.import.text returned " . Dumper( $obj->{success}->{import} ) );
		}
	};
	if ( $@ ) {
		my $error = $@;
		$vars->{error} = "transformStyles : $error";
	}
}

# Get list of languages (name and corresponding 2 letter language code)

sub getLanguages {
	my ( $query, $session, $vars ) = @_;

	eval {
		my $rows = $Config::dba->hashed_process_sql( 
			"SELECT c.language, c.language_code, c.country, c.iso_2_country_cd, ".
			" iso.alpha3_bib_cd, COALESCE(NULLIF(c.font_region,''), 'en') as font_region" .
			" FROM one2edit_language_codes c " .
			" LEFT OUTER JOIN one2edit_iso639_2 iso ON iso.alpha2_cd = c.language_code " .
			" ORDER BY language" );
		$vars->{languages} = $rows;

	};
	if ( $@ ) {
		my $error = $@;
		$vars->{error} = "getLanguages : $error";
	}
}

# Update language for job, sets column iso_language_cd in table one2edit_job_language
# Doesnt use INSERT ... ON DUPLICATE UPDATE as we dont supply all the necessary cmd values

sub updateLanguage {
	my ( $query, $session, $vars ) = @_;

	my $job_number = $query->param("job_number");
	my $language_cd = $query->param("language_cd");
	my $country_cd = $query->param("country_2_cd");
	eval {
		die "Missing job_number parameter" unless ( defined $job_number );
		Validate::job_number( $job_number );
		die "Missing language_cd parameter" unless ( defined $language_cd );
		die "Invalid language_cd '$language_cd'" unless ( $language_cd =~ /^[a-z][a-z]$/ ); # Must be 2 lowercase letters
		die "Missing country_cd parameter" unless ( defined $country_cd );

		$Config::dba->process_oneline_sql( 
			"UPDATE one2edit_job_language SET iso_language_cd = ?, iso_country_cd = ? WHERE job_number = ? ", 
			[ $language_cd, $country_cd, $job_number ] );
	};
	if ( $@ ) {
		my $error = $@;
		$vars->{error} = "updateLanguage : $error";
	}
}

# Look up the job language, look first at table one2edit_job_language, then if not there,
# make API request to CMD to get language info and insert data into one2edit_job_language
# Also note lookup on one2edit_iso639_2 to translate 3 letter language to 2 letter code.

sub getJobLanguage {
	my ( $job_number, $vars ) = @_;

	my $result;
	eval {
		Validate::job_number( $job_number );

		push( @{$vars->{history}}, "Job $job_number, querying table one2edit_job_language" );

		my ( $cmd_iso_language_id, $cmd_iso_language_cd, $cmd_iso_country_cd, $iso_language_cd, $iso_country_cd ) = $Config::dba->process_oneline_sql(
			"SELECT cmd_iso_language_id, cmd_iso_language_cd, cmd_iso_country_cd, iso_language_cd, iso_country_cd " .
				" FROM one2edit_job_language WHERE job_number = ?",
			[ $job_number ], 5 );

		if ( defined $cmd_iso_language_cd ) {
			push( @{$vars->{history}}, "row exists on one2edit_job_language, lang_cd $cmd_iso_language_cd" );

		} else {
			push( @{$vars->{history}}, "No row exists on one2edit_job_language for job $job_number" );

			# Curl request
			my $url = Config::getConfig('cmd_api_get_job_lang');
			die "Config setting 'cmd_api_get_job_lang' not defined" unless defined $url;

			die "Setting 'cmd_api_get_job_lang' doesnt have <job number> placeholder" unless $url =~ /\<job\_number\>/;
			$url =~ s/\<job\_number\>/$job_number/;
			push( @{$vars->{history}}, "Using URL $url" );

			eval {
				my $json_text = `/usr/bin/curl -s -d x=1 $url`;  # the -d x=1 forces a POST request
				die "get language API call returned blank\n" if ( ! defined $json_text || $json_text eq '' );
				push( @{$vars->{history}}, "Response $json_text" );

				# $response should be something like
				# {"isocountrycode3alpha":"USA","isolangaugecodeid":123,"isolangcode":"eng","ismainlanguage":1,"languagename":"Eng",
				#	"countryname":"USA","isocountrycode":"US"}

				my $response = decode_json $json_text;  # Returns an array of hash, so we want the 1st entry (can throw an exception)
				die "curl response: Expected array ref, got '" . ref( $response ) . "'" unless ( ref( $response ) eq 'ARRAY' );
				$response = ${$response}[ 0 ];  # Should now have hash ref
				die "curl response: Expected hash ref, got '" . ref( $response ) . "'" unless ( ref( $response ) eq 'HASH' );

				$cmd_iso_language_id = $response->{ isolangaugecodeid }; 
				$cmd_iso_language_cd = $response->{ isolangcode };
				$cmd_iso_country_cd  = $response->{ isocountrycode };
				die "CMD language code is blank" if ( ( ! defined $cmd_iso_language_cd ) || ( $cmd_iso_language_cd eq '' ) );
			};
			if ( $@ ) {
				my $error = $@;
				push( @{$vars->{history}}, "Error getting job language, $error, using defaults" );

				$cmd_iso_language_id = 123;
				$cmd_iso_language_cd = 'eng';
				$cmd_iso_country_cd  = 'GB';
			}
			$iso_country_cd = $cmd_iso_country_cd;

			push( @{$vars->{history}}, "Looking up language code '$cmd_iso_language_cd' to get 2 letter code" );

			( $iso_language_cd ) = $Config::dba->process_oneline_sql(
				"SELECT alpha2_cd FROM one2edit_iso639_2 WHERE alpha3_bib_cd = ? OR alpha3_term_cd = ?", [ $cmd_iso_language_cd, $cmd_iso_language_cd ] );
			if ( ! defined $iso_language_cd ) {
				push( @{$vars->{history}}, "Failed to find 2 letter code, for $iso_language_cd" );
			} else {
				push( @{$vars->{history}}, "Found 2 letter code $iso_language_cd" );
			}

			push( @{$vars->{history}}, "Adding row to one2edit_job_language" );

			$Config::dba->process_oneline_sql(
				'INSERT INTO one2edit_job_language ' .
					'( job_number, cmd_iso_language_id, cmd_iso_language_cd, cmd_iso_country_cd, iso_language_cd, iso_country_cd ) ' .
					'VALUES( ?, ?, ?, ?, ?, ? )',
				[ $job_number, $cmd_iso_language_id, $cmd_iso_language_cd, $cmd_iso_country_cd, $iso_language_cd, $iso_country_cd ] );
		}

		# Lookup on one2edit_workflows to see if we have a language specific workflow
		# and also get the Language name from one2edit_language_codes (as thats the Display text used by One2Edit internally)

		my $workflow_name;
		my $workflow_id;
		my $language_name;
		my $country_name;
		my $font_region;
		
		if ( ( defined $iso_language_cd ) && ( $iso_language_cd ne '' ) ) {


			findJobWorkflow($job_number, $iso_language_cd, $iso_country_cd, $vars);

			$workflow_name	=	$vars->{workflow_name};
			$workflow_id	=	$vars->{workflow_id};


			push( @{$vars->{history}}, "Looking for Language name/Country name where language_cd = '$iso_language_cd', country  '$iso_country_cd'" );
			( $language_name, $country_name, $font_region ) = $Config::dba->process_oneline_sql(
				"SELECT language, country, font_region FROM one2edit_language_codes WHERE language_code = ? AND iso_2_country_cd = ?",
				[ $iso_language_cd, $iso_country_cd ] );
		}

		if ( defined $workflow_name ) {
			push( @{$vars->{history}}, "Found Language specific Workflow $workflow_name ($workflow_id)" );
		} else {
			push( @{$vars->{history}}, "Using default Workflow $workflow_name ($workflow_id)" );
		}

		$language_name = 'Unknown' unless defined $language_name;
		$country_name = 'Unknown' unless defined $country_name;
		$font_region  = 'EN'	unless defined $font_region;

		$result = { 
			cmd_iso_language_id	=> $cmd_iso_language_id,
			cmd_iso_language_cd	=> $cmd_iso_language_cd, 
			language_name		=> $language_name,
			cmd_iso_country_cd	=> $cmd_iso_country_cd, 
			iso_language_cd		=> $iso_language_cd,
			iso_country_cd		=> $iso_country_cd,
			country_name		=> $country_name,
			workflow_name		=> $workflow_name,
			workflow_id		=> $workflow_id,
			font_region 	=> $font_region, 
		};
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = 'getJobLanguage: ' . $error;
	}
	return $result;
}

sub getJobWorkflow {
	my ( $query, $session, $vars ) = @_;
	
	my $job_number		=	$query->param("job_number");
	my $language_code	=	$query->param("language_code");
	my $country_code	=	$query->param("country_code");


	eval {
		die "Missing job_number param" unless defined $job_number;
		die "Missing language_code param" unless defined $language_code;
		die "Missing country_code param" unless defined $country_code;

		findJobWorkflow( $job_number, $language_code, $country_code, $vars );
	};

    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        $vars->{error} = 'getJobWorkflow : ' . $error;
    }
}


sub findJobWorkflow {
	my ( $job_number, $language_code, $country_code, $vars ) = @_;

	my ($workflow_name, $workflow_id);

	eval {
        getCampaignDetails( $job_number, $vars ) unless exists ( $vars->{customer} );
        my $pattern = Config::getConfig('one2edit_workflow_language_pattern') || '%client%_%language%%country%';
        my $hash = {
            customer => $vars->{customer},
            client   => $vars->{client},
            campaign => $vars->{campaign},
            language => $language_code,
            country  => $country_code,
        };
        foreach my $k ( keys %$hash ) {
            my $e = $hash->{ $k };
            $pattern =~ s/\%$k\%/$e/;
        }

        push( @{$vars->{history}}, "Looking for Workflow with name '$pattern'" );

        ( $workflow_name, $workflow_id ) = $Config::dba->process_oneline_sql(
                "SELECT name, workflow_id FROM one2edit_workflows WHERE name = ?",
                [ $pattern ] );

        if ( ! defined $workflow_name ) {
            $workflow_name = $settings->{ workflow };
            $workflow_id = $settings->{ workflow_id };
        }
        $vars->{workflow_name}  = $workflow_name;
        $vars->{workflow_id}    = $workflow_id;

    };
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		die 'findJobWorkflow : ' . $error;
	}
}
# Test harnesses, see t/one2edit.t

1;

