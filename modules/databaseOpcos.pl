#!/usr/local/bin/perl
package ECMDBOpcos;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use JSON;
use Carp qw/carp/;
use Data::Dumper;

# Home grown
use lib 'modules';

use Validate;

require "config.pl";

##############################################################################
#       getOperatingCompanies
#
#	Return names of all the operating companies except for 
#	IN TRANSIT, REPOSITORY, CHECK-IN QUEUE and CHECK-OUT QUEUE
#
#	Arguments
#	options, optional HASH { all }
#	- all, if set, include the 4 opcos normally excluded
#      
##############################################################################
sub getOperatingCompanies {

	my ( $options ) = @_; # Just one optional arg options eg { all => 1 }
	my $all 		= 0;

	if ( ( defined $options ) && ( ref ( $options ) eq "HASH" ) ) {
		$all = $options->{all} || 0;
	}

	my $sql = qq{ SELECT opcoid, opconame FROM OPERATINGCOMPANY };
	$sql .= " WHERE opcoid NOT IN ( 13, 14, 15, 16, 17 ) " unless $all;
	$sql .= " ORDER BY OPCONAME";

	my $rows = $Config::dba->hashed_process_sql($sql);

	return $rows ? @$rows : ();
} #getOperatingCompanies

# Checklist related functions moved to modules/Checklist.pl

##############################################################################
#       getRemoteOperatingCompanies
#      
##############################################################################
sub getRemoteOperatingCompanies {

	my $myopcoid = $_[0];

	Validate::opcoid($myopcoid); #Validae input.
	
	my $rows = $Config::dba->hashed_process_sql("SELECT oc.opcoid, oc.opconame 
                    								FROM OPERATINGCOMPANY oc
                    								INNER JOIN OUTSOURCEOPERATINGCOMPANY ooc ON ooc.remoteopcoid = oc.opcoid
                    								WHERE ooc.opcoid = ?
                    								ORDER BY oc.opconame ", [ $myopcoid ]);
	return $rows ? @$rows : ();
}

# Lookup up operating company name, returns opco id

sub findOperatingCompany {

	my $opco_name = $_[0];

	Validate::opcoName( $opco_name );

	my ( $opcoid ) = $Config::dba->process_oneline_sql("SELECT oc.opcoid FROM OPERATINGCOMPANY oc WHERE oc.opconame = ?", [ $opco_name ]);

	return $opcoid;
}

# Lookup up customer name, qualified by opco id, returns customer id

sub findCustomer {

	my ( $opco_id, $customer_name ) = @_;

	Validate::opcoid($opco_id);
	Validate::customerName( $customer_name );

	my ( $customer_id ) = $Config::dba->process_oneline_sql("SELECT c.id FROM customers c WHERE c.opcoid = ? AND  c.customer = ?", [ $opco_id, $customer_name ]);

	return $customer_id;
}

##############################################################################
#       getRepoDestinations
##############################################################################
sub getRepoDestinations {

	my $myopcoid = $_[0];

	Validate::opcoid( $myopcoid );
	
	my $rows = $Config::dba->hashed_process_sql("SELECT oc.opcoid, oc.opconame 
                    								FROM OPERATINGCOMPANY oc
                    								INNER JOIN REPODESTINATIONS rd ON rd.toopcoid = oc.opcoid
                    								WHERE rd.fromopcoid = ? 
                    								ORDER BY oc.OPCONAME", [ $myopcoid ] );
	return $rows ? @$rows : ();
}


##############################################################################
#       :getOperatingCompany
#      
##############################################################################
sub getOperatingCompany {
	
	my $opcoid 			= $_[0];
	my $rows 			= undef;;

	Validate::opcoid( $opcoid );
	eval {
		$rows = $Config::dba->hashed_process_sql("SELECT * 
													FROM OPERATINGCOMPANY 
													WHERE opcoid = ? ", [ $opcoid ] );
	}; # end eval
	if ( $@ ) {
		print "Error: $@<br>";
	}
	return $rows ? @$rows : ();
}

##############################################################################
#		updateOperatingCompanies
#      
##############################################################################
sub updateOperatingCompany {

	my ( $opcoid, $opconame, $rootpath, $wippath, $mountpath, $preflightin, $preflightpassed, $preflightfail, $preflightwarning, $ftpserver, 
			$ftpusername, $ftppassword, $bwdef, $bwcroppeddef, $lowrescroppeddef, $lowresdef, $hiresdef, $proofonlydef, $rgbdef, $rgbcroppeddef, 
			$rgbjpegdef, $colourmanagedef, $colourmanage_isactivedef, $optimisedef, $optimise_isactivedef, $pdfcomparedef, $filenameconfig, $pathtype, $jobfolderenabled, 
			$ttpfolder, $preflightprofile, $qcenabled, $filenameoverride, $ftpenabled, $jobassignment, $viewhighres, $creative, $jobpoolassign, $publishing, $domainname,
			$ttpupload, $remotetempfolder, $remotettpfolder, $usejobrepository, $useassetrepository, $repositorysync, $syncstart, $syncend, $adgatesend, 
			$thumbview, $estimateon, $enablelocalupload, $localuploadsource, $localuploaddestination ) = @_;

	Validate::opcoid( $opcoid );
	
	# convert on to 1 and off to 0
	$bwdef 			= ($bwdef eq 'on') ? 1 : 0; 
	$bwcroppeddef 		= ($bwcroppeddef eq 'on') ? 1 : 0; 
	$lowrescroppeddef 	= ($lowrescroppeddef eq 'on') ? 1 : 0; 
	$lowresdef 		= ($lowresdef eq 'on') ? 1 : 0; 
	$hiresdef 		= ($hiresdef eq 'on') ? 1 : 0; 
	$proofonlydef 		= ($proofonlydef eq 'on') ? 1 : 0; 
	$rgbdef 		= ($rgbdef eq 'on') ? 1 : 0; 
	$rgbcroppeddef 		= ($rgbcroppeddef eq 'on') ? 1 : 0; 
	$rgbjpegdef 		= ($rgbjpegdef eq 'on') ? 1 : 0; 
	$colourmanagedef 	= ($colourmanagedef eq 'on') ? 1 : 0; 
	$optimisedef		= ($optimisedef eq 'on') ? 1 : 0;
	$pdfcomparedef 		= ($pdfcomparedef eq 'on') ? 1 : 0; 
	$qcenabled		= ($qcenabled eq 'on') ? 1 : 0;
	$filenameoverride	= ($filenameoverride eq 'on') ? 1 : 0;
	$ftpenabled		= ($ftpenabled eq 'on') ? 1 : 0;
	$jobassignment		= ($jobassignment eq 'on') ? 1 : 0;
	$viewhighres		= ($viewhighres eq 'on') ? 1 : 0;
	$creative		= ($creative eq 'on') ? 1 : 0;
	$jobpoolassign		= ($jobpoolassign eq 'on') ? 1 : 0;
	$publishing		= ($publishing eq 'on') ? 1 : 0;
	$ttpupload		= ($ttpupload eq 'on') ? 1 : 0;
	$usejobrepository	= ($usejobrepository eq 'on') ? 1 : 0;
	$useassetrepository	= ($useassetrepository eq 'on') ? 1 : 0;
	$repositorysync		= ($repositorysync eq 'on') ? 1 : 0;
	$adgatesend		= ($adgatesend eq 'on') ? 1 : 0;
	$thumbview		= ($thumbview eq 'on') ? 1 : 0;
	$estimateon		= ($estimateon eq 'on') ? 1 : 0;
	$jobfolderenabled	= ($jobfolderenabled eq 'on') ? 1 : 0;
	$enablelocalupload	= ($enablelocalupload eq 'on') ? 1 : 0;

	$Config::dba->process_sql( "UPDATE OPERATINGCOMPANY SET 
					opconame = ?,
					rootpath = ?,
					wippath = ?,
					mountpath = ?,
					preflightin = ?,
					preflightpassed = ?,  
					preflightfail = ?, 
					preflightwarning = ?,  
					ftpserver = ?, 
					ftpusername = ?, 
					ftppassword = ?, 
					bwdef = ?,  
					bwcroppeddef = ?,
					lowrescroppeddef = ?,  
					lowresdef = ?,
					hiresdef = ?,					 
					proofonlydef = ?, 
					rgbdef = ?,  
					rgbcroppeddef = ?, 
					rgbjpegdef = ?,  
					colourmanagedef = ?,   
					colourmanage_isactivedef = ?,
					optimisedef = ?,
					optimise_isactivedef = ?,
					pdfcomparedef = ?,  
					filenameconfig	= ?,
					pathtype = ?,
					jobfolderenabled = ?,
					preflight_profile = ?,
					qcenabled = ?,
					filenameoverride = ?,
					ftpenabled = ?,
					jobassignment = ?,
					viewhighres = ?,
					creative = ?,
					jobpoolassign = ?,
					publishing = ?,
					domainname = ?,
					ttpupload = ?,
					remotetempfolder = ?,
					remotettpfolder = ?,
					usejobrepository = ?,
					useassetrepository = ?,
					repositorysync = ?,
					syncstart = ?,
					syncend = ?,
					adgatesend = ?,
					thumbview = ?,
					estimateon = ?,
					enablelocalupload = ?,
					localuploadsource = ?,
					localuploaddestination = ?
					WHERE opcoid = ? ", [ $opconame, $rootpath, $wippath, $mountpath, $preflightin, $preflightpassed, $preflightfail, $preflightwarning, $ftpserver, $ftpusername, $ftppassword, 
											$bwdef, $bwcroppeddef, $lowrescroppeddef, $lowresdef, $hiresdef, $proofonlydef, $rgbdef, $rgbcroppeddef, $rgbjpegdef, $colourmanagedef, 
											$colourmanage_isactivedef, $optimisedef, $optimise_isactivedef, $pdfcomparedef,	$filenameconfig, $pathtype,  $jobfolderenabled, $preflightprofile, 
											$qcenabled, $filenameoverride, $ftpenabled, $jobassignment, $viewhighres, $creative, $jobpoolassign, $publishing, $domainname, 
											$ttpupload, $remotetempfolder, $remotettpfolder, $usejobrepository, $useassetrepository, $repositorysync, 
											$syncstart, $syncend, $adgatesend, $thumbview, $estimateon,
											$enablelocalupload, $localuploadsource, $localuploaddestination, $opcoid ] );

	return 1;
}

##############################################################################
#       addOperatingCompany
#      
##############################################################################
sub addOperatingCompany {

	my ( $opcoid, $opconame, $rootpath, $preflightin, $preflightpassed, $preflightfail, $preflightwarning, $ftpserver, 
			$ftpusername, $ftppassword, $bwdef, $bwcroppeddef, $lowrescroppeddef, $lowresdef, $hiresdef, $proofonlydef, $rgbdef, $rgbcroppeddef, 
			$rgbjpegdef, $colourmanagedef, $colourmanage_isactivedef, $optimisedef, $optimise_isactivedef, $pdfcomparedef, $filenameconfig, $pathtype, 
			$ttpfolder, $preflightprofile, $qcenabled, $filenameoverride, $ftpenabled, $jobassignment, $viewhighres, $creative, $jobpoolassign, $publishing, $domainname  ) = @_;

	# validate input
	Validate::opcoid($opcoid);
	Validate::opcoName( $opconame );
	
	# convert on to 1 and off to 0
	$bwdef 				= ( $bwdef eq 'on' ) ? 1 : 0; 
	$bwcroppeddef 			= ( $bwcroppeddef eq 'on' ) ? 1 : 0; 
	$lowrescroppeddef 		= ( $lowrescroppeddef eq 'on' ) ? 1 : 0; 
	$lowresdef 			= ( $lowresdef eq 'on' ) ? 1 : 0; 
	$hiresdef 			= ( $hiresdef eq 'on' ) ? 1 : 0; 
	$proofonlydef 			= ( $proofonlydef eq 'on') ? 1 : 0; 
	$rgbdef 			= ( $rgbdef eq 'on') ? 1 : 0; 
	$rgbcroppeddef 			= ( $rgbcroppeddef eq 'on') ? 1 : 0; 
	$rgbjpegdef 			= ( $rgbjpegdef eq 'on') ? 1 : 0; 
	$colourmanagedef 		= ( $colourmanagedef eq 'on') ? 1 : 0; 
	$colourmanage_isactivedef	= ( $colourmanage_isactivedef eq 'enabled') ? 1 : 0;
	$optimisedef			= ( $optimisedef eq 'on') ? 1 : 0;
	$optimise_isactivedef		= ( $optimise_isactivedef eq 'enabled') ? 1 : 0;
	$pdfcomparedef 			= ( $pdfcomparedef eq 'on') ? 1 : 0; 
	$qcenabled 			= ( $qcenabled eq 'on') ? 1 : 0; 
	$filenameoverride		= ($filenameoverride eq 'on') ? 1 : 0;
	$ftpenabled 			= ( $ftpenabled eq 'on') ? 1 : 0; 
	$jobassignment 			= ( $jobassignment eq 'on') ? 1 : 0; 
	$viewhighres 			= ( $viewhighres eq 'on') ? 1 : 0; 
	$creative 			= ( $creative eq 'on') ? 1 : 0; 
	$jobpoolassign 			= ( $jobpoolassign eq 'on') ? 1 : 0; 
	$publishing				= ($publishing eq 'on') ? 1 : 0;

	##Added by Feroz - To fix the bug of throwing error of field values not present which is used in edit
	##ttpfolder,  wippath, mountpath are the fields used for 'edit' operating company but not in 'add' operating company
	##But OPERATINGCOMPANY table common for both
	##ttpfolder cant be null hence added '-' as default
	##wippath, mountpath are defaulted to blank
	##This can be adjust while editing the operating company
	$ttpfolder = "-" if ($ttpfolder eq "");

	
	# The following execute is a bit odd, in that it sets rootpath and wippath to "" and mountpath to $rootpath
	# but the assumption is this is all corrected by a subsequent call to updateOperatingCompany SAT 2015-11-25
	$Config::dba->process_sql( "INSERT INTO OPERATINGCOMPANY (opcoid, opconame, rootpath, wippath, mountpath, preflightin, preflightpassed, preflightfail, preflightwarning, ftpserver, 
				ftpusername, ftppassword, bwdef, bwcroppeddef, lowrescroppeddef, lowresdef, hiresdef, proofonlydef, rgbdef, rgbcroppeddef, 
				rgbjpegdef, colourmanagedef, colourmanage_isactivedef, optimisedef, optimise_isactivedef, pdfcomparedef, filenameconfig, pathtype,
				jobfolderenabled, ttpfolder, preflight_profile, 
				qcenabled, filenameoverride, ftpenabled, jobassignment, viewhighres, creative, jobpoolassign, publishing, domainname )
				VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
					?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
					?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
					?, ?, ?, ?, ?, ?, ?, ?, ? )", [ $opcoid, $opconame, "", "", $rootpath, $preflightin, $preflightpassed, $preflightfail, $preflightwarning, $ftpserver, 
				$ftpusername, $ftppassword, $bwdef, $bwcroppeddef, $lowrescroppeddef, $lowresdef, $hiresdef, $proofonlydef, $rgbdef, $rgbcroppeddef, 
				$rgbjpegdef, $colourmanagedef, $colourmanage_isactivedef, $optimisedef, $optimise_isactivedef, $pdfcomparedef, $filenameconfig, $pathtype, 0,
				$ttpfolder, $preflightprofile, $qcenabled, $filenameoverride, $ftpenabled, $jobassignment, $viewhighres, $creative, $jobpoolassign, $publishing, $domainname ] );

	return 1;
}


##############################################################################
# getOpcoName
##############################################################################
sub getOpcoName {

	my $opcoid		= $_[0];
	
	Validate::opcoid( $opcoid );	

	my ( $name ) = $Config::dba->process_oneline_sql("SELECT opconame FROM OPERATINGCOMPANY WHERE opcoid = ?", [ $opcoid ] );

	return $name;
}

##############################################################################
# db_getDomainName
##############################################################################
sub db_getDomainName {

	my $opcoid	= $_[0];

	Validate::opcoid( $opcoid );

	my ( $domainname ) = $Config::dba->process_oneline_sql("SELECT domainname FROM OPERATINGCOMPANY WHERE opcoid = ?", [ $opcoid ]);

	return $domainname;
}

##############################################################################
#       getOperartingCompanyVendorCompanies
##############################################################################
sub getOperatingCompanyVendorCompanies {
	
	my $opcoid	= $_[0];	
	
	Validate::opcoid( $opcoid );

	my $rows = $Config::dba->hashed_process_sql("SELECT c.id 	
													FROM COMPANIES c
													INNER JOIN OPERATINGCOMPANYVENDOR ocv 
													ON ocv.companyid = c.id 
													WHERE ocv.opcoid = ?", [ $opcoid ]);
	return $rows ? @$rows : ();
}

##############################################################################
#       getOperartingCompanyVendors
#      
##############################################################################
sub getOperatingCompanyVendors {
	
	my $opcoid	= $_[0];	
	Validate::opcoid( $opcoid );	

	my $rows = $Config::dba->hashed_process_sql("SELECT c.id, c.name, u.email
												FROM USERS u
												INNER JOIN COMPANIES c ON c.id = u.companyid
												INNER JOIN OPERATINGCOMPANYVENDOR ocv ON ocv.companyid = c.id 
												WHERE u.status != 0
												AND u.role = 2 
												AND ocv.opcoid = ?
												ORDER BY c.name", [ $opcoid ]);
	return $rows ? @$rows : ();
}






##############################################################################
#       updateOperatingCompanyVendors()
#
##############################################################################
sub updateOperatingCompanyVendors {

	my ( $opcoid, $selected ) = @_;

	Validate::opcoid( $opcoid );	
	
	#delete existing rows
	$Config::dba->process_sql("DELETE FROM OPERATINGCOMPANYVENDOR 
				WHERE opcoid = ?", [ $opcoid ]);

	foreach my $v ( @{$selected} ) {
		#delete existing rows
		$Config::dba->process_sql("INSERT INTO OPERATINGCOMPANYVENDOR ( opcoid, companyid) VALUES ( ?, ? )", [ $opcoid, $v ] );
	}

	return 1;
}


##############################################################################
#       opcoidExists()
#
##############################################################################
sub opcoidExists {

	my $opcoid			= $_[0];
	my $numopcos 		= 0;

	Validate::opcoid($opcoid);

	( $numopcos ) = $Config::dba->process_oneline_sql("SELECT count(*) AS numopcos FROM OPERATINGCOMPANY WHERE opcoid = ?", [ $opcoid ] );

	return $numopcos;
}


##############################################################################
#       db_isQCEnabled()
#
##############################################################################
sub db_isQCEnabled {

	my $opcoid			= $_[0];
	my $qcenabled		= 0;

	Validate::opcoid($opcoid);

	( $qcenabled ) = $Config::dba->process_oneline_sql("SELECT qcenabled FROM OPERATINGCOMPANY WHERE opcoid = ?", [ $opcoid ] );

	return $qcenabled;
}

##############################################################################
#       db_isRepoSyncEnabled()
#	For this CUSTOMER is the repoSync Enabled?
##############################################################################
sub db_isRepoSyncEnabled {

	my $customerid			= $_[0];
	my $syncenabled 		= 0;

	Validate::customerid($customerid);

	( $syncenabled ) = $Config::dba->process_oneline_sql("SELECT REPOSITORYSYNC FROM CUSTOMERS WHERE ID = ? ", [ $customerid ] );

	return $syncenabled;
} # end isRepoSyncEnabled

##############################################################################
#       db_usejobrepository()
# 	Does this opco use the job repository?
##############################################################################
sub db_usejobrepository {

	my $opcoid = $_[0];

	Validate::opcoid( $opcoid );

	my ( $usejobrepo ) = $Config::dba->process_oneline_sql("SELECT usejobrepository FROM operatingcompany WHERE opcoid=?", [ $opcoid ]);

	return $usejobrepo;
}



##############################################################################
#       db_adgateSend()
# 	Does this opco use the adgate?
##############################################################################
sub db_adgateSend {

	my $opcoid			= $_[0];

	Validate::opcoid( $opcoid );

	my ( $adgate ) = $Config::dba->process_oneline_sql("SELECT ADGATESEND FROM OPERATINGCOMPANY WHERE opcoid = ?", [ $opcoid ] );

	return $adgate;
}

##############################################################################
#       db_useassetrepository()
# 	Does this opco use the asset repository?
##############################################################################
sub db_useassetrepository {

	my $opcoid			= $_[0];

	Validate::opcoid( $opcoid );

	my ( $useassetrepo ) = $Config::dba->process_oneline_sql("SELECT USEASSETREPOSITORY FROM OPERATINGCOMPANY WHERE opcoid = ?", [ $opcoid ] );

	return $useassetrepo;
}


##############################################################################
#       db_useassetrepositorycustomer()
# 	Does this customer use the asset repository?
##############################################################################
sub db_useassetrepositorycustomer {

	my $customerid			= $_[0];
	my $useassetrepo 		= 0;

	eval {

		Validate::customerid( $customerid );

		( $useassetrepo ) = $Config::dba->process_oneline_sql( "SELECT USEASSETREPOSITORY FROM CUSTOMERS WHERE id = ?", [ $customerid ] );
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "db_useassetrepositorycustomer: $error";
	}

	return $useassetrepo;
}

##############################################################################
#       db_creativeenabled()
#
##############################################################################
sub db_creativeenabled {

	my $opcoid					= $_[0];
	my $creativeenabled 		= 0;

	Validate::opcoid( $opcoid );

	( $creativeenabled ) = $Config::dba->process_oneline_sql( "SELECT creative FROM OPERATINGCOMPANY WHERE opcoid = ?", [ $opcoid ] );

	return $creativeenabled;
}

##############################################################################
#       db_isFTPEnabled()
#
##############################################################################
sub db_isFTPEnabled {

	my $opcoid					= $_[0];
	my $ftpenabled 				= 0;

	Validate::opcoid( $opcoid );

	( $ftpenabled ) = $Config::dba->process_oneline_sql( "SELECT ftpenabled FROM OPERATINGCOMPANY WHERE opcoid = ?", [ $opcoid ] );

	return $ftpenabled;

} 

##############################################################################
#       db_getFilenameoverride()
#
##############################################################################
sub db_getFilenameoverride {

	my $opcoid					= $_[0];
	my $filenameoverride 		= 0;

	Validate::opcoid( $opcoid );

	( $filenameoverride ) = $Config::dba->process_oneline_sql( "SELECT filenameoverride FROM OPERATINGCOMPANY WHERE opcoid = ?", [ $opcoid ] );

	return $filenameoverride;
}

##############################################################################
#       db_getUserOpcos()
#
##############################################################################
sub db_getUserOpcos {

	my $userid					= $_[0];

	Validate::userid( $userid );
	
	my $rows = $Config::dba->hashed_process_sql("SELECT oc.opcoid, oc.opconame 
				FROM OPERATINGCOMPANY oc
				INNER JOIN USEROPCOS uo ON oc.opcoid = uo.opcoid
				WHERE uo.userid = ?
				ORDER BY oc.opconame", [ $userid ] );


	return $rows ? @$rows : ();
	
}


##############################################################################
# getOperatingCompanyFilenameTemplate
##############################################################################
sub getOperatingCompanyFilenameTemplate {

	my $opcoid		= $_[0];

	Validate::opcoid( $opcoid );

	my ( $filename ) = $Config::dba->process_oneline_sql( "SELECT filenameconfig FROM OPERATINGCOMPANY WHERE opcoid = ? ", [ $opcoid ] );

	return $filename;
}


##############################################################################
#       db_updateFTPServer()
# Update ftpserver field fofr EACH row on operatingcompany
# - mistake surely but apparently not .... see PD-3353
##############################################################################
sub db_updateFTPServer {

	my $ftpserver	= $_[0];

	$Config::dba->process_sql( "UPDATE OPERATINGCOMPANY SET FTPSERVER = ? ", [ $ftpserver ] );

	return 1;
}

##############################################################################
#       Customer Related Functions 
##############################################################################
##############################################################################
#       db_customerValid()
##############################################################################

sub customerValid {

	my $opcoid				= $_[0];
	my $customername		= $_[1];
	my $isCustomerValid 	= 0;

	Validate::opcoid( $opcoid );
	Validate::customerName( $customername );

	my ( $rows ) = $Config::dba->process_oneline_sql( "SELECT AGENCY FROM TRACKER3 WHERE opcoid = ? AND AGENCY = ? LIMIT 1 ", [ $opcoid, $customername ] );

	if ( $rows ) {
		$isCustomerValid = 1;
	}

	return $isCustomerValid;
} 



##############################################################################
# customerExists
##############################################################################
sub customerExists {

	my $opcoid				= $_[0];
	my $customername		= $_[1];
	my $customerExists 		= 0;

	Validate::opcoid( $opcoid );
	Validate::customerName( $customername );
	
	my ( $rows ) = $Config::dba->process_oneline_sql( "SELECT customer FROM CUSTOMERS WHERE opcoid = ? AND customer = ? ", [ $opcoid, $customername ] );

	if ( $rows ) {
		$customerExists = 1;	
	}

	return $customerExists;
}


##############################################################################
# getCustomerName
##############################################################################
sub getCustomerName {

	my $customerid			= $_[0];

	Validate::customerid($customerid);

	my ( $customerName, $customerlogo ) = $Config::dba->process_oneline_sql( "SELECT customer,customerlogo FROM CUSTOMERS WHERE id = ? ", [ $customerid ] );

	return $customerName,$customerlogo;
}

##############################################################################
# getCustomerName
##############################################################################
sub getCustomerID {

	my $custname  			= $_[0];
	my $opcoid  			= $_[1];

	Validate::opcoid( $opcoid );
	Validate::customerName( $custname );

	my ( $custid ) = $Config::dba->process_oneline_sql( "SELECT id FROM CUSTOMERS WHERE customer = ? and opcoid = ? ", [ $custname, $opcoid ] );

	return $custid;
}

##############################################################################
# getMedianame
##############################################################################
sub getMedianame {

    my $mediaid  = $_[0];

    my ( $medianame );

    # validate input
    if ( $mediaid && (! looks_like_number( $mediaid )) ) {
        return $medianame;
    }

    ( $medianame ) = $Config::dba->process_oneline_sql( "SELECT type FROM MEDIATYPES WHERE id = ? ", [ $mediaid ] );

    return $medianame;
}


##############################################################################
# getCustomerFilenameTemplate
##############################################################################
sub getCustomerFilenameTemplate {

	my $customerid			= $_[0];

	Validate::customerid($customerid);

	my ( $filename ) = $Config::dba->process_oneline_sql( "SELECT filename FROM CUSTOMERS WHERE id = ? ", [ $customerid ] );

	return $filename;
}


##############################################################################
# getCustomerJobPoolAssign
##############################################################################
sub getCustomerJobPoolAssign {

	my $customerid			= $_[0];

	Validate::customerid($customerid);
	
	my ( $jobpoolassign ) = $Config::dba->process_oneline_sql( "SELECT jobpoolassign FROM CUSTOMERS WHERE id = ? ", [ $customerid ] );

	return $jobpoolassign;
}

##############################################################################
# getCustomerEstimateOn
##############################################################################
sub getCustomerEstimateOn {

	my $customerid  		= $_[0];

	Validate::customerid($customerid);

	my ( $estimateon ) = $Config::dba->process_oneline_sql( "SELECT estimateon FROM CUSTOMERS WHERE id = ? ", [ $customerid ] );
	
	return $estimateon;
}


##############################################################################
# getCustomerPDFCompareDef
##############################################################################
sub getCustomerPDFCompareDef {

	my $customerid  = $_[0];

	Validate::customerid($customerid);

	my ( $pdfcomparedef ) = $Config::dba->process_oneline_sql( "SELECT pdfcomparedef FROM CUSTOMERS WHERE id = ? ", [ $customerid ] );
	
	return $pdfcomparedef;
}

##############################################################################
# getCustomerPFCDisabled
##############################################################################
sub getCustomerPFCDisabled {

	my $customerid  = $_[0];

	Validate::customerid($customerid);

	my ( $pfc_disable ) = $Config::dba->process_oneline_sql( "SELECT pfc_disable FROM CUSTOMERS WHERE id = ? ", [ $customerid ] );

	return $pfc_disable;
}


##############################################################################
# getOpcoEstimateOn
##############################################################################
sub getOpcoEstimateOn {

	my $opcoid  = $_[0];

	Validate::opcoid($opcoid);
	
	my ( $pfc_disable ) = $Config::dba->process_oneline_sql( "SELECT estimateon FROM OPERATINGCOMPANY WHERE opcoid = ? ", [ $opcoid ] );

	return $pfc_disable;
}

##############################################################################
# getAutoCheckin
##############################################################################
sub getCustomerAutoCheckin
{
	my $customerid = $_[0];
	
	Validate::customerid($customerid);

	my ( $autocheckin ) = $Config::dba->process_oneline_sql( "SELECT autocheckin FROM CUSTOMERS WHERE id = ? ", [ $customerid ] );
	
	return $autocheckin;
}

##############################################################################
# updateCustomer
#
# ECMDBOpcos::updateCustomer( $id, $filename, $syncstart, $syncend, $estimate, $documentmaxsize, $digitalAssetMaxSize, $mediaid, \%settings, $customerLogo );
#
# where settings is a hash, with key = column name and value = 0 or 1 (for the checkbox fields, which are TINYINT columns)
#
##############################################################################
sub updateCustomer{

	my ( $customerid, $filename, $syncstart, $syncend, $estimate, $docMaxSize, $digitalUpldMaxSize, $mediaid, $settings, $customerlogo, ) = @_;

	eval {
		# validate inputs ( $customerid, $filename, $syncstart, $syncend, $estimate, $docMaxSize, $digitalUpldMaxSize, $mediaid, $settings )

		Validate::customerid( $customerid );

		# filename - just a filename or blank, VARCHAR(100)

		# Sync start and end are displayed at hh:mm on the panel, but in reality they are stored as a TINYINT(4)
		# so the fields accept a number 0..23 (although there is no range checking)
		$syncstart		= 0 unless defined $syncstart;
		$syncend		= 0 unless defined $syncend;

		# $estimate		= ? # FLOAT(6, 2) column

		$docMaxSize		= $docMaxSize || 10; # Can be "" so set to default 10 Mb if blank or null
		$digitalUpldMaxSize	= $digitalUpldMaxSize || 10; 

		# $mediaid ?? should be foreign key to mediatypes

		$settings = { } unless defined $settings;

		my $sql	= undef;
		my @values = ();

		if($customerlogo ne "")
		{
			$sql = "UPDATE CUSTOMERS SET filename = ?, syncstart = ?, syncend = ?, docmaxsize = ?, digitalmaxsize = ?, lastupdmedia = ?, customerlogo = ?";
			@values = ( $filename, $syncstart, $syncend, $docMaxSize, $digitalUpldMaxSize, $mediaid, $customerlogo );
		}
		else
		{
			$sql = "UPDATE CUSTOMERS SET filename = ?, syncstart = ?, syncend = ?,  docmaxsize = ?, digitalmaxsize = ?, lastupdmedia = ?";
                        @values = ( $filename, $syncstart, $syncend, $docMaxSize, $digitalUpldMaxSize, $mediaid );
		}


		# Add the checkbox settings
		foreach my $col ( keys %$settings ) {
			my $val = $settings->{ $col };
			$sql = $sql . ", $col = ?";
			push( @values, $val );
		}
		# and the WHERE clause
		$sql = $sql . " WHERE id = ?";
		push( @values, $customerid );

		$Config::dba->process_oneline_sql( $sql, \@values );
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "updateCustomer: $error";
		return 0;
	}
	return 1;
}




##############################################################################
# getCustomers
##############################################################################
sub getCustomers {

	my $rows = $Config::dba->hashed_process_sql("select c.id, oc.opconame, c.customer 
			from CUSTOMERS c
			INNER JOIN OPERATINGCOMPANY oc ON oc.opcoid = c.opcoid
			LIMIT 5000");
	return $rows ? @$rows : ();
}

##############################################################################
# addCustomer
##############################################################################
sub addCustomer {

	my $opcoid			= $_[0];
	my $customername	= $_[1];

	Validate::opcoid($opcoid);	
	Validate::customerName( $customername );

	$Config::dba->process_sql("INSERT INTO CUSTOMERS (opcoid, customer, filename, jobpoolassign, autocheckin, rec_timestamp) VALUES (?, ?, '', 0, 0, now())", [ $opcoid, uc($customername) ] );

	return 1;
}

##############################################################################
# getCustomerOpcoid
##############################################################################
sub getCustomerOpcoid {

	my $customerid		= $_[0];
	my $opcoid			= 0;
	
	Validate::customerid($customerid);	

	( $opcoid ) = $Config::dba->process_oneline_sql("SELECT opcoid FROM CUSTOMERS WHERE id = ? ", [ $customerid ] );

	return $opcoid;
}

##############################################################################
# addCustomerFTP
##############################################################################
sub addCustomerFTP {

	my $customerid	= $_[0];
	my $label 		= $_[1]; 
	my $level1 		= $_[2];
	my $level2		= $_[3]; 
	my $level3 		= $_[4]; 
	my $username 	= $_[5]; 
	my $password 	= $_[6]; 
	my $domainname 	= $_[7];

	# validate input
	Validate::customerid($customerid);
	
	$Config::dba->process_sql("INSERT INTO CUSTOMERFTP (customerid, label, level1, level2, level3, username, password, domainname, rec_timestamp) 
					VALUES (?, ?, ?, ?, ?, ?, ?, ?, now()) ", [ $customerid, $label, $level1, $level2, $level3, $username, $password, $domainname ] );

	return 1;
}

##############################################################################
# updateCustomerFTP
##############################################################################
sub updateCustomerFTP {

	my $ftpid		= $_[0];
	my $label 		= $_[1]; 
	my $level1 		= $_[2];
	my $level2		= $_[3]; 
	my $level3 		= $_[4]; 
	my $username 	= $_[5]; 
	my $password 	= $_[6]; 
	my $domainname 	= $_[7];

	# validate input
	if ( $ftpid && (! looks_like_number( $ftpid )) ) {
		return 0;
	}
	
	$Config::dba->process_sql("UPDATE CUSTOMERFTP SET label = ?, 
					level1 = ?, level2 = ?, level3 = ?, username = ?, password = ?, domainname = ?
					WHERE id = ? ", [ $label, $level1, $level2, $level3, $username, $password, $domainname, $ftpid ] );

	return 1;
}



##############################################################################
# getCustomerFTPs
##############################################################################
sub getCustomerFTPs {

	my $customerid 		= $_[0];
	Validate::customerid($customerid);
	
	my $rows = $Config::dba->hashed_process_sql("SELECT id, label, level1, username, domainname FROM CUSTOMERFTP WHERE customerid = ?	LIMIT 500", [ $customerid ] );

	return $rows ? @$rows : ();
}

##############################################################################
# getCustomerFileoptions
##############################################################################
sub getCustomerFileoptions {

	my $customerid 		= $_[0];
	Validate::customerid($customerid);

	my ( $additionalfiletypes, $bw_asset, $bw_crp_asset, $lr_asset, $lr_crp_asset, $rgb_asset, $rgb_crp_asset, $rgb_jpg_asset, $hires_crp_bleedbox_asset, $hires_crp_trimbox_asset,  
		$aux_files, $split_pages, $open_assets, $high_res, $bwdef, $bwcroppeddef, $lowrescroppeddef, $lowresdef, $hiresdef, $proofonlydef, $rgbdef, $rgbcroppeddef, 
		$rgbjpegdef, $hirescroppedbleedboxdef, $hirescroppedtrimboxdef, $docMaxSizedef, $digitalMaxSize, $zip_dir, $es_compare, $enableproducts,$enabledelete, $LASTUPDMEDIA,$customerLogo, $enable_one2edit ); 

	my $rows = $Config::dba->hashed_process_sql("SELECT additionalfiletypes, bw_asset, bw_crp_asset, lr_asset, lr_crp_asset, rgb_asset, rgb_crp_asset, rgb_jpg_asset, 
				hires_crp_bleedbox_asset, hires_crp_trimbox_asset, 
				aux_files, split_pages, open_assets, high_res, bwdef, bwcroppeddef, lowrescroppeddef, lowresdef, hiresdef, 
				proofonlydef, rgbdef, rgbcroppeddef, rgbjpegdef,
				hirescroppedbleedboxdef, hirescroppedtrimboxdef, docmaxsize, digitalmaxsize, zip_dir, es_compare, enableproducts,enabledelete,LASTUPDMEDIA,customerlogo,
				enable_one2edit FROM CUSTOMERS WHERE id = ? ", [ $customerid ] );

	return $rows ? @$rows : ();
}

##############################################################################
# getCustomerAddressees
##############################################################################
sub getCustomerAddressees {

	my $customerid 		= $_[0];
	Validate::customerid($customerid);
	
	my $rows = $Config::dba->hashed_process_sql("SELECT id, email FROM CUSTOMERADDRESSES WHERE customerid = ?", [ $customerid ] );
	return $rows ? @$rows : ();
}

##############################################################################
# getCustomerRepositoryOptions
##############################################################################

sub getCustomerRepositoryOptions {

	my $customerid 		= $_[0];
	Validate::customerid($customerid);
	my ( $usejobrepository, $repoautoarchive, $useassetrepository, $repositorysync, $syncstart, $syncend, $enhancedftp );
	my ( @repoOptions );

	
	( $usejobrepository, $repoautoarchive, $useassetrepository, $repositorysync, $syncstart, $syncend, $enhancedftp ) = $Config::dba->process_oneline_sql("SELECT USEJOBREPOSITORY, REPOAUTOARCHIVE, USEASSETREPOSITORY, REPOSITORYSYNC, SYNCSTART, SYNCEND, ENHANCEDFTP FROM CUSTOMERS WHERE id = ? ", [ $customerid ] );

	push ( @repoOptions, $usejobrepository );
	push ( @repoOptions, $repoautoarchive );
	push ( @repoOptions, $useassetrepository );
	push ( @repoOptions, $repositorysync );
	push ( @repoOptions, $syncstart );
	push ( @repoOptions, $syncend );
	push ( @repoOptions, $enhancedftp );

	return @repoOptions;
}

##############################################################################
# getCustomerRecipientEmail
##############################################################################
sub getCustomerRecipientEmail {

	my $recipientId 	= $_[0];

	my ( $email )	= $Config::dba->process_oneline_sql("SELECT email FROM CUSTOMERADDRESSES WHERE id = ? ", [ $recipientId ] );

	return $email;
}

##############################################################################
# getCustomerFTP
##############################################################################
sub getCustomerFTP {

	my $ftpid 	= $_[0];
	
	my $rows = $Config::dba->hashed_process_sql("SELECT id, label, level1, level2, level3, username, password, domainname FROM CUSTOMERFTP WHERE id = ? ", [ $ftpid ] );

	return $rows ? @$rows : ();
}

##############################################################################
# updateCustomerAddresses
##############################################################################
sub updateCustomerAddresses {

	my ($id_ref, $addresses_ref) = @_;

	$Config::dba->process_sql("DELETE FROM CUSTOMERADDRESSES WHERE customerid = ?", [ $$id_ref ] );

	foreach ( @$addresses_ref ) {
		#print "add $_<br>";
		if ( length($_) > 0 ) {
			$Config::dba->process_sql("INSERT INTO CUSTOMERADDRESSES ( customerid, email) VALUES ( ?, ? )", [ $$id_ref, $_ ] );
		}
	}

	return 1;	
}

##############################################################################
# getCustomerAddresses
##############################################################################
sub getCustomerAddresses {

	my $id 		= $_[0];
	Validate::customerid($id);
	
	my $rows = $Config::dba->hashed_process_sql("SELECT email FROM CUSTOMERADDRESSES WHERE customerid = ? ", [ $id ] );
	return $rows ? @$rows : ();
}

##############################################################################
# getCustomerId
##############################################################################
sub getCustomerId {

	my $jobid 		= $_[0];
	my $opcoid		= $_[1];
	my $opcoidvalue;
	Validate::job_number( $jobid );
	if( $opcoid ) {
		Validate::opcoid( $opcoid );
		$opcoidvalue = $opcoid;
	}else{
		$opcoidvalue = "(SELECT opcoid FROM TRACKER3PLUS WHERE JOB_NUMBER = $jobid)";
	}
	my ( $customerId ) = $Config::dba->process_oneline_sql("SELECT id FROM CUSTOMERS WHERE customer = 
															(SELECT AGENCY FROM TRACKER3 WHERE JOB_NUMBER = ?) 
															AND OPCOID = $opcoidvalue", [ $jobid ] );
	return $customerId;
}

##############################################################################
#  customerJobRepositoryEnabled
##############################################################################
sub customerJobRepositoryEnabled {

	my $customerId		= $_[0];
	my $enabled			= 0;
	Validate::customerid($customerId);
	( $enabled ) = $Config::dba->process_oneline_sql("SELECT usejobrepository FROM CUSTOMERS WHERE id = ? ", [ $customerId ] );

	return $enabled;
}

##############################################################################
# getCustomerAgencyRefAsFilename
##############################################################################
sub getCustomerAgencyRefAsFilename {
	my $customerid 		= $_[0];
	my $araf			= undef;					#AgencyRefAsFilename

	Validate::customerid($customerid);	
	
	( $araf ) = $Config::dba->process_oneline_sql("SELECT useagencyrefasfilename FROM CUSTOMERS WHERE id = ? ", [ $customerid ] );
	
	return $araf;
}	

##############################################################################
# isJobAssignment
##############################################################################

sub isJobAssignment {

	my $opcoid 			= $_[0];
	my $jobassignment 	= 0;

	Validate::opcoid($opcoid);
	
	( $jobassignment ) = $Config::dba->process_oneline_sql("SELECT JOBASSIGNMENT FROM OPERATINGCOMPANY WHERE opcoid = ?", [ $opcoid ] );

	return $jobassignment;
}

##############################################################################
## isLocalUpload
##############################################################################
sub isLocalUpload {

    my $opcoid      		= $_[0];
    my $localupload   		= 0;

	Validate::opcoid($opcoid);

    ( $localupload ) = $Config::dba->process_oneline_sql("SELECT ENABLELOCALUPLOAD FROM OPERATINGCOMPANY WHERE opcoid = ? ", [ $opcoid ] );

    return $localupload;
}

##############################################################################
# db_getRelaxRoles
#
##############################################################################
sub db_getRelaxRoles {

    my $opcoid = $_[0];
	Validate::opcoid($opcoid);

	my ($relaxroles) = $Config::dba->process_oneline_sql("SELECT relaxroles FROM operatingcompany WHERE OPCOID=?", [ $opcoid ]);

    return $relaxroles;
}


##############################################################################
# isPublishing
##############################################################################
sub isPublishing {
	my ( $opcoid ) = @_;

	Validate::opcoid($opcoid);

    my ( $publishing ) = $Config::dba->process_oneline_sql("SELECT publishing FROM operatingcompany WHERE OPCOID=? ", [ $opcoid ] );
	$publishing = 0 unless defined $publishing;   
 
    return $publishing;
}

sub isArchant {
	my ( $opcoid ) = @_;

	Validate::opcoid($opcoid);

	my ( $opconame ) = $Config::dba->process_oneline_sql("SELECT OPCONAME FROM operatingcompany WHERE OPCOID=?", [ $opcoid ] );
	$opconame = '' unless defined $opconame;

	##Modified to display the advert fields whenever we found operating company name ARCHANT in any of the OPCONAME
	return ( $opconame =~ /Archant/i ) ? 0 : 1;
}

##############################################################################
# agencyrefasfilename? 
# do we use the agency ref as filename
##############################################################################
sub agencyrefasfilename {
	my ( $opcoid, $customer ) = @_;

	Validate::opcoid($opcoid);
	
	my ( $agencyrefasfilename ) = $Config::dba->process_oneline_sql("SELECT  c.useagencyrefasfilename 
																	FROM CUSTOMERS c
																	WHERE c.OPCOID = ?
																	AND c.customer = ? ", [ $opcoid, $customer ] );
	return $agencyrefasfilename;
}

##############################################################################
# enhanced ftp? 
# does this customer use the enhanced FTP mechanism? e.g. (H&M)
##############################################################################
sub isEnhancedFTP {

	my $customerid 			= $_[0];
	# my $enhancedftp			= 0;

	Validate::customerid($customerid);
	
	my ( $enhancedftp ) = $Config::dba->process_oneline_sql("SELECT  c.enhancedftp 
															FROM CUSTOMERS c
															WHERE c.id = ? ", [ $customerid ] );
	return $enhancedftp;
}

#####################################################################################
# getUploadTempFolder 
# What is the remote temp folder?
#####################################################################################
sub getUploadTempFolder {

	my $jobid 				= $_[0];

	Validate::job_number( $jobid );
	
	my ( $folder ) = $Config::dba->process_oneline_sql("SELECT REMOTETEMPFOLDER 
														FROM OPERATINGCOMPANY
														WHERE opcoid = ( SELECT opcoid FROM TRACKER3 WHERE JOB_NUMBER = ? )", [ $jobid ] );
	return $folder;
}

#####################################################################################
# getUploadDropFolder 
# What is the remote drop folder?
#####################################################################################
sub getUploadDropFolder {

	my $jobid 				= $_[0];

	Validate::job_number( $jobid );
	
	my ( $folder ) = $Config::dba->process_oneline_sql("SELECT REMOTETTPFOLDER 
														FROM OPERATINGCOMPANY
														WHERE opcoid = ( SELECT opcoid FROM TRACKER3 WHERE JOB_NUMBER = ? )", [ $jobid ] );
	return $folder;
}

##############################################################################
#       db_thumbview()
# 	Does this opco use the thumbs view?
##############################################################################
sub db_thumbview {

	my $opcoid				= $_[0];
	my $thumbview 			= 0;

	Validate::opcoid($opcoid);

	( $thumbview ) = $Config::dba->process_oneline_sql("SELECT THUMBVIEW FROM OPERATINGCOMPANY WHERE opcoid = ?", [ $opcoid ] );

	return $thumbview;
}


##############################################################################
#       db_isOpcoImageWorkFlow()
# 	Does this opco use Image Workflow?
##############################################################################
sub db_isOpcoImageWorkFlow {

    my $opcoid				= $_[0];
	
	Validate::opcoid($opcoid);

	my ( $retval ) = $Config::dba->process_oneline_sql("SELECT COUNT(1) FROM imageworkflow WHERE opcoid = ?", [ $opcoid ] );

	return $retval;
}

  
##############################################################################
#       db_getOpcoWIP()
# 
##############################################################################

sub db_getOpcoWIP {

    my $opcoid				= $_[0];

	Validate::opcoid($opcoid);
	
	my ( $wippath ) = $Config::dba->process_oneline_sql("SELECT wippath
														FROM OPERATINGCOMPANY 
														WHERE opcoid = ?", [ $opcoid ] );
	return $wippath;
}

##############################################################################
# 	getDocMaxSizes()
##############################################################################
sub getDocMaxSizes {

	my $rows = $Config::dba->hashed_process_sql("SELECT docmaxsize FROM T_CUSTDOCSIZE_MAPPING");

	return \@$rows;
}

##############################################################################
# db_getdepartments
##############################################################################
sub db_getdepartments {
	my $rows = $Config::dba->hashed_process_sql("SELECT id,name FROM departments order by name asc");
	return $rows ? @$rows : ();
}

##############################################################################
# db_getteams
##############################################################################
sub db_getteams {

    my $rows = $Config::dba->hashed_process_sql("SELECT id, name FROM teams");

    return $rows ? @$rows : ();
}

##############################################################################
# db_updatedepartmentdetails
##############################################################################
sub db_updatedepartmentdetails {

	my $opcoid			=	$_[0];
	my $departmentid	=	$_[1];
	my $checked			=	$_[2];

	Validate::opcoid($opcoid);

	my ($alreadypresent) = $Config::dba->process_oneline_sql("select 1 from departmentdetails where opcoid=? and departmentid=? ", [ $opcoid, $departmentid ] );

	if ( $alreadypresent ) {
		$Config::dba->process_sql("UPDATE departmentdetails SET checked=?, rec_timestamp=now()  where opcoid=? and departmentid=?", [ $checked, $opcoid, $departmentid ]);
	} else {
		$Config::dba->process_sql("INSERT into  departmentdetails ( opcoid, departmentid, checked, rec_timestamp ) values ( ?, ?, ?, now() ) ", [ $opcoid, $departmentid, $checked ]);
	}
}

##############################################################################
# db_updateteamdetails
##############################################################################
sub db_updateteamdetails {

    my $opcoid          =   $_[0];      
    my $teamid    		=   $_[1];      
    my $checked         =   $_[2];     

	Validate::opcoid($opcoid);

    my ($alreadypresent) = $Config::dba->process_oneline_sql("select 1 from teamdetails where opcoid=? and teamid=? ", [ $opcoid, $teamid ] );

    if ( $alreadypresent ) {
        $Config::dba->process_sql("UPDATE teamdetails SET checked=?, rec_timestamp=now()  where opcoid=? and teamid=?", [ $checked, $opcoid, $teamid ]);
    } else {
        $Config::dba->process_sql("INSERT into  teamdetails ( opcoid, teamid, checked, rec_timestamp ) values ( ?, ?, ?, now() ) ", [ $opcoid, $teamid, $checked ]);
    }
}

##############################################################################
# db_getdepartmentstatus
##############################################################################
sub db_getdepartmentstatus {

	my $opcoid			=	$_[0];

	Validate::opcoid($opcoid);

	my $rows = $Config::dba->hashed_process_sql("SELECT d1.name, d1.id, d2.departmentid, d2.checked FROM departments d1 LEFT OUTER JOIN departmentdetails d2 ON d1.id = d2.departmentid and d2.opcoid=? order by d1.name asc",[ $opcoid ] );

    return $rows ? @$rows : ();

}

##############################################################################
# db_getteamstatus
##############################################################################
sub db_getteamstatus {

    my $opcoid          =   $_[0];

	#Validate::opcoid($opcoid);

    my $rows = $Config::dba->hashed_process_sql("SELECT d1.name, d1.id, d2.teamid, d2.checked FROM teams d1 LEFT OUTER JOIN teamdetails d2 ON d1.id = d2.teamid and d2.opcoid=? order by d1.name asc",[ $opcoid ] );

    return $rows ? @$rows : ();

}

##############################################################################
## db_getquerytypes
###############################################################################
sub db_getquerytypes {

    my $opcoid          =   $_[0];

	#Validate::opcoid($opcoid);

    my $rows = $Config::dba->hashed_process_sql("SELECT id, querytype as name  FROM querytypes order by querytype asc");

    return $rows ? @$rows : ();

}

##############################################################################
## db_getquerytype
###############################################################################
sub db_getquerytype {

    my $opcoid          =   $_[0];

	Validate::opcoid($opcoid);

    my $rows = $Config::dba->hashed_process_sql("SELECT id, querytype  FROM querytypes WHERE id != 0 order by querytype asc ");

    return $rows ? @$rows : ();

}

##############################################################################
# db_deletedeptbyopcoid
##############################################################################
sub db_deletedeptbyopcoid {

	my $opcoid			=	$_[0];

	Validate::opcoid($opcoid);

	$Config::dba->process_sql("DELETE FROM departmentdetails WHERE opcoid=?", [ $opcoid ] );
}

##############################################################################
# db_deleteteambyopcoid
##############################################################################
sub db_deleteteambyopcoid {

    my $opcoid          =   $_[0];

	Validate::opcoid($opcoid);

    $Config::dba->process_sql("DELETE FROM teamdetails WHERE opcoid=?", [ $opcoid ] );
}

##############################################################################
#   getdigitalSizeLimit(): Used to get all size range list.
##############################################################################
sub getdigitalSizeLimit {

    my $rows = $Config::dba->hashed_process_sql("SELECT size_range FROM custdigital_size_mapping");
	return \@$rows;

}#getdigitalSizeLimit

#############################################################################
#       db_getTTPFolder() - get the TTP folder for the destination of this
# 		job 
#
##############################################################################
sub db_getTTPFolder {

	my ($fromopcoid, $toopcoid) = @_;

	Validate::opcoid($toopcoid);
	
	my ($ttpfolder) = $Config::dba->process_oneline_sql("SELECT ttpfolder FROM OUTSOURCEDESTINATIONS WHERE fromopcoid=? AND toopcoid=?", [ $fromopcoid, $toopcoid ]);
	
	return $ttpfolder;
}



##############################################################################
#      db getDestinationDirectory 
#
##############################################################################
sub db_getDestinationDirectory
{
	my ( $opcoid ) = @_;
	
	Validate::opcoid($opcoid);

	my ($rootpath, $preflightin) = $Config::dba->process_oneline_sql("SELECT rootpath, preflightin FROM OPERATINGCOMPANY WHERE opcoid=?", [ $opcoid ]);
	
	return ($rootpath || "") . ($preflightin || "");
}


##############################################################################
##     db_ getLocalUploadSourceDirectory 
##
###############################################################################
sub db_getLocalUploadSourceDirectory
{
    my ($opcoid) = @_;

	Validate::opcoid($opcoid);
    
    my ($rootpath) = $Config::dba->process_oneline_sql("SELECT LOCALUPLOADSOURCE FROM OPERATINGCOMPANY WHERE opcoid=?", [ $opcoid ]);

    return $rootpath;
}


##############################################################################
##      db_getLocalUploadDestDirectory 
##
###############################################################################
sub db_getLocalUploadDestDirectory
{
    my ($opcoid) = @_;

	Validate::opcoid($opcoid);
    
    my ($rootpath) = $Config::dba->process_oneline_sql("SELECT LOCALUPLOADDESTINATION FROM OPERATINGCOMPANY WHERE opcoid=?", [ $opcoid ]);

    return $rootpath;
}



#############################################################################
#       db_getFilenameTemplate()
#
##############################################################################
sub db_getFilenameTemplate
{
	my ($opcoid) = @_;
	
	Validate::opcoid($opcoid);
	
	my ($filenameconfig) = $Config::dba->process_oneline_sql("SELECT filenameconfig FROM OPERATINGCOMPANY WHERE opcoid=?", [ $opcoid ]);
	return $filenameconfig;
}

##############################################################################
## Get the opcoid using operating company name
###############################################################################
sub db_getopcoidfromcompany
{
	my ($company) = @_;
    
    $company =~ s/,/','/g;
	my ($opcoid) = $Config::dba->process_oneline_sql("SELECT group_concat(opcoid separator ',') FROM operatingcompany WHERE opconame in ('$company')");
	return $opcoid;
}
##############################################################################
### Update the estimation time based on customer and media type
################################################################################
sub updateestimate {

	my ($opcoid, $custid, $mediaid, $estimatetime) = @_;

	Validate::opcoid($opcoid);
	Validate::customerid($custid);

	return "" if $mediaid && !looks_like_number($mediaid);

	#If already present delete and insert     
	my $exists = $Config::dba->process_oneline_sql("Select 1 from estimation where OPCOID=? and CUSTOMERID=? and MEDIAID=?",[$opcoid, $custid, $mediaid]);

	if ($exists) {
		$Config::dba->process_sql("delete from estimation where OPCOID=? and CUSTOMERID=? and MEDIAID=?",[$opcoid, $custid, $mediaid]);
	}

	$Config::dba->process_sql("INSERT INTO estimation(OPCOID, CUSTOMERID, MEDIAID, ESTIMATE, REC_TIMESTAMP) values (?, ?, ?, ?, now())", [$opcoid, $custid, $mediaid, $estimatetime]);
}

##############################################################################
### getUserskills - Fetching values from user_skills table
################################################################################
sub getUserskills {

    my @skills;

    my $rows = $Config::dba->hashed_process_sql("SELECT id, type 
                								FROM user_skills 
												ORDER BY type ASC");

    foreach my $row ( @$rows ) {
        my %row = (
            skillsid      => $row->{id},
            skillstype    => $row->{type},
        );
        push ( @skills, \%row );

    }

    return @skills;
}

##############################################################################
### db_editAdvisoryColour - To insert or update advisory color
##############################################################################

sub db_editAdvisoryColour {

	my $opcoid 				= $_[0];
	my $db_advisoryID		= $_[1];
	my $AdvisoryColor		= $_[2];
	my $AdvisoryColorFlag	= $_[3];
	my $tableName			= 'ADVISORY_COLOUR_MAPPING';

	Validate::opcoid($opcoid);
	
	if ( $AdvisoryColorFlag =~ /^ON$/i ) { $AdvisoryColorFlag = 1;}else{ $AdvisoryColorFlag = 0;}
	
	my $exists = $Config::dba->process_oneline_sql( "SELECT 1 
													FROM $tableName 
													WHERE OPCOID=? AND advisory_id=? ", [ $opcoid, $db_advisoryID ] );

	if ( $exists ) {
		$Config::dba->process_sql("UPDATE $tableName 
									SET advisory_colour = ?, advisory_color_flag = ?, rec_timestamp = now()
									WHERE OPCOID=? and advisory_id=? ", [ $AdvisoryColor, $AdvisoryColorFlag, $opcoid, $db_advisoryID ]);
	} else {
		$Config::dba->process_sql("INSERT INTO $tableName ( opcoid, advisory_id, advisory_colour, advisory_color_flag, rec_timestamp )
                                    VALUES ( ?, ?, ?, ?, now() ) ", [ $opcoid, $db_advisoryID, $AdvisoryColor, $AdvisoryColorFlag ]);	
	}

} # db_editAdvisoryColour

##############################################################################
### db_getadvisorysColourDetails - Getting all advisory and colour detals.
###############################################################################

sub db_getadvisorysColourDetails {

	my $opcoid		= $_[0];	

	Validate::opcoid($opcoid);

	my $rows       = $Config::dba->hashed_process_sql( "SELECT qt.id AS ID, qt.querytype AS ADVISORYNAME, acm.advisory_color_flag AS FLAG, acm.advisory_colour AS COLOUR
														FROM querytypes qt
														LEFT JOIN ADVISORY_COLOUR_MAPPING acm
														ON qt.id=acm.advisory_id
														AND acm.opcoid = ? ", [ $opcoid ] );
	return $rows ? @$rows : ();
} # db_getadvisorysColourDetails

sub getCustomerDefaultEstimateDB {
	my ($opcoid, $customerid, $mediaid)= @_;

	Validate::opcoid($opcoid);
	Validate::customerid($customerid);

    my ( $estimate ) = $Config::dba->process_oneline_sql("SELECT estimate FROM estimation WHERE OPCOID=? and CUSTOMERID=? and MEDIAID=?", [ $opcoid, $customerid, $mediaid ] );

    return $estimate;
}

############################################
# get operating company name for opcoid
############################################
sub getOpname {

	my ( $opcoid ) = @_;

	Validate::opcoid($opcoid);

    my ( $opname ) = $Config::dba->process_oneline_sql("SELECT OPCONAME FROM OPERATINGCOMPANY WHERE OPCOID=?", [ $opcoid ] );
    return $opname;
} # getOpname

sub db_mapexternalUrl {

	my ( $actualURL, $extnernalURL, $userid ) = @_;

	Validate::userid( $userid );

	my ( $exists ) = $Config::dba->process_oneline_sql( "SELECT 1 FROM digitalindexURL where actualURL=?", [ $actualURL ] );

	chomp( $actualURL ); 
	chomp( $extnernalURL );

	if ($exists) {	
		$Config::dba->process_oneline_sql("UPDATE digitalindexURL SET externalURL=?, userid=?, rec_timestamp=now() where actualURL=?", [ $extnernalURL, $userid, $actualURL ] );
	} else {
		$Config::dba->process_oneline_sql("INSERT INTO digitalindexURL(actualURL, externalURL, userid, rec_timestamp) values (?, ?, ?, now())", [ $actualURL, $extnernalURL, $userid ] );
	}
}

sub db_getexternalUrl {

	my ( $actualURL ) = @_;
	my ( $url ) = $Config::dba->process_oneline_sql( "SELECT externalURL FROM digitalindexURL where actualURL=?", [ $actualURL ] );
	return $url;
}

##############################################################################
##       db_getFTPDetails
##
###############################################################################
sub db_getFTPDetails {
    my ( $opcoid ) = @_;

	Validate::opcoid($opcoid);

    my ($ftpserver, $ftpusername, $ftppassword) = $Config::dba->process_oneline_sql("SELECT ftpserver, ftpusername, ftppassword FROM OPERATINGCOMPANY
                                                                            WHERE opcoid=?", [ $opcoid ]);
    return ($ftpserver, $ftpusername, $ftppassword);
}
 
##############################################################################
# getCustomerId2
##############################################################################
sub getCustomerId2 {
    my $jobid       = $_[0];

	Validate::job_number( $jobid );

	my ( $customerId ) = $Config::dba->process_oneline_sql(
		"SELECT id FROM CUSTOMERS WHERE customer = (SELECT AGENCY FROM TRACKER3 WHERE JOB_NUMBER = ?) AND OPCOID = (SELECT location FROM TRACKER3PLUS WHERE JOB_NUMBER = ?)", 
		[ $jobid, $jobid] );

    return $customerId;
}

##############################################################################
#       end of file
##############################################################################

1;
