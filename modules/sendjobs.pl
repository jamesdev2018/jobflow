﻿#!/usr/local/bin/perl
package SendJobs;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use Template;
use Data::UUID;
use XML::Simple qw(:strict);
use Data::Dumper;
use Date::Simple ('date', 'today');
use JSON;
use POSIX qw/strftime/;
use Time::Local;
use Encode qw(decode encode);
use Carp qw/carp cluck/;

# Home grown

# Please do NOT include jobs.pl or any module that requires jobs.pl
use lib 'modules';

use CTSConstants;
use XMLResponse qw ( :statuses );

require "config.pl"; # for calls to Config::getConfig and $Config::dba->hashed_process_sql
require "databaseJob.pl";
require "databaseOpcos.pl";
require "databaseUser.pl";
require "databaseImageworkflow.pl";
require "users.pl";
require "databaseAudit.pl";
require "Mailer.pl";
require "general.pl";
require "jf_wsapi.pl"; # JF_WSAPI package
require "Watchlist.pl";
require "jobpath.pl";
require "streamhelper.pl";
require 'databaseJobBundle.pl';
require 'databaseCustomerUser.pl';
require 'jobfilename.pl';  # getJobFilename

##################################################################
# from config 
##################################################################

my $fromemail 		= Config::getConfig("fromemail");
my $supportemail	= Config::getConfig("supportemail");
my $supportemail2	= Config::getConfig("supportemail2");
my $environment		= Config::getConfig("environment");

my $adgateremote 	= Config::getConfig("adgateremote");
my $gearmanservers 	= Config::getConfig("gearmanservers");
my $cmdurl	 	= Config::getConfig("cmdurl");
my $jobflowServer 	= Config::getConfig("jobflowserver") || "";
my $adspecServer	= Config::getConfig("adspecserver" );
my $digitalRenderedPdfFldr	= Config::getConfig("digitalRenderedPdfFldr" ) || "RENDERED_PDF";
my $reviewDigitalAsset		= Config::getConfig("reviewDigitalAsset" );
my $digitalJobflowServer	= Config::getConfig( "digitalJobflowServer" ) || "";
my $contactPhone			= Config::getConfig( "contactPhone" ) || "+44 (0)20 7251 4571";
my $testSendEmail			= Config::getConfig( "testSendEmail" ) || "testsend\@tagworldwide.com";
my $smtp_gateway        	= Config::getConfig("smtp_gateway");
my $digitalIndexCustoLogPath= Config::getConfig("customerlogo_path");
my $ftpassetstocmd          = Config::getConfig("ftpassetstocmd");
my $ftpassetspath           = Config::getConfig("ftpassetspath");
my $bundle_url				= Config::getConfig("bundle_download_url") || 'bundle_download_url_undef';


##################################################################
# globals 
##################################################################

my $applicationurl      	= $jobflowServer . "/jobflow.pl";

my $digitalApplicationURL	= $digitalJobflowServer . "/reviewDigitalAsset.pl";

my $zipcmd 		= "/usr/bin/zip";
my $unzipcmd 		= "/usr/bin/unzip";
my $autoarchiveafter	= 10;		# autoarchive after this number of days

my %preflightstatuses = (
	InProgress 		=> '1',
	Passed 			=> '2',
	Warning			=> '3',
	Failed			=> '4',
	None			=> '5',
	Dispatched		=> '6',
);

my %assetSubPath = (  # used in GetHiRes
	bw			=> 'BW_ASSET/',
	bwcropped		=> 'BW_CRP_ASSET/',
	lowrescropped		=> 'LR_CRP_ASSET/',
	lowres			=> 'LR_ASSET/',
	proofonly		=> 'PROOFING_ASSET/',
	rgb			=> 'RGB_ASSET/',
	rgbcropped		=> 'RGB_CRP_ASSET/',
	rgbjpeg			=> 'RGB_JPG_ASSET/',
	hirescroppedbleedbox	=> 'HIRES_CRP_BLEEDBOX/',
	hirescroppedtrimbox	=> 'HIRES_CRP_TRIMBOX/',
	hires			=> 'OUTPUT_FILE/',
);

#my %qcstatuses = (
#	0	 		=> 'None',
#	1 			=> 'QC Required',
#	2			=> 'QC Approved',
#	3			=> 'QC Rejected',
#);

# This hard coded list works for Press, but should really be constructed from either a config file or a DB lookup, used in sendByFTP & sendByEmail.
# This also defines the order the assets appear on the Choose Destination dialog (as hashes have no implicit sort order)
my @file_type_list = ( 
			# "additionalfiletypes",  # this is not an asset type, so exclude
			"bw_asset", "bw_crp_asset", "lr_asset", "lr_crp_asset",
			"rgb_asset", "rgb_crp_asset", "rgb_jpg_asset",
			"hires_crp_bleedbox_asset", "hires_crp_trimbox_asset", 
			"aux_files", "split_pages", "open_assets", "high_res", "documents", "rendered_pdf", "zip_dir", "proof_only" );

# hashmap used by SendJobsViaEMail to work out the source and target directory and filenames
# The main hash keys are the filetypes, each value being an anonymous hash
# - suffix is the jobpath suffix used to identify the asset <jobpath>/<suffix>/
# - target is the target filename (or filename suffix).
# - type is "", "all", "pdf" or "zip" (defaults to "all") (for SendByEmail)
# - name is the checkbox label text to use on the Choose Destination dialog 

my %file_type_send_details = (
	"bw_asset" 		=> { suffix => "JOB_ASSETS/BW_ASSET",	target => "bw_pdf", type => "pdf", name => "BW PDF" },
	"bw_crp_asset"		=> { suffix => "JOB_ASSETS/BW_CRP_ASSET",	target => "cropped_bw_pdf", type => "pdf", name => "Cropped BW PDF" },
	"lr_asset"		=> { suffix => "JOB_ASSETS/LR_ASSET",	target => "lowres_pdf", type => "pdf", name => "Low Res PDF" },
	"lr_crp_asset"		=> { suffix => "JOB_ASSETS/LR_CRP_ASSET",	target => "cropped_lowres_pdf", type => "pdf", name => "Cropped Low Res PDF" },
	"rgb_asset"		=> { suffix => "JOB_ASSETS/RGB_ASSET",	target => "rgb_pdf", type => "pdf", name => "RGB PDF" },
	"rgb_crp_asset"		=> { suffix => "JOB_ASSETS/RGB_CRP_ASSET",	target => "cropped_rgb_pdf", type => "pdf", name => "Cropped RGB PDF" },
	"rgb_jpg_asset"		=> { suffix => "JOB_ASSETS/RGB_JPG_ASSET",	target => "rgb_jpg", type => "zip", name => "RGB Jpeg" },
	"hires_crp_bleedbox_asset" => { suffix => "JOB_ASSETS/HIRES_CRP_BLEEDBOX", target => "hires_crp_bleedbox", type => "pdf", name => "Hi Res Cropped Bleedbox" },
	"hires_crp_trimbox_asset" => { suffix => "JOB_ASSETS/HIRES_CRP_TRIMBOX", target => "hires_crp_trimbox", type => "pdf", name => "Hi Res Cropped Trimbox" },
	"aux_files"		=> { suffix => "JOB_ASSETS/AUX_FILES",	target => "alt_file", type => "all", name => "Alternative File Formats" },
	"split_pages"		=> { suffix => "JOB_ASSETS/SPLIT_PAGES",	target => "split_pages", type => "split", name => "Split Pages" },
	"open_assets"		=> { suffix => "JOB_ASSETS/OPEN_ASSETS",	target => "open_assets", type => "all", name => "Job Assets" },
	"high_res"		=> { suffix => "OUTPUT_FILE", target => "", type => "pdf", name => "Hi Res" },
	"documents"		=> { name => "Documents" },
	"rendered_pdf"		=> { suffix => "RENDERED_PDF", target => "rendered_pdf", type => "pdf", name => "Rendered PDF" },
	"zip_dir"		=> { suffix => "ZIP_FTP_DELIVERY", target => "zip_dir", type => "zip", name => "Multiple Files" },
	"proof_only"    => { suffix => "JOB_ASSETS/PROOFING_ASSET", target => "proof_pdf", type => "pdf", name => "Proof Only" },
	);

# SUPPORTING_DOCUMENTS and PROOFING_ASSET used elsewhere

##################################################################
# the template object 
##################################################################
my $tt = Template->new( { RELATIVE => 1, } ) || die "$Template::ERROR\n";

my $debug = 0;   # Set by test cases at end

# Get publication email from publication name

# This used to return just an email address, but since in CMD, the publication name needs to be qualified by country and specification
# there are potentially many email address for a publication. See PD-2068.
# The jf_batch_webservice now returns an XML string rather than the email.
# Trap the error conditions (either explicit error in the XML or just no data returned)
# and otherwise return an array of hash ( email, ... )
# Inputs
# - pubname, publication name
# Returns
# - array of hash
# Throws exception if pubname is undef or blank or if XML returned from CMD has error element

sub getPubEmailFromPubName
{
	my ($pubname) = @_;

	die "No publication name" unless ( ( defined $pubname ) && ( $pubname ne "" ) );

	my ( $response, $request ) = JF_WSAPI::batchWebservice( { action => 'getpubemail', pubname => $pubname } );

	die "No response to request to get publication email for $pubname" unless ( $response );  # ie if blank XML response
	die "Invalid publication name $pubname" if ( $response =~ /\<pubEmail\> \<\/pubEmail\>/ );

	# Unmarshall the XML
	# Revised PubEmailService returns
	# <pubEmail>
	# <spec><email>lsebag@condenast.fr</email><country>FRANCE</country></spec>
	# <spec><email>mcanesi@condenast.it</email><country>ITALY</country></spec>
	# :
	# </pubEmail>
	# Returns <pubEmail></pubEmail> if the publication is not found
	# Returns <pubEmail> <error>Param 'pubname' required</error> </pubEmail>  if for example param is missing

	my $results;
	eval {
		$results = XMLin( $response, KeyAttr => [ ], ForceArray => [ ] );
	};
	if ( $@ ) {
		die "XMLin threw '$@', input " . Dumper( $response ) . " curl: $request";
	}
	die $results->{error} if ( exists $results->{error} );

	my $list = $results->{spec};
	die "Publication '$pubname' has no email addresses" unless ( defined $list );

	# The relationship between email and country is not straightforward
	# there can be several emails for one country (impossible according to some)
	# and an email could be associated with more than one country.

	my @list2;
	foreach my $entry ( @$list ) {
		my $email = $entry->{email};
		next if ( lc($email) =~ /add\s*details/ );
		my $country = $entry->{country};
		push( @list2, { email => $email, country => $country } );
	}

	# Should really sort the list by email
	my @sortedlist = sort { $a->{email} cmp $b->{email} } @list2;

	my @finalList;
	my $lastEntry;
	foreach my $entry ( @sortedlist ) {
		next if ( ( defined $lastEntry ) && ( $entry->{email} eq $lastEntry->{email} ) && ( $entry->{country} eq $lastEntry->{country} ) );
		$lastEntry = $entry;
		push( @finalList, $entry );
	}

	return @finalList;
}

##########################################################################
# 	sendArchantStatusUpdate
#	sends file to Archant indicating pass/fail/warning etc.
#
##########################################################################
sub sendArchantStatusUpdate {
	my $file	= $_[0];

	my $ftp;
	eval {
			my $archantftpserver 	= Config::getConfig("archantftpserver");
			die "Archant FTP server invalid" unless ( $archantftpserver );

			my $archantftpusername	= Config::getConfig("archantftpusername");
			my $archantftppassword 	= Config::getConfig("archantftppassword");
			my $archantftppath 	= Config::getConfig("archantftppath");

			# ftp to archant
			$ftp = Net::FTP->new( $archantftpserver, Debug => 0 ) or die "Cannot connect to $archantftpserver : $@";
			$ftp->login( $archantftpusername, $archantftppassword ) or die "Cannot login ", $ftp->message;
			$ftp->cwd( $archantftppath ) or die "Unable to cwd: to $archantftppath: ", $ftp->message;
			$ftp->binary();
			$ftp->put( $file ) or die "Cannot get ", $ftp->message;
			# system ( "mv $file $incomingFTPFolder" );
			# rm from FTP server
			#$ftp->delete( $file );		
			$ftp->quit( );
			$ftp = undef;

	};
	if ( $@ ) {
		my $error = $@;
		carp "sendArchantStatusUpdate: $error";
		$ftp->quit() if ( $ftp );
	}
}

# findArtworkProduct, helper for Approve, called when query type indicates a One2Edit job

sub findArtworkProduct {
	my ( $job_number ) = @_;

	my $artwork_product_id;
	my $artwork_product_name = '';
	my $product_count = 0;

	eval {
		no warnings 'once';  # Only use of dba->hashed_process_sql in this module
		my $rows = $Config::dba->hashed_process_sql(
				"SELECT id, productname FROM products " .
				" WHERE job_number = ? AND isDeleted = 0 AND JOBVERSION = (SELECT MAX(JOBVERSION) FROM products WHERE job_number = ?)",
				[ $job_number, $job_number] );
		my @product_list;
		if ( defined $rows ) {
			warn "Dump of products for latest job version\n";
			foreach my $res ( @$rows ) {
				push( @product_list, { id => $res->{ id }, productname => $res->{ productname } } );
				# warn "Product name $res->{ productname }, id $res->{ id }"; # DEBUG
			}
		}
		$product_count = scalar @product_list;
		if ( $product_count > 0 ) {
			my $o2e_artwork_task    = Config::getConfig("one2edit_artwork_task" ) || 'Artwork';

			my @pattern_list = split /,/, $o2e_artwork_task;
			foreach my $pattern ( @pattern_list ) {
				foreach my $h ( @product_list ) {
					# warn "Check task $h->{productname} v pattern $pattern\n"; # DEBUG
					if ( $h->{productname} =~ /$pattern/i ) {
						$artwork_product_id = $h->{id};
						$artwork_product_name = $h->{productname};
						last;
					}
					last if defined $artwork_product_id;
				}
			}
		}
	};
	if ( $@ ) {
		my $error = $@;
		die "findArtworkProduct: $error\n";
	}
	return ( $artwork_product_name, $artwork_product_id, $product_count );
}

##############################################################################
#       Approve - approve / decline a job 
#
##############################################################################
sub Approve {
	my ( $query, $session ) = @_;

	my $mode			= $query->param( "mode" ) ||'';
	my $jobid			= $query->param( "jobid" );
	my $key				= $query->param( "key" );
	my $jobaction		= $query->param( "jobaction" ) ||'';

	my $opcoid			= $session->param( 'opcoid' );
	my $userid			= $session->param( 'userid' );

	my $vars;
	my $ftp;
	my @history;

	eval {
		Validate::job_number( $jobid );

		my ( $jobpath, $jobopcoid, $customerid, $location, $remoteCustomerId, $customerName ) = JobPath::getJobPath( $jobid );
		die "Job doesnt exist" unless defined $jobpath;
		# warn "Approve: jobopcoid $jobopcoid, cust: $customerid, location: $location, remote cust: $remoteCustomerId";  # DEBUG
		
		my $twisttmpconfig 	= Config::getConfig( "twisttmppath" );
		$twisttmpconfig =~ s/(\/)?$//;
		
		## A job's mediatype can override the twist config path.
		## Absolute overrides replace the path, otherwise the suffix it

		my $twist_tmp_override	= ECMDBJob::db_getJobInfo( $jobid );
		if ( $twist_tmp_override ) { 
			$twist_tmp_override = $twist_tmp_override->{twist_tmp_path}
		} else { 
			$twist_tmp_override = ""; 
		}
		if ( $twist_tmp_override =~ m/\// ) { 
			$twisttmpconfig = $twist_tmp_override;
		} else { 
			$twisttmpconfig .= $twist_tmp_override; 
		}
		$twisttmpconfig .= '/' if $twisttmpconfig !~ m/\/$/;
		
		my $twistpickupconfig = Config::getConfig( "twistpickuppath" );

		#ftp the file over.

		my $twistrootpath 	= getDestinationDirectory( $jobopcoid );
		die "Twist path invalid, opcoid $jobopcoid" unless ( $twistrootpath );

		$twistrootpath 		= substr( $twistrootpath, 0, rindex( $twistrootpath, '/' ) + 1 ); #chop off the last subdir (WORKFLOW) to get the root.
		my $twisttmp 		= $twistrootpath.$twisttmpconfig.$jobid.".pdf";
		my $twistpickup	 	= $twistrootpath.$twistpickupconfig.$jobid.".pdf";

		# if this is a repost (to myself) (and jobaction is define - change job status FIRST!!!!)
		#### UPDATE the Database
		if ( $jobaction eq "approve" ) {
			ECMDBJob::db_updateJobPreflightStatus( $jobid, $preflightstatuses{ Passed } );
			ECMDBJob::db_lockJob( $jobid, 0 ); #unlock the job
			
			# move job from TWIST_TMP to TWIST_PICKUP OLD WAY
			#my ( $ftpserver, $ftpusername, $ftppassword ) = getFTPDetails( $jobopcoid );
			#my $ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to : $@";
			#$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login ", $ftp->message;
			#$ftp->rename( $twisttmp, $twistpickup ) or die "Unable to rename: $twisttmp to $twistpickup";
			
			ECMDBJob::db_sendToTwist( $jobid, 1 ); # Just sets tracker3plus.sendtotwist = 1

			##In preflight warning if job is One2edit and user clicks on Approve move the job location to QC_monitor

			my $automation_querytype = Config::getConfig("one2edit_automation_querytype") || 'Automation_Complete';
			my $send_to_qcmonitor = Config::getConfig("one2edit_send_to_qcmonitor") || 0;
			my ( $qt_id, $qt_name ) = ECMDBJob::getJobQueryType( $jobid );
			my $task_level = ECMDBProducts::checkJobLevel( $jobid ); # 0 means not assigned, 1 assigned at job level, 2 at task level

			ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Job query type $qt_name, send_to_qcmonitor $send_to_qcmonitor, task level $task_level." );

			# a) search for Artwork task and if found set its QC status
			# b) if Artwork task exists and several other conditions, move to QC Monitor

			if ( $qt_name eq $automation_querytype ) {

				my ( $artwork_product_name, $artwork_product_id, $product_count ) = findArtworkProduct( $jobid );
				#warn "name $artwork_product_name, id $artwork_product_id, Count $product_count\n";

				if ( $product_count == 0 ) {
					ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Job has no tasks" );

				} elsif ( ! defined $artwork_product_id ) {
					ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Could not find Artwork task" );

				} else {
					# At least one task exists and is an Artwork (like) task
					ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Found task $artwork_product_name ($artwork_product_id), which matches Artwork pattern" );

					# Set task QC status .... is id same as lineitemid TODO, otherwise would use ECMDBProducts::updateQcStatus( $lineitemid, $status );
					$Config::dba->process_sql( "UPDATE products SET qcstatus=? WHERE id=?", [ 1, $artwork_product_id ] ); # 1 is QC Required

					if ( $send_to_qcmonitor == 0 ) {
						ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "QC Monitor disabled for One2Edit jobs" );

					} elsif ( $task_level == 1 ) {
						ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Job assigned at Job level" );

					} elsif ( $product_count < 2 ) {
						ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Job must have at least 2 tasks for QC Monitor" );

					} else {
						my $qc_monitor = ECMDBProducts::getQcOpcoid(); # id for QC_MONITOR location
						if ( $location == $qc_monitor ) {
							ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Job already at QC_MONITOR" );
						} else {
							my $opconame = ECMDBOpcos::getOpcoName( $location );
							ECMDBProducts::updateJobLocation( $jobid, $qc_monitor );
							ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Job moved from $opconame to QC_MONITOR" );
						}
					}
				}
			}
		
			ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Approved job" );

		} elsif ( $jobaction eq "decline" ) {
			ECMDBJob::db_updateJobPreflightStatus( $jobid, $preflightstatuses{ Failed } );		
			ECMDBJob::db_lockJob( $jobid, 0 ); #unlock the job

			# delete via ftp
			my ( $ftpserver, $ftpusername, $ftppassword ) = getFTPDetails( $jobopcoid );
			die "Invalid FTP server, opcoid $jobopcoid" unless ( $ftpserver );

			$ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to $ftpserver : $@";
			$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login ", $ftp->message;
			$ftp->delete( $twisttmp ) or die "Unable to delete: $twisttmp";
			$ftp->quit();
			$ftp = undef;
	
			ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Declined job" );
		}

		#### SHOW results page
		# get TRACKER3 job data
		my @job		= ECMDBJob::db_getJob( $jobid );
		my @jobstatus	= ECMDBJob::db_getJobstatus();

		$vars = {
			jobflowserver		=> $jobflowServer,
			jobid			=> $jobid,
			myjob 			=> \@job,
			jobstatus 		=> \@jobstatus,
			jobaction 		=> $jobaction,
			postValue		=> "approveJob",
		};
	};

	my $rnd_no = $session->param( 'rnd_no' ) || '';
	$vars->{ rnd_no } = $rnd_no;
	$vars->{ sessionid } = $session->id;

	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => $error };
		$ftp->quit() if ( $ftp );
	}
	if ( $mode eq "json" ) {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

	General::setHeaderVars( $vars, $session );
	$tt->process( 'ecm_html/approvejob.html', $vars )
			|| die $tt->error(), "\n";
}

sub email_error {
	my ( $body ) = @_;
	Mailer::send_mail ( $fromemail, $supportemail, $body, "Job Flow: Error", { host => $smtp_gateway } );
}

##############################################################################
#       ViewLowRes
#
##############################################################################
sub ViewLowRes {
	my ( $query, $session ) = @_;

	my $jobid 		= $query->param( "jobid" );

	my $roleid		= $session->param( 'roleid' );
	my $userid		= $session->param( 'userid' );
	my $username	= $session->param( 'username' );

	my $filename	= "";
	my $assetpath	= "";
	my $assetname	= "";
	my $ftp;

	# copy the low res to the web server:
	# ftp the file from /gpfs to /temp	
	# get the ftp details

	eval {
		Validate::job_number( $jobid );

		my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobid );
		die "Job doesnt exist" unless defined $jobpath;

		# Check user can see jobs customer
		StreamHelper::assert_customer_visible( $userid, $customerid );

		my $qcstatus		= ECMDBJob::db_getQCStatus( $jobid );
		my $assetsCreated	= ECMDBJob::db_assetsCreated ( $jobid, 0 );

		$assetpath  		= $jobpath . '/JOB_ASSETS/LR_ASSET/';
		$assetname 			= $jobid . '.pdf';

		my ( $ftpserver, $ftpusername, $ftppassword ) = getFTPDetails( $jobopcoid ) ;
		die "Invalid FTP server for opcoid $jobopcoid" unless ( $ftpserver );

		my $ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to $ftpserver : $@";
		$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login ", $ftp->message;
		$ftp->binary();
		$ftp->cwd( $assetpath ) or die "Low Res directory '$assetpath' does not exist";
		my @lis = $ftp->ls( $assetname );
		die "Low Res PDF '$assetpath/$assetname' does not exist" unless ( ( scalar @lis ) > 0 );
		#carp Dumper( \@lis );
		$ftp->quit();
		$ftp = undef;

		my $vars = {
			assetsCreated	=> $assetsCreated,
			jobflowserver	=> $jobflowServer,
			filename		=> $filename,
		};

		General::setHeaderVars( $vars, $session );
	
		$tt->process( 'ecm_html/viewlowres.html', $vars )
				|| die $tt->error(), "\n";
	};
	if ( $@ ) {
		my $error = $@;
		carp "ViewLowRes: Error $error";
		$error =~ s/ at .*//gs;
		# email support that an error has occured.

		my $body = "An error occurred when user $username attempted to view low res file: $assetpath/$assetname\n\n";
		$body .= "Error occured in sendjobs.pl::ViewLowRes(). Either the file was not found or the ftp failed. \n";
		$body .= "The error returned: $error\n\n";
		$body .= "Team JobFlow\n\n";

		email_error( $body );
		$ftp->quit() if ( $ftp );

		my $vars = {
			errormessage	=> "There was a problem transferring files.", error => $error,
		};

		General::setHeaderVars( $vars, $session );

		$tt->process( 'ecm_html/genericErrorPage.html', $vars )
				|| die $tt->error(), "\n";

	}

}


##############################################################################
#       getLowResZip - THIS IS FOR LEGACY!!!!
#
##############################################################################
sub getLowResZip {
	my ( $query, $session ) = @_;

	my $jobid 		= $query->param( "jobid" );

	my $username 	= $session->param( 'username' );
	
	my $filename	= "";   
	my $zippath		= "";
	my $ftp;

	# copy the low res to the web server:
	# ftp the file from /gpfs to /temp	
	# get the ftp details

	eval {
		my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobid ); 
		die "Job doesnt exist" unless defined $jobpath;

		$zippath	= "$jobpath/JOB_ASSETS/LR_ASSET/$jobid.zip"; #full path to the low-res asset
	
		my $assetsCreated	= ECMDBJob::db_assetsCreated ( $jobid, 0 );

		my ( $ftpserver, $ftpusername, $ftppassword ) = getFTPDetails( $jobopcoid );
		die "Invalid FTP server for opcoid $jobopcoid" unless ( $ftpserver );

		$ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to $ftpserver : $@";
		$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login ", $ftp->message;
		$ftp->binary();
		
		#get a new uuid
		my $ug		= new Data::UUID;
		my $uuid	= $ug->create();
		my $uuidstr	= $ug->to_string( $uuid );
		$filename	= "$uuidstr.zip";
		
		$ftp->get( $zippath, "/temp/$filename" ) or die "put failed ", $ftp->message;

		$ftp->quit();
		$ftp = undef;

		my $vars = {
			jobflowserver	=> $jobflowServer,
			filename	=> $filename,
		};

		General::setHeaderVars( $vars, $session );
	
		$tt->process( 'ecm_html/getlowreszip.html', $vars )
				|| die $tt->error(), "\n";

	};
	if ( $@ ) {
		my $error = $@;
		carp "getLowResZip: $error";
		$error =~ s/ at .*//gs;

		# email support that an error has occured.

		my $body = "An error occurred when user $username attempted to view low res file: $zippath\n\n";
		$body .= "Error occurred in sendjobs.pl::getLowResZip()";
		$body .= "Either the file was not found or the ftp failed.";
		$body .= "The error returned: $error\n\n";
		$body .= "Team JobFlow\n\n";

		email_error( $body );

		my $vars = {
			errormessage	=> "There was a problem transferring files.",
		};

		General::setHeaderVars( $vars, $session );

		$tt->process( 'ecm_html/genericErrorPage.html', $vars )
				|| die $tt->error(), "\n";
	}
}

##############################################################################
#       GetHiRes
#
#		This function receives a jobid and a file type - requested by the
# 		user. A copy of the hi-res is made to /temp and an email link is sent 
#		to the user to allow him/her to download.
#
#	In fact the function name is misleading as this allows downloading one or more of a set
#	of asset types for a job, depending on which query params are set to "on".
#	It copies the asset into the /temp directory (or a sub directory of that)
#	and the link in the email (for each asset type), runs getjobasset.pl, which expects
#	to find the asset in the temp directory (the link includes the name of the file).
#
#	It would have been relatively easy to convert this to use filedownload.pl 
#	(ie add a type param to make it more general), so avoiding FTP'ing all the files
#	in the apache thread. However, since the user may click on the link at some point in the
#	future the auth method used by filedownload wont work.
#	It is more link sendbyemail, in that the bundle of assets can be downloaded
#	within some period of time.
# 
##############################################################################
sub GetHiRes {
	my ( $query, $session ) = @_;

	my $jobid 		= $query->param( "jobid" );
	my $mode		= $query->param( "mode" ) || '';
	my $type		= $query->param( "type" ) || '';

	my $userid		= $session->param( 'userid' );

	my $download_temp = Config::getConfig("downloadtemp") || '/temp';

	# which file types were selected ?
	my @filetypes;
	foreach my $ft ( 'bw', 'bwcropped', 'lowrescropped', 'lowres', 'hires', 'proofonly', 'rgb', 'rgbcropped', 'rgbjpeg', 'hirescroppedbleedbox', 'hirescroppedtrimbox' ) {
		my $val = $query->param( $ft );
		push( @filetypes, $ft ) if ( defined $val ) && ( ($val eq "on") || ($val eq "true") ); # Checkbox value will be either undef or "on" ("true" as that is what the js provides)
	}

	#carp "GetHiRes " . Dumper( @filetypes );

	my $rootpath	= "";
	my @filenames;		# holds the list of filenames to email user
	my $filetypelist = join( ',', @filetypes );
	my $ftp;
	my $assets_copied = 0;
	my $assets_failed = 0;
	my $vars;
	my $t_start = time();

	my $downloadurl	= Config::getConfig( 'getjobasset_url' ) || $jobflowServer . "/stream/getjobasset.pl";

	eval {

		die "No asset types selected" unless scalar @filetypes;

		my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobid );
		die "Job doesnt exist" unless defined $jobpath;

		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Downloading job assets " . join( ',', @filetypes ) );

		# Ensure temp directory exists (esp if not /temp)
		if ( ! -e $download_temp ) {
			mkdir $download_temp or die "Failed to make temp directory $download_temp";
		}

		# ftp the file from /gpfs to /temp	
		# get the ftp details

		my ( $ftpserver, $ftpusername, $ftppassword ) = getFTPDetails( $jobopcoid );
		die "Invalid FTP server for opcoid $jobopcoid" unless ( $ftpserver );

		# ftp
		$ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to $ftpserver : $@";
		$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login ", $ftp->message;
		$ftp->binary();

		$ftp->cwd( $jobpath ) or die "Job path directory '$jobpath' doesnt exist";

		foreach $type ( @filetypes ) {
			my $assetpath;
			my $filename = ""; # Name of the file when saved in /temp 
			my $fileext = 'pdf'; # True for all but rgbjpeg

			if ( $type eq "hires") { 
				$assetpath 		= $jobpath . '/OUTPUT_FILE/';
				$filename		= $jobid . '.' . $fileext;
			} else {
				my $assetsubpath	= $assetSubPath{ $type };
				die "Sub path doesnt exist for asset type '$type'" unless ( $assetsubpath );
				$assetpath 		= $jobpath . '/JOB_ASSETS/' . $assetsubpath;
				$fileext		= 'zip' if ( $type eq 'rgbjpeg' );
				$filename		= $jobid . '_' . $type . '.' . $fileext;
			}
			my $assetname		= "$jobid.$fileext";
			#print "Asset: $assetpath/$assetname<br>";

			eval {
				$ftp->cwd( $assetpath ) or die "Asset directory '$assetpath' doesnt exist";
				$ftp->get( $assetname, "$download_temp/$filename" ) or die "get '$assetpath/$assetname' failed ", $ftp->message;
				$assets_copied++;
				push ( @filenames, { filename => $filename, type => $type, error => '' } );

				my $file_size = -s $download_temp/$filename;
				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Downloaded job asset $type to $download_temp/$filename, size $file_size" );

			};
			if ( $@ ) {
				my $error = $@;
				$error =~ s/ at .*//gs;
				$assets_failed++;
				push ( @filenames, { filename => $filename, type => $type, error => $error } );
			}
		}

		$ftp->quit();
		$ftp = undef;

		# email the user
		if ( $assets_copied > 0 ) {
			my $email		= ECMDBUser::db_getEmail ( $userid );
			my $body = "Hi,\nThe files you requested for download are ready. Please use the link(s) below to access them.\n\n";
	
			foreach my $myfile ( @filenames ) {
				my $filename 	= $myfile->{"filename"};
				my $type		= $myfile->{"type"};
				$body			.= "$type\t\t$downloadurl?action=sendfile&id=$filename\n\n";
			}
			$body .= "Thank you.\n\nJob Flow.\n\n";
			Mailer::send_mail ( $fromemail, $email, $body, "Jobflow files ready for download", { host => $smtp_gateway } );
		}

		my $t_elapsed = time() - $t_start;

		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Downloading job assets completed, copied $assets_copied, failed $assets_failed, elapsed $t_elapsed sec" );

		$vars = {
			downloadurl		=> $downloadurl,
			filenames		=> \@filenames,
			filetypelist	=> $filetypelist,
			postValue		=> "gethires",
			assetsCopied	=> $assets_copied,
			assetsFailed	=> $assets_failed,
			elapsed			=> $t_elapsed,
		};
	};
	if ( $@ ) {
		my $error = $@;
		carp "getHiRes: $error";
		$error =~ s/ at .*//gs;
		$ftp->quit() if ( $ftp );

		my $t_elapsed = time() - $t_start;
		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Download failed, copied $assets_copied, failed $assets_failed, elapsed $t_elapsed sec, $error" );
		$vars = { error => $error, elapsed => $t_elapsed, postValue => 'gethires', 
				assetsCopied => $assets_copied, assetsFailed => $assets_failed, 
				filetypelist => $filetypelist,
				filenames	=> \@filenames, 
				downloadurl => $downloadurl, 
		};
	}

	if ( $mode eq "json" ) {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/gethires.html', $vars )
			|| die $tt->error(), "\n";
}

##############################################################################
#   :chooseDestination()
#	where are we sending files to?
##############################################################################
sub chooseDestination {
	my ( $query, $session ) = @_;

	my $opcoid 		= $session->param( 'opcoid' );
	my $mode		= $query->param( 'mode' ) || '';

	my $isEnhancedFTP = 0;
	my $additionalfiletypes = 0; #by default all options are off

	my @errormessages;
	my @warningmessages;
	my @digital_link_errors;

	# Array of hash passed to template, defining all the assets to display, whether they are enabled/checked and label text
	my @file_types_for_display;

	# This list is used by the javascript to correctly enable the file types when switching to Send Email - Client.
	my @client_fts;

	my $options;		# hash returned by getCustomerFileoptions
	my $vars;
	my @jobpathlist;	# Save job path details for each job 
	my @history;
	my $jobs_with_assets_enabled = 0;	# count of those jobs that have at least one asset that is enabled and exists

	# get asset types for selected jobs and combine into one array
	my @asset_list;
	my @asset_errors; # Missing asset errors

	my $jobList 		= $query->param( "jobslist" ) || "";
	$jobList 		=~ s/,$//; # remove spurious trailing comma
	my @jobs 		= split( ',', $jobList );

	# push( @file_types_for_display, { name => "Disabled", on => 0, checked => 0, label => "Is Disabled" } );
	# push( @file_types_for_display, { name => "Enabled", on => 1, checked => 0, label => "Is Enabled" } );
	# push( @file_types_for_display, { name => "Checked", on => 1, checked => 1, label => "Is Checked" } );

	eval {
		die "No jobs specified" unless (scalar @jobs );

		my $opconame		= ECMDBOpcos::getOpcoName( $opcoid ); # opconame for session opcoid BEWARE

		push( @history, "Doing initial jobpath check" );

		# Do the basic checks for jobs first before the product check
		foreach my $jobid (@jobs) {
			eval {
				push( @history, "Getting job path for $jobid" );
				my ( $jobpath, $jobopcoid, $customerid, $location, $remoteCustomerId, $customer_name ) = JobPath::getJobPath( $jobid );
				push( @jobpathlist, { jobid => $jobid, path => $jobpath, opcoid => $jobopcoid, customerid => $customerid, 
										location => $location, remotecustomerid => $remoteCustomerId, customer_name => $customer_name } );
			};
			if ( $@ ) {
				my $error = $@;
				push ( @errormessages, "Failed to get job path for $jobid, $error" );
			}
		}
		die "Failed getting job path info for 1 or more jobs" if ( scalar @errormessages );
		push( @history, "Got jobpath for all jobs, count = " . (scalar @jobpathlist) );

		push( @history, "Calling get_customer_for_jobs" );

		# Get the customer id (use one job to do this)
		my $customerId = get_customer_for_jobs( \@jobs, \@errormessages );
		die "Failed to get customer id for these jobs" unless ( defined $customerId ) && ( $customerId != 0 );

		push( @history, "Calling isProductsEnabled, customerId $customerId" );

		# Check for isProductsEnabled moved to after the check that there are any assets to download
		# so we dont see the annoying "Please ensure all QC checks have been completed" if there are no assets
	
		push( @history, "Doing enhanced FTP check, customerId $customerId" );
	
		#if ( $customerId > 0 ) { # we have a customer id - pull in ftp list for this customer
			$isEnhancedFTP = ECMDBOpcos::isEnhancedFTP( $customerId );
			my @fileoptions = ECMDBOpcos::getCustomerFileoptions( $customerId ); 
			if ( ( scalar @fileoptions ) > 0 ) {
				#push( @history, "Fileoptions " . Dumper( $fileoptions[0] ) );
				$options = $fileoptions[0] if ( $fileoptions[0]{additionalfiletypes} );
				#print "Values returned by ECMDBOpcos::getCustomerFileoptions for customer $customerId<br>\n";
				#foreach my $k ( keys %$options ) {
				#	print "key $k -> " . $options->{ $k } . "<br>\n";
				#}

				$additionalfiletypes = $options->{additionalfiletypes};
				delete $options->{additionalfiletypes};	# Flag to enable the other flags, and this is set

				delete $options->{open_assets};		# We don't handle open_assets "Job Assets"

				# Special case for HM_Tag_London and documents
				if ( $opconame eq "HM_Tag_London" ) {
					$options->{documents} = $isEnhancedFTP;
				} else {
					delete $options->{documents};
				}
			}
		#}

		push( @history, "Checking assets for each job" );

		foreach my $job_ref ( @jobpathlist ) {  # Saves doing 2nd iteration with getJobPath
			# my ( $jobpath, $jobopcoid, $customerid, $location, $remoteCustomerId, $customer_name ) = JobPath::getJobPath( $jobid );

			my $jobid = $job_ref->{jobid};
			my $jobpath = $job_ref->{path};
			my $jobopcoid = $job_ref->{opcoid};
			my $customerid = $job_ref->{customerid};
			my $location = $job_ref->{location};
			my $remotecustomerid = $job_ref->{remotecustomerid};
			my $customer_name = $job_ref->{customer_name};

			my $asset_types;
			my $assets;

			eval {
				# getJobAssetTypes uses table asset_types to query other tables to determine the enabled/checked attribute for each asset type
				$asset_types = ECMDBJobBundle::getJobAssetTypes( $jobid, $customerid );	# returns hash of hash { checked, enabled, label } for each asset type
				#push( @history, "lr asset " . Dumper(  $asset_types->{lr_asset} ) ); # see tracker3plus.lowres for lr_asset 
				$assets = ECMDBJobBundle::checkedAssetsExist( $jobid, $jobpath, $jobopcoid, $asset_types );
			};
			if ( $@ ) {
				my $error = $@;
				$error =~ s/ at .*//gs;
				$error =~ s/Error in checkedAssetsExist, //g;
				$error =~ s/, Failed to change directory.//g;
				push( @errormessages, $error );
				next;
			}

			my $asset_count = 0;
			my $assets_exist_and_enabled = 0;
			my @asset_names_exist;
			my @asset_names_exist_and_enabled;


			foreach my $k ( keys %$asset_types ) {
				my $h1 = $assets->{ $k };
				my $h2 = $asset_types->{ $k };

				$asset_count += 1 if ( $h1->{asset_exists} == 1 );
				push( @asset_names_exist, $k ) if ( $h1->{asset_exists} == 1 );

				$assets_exist_and_enabled++ if ( ( $h2->{enabled} == 1 ) && ( $h1->{asset_exists} == 1 ) );
				push( @asset_names_exist_and_enabled, $k ) if ( ( $h2->{enabled} == 1 ) && ( $h1->{asset_exists} == 1 ) );

				# if asset type already exists then modify checked, enabled and asset_exists for that particular asset type	
				if ( my @index_contains = grep{ $asset_list[$_]->{type} =~ /^$k$/i } 0..$#asset_list) {
	                $asset_list[$index_contains[0]]->{checked}      = $h2->{checked} if( $h2->{checked} == 1 );
    	            $asset_list[$index_contains[0]]->{enabled}      = $h2->{enabled} if( $h2->{enabled} == 1 );
        	        $asset_list[$index_contains[0]]->{asset_exists} = $h1->{asset_exists} if( $h1->{asset_exists} == 1 );
				} else {
					push( @asset_list, {
						type => $k, checked => $h2->{checked}, enabled => $h2->{enabled}, label => $h2->{label},
						asset_exists => $h1->{asset_exists}, files => $h1->{files},
					} );
				}
			}
			# if there exists no assets for a job then display error
			if ( $asset_count == 0 ) {
				push( @warningmessages, "No assets available for job $jobid" );
			} elsif ( $assets_exist_and_enabled == 0 ) {
				push( @warningmessages, "Job $jobid has assets available " . join(',', @asset_names_exist) . ", but none enabled" );
			} else {
				push( @history, "Job $jobid has assets available and enabled, " . join(',', @asset_names_exist_and_enabled ) );
				$jobs_with_assets_enabled ++;
			}

			# check for digital link errors

			my $a = $assets->{ "rendered_pdf" };
			my $at = $asset_types->{ "rendered_pdf" };
			if ( $at->{enabled} ) {
				push( @digital_link_errors, { error => "Job number: $jobid - The rendered asset does not exist, please use Create Digital URL " } ) unless( $a->{asset_exists} );
			} else {
				push( @digital_link_errors, { error => "Job number: $jobid - The digital URL does not exist, please use Create Digital URL" } );
			}

		}
		die "No jobs found with assets available and enabled" if ( $jobs_with_assets_enabled == 0 );

		# Time Capture-Products: File Delivery Check PD-2020

		if (ECMDBProducts::isProductsEnabled( $customerId ) == 1) {
			my $qcProdTime = ECMDBProducts::isQCProductTime( \@jobs );
			if ( $qcProdTime == 1 ) {
				#push( @errormessages, "Please confirm all QC checks have been completed" );
				push( @warningmessages, "Please ensure all QC checks have been completed" );
			} else {
				#push( @errormessages, "Job has not been QC approved" );
				push( @warningmessages, "Please ensure all QC checks have been completed" );
			}
		}

		my @sorted_asset_list = sort { $a->{label} cmp $b->{label} } @asset_list;

		# ps if we havent set %options yet, there is most likely an errormessage, so the fileoptions arent displayed in that case
 
		# Create target array of hash @file_types_for_display

		foreach my $key ( @file_type_list ) { # Use this array rather than keys @file_types_for_display to get the correct order displayed
			if ( exists $options->{ $key } ) {
				push( @file_types_for_display, { name => $key, on => $options->{$key}, checked => 0, label => $file_type_send_details{$key}->{name} } );
				push( @client_fts, $key ) if ( $key !~ "(zip_dir|rendered_pdf)" ) && ( $options->{$key} );
			} else {
				#print "file type $key missing\n";
			}
		}

		# If no assets enabled then enable high_res by default and set it checked too.
		if ( (scalar @file_types_for_display) == 0 ) {
			$options->{high_res} = 1;
			push( @file_types_for_display, { name => 'high_res', on => 1, checked => 1, label => $file_type_send_details{high_res}->{name} } );
			push ( @client_fts, 'high_res' );
		}

		# Assume that rendered_pdf is NOT picked up from customers

		push( @file_types_for_display, { name => 'rendered_pdf', on => 1, checked => 0, label => $file_type_send_details{rendered_pdf}->{name} } );

		my $customerfiletypes	= join( " ", @client_fts ); # eg "bw_asset bw_crp_asset aux_files";
		$vars = {
			jobs				=> \@jobs,
			customerId			=> $customerId,
			errormessages		=> \@errormessages,
			warningmessages		=> \@warningmessages,
 			additionalfiletypes => $additionalfiletypes,
			customerfiletypes	=> $customerfiletypes,
			opcoid				=> $opcoid,
			opconame			=> $opconame,
			applicationurl		=> $applicationurl,
			asset_list			=> \@sorted_asset_list,
			asset_errors		=> \@asset_errors,
			history 			=> \@history,
			digital_link_errors	=> \@digital_link_errors,
		};

	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		push( @errormessages, $error );
		$vars = { error => $error, errormessages => \@errormessages, warningmessages => \@warningmessages, history => \@history, asset_errors => \@asset_errors, };
	}

	# Allow JSON response, useful for test purposes
	if ( $mode eq 'json' ) {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

	# warn "Vars " . Dumper( $vars ) . "\n";

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/chooseDestination.html', $vars ) || die $tt->error(), "\n";
}

# Convert list of recipient id to list of email addresses.
# eg my @list = get_recipient_emails( \@recipients, \@warnings );

sub get_recipient_emails {
	my ( $recipients_ref, $warningmessages_ref ) = @_;

	my @recipientEmailList;
	# Handle recipient list as CC, ie dont copy to @emailto even if sendlinkcc is checked
	if ( ( scalar @$recipients_ref ) > 0 ) {
		for my $recipient ( @$recipients_ref ) {
			eval {
				my $recipientEmail = ECMDBOpcos::getCustomerRecipientEmail( $recipient ); # select email from customeraddresses where id=?
				push( @recipientEmailList, $recipientEmail );
			};
			if ( $@ ) {
				my $error = $@;
				$error =~ s/at module.*//g;
				push( @$warningmessages_ref, "Failed to get email address for recipient id $recipient, $error" );
			}
		}
	}
	return @recipientEmailList;
}

##############################################################################
# get_publication_for_jobs
# Get the publisher name for the jobs, ensure that the jobs all have the same publisher name
# Usage: $publicationName = get_publication_for_jobs( \@jobs, \@errormessages ); 
# Inputs
# - jobs_ref, array of job numbers, by ref
# - errormessage_ref, array of error messages, by ref
# Returns
# - publication name, can be undef (if errormessage returned)

# used by sendByEmail, sendByFtp
##############################################################################
sub get_publication_for_jobs {
	my ( $jobs_ref, $errormessages_ref ) = @_;

	my $publicationName;
	for my $job ( @$jobs_ref ) {
		my @job_details = ECMDBJob::db_getJobFields( $job );
		my $pubName;
		if ( ( scalar @job_details ) == 0 ) { 
			push( @$errormessages_ref, "Cant get job details for job $job" );
		} else {
			$pubName = $job_details[0]->{publication};
			if ( ( ! defined $pubName ) || ( $pubName eq "" ) ) {
				push( @$errormessages_ref, "No publication name for job $job" );
			} elsif (( defined $publicationName ) && ( $publicationName ne $pubName )) {
				push( @$errormessages_ref, "Publisher for job $job ($pubName) differs from previous job(s) $publicationName" );
			} else {
				$publicationName = $pubName;
			}
		}
	}
	return $publicationName;
}
	
##############################################################################
# Check that all jobs have a customer and it is the same for all jobs.
# Usage: $customerId = get_customer_for_jobs( \@jobs, \@errormessages );
##############################################################################
sub get_customer_for_jobs {

	my ( $jobs_ref, $errormessages_ref ) = @_;

	my $customerId;
	for my $job ( @$jobs_ref ) {
		my $jobCustId = ECMDBJob::db_getCustomerid( $job );
		if ( ( ! defined $jobCustId ) || ( $jobCustId <= 0 ) ) {
			push( @$errormessages_ref, "No customer for job $job" );
		} elsif ( ( defined $customerId ) && ( $jobCustId != $customerId ) ) {
			push( @$errormessages_ref, "Customer for job $job differs from previous job" );
			last;
		} else {
			$customerId = $jobCustId;
		}
	}
	return $customerId;
}
##############################################################################
# Check that all jobs have a project and it is the same for all jobs.
# Usage: $projectId = get_projects_for_jobs( \@jobs, \@errormessages );
##############################################################################
sub get_projects_for_jobs {

    my ( $jobs_ref, $errormessages_ref ) = @_;

    my $projectName;
    for my $job ( @$jobs_ref ) {
        my $jobProjName = ECMDBJob::db_getProjectJob( $job ) || "";
        if ( ( ! defined $jobProjName ) || ( $jobProjName eq "" ) ) {
            push( @$errormessages_ref, "No project for job $job" );
        } elsif ( ( defined $projectName ) && ( $jobProjName !~ /^$projectName$/ ) ) {
            push( @$errormessages_ref, "Project for job $job differs from previous job" );
            last;
        } else {
            $projectName = $jobProjName;
        }
    }
    return $projectName;
}

# Get override filenames for each job
# returns hash ref, indexed by job
# eg my $override_filenames = get_job_filename_overrides( $query, \@jobs );

sub get_job_filename_overrides {
	my ( $query, $jobs_ref ) = @_;

	my %override_filenames;
	for my $job ( @$jobs_ref ) {
		$override_filenames{ $job } = my $filename = $query->param( "overridefn_$job" ) || "";
	}
	return \%override_filenames;
}

##############################################################################
# Validate filenames for jobs to be sent (used by send_by_FTP and send_by_email)
# Inputs
# - jobs_ref, array of job numbers, by ref
# - customerId, id of customer (common to all the jobs in the list)
# - opcoid, id of opco for customer
# - override_fn, ref to hash of override filenames, index by job
# - filenameerror_ref, ref to filename error array
# - warning_ref, ref to warning message array
#
# Returns ( \%job_filenames, \%eff_job_filenames );
#
##############################################################################

sub get_job_filenames {
	my ( $jobs_ref, $customerId, $opcoid, $override_fn, $filenameerror_ref, $warning_ref ) = @_;

	my %job_filenames;
	my %eff_job_filenames; 
	my %unique;
	my $template = "";
	my @dummy_warnings;

	$template = ECMDBOpcos::getCustomerFilenameTemplate( $customerId ) if ( $customerId > 0 );
	$template = ECMDBOpcos::getOperatingCompanyFilenameTemplate( $opcoid ) if (( $opcoid > 0 ) && ( $template eq "" ));
	$template = "" unless ( defined $template );

	for my $job ( @$jobs_ref ) {
		my $job_filename = "";
		my $override_filename = $override_fn->{ $job } || "";

		# Validate the job filename overrides and create hash of effective filename for each job

		if ( $override_filename ne "" ) {
			push ( @$filenameerror_ref, "Filename for job $job has illegal characters" ) if ( $override_filename =~ /[^a-zA-Z0-9_]+/ );
			$job_filename = JobFilename::getJobFilename( $job, $template, \@dummy_warnings ) if ( $template ne "" );
		} else {
			$job_filename = JobFilename::getJobFilename( $job, $template, $warning_ref ) if ( $template ne "" );
		}
		$job_filenames{ $job } = $job_filename;

		my $eff_filename = $job_filename;
		$eff_filename = $override_filename . '.pdf' if ( $override_filename ne '' );
		$eff_job_filenames{ $job } = $eff_filename;

		# Filenames must be unique across the selection of jobs

		push( @$warning_ref, "Effective filename $eff_filename' is not unique, used by both $job and " . $unique{ $eff_filename } ) if exists $unique{ $eff_filename };
		$unique{ $eff_filename } = $job;

	}
	return( \%job_filenames, \%eff_job_filenames );
}


##############################################################################
# Do basic job checks for each job to make sure it is valid to send them.
# Usage check_jobs_for_send( $opcoid, $customerid, $subaction, $vars, \@jobs, \@errormessages, \@warningmessages );
# used by sendByFtp and sendByEmail
##############################################################################
sub check_jobs_for_send {
	my ( $opcoid, $customerid, $subaction, $vars, $jobs_ref, $errormessages_ref, $warningmessages_ref ) = @_;

	my $jobsPassed		= 1; 	#assume all jobs have passed initially
	my $jobsqcapproved	= 1;	# check if all selected jobs have been qc approved
	my $unapprovedjobs	= "";	# jobs that have NOT been qc'd
	my $isCreative		= 0;	# assume all jobs are NOT creative initially
	my @unapprovedjobs;		# list of unapproved jobs

	# if qc is enabled for this opcoid the need to check QC Status of each job
	my $isqcenabled		= ECMDBOpcos::db_isQCEnabled( $opcoid );
	if ( $isqcenabled ) {
		for my $job ( @$jobs_ref ) {
			if ( jobQCApproved ( $job ) != 1 ) {
				$jobsqcapproved = 0;
				push( @unapprovedjobs, $job );
			}
		}
		push( @$errormessages_ref, "You cannot send jobs unless they have all been QC Approved." ) if ( $jobsqcapproved == 0 );
	}

	my $preflightCheckDisabled = ECMDBOpcos::getCustomerPFCDisabled( $customerid );
	if ( $preflightCheckDisabled ) {
		push( @$warningmessages_ref, "Preflight Status check disabled" ) if ($subaction ne "sendbyproject"); 
	} else {
		for my $job ( @$jobs_ref ) {
			##If action is sendbyproject along with passed and dispatched we need to allow creative also.
			##To prevent the existing condition get impact handled it in differnt function as seding files is sensitive one
			if ($subaction ne "sendbyproject")
			{
				if ( jobPassedPreflight ( $job ) != 1 ) {
					$jobsPassed = 0;
				}
			}
			else
			{
                                if ( jobPreflightforproject ( $job ) != 1 ) {
                                        $jobsPassed = 0;
                                }
			}
		}
	}

	#check if any of these jobs is a 'creative' job - if it is can't send to vendor
	#If action is sendbyproject this check is not require
	if ($subaction ne "sendbyproject")
	{
		for my $job ( @$jobs_ref ) {
			if ( ECMDBJob::isCreative( $job ) == 1 ) {
				$isCreative = 1;
			}
		}
	}

	# error messages (lifted from selectVendor.html)

	push( @$errormessages_ref, "You cannot send jobs unless they have all passed the Pre Flight process" ) if (( $jobsPassed != 1 ) && ( $isCreative != 1 ));
	
	if ( $isCreative == 1  ) {
		if ( ( $subaction eq "Client" ) || ( $subaction eq "sendbyftp" ) ) {
			push( @$warningmessages_ref, "You are about to send jobs that are 'Creative' jobs." ) if ( $jobsqcapproved != 0 );
		} else {
			# subaction MediaOwner or Vendor
			push( @$errormessages_ref, "This is a creative job - cannot send" );
		}
	}

	# Set vars (although most not used in templates, but may be at some point)
	$vars->{jobsPassed} = $jobsPassed;
	$vars->{isCreative} = $isCreative;
	$vars->{isqcenabled} =  $isqcenabled;
	$vars->{jobsqcapproved} = $jobsqcapproved;
	$vars->{pfc_disable} = $preflightCheckDisabled;
	my $unapprovedjobslist = join( ",", @unapprovedjobs );
	$vars->{unapprovedjobs} = $unapprovedjobslist;
	
}

##############################################################################
# Verify that the user is not trying to send a Legacy job along with another job.
# The reason being that the Legacy job may create its own zip file (if sendhires enabled)
# but otherwise all the files are zipped up.
# Outputs:
#  @errormessages - message if more than one Legacy job present
# Returns: 0 or 1 (1 if Legacy job present)
# used by sendByFtp, sendByEmail
##############################################################################
sub legacy_job_check {
	my ( $jobs_ref, $errormessages_ref ) = @_;

	my $NumLegacyJobs = 0;
	my $NumNonLegacyJobs = 0;
	my @legacy_jobs;
	
	for my $job ( @$jobs_ref ) {
		my $isLegacy = ECMDBJob::db_isLegacy( $job );
		if ( $isLegacy ) {
			$NumLegacyJobs++;
			push ( @legacy_jobs, $job );
		} else {
			$NumNonLegacyJobs++;
		}	
	}
	if ( $NumLegacyJobs > 0 ) {
		my $legacyjob_list = join( ",", @legacy_jobs );
		if ( $NumNonLegacyJobs > 0 ) {
			push ( @$errormessages_ref, "Can't mix Legacy and non Legacy jobs, legacy jobs: $legacyjob_list" );
		} elsif ( $NumLegacyJobs > 1 ) { 
			push ( @$errormessages_ref, "Can't send more than one Legacy job at a time, legacy jobs: $legacyjob_list" );
		}
	}
	return ( $NumLegacyJobs > 0 ) ? 1 : 0;
}

##############################################################################
# getFileTypesFromQuery( query, session )
# Extracts file types (current the set of additional file types) from CGI query
# Returns hash, with keys for some or all of the file types (the key matches the 
# existing names used in query, such as bw_crp
# Used by sendByFtp, sendByEmail
##############################################################################

sub getFileTypesFromQuery {
	my ( $query, $session ) = @_;

	my %file_types;
	foreach my $ft ( @file_type_list ) {
		my $val = $query->param( $ft );
		$file_types{ $ft } = 0;
		$file_types{ $ft } = 1 if ( (defined $val) && ( $val eq "on") );
	}

	return \%file_types;
}

###############################################################################
# addFileType - add the specified file type to the hash of selected file types
# eg addFileType( $filetypes_ref, "high_res" ); 
# Input
# name - name of file type to add, use same name as used in query param
# Output
# filetypes_ref (address of hash), hash entry added for type
##############################################################################

sub addFileType {
	my ( $filetypes_ref, $name ) = @_;

	$filetypes_ref->{ $name } = 1;
}

##############################################################################
# setFileTypesForSend
#
# Sets file type specific variables ready for the template for either sendByEmail or sendByFtp.
#
# In theory, the pre send dialog displays the list of file types
# and all we do here is ensure they get copied to $vars so that they appear on the page
# and SendJobsViaFtp and FtpJobAssets just picks them out from $query.
# Inputs:
# - $filetypes_ref
# Outputs:
# - $vars (has keys set for each select file type $ft)
#   $vars->{$ft_on} = 1
#   $vars->{$ft} = $filetypes_ref->{$ft}
# and 
#   $vars->{file_types_for_display}	= \@file_types_for_display;
#   $vars->{file_type_list}		= $file_type_list;
# 
##############################################################################
sub setFileTypesForSend {
	my ( $filetypes_ref, $vars ) = @_;

	# Selected file types
	my %file_types;
	my $file_types_present = 0;
	my @file_types_for_display; # used in addn_file_options.html
	my @file_types_a;

	# Check first to see if any file types appear (at all) in the query (with any value)
	# Note re File types:
	# 1) The list of file types is coded as a list in @file_type_list, which has all the Press file types.
	# 2) The file types appear as check boxes (originally selectVendor) so for instance bw_asset is one
	#    of these types, appears in @file_type_list. We set bw_asset_on, to enable the checkbox
	#    and the value bw_asset is 0 or 1 for the unchecked/checked state.

	# Count the number of checkboxes ticked on entry
	foreach my $ft ( @file_type_list ) {
		my $h = $file_type_send_details{ $ft };
		if ( defined $h ) {
			push ( @file_types_for_display, {
				on => ( exists $filetypes_ref->{ $ft } )||0,
				checked => $filetypes_ref->{ $ft }||0,
				name => "$ft",
				label => $h->{name},
			} );
			push( @file_types_a, $h->{name} ) if ( $filetypes_ref->{ $ft }||0 == 1 );
		}
	}
	my $file_type_list = join( ", ", @file_types_a );

	$vars->{file_types_for_display}	= \@file_types_for_display;
	$vars->{file_type_list}		= $file_type_list;

	# Add the file types (set values 0 or 1) but suffix the key with "_on" for the template
	foreach my $ft ( @file_type_list ) {
		$vars->{ $ft."_on" } = 1; # Enable the checkbox
		$vars->{ $ft } = $filetypes_ref->{ $ft };
	}

}

##############################################################################
# Handler for action "sendByFTP" called either by selecting "Send by FTP"
# from the pre send dialog (see action "chooseDestination") or as a result of clicking the "Send" button.
##############################################################################
sub sendByFTP {
	my ( $query, $session ) = @_;

	my @errormessages;		# Array of validation error messages, things that cant be resolved
	my @warningmessages;		# warning messages, which can be resolved such as giving a filename override
	my @filenameerror;		# Set if error with override filename
	
	# The pre send dialog passes several parameters including the job list, the opcoid, customerid

	# Get list of jobs to send

	my $jobList = $query->param( "jobs" ) || "";
	$jobList =~ s/,$//; # remove spurious trailing comma
	my @jobs = split( ',', $jobList );

	my $opcoid = $session->param( 'opcoid' );
	my $customerId	= $query->param( "customerId" );
	my $ftpId = $query->param( "ftpid" );
	my $custommessage = General::htmlEditFormat( $query->param( "custommessage" )||"" );
	my $userid = $session->param( 'userid' );

	my @ftpdeliveries;
	my @addressees;
	my @recipients;  # these people will get notified ( if selected )
	{
		# see http://www.perlmonks.org/?node_id=1140831
		$CGI::LIST_CONTEXT_WARN = 0;
		@recipients = $query->param( "recipient" );
	}
	if ( ( scalar @recipients ) == 0 ) {
		my @recipients2     = ECMDBOpcos::getCustomerAddressees( $customerId ) if ( defined $customerId );
		foreach my $r ( @recipients2 ) {
			push( @recipients, $r->{id} );   # hash entry has id, email we just want id here
		}
	}

	# Get asset types specified and include high_res by default if none selected

	my $filetypes_ref = getFileTypesFromQuery( $query, $session );
	addFileType( $filetypes_ref, "high_res" ) if ( ( scalar keys %{ $filetypes_ref } ) == 0 );

	# Validate things associated with the jobs whether or not the user has pressed the Send button

 	# Customer id should be supplied from pre send dialog
	# For time being check that all jobs have a customer and it is the same for all jobs.

	$customerId = get_customer_for_jobs( \@jobs, \@errormessages ) if ( ! defined $customerId );
	$opcoid = ECMDBOpcos::getCustomerOpcoid( $customerId );

	# Check Legacy jobs are sent on their own
	my $isLegacy = legacy_job_check( \@jobs, \@errormessages );

	# get the current filename for each job (based on opcoid, customer)

	# Build hash of override filenames, indexed by job number
	my $override_filenames = get_job_filename_overrides( $query, \@jobs ); # returns hash ref

	# Get effective filename for each job
	my ( $job_filenames_ref, $eff_job_filenames_ref ) = get_job_filenames( \@jobs, $customerId, $opcoid, $override_filenames, \@filenameerror, \@warningmessages );

	# If the user has clicked the Send button, validate the customerId and ftp id and if ok, send the jobs

	my $formPosted = $query->param( "formPosted" ) || "";
	if ( $formPosted eq "yes" ) {

		push( @errormessages, "Customer Id not defined" ) unless defined $customerId;
		push( @errormessages, "Opco Id not defined" ) unless ( defined $opcoid ) && ( $opcoid != 0 );
		push( @errormessages, "FTP Id not defined (FTP Server not selected)" ) unless defined $ftpId;

		# The send_by_ftp doesnt allow you to select a list of recipients, or pass it back....
		#if ( ( scalar @recipients ) == 0 ) {
		#	push( @errormessages, "No recipients defined" );
		#}

		push( @errormessages, "One or more warning messages" ) if ( ( scalar @warningmessages ) > 0 );
		push( @errormessages, "One or more filename error messages" ) if ( ( scalar @filenameerror ) > 0 );


		if ( ( scalar @errormessages ) == 0 )  {
			my @messages;
			my $rc;
			eval {
				$rc = SendJobsViaFtp( $customerId, $opcoid, $filetypes_ref, $ftpId, $custommessage, $userid, 
						\@jobs, \@recipients, $eff_job_filenames_ref, \@messages );
			};
			if ( $@ ) {
				push ( @errormessages, $@ );
			} elsif ( $rc == 0 ) {
				# Failed to copy (or queue request to copy)
				foreach my $msg ( @messages ) {
					push ( @errormessages, $msg );
				}
			} else {
				my $vars = {
					job => \@jobs,
					joblist => $jobList,
					messages => \@messages,
				};
				General::setHeaderVars( $vars, $session );
				$tt->process( 'ecm_html/SendJobsFTPConfirmation.html', $vars ) || die $tt->error(), "\n";
				return;
			}
		}
	}

	# Get here on initial entry or after validation failure

	my $isftpenabled	= ECMDBOpcos::db_isFTPEnabled( $opcoid );
	my $filenameoverride	= ECMDBOpcos::db_getFilenameoverride( $opcoid );

	# get enhancedFTP status for this customer
	my $isEnhancedFTP = ECMDBOpcos::isEnhancedFTP( $customerId );

	# handle the Enhanced FTP condition the JOB LOCATION must be same as the opcoid
	for my $job ( @jobs ) {
		my $location = ECMDBJob::db_getLocation( $job );
		# if enhanced FTP is ON for this job
		if ( ECMDBJob::isEnhancedFTP ( $job ) ) {
			#print "Enhanced FTP ON<br>, Opcoid: $opcoid<br> Location: $location<br>";
			if ( $opcoid != $location ) {
				my $location_opconame = ECMDBOpcos::getOpcoName( $location );
				push( @warningmessages, "Job $job is checked out to '$location_opconame'" ); #
				last;
			}
		}
	}

	if ( ( defined $customerId ) && ( $customerId > 0 ) ) { # we have a customer id - pull in ftp list for this customer
		@ftpdeliveries 	= ECMDBOpcos::getCustomerFTPs( $customerId ); 
		@recipients 	= ECMDBOpcos::getCustomerAddressees( $customerId ); 
	}

	my $vars = {
		jobs			=> \@jobs,
		joblist			=> $jobList,
		customerId		=> $customerId,
		errormessages		=> \@errormessages,
		warningmessages		=> \@warningmessages,
		recipients		=> \@recipients,
		addressees		=> \@addressees,
 		filenameoverride	=> $filenameoverride,	# Override filenames provided by user, hash { job }
		filenames		=> $job_filenames_ref,  # Job filenames before override, hash { job }
		fn_overrides		=> $override_filenames, # Override filenames, hash { job }
		filenameerror		=> \@filenameerror,
		ftpid			=> $ftpId,
		isftpenabled		=> $isftpenabled,
		isEnhancedFTP		=> $isEnhancedFTP,
		ftpdeliveries		=> \@ftpdeliveries,
		applicationurl		=> $applicationurl,
	};

	check_jobs_for_send( $opcoid, $customerId, "sendbyftp", $vars, \@jobs, \@errormessages, \@warningmessages );

	# Set the flags for each file type and create a details hash and summary list

	setFileTypesForSend( $filetypes_ref, $vars );

	$vars->{oktogo} = ( ( scalar @errormessages ) == 0 ) ? 1 : 0;

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/send_by_ftp.html', $vars ) || die $tt->error(), "\n";
}

# Returns a summary (CI Details) of a job, given a job number
# Used by sendByEmail to provide a summary of the job details

sub email_job_summary {
	my ( $jobno ) = @_;

	# The keys and order of this table determine the order of CI details
	# the key names match those returned by ECMDBJob::db_getJobFields.
	# The values are the label text to show next to the detail.
	# nb szwidth and szheight are treated as a special case.
	# keys returns (coverdate, agency, client, headline, formatsize, publication, matdate, project)
	# so foreach has list in desired order.

	my %jobattribs = ( 
		agency => "Agency",
		client  => "Client",
		project => "Project",
		headline => "Headline",
		publication => "Publication",
		matdate => "Material date",
		coverdate  => "Cover date",
		formatsize => "Format size",
	);

	my $summary = "";

	my @jobdetails = ECMDBJob::db_getJobFields( $jobno );
	#Returns either 0 or an array but how to handle both?
	if ( ( scalar @jobdetails ) > 0 ) {
		my $details = $jobdetails[0];
		my $jobtext = "";
		# Use literal array of key names instead of keys to enforce correct order
		foreach my $key ( 'agency', 'client', 'project', 'headline', 'publication', 'matdate', 'coverdate', 'formatsize' ) { # was keys %jobattribs but it returns wrong order
			my $label = $jobattribs{$key};
			if ( ( exists $details->{$key} ) && ( defined $details->{$key} ) ) {

				$jobtext .= "  $label: " . $details->{$key} . "\n";
			}
		}
		if ( ( defined $details->{szheight} ) && ( defined $details->{szwidth} ) ) { # Both height and width
			$jobtext .= "  SzHeight x SzWidth: $details->{szheight}x$details->{szwidth}\n";
		} else {
			$jobtext .= "  SzHeight: $details->{szheight}\n" if defined $details->{szheight};
			$jobtext .= "  SzWidth: $details->{szwidth}\n" if defined $details->{szwidth};
		}
		$summary = "Details for job $jobno\n$jobtext\n";
	}
	return $summary;
}

# Function to transfer all the job assets (via FTP) then create ZIP file
# Inputs 
# - many
# Outputs 
# - @$messages
# Returns $results_file, name of PDF or ZIP file created
# Throws exception failed to conect via FTP or failure to create ZIP file (or invalid arguments)

sub ftp_job_assets_and_zip {
	my ( $userid, $username, $subaction, $uuidstr, $tempDir, $jobs, $opcoid, $customerId, $isLegacy, $eff_filenames, $filetypes_ref, $digitalRenderedPdfFldr, $messages ) = @_;

	my $filesCopied = 0;
	my $result_file;

	die "Invalid opcoid" unless ( ( defined $opcoid ) && looks_like_number( $opcoid ) );
	die "Invalid customerid" unless ( ( defined $customerId ) && looks_like_number( $customerId ) );

	# Get the FTP connect details
	my ( $ftpserver, $ftpusername, $ftppassword ) = getFTPDetails( $opcoid ) ;
	die "FTP Server not defined for Operating company ($opcoid)" if ( ( ! defined $ftpserver ) || ( $ftpserver eq "" ) );

	foreach my $jobid ( @$jobs ) {

		my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobid );  # Path has no trailing slash
		die "Job $jobid doesnt exist" unless defined $jobpath;

		my $jobFilename = $eff_filenames->{ $jobid };

		print "Transfer job $jobid, filename $jobFilename, path $jobpath\n" if ( $debug );

		eval {  # Trap exception for individual job

			if ( $isLegacy ) { # Job is legacy, hence only Hi-Res and one job only

				push( @$messages, "Copying the Legacy Hi Res" );

				$result_file = FtpGetJobAsset( $jobpath . "/OUTPUT_FILE/" . $jobid . ".zip", 
								$tempDir . $jobid . ".zip",
								$ftpserver, $ftpusername, $ftppassword );

				die "Legacy Hi-Res ZIP doesnt exist $result_file" unless ( -e $result_file );
				$filesCopied++;
				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "$username FTPd Hi-Res ZIP (Legacy)" );
	
			} elsif ( $subaction eq "Digital" ) { # rendered PDF (again just one job) -- ASSUME it cant be renamed

				push( @$messages, "Copying the Digital Rendered PDF type" );
	
				my $externalUrlJobRef = ECMDBJob::db_getexternalUrlJobRef( $jobid );
				# For subaction Digital, we get the Renderer PDF (one job only) and attach to the email
				$result_file = FtpGetJobAsset( $jobpath . '/' . $digitalRenderedPdfFldr . '/' . $jobid . '.pdf', 
								$tempDir . '/' . $externalUrlJobRef . '.pdf',
								$ftpserver, $ftpusername, $ftppassword );

				die "Renderer PDF doesnt exist $result_file" unless ( -e $result_file );
				$filesCopied++;
				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "$username FTPd Rendered PDF (Digital Link)" );
	
			} else { # any combination of file types including hires

				push( @$messages, "Subaction $subaction, Copying the other asset types" ) if $debug;

				# Filenames variable has correct name, either original value or rename value supplied by user
				my $assetsCopied = FtpJobAssets( $tempDir, $jobid, $jobpath,
							$ftpserver, $ftpusername, $ftppassword, 
							$jobFilename, $filetypes_ref, $messages );
				$filesCopied += $assetsCopied;
				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "$username FTPd $assetsCopied job asset(s)" );
			}
		};
		if ( $@ ) { # Handle exception for individual job
			my $error = $@;
			$error =~ s/at module.*//g;
			push( @$messages, "Failed to copy assets for job $jobid, $error" );
		}
	} # end foreach job number

	die "No assets copied" if ( $filesCopied == 0 );

	if ( ! defined $result_file ) {
		print "Creating ZIP file, zipcmd is $zipcmd\n" if $debug;
		push( @$messages, "Creating ZIP file" );
		if ( $debug ) { # Called from unit test at end
			print "Result_file is undef\n";
			print "Temp dir contains " . `ls $tempDir` . "\n";
		}
		# Anything else gets zipped up
		my $dst = "$uuidstr.zip";
		my $cmd = "cd '$tempDir'; $zipcmd -rq '$dst' . ; chmod 777 '$dst'";
		system( $cmd );
		$result_file = $tempDir . $uuidstr . ".zip";
		print "Created $result_file, size " . ( -s $result_file ) . "\n"; 
		die "Failed to create ZIP file '$result_file'" if ( ! -f "$result_file" );
		push( @$messages, "Created ZIP file '$result_file'" );
	}

	return $result_file;
}

######################################################################################################################
# Handler for action "sendByEmail" with subactions "Vendor", "MediaOwner" and "Client"
# Called from the pre send dialog (see action "chooseDestination") or as a result of clicking the "Send" button.
#
# For subaction "Vendor", we create a list of vendors @vendors (id, companyname, email) and use this to
# display the Vendor drop down, getting back vendorId, which is currently passed to FtpJobAssets.
#
# For subaction "MediaOwner", we send to a specific email associated with the publication. The publication name
# is in tracker3.publication but we have to make a web service call to CMD with that name to get the email
# address. So we check here to make sure that all the jobs passed have the same publication name.
# 
# For subaction "Client" the user just enters a free form email address and apart from usual email validation, 
# we just use that as is.
#
# For both "MediaOwner" and "Client" we have to create a temporary userid and password, pass these in the email
# and add these credentials to the temporary users table.
#
# Inputs
# - session.(userid, username, fname, lname)
# - session.opcoid (would this be valid to use?)
# - query.(action, subaction), subaction is one of "Vendor", "MediaOwner", "Client" or "Digital"
# - query.jobs, list of job numbers to send
# - query.<ft> where ft is one of many file types eg bw_asset
# (and on subsequent entries ...)
# - query.customerId (customerid of all the jobs)
# - query.recipient, list of possible recipients
# - query.(emailsubject, emailcontent)
# - query.vendorId if subaction Vendor
# - query.publicationEmail if subaction Media Owner
# - query.(email, emailcc, sendlinkcc) if subaction Client or Digital
# - query.uuidstr
# - query.formPosted is yes if form posted, ie user clicks on Send button
# 
######################################################################################################################
sub sendByEmail {
	my ( $query, $session ) = @_;

	my @errormessages;		# Array of validation error messages, errors which cant be corrected
	my @warningmessages;		# Messages that mean the user can correct the error
	my @filenameerror;		# Set if error with override filename
	my @messages;			# Messages saying what happended for each job (during FTP)
	my $tempDirectory;		# Place to hold job assets (from FTP) and ZIP file

	my $linkplaceholder = "The link goes here, please do not delete this placeholder";
	my $targetType;			# One of "Vendor", "Publication", "Client"
	my $targetName;
	my $result_file;		# Name of file produced in temp directory either PDF or ZIP file
	my $override_filenames;		# Hash ref - override filenames per job, as supplied by user
	my $job_filenames_ref;		# Hash ref - filename for job, using customerid to get template and job attributes
	my $eff_job_filenames_ref;	# Hash ref - effective filename for job
	
	# Things which are subaction specific	

	my $vendorId;
 	my $vendorname;
	my @vendors;
	my $publicationName = ""; 
 	my $publicationEmail;
	my @publicationEmailList;	# array of hash { email address, country }
	my $email; 			# Used by send to Vendor or Media Owner
	my @emailto;			# Used by Send to Client (has one or more To/CC fields)
	my @emailcc;
	my @recipients;			# these people will get notified ( if selected )
	my $sendlinkcc = "";
	my @bundlelinks;
	my $filenamespanel = 1; # PD-3511 remove jobs filename panel for send files (digital link email option)
	# Session parameters

	my $opcoid = $session->param( 'opcoid' ); # really should be derived from jobs customerid

	# Get user full name if available for end of email.

	my $username = $session->param( 'username' );
	my $fname = $session->param("fname") || "";
	my $lname = $session->param("lname") || "";
	my $userfullname = $username;
	$userfullname = "$fname $lname" if (( $fname ne "" ) && ( $lname ne "" ) );
	my $userid = $session->param( 'userid' ); # used in call to db_recordJobAction
	my $useremail = ECMDBUser::db_getEmail( $userid ); # get the email of this user so we can populate 'from'

	# Query parameters

	# action and subaction, note that we allow subaction = "" as dialog can be run in standalone mode, and choose subaction
	my $action = $query->param( "action" );
	my $subaction = $query->param( "subaction" ) || ""; # Should be "Vendor", "MediaOwner", "Client" or "Digital"
	if ( $action ne "sendByEmail" ) {
		( $subaction ) = $action =~ /sendByEmail(.*)/;
		$subaction = "Client" unless defined $subaction;
	}

	#system("echo \"send by email activated:\n action: $action \n subaction: $subaction \"> zlog");

	# The pre send dialog passes several parameters including the job list, the opcoid, customerid

	# Get list of jobs to send

	my $jobList = $query->param( "jobs" ) || "";
	$jobList =~ s/,$//; # remove spurious trailing comma
	my @jobs = split( ',', $jobList );
    my @jobnosArray = ();

	my $customerId	= $query->param( "customerId" );
	my $customer_name	= $query->param( "custname") || "";

	if ( $customer_name )
	{
		$customerId = ECMDBOpcos::getCustomerID($customer_name, $opcoid);
	}

	# Customer visibility check
	$customerId = get_customer_for_jobs( \@jobs, \@errormessages ) if ( ! defined $customerId );
	my $vc = ECMDBCustomerUser::getUserCustomerIdsByOpco( $userid, $opcoid );
	my @visible_customers = @$vc;
	my @found;
	die "Customer not visible" unless ( @found = grep { $_ eq $customerId } @visible_customers );

	{
		# Avoid the CGI::param called in list context error message
		# see http://www.perlmonks.org/?node_id=1140831
		$CGI::LIST_CONTEXT_WARN = 0;
		@emailto = $query->param( "email" );
		@emailcc = $query->param( "emailcc" );
		@recipients= $query->param( "recipient" ); # these people will get notified ( if selected )
	
	}
	$sendlinkcc = $query->param( "sendlinkcc" ) || "off"; # Checkbox "on" if checked

	# Handle recipient list as CC, ie dont copy to @emailto even if sendlinkcc is checked
	my @recipientEmailList = get_recipient_emails( \@recipients, \@warningmessages );

	# The user can edit the subject and email content
	my $emailsubject = $query->param( "emailsubject" );
	my $emailcontent = $query->param( "emailcontent" );

	my $formPosted = $query->param( "formPosted" ) || "";
	# $formPosted = "no"; # uncomment when debugging, so it just redisplays the form

	my $isLegacy; # "yes" or "no" - yes if just one job and thats a Legacy job

	# UUID is used to create a unique key which we use for the folder and zip name
	# but also have to display it, so pass it to the dialog, so we get the same value each time
	# and the email preview will also contain it.

	my $uuidstr = $query->param( "uuidstr" );
	$uuidstr = getUUIDstr() if ( ! defined $uuidstr );

	# Get the various and many filetype parameters from the query, include high_res by default if none selected
	my $filetypes_ref = getFileTypesFromQuery( $query, $session );
	addFileType( $filetypes_ref, "high_res" ) if ( ( scalar keys %{ $filetypes_ref } ) == 0 );


	##PD-2673 - 'Sending Files for Index'. Not through Action->Sendfiles->Selection->Post->Email page. Skipping all this. Directly having 'Send File' button in Index page.
	##When user click it has to directly go to Email page. So the below checking is not required.
	if ( $subaction !~ /^$|^Index$/ )
	{
			eval {

				# Validate things associated with the jobs whether or not the user has pressed the Send button
				die "No jobs selected" unless ( ( scalar @jobs ) > 0 );
				die "Only one job allowed for Digital" if ( ( $subaction eq "Digital" ) && ( ( scalar @jobs ) > 1 ) );

				# For time being check that all jobs have a customer and it is the same for all jobs.

				$customerId = get_customer_for_jobs( \@jobs, \@errormessages ) if ( ! defined $customerId );
				$opcoid = ECMDBOpcos::getCustomerOpcoid( $customerId ) if ( defined $customerId );
				
				# The customerId is determined by the list of jobs but the user determines the Vendor id by selecting the Vendor email address.

				if ( ( $formPosted eq "yes" ) && ( $subaction ne "" ) ) {

					die "Customer Id not defined" if ( ! defined $customerId );
					die "Opco Id not defined" if ( ! defined $opcoid ) || ( $opcoid == 0 );

					# could use general::isValidEmail
					die "Invalid user email address" if ( defined $useremail ) && ( $useremail !~ /^\w[\w\.\-]*\w\@\w[\w\.\-]*\w(\.\w{2,4})$/ );
				}

				# Check Legacy jobs are sent on their own
				$isLegacy = legacy_job_check( \@jobs, \@errormessages );

				# get the current, override and effective  filename for each job (based on opcoid, customer)

				# Build hash of override filenames, indexed by job number
				$override_filenames = get_job_filename_overrides( $query, \@jobs ); # returns hash ref

				# Get effective filename for each job
				( $job_filenames_ref, $eff_job_filenames_ref ) = get_job_filenames( \@jobs, $customerId, $opcoid, $override_filenames, \@filenameerror, \@warningmessages );

				# Determine email address according to action, as well as targetType (Vendor/Publication) and targetName (Vendor Name(id)/Publication Name)

				$email = "unknown"; # Resultant email to send to
				if ( $subaction eq "Vendor" ) {
					$targetType = "Vendor";
					$vendorId = $query->param( "vendorId" ); # used in sendByEmail (really send by vendor)
					@vendors = getVendors( $opcoid ) if ( defined $opcoid );
					die "No vendors for opco $opcoid" if ( ( defined $opcoid ) && ( scalar( @vendors ) == 0 ) );

				} elsif ( $subaction eq "MediaOwner" ) {
					$targetType = "Publication";
					$publicationEmail = $query->param( "publicationEmail" );  # Selected from list for publisher name ( email, country )
					# $publicationName = $query->param( "publicationName" );
					$publicationName = get_publication_for_jobs( \@jobs, \@errormessages );
					if ( $formPosted ne "yes" ) {
						# $publicationName = get_publication_for_jobs( \@jobs, \@errormessages );
						# die "No publication name for the selected jobs" unless ( defined $publicationName );
						if ( defined $publicationName ) {
							$targetName = $publicationName;

							# now returns array of hash ( email, country ), need country from front end too
							@publicationEmailList = getPubEmailFromPubName( $publicationName );
							#
							# For test purpose add a test email on the end
							my $tag_env = $ENV{'TAG_ENV'} || "DEV";
							push( @publicationEmailList, { email => $testSendEmail, country => "UK" } ) if ( lc( $tag_env ) ne "live" );
							#
							#push( @warningmessages, Dumper( @publicationEmailList ) ) if $debug;
							my $listSize = scalar ( @publicationEmailList );
							die "No emails for publication '$publicationName'" if ( $listSize == 0 );
							#push( @warningmessages, "Pub email list size $listSize" );
							if ( $listSize == 1 ) {
								my $entry = $publicationEmailList[ 0 ];
								#push( @warningmessages, Dumper( $entry ) );
								$publicationEmail = $entry->{email};
							}
						}
					}
					# $email = 'streanor@tagworldwide.com'; # for testing only (as you cant override pub email otherwise)

				} elsif ( ( $subaction eq "Client" ) || ( $subaction eq "Digital" ) ) {
					$targetType = $subaction;
					# PD-1417 is not specific as to the email address handling

				} else {
					# Allow case of subaction = "" for testing
					#push( @warningmessages, "Please select subaction" );
				}
			};

			if ( $@ ) {
				my $error = $@;
				$error =~ s/at module.*//g;
				push( @errormessages, $error );
			}
	}

	# Warning and file (rename) error messages must be resolved before send
	if ( $formPosted eq "yes" ) {
		die "Customer Id not defined" if ( ! defined $customerId );
		die "Opcoid not defined" unless ( defined $opcoid );

		# Squidge email to/cc arrays
		# filter out blank entries in emailto, emailcc arrays
		for ( my $i = ( scalar @emailto ) -1; $i >= 0; $i-- ) {
			delete $emailto[ $i ] if $emailto[ $i ] eq "";
		} 
		for ( my $i = ( scalar @emailcc ) -1; $i >= 0; $i-- ) {
			delete $emailcc[ $i ] if $emailcc[ $i ] eq "";
		} 
		if ( $subaction eq "Client" ) {
			if ( (scalar @emailto) == 0 ) {
				push( @warningmessages, "Please enter an email address" );
			} else {
				$targetName = $emailto[0];
			}

		} elsif ( $subaction eq "Digital" || $subaction eq "Index" ) {
			# Doesnt need email address as download is streamed

		} elsif ( $subaction eq "MediaOwner" ) {
			if ( ( ! defined $publicationName ) || ( $publicationName eq "" ) ) {
				push( @warningmessages, "Publication name is undefined" );
			} elsif ( ( ! defined $publicationEmail ) || ( $publicationEmail eq "" ) ) {
				push( @warningmessages, "Please select Publication email" );
			} else {
				$targetName = $publicationName;
				# Publication Email can be list of emails seperated by semi colon
				$email = $publicationEmail;
				@emailto = split /;/, $publicationEmail; # Should really squidge the cc list again
			}

		} elsif ( $subaction eq "Vendor" ) {
			if ( ! defined $vendorId ) {
				push( @warningmessages, "Vendor Id not defined (not selected)" );
			} else {
				$email = ECMDBUser::db_getEmail( $vendorId );
				push( @emailto, $email );
				$vendorname = ECMDBUser::db_getUsername( $vendorId );
				$targetName = "$vendorname ($vendorId)";
			}
		}
		push( @errormessages, "No sub action selected" ) if ( $subaction eq "" );
		push( @errormessages, "One or more warning messages" ) if ( ( scalar @warningmessages ) > 0 );
		push( @errormessages, "One or more filename error messages" ) if ( ( scalar @filenameerror ) > 0 );
	}

	# Do the transfer of files from the Jobflow studios to the temp directory

	# commented by anbu as we dont require ftp process because we use cdn

	##PD-2673 -  No files for transfer/FTP for index so skipping it
	# if ( $subaction !~ /^$|^Index$/ ) {
	#	if ( ( $formPosted eq "yes" ) && ( ( scalar @errormessages ) == 0 ) ) {
	
	#		eval {
					# commented by anbu as we dont require ftp process because we use cdn
					# Temp directory has to exist in /temp/$uuidstr for the script stream/getvendorasset.pl to find it.
	#				$tempDirectory = getTempDir( $uuidstr );
	#				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobs[0], "Created temp dir $tempDirectory" );

					# ftp_job_assets_and_zip returns name of PDF or ZIP file but we dont use it here (file exists in $tempDir)

	#				$result_file = ftp_job_assets_and_zip( $userid, $username, $subaction, $uuidstr, $tempDirectory, \@jobs, $opcoid, $customerId, $isLegacy, 
	#					$eff_job_filenames_ref, $filetypes_ref, $digitalRenderedPdfFldr, \@messages );
	#		};
	#		if ( $@ ) {
	#			my $error = $@;
	#			$error =~ s/at modules.*//;
	#			push ( @errormessages, $error );
	#			foreach my $msg ( @messages ) { # Add FTP messages to warningmessages
	#				push ( @warningmessages, $msg );
	#			}
	#		}
	#	}
	#}

	# Now the sending of the email

	if ( ( $formPosted eq "yes" ) && ( ( scalar @errormessages ) == 0 ) ) {
		eval {	
			# If user checks the send to CC checkbox treat as per TO
			if ( ( scalar ( @emailcc ) > 0 ) && ( $sendlinkcc eq "on" ) || ( $subaction =~ /^Index$|^Digital$/ ) ) { #PD-2920_Digital_Email_Content_Mismatch_for_CC_person when $sendlinkcc is unchecked.
				foreach my $cc ( @emailcc ) { push( @emailto, $cc ) };
				@emailcc = ();
			}

			# Handle recipient list as CC, ie dont copy to @emailto even if sendlinkcc is checked
			foreach my $r ( @recipientEmailList ) {
				push( @emailcc, $r );
			}

			my $email_to_list = join( ",", @emailto );
			my $email_cc_list = join( ",", @emailcc );  # modules/Mailer.pl doesnt allow cc to be distinguished
	
			# For the TO (& CC users if the sendlinkcc checkbox is checked), send the main email content plus link and credentials

			my $emaildownloadlink	= undef;
			my $externalUrlJobRef	= undef;

			if ( $subaction eq "Digital" ) {
				$externalUrlJobRef = ECMDBJob::db_getexternalUrlJobRef( $jobs[0] );
			} 
			elsif ($subaction eq "Index")
			{
				$targetType=$subaction;
				push (@messages, "Index link sent");
			}
			else {
				$emaildownloadlink = "$applicationurl?action=vendorAssetDownload&id=$uuidstr&joblist=$jobList";
				$emaildownloadlink .= "&vendorId=$vendorId" if ( defined $vendorId );
			}

			# Create CDN bundle for each job
			my $creator = $session->param( 'userid' );
			my $expiry_dt = Config::getConfig( "download_expiry" ) || 1;
			
			if ( $subaction !~ /^$|^Index|Digital$/ ){ # bundle creation is not required for digital and index
			foreach my $jobid (@jobs){
				Validate::job_number( $jobid );

				# get the job path
				my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobid );
				die "Job $jobid does not exist" unless defined $jobpath;

				# get job asset types
				my ($asset_types) = ECMDBJobBundle::getJobAssetTypes($jobid, $customerid);
				my $assets = ECMDBJobBundle::checkedAssetsExist( $jobid, $jobpath, $jobopcoid, $asset_types );

				# loop through job specific asset types
				my @assets;
				my @asset_lables;
				foreach my $asset_type ( keys %$asset_types ) {
					my $h1 = $assets->{ $asset_type };
					my $h2 = $asset_types->{ $asset_type };

					if( $h2->{checked} == 1 && $h2->{enabled} == 1 && $h1->{asset_exists} == 1 ){
						my $a = $query->param( $asset_type ) || '';
						my $l = $h2->{label};
						push( @assets, $asset_type ) if ( $a eq 'on' );
						push( @asset_lables, $l ) if ( $a eq 'on' );
					}
				}
				my $asset_str = join( ',', @assets );
				my $asset_label_str = join( ', ', @asset_lables );

				my $ug      = new Data::UUID;
				my $uuid    = $ug->create();
				my $uuidstr = $ug->to_string( $uuid );

				# call create bundle with selected assets if assets exists else log the error
				if ( scalar(@assets) ){
					die "Assets empty for Job $jobid" unless defined $asset_str;

					my $job_filename = $eff_job_filenames_ref->{ $jobid };
					$job_filename =~ s/[^a-zA-Z0-9_]/_/;
					$job_filename =~ s/_pdf$/.pdf/;  # we rename "_pdf" to ".pdf" as previous regex replaces ALL .'s with _'s

					my ( $bundle_id, $bundle_uuid ) = ECMDBJobBundle::create( $jobid, $creator, $expiry_dt, $asset_str, $uuidstr, $job_filename );
					ECMDBAudit::db_auditLog( $creator, "JOBBUNDLE", $jobid, "Created job bundle $bundle_id, assets $asset_str " );

					push( @bundlelinks, {job_number => $jobid, bundleid => $bundle_id, bundle_uuid => $bundle_uuid, assets => $asset_str, asset_labels => $asset_label_str} ) if ( $bundle_id );
				}else{
					ECMDBAudit::db_auditLog( $creator, "JOBBUNDLE", $jobid, "Job bundle has not been created, no assets available" );
				}
			}
			}
			# End of bundle creation

			# $result_file =  $tempDirectory . $uuidstr . ".zip" unless ( defined $result_file );
			foreach my $email ( @emailto ) {
				eval {
					# my $email_link_copy = $emaildownloadlink;
					my $email_link_copy = "";
					my $temp_user = Users::getTempUsername();
					my $temp_pwd = Users::getTempPassword();

					# supply bundle links to email content 
					if ( $subaction !~ /^$|^Index|Digital$/ ){
					foreach ( @bundlelinks ){
						my $job = $_->{job_number};
						my $bundleid = $_->{bundleid};
						my $bundle_uuid = $_->{bundle_uuid};

						my $url_link = 'Click <a href="' . $bundle_url . '?action=downloadbundle&bundleid=' . $bundle_uuid . '">here</a> to download assets for job ' . $job;
						$email_link_copy = $email_link_copy."\n".$url_link."\n";

						# add job bundle recipients
						ECMDBJobBundle::addRecipient($bundleid, $email, $temp_user, $temp_pwd);
					}
					}

					if ( ( $subaction eq "MediaOwner" ) || ( $subaction eq "Client" ) ) {
						# my $comment = "Email $email Jobs $jobList Zip $result_file";
						my $comment = "Email $email Jobs $jobList";
						$comment = substr( $comment, 0, 254 ) if ( length( $comment ) > 254 );
						ECMDBUser::addUserTemp( $temp_user, Users::EncryptPwd( $temp_pwd ), $comment, $email );
						$email_link_copy .= "\nIf you do not have a Jobflow account, please use the temporary credentials below, when logging on. \n";
						$email_link_copy .= "\n<b>User ID: </b>$temp_user\n";
						$email_link_copy .= "\n<b>Password: </b>$temp_pwd\n";
						$email_link_copy .= "\nThese temporary credentials will expire in 7 days. \n";
					}

					# Substitute the link (& potentially the temp credentials), or append if cant find placeholder
					my $email_body = $emailcontent;
					if ( $email_body =~ /\[$linkplaceholder\]/ ) {
						$email_body =~ s/\[$linkplaceholder\]/$email_link_copy/;
					} else {
						$email_body .= $email_link_copy;
					}

					# Send email or send email with attachment
					my $options = { dieonerror => 1, host => $smtp_gateway }; # send_mail would return 0 or 1 otherwise

					if ( $subaction eq "Digital" ) { 
						# my $attachment = { filename => "$result_file", contenttype => 'application/pdf' };
						# $options->{attachments} = $attachment;

					} elsif ( $subaction =~ /^Index$/ ) {

						#Attached Cust and Brand logos as array of hash.
						my @attachments;
						my ( $currDir, $digitalIndexCustLogoPrefixName, $digitalIndexCustLogoExt )	= getDigitalIndexCustLogoDetails( $opcoid, $customerId );

						# Williams Lea logo first
						push ( @attachments ,{
        						filename            => "$currDir/digital/img/Williamslealogo.png",
        						contentid           => 'Williamslealogo.png',
        						contenttype         => 'image/png',
        						contentdisposition  => 'inline',
    						});
						$email_body  =   "$email_body" . '<img height="40px" src="cid:Williamslealogo.png"></img>&#160;&#160;&#160;';

						if ( ( defined $digitalIndexCustLogoExt ) && ( $digitalIndexCustLogoExt ne "" ) &&
							 ( defined $digitalIndexCustLogoPrefixName ) && ( $digitalIndexCustLogoPrefixName ne "" ) ) {

							my $digitalIndexCustLogoName = sprintf( "%s.%s", "$digitalIndexCustLogoPrefixName", "$digitalIndexCustLogoExt" );
							my $digitalIndexCustoLogPath = sprintf( "%s/%s/%s", "$currDir", "$digitalIndexCustoLogPath", "$digitalIndexCustLogoName" );
							$email_body  =   "$email_body" . '&#160;&#160;&#160;<img width="100px" height="100px" border="0" src="cid:' . $digitalIndexCustLogoName . '"></img>';

    						push ( @attachments ,{
        						filename            => "$digitalIndexCustoLogPath",
        						contentid           => "$digitalIndexCustLogoName",
        						contenttype         => "image/$digitalIndexCustLogoExt",
        						contentdisposition  => 'inline',
    						});
						}

						$options->{attachments}         = \@attachments;	
					}

					my $html_body = $email_body;
					$html_body =~ s/\n/<br>\n/g;
					$email_body =~ s/<b>User ID:<\/b>/User ID:/;
					$email_body =~ s/<b>Password:<\/b>/Password:/;
					$options->{ html } = $html_body; # Gets wrapped with body/html by Mailer::send_mail
					Mailer::send_mail ( $useremail, $email, $email_body, $emailsubject, $options );

					for my $job ( @jobs ) {
						ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $job, "$username($userid) has sent $job to: $email" );
					}
				};
				if ( $@ ) {
					my $error = $@;
					$error =~ s/at module.*//;
					push( @errormessages, "Error sending to $email, $error" );
				}
			} # end of foreach @emailto

			# Send a shorter email without link or credentials to users in CC list (if sendlinkcc unchecked) and any recipients
			if ( ( scalar @emailcc ) > 0 ) {

				$email_to_list = "No one" if ( $email_to_list eq '' );
				my $email_body = "Hi,\n\nA link to the job(s) detailed below has been sent to the following user(s):\n$email_to_list\n\n";

				# Include the job CI details (for Client, Media Owner and Vendor)
				foreach my $job ( @jobs ) {
					$email_body .= email_job_summary( $job );
					# Cant do this as the body is set before the 1st dialog display and the user can then edit it
					# before entering a rename filename
					# $email_body .= "Job $job renamed to " . $override_filenames->{ $job } . "\n" if ( exists $override_filenames->{ $job } );
				}
				$email_body .= "Regards\n$userfullname\n$useremail";

				foreach my $email ( @emailcc ) {
					eval {
						my $rc = Mailer::send_mail ( $useremail, $email, $email_body, $emailsubject, { dieonerror => 1, host => $smtp_gateway } );
						die "send_email cc failed ($rc)" if ( $rc != 1 );
						for my $job ( @jobs ) {
							ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $job, "$username($userid) has cc'd $job to: $email" );
						}
					};
					if ( $@ ) {
						my $error = $@;
						$error =~ s/at module.*//;
						push( @errormessages, "Error sending to $email, $error" );
					}
				}
			} # endif @emailcc.length > 0
		};
		if ( $@ ) {
			my $error = $@;
			$error =~ s/at module.*//;
			push( @errormessages, "Error sending mail to:, $error" );
		}

		# Update the audit log

		# Dilemma: Do we fail if one email fails and record nothing or log it ?
		# For MediaOWner and Vendor it is straight forward, only one recipient but for Client, this is not the case
 
		if ( ( scalar @errormessages ) == 0 ) {

			# Log the fact that we've sent an email to the vendor
			ECMDBAudit::db_auditLog( $vendorId, "USERS", $vendorId, "Vendor $vendorname has been sent link to asset" ) if ( defined $vendorId );;

			my $preflightCheckDisabled = ECMDBOpcos::getCustomerPFCDisabled( $customerId );

			my $userJobnumberMapping_ref;
			if ( $subaction !~ /^Index$/ )
			{
				$userJobnumberMapping_ref = Watchlist::Watchlistnotification( $userid, $opcoid, "Job(s) is Dispatched ", \@jobs, "DISPATCH" );
				# Log against each job that this user send it to the vendor
				for my $job ( @jobs ) {
					if ( $subaction eq "Vendor" ) {
						ECMDBJob::db_recordJobAction( $job, $userid, CTSConstants::EMAIL_VENDOR );
						ECMDBAudit::updateVendorHistory($job, $userid, $vendorId) if ( defined $vendorId );  # TODO where is this ??
					} elsif ( $subaction eq "MediaOwner" ) {
						ECMDBJob::db_recordJobAction( $job, $userid, CTSConstants::EMAIL_MEDIA_OWNER );
					} elsif ( $subaction eq "Client" ) {
						ECMDBJob::db_recordJobAction( $job, $userid, CTSConstants::EMAIL_CLIENT );
					} elsif ( $subaction eq "Digital" ) {
						ECMDBJob::db_recordJobAction( $job, $userid, CTSConstants::EMAIL_DIGITAL );
					}

					# Calls to ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS",  ...) moved to just after the emails sent
					# so that we have a record of emails sent, even if we fail to send to one recipient & dont do this audit logging
					# and DONT change the job preflight status until we have successfully sent to all recipients.

					# Set the job preflight status to dispatched (after everything else)
					ECMDBJob::db_updateJobPreflightStatus( $job, $preflightstatuses{ Dispatched } ) unless $preflightCheckDisabled;

					# NOT sure if we should update the watch list if the Preflight Status check is disabled...


				}	
                
			}
          my @jobnosArray = ();
          if( $jobList =~ /,/ ) {
                @jobnosArray = split( ",", $jobList );
            } else {
                push ( @jobnosArray, $jobList );
            }
            foreach my $job_number ( @jobnosArray ) {
                 if( $subaction eq "Index"){
                        ECMDBJob::db_recordJobAction($job_number, $userid, CTSConstants::SENDEMAILTOCLIENTS );
                   }
            }
			my $filenames = "";
			$filenames = join( "-\t", values %$eff_job_filenames_ref ) if ( defined $eff_job_filenames_ref );
			my $vars = {
					subaction	=> $subaction,
					email		=> $email,
					emailto		=> \@emailto,
					emailcc		=> \@emailcc,
					sendlinkcc	=> $sendlinkcc,
					targetname	=> $targetName,
					targettype	=> $targetType,
					joblist		=> $jobList,
					filenames	=> $filenames,
					vendorId	=> $vendorId,
					publicationname	=> $publicationName,
					messages	=> \@messages,
					userJobnumberMapping	=> $userJobnumberMapping_ref,
					bundlelinks => \@bundlelinks,
					bundle_url => $bundle_url,
			};

			General::setHeaderVars( $vars, $session );

			$tt->process( 'ecm_html/SendJobsConfirmation.html', $vars ) || die $tt->error(), "\n";
			return;
		}
	} # end of 'if posted'

	# Get here on initial entry or after validation failure, ie errormessages or fileerrors

	# Build the components of the emailPreview

	if ( $subaction eq "Digital" ) {
		$emailsubject = "Asset(s) Review" if ( ! defined $emailsubject );
	} 
	elsif ($subaction eq "Index")
	{
		$emailsubject = "Index(s) Review" if ( ! defined $emailsubject );
	}
	else {
		$emailsubject = "JobFlow files ready for download" if ( ! defined $emailsubject );
	}

	if ( !defined $emailcontent ) {

		my $jobs_plural = ((scalar @jobs) > 1) ? "s" : "";
		$emailcontent = "Hi, ...\n\n"; 

		if ( $subaction eq "Digital" ) {
			$emailcontent .= "Please review the following link,\n\n";
			my $externalUrlJobRef = ECMDBJob::db_getexternalUrlJobRef( $jobs[0] );
			$emailcontent  .= sprintf ( "%s?%s=%s", "$digitalApplicationURL", "jobref", "$externalUrlJobRef\n");
			$filenamespanel = 0; # for remove jobs filename panel from digital link send mail page
		} 
		elsif ($subaction eq "Index")
		{
			my $userID	= $query->param( "userID" );
			$emailcontent .= "Please review the following link\n\n";
			my $externalUrlJobRef = getUUIDstr();

			my $externalURL = sprintf ( "%s?%s=%s&%s=%s", "$digitalApplicationURL", "action", "digitalindexpage","jobref", "$externalUrlJobRef\n");
			my $actualURL = sprintf ( "%s?%s=%s&%s=%s&%s=%s&%s=%s&%s=%s", "$applicationurl", "action", "digitalindexpage", "jobs", $jobList, "customer_name", "$customer_name", "userID", "$userID", "opcoid", "$opcoid\n\n");
			my $url = ECMDBOpcos::db_getexternalUrl($actualURL);
			$externalUrlJobRef=undef;
 
			if ( $url ) {
				my @ref = split /jobref=/,$url;
				$externalUrlJobRef = $ref[1];
			} else {
				$externalUrlJobRef = getUUIDstr();
			}
	
			$externalURL = sprintf ( "%s?%s=%s&%s=%s", "$digitalApplicationURL", "action", "digitalindexpage","jobref", "$externalUrlJobRef\n");
			$emailcontent  .= sprintf ( "%s?%s=%s&%s=%s", "$digitalApplicationURL", "action", "digitalindexpage","jobref", "$externalUrlJobRef\n");
		
			ECMDBOpcos::db_mapexternalUrl($actualURL, $externalURL, $userid);
		}
		else 
		{
			#$emailcontent .= "The following job$jobs_plural will be contained in a zip file available by clicking the link below:\n\n";
			$emailcontent .= "The following job$jobs_plural are available for download. \n";
			$emailcontent .= "To download the assets for a job, click on the link next to the job data. \n\n";

			# List the files included for each job first

			#if ( $isLegacy ) { # Removed for the time being PD-1125
			#	$emailcontent .= "The zip file " . $jobs[0] . ".zip contains files for legacy job " . $jobs[0] . "\n";
			#} else {
			#	$emailcontent .= "The following file(s) will be contained in this zip file:\n";
			#	foreach my $job ( @jobs ) {
			#		my $filename = $eff_job_filenames{$job};
			#		$emailcontent .= "   $job - $filename\n";
			#	}
			#}
			#$emailcontent .= "\n\n";

			# Include the CI details for each job (but not for Vendor who has access to the job info directly), send to Media Owner, Client and Vendor

			foreach my $job ( @jobs ) {
				$emailcontent .= email_job_summary( $job );
			}

			# Then a place holder for the link and credentials

			$emailcontent .= "\[$linkplaceholder\]\n";
		}


		$emailcontent .= "\nIf you have any problems or questions about the material supplied, please contact us on\n $contactPhone\n\n";
		$emailcontent .= "Regards\n$userfullname\n$useremail\n";

	}

	# TODO Encode subject and content, best fix for UTF-8 is to replace this with JQuery dialog and use Ajax calls

	my $filenameoverride	= ECMDBOpcos::db_getFilenameoverride( $opcoid );

	if ( ( defined $customerId ) && ( $customerId > 0 ) ) { 
		@recipients 	= ECMDBOpcos::getCustomerAddressees( $customerId ); 
	}

	my $vars = { # Settings mainly for ecm_html/send_by_email.html
		jobs			=> \@jobs,
		joblist			=> $jobList,
		customerId		=> $customerId,
		subaction		=> $subaction,
		vendors			=> \@vendors,
		vendorId		=> $vendorId,
		publicationname		=> $publicationName,
		publicationemail	=> $publicationEmail,
		publicationemaillist	=> \@publicationEmailList,
		errormessages		=> \@errormessages,
		warningmessages		=> \@warningmessages,
		filenameerror		=> \@filenameerror,
		recipients		=> \@recipients,
		uuidstr			=> $uuidstr,
		email			=> $email,
		emailto			=> \@emailto,
		emailcc			=> \@emailcc,
		sendlinkcc		=> $sendlinkcc,
		emailsubject		=> $emailsubject,
		emailcontent		=> $emailcontent,
		targettype		=> $targetType,
		targetname		=> $targetName,
		applicationurl		=> $applicationurl,

		# settings for ecm_html/file_name_overrides.html
		filenameoverride	=> $filenamespanel, # 0 prevents display of job filenames panel
		filenames		=> $job_filenames_ref,	# job filenames before rename
		fn_overrides		=> $override_filenames,	# User supplied filename overrides
	};

	# Set the flags for each file type and create a details hash and summary list

	setFileTypesForSend( $filetypes_ref, $vars );

	# Check Pre flight checked etc

	##By passing the error check for digital alone
	if ( $subaction ne "Digital" && $subaction ne "Index")
	{
		check_jobs_for_send( $opcoid, $customerId, $subaction, $vars, \@jobs, \@errormessages, \@warningmessages );
	}

	# Set the oktogo if no serious uncorrectable errors, so the user can correct filename overrides and press Send
	$vars->{oktogo} = ( ( scalar @errormessages ) == 0 ) ? 1 : 0;

	General::setHeaderVars( $vars, $session );

	$tt->process( "ecm_html/send_by_email.html", $vars ) || die $tt->error(), "\n";
}

######################################################################################################################
# SendJobsViaFtp
#
# Posts a request in the FTP transfer queue for the jobs and file types specified
# Uses ECMDBJob::enQueueFtp to queue a request to jobflow table ftpqueue
#
# Inputs: 
# - customerId
# - opcoid
# - filetypes_ref, hash of file types, value is 0 or 1 (keys bw_asset, bw_crp_asset etc)
# - ftpid, id of FTP server
# - custommessage
# - userid (from session)
# - jobs_ref, array of job numbers
# - recipients_ref, array of recipients email addresses
# - eff_job_filenames_ref - hash of filenames, key is job number
#
# Outputs: $messages_ref, array of messages returned
#
# Returns: 1 if ok, 0 otherwise (see contents of message array)
# 
######################################################################################################################
sub SendJobsViaFtp {
	my ( $customerId, $opcoid, $filetypes_ref, $ftpid, $custommessage, $userid, $jobs_ref, $recipients_ref, $eff_job_filenames_ref, $messages_ref ) = @_;

	my $emailRecipientList = join( ",", @{ $recipients_ref } );

	my ( $failed, $copied ) = ( 0, 0 );

	my @assets;

	foreach my $ft ( @file_type_list ) {
		push( @assets, $ft ) if ( $filetypes_ref->{$ft} ); # values in filetypes_ref hash should be 0 or 1
	}
	my $assetList = join( ",", @assets );
	$assetList = "high_res" if ( $assetList eq "" );

	for my $job ( @$jobs_ref ) {
		my $filename = $eff_job_filenames_ref->{ $job };
		# Do we still need this, as a check is done in get_job_filenames()
		$filename =~ s/[^a-zA-Z0-9_]/_/;
		$filename =~ s/_pdf$/.pdf/;  # we rename "_pdf" to ".pdf" as previous regex replaces ALL .'s with _'s

		eval {
			# cluck "Calling ECMDBJob::enQueueFtp job $job, user $userid, ftpid $ftpid, custommsg $custommessage, assetlist $assetList emailRecips " . Dumper( $emailRecipientList ); 
			ECMDBJob::enQueueFtp( $job, $filename, $ftpid, $userid, $emailRecipientList, $custommessage, $assetList );
			ECMDBJob::db_recordJobAction( $job, $userid, CTSConstants::SEND_BY_FTP );

			# make a call to Gearman
			#my $client = Gearman::Client->new();
			#$client->job_servers( $gearmanservers );
			#$client->dispatch_background( "processftpqueue", $job );

			# if this opcoid uses the new Job Repo - we mark this Job for auto-archive 2 weeks from now.
			#my $usejobrepository;	# is this job eligible for being AutoArchived? (via Customer)
			# $usejobrepository = ECMDBOpcos::db_usejobrepository( $job );
			# $usejobrepository = isJobRepositoryEnabled( $job );
			# temp comment out auto archive enqeue - not working properly yet.
			#if ( $usejobrepository == 1 ) { 
			#	ECMDBJob::enQueueRepoAutoArchive( $job, $opcoid, $userid, $autoarchiveafter ); 
			#}

			push( @$messages_ref, "Queued request for job $job ok" );
			$copied++;
	
		};
		if ( $@ ) {
			push( @$messages_ref, "Failed to queue request for job $job, $@" );
			$failed++;
		}
	}
	if (( $failed + $copied ) == 0 ) {
		push( @$messages_ref, "No jobs copied" );
		return 0;
	}
	if ( $failed > 0 ) {
		push( @$messages_ref, "Failed to copy $failed jobs" );
		return 0;
	}
	
	# Display of confirmation is done by caller, sendByFTP using SendJobsFTPConfirmation.html
	return 1;
}

sub getUUIDstr {
	#get a new uuid
	my $ug = new Data::UUID;
	my $uuid = $ug->create();
	return $ug->to_string( $uuid );
}

# Create (or if exist clean up) temp directory
# Arguments
# - subdir, name to use for subdirectory of /temp or whatever, eg uuid string
# Returns temp directory name
# Throws exception if failed to create directory or failed to remove existing files

sub getTempDir {
	my ( $subdir ) = @_;

	# TODO BRIAN

	my $tempdir		= "/temp/$subdir/";

	# Create or otherwise cleanup the temporary directory
	eval {
		mkdir ( $tempdir ) or die "$!";
	};
	if ( $@ ) {
		if ( $@ =~ /^File exists/ ) {
			# Clean it up
			opendir my ($dirhandle), $tempdir or die "$! $tempdir - cleanup";
			my @files = readdir($dirhandle);
			closedir $dirhandle;
			foreach my $filename (@files) {
				next if $filename =~ /^\.{1,2}$/;
				my $rc = unlink "$tempdir/$filename";
				if( $rc == 0 ) {
					die "Failed to clean up temporary directory $tempdir";
				}
			}
		} else {
			die "Failed to create temporary directory $tempdir";
		}
	}

	return $tempdir;
}

######################################################################################################################
# FtpJobAssets
#
# Use FTP to transfer the required files from their current location into a temporary directory.
# Uses the drive tables @file_type_list and @file_type_send_details to decide what the source
# and target directories are and whether to copy one file or all the files in the source directory.
# Note the special case for high_res Legacy files, these are zip files rather than *.pdf files
# and we should only return that one file rather than a zip of all the files.
# 
# Called by sendByEmail, which then sends the user an email telling them how to download the
# files in the temporary directory.
# Usage
# $files_copied = FtpJobAssets( $tempdir, $customerId, $opcoid, $filetypes_ref, $uuidstr, \@jobs, \%eff_job_filenames, \@messages );
#
# Inputs: 
# - tempdir
# - jobNumber
# - jobPath
# - ftpserver
# - ftpusername
# - ftppassword,
# - job_filename
# - filetypes_ref, hash of file types selected
# - messages_ref
#
# Outputs:
# - messages in the array $message_ref
#
# Returns:  
# - count of files copied
#
# Throws exception if
# - failed to connect to FTP server
# Note, the following FTP errors are trapped and dont throw an exception
# - failure to cwd to asset directory 
# - failure to copy an individual file
#
######################################################################################################################

sub FtpJobAssets {

	my ( $tempdir, $jobNumber, $jobPath, $ftpserver, $ftpusername, $ftppassword, $job_filename, $filetypes_ref, $messages_ref ) = @_;

	my $filesCopied = 0;

	print "Opening FTP connection to $ftpserver\n" if $debug;

	print "filetyperef " . Dumper( $filetypes_ref ) . "\n" if $debug;

	my $ftp;
	eval {
		print "FTP login, server $ftpserver, user $ftpusername\n" if $debug;
		$ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to FTP server '$ftpserver' : $@";
		$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login to FTP server '$ftpserver' as '$ftpusername'", $ftp->message;
		$ftp->binary();

		print "Opened FTP connection in binary mode\n" if $debug;

		# copy each of the job files to the above directory

		my @found_assets; # list of assets transferred ( & list of those not )
		my @missing_assets;

		# Check whether jobPath directory exists, no point in checking each asset otherwise
		print "ftp cwd $jobPath\n" if $debug;
		$ftp->cwd( $jobPath ) or die "Job path directory does not exist, " . $ftp->message;
		my @toplist = $ftp->ls();
		die "Job path directory is empty" unless ( scalar @toplist );
		print "Job path dir contains " . join(", ", @toplist) . "\n" if ( $debug );
	
		# ftp file over into temp dir for this send
		foreach my $ft ( @file_type_list ) {
			if ( ( exists $filetypes_ref->{ $ft } ) && ( $filetypes_ref->{ $ft } == 1 ) ) {  # ie the user selected to copy this asset type

				my $ref = $file_type_send_details{ $ft };  # Big drive table at top of module
				if ( ! defined $ref ) {
					push( @$messages_ref, "Not sure how to handle asset type $ft" );
					next;
				}

				my $name = $ref->{name}; # eg "BW PDF" for bw_asset, name as it appears to user
				my $suffix = $ref->{suffix}; # eg "JOB/ASSETS/BW_ASSET" for bw_asset
				my $currentdir = $jobPath . "/$suffix/";
				my $target = $ref->{target} ||""; # eg "bw_pdf" for bw_asset
				print " - file type $ft, suffix $suffix, target $target, dir $currentdir\n" if $debug;
				eval {
					my $found = 0;
					$ftp->cwd( $currentdir ) or die $ftp->message . " to $currentdir";
					my @list = $ftp->ls();
					die "Asset directory is empty" unless ( scalar @list );
					print " - contains files " . join(", ", @list) . " files\n" if ( $debug );

					my $type = $ref->{type} || "all"; # Either "pdf" "zip" or "all" (pdf for bw_asset, zip for rgb_jpg_asset
					if ( $type eq "all" ) {
						# Copy all the files in the directory, true for aux_files, open_assets (open assets is Enhanced FTP only)
						my $split_fn = $job_filename;
						$split_fn =~ s/\.pdf$//g;
						print " - Split_fn for all is '$split_fn'\n" if $debug;
						my $filesThisDir = 0;
						# Assume each file in the directory has a unique extension eg .pdf, .tif, .png, copy each file preserving extension
						foreach my $file ( @list ) {
							print " - file for all is '$file'\n" if $debug;
							my ( $ext ) = $file =~ /(\.\w*)$/;
							print "Ext $ext\n" if $debug;
							my $tolocation = $tempdir . $split_fn . $ext;
							print " - tolocation $tolocation\n" if $debug;
							$ftp->get( $file, $tolocation ) or die "ftp(1) get failed, from $currentdir/$file to $tolocation, " . $ftp->message;
							$filesCopied++;
							$filesThisDir++;
							$found = 1;
						}
						push( @$messages_ref, "Job $jobNumber, asset directory '$suffix' no files copied" ) if ( $found == 0 );
						push( @$messages_ref, "Job $jobNumber, asset directory '$suffix' copied $filesCopied file(s)" ) if ( $filesCopied > 0 );

					} elsif ( $type eq "split" ) { # Split pages
						my $split_fn = $job_filename;
						$split_fn =~ s/\.pdf$//g;	# Split prefix is job_filename without trailing .pdf

						foreach my $fn ( @list ) {
							next unless $fn =~ /^$jobNumber/;
							next unless $fn =~ /\_(R|L)HP\.pdf$/;
							my $tolocation = $tempdir . $split_fn . "_" . $1 . "HP.pdf"; # $1 is "L" or "R"
							print " - split pages matched '$fn', target is $tolocation\n" if ( $debug );
							$ftp->get( $fn, $tolocation ) or die "ftp(3) get failed, from $currentdir/$fn to $tolocation, " . $ftp->message;
							$filesCopied++;
							$found = 1; # If found at least 1 page
						}
						push( @$messages_ref, "Job $jobNumber, asset directory '$suffix' does not contain split pages" ) if ( $found == 0 );

					} else {
						# Type is "pdf" or "zip", expect to find <job>.<type> (Apply the job filename rename)
						# Most of the file types which just copy one file and use the user or customer supplied filename

						my $fn = $jobNumber . "." . $type; # Typically $jobNumber.pdf or $jobNumber.zip
						my $tolocation = $tempdir . "$target-$job_filename";

						# Change target filetype from .pdf or .jpeg to .zip if target is also a .zip file
						$tolocation =~ s/\.(pdf|jpg|jpeg)$/\.zip/ if ( $fn =~ /\.zip$/ );
	
						# Hi_res is a special case, the source file is a PDF (<job no>.pdf and we use the user supplied filename for target.
						# The Legacy case of Hi-Res is handled elsewhere.
	
						if ( $ft eq "high_res" ) {
							$tolocation = $tempdir . $job_filename; # No target prefix
							$fn = $jobNumber . ".pdf";
						}
						print " - looking for file '$fn'\n" if $debug;
						foreach my $file ( @list ) {
							print " - got file $file\n" if $debug;
							next unless ( $file eq $fn );
							$found = 1;
							my $fromlocation = $currentdir . $fn;
							$ftp->get( $fromlocation, $tolocation ) or die "ftp(2) get failed, file $file from $fromlocation to $tolocation, " . $ftp->message ;
							$filesCopied++;
						}
						push( @$messages_ref, "Job $jobNumber, asset directory '$suffix' does not contain '$fn'" ) if ( $found == 0 );
					}
					push( @found_assets, $name ) if ( $found == 1 );
					push( @missing_assets, $name ) if ( $found == 0 );
				};
				if ( $@ ) {
					my $error = $@;
					$error =~ s/at modules.*//; # Lose module/line for error message
					if ( $error =~ /^Failed to change directory/ ) {
						# If we can't change directory then the asset is just not there
						push( @$messages_ref, "Job $jobNumber, asset directory '$suffix' does not exist" );
					} else {
						push( @$messages_ref, "Job $jobNumber, failed to transfer asset $ft, $error" )
					}
					push( @missing_assets, $name );
				}
			}
		} # end foreach file type (within a job)

		my $missing_assets_list = join( ", ", @missing_assets );
		my $found_assets_list = join( ", ", @found_assets );

		push( @$messages_ref, "Job $jobNumber, no assets found" ) if ( ( $missing_assets_list eq "" ) && ( $found_assets_list eq "" ) );
		push( @$messages_ref, "Job $jobNumber, failed to copy asset(s) $missing_assets_list" ) if ( $missing_assets_list ne "" );
		push( @$messages_ref, "Job $jobNumber, path $jobPath" ) if ( ( $found_assets_list eq "" ) || ( $missing_assets_list ne "" ) );
		push( @$messages_ref, "Job $jobNumber, copied asset(s) $found_assets_list" ) if ( $found_assets_list ne "" );

		$ftp->close() if ( defined $ftp );
		$ftp = undef;
	}; # end of 'eval'
	if ( $@ ) {
		my $error = $@;
		$ftp->close() if ( defined $ftp ); $ftp = undef;
		$error =~ s/at modules.*$//; 
		if ( $error =~ /Job path directory does not exist/ ) {
			push( @$messages_ref, "Job $jobNumber, No assets found" );
			push( @$messages_ref, "Job $jobNumber, job path does not exist, $jobPath" );
		} else {
			die "FtpJobAssets Failed during FTP of files into core $error<br>";
		}
	}

	return $filesCopied;
}

# Transfer single job asset
#
# result = FtpGetJobAsset( $source, $destination, $ftpserver, $ftpusername, $ftppassword ) 
#
sub FtpGetJobAsset {
	my ( $source, $destination, $ftpserver, $ftpusername, $ftppassword ) = @_;

	my $ftp;
	print "FtpGetJobAsset: $source, $destination, $ftpserver, $ftpusername, $ftppassword\n" if ( $debug );
	eval {
		$ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to : $@";
		$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login ", $ftp->message;
		$ftp->binary();

		$ftp->get( $source, $destination ) or die "get failed ", $ftp->message;

		$ftp->close();
		$ftp = undef;
	};
	if ( $@ ) {
		my $error = $@;
		$ftp->close() if ( defined $ftp ); $ftp = undef;
		$error =~ s/at modules.*$//; 
		die "FtpGetJobAsset Failed during FTP from $source to $destination, $error<br>";
	}
	return $destination;

}

##############################################################################
#       email_vendor()
#
##############################################################################
sub email_vendor {
	my ( $email, $foldername, $vendorId, $filename, $joblist, $custommessage ) = @_;

	my $body;
	
	$body 	= "Hi,\nPlease use the link below to access files.\n\n";
	$body	.= "The following files:\n $filename";
	$body 	.="\n";	
	$body	.= "Will be contained in this zip file:\n\t$applicationurl?action=vendorAssetDownload&id=$foldername&vendorId=$vendorId&joblist=$joblist\n\n\n";
	if($custommessage)
	{
		$body   .= qq { This message was sent with the files:\n\t"$custommessage"\n\n };
	}
	$body 	.= "Thank you.\n\nJob Flow.\n\n";
	
	Mailer::send_mail ( $fromemail, $email, $body, "Job Flow: Files ready for download", { host => $smtp_gateway } );
}

##############################################################################
#       email_recipient()
#
##############################################################################
sub email_recipient {
	my ( $email, $filename, $joblist, $custommessage, $vendorEmail, $recipientType ) = @_;

	my $body = "A link for the following files:\n $filename\n";
	$body	.= "has been sent to the $recipientType at this email address: $vendorEmail\n\n\n";
	if( $custommessage ) {
		$body   .= qq { This message was sent with the files:\n\t"$custommessage"\n\n };
	}
	$body 	.= "Thank you.\n\nJob Flow.\n\n";
	
	Mailer::send_mail ( $fromemail, $email, $body, "Job Flow: Download link sent to $recipientType", { host => $smtp_gateway } );
}

##############################################################################
#       getDestinationDirectory()
#	used by Approve (copy of Jobs::getDestinationDirectory
#
##############################################################################
sub getDestinationDirectory {
	my $opcoid = $_[0];

	return ECMDBOpcos::db_getDestinationDirectory( $opcoid );
}

##############################################################################
#       getFTPDetails()
#	used extensively in this module
#
##############################################################################
sub getFTPDetails {
	my $opcoid = $_[0];

	return ECMDBOpcos::db_getFTPDetails( $opcoid );
}


##############################################################################
#       jobPassedPreflight()
#	copy of Jobs::jobPassed
##############################################################################
sub jobPassedPreflight {
	my $jobid = $_[0];

	return ECMDBJob::db_jobPassed( $jobid );
}
##############################################################################
#       jobPreflightforproject()
#       copy of Jobs::jobPassed
##############################################################################
sub jobPreflightforproject {
        my $jobid = $_[0];

        return ECMDBJob::db_jobPreflightforproject( $jobid );
}

##############################################################################
#       jobQCApproved()
#
##############################################################################
sub jobQCApproved {
	my $jobid = $_[0];

	return ECMDBJob::db_jobQCApproved( $jobid );
}

##############################################################################
#       jobQCRequired()
#
##############################################################################
sub jobQCRequired {
	my $jobid = $_[0];

	return ECMDBJob::db_jobQCRequired( $jobid );
}
##############################################################################
#       getVendors()
#
##############################################################################
sub getVendors {
	my $opcoid = $_[0];
	
	return ECMDBCompanies::db_getVendors( $opcoid );
}

##############################################################################
#       getOperatingCompanyFromJob()
#
##############################################################################
sub getOperatingCompanyFromJob {
	my $jobid = $_[0];

	return ECMDBJob::db_getOperatingCompanyFromJob( $jobid );
}

##############################################################################
#       ViewPreflight - checks that job exists, then creates page with frame with link to stream/filedownload
#
##############################################################################
sub ViewPreflight {
	my ( $query, $session ) = @_;

	my $jobid 		= $query->param( "jobid" );

	my $userid		= $session->param( 'userid' );

	my $ftp;
	my $vars;

	my $preflightrespath = Config::getConfig("preflightrespath") || 'PREFLIGHT_RESULT';
	$preflightrespath =~ s/\/$//g; # Strip trailing slash if present

	eval {
		Validate::job_number( $jobid );

		my ( $jobpath, $jobopcoid, $customerid, $location, $remoteCustomerId) = JobPath::getJobPath( $jobid );
		die "Job doesnt exist" unless defined $jobpath;

		# Check user can see jobs customer
		StreamHelper::assert_customer_visible( $userid, $jobopcoid, $customerid, $location, $remoteCustomerId ); ##$userid, $opcoid, $customerid, $location, $remoteCustomerid is the order

		my $preflightdir		= $jobpath . '/' . $preflightrespath;
		my $preflightfile		= $jobid . '.pdf';
		my $preflightpath 		= $preflightdir . '/' . $preflightfile;

		# Use FTP to verify Preflight file exists
	
		my ( $ftpserver, $ftpusername, $ftppassword ) = getFTPDetails( $jobopcoid ) ;
		die "Invalid FTP server for opcoid $jobopcoid" unless ( $ftpserver );

		$ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to $ftpserver : $@";
		$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login ", $ftp->message;
		$ftp->binary();

		$ftp->cwd( $preflightdir ) or die "Preflight result directory '$preflightdir' does not exist";
		my @lis = $ftp->ls( $preflightfile );
		die "Preflight PDF '$preflightpath' does not exist" unless ( ( scalar @lis ) > 0 );
		#carp Dumper( \@lis );
		$ftp->quit();
		$ftp = undef;

		$vars = {
			jobflowserver	=> $jobflowServer,
			jobid			=> $jobid,
		};

		General::setHeaderVars( $vars, $session );

		# PD-2622 Generate random number to pass to stream scripts
		# Only do if displaying HTML as that has the configSettings.

		my $rnd_no = StreamHelper::createKey( $session );
		$vars->{ rnd_no } = $rnd_no;
		$vars->{ sessionid } = $session->id;

	};
	if ( $@ ) {
		my $error = $@;
		carp "ViewPreflight: $error";
		$error =~ s/ at .*//gs;
		$vars = { error => $error, jobid => $jobid, };
	}

	$tt->process( 'ecm_html/viewpreflight.html', $vars )
				|| die $tt->error(), "\n";
	
}

##############################################################################
#	DownloadAsset
#
##############################################################################
sub DownloadAsset {
	my ( $query, $session ) = @_;

	my $id			= $query->param( "id" );
	my $vendorId	= $query->param( "vendorId" );
	my $joblist		= $query->param( "joblist" );

	my $userid		= $session->param( 'userid' );
	my $username	= $session->param( 'username' ) || "Undef";
	my $email		= $session->param( 'email' );

	my $errormsg;	

	my $vendordownloadurl 		= $jobflowServer . "/stream/getvendorasset.pl";

	if ( ( ! defined $userid ) || ( $userid !~ /^\d*$/ ) ) {
		$errormsg = "User not logged on"; 

	} elsif ( ( ! defined $id ) || ( $id eq "" ) ) {
		$errormsg = "id not supplied";

	} elsif ( $username eq "tempuser" ) {
		my $note = ECMDBUser::db_GetTempUserNote( $userid );
		( $email ) = $note =~ /Email ([\S\-\@]*)/ if ( ( ! defined $email ) && ( defined $note ) && ( $note =~ /Email / ) );
		if ( ! defined $email ) {
			#print "note is $note<br>\n" if ( defined $note );
			$errormsg = "Could not get User details";
		} else {
			#print "DownloadAsset: username: $username, userid: $userid, Email $email<br>";
			$userid = 2; # Use a userid reserved for such an event, so we dont have to change getvendorasset.pl
			ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $userid, "User $email downloaded assets: $joblist, uuid $id" );
		}
		$vendorId = $userid;

	} else {
		# Assume internal user
		#print "DownloadAsset: userid: $userid, Username $username<br>/n";
		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $userid, "Vendor downloaded Assets: $joblist, uuid $id" );
		$vendorId = $userid unless ( defined $vendorId );
	}

	my $vars = {
		vendordownloadurl 	=>	$vendordownloadurl,
		id			=>	$id,
		vendorId		=>	$vendorId,
		joblist			=>	$joblist,
		errormsg		=>	$errormsg,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/downloadasset.html', $vars )
			|| die $tt->error(), "\n";
}

# TODO some of the following short functions are replicants of those in jobs.pl
# perhaps should be in a shared jobutils.pl module

#############################################################################
#       :getCustomerFilename()
#
##############################################################################
sub getCustomerFilename {
	my $jobid	= $_[0];
	my $customerId	= $_[1];

	# we need the opco file name config for this job
	my $filenametemplate	= ECMDBOpcos::getCustomerFilenameTemplate( $customerId );	
	#print "customer filename template: $filenametemplate<br>";

	return JobFilename::getJobFilename( $jobid, $filenametemplate );
}

#############################################################################
#       :getOperatingCompanyFilename()
#
##############################################################################
sub getOperatingCompanyFilename {
	my $jobid	= $_[0];

	# we need the opco file name config for this job
	my $opcoid 		= ECMDBJob::db_getJobOpcoid( $jobid );
	my $filenametemplate	= ECMDBOpcos::getOperatingCompanyFilenameTemplate( $opcoid );	
	#print "opco filename template: $filenametemplate<br>";

	return JobFilename::getJobFilename( $jobid, $filenametemplate );
}

# getJobFilename moved to jobfilename.pl

##############################################################################
#       getFilenameTemplate()
#
##############################################################################
sub getFilenameTemplate {
	my $opcoid 	= $_[0];
	
	return ECMDBOpcos::db_getFilenameTemplate( $opcoid );
}

##############################################################################
# Adgate Delivery
##############################################################################
sub AdGateDeliver {
	my ( $query, $session ) = @_;

	my $jobid			= $query->param( "jobid" );
	my $accountname			= $query->param( "accountname" );
	my $filename			= $query->param( "filename" );
	my $replacement			= $query->param( "replacement" );
	my $formPosted			= $query->param( "formPosted" );
	my $publicationCode		= $query->param( "publicationCode" );
	my $adspecPublication		= $query->param( "adspecPublication" );
	my $adspecSection		= $query->param( "adspecSection" );
	my $adspecSizeDescription1 	= $query->param( "adspecSizeDescription1" ); 
	my $adspecSizeDescription2 	= $query->param( "adspecSizeDescription2" ); 
	my $adspecHeight		= $query->param( "adspecHeight" );
	my $adspecWidth			= $query->param( "adspecWidth" );
	my $adspecColor			= $query->param( "adspecColor" );
	my $deliveryMethod		= $query->param( "deliveryMethod" );
	my $comment			= $query->param( "comment" );
	my $insertiondate		= $query->param( "insertiondate" );
	my $publicationCountry		= $query->param( "publicationCountry" );

	my $userid			= $session->param( 'userid' );

	my $debug			= 1;
	my $message = "";
	my $oktogo = 1;
	my $disablesubmit = 0;
	my $userJobnumberMapping_ref;
	my @errormessages;

	# Validate job number(s)
	if ( ! defined $jobid ) {
		# Initial call from chooseDestination passes jobs rather than jobid
		my $jobList = $query->param( "jobs" ) || "";
		$jobList =~ s/,$//; # Remove extraneous comma
		my @jobs = split( ',', $jobList );
		if ( ( scalar @jobs ) != 1 ) {
			push( @errormessages, "Expect just one job number, got '$jobList'" );
		} else {
			$jobid = $jobs[0];
		}
	}

	if ( $formPosted eq "yes" ) {
		$replacement 	= ( $replacement eq "on" ) ? 'true' : 'false';

		if ( $debug == 1 ) {
			print "form posted<br>";
			print "jobid: $jobid<br>";
			print "acct name: $accountname<br>";
			print "filename: $filename<br>";
			print "replacement: $replacement<br>";
			print "Country: $publicationCountry<br>";
			print "PC: $publicationCode<br>";
			print "Pub: $adspecPublication<br>";
			print "Sec: $adspecSection<br>";
			print "Size 1: $adspecSizeDescription1<br>";
			print "Size 2: $adspecSizeDescription2<br>";
			print "Height: $adspecHeight<br>";
			print "Width: $adspecWidth<br>";
			print "Colour: $adspecColor<br>";
			print "DM: $deliveryMethod<br>";
			print "Comment: $comment<br>";
			print "in date: $insertiondate<br>";
			print "<br><br>";
		}

		# validation
		if ( $accountname eq "0" ) {
			push( @errormessages, "Need to select an account" );
		}

		# pevent illegal chars in filename
		if ( ( ! defined $filename ) || ( length( $filename ) == 0 ) ) {
			push( @errormessages, "Filename is required" );
		} elsif ( $filename =~ /[^a-zA-Z0-9_]+/ ) {
			push( @errormessages, "Filename has illegal characters - please fix" );
		}
		# depth must not be greater than height
		#if ( looks_like_number ($adspecHeight) && looks_like_number($adspecWidth) ) {
		#}
	
		if ( length( $insertiondate ) == 0 ){
			push( @errormessages, "Date is required" );
		} else {
			# is date OK?
			my $diff 	= date( $insertiondate ) - today();
			#print "Diff: $diff<br>";
			if ( $diff <= 0 ) {
				push( @errormessages, "Date must be in the future" );
			}
		}
		$oktogo = ( ( scalar @errormessages ) == 0 ) ? 1 : 0;

		# send XML to Adgate
		# write values to database
		if ( $oktogo == 1 ) {
			ECMDBJob::enQueueAdgate( $jobid, $userid, $accountname, $filename, $replacement, $publicationCountry, $publicationCode, $adspecPublication, $adspecSection, 
							$adspecSizeDescription1, $adspecSizeDescription2, $adspecHeight, $adspecWidth, $adspecColor, 			
							$deliveryMethod, $comment, $insertiondate );

			# audit this send
			ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Job: $jobid was delivered to AdSend by user: $userid" );
			ECMDBJob::db_recordJobAction($jobid, $userid, CTSConstants::SEND_BY_ADSEND );
			$message 	= "File has been sent to AdSend";

			# mark as dispatched
			ECMDBJob::db_updateJobPreflightStatus( $jobid, $preflightstatuses{ Dispatched } );

			$userJobnumberMapping_ref = Watchlist::Watchlistnotification( $userid, $session->param( 'opcoid' ), " Job: $jobid was delivered to AdSend by user: $userid  ", $jobid, "DISPATCH" );

			if ( $userJobnumberMapping_ref )
			{
				ECMDBAudit::db_auditLog( $userid, "Watchlistnotification", $jobid, "WATCHLIST SUCCESS" );
			}
			else
			{
				ECMDBAudit::db_auditLog( $userid, "Watchlistnotification", $jobid, "WATCHLIST FAIL" );
			}
		}

	} else {	# before form is posted
		my $workflow = ECMDBJob::db_getWorkflow( $jobid );

		$filename = $jobid;

		# job validation, mus be QC Approved, a Press job and not a Creative job

		if ( jobQCApproved ( $jobid ) != 1 ) {
			push ( @errormessages, "Job is not QC Approved - cannot release!" );
			$disablesubmit = 1;
		}
	
		# is this a press Job? 
		if ( !(ECMDBJob::db_getWorkflow( $jobid ) eq "PRESS") ) {
			push ( @errormessages, "This is not a Press Job - cannot release!" );
			$disablesubmit = 1;
		}

		# is this a creative Job? 
		if ( ECMDBJob::isCreative( $jobid ) ) {
			push ( @errormessages, "This is a Creative Job - cannot release" );
			$disablesubmit = 1;
		}
		$oktogo = ( ( scalar @errormessages ) == 0 ) ? 1 : 0;
	}

	my $vars = {
		filename				=>	$filename,
		oktogo					=>	$oktogo,
		disablesubmit			=>	$disablesubmit,
		jobid					=>	$jobid,
		adgateremote			=>	$adgateremote,
		adspecserver			=>	$adspecServer,
		message					=>	$message,
		errormessages			=>	\@errormessages,
		userJobnumberMapping	=> $userJobnumberMapping_ref,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/adgatedeliver.html', $vars )
			|| die $tt->error(), "\n";
}

sub test_FTPJobAssets {

	my $uuidstr = getUUIDstr();
	my $tempdir = getTempDir( $uuidstr );
	my @messages;

	$debug = 1;
	my %file_types;
	$file_types{ bw_asset } = 1;
	$file_types{ rendered_pdf } = 1;

	my $job = "104000005";

	my ( $jobPath, $opcoid, $customerId ) = JobPath::getJobPath( $job );

	$jobPath .= "/" unless $jobPath =~ /\/$/;

	# Get the FTP connect details
	my ( $ftpserver, $ftpusername, $ftppassword ) = getFTPDetails( $opcoid ) ;
	die "FTP Server not defined for Operating company ($opcoid)" if ( ( ! defined $ftpserver ) || ( $ftpserver eq "" ) );

	my $files_copied = 0;
	eval {
		$files_copied = FtpJobAssets( $tempdir, $job, $jobPath,
			$ftpserver, $ftpusername, $ftppassword,
			"plaything", # $job_filename, 
			\%file_types, # $filetypes_ref, 
			\@messages );

		print "FtpJobAsset completed ok\n";
		print Dumper( @messages );
		my @files = `ls $tempdir`;
		print Dumper( @files );
	};
	if ( $@ ) {
		print "Exception from FtpJobAsset, $@\n";
	}
}

sub test_ftp_job_assets_and_zip {

	my @messages;
	my @errormessages;
	my @filenameerror;
	my @warningmessages;

	my $result_file;
	$debug = 1;
	my $tag_env = $ENV{'TAG_ENV'} || "DEV";

	my $userid = 330;
	my $username = "streanor";
	my $subaction = "Vendor";  # One of "Vendor", "MediaOwner", "Client" or "Digital"
	my $uuidstr = getUUIDstr();
	my $tempdir = getTempDir( $uuidstr );

	my @jobs = ( "104000004", "104000005" );
	# @jobs = ( 90683767, 90683771, 90683772, 90683773, 90683765, 90683766, 90683768, 90683774 ) if ( $tag_env eq "LIVE" );
	# @jobs = ( 16171825 ) if ( $tag_env eq "LIVE" ); # PD-2125
	# @jobs = ( 16173924 ) if ( $tag_env eq "LIVE" ); # PD-2136 open asset test
	@jobs = ( 11053258, 11053259, 11053254, 11053260, 11053255, 11053261, 11053256, 11053262, 11053257, 11053253 ) if ( $tag_env eq "LIVE" ); # PD-2136 open asset test
	@jobs = ( 67000102, 67000103 ) if ( $tag_env eq "STAGING" );

	my %fn_overrides;
	$fn_overrides{ "104000004" } = "Bill.pdf";
	$fn_overrides{ "104000005" } = "Ben.pdf";

	my %file_types;
	$file_types{ bw_asset } = 1;
	$file_types{ lr_asset } = 1;
	$file_types{ high_res } = 1;
	$file_types{ split_pages } = 1;
	$file_types{ aux_files } = 1;

	#$file_types{ rendered_pdf } = 1; # Should appear one its own

	my $customerId = get_customer_for_jobs( \@jobs, \@errormessages );
	my $opcoid = ECMDBOpcos::getCustomerOpcoid( $customerId );
	die Dumper( @errormessages ) if ( (scalar @errormessages) > 0 );
	print "Opco $opcoid, customer $customerId\n";
	
	my $isLegacy = 0;
	my $digitalRenderedPdfFldr = Config::getConfig("digitalRenderedPdfFldr" ) || "RENDERED_PDF";

	my ( $job_filenames_ref, $eff_job_filenames_ref ) = get_job_filenames( \@jobs, $customerId, $opcoid, \%fn_overrides, \@filenameerror, \@warningmessages );
	print "Job filenames " . Dumper( $job_filenames_ref ) . "\n";
	print "Eff filenamed " . Dumper( $eff_job_filenames_ref ) . "\n";

	eval {

		$result_file = ftp_job_assets_and_zip( $userid, $username, $subaction, $uuidstr, $tempdir, \@jobs, $opcoid, $customerId, $isLegacy, 
					$eff_job_filenames_ref, \%file_types, $digitalRenderedPdfFldr, \@messages ); 

		print "ftp_job_assets_and_zip completed ok, result_file $result_file\n";
		print Dumper( @messages );
		my @files = `ls $tempdir`;
		print Dumper( @files );
		print `unzip -l $result_file` . "\n" if ( $result_file =~ /zip$/i );
	};
	if ( $@ ) {
		print "Exception from ftp_job_assets_and_zip, $@\n";
		print Dumper( @messages );
	}
	return 0;
}

sub test_ftp_job_assets_and_zip_digital {

	my @messages;
	my @errormessages;
	my @filenameerror;
	my @warningmessages;

	my $result_file;
	$debug = 1;
	my $tag_env = $ENV{'TAG_ENV'} || "DEV";

	my $userid = 330;
	my $username = "streanor";
	my $subaction = "Digital";  # One of "Vendor", "MediaOwner", "Client" or "Digital"
	my $uuidstr = getUUIDstr();
	my $tempdir = getTempDir( $uuidstr );

	my @jobs = ( 105000035 );
	@jobs = ( 65000047 ) if ( $tag_env eq "STAGING" );

	my %fn_overrides;
	$fn_overrides{ "104000004" } = "Bill.pdf";

	my %file_types;
	$file_types{ rendered_pdf } = 1; # Should appear one its own

	my $customerId = get_customer_for_jobs( \@jobs, \@errormessages );
	my $opcoid = ECMDBOpcos::getCustomerOpcoid( $customerId );
	die Dumper( @errormessages ) if ( (scalar @errormessages) > 0 );
	print "Opco $opcoid, customer $customerId\n";
	
	my $isLegacy = 0;
	my $digitalRenderedPdfFldr = Config::getConfig("digitalRenderedPdfFldr" ) || "RENDERED_PDF";

	my ( $job_filenames_ref, $eff_job_filenames_ref ) = get_job_filenames( \@jobs, $customerId, $opcoid, \%fn_overrides, \@filenameerror, \@warningmessages );
	print "Job filenames " . Dumper( $job_filenames_ref ) . "\n";
	print "Eff filenamed " . Dumper( $eff_job_filenames_ref ) . "\n";

	eval {

		$result_file = ftp_job_assets_and_zip( $userid, $username, $subaction, $uuidstr, $tempdir, \@jobs, $opcoid, $customerId, $isLegacy, 
					$eff_job_filenames_ref, \%file_types, $digitalRenderedPdfFldr, \@messages ); 

		print "ftp_job_assets_and_zip completed ok, result_file $result_file\n";
		print Dumper( @messages );
		my @files = `ls $tempdir`;
		print Dumper( @files );
		print `unzip -l $result_file` . "\n" if ( $result_file =~ /zip$/i );
	};
	if ( $@ ) {
		print "Exception from ftp_job_assets_and_zip, $@\n";
		print Dumper( @messages );
	}
	return 0;
}


sub test_get_job_filenames {

	my $debug = 1;
	my $tag_env = $ENV{'TAG_ENV'} || "DEV";

	my @errormessages;
	my @filenameerror;
	my @warningmessages;	

	my @jobs = ( '104000004', '104000005', '104000006' );
	#@jobs = ( 90683767, 90683771, 90683772, 90683773, 90683765, 90683766, 90683768, 90683774 ) if ( $tag_env eq "LIVE" );
	@jobs = ( 16171825 ) if ( $tag_env eq "LIVE" ); # PD-2125


	my $customerId = get_customer_for_jobs( \@jobs, \@errormessages );
	my $opcoid = ECMDBOpcos::getCustomerOpcoid( $customerId );
	die Dumper( @errormessages ) if ( (scalar @errormessages) > 0 );
	print "Opco $opcoid, customer $customerId\n";
	
	my %fn_overrides;      # filenames specified by user override
	$fn_overrides{ '104000004' } = "Bill"; # Leave 104000005 without
	$fn_overrides{ '104000006' } = 'Mouse!'; # generate filename error

	my ( $job_filenames_ref, $eff_job_filenames_ref ) = get_job_filenames( \@jobs, $customerId, $opcoid, \%fn_overrides, \@filenameerror, \@warningmessages );

	print "Filename Error " . Dumper( @filenameerror ) . "\n" if ( ( scalar @filenameerror ) > 0 );
 	print "Warnings " . Dumper( @warningmessages ) . "\n" if ( ( scalar @warningmessages ) > 0 );
	print "Job filenames " . Dumper( $job_filenames_ref ) . "\n";
	print "Eff filenamed " . Dumper( $eff_job_filenames_ref ) . "\n";
}

sub getDigitalIndexCustLogoDetails {
	my ( $opcoid, $customerId )	= @_;
	my $fileExt				= undef;
	my $fileNamePrefix		= undef;
	my $currDir         	= `pwd`;
	chomp($currDir);

	my $digitalIndexCustLogoName    = ECMDBJob::db_getDigitalIndexCustoLogName( $opcoid, $customerId );
	if ( $digitalIndexCustLogoName =~ /^(.*)\.(.*)$/ && $digitalIndexCustLogoName !~ /^$/ ) {
		$fileNamePrefix = $1;
		$fileExt        = $2;
	}

	return $currDir, $fileNamePrefix, $fileExt;
}
sub Sendbyproject {

    	my ( $query, $session ) = @_;

		my $jobList	= $query->param( "jobs" ) || "";
		my $action 	= $query->param( "action" );
    	my $opcoid 	= $session->param( 'opcoid' );

		$jobList =~ s/,$//; # remove spurious trailing comma
		my @jobs = split( ',', $jobList ); 
		my @errormessages;
		my @warningmessages;

    	my $customerId 	= get_customer_for_jobs( \@jobs, \@errormessages ) ;
    	my $projectName = get_projects_for_jobs( \@jobs, \@errormessages ) ;
	
	    my $vars = {
				jobs            => \@jobs,
				joblist         => $jobList,
				customerId      => $customerId,
				projectName	=> $projectName,
				errormessages   => \@errormessages,
				warningmessages => \@warningmessages,
        };


		check_jobs_for_send( $opcoid, $customerId, $action, $vars, \@jobs, \@errormessages, \@warningmessages );

		if ( ( scalar @errormessages ) == 0 )
		{
			CopyLRtoCMD( \@jobs, $query, $session );
		}

	    $vars->{oktogo} = ( ( scalar @errormessages ) == 0 ) ? 1 : 0;

        General::setHeaderVars( $vars, $session );		

		$tt->process( 'ecm_html/Sendbyproject.html', $vars )
			|| die $tt->error(), "\n";
}
sub CopyLRtoCMD
{
    my ( $jobs_ref, $query, $session ) = @_;
    my $userid       = $session->param( 'userid' );
    my $opcoid  	 = $session->param( 'opcoid' );

    for my $job ( @$jobs_ref ) 
	{ 
			my $assetname  = $job . '.pdf';
			eval
			{
				my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $job );
				die "Job doesnt exist" unless defined $jobpath;

  			    my ( $ftpserver, $ftpusername, $ftppassword ) = getFTPDetails( $jobopcoid ) ;
                die "Invalid FTP server for opcoid $jobopcoid" unless ( $ftpserver );

				my $assetpath  = $jobpath . '/JOB_ASSETS/LR_ASSET/';
				my $sourceFile = $assetpath . $assetname;

                my $ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to $ftpserver : $@";
                $ftp->login( $ftpusername, $ftppassword ) or die "Cannot login ", $ftp->message;
                $ftp->binary();
                $ftp->cwd( $assetpath ) or die "Low Res directory '$assetpath' does not exist";
                my @lis = $ftp->ls( $assetname );
                die "Low Res PDF '$assetpath/$assetname' does not exist" unless ( ( scalar @lis ) > 0 );
				$ftp->quit();

				my ( $listCurl, $dirlistCGI ) = JF_WSAPI::fileopsOther( $ftpserver, { function => 'copytype', source => $sourceFile, destination => $ftpassetspath, type => 'copy' } );
			};
			if ( $@ )
			{
				my $error = $@;
				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $job, "Low res file not found or copy failed $error" );
			}
			else
			{
				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $job, "Low res file copied to CMD $ftpassetspath/$assetname" );
			}
	}
}

#exit __PACKAGE__->test_FTPJobAssets() unless caller();
#exit __PACKAGE__->test_ftp_job_assets_and_zip() unless caller();
#exit __PACKAGE__->test_ftp_job_assets_and_zip_digital() unless caller();
#exit __PACKAGE__->test_get_job_filenames() unless caller();

1;
