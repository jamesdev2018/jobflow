#!/usr/local/bin/perl -w
package Filters;

use strict;
use warnings;

use Data::Dumper;
use Scalar::Util qw( looks_like_number );

use constant BLANK => '(blank)'; # Substitute for '' in filter values
use constant UNDEF => 'undef'; # Substitute for undefined filter values

# Home grown - please do NOT include jobs.pl or anything that includes jobs.pl

use lib 'modules';
use lib 'modules/Utils'; # if unit testing

use DBAgent; # if unit testing
use Validate;

require "config.pl";

# List of filters used in Jobflow
# as ordered list of hash { fkey, label, inputs, pred, lookup_names, opflags }
# - fkey, is internal filter name, something short and meaningful
# - label, is text to display (if this table is used to drive the client side)
# - inputs, list of query param names used by existing code
# - filterid, name to use for div in Filter structure
# - selectfrom, name of array (in dashboard response) used to update filter with
# - always, 0 or 1, 1 implies set of filter values can vary depending on other filter settings 
# - pred, SQL snippet used to form WHERE clause
# - lookup_names, used when filter values supplied is a list of names which need to map to ids for the SQL query
#   eg for location mapping => 'OperatingCompany:opcoid:opconame' 
# - opflags, used to select a filter based on operating company flags, such as filter mediatype
#   eg for mediatype opflags => { ArchantChk => 1 },
# - valid/validate, apply validation either numeric or alphanumeric, see get_filter_values
#   eg for customer, valid => 'alphanumeric'
# - allblank, sets filter to blank if passed as 'All'
#   eg for agency, ie agency, allblank => 1  (changes newagencyFilter=ALL to newagencyFilter='')
# - noupdate, if set the filter is fixed (so joblist_update_filter does NOT add new values to it, set for fromdate, archived)

#our $joblist_filters = {
#	accounthandler => { label => 'Account Handler', inputs => 'newacchandlerFilter', filterid => 'newacchandler', selectfrom => 'accounthandler', always => 0,
#			pred => 't.accounthandler IN ( ? )', noupdate => 0 },
#
#	advisory => { label => 'Advisory', inputs => 'newquerytypeFilter', filterid => 'newquerytype', selectfrom => 'querytypelist', always => 0,
#			pred => 'qt.id IN ( ? )', noupdate => 0 }, # Really querytype
# :
#	location => { label => 'Location', inputs => 'newLocationFilter', filterid => 'newLocationID', selectfrom => 'location', always => 0,
#			pred => 't3p.location IN ( ? )',  mapping => 'OperatingCompany:opcoid:opconame', noupdate => 0 },
#

# Helper to create symbol table for use with matchCondition

sub getMatchConditionSymbols {
	my ( $opflags, $roleid, $rolename, $relaxroles ) = @_;

	my %symbol_table;
	%symbol_table = %{ $opflags };
	$symbol_table{ roleid } = $roleid;
	$symbol_table{ rolename } = $rolename; # Note no wrapping single or double quote, so condition must include quotes
	$symbol_table{ relaxroles } = $relaxroles;

	return \%symbol_table;
}

# Evaluate a condition string eg "isjobassignment==1 && (roleid =~/(2|4|6)/ || relaxroles==1)"
# Returns 0 or 1

sub matchCondition {
	my ( $condition, $symbol_table ) = @_;

	my $condition_copy = $condition; # Avoid contaminating the source data

	# warn "matchCondition: entry, condition '$condition'\n";	
	foreach my $key ( keys %$symbol_table ) {
		my $val = $symbol_table->{ $key };
		$condition_copy =~ s/$key/$val/;
	}
	# warn "matchCondition: post subs, condition '$condition_copy'\n";

	my $match = eval $condition_copy;
	$match = 0 unless ( defined $match ) && ( $match == 1 );
	
	return $match;
}

# Get the filter for use with the Production page
#
# Inputs 
# - contents of table job_filters
# - symbol_table, see getMatchConditionSymbols, which takes opflags, roleid etc and produces hash
# Returns, hash ref
# hash has keys matching columns from job_filters table
# eg 'project' => {           
#        'filter_key' => 'project',
#        'label' => 'Project/Campaign',
#        'inputs' => 'newprojectFilter',
#        'filterid' => 'newprojectID'
#        'selectfrom' => 'projects',
#        'validate' => 'alphanumeric',
#        'condition' => undef,
#        'pred' => 't.project IN ( ? )',
#        'lookup_names' => '',    
#        'noupdate' => '0',       
#        'always' => '1',         
#        'mapping' => '',         
#        'allblank' => '0',       
#   },
#

sub get_joblist_filters {
	my ( $symbol_table ) = @_; 

	return load_filter_from_table( 'job_filters', $symbol_table ); # Read filter from table job_filters
}

# Get the filter for use with the Resource page

sub get_resource_filters {
	my ( $symbol_table ) = @_; 

	return load_filter_from_table( 'resource_filters', $symbol_table ); # Read filter from table resource_filters
}

# Read filter definition from named tables
# Inputs
# - table_name eg job_filters
# - symbol_table, hash ref, as produced by 	my $symbol_table = getMatchConditionSymbols( $opflags, $roleid, $rolename, $relaxroles );
# Returns hash ref

sub load_filter_from_table {
	my ( $table_name, $symbol_table ) = @_;

	my %filter_hash;

	my $rows = $Config::dba->hashed_process_sql(
		"SELECT f.filter_key, f.label, f.inputs, f.filterid, f.selectfrom, f.pred, f.validate, f.lookup_names, f.condition, f.always, f.noupdate, f.allblank, f.mapping " .
		"FROM " . $table_name . " f " );
	if ( defined $rows ) { # rows is ref to array of hash
		foreach my $row ( @$rows ) {
			my $condition = $row->{condition};
			if ( ( defined $condition ) && ( $condition ne '' ) ) {
				my $match = matchCondition( $condition, $symbol_table );
				next unless $match == 1;
			}
			$filter_hash{ $row->{filter_key} } = $row;
		}
	}
	return \%filter_hash;
}

# Used by get_filter_values to handle the mapping attribute of a filter element
# eg for location, the mapping is "OperatingCompany:opcoid:opconame"
# so Opconames get mapping to opcoid values

sub filter_mapping_lookup {
	my ( $value_list_ref, $table, $id_column, $name_column ) = @_;

	die "Invalid value list" unless (defined $value_list_ref) && ((ref $value_list_ref) eq 'ARRAY' );
	my $expected_rows = scalar @{$value_list_ref};

	die "Invalid table name " unless ( defined $table ) && ( $table ne '' );
	
	my $sql = "SELECT $id_column AS id FROM $table WHERE $name_column" . ' IN ("' . join( '","', @{$value_list_ref} ) . '")';
	my @mapped_list = $Config::dba->process_onerow_sql( $sql );

	my $actual_rows = scalar @mapped_list;
	die "Expected $expected_rows, got $actual_rows" if ( $expected_rows != $actual_rows );
	return \@mapped_list;
}

#
# get_filter_values, return hash containing all the filter values supplied in the current query
# So for most filters, validates the filter values and in some cases does name lookup (eg for location)
#
# Usage
# $filter_values = get_filter_values( $query->Vars, $filters )
# 
# Inputs
# - query_hash, hash containing various filter parameters (use $query->Vars to get params as hash)
# - filters, hash as returned by Filters::get_joblist_filters
#
# Returns - Hash, key is simple filter name, such as customer, value is filter value extracted
#
sub get_filter_values {
	my ( $query_hash, $filters ) = @_;

	my $filter_values = { };

	foreach my $key ( keys %$filters ) {
		my $entry = $filters->{ $key };

		# See whether filter value specified in query
		my $val = '';
		#foreach my $name ( @{ $entry->{ inputs } } ) {
		#	$val = $query_hash->{ $name };
		#	last if ( ( defined $val ) && ( $val ne '' ) );
		#}
		my $name = $entry->{ inputs }; # Name of query parameter
		$val = $query_hash->{ $name } if ( defined $name );
		next unless ( ( defined $val ) && ( $val ne '' ) );

		$val = '' if ( $entry->{allblank} && lc( $val ) eq 'all' ); # eg convert agencyFilter=ALL to value ''

		# Split into list, use commas
		my @value_list = split /,/, $val;

		# See if lookup of names required or validation of values
		my $valid = $entry->{validate};
		if ( ( defined $valid ) && ( $valid ne '' ) ) {
			if ( $valid eq 'alphanumeric' ) {
				foreach my $v ( @value_list ) {
					next if $v eq BLANK;
					die "Invalid $key filter, value '$v'" unless Validate::isAlphaNumeric( $v );
				}
			} elsif ( $valid eq 'numeric' ) {
				foreach my $v ( @value_list ) {
					die "Invalid $key filter, value '$v'" unless looks_like_number( $v );
				}
			}
		}

		# Do we map the names in the list to values, eg location names (and possibly others such as QC status
		my $mapping = $entry->{mapping};
		if ( ( defined $mapping ) && ( $mapping ne '' ) ) {
			my ( $table, $id_column, $name_column ) = split /:/, $mapping; # eg "OperatingCompany:opcoid:opconame"
			$filter_values->{ $key . "_mapped" } = filter_mapping_lookup( \@value_list, $table, $id_column, $name_column );
		}
		$filter_values->{ $key } = $val;
	}
	return $filter_values;
}

#
# update_filter_values, sve the filter values in output hash
#
# eg update_filter_values( $filter_values, $filters, $vars )  
#
# Inputs
# - $filter_values, hash containing current filter settings
# - $filters, hash returned by Filters::get_joblist_filters
# Outputs 
# - $vars, output hash (used to form JSON)
#
sub update_filter_values {
	my ( $filter_values, $filters, $vars ) = @_;

	foreach my $key ( keys %$filter_values ) {
		next if $key =~ /_mapped$/; # Ignore special for location where we store location names AND opcoids in location_mapped
		my $val = $filter_values->{ $key };
		if ( ( defined $val ) && ( $val ne '' ) ) {
			my $entry = $filters->{ $key };
			#foreach my $name ( @{ $entry->{ inputs } } ) {
			#	$vars->{ $name } = $val;
			#}
			my $name = $entry->{ inputs };
			$vars->{ $name } = $val if ( defined $name );
		}
	}
}

#
# filter_where_clause
# Converts a filter and set of filter values into a WHERE clause
# So for example if the CGI query contained newagencyFilter: "AA_PRODUCTION_COMPANY,CANCER_RESEARCH_UK"
# then the filter_values hash would contain { customer: "AA_PRODUCTION_COMPANY,CANCER_RESEARCH_UK" }
# Returns an array ref, with a set of where clauses

sub filter_where_clause {
	my ( $filters, $filter_values ) = @_;

	my @where_list;
	foreach my $fk ( keys %$filter_values ) { 
		next if $fk =~ /_mapped$/; # Special case for location where we store list of location names and mapped values (opco ids)
		my $entry = $filters->{ $fk }; # Drive table entry
		if( ! defined $entry ) {
			warn "No filter entry for filter $fk";
			next;
		}
		my $fv = $filter_values->{ $fk };
		if( ! exists $entry->{pred} ) {
			warn "No predicate for filter $fk";
			next;
		}
		next if ( $entry->{pred} eq '' ); # For fromId, archived, box we test the filters outside of this
		
		my $pred = $entry->{pred};
		my $has_undefined_values = 0;
			# Most of the filters have predicates like eg pred => 't.accounthandler IN ( ? )'
			# The exceptions are 
			# - fromId, results in DATE range check, handled explicitely where used 
			# - location (has mapping of names to location ids), 
			# - mediaowner, just doesnt have a corresponding pred clause yet
		if ( ( exists $entry->{mapping} ) && ( $entry->{mapping} ne '' ) ) {
			# Use mapped value eg list of location ids instead of location names
			$fv = $filter_values->{ $fk . '_mapped' };
			#print "fv " . Dumper( $fv );
			$fv = join( ',', @$fv );
			#-- $pred =~ s/\?/$fv/;   # So we end up with "t3p.location IN (1,2,3)"
		} else {
			# Substitute into pred clause, assume we are dealing with string values, which in most cases is false
			my @x = split /,/, $fv;
			for ( my $i = (scalar @x) - 1; $i >= 0; $i-- ) {
				$x[ $i ] = '' if ( $x[ $i ] eq BLANK );   # Change '(blank)' back to '' for the SQL query
				
				## Extract undefined values, make sure we handle them separately (i.e. for IS NULL)
				if( $x[ $i ] eq UNDEF ) {
					splice(@x, $i, 1);
					$has_undefined_values = 1;
				}
			}
			if(@x) { $fv = "'" . join( "','", @x ) . "'"; }
			else	   { $fv = ""; }
			#-- $pred =~ s/\?/$fv/;
		}
		
		my @predicates;
		push(@predicates, "$pred IN ($fv)") if $fv;
		push(@predicates, "$pred IS NULL") if $has_undefined_values;
		
		push(@where_list, "(" . join(' OR ', @predicates) . ")");
	}
	
	return \@where_list;
}

# Test stubs

sub test_connect {
	eval   { # for unit test only
		Config::initialise_config(); 
	};
	if($@) { 
		my $error = $@;
		print "{\"error\":\"$error\",}\n";
		die "badly";
	}
}


sub test_getFilters {

	test_connect();
	my $opflags = { ArchantChk => 1 };
	my $roleid = 1;
	my $rolename = 'Operator';
	my $relaxroles = 0;
	my $symbol_table = getMatchConditionSymbols( $opflags, $roleid, $rolename, $relaxroles );

	my $filter = load_filter_from_table( 'job_filters', $symbol_table );

	my $e = $filter->{mediatype};
	print Dumper( $e );

	return $filter;
}

sub test_get_filter_values {

	test_connect();
	my $query_hash = { newLocationFilter => "Remote_Hub_One" };
	print "Query hash " . Dumper( $query_hash );

	my $filters = test_getFilters();

	my $filter_values = get_filter_values( $query_hash, $filters );
	print "get_filter_values returned " . Dumper( $filter_values );
}

sub test_filter_where_clause {

	test_connect();
	my $query_hash = { 
			newLocationFilter => "Remote_Hub_One,London_Hub", 
			newagencyFilter => "AA_PRODUCTION,CANCER_RESEARCH_UK",
			newmediachoiceidFilter => BLANK,
	};
	print "Query hash " . Dumper( $query_hash );

	my $filters = test_getFilters();

	my $filter_values = get_filter_values( $query_hash, $filters );
	print "get_filter_values returned " . Dumper( $filter_values );

	my $where_clauses = filter_where_clause( $filters, $filter_values );
	print "filter_where_clauses returned " . Dumper( $where_clauses );
}

sub test_load_filter_from_table {

	test_connect();
	my $opflags = { ArchantChk => 1 };
	my $roleid = 1;
	my $rolename = 'Operator';
	my $relaxroles = 0;
	my $symbol_table = getMatchConditionSymbols( $opflags, $roleid, $rolename, $relaxroles );

	# my $filters = load_filter_from_table( 'job_filters', $symbol_table );
	my $filters = load_filter_from_table( 'resource_filters', $symbol_table );

	print Dumper( $filters );
}

# test_getFilters();
# test_get_filter_values();
# test_filter_where_clause();
# test_load_filter_from_table();

1;

