package ECMDBImageworkflow;

use strict;
use Scalar::Util qw( looks_like_number );

use lib 'modules';

require "config.pl";

my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");

my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");


##############################################################################
#		updateImageworkflow
#      
##############################################################################
sub updateImageworkflow {


    my $opcoid       	  = $_[0];
	my $bookingform  	  = $_[1];
	my $xmppath      	  = $_[2];
	my $tmppath      	  = $_[3];
	my $batchserver  	  = $_[4];
	my $autocheckout  	  = $_[5];
	my $autocheckouttime  = $_[6];
	my $autoOPCOID		  = $_[7];
    my $ingestionhost     = $_[8];
    my $ingestionpath     = $_[9];
    my $ingestionprotocol = $_[10];
	my $ingestionusername = $_[11];
	my $ingestionpassword = $_[12];
	my $mastersync        = $_[13];
	my $autocheckin       = $_[14];

	# convert on to 1 and off to 0
	$bookingform  = ($bookingform eq 'on') ? 1 : 0; 
	$autocheckout = ($autocheckout eq 'on') ? 1 : 0; 
	$mastersync   = ($mastersync eq 'on') ? 1 : 0; 
	$autocheckin  = ($autocheckin eq 'on') ? 1 : 0; 
	
	# validate input
	if ( $opcoid && (! looks_like_number( $opcoid )) ) {
		return 0;
	}
	
	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ UPDATE IMAGEWORKFLOW SET 
					bookingform = ?,
					Imagehostprotocol = ?,
					Imagehostpath = ?,
					Imagehost = ?,
					Imagehostusername = ?,
					Imagehostpassword = ?,
					xmppath = ?,
					tmppath = ?,
					batchserver = ?,
					mastersync = ?,
					autocheckin = ?,
					AUTOCHECKOUT = ?,
					AUTOCHECKOUTTIME = ?,
					CHECKOUTOPCOID = ?
					WHERE opcoid = ? };				
	my $sth = $dbh->prepare( $sql );
	$sth->execute( $bookingform, $ingestionprotocol, $ingestionpath, $ingestionhost, $ingestionusername, $ingestionpassword, $xmppath, $tmppath, $batchserver, $mastersync, $autocheckin, $autocheckout, $autocheckouttime, $autoOPCOID, $opcoid );
											
						
	$sth->finish();
	$dbh->disconnect();

	return 1;
			
}


##############################################################################
#      db_isImageBookingForm()
# 	Does this opco use Image Workflow Booking Form?
##############################################################################
sub db_isImageBookingForm {
    my $opcoid	= $_[0];
	
	my $bookingform = 0;
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT bookingform FROM imageworkflow WHERE opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$bookingform );

	if ( $sth->fetch() ) {
		$bookingform = $bookingform;
	}

	$sth->finish();
	$dbh->disconnect();

	return $bookingform;
}


##############################################################################
#       db_isMastersync()
# 	Does this opco use Master Sync?
##############################################################################
sub db_isMastersync {
    my $opcoid	= $_[0];
	
	my $mastersync = 0;
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT mastersync FROM imageworkflow WHERE opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$mastersync );

	if ( $sth->fetch() ) {
		$mastersync = $mastersync;
	}

	$sth->finish();
	$dbh->disconnect();

	return $mastersync;
}


##############################################################################
#       db_isAutocheckin()
# 	Does this opco use Autocheckin?
##############################################################################
sub db_isAutocheckin {
    my $opcoid	= $_[0];
	
	my $autocheckin = 0;
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT autocheckin FROM imageworkflow WHERE opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$autocheckin );

	if ( $sth->fetch() ) {
		$autocheckin = $autocheckin;
	}

	$sth->finish();
	$dbh->disconnect();

	return $autocheckin;
}


##############################################################################
#       db_getXMPPath()
# 	
##############################################################################
sub db_getXMPPath {
    my $opcoid	= $_[0];
	
	my $xmppath = "";
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT xmppath FROM imageworkflow WHERE opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$xmppath );

	if ( $sth->fetch() ) {
		$xmppath = $xmppath;
	}

	$sth->finish();
	$dbh->disconnect();

	return $xmppath;
}


##############################################################################
#       db_getTMPPath()
# 	
##############################################################################
sub db_getTMPPath {
    my $opcoid	= $_[0];
	
	my $tmppath = "";
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT tmppath FROM imageworkflow WHERE opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$tmppath );

	if ( $sth->fetch() ) {
		$tmppath = $tmppath;
	}

	$sth->finish();
	$dbh->disconnect();

	return $tmppath;
}


##############################################################################
#       db_getBatchserver()
# 	
##############################################################################
sub db_getBatchserver {
    my $opcoid	= $_[0];
	
	my $batchserver = "";
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT batchserver FROM imageworkflow WHERE opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$batchserver );

	if ( $sth->fetch() ) {
		$batchserver = $batchserver;
	}

	$sth->finish();
	$dbh->disconnect();

	return $batchserver;
}


##############################################################################
#       db_getBatchuser()
# 	
##############################################################################
sub db_getBatchuser {
    my $opcoid	= $_[0];
	
	my $batchuser = "";
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT batchuser FROM imageworkflow WHERE opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$batchuser );

	if ( $sth->fetch() ) {
		$batchuser = $batchuser;
	}

	$sth->finish();
	$dbh->disconnect();

	return $batchuser;
}


##############################################################################
#       db_getCustomerID()
# 	
##############################################################################
sub db_getCustomerID {
    my $opcoid	= $_[0];
	
	my $customerID = 0;
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT customerid FROM imageworkflow WHERE opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$customerID );

	if ( $sth->fetch() ) {
		$customerID = $customerID;
	}

	$sth->finish();
	$dbh->disconnect();

	return $customerID;
}



##############################################################################
#       db_getMasterCustomerID()
# 	
##############################################################################
sub db_getMasterCustomerID {
    my $opcoid	= $_[0];
	
	my $masterCustomerID = 0;
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT mastercustomerid FROM imageworkflow WHERE opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();
	$sth->bind_columns( \$masterCustomerID );
	if ( $sth->fetch() ) {
		$masterCustomerID = $masterCustomerID;
	}
	$sth->finish();
	$dbh->disconnect();
	return $masterCustomerID;
}



##############################################################################
#       db_getCustomerName() test
# 	
##############################################################################
sub db_getCustomerName {
    my $id	= $_[0];
	
	my $customer = "";
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT customer FROM customers WHERE id = $id };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$customer );

	if ( $sth->fetch() ) {
		$customer = $customer;
	}

	$sth->finish();
	$dbh->disconnect();

	return $customer;
}


##############################################################################
#       db_Autocheckoutdetails()
# 	
##############################################################################
sub db_Autocheckoutdetails {
    	my $opcoid  	= $_[0];
		my $fieldname   = $_[1];

        my $batchuser = "";

        my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
        my $sql = qq{ SELECT $fieldname FROM imageworkflow WHERE opcoid = $opcoid };
        my $sth = $dbh->prepare( $sql );
        $sth->execute();

      	$sth->bind_columns( \$fieldname );

        if ( $sth->fetch() ) {
                $batchuser = $fieldname;
        }

        $sth->finish();
        $dbh->disconnect();

        return $batchuser;
}

##############################################################################
#       db_getBatchpassword()
# 	
##############################################################################
sub db_getBatchpassword {
    my $opcoid	= $_[0];
	
	my $batchpassword = "";
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT batchpassword FROM imageworkflow WHERE opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$batchpassword );

	if ( $sth->fetch() ) {
		$batchpassword = $batchpassword;
	}

	$sth->finish();
	$dbh->disconnect();

	return $batchpassword;
}


##############################################################################
#       getBookingForm
#      
##############################################################################
sub getBookingForm {

    my $opcoid	= $_[0];
	
	my @bookingform;
	
	my ( $id, $label, $fieldname, $fieldvalue, $parentid, $position );

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ SELECT id, label, fieldname, fieldvalue, parentid, position 
				FROM BOOKINGFORM
                WHERE opcoid = $opcoid				
				ORDER BY parentid asc, position asc };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$id, \$label, \$fieldname, \$fieldvalue, \$parentid, \$position );

	while ( $sth->fetch() ) {
		my %row = (
			id 	        => $id,
			label 	    => $label,
			fieldname 	=> $fieldname,
			fieldvalue 	=> $fieldvalue,
			parentid 	=> $parentid,
			position 	=> $position,
		);
		push ( @bookingform, \%row );

	}

	$sth->finish();
	$dbh->disconnect();

	return @bookingform;
}


##############################################################################
#       createJob
#      
##############################################################################
sub createJob {

    my $opcoid	        = $_[0];
	my $imageUniqueID   = $_[1]; #SKU
	my $level1          = $_[2]; #LEVEL1
	my $level2          = $_[3]; #LEVEL2
	my $suffix          = $_[4]; 
	my $cameraOperator  = $_[5];
	my $coverage        = $_[6];
	my $userComment     = $_[7];
	
	my @opco			= ECMDBOpcos::getOperatingCompany( $opcoid );
	my $opconame	    = $opco[0]{ opconame };	
	
	my $job_num         = 400000000;
	my $customerID      = db_getCustomerID($opcoid);
	my $agency          = db_getCustomerName($customerID);
	my $client          = db_getCustomerName($customerID);
	my $letter          = substr($opconame, 0, 1);
	my $checkoutstatus  = 0;
	my $preflightstatus = 5;
	my $qcstatus        = 0;
	my $workflow        = "IMAGE";
	my $pathtype        = 1;
	#my $operatorstatus  = "B";
	my $operatorstatus  = "W";
	
	
	
	#GET JOB NUM
	my $dbs = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sqs = qq{ SELECT JOB_NUMBER FROM tracker3 WHERE opcoid = $opcoid order by JOB_NUMBER desc LIMIT 1 };
	my $sts = $dbs->prepare( $sqs );
	$sts->execute();

	$sts->bind_columns( \$job_num );

	if ( $sts->fetch() ) {
		$job_num = $job_num +=1;
	}

	$sts->finish();
	$dbs->disconnect();
	
	
	#Appendix
	if($suffix ne ''){
	   $imageUniqueID = $imageUniqueID."_".$suffix;
	}
	
	
	#DB INSERT
	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ INSERT INTO TRACKER3 (JOB_NUMBER, AGENCY, CLIENT, PROJECT, LETTER, OPCOID, HEADLINE, WORKFLOW, AGENCYREF, PATHTYPE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) };				
	my $sth = $dbh->prepare( $sql );
	$sth->execute( $job_num, $agency, $client, $level1, $letter, $opcoid, $level2, $workflow, $imageUniqueID, $pathtype );
	$sth->finish();
	
	my $sqla = qq{ INSERT INTO TRACKER3PLUS (JOB_NUMBER, OPCOID, CUSTOMERID, LOCATION, CHECKOUTSTATUS, PREFLIGHTSTATUS, QCSTATUS, ASSIGNEE, DUEDATE, OPERATORSTATUS, BRIEF, REC_TIMESTAMP) VALUES (?, ?, ?, ?, ?, ?, ?, ?, DATE_ADD(Now(), INTERVAL $coverage MINUTE), ?, ?, Now()) };
	my $stha = $dbh->prepare( $sqla );
	$stha->execute( $job_num, $opcoid, $customerID, $opcoid, $checkoutstatus, $preflightstatus, $qcstatus, $cameraOperator, $operatorstatus, $userComment );
	$stha->finish();
	
	my $sqlb = qq{ INSERT INTO IMAGEWORKFLOWTRACKER (JOB_NUMBER, SKU, SUFFIX) VALUES (?, ?, ?) };
	my $sthb = $dbh->prepare( $sqlb );
	$sthb->execute( $job_num, $imageUniqueID, $suffix );
	$sthb->finish();
	
	
	$dbh->disconnect();
	
    return $job_num;
}


##############################################################################
#       getCameraOperators
#      
##############################################################################
sub getCameraOperators {

    my $opcoid	= $_[0];
	
	my @operators;
	
	my ( $username, $userid );

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ SELECT u.username, io.userid as userid FROM imageworkflowoperators io
                  INNER JOIN users u ON u.id = io.userid
                  WHERE io.opcoid = $opcoid				
				  ORDER BY username };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$username, \$userid );

	while ( $sth->fetch() ) {
		my %row = (
			username    => $username,
			userid 	    => $userid,
	
		);
		push ( @operators, \%row );

	}

	$sth->finish();
	$dbh->disconnect();

	return @operators;
}


##############################################################################
#       imageCheckout
#      
##############################################################################

sub imageCheckin {

     my $jobid	  = $_[0];
	 my $opcoid   = $_[1];
	 my $userid   = $_[2];
	 
	 my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	 my $sql = qq{INSERT INTO reposendjobsqueue (JOB_NUMBER, OPCOID, USERID, CHECKINGIN, CHECKEDIN, REC_TIMESTAMP) VALUES (?, ?, ?, 0, 0, Now()) };
	 my $sth = $dbh->prepare( $sql );
	 $sth->execute($jobid, $opcoid, $userid);
	 $sth->finish();

}


##############################################################################
#       checkSKU
#      
##############################################################################

sub checkSKU {

	my $sku	  = $_[0];

	my $result = 0;
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT * FROM imageworkflowtracker WHERE sku = $sku };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	if ( $sth->fetch() ) {
		$result = 1;
	}

	$sth->finish();
	$dbh->disconnect();

	return $result;

}


##############################################################################
#       getSKU/Image Name
#      
##############################################################################

sub getSKU {

	my $job_number = $_[0];

	my $agencyref = "";
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT agencyref FROM tracker3 WHERE job_number = $job_number };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	
	$sth->bind_columns( \$agencyref );

	if ( $sth->fetch() ) {
		$agencyref = $agencyref;
	}

	$sth->finish();
	$dbh->disconnect();

	return $agencyref;

}

##############################################################################
#       getUserComment
#      
##############################################################################

sub getUserComment {

	my $job_number = $_[0];
	my $opcoid     = $_[1];

	my $brief = "";
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT brief FROM tracker3plus WHERE job_number = $job_number AND opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	
	$sth->bind_columns( \$brief );

	if ( $sth->fetch() ) {
		$brief = $brief;
	}

	$sth->finish();
	$dbh->disconnect();

	return $brief;

}

##############################################################################
#       getDescription
#      
##############################################################################

sub getDescription {

	my $job_number = $_[0];
	my $opcoid     = $_[1];

	my $headline = "";
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT headline FROM tracker3 WHERE job_number = $job_number AND opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	
	$sth->bind_columns( \$headline );

	if ( $sth->fetch() ) {
		$headline = $headline;
	}

	$sth->finish();
	$dbh->disconnect();

	return $headline;

}

##############################################################################
#       getIdentifier
#      
##############################################################################

sub getIdentifier {

	my $job_number = $_[0];
	my $opcoid     = $_[1];

	my $businessunitref = "";
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT businessunitref FROM tracker3 WHERE job_number = $job_number AND opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	
	$sth->bind_columns( \$businessunitref );

	if ( $sth->fetch() ) {
		$businessunitref = $businessunitref;
	}

	$sth->finish();
	$dbh->disconnect();

	return $businessunitref;

}


##############################################################################
#       isImageFTP
#      
##############################################################################

sub isImageFTP {

	my $opcoid	= $_[0];

	my $result = 0;
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT imagehostprotocol FROM imageworkflow WHERE imagehostprotocol = 'FTP' and opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	if ( $sth->fetch() ) {
		$result = 1;
	}

	$sth->finish();
	$dbh->disconnect();

	return $result;

}


##############################################################################
#       db_getMastersHotPath()
# 	
##############################################################################
sub db_getMastersHotPath {
    my $opcoid	= $_[0];
	
	my $mastersHotPath = "";
	
	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT mastershotpath FROM imageworkflow WHERE opcoid = $opcoid };
	my $sth = $dbh->prepare( $sql );
	$sth->execute();

	$sth->bind_columns( \$mastersHotPath );

	if ( $sth->fetch() ) {
		$mastersHotPath = $mastersHotPath;
	}

	$sth->finish();
	$dbh->disconnect();

	return $mastersHotPath;
}

################################################################################
#   db_setAssignee
#
#
################################################################################
sub db_clearAssignee {
    my $opcoid	  = $_[0];
	my $jobid	  = $_[1];
	
	
	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ UPDATE TRACKER3PLUS SET 
					assignee = 0,
					operatorstatus = 'U'
					WHERE opcoid = ?
                    AND job_number = ?};				
	my $sth = $dbh->prepare( $sql );
	$sth->execute( $opcoid, $jobid );
											
						
	$sth->finish();
	$dbh->disconnect();

	return 1;
}


##############################################################################
#       updateCurlhistory
#      
##############################################################################
sub updateCurlhistory {

	my ( $job_number, $curl ) = @_;

	my $curlType        = 4;     # The Repo daemons use 1 checkin, 2 checkout, 3 sync

	# curl is VARCHAR(255), so trim it down if too long
	if ( length ( $curl ) > 255 ) {
		$curl = substr( $curl, 0, 252 ) . '...'; 
	}

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ INSERT INTO repocurlhistory ( job_number, curltype, curl, rec_timestamp ) VALUES (?, ?, ?, now()) };				
	my $sth = $dbh->prepare( $sql );
	$sth->execute( $job_number, $curlType, $curl );
			
	$sth->finish();
	$dbh->disconnect();

	return 1;
	
}


##############################################################################
#       updateJob
#      
##############################################################################
sub updateJob {

    my $job_number	    = $_[0];
	my $opcoid          = $_[1];
	my $userComment     = $_[2]; 
	my $identifier      = $_[3];
	my $coverage        = $_[4];
	
	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ UPDATE TRACKER3PLUS SET 
					brief = ?,
					duedate = DATE_ADD(now(), INTERVAL $coverage MINUTE)
					WHERE opcoid = ?
                    AND job_number = ?};				
	my $sth = $dbh->prepare( $sql );
	$sth->execute( $userComment, $opcoid, $job_number );
	
	$sql = qq{ UPDATE TRACKER3 SET 
					businessunitref = ?
					WHERE opcoid = ?
                    AND job_number = ?};				
	$sth = $dbh->prepare( $sql );
	$sth->execute( $identifier, $opcoid, $job_number );
											
						
	$sth->finish();
	$dbh->disconnect();

	return 1;
	
}

1;

