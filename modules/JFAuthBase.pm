#!/usr/local/bin/perl

package JFAuthBase;

use Carp qw/cluck carp/;

my $self = { auth_id => 0, auth_name => '', debug => 0 };

sub new {
	my ($class, $auth_id, $auth_name, $auth_table) = @_;

	$self = bless { auth_id => $auth_id, auth_name => $auth_name, auth_table => $auth_table }, $class;
	return $self;
}

# Read settings from associated table, so we have settings tailored to the type of auth system

sub init {
	my $self = shift;
	
	my $auth_table = $self->{auth_table};
	if ( defined $auth_table ) {
		# carp "Reading settings from table $auth_table";
		eval {
			# Read columns setting, value from the table
			my $rows = $Config::dba->hashed_process_sql( "SELECT setting, value FROM $auth_table" );

			foreach my $r ( @$rows ) {
				$self->{ $r->{setting} } = $r->{value};
			}
		};
		if ( $@ ) {
			die "Error reading settings from table $auth_table, $@";
		}
	}
}

sub setDebug {
	my ( $class, $debug ) = @_;
	$class->{ debug } = $debug;
}

#
# Authenticate user, given credentials (principle + password)
# details = authenticate( username, password [, $history ] );
# - history if supplied should be an array ref
#
# return hash with details (or undef if not authenticated)
# - userid	userid (query what happens for temp users, userid = 2, tempuser set to userstemp.id)
#		for Active Directory this can be undef, meaning the credentials are not associated with a Jobflow userid
# - username	username (can be present even if userid is not)
# - fname	first name
# - lname	last name
# - email	email address
# - rolename	role name (depends on userid)
# - roleid	role id   (depends on userid)
# - tempuser	0 (if userid from users table) or > 0 if id from userstemp 
#
# Returns undef if authentication failed.
#

sub authenticate {
	my $class = shift;
	my ( $principle, $password, $history ) = @_;

	die "authenticate not valid for JFAuthBase package";
}

sub bind_auth {
	my $class = shift;
	my ( $userid, $email, $user_principle_name, $sam_account_name ) = @_;

	die "bind_auth not valid for JFAuthBase package";
}

sub unbind_auth {
	my $class = shift;
	my ( $userid ) = @_;

	die "unbind_auth not valid for JFAuthBase package";
}

sub get_potential_matches {
	die "get_potential_matches not valid for JFAuthBase package";
}

sub auth_failure {
	my $class = shift;
	my ( $auth_id, $username, $reason ) = @_;

	my $remote_host = $ENV{REMOTE_HOST} || $ENV{REMOTE_ADDR} || "Unknown";

	carp "auth_failure: auth_id $auth_id, host $remote_host, username $username, reason $reason";

	$username = substr( $username, 0, 98 ) . ".." if ( length( $username ) > 100 );
	$remote_host = substr( $remote_host, 0, 62 ) . ".." if ( length( $remote_host ) > 64 );
	$reason = substr( $username, 0, 48 ) . ".." if ( length( $reason ) > 50 ); 
	eval {
		$Config::dba->process_sql( "INSERT INTO auth_failures ( auth_id, principal, ip_address, reason ) VALUES ( ?, ?, ?, ? )",
			[ $auth_id, $username, $remote_host, $reason ] ); 
	};
	if ( $@ ) {
		carp "JFAuthBase::auth_failure: $@";
	}
}

1;

