#!/usr/local/bin/perl

use DBI;
use strict;
use warnings;
require "databaseJob.pl";

my $tmp;

print "Test add to autoArchive Queue\n";
my $job_number  = 100000001;
my $userid	= 2;
my $opcoid	= 5544;
my $numdays	= 14;

$tmp	= 	ECMDBJob::enQueueRepoAutoArchive( $job_number, $opcoid, $userid, $numdays );

print "Test add to autoArchive Queue complete \n";
