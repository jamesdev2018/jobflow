#!/usr/local/bin/perl
package ECMDBOne2Edit;

use strict;
use warnings;

# use Switch; # doesnt use switch statement

use Scalar::Util qw( looks_like_number );
use Carp qw/carp cluck/;
use Data::Dumper;
use URI::Escape;

# Home grown

use lib 'modules';

use Validate;

require "config.pl";

sub checkEntryInOne2EditMaster {
	my ( $job_number, $filename ) = @_;

	my $rows = $Config::dba->hashed_process_sql("SELECT folder_id, document_id, document_name FROM one2edit_master WHERE job_number=? LIMIT 1",[ $job_number ]);

	return $rows ? $rows : ();
}


sub getJobDetails {
	my ( $job_number ) = @_;

	my $rows = $Config::dba->hashed_process_sql("SELECT t3.agency AS customer, t3.client, t3.project AS campaign  FROM tracker3 t3 WHERE t3.job_number=?",[ $job_number ]);

    return $rows ? $rows : ();
}

sub getColorManageSetting {
	 my ( $job_number ) = @_;

    my $rows = $Config::dba->hashed_process_sql(
		"SELECT t3p.colourmanage, t3p.colourmanage_isactive FROM tracker3plus t3p WHERE t3p.job_number=?", [ $job_number ] );

    return $rows ? $rows : ();
}

sub getOne2EditQueueDetails {
	my ( $limit, $search_type, $search_value ) = @_;

	my $where = "";
	if ( $search_type ne "" && $search_value ne "" ) {
		$where	=	"WHERE $search_type like '%$search_value%'";
	}
	my $rows = $Config::dba->hashed_process_sql(
		"SELECT id,job_number,document_id,document_name,progress,rec_timestamp FROM one2edit_queue $where ORDER BY rec_timestamp desc limit $limit" );

	return $rows ? $rows : ();
}

sub getOne2EditCampaignList {
	my( $limit, $search_type, $search_value ) = @_;

	my $where = "";
	if ( ($search_type eq "document_name" || $search_type eq "job_number") && $search_value ne "") {
		$where  =   "WHERE $search_type like '%$search_value%'";
	}
	my $rows =  $Config::dba->hashed_process_sql(
			"SELECT group_concat(job_number) as job_number,campaign_path FROM one2edit_master $where group by campaign_path order by id desc limit $limit");

	return $rows ? $rows : ();
}

# Get the jobs in a campaign
# Does a query on tracker3 (to find the jobs in the campaign), 
# LOJ one2edit_master m to get m.document_id, m.document_name (so only for jobs used to create a Master document)
# LOJ one2edit_version v to get version_doc_id, version_completed
 
sub db_getJobsInCampaign {
	my ( $customer, $client, $campaign ) = @_;

	# warn "db_getJobsInCampaign: Customer $customer, Client $client, Campaign $campaign\n";
	my $rows = $Config::dba->hashed_process_sql(
		"SELECT t3.job_number, " .
		"  COALESCE(m.document_id,'-') AS master_document_id, COALESCE(m.document_name,'') AS document_name, " .
		"  COALESCE(v.version_doc_id,'-') AS version_doc_id, COALESCE(v.master_doc_id,'-') AS version_master_document_id, COALESCE(v.completed,'-') AS version_completed, " .
		"  COALESCE(a.master_job_no,'-') AS assoc_job_no, COALESCE(a.master_doc_id,'-') AS assoc_doc_id, COALESCE(a.master_doc_name,'-') AS assoc_doc_name " . 
		"FROM tracker3 t3 " .
		"LEFT OUTER JOIN one2edit_master m ON m.job_number = t3.job_number " .
		"LEFT OUTER JOIN one2edit_version v ON v.job_number = t3.job_number " .
		"LEFT OUTER JOIN one2edit_associate a ON a.job_number = t3.job_number " .
		"WHERE t3.agency = ? AND t3.client = ? AND t3.project = ?",
		[ $customer, $client, $campaign ] );

	return $rows ? $rows : ();
}

#######################################################################################
#Get count from jobevents
#There are 3 constants starting One2Edit 
#i) One2Edit, used in One2EditDaemon.pl
#ii) One2Edit_Master, when user uploads a Master
#iii) One2Edit_Version, when user creates a Version
######################################################################################

sub getProcessCount {
	my ($days) = @_;

	my $where = "";
	if ( $days > 0 ) {
		$where 	=	"AND rec_timestamp >= DATE_SUB(CURDATE(), INTERVAL $days  DAY)";
	}
	my ($count)	=	$Config::dba->process_oneline_sql("SELECT count(*) FROM jobevents WHERE action = 'one2edit_version' $where");

	return $count || 0;
}

1;

