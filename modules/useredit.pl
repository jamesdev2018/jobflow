#!/usr/local/bin/perl

package UserEdit;

use strict;
use warnings;

use CGI::Session;
use CGI::Cookie;
use CGI;
use CGI::Carp qw( confess );
use Scalar::Util qw( looks_like_number );
use Encode qw(decode encode);
use JSON;
use Template;
use Crypt::CBC;

# Home grown - please do NOT include jobs.pl or databaseJob.pl or anything that includes jobs.pl

use lib 'modules';

use CTSConstants;
use SessionConstants;

require "config.pl";
require "htmlhelper.pl"; # setHeaderVars etc

require "databaseUser.pl";
require "databaseUserNotification.pl";	# Getwholeusernotification
require "databaseOpcos.pl"; 			# db_getdepartments, db_getUserSkills
require "databaseCompanies.pl";			# getCompanies

require "users.pl"; # getTempPassword

#
# Functions		Action/Subaction	Source of action
#			(in jobflow.pl)
#
# Listusers 		listusers		listusers.html + others
# Adduser 		adduser			lefthandnav.html
# Addvendor 		addvendor		addvendor.html, lefthandnav.html "Add Vendor"
# Edituser 		edituser		edituser.html via EditUser() + listusers.html (commented out)
# validateUser 		(local use only)
# AddUsersToOpco 	adduserstoopco		addUsersToOpco.html + lefthandnav
# EditSettings 		settings		lefthandnav "Account Settings"
# Deleteuser 		deleteuser		?
#

##################################################################
# from config // Khalid has changed this comment again 16:24
##################################################################

my $jobflowServer	 = Config::getConfig("jobflowserver");
my $applicationurl	 = $jobflowServer . "/jobflow.pl"; # Used in EditSettings

#View_Message Days Get Here
my $view_messages        = Config::getConfig("view_messages_max_days");


# Mapping of names (back from front end) to column names in users table

my $checkbox_names_to_columns = { # key is column name, value is input name in HTML
		userstatus => 'userstatus',
		broadcast => 'broadcast',
		catalogue => 'catalogue',
		directmarketing => 'Direct_Marketing',
		interactive => 'interactive',
		literature => 'Literature',
		outdoor => 'Outdoor_OOH',
		packaging => 'packaging',
		pointofsale => 'POS',
		press => 'Press',
		artworker => 'Artworker',
		creative => 'Creative',
		design => 'Design',
		junior => 'Junior',
		retouching => 'Retouching',
	};


##############################################################################
# the template object 
#
##############################################################################
my $tt = Template->new({
	RELATIVE => 1,
}) || die "$Template::ERROR\n";

# Decode the , ~ format of userskills currently used

sub decode_user_skills {
	my ( $userSkills ) = @_;

	return undef unless defined $userSkills;

	my @userSkillsMap;
	foreach my $skill ( split /,/, $userSkills ) {
		my ( $skillid, $skilloption ) = split /~/, $skill;
		push( @userSkillsMap, { skillid => $skillid, skilloption => $skilloption } );
	}
	return \@userSkillsMap;
}

# Decode the , ~ format of usermediatypes currently used

sub decode_user_mediatypes {
	my ( $userMediaTypes ) = @_;

	return undef unless defined $userMediaTypes;

	my @userMediaTypesMap;
	foreach my $mediatype ( split /,/, $userMediaTypes ) {
		my ( $mediatypeid, $mediaoption ) = split /~/, $mediatype;
		push( @userMediaTypesMap, { mediatypeid => $mediatypeid, mediaoption => $mediaoption } );
	}
	return \@userMediaTypesMap;
}

##############################################################################
#       Listusers
#
##############################################################################
sub Listusers {
	my ( $query, $session, $options ) = @_;

	my $lname			= $query->param( "lname" );
	my $sidemenu		= $query->param( "sidemenu" );
	my $sidesubmenu		= $query->param( "sidesubmenu" );
	my $searchById		= $query->param( "searchById" );
	my $buttontype		= $query->param( "buttontype" ) || '';
	my $searchByuserID	= $query->param( "searchByuserID" ) || '';
	my $userID			= $query->param( "userID" );

	my $roleid			= $session->param( 'roleid' );

	my @users;

	# Overload (when called by edituser, which provides 3rd arg).
	$options = { } unless defined $options;
	my $newuser = $options->{newuser}; # Set by call from edituser with subaction 'add'
	my $edituser = $options->{edituser}; # Set by call from edituser with subaction 'edit' in which case edituser is userid of user

	# If button type is 'Me' the search has to be based on 'lastname/surname' of the login current user
	if ( $buttontype eq "Me" ) {
		my $userid = $session->param( 'userid' );
		my $lname  = ECMDBUser::db_getlastname($userid);
		@users = ECMDBUser::db_getUsersBySurname( $lname );
	} elsif ( $buttontype eq "Search" ) {
		@users = ECMDBUser::db_getUsers( $lname, $searchById ); ##lname brings the text value. Searchbyid value shows us the selected value
	} elsif ( $searchByuserID =~ /^YES$/i && $userID ne "" ) {
		@users = ECMDBUser::db_getUsersByUserID( $userID ); #To get user based on user id.
	} else {
		# The default case is to display all users (or as many as db_getUsersBySurname allows), even if newuser or edituser set
		@users = ECMDBUser::db_getUsersBySurname( $lname );	##For list users
	}

	my @mediatypes  = getMediatypes();
	my @departments = ECMDBOpcos::db_getdepartments();
	my @userskills  = ECMDBOpcos::getUserskills();
 	my @roles = ECMDBUser::getRoles();


	my $vars = {	
		sidemenu	=> $sidemenu,
		sidesubmenu	=> $sidesubmenu,
		session		=> $session,
		users		=> \@users,
		mediatypes	=> \@mediatypes,
		departments	=> \@departments,
		userskills	=> \@userskills,
		roleid		=> $roleid,
		searchById	=> $searchById,
		newuser		=> $newuser,
		edituser	=> $edituser,
		roles		=> \@roles
	 };

	HTMLHelper::setHeaderVars( $vars, $session );

	$vars->{extracss} = [ "listusers" ]; # include styles/listusers.css

	$tt->process( 'ecm_html/listusers.html', $vars )
		    || die $tt->error(), "\n";
}

##############################################################################
#       Adduser
#
##############################################################################
sub Adduser_deprecated {
	my ( $query, $session ) = @_;

	# Several issues still
	# a) no password supplied from form, form doesnt have password field
	# b) only some media types are passed, eg 
	# Press=false&Outdoor_OOH=false&POS=false&Literature=false&Direct_Marketing=false&
	# when I've checked Literature and Retouching

	# userid is 0 so ignore
	my $opcoid	= $query->param( "opcoid" );  # can be 0 in which case make undef
	my $companyid	= $query->param( "companyid" ); # not supplied so undef
	my $fname		= $query->param( "fname" );
	my $lname		= $query->param( "lname" );
	my $username	= $query->param( "username" );
	my $password	= $query->param( "password" );
	my $email		= $query->param( "email" );
	my $employeeid	= $query->param( "employeeid" );
	#my $roleid		= $query->param( "roleid" );
	my $roleid		= $query->param( "defroleid" );

	my $formPosted	= $query->param( "formPosted" );
	my $sidemenu	= $query->param( "sidemenu" );
	my $sidesubmenu	= $query->param( "sidesubmenu" );

	# Do call to HTMLHelper::htmlEditFormat( ) AFTER call to query->param to avoid CGI warnings
	$fname		= HTMLHelper::htmlEditFormat( $fname );
	$lname		= HTMLHelper::htmlEditFormat( $lname );
	$username	= HTMLHelper::htmlEditFormat( $username );
	$password	= HTMLHelper::htmlEditFormat( $password );
	$email		= HTMLHelper::htmlEditFormat( $email );
	$employeeid	= HTMLHelper::htmlEditFormat( $employeeid );

	my $oktogo = 1;
	my %error;
	$error{ oktogo } = 1;
	my $vars = { };

	if ( $formPosted eq "yes" ) {
		%error = validateUser( $fname, $lname, $username, $password, $email );
		if ( $error{ oktogo } == 1 ) {
			# encrypt the password
			my $cryptpassword  = crypt( $password, "ab");

			my $options = { 
				deputyemail => $query->param( "deputyemail" ),
				department => $query->param( "departmentid" ),
				cost_center => $query->param( "costid" ),
				contracted_hours => $query->param( "contractid" ) || undef,
				workhoursstarts => $query->param( "workhrsstart" ),
				workhoursends => $query->param( "workhrsend" ),
			};

# TODO1 convert the checkboxes including userstatus

			my ( $success, $userid ) = ECMDBUser::addUser( $opcoid, $companyid, $fname, $lname, $username, $cryptpassword, $email, $employeeid, $roleid, $options );

			my $userSkills = $query->param( "user_skills" );
			if ( defined $userSkills ) {
				my $userSkillsMap = decode_user_skills( $userSkills );
				ECMDBUser::updateUserSkillsMap( $userid, $userSkillsMap );
			}

			my $userMediaTypes = $query->param( "medialist" );
			if ( defined $userMediaTypes ) {
				my $userMediaTypesMap = decode_user_mediatypes( $userMediaTypes );
				ECMDBUser::updateUserMediaTypes( $userid, $userMediaTypesMap );
			}
	
			HTMLHelper::setHeaderVars( $vars, $session );
	
			$tt->process( 'ecm_html/useraddedsuccess.html', $vars ) || die $tt->error(), "\n";
			return;
		}
	}
	
	# get the operating companies.
	my @opcos = ECMDBOpcos::getOperatingCompanies();

	# get the companies.
	my @companies = ECMDBCompanies::getCompanies();
	
	# get the role types
	my @roles = ECMDBUser::getRoles( );

	my @mediatypes  = getMediatypes();
	my @departments = ECMDBOpcos::db_getdepartments();
	my @userskills  = ECMDBOpcos::getUserskills();
	
	$vars = {
		opcos		=> \@opcos,
		companies	=> \@companies,
		roles		=> \@roles,
		opcoid		=> $opcoid,
		fname		=> $fname,
		lname		=> $lname,
		username	=> $username,
		email		=> $email,
		employeeid	=> $employeeid,
		roleid		=> $roleid,
		error		=> \%error,
		sidemenu	=> $sidemenu,
		sidesubmenu	=> $sidesubmenu,
		roleid		=> $roleid,
		mediatypes	=> \@mediatypes,
		departments	=> \@departments,
		userskills	=> \@userskills,
	 };

	HTMLHelper::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/adduser.html', $vars ) || die $tt->error(), "\n";
}

####################################################
# Add Vendor
# ##################################################

sub Addvendor {
	my ( $query, $session ) = @_;

	my $opcoid      = $query->param( "opcoid" );
	my $companyid   = $query->param( "companyid" );

	my $fname	= $query->param( "fname" );
	my $lname	= $query->param( "lname" );
	my $username	= $query->param( "username" );
	my $password	= $query->param( "password" );
	my $email	= $query->param( "email" );
	my $employeeid	= $query->param( "employeeid" );
	my $roleid	= $query->param( "roleid" );

	my $formPosted	= $query->param( "formPosted" ) || "";
	my $sidemenu	= $query->param( "sidemenu" );
	my $sidesubmenu	= $query->param( "sidesubmenu" );

	# Avoid the CGI warnings ...

	$fname       = HTMLHelper::htmlEditFormat( $fname );
	$lname       = HTMLHelper::htmlEditFormat( $lname );
	$username    = HTMLHelper::htmlEditFormat( $username );
	$password    = HTMLHelper::htmlEditFormat( $password );
	$email       = HTMLHelper::htmlEditFormat( $email );
	$employeeid  = HTMLHelper::htmlEditFormat( $employeeid );

	my $oktogo      = 1;
	my %error;
	$error{ oktogo } = 1;

	my $vars = { };
	
	if ( $formPosted eq "yes" ) {
		
		%error = validateUser( $fname, $lname, $username, $password, $email );
		if ( $error{ oktogo } == 1 ) {
			# encrypt the password
			#my $cryptpassword  = crypt( $password, "ab");

			eval {
				ECMDBUser::addUser( $opcoid, $companyid, $fname, $lname, $username, $password, $email, $employeeid, $roleid );
			};
			if ( $@ ) {
				my $saved = $@;
				$saved =~ s/ at mod.*//gs;
				$error{ oktogo } = 0;
				$error{ submit } = $saved;
			}
			if (  $error{ oktogo } == 1 ) {
				# Dont bother adding userskills or mediatypes mapping for Vendor

				HTMLHelper::setHeaderVars( $vars, $session );
				$tt->process( 'ecm_html/vendoraddedsuccess.html', $vars ) || die $tt->error(), "\n";
				return;
			}
		}
	}

	# Get the operating companies.
	my @opcos = ECMDBOpcos::getOperatingCompanies();
 
	# Get the companies.
	my @companies = ECMDBCompanies::getCompanies();
 
	# get the role types
	my @roles = ECMDBUser::getRoles( );

	$vars = {
		opcos		=> \@opcos,
		companies	=> \@companies,
		roles		=> \@roles,
		opcoid		=> $opcoid,
		fname		=> $fname,
		lname		=> $lname,
		username	=> $username,
		email		=> $email,
		employeeid      => $employeeid,
		roleid		=> $roleid,
		error		=> \%error,
		sidemenu	=> $sidemenu,
		sidesubmenu     => $sidesubmenu,
		roleid		=> $roleid,
	};

	HTMLHelper::setHeaderVars( $vars, $session );
	$tt->process( 'ecm_html/addvendor.html', $vars ) || die $tt->error(), "\n";
}

##############################################################################
#       Edituser
#
#	Replace what was in ajax/saveuser.pl which either creates a new user
#	or updates an existing user.
#
##############################################################################
sub Edituser {
	my ( $query, $session ) = @_;

	my $subaction = $query->param( "subaction" ) || "";
	my $vars;
	eval {
		if ( $subaction eq "create" ) {
			create_user( $query, $session );

		} elsif ( $subaction eq "update" ) {
			update_user( $query, $session );

		} elsif ( $subaction eq "add" ) {
			# This should be from sub menu "Add User" on "List Users" menu 
			# Display edit settings page with defaults for new user
			Listusers( $query, $session, { newuser => 1 } );

		} elsif ( $subaction eq "edit" ) {
			# This should be used as link from email to system admins
			my $userid = $query->param( "userid" ); # Userid of user being edited rather than current user
			my $exists = ECMDBUser::userExistsById( $userid ); # returns 0 if userid invalid or userid doesnt exist
			die "User does not exist" unless $exists;
			Listusers( $query, $session, { edituser => $userid } );

		} elsif ( $subaction eq "password" ) {
			setUserPassword( $query, $session );

		} else {
			die "Edituser: Invalid subaction '$subaction'";
		}
	};
	if ( $@ ) {
		my $error = $@;
		#$error =~ s/ at mod.*//gs;
		$error =~ s/at module.*//g;
		$vars = { error => $error };
	}
	if ( $vars ) {
		my $json = encode_json ( $vars );
		print $json;
	}
}

# Create new user with attached skills types and media types

sub create_user {
	my ( $query, $session ) = @_;

	# Wrapper for ECMDBUser::addUser( $opcoid, $companyid, $fname, $lname, $username, $password, $email, $employeeid, $roleid, $options );

	my $opcoid = $query->param("opcoid");  # Can be undef
	$opcoid = undef if ( defined $opcoid ) && ( $opcoid == 0 );

	my $companyid; # Not sure about this but column is INT and nullable

	my $fname	= $query->param("fname");
	my $lname	= $query->param("lname");
	my $username	= $query->param("username");
	my $password	= $query->param("password");
	my $email	= $query->param("email");
	my $employeeid	= $query->param("employeeid");
	my $roleid	= $query->param("defroleid");

	$password = Users::getTempPassword() unless ( defined $password );

	my $options = { # The keys in this hash are the column names in users table (not checkboxes)
		deputyemail		=> $query->param( 'deputyemail' ),
		department		=> $query->param( 'departmentid' ) || 0,  # NOT NULL column
		cost_center		=> $query->param( 'costid' ),
		contracted_hours	=> $query->param( 'contractid' ) || undef,
		workhoursstarts		=> $query->param( 'workhrsstart' ),
		workhoursstarts_min	=> $query->param( 'workhrsstart_min' ),
		workhoursends		=> $query->param( 'workhrsend' ),
		workhoursends_min	=> $query->param( 'workhrsend_min' ),
	};

	# Then add the checkbox options

	foreach my $k ( keys %$checkbox_names_to_columns ) {
		my $param_name = $checkbox_names_to_columns->{ $k };  # In some cases the query param name is different to the key used by addUser in options
		my $param_value = $query->param( $param_name ) || 'false';
		$options->{ $k } = ( $param_value eq 'true' ) ? 1 : 0; # So 1 if checkbox is set
	}

	my $vars;
	eval {
		# addUser validates the important options

		my ( $success, $userid ) = ECMDBUser::addUser( $opcoid, $companyid, $fname, $lname, $username, $password, $email, $employeeid, $roleid, $options );
		$vars = { userid => $userid };

		my $userSkills = $query->param( "user_skills" );
		if ( defined $userSkills ) {
			my $userSkillsMap = decode_user_skills( $userSkills );
			ECMDBUser::updateUserSkillsMap( $userid, $userSkillsMap );
		}

		my $userMediaTypes = $query->param( "medialist" );
		if ( defined $userMediaTypes ) {
			my $userMediaTypesMap = decode_user_mediatypes( $userMediaTypes );
			ECMDBUser::updateUserMediaTypes( $userid, $userMediaTypesMap );
		}
	
	};
	if ( $@ ) {
		$vars = { error => $@ }; 
	}
	my $json = encode_json ( $vars );
	print $json;
}

# Update user and return JSON response

sub update_user {
	my ( $query, $session ) = @_;

	my $userid	= $query->param( "userid" );
	my $opcoid	= $query->param( "opcoid" );
	my $fname	= $query->param( "fname" );
	my $lname	= $query->param( "lname" );
	my $username	= $query->param( "username" );
	#my $password	= $query->param( "password" ); # seperate ajax call to update password
	my $email	= $query->param( "email" );
	my $employeeid	= $query->param( "employeeid" );
	my $roleid	= $query->param( "defroleid");

	# Avoid the CGI warnings ie dont call a function with arg ( $query->param( "something" ) )
	$fname		= HTMLHelper::htmlEditFormat( $fname );
	$lname		= HTMLHelper::htmlEditFormat( $lname );
	$username	= HTMLHelper::htmlEditFormat( $username );
	$email		= HTMLHelper::htmlEditFormat( $email );
	$employeeid	= HTMLHelper::htmlEditFormat( $employeeid );
	$roleid		= HTMLHelper::htmlEditFormat( $roleid );

	$opcoid = undef if $opcoid eq ''; # Allow opcoid to be undefined

	my $options = {
		deputyemail		=> $query->param( 'deputyemail' ),
		department		=> $query->param( 'departmentid' ) || 0,  # NOT NULL column
		cost_center		=> $query->param( 'costid' ),
		contracted_hours	=> $query->param( 'contractid' ) || undef, # SMALLINT is NULL if blank
		workhoursstarts		=> $query->param( 'workhrsstart' ),
		workhoursstarts_min	=> $query->param( 'workhrsstart_min' ),
		workhoursends		=> $query->param( 'workhrsend' ),
		workhoursends_min	=> $query->param( 'workhrsend_min' ),
	};

	# Then add the checkbox options

	foreach my $k ( keys %$checkbox_names_to_columns ) {
		my $param_name = $checkbox_names_to_columns->{ $k };  # In some cases the query param name is different to the key used by addUser in options
		my $param_value = $query->param( $param_name ) || 'false';
		$options->{ $k } = ( $param_value eq 'true' ) ? 1 : 0; # So 1 if checkbox is set
	}

	my $vars;
	eval {
		ECMDBUser::updateUser( $userid, $opcoid, $fname, $lname, $username, $email, $employeeid, $roleid, $options );
		$vars = { userid => $userid };

		my $userSkills = $query->param( "user_skills" );
		if ( defined $userSkills ) {
			my $userSkillsMap = decode_user_skills( $userSkills );
			ECMDBUser::updateUserSkillsMap( $userid, $userSkillsMap );
		}

		my $userMediaTypes = $query->param( "medialist" );
		if ( defined $userMediaTypes ) {
			my $userMediaTypesMap = decode_user_mediatypes( $userMediaTypes );
			ECMDBUser::updateUserMediaTypes( $userid, $userMediaTypesMap );
		}
	};
	if ( $@ ) {
		$vars = { error => "Error in update_user: $@" }; 
	}
	my $json = encode_json ( $vars );
	print $json;
}

sub setUserPassword {
	my ( $query, $session ) = @_;

	my $userid = $query->param("userid");
	my $password = $query->param("password");

	$password = crypt($password, "ab");
	my $vars;
	eval {
		## Perl incorrectly thinks the $Config::dba is a potential typo, so quiet the warning
		no warnings 'once';
		##
		
		$Config::dba->process_oneline_sql( "UPDATE USERS SET password = ? where id = ? ", [ $password, $userid ] );
		$vars = { success => 1 };
	};
	if ( $@ ) {
		$vars = { error => $@ };

	}
	my $json = encode_json ( $vars );
	print $json;
}

##############################################################################
#       validateUser
#
##############################################################################
sub validateUser {
	my ( $fname, $lname, $username, $password, $email, $update, %error );
	
	$fname		 = $_[0];
	$lname		 = $_[1];
	$username	 = $_[2];
	$password	 = $_[3]; 
	$email		 = $_[4];
	$update	 = $_[5];

	$error{ oktogo } = 1;

	#validate params
	if ( length( $fname ) < 2 ) {
		$error{ fname } = "First name must be longer than 2 characters.";
		$error{ oktogo } = 0;
	}
	if ( length( $lname ) < 2 ) {
		$error{ lname } = "Last name must be longer than 2 characters.";
		$error{ oktogo } = 0;
	}
	if ( $update ne "yes" ) {		 # don't validate username if its an update
		if ( length( $username ) < 4 || length( $username ) > 25) {
			$error{ username } = "Username must be between 4 and 25 characters.";
			$error{ oktogo } = 0;
		}
		if ( ECMDBUser::userExists( $username ) ) {
			$error{ username } = "This username is taken.";
			$error{ oktogo } = 0;
		}
	}
	if ( length( $password ) < 4 || length( $password ) > 16) {
		$error{ password } = "Password must be between 4 and 16 characters.";
		$error{ oktogo } = 0;
	}
	if ( ! ($email =~ /^(\w|\-|\_|\.)+\@((\w|\-|\_)+\.)+[a-zA-Z]{2,}$/)) {
		$error{ email } = "Email address is not valid.";
		$error{ oktogo } = 0;
	}
	return %error;
}


##############################################################################
#       AddUsersToOpco
#
##############################################################################
sub AddUsersToOpco {
	my ( $query, $session ) = @_;

	my $opcoid	= $query->param( "opcoid" );
	my $users	= $query->param( "users" );
	my $formPosted	= $query->param( "formPosted" ) || "no";
	my $sidemenu	= $query->param( "sidemenu" );
	my $sidesubmenu	= $query->param( "sidesubmenu" );
	my $roleid	= $session->param( 'roleid' );

	my $vars;

	eval {
		Validate::opcoid( $opcoid ) if ( $opcoid ne "" );

		if ( $formPosted eq "yes" && $opcoid != 0 ) {

			die "users list is undef" unless defined $users;
			my @usersArray = split /,/, $users;

			for my $user ( @usersArray ) {
				next unless ECMDBUser::userExistsById( $user );

				if ( ! ECMDBUser::isUserAssignedToOpco( $user, $opcoid ) ) {
					#add this operating company to this user
					ECMDBUser::addUserOpco( $user, $opcoid );
				}
			}
		}


		# get the operating companies.
		my @opcos = ECMDBOpcos::getOperatingCompanies( { all => 1 } );
		$vars = {
			opcos		=> \@opcos,
			opcoid		=> $opcoid,
			users		=> $users,
			formPosted	=> $formPosted,
			sidemenu	=> $sidemenu,
			sidesubmenu	=> $sidesubmenu,
			roleid		=> $roleid,
		 };
	};
	if ( $@ ) {
		my @opcos = ECMDBOpcos::getOperatingCompanies( { all => 1 } );
		my $invalidOpcoID	= undef;
		my $invalidUserID	= undef;

		if ( $opcoid == 0 ) {
			$invalidOpcoID	= $opcoid;
		} else {
			$invalidUserID	= $users;
		}

		$vars = { 	opcos       	=> \@opcos,
					error 			=> $@ ,
					invalidUserID	=> $invalidUserID,
					invalidOpcoID	=> $invalidOpcoID,
		};	
	}

	HTMLHelper::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/addUsersToOpco.html', $vars ) || die $tt->error(), "\n";
}

##############################################################################
#       EditSettings - Handle 'Account Settings'
#
##############################################################################
sub EditSettings {
	my ( $query, $session ) = @_;

	my $sidemenu		= $query->param( "sidemenu" );
	my $sidesubmenu		= $query->param( "sidesubmenu" );
	my $updatenotify 	= $query->param( "updatenotify" ) || 0;
	my $mode			= $query->param( "mode" ) || "";

	my $userid			= $session->param( SessionConstants::SESS_USERID );
	my $currentopcoid	= $session->param( SessionConstants::SESS_OPCOID );
	my $currentroleid	= $session->param( SessionConstants::SESS_ROLEID );
	my $roleid			= $currentroleid;

	my $updopcoid		= $currentopcoid;
	$updopcoid			= $query->param( 'opcoid' ) if ( $updatenotify == 1 );

	my @useropcos		= ECMDBOpcos::db_getUserOpcos( $userid );
	my @useropcoroles	= ECMDBUser::db_getUserOpcoRoles( $userid, $currentopcoid );

	# PD-2451 Getwholeusernotification only uses 1st arg ie userid
	my ( $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $teams, $message, $email , $advisory) = ECMDBUserNotification::Getwholeusernotification( $userid );

	# Needed for Cancel function in JQuery dialog

	my $auto_refresh 		= ECMDBUser::db_get_autorefresh_duration( $userid ); 
	my $get_messagedays 	= ECMDBUser::db_getMessagedays( $userid );
	my $status_note_flag 	= ECMDBUser::db_getStatusNoteFlag( $userid );

	my $vars = {
		userid		=> $userid,
		fname		=> $session->param( SessionConstants::SESS_FNAME ),
		lname		=> $session->param( SessionConstants::SESS_LNAME ),
		useropcos	=> \@useropcos,
		currentopcoid	=> $currentopcoid,
		currentroleid	=> $currentroleid,
		useropcoroles	=> \@useropcoroles,
		sidemenu	=> $sidemenu,
		sidesubmenu	=> $sidesubmenu,
		roleid		=> $roleid,
		applicationurl	=> $applicationurl,
		assignjob	=> $assignjob,
		qcrequired	=> $qcrequired,
		qcapproved	=> $qcapproved,
		qcreject	=> $qcreject,
		jobreject	=> $jobreject,
		dispatched	=> $dispatched,
		teams		=> $teams,
		message		=> $message,
		email		=> $email,
		advisory	=> $advisory,
		auto_refresh	=> $auto_refresh,
		view_messages	=> $view_messages,
		get_messagedays => $get_messagedays,
		status_note_flag 	=> $status_note_flag
	 };

	if ( $updatenotify == 1) {
		$vars = {
			assignjob   => $assignjob, 
			qcrequired  => $qcrequired,
			qcapproved  => $qcapproved,
			qcreject    => $qcreject,
			jobreject   => $jobreject, 
			dispatched  => $dispatched,
			teams	    => $teams,
			message     => $message,
			email       => $email,
		};	
	}

	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

	HTMLHelper::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/editSettings.html', $vars ) || die $tt->error(), "\n";
}

sub Deleteuser {
	my ( $query, $session ) = @_;

	# TODO check user permissions

	my $userid	= $query->param( "id" );
	my $mode	= $query->param( "mode" ) || "";

	my $vars;
	eval {	
		Validate::userid( $userid );
		ECMDBUser::deleteUser( $userid );
		$vars = { success => 1 };
	};
	if ( $@ ) {
		$vars = { error => $@ };
	}
	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

# Local copy of getMediaTypes (from Checklist.pl) as we dont want to drag in databaseProducts as well.

#############################################################################
##       getMediatypes
##      
###############################################################################
sub getMediatypes {

	my @media;
	eval {
		my $rows = $Config::dba->hashed_process_sql( "SELECT id, type FROM MEDIATYPES" );
		if ( $rows ) {
			foreach my $r ( @$rows ) {
				push ( @media, { mediaid => $r->{id}, medianame => $r->{type} }  )
			}
		}
	};
	if ( $@ ) {
		confess "getMediatypes, error $@";
	}
	return @media;
}

sub dashboardEditResource {

	my ( $query, $session ) 	= @_;

	my $vars							= undef;
	my $workHrsEnds						= undef;
	my $workHrsStarts					= undef;
    my $costcode                        = undef;
	my $adjustedcontractualHrsEnds		= undef;
	my $adjResourceContractualHrsStarts	= undef;
	my $mode							= $query->param( "mode" );
	my $type							= $query->param( "type" );
	my $userID							= $query->param( "userID" );
	my $selectedDate					= $query->param( "selectedDate" );
	my $useNname    					= ECMDBUser::db_getUsername($userID);
	
	eval {
		if ( $type =~ /^GET$/i ) {
			( $workHrsStarts, $workHrsEnds , $costcode, $adjResourceContractualHrsStarts, $adjustedcontractualHrsEnds  )  = ECMDBUser::db_getResourceContractualHours( $userID, $selectedDate );			
			$vars	= { userName => $useNname, workHrsStarts => $workHrsStarts, workHrsEnds => $workHrsEnds, CostCode=>$costcode, adjustedContractualHrsStarts => $adjResourceContractualHrsStarts, adjustedcontractualHrsEnds => $adjustedcontractualHrsEnds };
		} elsif ( $type =~ /^UPDATE$/i ) {

			my $adjResourceContractualHrsStart	= $query->param( "adjResourceContractualHrsStart" );
			my $adjResourceContractualHrsEnds	= $query->param( "adjResourceContractualHrsEnds" );
			my $succRet  = ECMDBUser::db_editResourceContractualHours( $userID, $adjResourceContractualHrsStart, $adjResourceContractualHrsEnds );

			if ( $succRet == 1 ) {
				$vars   = { successFlag => "success"};
			}
		}
	}; 
	
	if ( $@ ) {
		$vars = { error => $@ };
	}

	if ( $mode eq "json" ) {
		my $json 	= encode_json( $vars );
		print $json;
		return $json;
	}
}

1;

