#!/usr/local/bin/perl
package Upload;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use File::Path;
use File::Copy;
use LWP::Simple;
use JSON qw( decode_json );
use Net::FTP::File;
use URI::Escape;
use URI::URL;

use Data::UUID;
use XML::Simple;
use Data::Dumper;
use Date::Simple ('date', 'today');
use JSON;
use Time::Local;
use Encode qw(decode encode);
use Cwd;
use Carp qw/carp/;
use CGI;
use IO::Handle;
# Home grown
use lib 'modules';

use CTSConstants;
use Validate;

require "config.pl";
require "jobpath.pl";
require "twist.pl";
require "imageworkflow.pl";

require "databaseOpcos.pl";
require "databaseJob.pl";
require "databaseAudit.pl";
require "streamhelper.pl";
require 'databaseProducts.pl';

use constant DEFAULT_UPLOAD_DIR => '/temp/';

my %preflightstatuses = (
	InProgress 		=> '1',
	Passed 			=> '2',
	Warning			=> '3',
	Failed			=> '4',
	None			=> '5',
	Dispatched		=> '6',
);

sub assert_can_see_customer {
	my ( $userid, $job_number, $customerid, $jobopcoid, $location ) = @_;

	my ( $canview ) = $Config::dba->process_oneline_sql( 
						"SELECT 1 FROM customeruser WHERE userid=? AND customerid = ?", 
						[ $userid, $customerid ] );
	if ( ( ! $canview ) && ( defined $location ) && ( $location != $jobopcoid ) ) {
		my ( $customer_name ) = $Config::dba->process_oneline_sql( 
								"SELECT customer FROM customers WHERE id = ?", [ $customerid ] );

		# opcos 13, 14, 15, 16 and 17 are specials, 17 is Multiple Locations
		if ( ( $location >= 13 ) && ( $location <= 16 ) ) {
		} elsif ( $location == 17 ) {
			my ( $canview ) = $Config::dba->process_oneline_sql( 
								"SELECT COUNT( c.id ) FROM customers c " .
								"INNER JOIN customeruser cu ON cu.customerid = c.id " .
								"INNER JOIN shards s ON s.location=c.opcoid " .
								"WHERE (cu.userid = ?) AND (c.customer = ?) AND (s.job_number = ?)",
								[ $userid, $customer_name, $job_number ] );
		} else {
			my ( $canview ) = $Config::dba->process_oneline_sql( 
								"SELECT COUNT( c.id ) FROM customers c " .
								"INNER JOIN customeruser cu ON cu.customerid = c.id " .
								"WHERE (cu.userid = ?) AND (c.customer = ?) AND (c.opcoid = ?)",
								[ $userid, $customer_name, $location ] );
		}
	}
	die "You do not have access to the customer for this job" unless ( $canview );
}

# Action/subaction handler

sub upload {
	my ( $query, $session ) = @_;
	my $vars;
	my $subaction = $query->param('subaction') || 'undef';
	my $userid = $session->param( 'userid' );

	eval {
		if ( $subaction eq "init" ) {
			$vars = upload_init( $query, $session ); # Initial queries for dialog

		} elsif ( $subaction eq "upload" ) {
			$vars = upload_upload( $userid, $query );

		} else {
			die "Unexpect upload subaction '$subaction'";
		}
	};
	if ( $@ ) {
		my $error = $@;
		carp "upload: $error";
		$error =~ s/ at .*//gs;
		$vars = { error => $error };
	}

	my $json = encode_json ( $vars );
	print $json;
	return $json;
}

# can_upload uses the role and current job state to decide if the user can upload
# (so past the basic check for roleid in [ 1, 4, 6 ] and user can view jobs customer.
#
# Administrators can upload any time
#
# Operators (image workflow camera operator), can upload any time
# Operators (normal), can upload unless the job is assigned but not worked on and not (isfromproduct and workedonproducts)
#    guess that means the operator must be assigned to the job or to a product in the job.
#    so that means they can upload to a job that is not assigned ??
#
# Studio managers can upload unless the job is assigned.
sub can_upload {
	my ( $roleid, $imageworkflow, $isjobassign, $workedon, $isfromproduct, $workedonproducts, $warnlist ) = @_;
	my $can_user_upload = 0; # by default the user can't upload
	my $canupload;

	if ( $roleid == 1 ) {  # Operator, assume they can by default
		$canupload = 1;
		# If I'm an operator - but not an image workflow camera operator
		if ( $imageworkflow != 1 )  { 
			if ( ($isjobassign == 1) && ($workedon != 1) ) {
				$canupload = 0 unless ( ($isfromproduct == 1) && ($workedonproducts == 1) );
			}
		}
	} elsif ( $roleid == 4 ) {  # Admin can always
		$canupload = 1;
	} elsif ( $roleid == 6 ) {  # Studio Manager
		if ( $isjobassign == 1 ) {
			push( @$warnlist, "Cant upload as job is assigned" );
		} else {
			$canupload = 1;
		}
	} else {  # All other roles (shouldn't get this far, so this is the back stop)
		push( @$warnlist, "Can't upload as role not permitted to" );
	}

	return $canupload;
}

sub requestJobCheckIn {
	my ( $jobid ) = @_;

	my $fromopcoid = ECMDBJob::db_getJobRemoteOpcoid ( $jobid );
	my $domainname = ECMDBOpcos::db_getDomainName( $fromopcoid );

	#  job in - CGI call
	my ( $var, $curlCommand ) = JF_WSAPI::jobflowCheckinOther( $domainname, { jobid => $jobid } );
}

# Handle the creative, colour manage and optimise checkboxes
# In operatingcompany, we have creative, colourmanagedef, colourmanage_isactivedef, optimisedef, optimise_asactivedef
# and in tracker3plus, we have creative, colourmanage, colourmanage_isactive, optimise, optimise_isactive
# in upload_init, most of the settings come from the call to db_getJob, ie colourmanage, colourmanage_isactivedef, optimise, optimise_isactivedef, creative,
# and only creativeenabled comes from operatingcompany.creative

sub handle_creative_colourmanage_optimise {
	my ( $query, $userid, $jobid, $jobopcoid ) = @_;

	# creative is a simple checked/unchecked status, it is enabled if creativeenabled == 1.
	my $creative		= $query->param( "creative" ) ||'';
	$creative			= $creative eq "on" ? 1 : 0;	#convert 'on' to 1

	# colourmanage and optimise have 2 settings, so colourmanage enables/disables the setting, whilst colourmanage_isactive records whether it is set or not

	my $colourmanage_isactive = $query->param("colourmanage_isactive" ) ||'';
	my $optimise_isactive = $query->param("optimise_isactive") ||'';

	$colourmanage_isactive = ( $colourmanage_isactive eq "on" ? 1 : 0 );
	$optimise_isactive	= ( $optimise_isactive eq "on" ? 1 : 0 );

	eval {
		my @changes; # Changes to be logged in audit log, try to combine multiple chnages

		my $creativeenabled = ECMDBOpcos::db_creativeenabled( $jobopcoid ); # SELECT creative FROM OPERATINGCOMPANY WHERE opcoid = ?
	
		# query to get current values from database, for reference
		my ( $c_creative, $c_colourmanage, $c_colourmanage_isactive, $c_optimise, $c_optimise_isactive ) = $Config::dba->process_oneline_sql( 
				"SELECT creative, colourmanage, colourmanage_isactive, optimise, optimise_isactive FROM tracker3plus WHERE job_number = ?", [ $jobid ] );
		die "Failed reading current creative/colour settings" unless ( defined $c_creative ); # Fields are NOT NULL

		# if the 'creative' option is enabled we need to save this additional data

		if ( $creativeenabled && ( $creative != $c_creative ) ) {
			ECMDBJob::db_setCreative( $jobid, $creative );  # UPDATE TRACKER3PLUS SET creative=? WHERE job_number=?
			if ( $creative == 1 ) {
				push( @changes, "Creative = 'ON'" );
			} else {
				push( @changes, "Creative = 'OFF'" );
			}
		}

		if ( ( $c_colourmanage == 1 ) && ( $c_colourmanage_isactive != $colourmanage_isactive ) ) {
			ECMDBJob::db_setActiveFlag( $jobid, "COLOURMANAGE", $colourmanage_isactive ); # UPDATE TRACKER3PLUS SET $columnname" . "_isactive=? WHERE job_number=?
			ECMDBJob::db_recordJobAction( $jobid, $userid, CTSConstants::COLOUR_MANAGE );
			if ( $colourmanage_isactive == 1 ) {
				push( @changes, "Colour Manager = 'ON'" );
			} else {
				push( @changes, "Colour Manager = 'OFF'" );
			}
		}

		if( ( $c_optimise == 1 ) && ( $c_optimise_isactive != $optimise_isactive ) ) {
			ECMDBJob::db_setActiveFlag( $jobid, "OPTIMISE", $optimise_isactive );
			if ( $optimise_isactive == 1 ) {
				push( @changes, "Optimise = 'ON'" );
			} else {
				push( @changes, "Optimise = 'OFF'" );
			}
		}
		if ( scalar @changes ) {
			ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Job: $jobid was saved with " . join( ', ', @changes ) );
		}
	};
	if ( $@ ) {
		my $error = $@;
		carp "handle_creative_colourmanage_optimise: $error";
		$error =~ s/ at .*//gs;
		die $error;
	}
}

##############################################################################
#       updateJobStatuses
#
##############################################################################
sub updateJobStatuses {
	my ( $jobid, $userid ) = @_;

	#ka - todo: - set assetscreated = 0 / we reset this flag each time a user uploads a new file
	ECMDBJob::db_updateAssetsCreated ( $jobid, 0 ); 
	#on each new upload we reset the compare
	ECMDBJob::db_updateCompare ( $jobid, 0 ); 

	# we lock the job as whenever a user uploads a file - lets Twist again!
	ECMDBJob::db_lockJob ( $jobid, 1 );
	
	# touch the job with this user
	ECMDBJob::db_touchJob ( $jobid, $userid );
	ECMDBAudit::updateHistory( $userid, $jobid );

	# file uploaded so we set job to 'uploaded'
	# change preflight status if uploaded OK
	ECMDBJob::db_updateJobPreflightStatus( $jobid, $preflightstatuses{ InProgress } );

	# mark the OPERATOR STATUS AS 'P' (Processing) in both T3P and AssetHistory
	ECMDBJob::setOperatorJobProcessing( $userid, $jobid );

	# To update the rejected qc status when job is reprocessing
	my $qcstatus = ECMDBJob::db_getQCStatus( $jobid );
	if ( $qcstatus == 3 ) { # QC Rejected
   		ECMDBJob::db_updateJob( $jobid, { qcstatus => 0 } ); # QC status 0 is None 
   		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "QC status changed from Rejected to None (after upload)"  );
	}
}

# upload_init - Called when user selects Action -> Upload

sub upload_init {
	my ( $query, $session ) = @_;

	my $jobid		= $query->param( 'jobid' );
	my $isfromproduct = $query->param( 'isfromproduct' ) || 0; # set in js/taskprocess.js, openUploadDialog

	my $roleid		= $session->param( 'roleid' );
	my $userid		= $session->param( 'userid' );

	my $http_upload_url = Config::getConfig( 'http_upload_url' ) || 'jobflow.pl';
	my $ttp_upload_url	= Config::getConfig( 'ttp_upload_url' ) || 'jobflow.pl';
	my $local_upload_url = Config::getConfig( 'local_upload_url' ) || 'jobflow.pl';

	my $vars;
	my @history;
	my @warnlist;

	eval {
		Validate::job_number( $jobid );

		my ( $jobPath, $jobopcoid, $customerid, $location ) = JobPath::getJobPath( $jobid ); # jobPath doesnt have trailing /
		
		die "Job $jobid has null jobopcoid" unless  ( defined $jobopcoid );
		push( @history, "Job $jobid, opco $jobopcoid, cust $customerid, location $location" );
		# when a job is at task level the action -> upload should show below warning message
		if(!$isfromproduct) {
			my ($level)	=	ECMDBProducts::checkJobLevel($jobid);	#if level is 2, the job is working by task level
			die "Please use the assigned task to Upload." if($level == 2);	
		}

        $customerid = ECMDBOpcos::getCustomerId2($jobid); ##The customer id in getJobPath is belongs to customer id which is get assign when job is created. In this case we actually need to the customer id of users (current location + customer name)

		assert_can_see_customer( $userid, $jobid, $customerid, $jobopcoid, $location );

		# $isTTPUpload, $isLocalUpload, $creative, $isjobassign depend on the jobs location NOT the home/origin opcoid

		my ( $isTTPUpload, $isLocalUpload, $creativeenabled, $isjobassign ) = $Config::dba->process_oneline_sql( 
			"SELECT ttpupload, enablelocalupload, creative, jobassignment  FROM operatingcompany WHERE opcoid=?", [ $location ] );

		push( @history, "isTTPUpload $isTTPUpload, isLocalUpload $isLocalUpload, creativeEnable $creativeenabled, job assigned $isjobassign" );

		# Check FTP details for opco
		my ( $ftpserver, $ftpusername, $ftppassword ) = ECMDBOpcos::db_getFTPDetails( $jobopcoid );
		if ( ! $ftpserver ) {
			push( @warnlist, "Invalid FTP server for opco $jobopcoid" );
			$isLocalUpload = 0;
		}
 
		# Check localuploadsource/dest as these are used by LocalUpload
		if ( $isLocalUpload ) {
			my ( $localsource_dir, $localdest_dir ) = $Config::dba->process_oneline_sql( 
					"SELECT localuploadsource, localuploaddestination FROM operatingcompany WHERE opcoid=?", [ $location ] );
			if ( ! $localsource_dir ) {
				push( @warnlist, "Local source directory invalid for opco $location" );
				$isLocalUpload = 0;
			} elsif ( ! $localdest_dir ) {
				push( @warnlist, "Local destination directory invalid for opco $location" );
				$isLocalUpload = 0;
			}
		}

		my $imageworkflow	= ECMDBOpcos::db_isOpcoImageWorkFlow( $jobopcoid ); # SELECT COUNT(1) FROM imageworkflow WHERE opcoid = ?

		# push( @warnlist, "Job uses Imageworkflow" ) if $imageworkflow;

		my $workedon		= ECMDBJob::isJobBeingWorkedOn( $jobid );
		my $workedonproducts = ECMDBJob::isJobBeingWorkedOnForProdcuts( $jobid );
		# system("echo \"roleid: $roleid, imageworkflow: $imageworkflow, isjobassign: $isjobassign, workedon:$workedon\"> httplog");
		my $canoperatorupload   = can_upload( $roleid, $imageworkflow, $isjobassign, $workedon, $isfromproduct, $workedonproducts, \@warnlist );

		push( @history, "IWF: $imageworkflow, workedon $workedon, workedonproducts $workedonproducts, canupload $canoperatorupload" );

		# From job, the dialog uses
		# job_number, colourmanage, colourmanage_isactivedef, optimise, optimise_isactivedef, creative, preflightstatus, pathtype, locked

		my @job	= ECMDBJob::db_getJob( $jobid );

		# Check preflight status etc
		my $j = $job[ 0 ];
		push( @history, "Preflight status " . $j->{preflightstatus} . ", pathtype " . $j->{pathtype} . ", locked " . $j->{locked} );
		if ( $j->{preflightstatus} eq "In Progress" ) {
			push( @warnlist, "Preflight status is '" . $j->{preflightstatus} . "'" );
		}
		if ( $j->{pathtype} == 10 ) { 
			push( @warnlist, "Path type is 10" );
		}
		if ( $j->{locked} == 1 ) {
			push( @warnlist, "Job is locked" );
		}
	
		# For HTTP upload it depends on imageworkflow and agencyrefasfilename
		my $customer        = ECMDBJob::db_getCustomer( $jobid ); # SELECT agency FROM TRACKER3 WHERE job_number=?
		my $agencyrefasfilename	= ECMDBOpcos::agencyrefasfilename( $jobopcoid, $customer ); # SELECT useagencyrefasfilename FROM CUSTOMERS WHERE id = ?
		my $agencyref		= ECMDBJob::getAgencyRef( $jobid ); # SELECT agencyref FROM TRACKER3 WHERE job_number=?
		push( @history, "customer $customer, agencyrefasfilename $agencyrefasfilename, agencyref $agencyref" ); # agencyref can be NULL

		if ( $canoperatorupload ) {

				# Provide user with help at to the required filename
				# push( @warnlist, "Local Upload expects '$jobid.pdf'" ) if $isLocalUpload;

				if ( $imageworkflow ) {
					my @extList = Imageworkflow::getSupportedFileTypes();
					# push( @warnlist, "HTTP Upload (Image Workflow) expects file with name '$agencyref' and extension one of '" . join( ',', @extList ) . "'" );
			
				} elsif ( $agencyrefasfilename ) {
					# push( @warnlist, "HTTP Upload (archant) expects '$agencyref.pdf'" )
			
				} else {
					# push( @warnlist, "HTTP Upload expects '$jobid.pdf'" )
				}
		}

		# create rnd_no
		my $rnd_no = $session->param( 'rnd_no' ) || '';
		my $sessionid = $session->id;
	
		$vars = {
			myjob			=> \@job,
			creativeenabled => $creativeenabled,
			canoperatorupload => $canoperatorupload,
			isLocalUpload	=> $isLocalUpload,
			isTTPUpload 	=> $isTTPUpload,
			location		=> $location,
			roleid			=> $roleid,
			jobopcoid		=> $jobopcoid,
			history			=> \@history,
			warnings		=> \@warnlist,
			http_upload_url => $http_upload_url,
			ttp_upload_url => $ttp_upload_url,
			local_upload_url => $local_upload_url,
			rnd_no => $rnd_no,
			sessionid => $sessionid,
		}

	};
	if ( $@ ) {
		my $error = $@;
		carp "upload_init: $error";
		push( @history, "upload_init: $error" );
		$error =~ s/ at .*//gs;
		$vars = { error => $error };
	}

	return $vars;	
}

# upload_upload - called when user clicks on Upload button on Upload dialogbox

# query params
# - jobid
# - uploadviattp, httpupload, localupload, creative, colourmanage_isactive, optimise_isactive
#      (all checkboxes, so "on" or undef)
# - uploadfile - name of file uploaded, should generally match job number
#      (exception being if imageworkflow)
# and other params taken as is
# - action=upload, subaction=upload, mode=json

sub upload_upload {
	my ( $userid, $query ) = @_;
	my $jobid 			= $query->param( "jobid" );

	my $localupload		= $query->param( "localupload" ) ||'';
	my $httpupload		= $query->param( "httpupload" ) ||'';
	my $uploadviattp	= $query->param( "uploadviattp" ) || '';

	$localupload		= ( $localupload eq "on" ) ? 1 : 0;
	$httpupload			= ( $httpupload eq "on" ) ? 1 : 0;
	$uploadviattp		= ( $uploadviattp eq "on" ) ? 1 : 0;

	my $isfromproduct	= $query->param( "isfromproduct" ) || 0; # from js/taskprocess.js 
	my $qcstatus		= $query->param( "qcstatus" ) || 0;
	my $vars;
	my @history;		# Pass commentary back to user
	my @warnlist;		# just to satisfy can_upload 

	eval {

		# Basic validation

		# Check that just one of the 3 possible upload methods is specified
		die "More than one upload method specified" if ( ( $localupload + $httpupload + $uploadviattp ) > 1 );
		die "No upload method specified" if ( ( $localupload + $httpupload + $uploadviattp ) == 0 );

		# Get job details, check job valid and user can view customer

		Validate::job_number( $jobid );

		my ( $jobPath, $jobopcoid, $customerid, $joblocation ) = JobPath::getJobPath( $jobid ); # jobPath doesnt have trailing /

		die "Job $jobid has null jobopcoid" unless  ( defined $jobopcoid );
		die "Job $jobid has null location" unless ( defined $joblocation );

        $customerid = ECMDBOpcos::getCustomerId2($jobid); ##The customer id in getJobPath is belongs to customer id which is get assign when job is created. In this case we actually need to the customer id of users (current location + customer name)

		assert_can_see_customer( $userid, $jobid, $customerid, $jobopcoid, $joblocation );

		die "Invalid QC status" if ( ( defined $qcstatus ) && ! looks_like_number( $qcstatus ) );

		my ( $isTTPUpload, $isLocalUpload, $creativeenabled, $isjobassign, $isqcenabled ) = $Config::dba->process_oneline_sql( 
			"SELECT ttpupload, enablelocalupload, creative, jobassignment, qcenabled  FROM operatingcompany WHERE opcoid=?", [ $joblocation ] );
	
		die "TTP upload not allowed for this job" if ( $uploadviattp && ! $isTTPUpload );
		die "Local upload not allowed for this job" if ( $localupload && ! $isLocalUpload );

		my $imageworkflow	= ECMDBOpcos::db_isOpcoImageWorkFlow( $joblocation );

		my $workedon		= ECMDBJob::isJobBeingWorkedOn( $jobid );
		my $workedonproducts = ECMDBJob::isJobBeingWorkedOnForProdcuts( $jobid );

		# Do the specific processing
		# Each of the following functions does validation of the supplied args
		# and should be using db_auditLog to provide a commentary and a call to db_recordJobAction

		if ( $localupload == 1 ) {
			push( @history, "Into Local Upload, job $jobid, location $joblocation, custid $customerid" );
			doLocalUpload( $query, $userid, $jobid, $joblocation, $customerid );

		} elsif ( $httpupload == 1 ) {
			push( @history, "Into HTTP Upload, job $jobid, location $joblocation, custid $customerid, iwf $imageworkflow" );

			##HTTP ftp path has to be taken from remote opcoid. It is taking from location path which is wrong as per steve L. So i am passing 
			##remote opcoid too to take it remote path
			http_upload( $query, $userid, $jobid, $joblocation, $customerid, $imageworkflow, \@history, $jobopcoid );
	
		} elsif ( $uploadviattp == 1 ) {
			push( @history, "Into TTP Upload, job $jobid, location $joblocation, custid $customerid" );
		 	my $uploadres = ttp_upload( $query, $userid, $jobid, $joblocation, $customerid );
		 	if($uploadres =~ /File not found/){
				die "File not found in source location.";
			}

		} else {
			die "No upload method specified";
		}
		push( @history, "Done specific Upload" );

		# Handle changes in Creative, Colour Manage and Optimise settings
		handle_creative_colourmanage_optimise( $query, $userid, $jobid, $joblocation );

		# Update Job status
		if ( $uploadviattp || $localupload ) {
			updateJobStatuses( $jobid, $userid );
		}
		if ( $httpupload ) {
			if ( $imageworkflow == 1 ) {
				# TODO maybe this should just call updateJobStatuses which also sets assetscreated=0, compare=0, preflightstatus=inprogress and locks/touches the job 
				ECMDBAudit::updateHistory( $userid, $jobid );  # INSERT INTO uploadhistory ...
				ECMDBJob::setOperatorJobProcessing( $userid, $jobid );
	
			} else {
				updateJobStatuses( $jobid, $userid );
			}
		}

		# TODO reset the Adgate status - is this needed ?
		ECMDBJob::db_updateAdgateStatus( $jobid, "" );

		# 
		$vars = {
			history => \@history,
		};

	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => $error, jobid => $jobid||'', history => \@history };
		carp "upload_upload: error, $error";
	}

	return $vars;
}

# Code lifted from Fileupload (originally by anbu)

sub doLocalUpload {
	my ( $query, $userid, $jobid, $jobopcoid, $customerid ) = @_;

	my $file = $jobid . '.pdf';

	my $ftp;
	eval {

		# No need to validate the filename supplied, the user shouldnt need to choose one
		# but they could select HTTP upload, then select a file, then select Local Upload
		# but we ignore the payload if doing Local upload.
 
		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Local Upload started, file $file" );

		# Both LOCALUPLOADSOURCE and LOCALUPLOADDESTINATION can be blank for some opcos
		my $localsource_dir = ECMDBOpcos::db_getLocalUploadSourceDirectory( $jobopcoid ); # SELECT LOCALUPLOADSOURCE FROM OPERATINGCOMPANY WHERE opcoid=?"
		my $localdest_dir   = ECMDBOpcos::db_getLocalUploadDestDirectory( $jobopcoid ); # SELECT LOCALUPLOADDESTINATION FROM OPERATINGCOMPANY WHERE opcoid=?
		die "Local source dir invalid" unless ( $localsource_dir );
		die "Local destination dir invalid" unless ( $localdest_dir );

		#get the ftp details
		my ( $ftpserver, $ftpusername, $ftppassword ) = ECMDBOpcos::db_getFTPDetails( $jobopcoid );
		die "Invalid FTP server for opcoid $jobopcoid" unless ( $ftpserver );

		$ftp = Net::FTP->new($ftpserver, Debug => 0) or die "FTP connection failed, $ftpserver";
		$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login to $ftpserver, opcoid $jobopcoid ", $ftp->message;

		$ftp->binary();
		$ftp->cwd( $localdest_dir ) or die "Failed to change to $localdest_dir, " . $ftp->message;
		# Test for file existance, or else delete will always fail on new uploads...
		my $source_size = $ftp->size( "$localsource_dir/$file" );
		die "Source file $localsource_dir/$file does not exist" unless defined $source_size;
		die "Source file $localsource_dir/$file is zero length" unless $source_size > 0;

		if ( defined $ftp->size("$localdest_dir/$file") ) 	{
		   $ftp->delete( "$localdest_dir/$file" ) or die "Failed to delete existing $localdest_dir/$file, " . $ftp->message; 
		}
		$ftp->move( "$localsource_dir/$file", "$localdest_dir/$file" ) or die "Failed to move $file, " . $ftp->message;
		my $message = $ftp->message;
		my $code = $ftp->code();
		die "Failed to move file '$localsource_dir/$file' to '$localdest_dir/$file', code $code, message $message" if ( $code != 250 );
		$ftp->quit();
		$ftp = undef;

		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Local Upload completed, file $file" );
		ECMDBJob::db_recordJobAction( $jobid, $userid, CTSConstants::LOCAL_UPLOAD );
	};
	if ( $@ ) {
		my $error = $@;
		carp "doLocalUpload: $error";
		$error =~ s/ at .*//gs;
		$ftp->quit() if ( $ftp );
		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "Local Upload failed: $error" );
		die $error;
	}
}

# Handle HTTP upload
sub http_upload {
	my ( $query, $userid, $jobid, $jobopcoid, $customerid, $imageworkflow, $history, $jobremoteid ) = @_;

	my $myfile 	= $query->param( "uploadfile" ) ||'';	#filename 
	my $upload_dir = Config::getConfig("uploaddirectory") || DEFAULT_UPLOAD_DIR;

	my $t_start;
	my $t_elapsed;

	eval {
			use CGI;

			# Validate supplied filename and extension

			die "No filename provided" unless ( $myfile );

			# TODO why not use "SELECT customer FROM customers WHERE id = ?" $customerid
			my $customer		= ECMDBJob::db_getCustomer( $jobid ); # SELECT agency FROM TRACKER3 WHERE job_number=?

			# do we use the agency reference as the filename (or just the jobnumber)?
			my $agencyrefasfilename	= ECMDBOpcos::agencyrefasfilename( $jobopcoid, $customer ); # SELECT useagencyrefasfilename FROM CUSTOMERS WHERE id = ?
			my $agencyref		= ECMDBJob::getAgencyRef( $jobid ); # SELECT agencyref FROM TRACKER3 WHERE job_number=?
			push( @$history, "http_upload: customer $customer, agencyrefasfilename $agencyrefasfilename, agencyref $agencyref" );
	
			my $requiredname	= "$jobid.pdf";  # Default for HTTP upload

			if ( $imageworkflow == 1 ) {

				die "Agency ref is undefined" if ( ( ! defined $agencyref ) || ( $agencyref eq '' ) );
				my $ext = ($myfile =~ m/([^.]+)$/)[0];
				my $fileValid = Imageworkflow::isSupportedFileType( $ext );

				die "Filetype (ext '$ext') not supported for ImageWorkflow" if ( (length( $myfile ) > 0) && ( $fileValid == 0 ));
				$requiredname = $agencyref . '.' . $ext;
				die "Bad filename: $myfile, the filename must be: '$requiredname' for Image Workflow" if ( $myfile ne $requiredname );

				#use Image Workflow tmp folder setting
				$upload_dir = ECMDBImageworkflow::db_getTMPPath( $jobopcoid );

			} elsif ( $agencyrefasfilename == 1 ) { 

				# for archant the filename rules are different
				# we use the agency ref as the 'accepted' filename.pdf

				die "Agency ref is undefined" if ( ( ! defined $agencyref ) || ( $agencyref eq '' ) );
				$requiredname = "$agencyref.pdf";
				die "Bad filename: $myfile, the filename must be: '$requiredname'" if ( (length( $myfile ) > 0) && ( $myfile ne $requiredname ));

			} else {  # Standard HTTP upload

				die "Bad filename: $myfile, the filename must be: $requiredname" if ( (length( $myfile ) > 0) && ( $myfile ne $requiredname ));
			}

			umask( 0 ); # Don't mask any permission set by mkdir or open

			# TODO might want to alter upload_dir to ensure no clash (if 2 people updating...)

			my $upload_filename = "$upload_dir/$myfile";
			if ( ! -e $upload_dir ) {
				mkdir $upload_dir or die "Failed to create dir '$upload_dir', $!";
			} 
			if ( -e "$upload_dir/$myfile" ) {
				unlink "$upload_dir/$myfile" or die "Failed to delete existing file '$upload_dir/$myfile', $!";
			}

			# See http://perldoc.perl.org/CGI.html#PROCESSING-A-FILE-UPLOAD-FIELD

			push( @$history, "Starting HTTP upload, file '$myfile'" );
			ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "HTTP upload, starting, file '$myfile'" );

			$t_start = time();

			# can change directory CGI uses for temp files (but it might stick sessions there too ?)
			# but since CGI changed recently (2014) this no longer works...or maybe I'm using it wrong!

			my $cgiuploadtemp = Config::getConfig('cgiuploadtemp');
			if ( defined $cgiuploadtemp ) {
				if ( ! -e $cgiuploadtemp ) { 
					mkdir $cgiuploadtemp or die "Failed in mkdir $cgiuploadtemp, $!";
				}
			}

			# could limit max size of POST eg
			my $cgimaxuploadsize = Config::getConfig('cgimaxuploadsize' );
			push( @$history, "Limit POST size to $cgimaxuploadsize" ) if ( defined $cgimaxuploadsize );
			push( @$history, "User CGI temp dir $cgiuploadtemp" ) if ( defined $cgiuploadtemp );

			$CGI::POST_MAX=$cgimaxuploadsize if ( defined $cgimaxuploadsize );  # Doesnt seem to work ...

			# $TempFile::TMPDIRECTORY = $cgiuploadtemp if ( defined $cgiuploadtemp ); # even older
			# $CGITempFile::TMPDIRECTORY = $cgiuploadtemp if ( defined $cgiuploadtemp );  # deprecated
			# or maybe
			$ENV{TMPDIR} = $cgiuploadtemp if ( defined $cgiuploadtemp );
			# see https://github.com/projectkudu/kudu/issues/1700 (Sep 2015)

			my $upload_filehandle = $query->upload( "uploadfile" );
			die "Upload error " . $query->cgi_error if ( ! $upload_filehandle && $query->cgi_error );
			die "Upload filehandle not available" unless ( defined $upload_filehandle );

			# Calling query->upload should make CGI create a temporary file containing the contents
			my $temp_file = $query->tmpFileName( $myfile );
			my $temp_size = -s $temp_file;

			# Explicitly check for file too big (although most of the work has been done in getting here)
			die "File exceeds HTTP upload limit, size $temp_size > $cgimaxuploadsize" if ( ( defined $cgimaxuploadsize ) && ( $temp_size > $cgimaxuploadsize ) );

			my $mime_type = $query->uploadInfo($myfile)->{'Content-Type'} ||'';

			push( @$history, "Temp file '$temp_file', size $temp_size, type '$mime_type'" );
	
			# TODO could use IO_Handle to read in blocks

			my $io_handle = $upload_filehandle->handle;
			my $buffer;  # Buffer to hold block of 1024 bytes
			my $bytesread;

			open( UPLOADFILE, ">", $upload_filename ) or die "Failed to open $upload_filename, $!";
			binmode UPLOADFILE; 
			#while ( <$upload_filehandle> ) { 
			while ( $bytesread = $io_handle->read( $buffer, 1024 ) ) {
				#print UPLOADFILE; 
				print UPLOADFILE $buffer;
			} 
			close UPLOADFILE; 
			close $upload_filehandle; # Brians fix 24/5/13
			chmod ( 0777, "$upload_filename" );

			$t_elapsed = time() - $t_start;

			my $actual_size = -s $upload_filename;
			die "File copied size $actual_size, temp file $temp_size" if ( $actual_size != $temp_size );

			push( @$history, "Finished copying temp file to $upload_filename, size $actual_size, elapsed $t_elapsed sec" );
			ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "HTTP upload, read file '$myfile', size $actual_size, type '$mime_type', elapsed $t_elapsed sec" );
	
			# Attempt to delete the CGI temp file (or at least make it zero length)
			unlink $temp_file or die "Failed to deleted $temp_file, $!";  # CGI should delete the temp files (but doesnt on live)
			push( @$history, "Deleted temp file '$temp_file'" );

			# look up the destination drop directory from db (could be different per operating company )
			my $dest_dir = ECMDBOpcos::db_getDestinationDirectory( $jobremoteid ); # SELECT rootpath, preflightin FROM OPERATINGCOMPANY WHERE opcoid=? (returns concat)
			die "Invalid destination '$dest_dir' for opcoid $jobopcoid" unless ( $dest_dir ) && ( $dest_dir =~ /^(\/|\\)/ );
			push( @$history, "Destination dir '$dest_dir'" );

			# Image WorkFlow
			if ( $imageworkflow == 1 ) {

				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "HTTP Upload ImageWorkflow start, $myfile" );

				Imageworkflow::fileUploadImageWorkflowJob(  $query, $jobid, $jobopcoid, $myfile, $upload_dir, $history );

			} else {

				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "HTTP Upload copying $myfile to $dest_dir/$jobid.pdf" );

				moveToPreflight( $jobid, $jobopcoid, $myfile, $upload_dir, $dest_dir, $history );
			}

			# Delete our copy of the file
			if ( -e $upload_filename ) {
				unlink $upload_filename or die "Failed to delete '$upload_filename', $!";
			}

			$t_elapsed = time() - $t_start;  # Total time including copying CGI temp file

			ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "HTTP Upload complete, $myfile, elapsed $t_elapsed secs" );

			ECMDBJob::db_recordJobAction( $jobid, $userid, CTSConstants::HTTP_UPLOAD );
	};
	if ( $@ ) {
		my $error = $@;
		carp "http_upload: $error";
		$error =~ s/ at .*//gs;
		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "HTTP Upload failed, $error" );
		die $error;
	}
}
# Handle TTP upload

sub ttp_upload {
	my ( $query, $userid, $jobid, $jobopcoid, $customerid ) = @_;
	my ( $var, $curlCommand )= "";

	eval {
		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "TTP upload, initialising" );

		# send curl request to remote server to get the file.
		my $domainname = ECMDBOpcos::db_getDomainName( $jobopcoid );
		die "Domain name invalid for opcoid $jobopcoid" unless ( $domainname );
 
	my $sourcepath = ECMDBOpcos::getUploadTempFolder($jobid);
	die "Source path is not available" unless ( $sourcepath );	
	( $var, $curlCommand ) = JF_WSAPI::ttpUploadOther( $domainname, { jobid => $jobid, sourcepath => $sourcepath } );
		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "TTP Upload, request submitted" );
		ECMDBJob::db_recordJobAction( $jobid, $userid, CTSConstants::TTP );
				
	};
	if ( $@ ) {
		my $error = $@;
		carp "ttp_upload: $error";
		$error =~ s/ at .*//gs;
		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobid, "TTP Upload, failed, $error" );
		die $error;
	}
	return $var;
}


# Move file from upload_dir to dest_dir on FTP server associated with jobopcoid
sub moveToPreflight {
	my ( $jobid, $jobopcoid, $myfile, $upload_dir, $dest_dir, $history ) = @_;

	push( @$history, "moveToPreflight, file '$myfile', upload '$upload_dir', dest '$dest_dir'" );

	my $ftp;
	eval {
		my ( $ftpserver, $ftpusername, $ftppassword ) = ECMDBOpcos::db_getFTPDetails( $jobopcoid );
		die "Invalid FTP server for opcoid $jobopcoid" unless ( $ftpserver );

		# move it to PREFLIGHT_INPUT ( via FTP )
		my $twistServerExt = Twist::getTwistServer();	#twist server extension - blank is '1', 2 is '2' 
		$dest_dir = $dest_dir.$twistServerExt;

		if ( $twistServerExt eq "" ) {
			ECMDBJob::db_setTwistServer( $jobid, 1 );
		} else {
			ECMDBJob::db_setTwistServer( $jobid, 2 );			
		}

		push( @$history, "FTP server '$ftpserver'" );

		my @ftpservers = split(',', $ftpserver); # Really ?? as of Aug 2016, no entries have commas in the list
		foreach my $ftpserver ( @ftpservers ) {
			next unless $ftpserver; # Ignore blanks
			$ftp = Net::FTP->new( $ftpserver, Debug => 0, Timeout => 60) or die "FTP connection failed, $ftpserver";
			die "Failed to create FTP instance for '$ftpserver' " unless $ftp;

			$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login to $ftpserver, opcoid $jobopcoid, ", $ftp->message;
			$ftp->binary( );
			$ftp->put( "$upload_dir/$myfile", "$dest_dir/$jobid.pdf" ) or die "failed to copy $upload_dir/$myfile to $dest_dir/$jobid.pdf", $ftp->message;
			$ftp->site("chmod 0777 $dest_dir/$jobid.pdf");
			$ftp->quit();
			$ftp = undef;
			last;
		}
	};
	if ( $@ ) {
		my $error = $@;
		carp "moveToPreflight: Failed, $error";
		$error =~ s/ at .*//gs;
		$ftp->quit() if $ftp;
		die $error;
	}
}

1;

