#!/usr/local/bin/perl
package Support;

use strict;
use warnings;
use Scalar::Util qw( looks_like_number );
use HTML::Entities;

use Template;

use lib 'modules';

require 'config.pl';
require "Mailer.pl";
require "htmlhelper.pl";

# Was originally in general.pl, but as it users users.pl, which drags in opcos.pl
# and ultimately jobs.pl, moved into its own module.

##############################################################################
# from config 
##############################################################################
my $fromemail 		= Config::getConfig("fromemail");
my $supportemail 	= Config::getConfig("supportemail");
my $smtp_gateway        = Config::getConfig("smtp_gateway");

##############################################################################
# the template object 
##############################################################################
my $tt = Template->new({
	RELATIVE => 1,
}) || die "$Template::ERROR\n";


##############################################################################
#       SupportPage ()
#
##############################################################################
sub SupportPage {
	my $query = $_[0];
	my $session = $_[1];

	my ( $message, $subject, $body, $userid, $username, $useremail, $jobnumber );
	my $sidemenu   = $query->param( "sidemenu" );
	my $sidesubmenu= $query->param( "sidesubmenu" );
	my $roleid     = $session->param( 'roleid' );

	my $formPosted = $query->param( 'formPosted' ) ||'';

	$message 	= HTMLHelper::htmlEditFormat( $query->param( 'message' ) || '' );
	$subject 	= HTMLHelper::htmlEditFormat( $query->param( 'subject' ) || '' );
	$jobnumber 	= HTMLHelper::htmlEditFormat( $query->param( 'jobnumber' ) || '' );

	$userid		= $session->param( 'userid' );
	$username	= $session->param( 'username' );
	$useremail	= $session->param( 'email' );

	$body 		= "A support message has been received from user: $username\n";
	$body		.= "Email: $useremail\n\n";
	$body		.= "============= Message ========================\n";
	$body		.= "$message\n";
	$body		.= "==============================================\n";
	
	if ( $formPosted eq "yes" ) {
		Mailer::send_mail ( $useremail, $supportemail, $body, "Job Flow: Support issue: [$jobnumber] $subject", { host => $smtp_gateway } );
	}

	my $vars = {
		formPosted	=> $formPosted,
		sidemenu	=> $sidemenu,
		sidesubmenu	=> $sidesubmenu,
		roleid		=>$roleid,
	};

	HTMLHelper::setHeaderVars( $vars, $session );
	
	$tt->process('ecm_html/support.html', $vars )
    		|| die $tt->error(), "\n";
}


##############################################################################
##############################################################################
1;
