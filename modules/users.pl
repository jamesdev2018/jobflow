#!/usr/local/bin/perl

package Users;

use strict;
use warnings;

use Template;
use Crypt::CBC;
use CGI::Session;
use CGI::Cookie;
use CGI;
use CGI::Carp qw /confess croak cluck carp/;
use Scalar::Util qw( looks_like_number );
use Encode qw(decode encode);
use JSON;
use Data::Dumper;
use POSIX qw(strftime);

# Home grown - please do NOT include jobs.pl, databaseJob.pl, databaseOpcos.pl or anything that includes jobs.pl

use lib 'modules';
use lib '.';

use CTSConstants;
use SessionConstants; #SessionConstants::SESS_* keys for session variables

use JFAuthFactory;

require "htmlhelper.pl";
require "config.pl";
require "Mailer.pl";

require "databaseAudit.pl";
require "databaseUser.pl";
require "databaseUserFilter.pl";
require "databaseColumnManager.pl";
require "databaseUserNotification.pl";
require "databaseProducts.pl";

use constant ROLE_VENDOR => 'Vendor';
use constant ROLE_VISITOR => 'Visitor';
use constant ROLEID_VISITOR => 8;

use constant DEFAULT_EXCLUDE_BY_ROLE => 'Vendor';
use constant DEFAULT_AUTH_ID => 1; # ie "Jobflow Local"
use constant DEFAULT_SMTP_GATEWAY => 'gateway.dhl.com';

use constant TEMP_USERID => 2;

use constant RS_JF_LOGGED_IN => 2; # Jobflow Logged In (see table resourcestatus)
use constant RS_JF_LOGGED_OUT => 7; # Jobflow Logged Out
use constant PRODUCT_STATUS_PAUSED => 3;



##############################################################################
# the template object 
#
##############################################################################
my $tt = Template->new({
	RELATIVE => 1,
}) || die "$Template::ERROR\n";

# Get the CGI user agent string, then look it up in table user_agents and return the id for the row
# If no entry exists, then add it.
# Returns [ $user_agent, $user_agent_id ]

sub getUserAgent {
	my $user_agent = $ENV{HTTP_USER_AGENT};
	my $user_agent_id;
	if ( ( defined $user_agent ) && ( $user_agent ne '' ) ) {
		( $user_agent_id ) = $Config::dba->process_oneline_sql( "SELECT id FROM user_agents WHERE user_agent = ?", [ $user_agent ] ); 
		if ( ! defined $user_agent_id ) {
			$Config::dba->process_oneline_sql(
				"INSERT INTO user_agents ( user_agent, rec_timestamp ) VALUES ( ?, now() )", [ $user_agent ] );
			( $user_agent_id ) = $Config::dba->process_oneline_sql( "SELECT last_insert_id()" ); 
		}
		# warn "User agent $user_agent, id $user_agent_id ";
	}
	return ( $user_agent, $user_agent_id );
}

# Determine if logged in or not

sub isLoggedIn {
	my ( $session ) = @_;

	return 0 if ! defined $session;
	return 0 if ( ( $session->param( SessionConstants::SESS_LOGGEDIN ) || 0 ) != 1 );
	return 0 if ( ! defined $session->param( SessionConstants::SESS_USERID ) );
	return 1;
}

sub sessionLogin {
	my ( $session ) = @_;

	my $userid = $session->param( SessionConstants::SESS_USERID ) || 0;
	my $username = $session->param( SessionConstants::SESS_USERNAME ) || 'unknown';
	my $tempuserid = $session->param( SessionConstants::SESS_TEMPUSER );
	$tempuserid = undef if ( defined $tempuserid ) && ( $tempuserid == 0 ); # Need undef for NULL value in usersessions

	my $email = $session->param( SessionConstants::SESS_EMAIL ) || '';
	my $auth_name = $session->param( SessionConstants::SESS_AUTH_NAME ) || 'Local';
	my $principal = $session->param( SessionConstants::SESS_PRINCIPAL ) || $username;

	eval {
		die "userid undef" unless $userid;

		# Identify the browser user agent
		my ( $user_agent, $user_agent_id ) = getUserAgent();

		# Clean up existing user sessions and make sure that the corresponding columns on users and resourcestatus are consistent

		# Delete any previously expired sessions for any user, any sessions for this user/user agent combination
		# and also sessions for users where update_time is NULL (so logically logged out)
		# The assumptions here are that
		# a) a browser's sessionid will be the same for all tabs or windows.
		# b) If the user has 2 different browsers open on the same PC/Mac connecting to jobflow, they will have different sessionids
 
		$Config::dba->process_oneline_sql(
			"DELETE FROM s USING usersessions s WHERE ( s.expiry < now() ) OR ( ( s.userid=? ) AND ( s.user_agent_id = ? ) )", [ $userid, $user_agent_id ] );

		$Config::dba->process_oneline_sql(
			"DELETE FROM s USING usersessions s WHERE EXISTS ( SELECT * FROM users u WHERE u.id = s.userid AND u.update_time IS NULL )" );

		# Update users table for any "logged in" user with no sessions
		# a) If the user has no sessions (irrespective of user agent)
		# b) users.update_time is NOT NULL and more than 24 hours in the past
		# Resource status 7 is "Jobflow User Logged Out"
		# nb We dont treat userid 2 differently, so users who login on with temporary credentials will not have their sessions
		#   destroyed but the resource status for userid 2 will perhaps be misleading.

		$Config::dba->process_oneline_sql(
			"UPDATE users u SET u.update_time = NULL, u.resourcestatusid = ? WHERE " .
				" ( NOT EXISTS ( SELECT * FROM usersessions s WHERE s.userid = u.id ) ) OR " .
				" ( ( u.update_time IS NOT NULL ) AND ( timestampdiff( HOUR, u.update_time, now() ) > 24 ) )", [ RS_JF_LOGGED_OUT ] );
        #whenwver user logout check that sessio (more than one browser). After session not present need to (Pause) in task.
		pauseTasks( $session );

		# Add row to usersessions table, the expiry date is set to 2 days time

		$Config::dba->process_oneline_sql( 
			"INSERT INTO usersessions ( userid, tempuserid, sessionid, expiry, user_agent_id ) VALUES ( ?, ?, ?, DATE_ADD( now(), INTERVAL 2 DAY ), ? )",
			[ $userid, $tempuserid, $session->id, $user_agent_id ] );

		# Common logon audit event here (remove any in JFAUth*)

		if ( ( defined $tempuserid ) && ( $tempuserid != 0 ) ) {

			ECMDBAudit::db_auditLog( $userid, "USERSTEMP", $tempuserid, "Logged in a temp user, $username ($tempuserid), email $email" );

		} else {

			# You could (in theory) login as user protemp (userid=2) without using a temporary username/pwd
			# If so, treat as per any normal user account (set update_time as well).

			$Config::dba->process_oneline_sql( "UPDATE users u SET last_loggedin = now(), update_time = now(), resourcestatusid = ? WHERE id = ?", [ RS_JF_LOGGED_IN, $userid ] );

			ECMDBAudit::db_auditLog( $userid, "USERS", $userid, "Logged in $username ($userid) using '$auth_name', username '$principal' " );
		}

		# Last but not least set the logged in flag
		$session->param( SessionConstants::SESS_LOGGEDIN, 1 );
	};
	if ( $@ ) {
		my $error = $@;
		# $error =~ s/ at .*//gs;
		carp "sessionLogin: $error";

		ECMDBAudit::db_auditLog( $userid, "USERS", $userid, "Failed during Login $username ($userid) using '$auth_name', username '$principal', $error" );
	}
}

sub sessionLogout {
	my ( $session ) = @_;

	my $userid =  $session->param( SessionConstants::SESS_USERID ) || 0;
	my $username = $session->param( SessionConstants::SESS_USERNAME ) || "unknown";
	my $tempuserid = $session->param( SessionConstants::SESS_TEMPUSER );
	my $email = $session->param( SessionConstants::SESS_EMAIL ) || '';

	eval {
		if ( $userid != 0 ) {
		
			$Config::dba->process_oneline_sql( "DELETE FROM usersessions WHERE sessionid = ?", [ $session->id ] );

			# When user logged out update the resource status as 4 which means Jobflow is logged out but Workstation is On	
			# but only do this if there are no active sessions.

			$Config::dba->process_sql( 
					"UPDATE users u SET u.update_time = NULL, resourcestatusid = ? WHERE u.id = ? AND NOT EXISTS ( SELECT * FROM usersessions s WHERE s.userid = u.id )", 
					[ RS_JF_LOGGED_OUT, $userid ] );

			if ( $userid == TEMP_USERID ) {
				ECMDBAudit::db_auditLog( $userid, "USERSTEMP", $tempuserid, "User $username ($tempuserid), $email logged out" );
			} else {
				pauseTasks( $session ); # Only pause tasks for real users 
				ECMDBAudit::db_auditLog( $userid, "USERS", $userid, "User $username ($userid) logged out" );
			}
		}

	};
	if ( $@ ) {
		my $error = $@;
		carp "sessionLogout: $error";
		if ( $userid == TEMP_USERID ) {
			ECMDBAudit::db_auditLog( $userid, "USERSTEMP", $tempuserid, "Error during logout, user $username ($tempuserid), $email, $error" );
		} else {
			ECMDBAudit::db_auditLog( $userid, "USERS", $userid, "Error during logout, user $username ($userid), $error" );
		}
	}
	$session->clear();
}

sub isADEnabled {
	my( $query ) = @_;

	my $enabled = $query->param( "ad_enable" ) || 0;
	if ( ! $enabled ) {
		$enabled = Config::getConfig('ad_enable') || 0; 
	}
	return $enabled;
}

sub DisplayLoginDialog {
	my ( $options ) = @_;

	my $message;		# error message
	# my $postLogin;	# post login action
	my $authid = 1;		# last selected authid
	my $ad_enable = 0;	# enable AD options if set
	my $debug = 0;		# Dont display history
	my @local_history;
	my $history = \@local_history;
	my $tag_env = lc( $ENV{'TAG_ENV'} || "DEV" );

	if ( ( defined $options ) && ( ref ($options) eq "HASH" ) ) {
		$message = $options->{message};
		# $postLogin = $options->{postlogin};
		$authid = $options->{authid} || DEFAULT_AUTH_ID;
		$ad_enable = $options->{ad_enable} || 0;
		$debug = $options->{debug} || 0;
		$history = $options->{history} unless ( $tag_env eq "live" ) || ( $debug == 1 );
	}

	my $rows;
	eval {
		$rows = $Config::dba->hashed_process_sql( 
				"SELECT id, auth_name, auth_title FROM auth_systems ORDER BY id" );
	};
	if ( $@ ) {
		$message = $@;
		confess "DisplayLoginDialog: $message";
		$message =~ s/ at .*//g;
	}

	my @auth_list;
	if ( defined $rows ) {
		foreach my $row (@$rows) {
			push( @auth_list, { id => $row->{id}, auth_name => $row->{auth_name}, auth_title => $row->{auth_title} } );
		}
	}

	# carp "DisplayLoginDialog: authid: $authid";

	my $vars = {
		# queryString	=> $postLogin, # Post Login actions
		nomenu		=> 1,
		login_credentials => 1,
		ad_enable	=> $ad_enable,
		authlist	=> \@auth_list,
		authid		=> $authid,
		message		=> $message, # either array ref or undef
		extracss	=> [ 'ecmstyles' ],
		history		=> $history,
	};

	# HTMLHelper::setHeaderVars( $vars );

	$tt->process('ecm_html/login.html', $vars ) || die $tt->error(), "\n";
}

# Handler for action "ProcessLogin" which can now have several sub action, so just make this a dispatcher
# and exception handler.
# Also, jobflow wont have output the HTML header, in case we redirect to masters/jrs after successful login.
# So any prompts to associate/create etc have to add the header there.
#
# Returns
# 0 - Login complete
# 1 - Login in progress, so inhibit any post action processing

sub ProcessLogin {
	my ( $query, $session ) = @_;

	my $subaction = $query->param( 'subaction' ) || 'login';
	my $authid = $query->param( 'authid' ) ||'';
	my $mode = $query->param( 'mode' ) ||'';

	# carp "ProcessLogin, subaction $subaction, authid $authid";
	my $response = 1;

	my @history;

	eval {
		if ( $subaction eq 'login' ) {
			$response = handle_login(  $query, $session, \@history );
		} elsif ( $subaction eq 'associate' ) {
			$response = handle_associate( $query, $session, \@history );
		} elsif ( $subaction eq 'create' ) {
			$response = handle_create( $query, $session, \@history );
		} elsif ( $subaction eq 'create_confirm' ) {
			$response = handle_create_confirm( $query, $session, \@history );
		} else {
			die 'Invalid command';
		}
	};
	if ( $@ ) {
		my $error = $@;
		# carp "ProcessLogin: error $error<br>\n";
		$error =~ s/ at .*//g;

		my $cookie = $query->cookie( -name =>'CGISESSID', -value => $session->id, -httponly => 1 );
		print $query->header( -cookie => $cookie, -charset => 'utf-8' );

		if ( ( $mode eq 'json' ) && ( $subaction eq 'associate' ) ) {
			my $json = encode_json ( { error => $error, history => \@history } );
			print $json;
		} else {
			my $ad_enable = isADEnabled( $query );
			DisplayLoginDialog( { message => $error, authid => $authid, ad_enable => $ad_enable, history => \@history } );
		}
		$response = 1; # ie do nothing else in jobflow.pl
	}
	return $response;
}

# Return Object corresponding to auth id (sub class of JFAuthBase)

sub get_auth_handler {
	my ( $auth_id ) = @_;

	my ( $auth_name, $auth_class_name, $auth_table ) = $Config::dba->process_oneline_sql(
		"SELECT auth_name, auth_class, auth_table FROM auth_systems WHERE id = ?", [ $auth_id ] );
	die "No auth_name for authid $auth_id" unless defined $auth_name;
	die "No auth_class  for authid $auth_id" unless defined $auth_class_name;
	# auth_table is optional, so dont test here

	my $auth_class = JFAuthFactory::createInstance( $auth_id, $auth_name, $auth_class_name, $auth_table );
	die "Auth class not created for authid $auth_id" unless defined $auth_class;

	return ( $auth_name, $auth_class );
}

# Handle ProcessLogin with sub action login
# Returns response (can be undef)
# Throws exceptions, which are handled by ProcessLogin

sub handle_login {
	my ( $query, $session, $history ) = @_;

	# Avoid CGI::param warnings, so get scalar value, then call htmlEditFormat
	my $username	= $query->param( 'username' );
	$username	= HTMLHelper::htmlEditFormat( $username );
	my $password	= $query->param( 'password' );
	$password	= HTMLHelper::htmlEditFormat( $password );

	my $auth_id =  $query->param( 'authid' ) || DEFAULT_AUTH_ID;
	my $response;  # Potential post login action

	# carp "handle_login, username $username, authid $auth_id";

	# Check for user logged in already
	sessionLogout( $session ) if ( isLoggedIn( $session ) ); 

	my ( $auth_name, $auth_class ) = get_auth_handler( $auth_id );
	my $details = $auth_class->authenticate( $username, $password, $history );
	die "Not authenticated" unless defined $details;
	die "Account disabled" unless defined $details->{enabled};

	# carp "handle_login: details from authenticate: " . Dumper( $details );

	# Copy the details (that we have) to the session, some will get
	# replaced in complete_login, when userid is known.
	$session->param( SessionConstants::SESS_FNAME, $details->{fname} || '' );
	$session->param( SessionConstants::SESS_LNAME, $details->{lname} || '' );
	$session->param( SessionConstants::SESS_EMAIL, $details->{email} || '' );
	$session->param( SessionConstants::SESS_ROLENAME, $details->{rolename} || '' );
	$session->param( SessionConstants::SESS_ROLEID, $details->{roleid} || 0 );
	$session->param( SessionConstants::SESS_TEMPUSER,  $details->{tempuser} || 0 ); # id from tempuser table (if userid == TEMP_USERID ie 2)

	# Save authentication system related for deferred use such as login audit event
	# Could in theory copy extra details such as employeeid, country etc
	$session->param( SessionConstants::SESS_AUTH_NAME, $auth_name );
	$session->param( SessionConstants::SESS_AUTH_ID, $auth_id );
	$session->param( SessionConstants::SESS_PRINCIPAL, $username ); # Save for audit event in complete_login
	$session->param( SessionConstants::SESS_DISTINGUISHED_NAME, $details->{dn} || '' );

	# If we havent got a userid, we are not associated yet
	my $userid = $details->{userid};
	if ( defined $userid ) {

		# warn "handle_login: userid $userid, username $username, roleid $details->{roleid}";
		# warn "handle_login: temp user id $details->{tempuser}" if ( $userid == 2 );

		# Force user down the associate/create path for test purposes if needed
		my $test_user_create = Config::getConfig("ad_test_user_create") || 0;
		my $test_user_associate = Config::getConfig("ad_test_user_associate") || 0;
		$userid = 0 if ( $test_user_create || $test_user_associate );
	}

	if ( ( ! defined $userid ) || ( $userid == 0 ) ) {

		my $enable_associate = Config::getConfig('ad_enable_associate') || 0;
		die "Not Authenticated" unless $enable_associate;

		$response = associate_login( $query, $session, $auth_class, $details, $history );
	} else {
		$response = complete_login( $query, $session, $userid );
	}

	return $response;
}

# Should be called on either Login or when user changes opcoid/role

sub set_role_and_opcoid {
	my ( $session, $roleid, $opcoid ) = @_;

	my $userid = $session->param( SessionConstants::SESS_USERID );
	die "set_role_and_opcoid: userid is undef" unless defined $userid;

	# warn "set_role_and_opcoid: roleid $roleid, userid $userid";

	# Get rolename for roleid
	die "Invalid roleid" unless defined $roleid;
	my ( $rolename ) = $Config::dba->process_oneline_sql( "SELECT role FROM roles WHERE id = ?", [ $roleid ] );
	die "Invalid role $roleid" unless defined $rolename;
	$session->param( SessionConstants::SESS_ROLEID, $roleid );
	$session->param( SessionConstants::SESS_ROLENAME, $rolename );

	# Could get opconame for opcoid ...
	my $opconame = '';
	my $relaxroles = '';
	if ( ( defined $opcoid ) && ( $opcoid != 0 ) ) {
		( $opconame ) = $Config::dba->process_oneline_sql( 
			"SELECT opconame FROM operatingcompany WHERE opcoid = ?", [ $opcoid ] ) if ( $opcoid );
		die "Opcoid $opcoid is invalid" unless defined $opconame;
		# Avoid loading databaseOpcos.pl
		# $relaxroles = ECMDBOpcos::db_getRelaxRoles( $opcoid );
		( $relaxroles ) = $Config::dba->process_oneline_sql( "SELECT relaxroles FROM operatingcompany WHERE OPCOID=?", [ $opcoid ] );
	}
	$session->param( SessionConstants::SESS_OPCOID, $opcoid );
	$session->param( SessionConstants::SESS_OPCONAME, $opconame );
	$session->param( SessionConstants::SESS_RELAXROLES, $relaxroles );

	# warn "set_role_and_opcoid: roleid $roleid, rolename $rolename, opconame $opconame, relaxroles $relaxroles";

	# Column manager get user settings if opcoid is valid

	my $usersettings_var = "";
	my $userfiltersettings_var = "";
	if ( $opcoid ) {
		# TODO TEMP FIX # $usersettings_var = join(",", ECMDBColumnManager::getUserSettings( $userid, $opcoid ));
		$userfiltersettings_var = join(",", ECMDBUserFilter::getUserFilterSettings( $userid, $opcoid ));
	}
	# $session->param( SessionConstants::SESS_USERSETTINGS, $usersettings_var );
	$session->param( SessionConstants::SESS_USERFILTERSETTINGS, $userfiltersettings_var );
}

# Called once we have a userid, and can thus use the userid to get other attributes
# Throws exceptions, which are handled by ProcessLogin

sub complete_login {
	my ( $query, $session, $userid ) = @_;

	die "complete_login: userid invalid" unless ( $userid );

	$userid = int( $userid ); # In case its a string, we need to save as integer

	# carp "complete_login: userid $userid";

	my ( $fname, $lname, $username, $roleid, $opcoid, $email, $status, $isdeveloper, $opmyjobs );

	# For temp users, JFAuthLocal has set the opcoid, roleid etc
	my $temp_user = $session->param( SessionConstants::SESS_TEMPUSER ) || 0;
	if ( $temp_user != 0 ) {

		my $note;
		( $email, $note ) = $Config::dba->process_oneline_sql( "SELECT email, note FROM userstemp WHERE id = ?", [ $temp_user ], 2 );
		$email = "" unless defined $email;
		if ( ( defined $note ) && ( $email eq "" ) ) {
			( $email ) = $note =~ /Email ([\S\-\_\@]*)/;
		}

		$fname = "Temp";
		$lname = "User";
		$username = "tempuser";
		$opcoid = 0; # a temp user has no opcoid
		$roleid = ROLEID_VISITOR;
		$isdeveloper = 0;
		$opmyjobs = 0;

	} else {

		( $fname, $lname, $username, $roleid, $opcoid, $email, $status ) = $Config::dba->process_oneline_sql( 
			"SELECT fname, lname, username, role, opcoid, email, status FROM users WHERE id = ?", [ $userid ] );
		die "Userid $userid invalid" unless defined $username;
		die "Userid $userid account disabled" unless ( $status == 1 );

		$isdeveloper = isDeveloper( $userid );
		$opmyjobs = 1; # This flag is set to show myjobs for operator login as default
	}
	$session->param( SessionConstants::SESS_USERID, $userid );
	$session->param( SessionConstants::SESS_USERNAME, $username );
	$session->param( SessionConstants::SESS_EMAIL, $email );
	$session->param( SessionConstants::SESS_FNAME, $fname );
	$session->param( SessionConstants::SESS_LNAME, $lname );
	$session->param( SessionConstants::SESS_FULLNAME, "$fname $lname" );
	$session->param( SessionConstants::SESS_ISDEVELOPER, $isdeveloper );
	$session->param( SessionConstants::SESS_OPMYJOBS, $opmyjobs );

	# warn "complete_login: userid $userid, roleid $roleid, opcoid $opcoid";

	set_role_and_opcoid( $session, $roleid, $opcoid );

	sessionLogin( $session );

	# carp "complete_login: Session " . Dumper( $session );
	return 0;
}

# This is called when the user has authenticated (eg using DHL AD) but the userid is undef
# It will eventually do a match (using the supplied details) against the users table
# and either display a list of potential matches or the create user dialog.
#
# Attributes returned by AD			Mapped to details
# =========================			=================
#            cn: stevtrean			samaccountname
#            sn: Treanor			lname
#             c: GB				country
#     givenName: Steve				fname
#   displayName: Steve Treanor (TAG GB)		displayname
# employeeNumber: 10593262			employeeid
#  employeeType: Employee			?
#          name: stevtrean			name
#    employeeID: 123456				employeeid2
#          mail: streanor@tagworldwide.com	email
#
# See setting attrib_set in table auth_dhl_ad_profile (sample)
# samaccount=cn,country=c,fname=givenName,lname=sn,displayname=displayName,employeeid=employeeNumber,name=name,employeeid2=employeeID,email=mail
#
# Returns 0 or 1,
#   0 if login has completed,
#   1 if the user has been prompted for more info
# Throws exception if details missing
#
sub associate_login {
	my ( $query, $session, $auth_class, $details, $history ) = @_;

	die 'associate_login, no details' unless ( defined $details );

	# carp "associate_login: details " . Dumper( $details ) . '<br>\n';

	$session->param( SessionConstants::SESS_FNAME, $details->{fname} );
	$session->param( SessionConstants::SESS_LNAME, $details->{lname} );
	$session->param( SessionConstants::SESS_EMAIL, $details->{email} );
	$session->param( SessionConstants::SESS_EMPLOYEE_ID, $details->{employeeid} );
	$session->param( SessionConstants::SESS_EMPLOYEE_ID2, $details->{employeeid2} );

	my $sam_account_name = $details->{samaccount};
	my $user_principal_name = $details->{userPrincipalName};

	$session->param( SessionConstants::SESS_SAM_ACCOUNT_NAME, $sam_account_name );
	$session->param( SessionConstants::SESS_USER_PRINCIPAL_NAME, $user_principal_name );

	my $enable_create = Config::getConfig('ad_enable_create') || 0;			# Allow create JF account from AD if needed
	my $test_user_create = Config::getConfig("ad_test_user_create") || 0;	# Force a create even if already associated
	if (( $test_user_create == 1 ) && ( $enable_create == 1 ) ) {
		push( @$history, "Forcing create Jobflow account" );
		display_create( $query, $session, $details );
		return 1;	# Dont display dashboard and we are prompting user with Create account dialog
	}

	my $candidates = $auth_class->get_potential_matches( $details, $history );
	if (( defined $candidates ) && (( scalar @$candidates ) > 0 )) {	# One or more candidates
		push( @$history, "Found " . (scalar @$candidates) . " candidates" );

		# Record list of candidate users (in session), in case user tries to fiddle response
		my @assoc_ids;
		foreach my $c ( @$candidates ) {
			push( @assoc_ids, $c->{id} );
		}
		$session->param( SessionConstants::SESS_ASSOC_USERIDS, join( ',', @assoc_ids ) ); # use to check response from associate
		push( @$history, "Displaying Associate dialog" );
		display_associate( $query, $session, $candidates );
		return 1; # Stop top level displaying dashboard
	}

	# We have ignored any existing Jobflow user ids, that are either disabled (status <> 0) or rolename Vendor/External User
	# So if there is nothing else left, allow the user to create a Jobflow userid.

	if ( $enable_create == 1 ) {
		push( @$history, "Prompting to create Jobflow account" );
		display_create( $query, $session, $details );
		return 1;
	}

	my @summary;
	foreach my $key ( qw/name userPrincipalName email employeeid employeeid2A/ ) { 
		my $v = $details->{$key};
		next unless defined $v;
		push( @summary, "$key = '$v'" );
	}
	die "User not associated with a Jobflow account,<br> " . join( ', ', @summary );
}

# Display the Create User dialog, the Create button has action ProcessLogin and subaction create

sub display_create {
	my ( $query, $session, $details ) = @_;

	# print "display_create: details " . Dumper( $details ) . '<br>\n';

	my $vars = {
		nomenu		=> 1,
		login_create	=> 1,
		details		=> $details,
		extracss	=> [ 'login_create' ],
	};

	# HTMLHelper::setHeaderVars( $vars );

	my $cookie = $query->cookie( -name =>'CGISESSID', -value => $session->id, -httponly => 1 );
	print $query->header( -cookie => $cookie, -charset => 'utf-8' );

	$tt->process('ecm_html/login_create.html', $vars ) || die $tt->error(), "\n";
}

# Display the Associate login dialog

sub display_associate {
	my ( $query, $session, $rows ) = @_;

	my $vars = {
		nomenu		=> 1,
		login_associate => 1,
		candidates	=> $rows,
		extracss	=> [ 'login_associate' ],
	};

	# HTMLHelper::setHeaderVars( $vars );

	my $cookie = $query->cookie( -name =>'CGISESSID', -value => $session->id, -httponly => 1 );
	print $query->header( -cookie => $cookie, -charset => 'utf-8' );

	$tt->process('ecm_html/login_associate.html', $vars ) || die $tt->error(), "\n";
}

# Display a page telling the user what the new Jobflow username is
# along with the AD credentials associated.
 
sub display_create_user_confirmation {
	my ( $query, $session ) = @_;

	my $vars = {
		nomenu		=> 1,
		login_create_confirm => 1,
		username	=> $session->param( SessionConstants::SESS_USERNAME ),
		auth_name	=> $session->param( SessionConstants::SESS_AUTH_NAME ),
		sam_account_name => $session->param( SessionConstants::SESS_SAM_ACCOUNT_NAME ),
		email		=> $session->param( SessionConstants::SESS_EMAIL ),
		extracss	=> [ 'login_create' ],
	};

	my $cookie = $query->cookie( -name =>'CGISESSID', -value => $session->id, -httponly => 1 );
	print $query->header( -cookie => $cookie, -charset => 'utf-8' );

	$tt->process('ecm_html/login_create_confirm.html', $vars ) || die $tt->error(), "\n";
}

# Associate userid with current AD attributes in session
# Used by handle_associate and handle_create

sub associate_user {
	my ( $query, $session, $userid ) = @_;

	my ( $username ) = $Config::dba->process_oneline_sql( "SELECT username FROM users WHERE id = ?", [ $userid ] );
	die "associate_user: userid has no corresponding username" unless defined $username;

	my $auth_id = $session->param( SessionConstants::SESS_AUTH_ID );
	# print "associate_user: userid $userid authid $auth_id<br>\n";

	my ( $auth_name, $auth_class ) = get_auth_handler( $auth_id );
	my $user_principal_name = $session->param( SessionConstants::SESS_USER_PRINCIPAL_NAME );
	my $sam_account_name = $session->param( SessionConstants::SESS_SAM_ACCOUNT_NAME );
	my $distinguished_name  = $session->param( SessionConstants::SESS_DISTINGUISHED_NAME );
	my $email = $session->param( SessionConstants::SESS_EMAIL );

	# die "associate_user: userid $userid email $email upn $user_principal_name, sam $sam_account_name, dn $distinguished_name <br>\n";

	$auth_class->bind_auth( $userid, $email, $user_principal_name, $sam_account_name, $distinguished_name );

	$session->param( SessionConstants::SESS_USERNAME, $username );
	$session->param( SessionConstants::SESS_USERID, $userid );
	$session->param( SessionConstants::SESS_FULLNAME, $session->param( SessionConstants::SESS_FNAME ) . ' ' . $session->param( SessionConstants::SESS_LNAME ) );

	# Add audit event
	ECMDBAudit::db_auditLog( $userid, "USERS", $userid, "Associated $username ($userid) using $auth_name, UPN $user_principal_name, SAM $sam_account_name" );

}

# Called when ProcessLogin called with subaction 'associate'
# ie the user has selected a userid from the list of potential matches
# This will eventually call complete_login.

sub handle_associate {
	my ( $query, $session, $history ) = @_;

	my $userid = $query->param( 'userid' );
	die "handle_associate: userid is undef" unless defined $userid;

	my $mode = $query->param( 'mode' ) || '';

	my $assoc_ids = $session->param( SessionConstants::SESS_ASSOC_USERIDS );
	die "handle_associate: list of potential matches missing" unless defined $assoc_ids;
	
	# Check userid is in the set of userids posted, so the user isn't trying to associate to someone elses userid

	my $found = 0;
	foreach my $id ( split /,/, $assoc_ids ) {
		if ( $id eq $userid ) {
			$found = 1;
			last;
		}
	}
	die "No match for userid" unless $found;

	push( @$history, "handle_associate: userid $userid matched" );

	my $pw = $query->param( "password" ); # User supplies password to confirm they are the owner of the JF account
	die "Password not supplied" unless defined $pw;
	my $encrypted_password = EncryptPwd( $pw );

	my ( $pw_match ) = $Config::dba->process_oneline_sql( "SELECT 1 FROM users WHERE id=? AND password=?", [ $userid, $encrypted_password ] );
	die "Password invalid" unless ( $pw_match );

	# Make the association (let JFAuthAD or whatever do it)

	associate_user( $query, $session, $userid );

	send_create_email( $query, $session, 0 ); # 3rd arg is 1 for create, 0 for associate

	# Then as per a normal login

	complete_login( $query, $session, $userid );

	# For handle_associate we use a JSON request, so if teh 2nd part fails, we can avoid the Login dialog
	if ( $mode eq 'json' ) {
		my $cookie = $query->cookie( -name =>'CGISESSID', -value => $session->id, -httponly => 1 );
		print $query->header( -cookie => $cookie, -charset => 'utf-8' );
		my $json = encode_json ( { userid => $userid, history => $history } );
		print $json;
		return 1;
	}
	return 0; # Should display Dashboard
}

# Get next available username based on users SAM account name
# Tries the supplied username, then if that exists already, adds a suffix n, n=1..99
# until it finds a username not in use.

sub get_nearest_free_username {
	my ( $seed ) = @_;

	my $suffix = 0;
	my $limit = 99;
	my $username = $seed;
	my $user_exists = 1;
	while ( $limit ) {
		# Check whether $username exists
		( $user_exists ) = $Config::dba->hashed_process_sql( 
				"SELECT 1 FROM users WHERE username = ?", [ $username ] );
		last unless $user_exists;
		$limit--;
		$suffix++;
		$username = $seed . $suffix;
	}
	die "Could not find a free username based on '$seed'" if $user_exists;
	return $username;
}

# Called when ProcessLogin called with subaction 'create'
# ie the user has clicked OK 

# The details (from DHL AD auth) should already be in the session, so we create the user
# whatever the databaseUser function is ...
# (and it now returns (1, $userid), so we can then log the user on
# by calling complete_login( $query, $session, $userid )

sub handle_create {
	my ( $query, $session, $history ) = @_;

	my $opcoid = undef;		# opcoid must be undef rather than 0
	my $companyid = undef;		# companyid must be undef rather than 0

	# Restore the details we need to create a user
	my $fname = $session->param( SessionConstants::SESS_FNAME );
	my $lname = $session->param( SessionConstants::SESS_LNAME );
	my $email = $session->param( SessionConstants::SESS_EMAIL );
	my $employeeid = $session->param( SessionConstants::SESS_EMPLOYEE_ID );
	my $employeeid2 = $session->param( SessionConstants::SESS_EMPLOYEE_ID2 );
	if ( ! $employeeid ) { $employeeid = $employeeid2 };

	my $sam_account_name = $session->param( SessionConstants::SESS_SAM_ACCOUNT_NAME );
	my $user_principal_name = $session->param( SessionConstants::SESS_USER_PRINCIPAL_NAME );

	push( @$history, "User $fname $lname, email '$email', employeeID '$employeeid', SAM '$sam_account_name', UPN '$user_principal_name'" );
	my ( $roleid ) = $Config::dba->process_oneline_sql( "SELECT id FROM roles WHERE role = ?", [ "Visitor" ] );
	die "Visitor role not defined in roles table" unless defined $roleid;
 
	# See if username already exists and if so, add something to the end until there is no clash
	my $username = get_nearest_free_username( $sam_account_name ); # Check for username in use or die trying
	push( @$history, "Nearest free username '$username'" );

	my $password = getTempPassword(); # Create random password, we send this in the email
	my $encrypted_password = EncryptPwd( $password );

	# Create the user
	my ( $status, $userid ) = ECMDBUser::addUser( $opcoid, $companyid, $fname, $lname, $username, $encrypted_password, $email, $employeeid, $roleid );

	# Audit event for adding user (should really be in addUser but we need callers userid)
	ECMDBAudit::db_auditLog( $userid, "USERS", $userid, "Added new user $username ($userid)" );

	push( @$history, "Added user $username, userid $userid, roleid $roleid" );
	$session->param( SessionConstants::SESS_USERNAME, $username );
	$session->param( SessionConstants::SESS_USERID, $userid );

	# Associate the userid with the SAM Account name/User Principal Name
	associate_user( $query, $session, $userid );

	# Send email to user, with the password and email to administrators, with link to edit user for new user
	send_create_email( $query, $session, 1 ); # 3rd arg is 1 for create, 0 for associate

	# Log user in - defer to handle_create_confirm 
	$session->param( SessionConstants::SESS_CREATE_CONFIRM, 1 );

	# Display confirmation message
	display_create_user_confirmation( $query, $session );

	return 1;
}

sub handle_create_confirm {
	my ( $query, $session, $history ) = @_;

	# Check that the session flag is set (otherwise here on false pretenses)
	my $create_confirm = $session->param( SessionConstants::SESS_CREATE_CONFIRM ) || 0;
	die "Not authenticated 2" unless $create_confirm;

	# Clear the session flag
	$session->param( SessionConstants::SESS_CREATE_CONFIRM, 0 );

	# Log user in
	my $userid = $session->param( SessionConstants::SESS_USERID );
	push( @$history, "create_confirm: calling complete_login, userid $userid" );
	return complete_login( $query, $session, $userid );
}

# Send email to user, with the password and email to administrators, with link to edit user for new user
# The create flag is 1 for a new user or 0 for associating with an existing user (As much of the email content is similar)
 
sub send_create_email {
	my ( $query, $session, $create ) = @_;

	my @user_msg;

	# Get list of admins first
	my $admins;
	my $auth_id = $session->param( SessionConstants::SESS_AUTH_ID );
	eval {
		$admins = $Config::dba->hashed_process_sql( "SELECT admin_name, admin_email FROM auth_admins WHERE auth_id = ?", [ $auth_id ] );
	};
	if ( $@ ) {
		my $message = $@;
		confess "send_create_email: $message";
		$message =~ s/ at .*//g;
	}
	my $current_time = strftime "%a %b %e %H:%M:%S %Y", gmtime;
	push( @user_msg, 'Dear ' . $session->param( SessionConstants::SESS_FULLNAME ) );
	push( @user_msg, 'You logged onto Jobflow using your ' . $session->param( SessionConstants::SESS_AUTH_NAME ) . " credentials on $current_time." );
	push( @user_msg, '' );
	push( @user_msg, 'Username: ' . $session->param( SessionConstants::SESS_SAM_ACCOUNT_NAME ) ); # should really be domain qualified TODO
	push( @user_msg, 'Email: ' . $session->param( SessionConstants::SESS_EMAIL ) );
    push( @user_msg, 'User Principal Name: ' . $session->param( SessionConstants::SESS_USER_PRINCIPAL_NAME ) );
	push( @user_msg, 'Employee id: ' . $session->param( SessionConstants::SESS_EMPLOYEE_ID ) );
	push( @user_msg, 'Employee id 2: ' . $session->param( SessionConstants::SESS_EMPLOYEE_ID2 ) );
	push( @user_msg, '' );

	my $assistance;
	my $action;
	if ( $create ) {
		push( @user_msg, 'No existing Jobflow user matched your credentials, so a new Jobflow user has been added' );
		push( @user_msg, 'with user name ' . $session->param( SessionConstants::SESS_USERNAME ) . ' (' . $session->param( SessionConstants::SESS_USERID ) . ').' );
		$assistance = 'to complete your profile';
		$action = 'create';
	} else {
		push( @user_msg, 'Your credentials have been matched to existing Jobflow username ' . $session->param( SessionConstants::SESS_USERNAME ) . ' (' . $session->param( SessionConstants::SESS_USERID ) . ').' );
		$assistance = 'if you need to change the association made';
		$action = 'associate';
	}

	push( @user_msg, '' );
	if ( ( ! defined $admins ) || ( scalar @$admins ) == 0 ) {
		push( @user_msg, 'No local auth system administrator defined for ' . $session->param( SessionConstants::SESS_AUTH_NAME ) . '.' );
		my $supportemail = Config::getConfig( 'usersupportemail' ) || Config::getConfig( 'supportemail' );
		push( @user_msg, "Please contact $supportemail, $assistance." ) if defined $supportemail;
	} else {
		push( @user_msg, "Please contact one of the Jobflow administrators listed below, $assistance." );
		push( @user_msg, '' );
		foreach my $row (@$admins) {
			push( @user_msg, $row->{admin_name} . ', ' . $row->{admin_email} );
		}
	}
	push( @user_msg, '' );
	push( @user_msg, 'Thanks' );
	
	my $fromaddress = Config::getConfig( 'fromemail' );
	my $smtp_gateway = Config::getConfig("smtp_gateway") || DEFAULT_SMTP_GATEWAY;
	my $toaddress = $session->param( SessionConstants::SESS_EMAIL );
	my $email_body = join( "\n", @user_msg );
	my $email_subject = 'Jobflow User Account ' . $session->param( SessionConstants::SESS_USERNAME ) . " " . $action;

	my $rc = Mailer::send_mail( $fromaddress, $toaddress, $email_body, $email_subject, { dieonerror => 1, host => $smtp_gateway } );
	die "send_create_email cc failed ($rc)" if ( $rc != 1 );

	# Now send an email to each admin as the user will need assistance
	# Only send the email if a user has been created, rather than associated

	if ( ( $create ) && ( defined $admins ) && ( scalar @$admins ) > 0 ) {
		@user_msg = ();
		push( @user_msg, 'User ' . $session->param( SessionConstants::SESS_SAM_ACCOUNT_NAME ) . ' has logged into Jobflow' );
		push( @user_msg, 'but no match to an existing Jobflow userid was found.' );
		push( @user_msg, 'A new Jobflow user ' . $session->param( SessionConstants::SESS_USERNAME ) . ' (' . $session->param( SessionConstants::SESS_USERID ) . ')' );
		push( @user_msg, 'was created and the credentials associated to this user.' );
		push( @user_msg, '' );
		push( @user_msg, 'Username: ' . $session->param( SessionConstants::SESS_SAM_ACCOUNT_NAME ) ); # should really be domain qualified TODO
		push( @user_msg, 'Email: ' . $session->param( SessionConstants::SESS_EMAIL ) );
		push( @user_msg, 'First name: ' . $session->param( SessionConstants::SESS_FNAME ) );
		push( @user_msg, 'Last name: ' . $session->param( SessionConstants::SESS_LNAME ) );
		push( @user_msg, 'Employee id: ' . $session->param( SessionConstants::SESS_EMPLOYEE_ID ) );
		push( @user_msg, 'Employee id 2: ' . $session->param( SessionConstants::SESS_EMPLOYEE_ID2 ) );
		push( @user_msg, '' );
		push( @user_msg, 'The following link takes you to the Edit User page for this user' );

		my $application_url = Config::getConfig("jobflowserver") . "/jobflow.pl";
		my $edit_link = $application_url . '?action=edituser&subaction=edit&userid=' . $session->param( SessionConstants::SESS_USERID ) . '&authsys=' . $session->param( SessionConstants::SESS_AUTH_ID );
		push( @user_msg, $edit_link );

		$email_subject = 'Jobflow User setup ' . $session->param( SessionConstants::SESS_AUTH_NAME );
		$email_body = join( "\n", @user_msg );

		foreach my $row ( @$admins ) {
			my $rc = Mailer::send_mail( $fromaddress, $row->{admin_email}, $email_body, $email_subject, { dieonerror => 1, host => $smtp_gateway } );
			die "send_create_email cc failed ($rc)" if ( $rc != 1 );	
		}
	}
}

# Save current query and some of session so that
# a) we have enough info to display Login with correct Auth sys selected
# b) After successfull login, we can execute the saved query

sub SaveQuery {
	my ( $query, $session ) = @_;

	my @params = $query->param;  # List of query parameter names
	my @values;

	foreach my $k ( @params ) {
		push( @values, "$k=" . $query->param( $k ) );
		#$_ = HTMLHelper::htmlEditFormat( $_ );
	}
	my $queryString = join( "&", @values );
	# carp "SaveQuery: string $queryString<br>";

	# Save a small number of session params
	my $auth_id = $session->param( SessionConstants::SESS_AUTH_ID );
	$session->clear();

	$session->param( SessionConstants::SESS_AUTH_ID, $auth_id );
	$session->param( SessionConstants::SESS_POSTLOGIN, $queryString );
}

sub RestoreQuery {
	my ( $query, $session ) = @_;

	my $queryString = $session->param( SessionConstants::SESS_POSTLOGIN );
	if ( defined $queryString  ) {
		# carp "RestoreQuery, string $queryString";
		$query->delete_all();
		my @values = split /&/, $queryString;
		foreach my $pair ( @values ) {
			my ( $k, $v ) = split /=/, $pair;
			$query->param( -name => $k, -value => $v )
		}
		
	}
	$session->clear( SessionConstants::SESS_POSTLOGIN );
}

##############################################################################
#       Login
#
##############################################################################
sub Login {
	my $query		= $_[0];

	my @params 		= $query->param;

	my $queryString = "";

	my $ad_enable = isADEnabled( $query );
	DisplayLoginDialog( { ad_enable => $ad_enable } );
}

##############################################################################
#       ProcessLogout
#
##############################################################################
sub ProcessLogout {
	my ( $query, $session ) = @_;

	my $userid = $session->param( SessionConstants::SESS_USERID ) || 0;
	my $authid = $session->param( SessionConstants::SESS_AUTH_ID ) || 1;
	my $authname = $session->param( SessionConstants::SESS_AUTH_NAME ) || '';


	sessionLogout( $session ) if ( $userid != 0 );
	$session->delete();

	my $ad_enable = isADEnabled( $query );
	DisplayLoginDialog( { authid => $authid, ad_enable => $ad_enable } );
}

##############################################################################
#       PageAccessRestricted
#
##############################################################################
sub PageAccessRestricted {

	my $vars = {
 	};

	# Dont add header vars

	$tt->process( 'ecm_html/pageAccessRestricted.html', $vars )
    		|| die $tt->error(), "\n";
}

##############################################################################
#       getEmail
#
##############################################################################
sub getEmail {
	my $userid = $_[0];
	
	return ECMDBUser::db_getEmail( $userid );
}


# Generate temporary userid and password 
# see http://www.perlmonks.org/?node_id=233023

sub rndStr{ join'', @_[ map{ rand @_ } 1 .. shift ] }
	
sub getTempUsername {

	my $username_len = 8 + int(rand(4)); # So userid is between 8..12 chars
	my $username = rndStr $username_len, 'A'..'Z', 'a'..'z', '0'..'9';

	return $username;
}

# Return password in clear text (as we need to send it in an email).

sub getTempPassword {

	my $password_len = 8 + int(rand(4));
 	my $password = rndStr $password_len, 'A'..'Z', 'a'..'z', '0'..'9';

	return $password;
}

# Encrypt the password before calling ECMDBUser::addUserTemp

sub EncryptPwd {
	return crypt ( $_[0], "ab" );
}


##############################################################################
#       isDeveloper
#
##############################################################################
sub isDeveloper {
	my ($userid ) = @_;

	return 0 unless (defined $userid) && looks_like_number( $userid );

	my $developers = Config::getConfig("developers") || '';

	my $isDeveloper = 0;
	my @list = split /,/, $developers;

	foreach ( @list ) {
		if ( $userid == $_ ) {
			$isDeveloper = 1;
			last;
		}
	}

	return $isDeveloper;
}

##############################################################################
#       isDeveloper
#
##############################################################################
sub isExplorer {
	my ($userid ) = @_;

	return 0 unless (defined $userid) && looks_like_number( $userid );

	my $explorers = Config::getConfig("intrepid_explorers") || '';

	my $isExplorer = 0;
	my @list = split /,/, $explorers;

	foreach ( @list ) {
		if ( $userid == $_ ) {
			$isExplorer = 1;
			last;
		}
	}

	return $isExplorer;
}

##############################################################################
#       HandleRoleidChange - handle role/opcoid change from Account Settings dialog
#	moved here from jobflow.pl by SAT 2016-06-29
#
##############################################################################

sub HandleRoleidChange {
	my ( $query, $session ) = @_;

	# The user has changed either of or both of opcoid and roleid

	my $new_role_id 			= $query->param( 'newroleid' );
	my $newOperatingCompany		= $query->param( "newopcoid" );

	my $roleid = $session->param( SessionConstants::SESS_ROLEID );
	my $userid = $session->param( SessionConstants::SESS_USERID );
	my $opcoid = $session->param( SessionConstants::SESS_OPCOID );

	$session->param( SessionConstants::SESS_OPMYJOBS, 1 );

	my $role_changed = 0;
	my $opcoid_changed = 0;
	my @history;

	my $vars;

	eval {

		# Determine if opcoid has changed
		if ( ( defined $newOperatingCompany ) && ( $newOperatingCompany ne '' ) ) {
			push( @history, "Validating opco '$newOperatingCompany', existing opcoid is $opcoid" );
			Validate::opcoid( $newOperatingCompany );
			my ( $opcoid_valid ) = $Config::dba->process_oneline_sql( 
					"SELECT 1 FROM useropcos WHERE userid = ? AND opcoid = ?", [ $userid, $newOperatingCompany ] );
			die "opcoid $newOperatingCompany is invalid for this user" unless $opcoid_valid;
			$opcoid = $newOperatingCompany;
			# Defer session update to set_role_and_opcoid
			$opcoid_changed = 1;
		}

		# Determine if role has changed
		if ( defined $new_role_id ) {
			push( @history, "Validating roleid '$new_role_id', existing roleid is $roleid" );
			Validate::roleid( $new_role_id );
			my ( $roleid_valid ) = $Config::dba->process_oneline_sql( 
					"SELECT 1 FROM userroles WHERE userid = ? AND opcoid = ? AND roleid = ?", [ $userid, $opcoid, $new_role_id ] );
			die "roleid $new_role_id is invalid for this user, with opcoid $opcoid" unless $roleid_valid;
			$roleid = $new_role_id;
			# Defer session update to set_role_and_opcoid
			$role_changed = 1;
		}

		# If either opcoid or role has changed, check that role is valid for this opco
		if ( $role_changed || $opcoid_changed ) {
			set_role_and_opcoid( $session, $roleid, $opcoid );
		}

		# Update opco if needed
		if ( $opcoid_changed ) {

			# change the opcoid? if so also clear the filter settings
			eval { 	# opcoid has changed

				# update session for column manager usersettings on change of opcoid
				my $usersettings_var = join(",", ECMDBColumnManager::getUserSettings( $userid, $newOperatingCompany ));
				$session->param( SessionConstants::SESS_USERSETTINGS, $usersettings_var);

				my $userfiltersettings_var = join(",", ECMDBUserFilter::getUserFilterSettings( $userid, $newOperatingCompany ));
				$session->param( SessionConstants::SESS_USERFILTERSETTINGS, $userfiltersettings_var );

				# when selecting a new opco - reset the search filters
				##Session clear for filters when opcoid is changed - Added by Feroz

				foreach my $filterName ( 
						"agencyFilter", "clientFilter", "projectFilter", "preflightFilter", 
						"newagencyFilter", "newclientFilter", "newprojectFilter", "newoperatorstatusFilter", "newqcstatusFilter", "newpreflightFilter", "newLocationFilter", "newAssignFilter", 
						"fromId", "orderbyid" ) { 
					$session->clear( $filterName );
				}
			};
			if ( $@ ) {
				die "Failed to change opcoid to $newOperatingCompany";
			}
		}

		# If the user checks/unchecks any checkboxes, reflect the changes in the UserNotification table

		# Watchlist Notifications ( Assigned, QC Required, QC Approved, QC Rejected, Job Rejected, Dispatched, Team Change )
		# Receive Notifications ( Message, Email )

		my $assignjob				= $query->param('assigned') || 0 ;
		my $qcrequired				= $query->param('qcrequired') || 0 ;
		my $qcapproved				= $query->param('qcapproved') || 0 ;
		my $qcreject				= $query->param('qcreject') || 0 ;
		my $jobreject				= $query->param('jobreject') || 0 ;
		my $dispatched				= $query->param('dispatched') || 0 ;
		my $teams					= $query->param('teams') || 0 ;
		my $message					= $query->param('message') || 0 ;
		my $email					= $query->param('email') || 0 ;
		my $advisory				= $query->param('advisory') || 0 ;

		ECMDBUserNotification::checkandupdatenotification( $userid, $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $teams, $message, $email, $advisory );

		# Auto Refresh (mins)
		# View Message (days)
		# Message Groups
		# Tooltips checkbox

		my $auto_refresh 			= $query->param( 'auto_refresh' ) || 0; 

		my $view_messageday			= $query->param( 'view_messages' );
		my $exist_messageday		= $query->param( 'dbviewmessageday' );

		my $db_statusNoteFlg		= $query->param( 'db_status_note_flag' );
		my $currstatusNoteFlg		= $query->param( 'tooltips' ) || 0;


		ECMDBUser::db_update_autorefresh_duration( $userid, $auto_refresh ) if ( defined $auto_refresh );

		if ( $exist_messageday != $view_messageday ) {
			push( @history, "Change message day, was $exist_messageday, now $view_messageday" );
			ECMDBUser::db_update_message_day( $userid, $view_messageday );
		}

		#To update status note flag in database if its status is changed.
		if ( $db_statusNoteFlg ne $currstatusNoteFlg && $currstatusNoteFlg ne "" ) {
			push( @history, "Changed tooltip, was $db_statusNoteFlg, now $currstatusNoteFlg" );
			ECMDBUser::db_updateStatusNoteFlag( $userid, $currstatusNoteFlg );
		}

		$vars = { success => 1, roleid => $roleid, roleid_changed => $role_changed, opcoid => $opcoid, opcoid_changed => $opcoid_changed, history => \@history };
	};
	if ( $@ ) {
		# Its a POST request, so return any error as JSON
		my $error = $@;
		push( @history, $error ); # Before trimming error
		my $long_error = $error;
		# cluck $error;
		$error =~ s/ at .*//g;
		$vars = { error => $error, long_error => $long_error, history => \@history };
	}

	# @TODO For some reason, this gets forced to reload the page, so we never see the JSON response

	my $json = encode_json ( $vars );
	return $json;
}

# moved from jobs.pl and changed to use JSON (means client end of Ajax call traps invalid JSON payload)

sub get_session_details {
	my ( $query, $session ) = @_;

	my $vars;

	my $userid = $session->param( SessionConstants::SESS_USERID );
	my $opcoid = $session->param( SessionConstants::SESS_OPCOID );

	$vars = { userid => $userid,
			  opcoid => $opcoid,
			 }; # Room for other things

	my $json = encode_json ( $vars );
	print $json;
}


#####################################################################
#
#Pause Task
#####################################################################
sub pauseTasks {
    my ( $session ) = @_;

    my $userid = $session->param( SessionConstants::SESS_USERID ) || 0;
    my $username = $session->param( SessionConstants::SESS_USERNAME ) || 'unknown';

    my ( $resourcestatusid ) = $Config::dba->process_oneline_sql(
      "SELECT resourcestatusid FROM users WHERE id = ?", [ $userid ] );
    return unless $resourcestatusid == RS_JF_LOGGED_OUT;

    my ( $lineitemid, $jobnumber ) = ECMDBProducts::getActiveProductdetails( $userid );
    return unless $jobnumber;

    # OK, so user has no active sessions but has an active task

    ECMDBProducts::pauseProduct( $lineitemid, $userid );
    ECMDBProducts::setProductStatus( PRODUCT_STATUS_PAUSED, $lineitemid );      # 3 = Paused, ie $productstatuses{ Paused } == 3
    ECMDBProducts::updateJobAssignmentHistory( $userid, $jobnumber, $lineitemid );
	my $productname     = ECMDBProducts::getProductName( $jobnumber, $lineitemid );
    ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) has been paused by user: $username" );
    ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $productname (Job: $jobnumber) has been paused by user: $username" );
}



##############################################################################
##############################################################################
#       end of file
##############################################################################
##############################################################################
1;
