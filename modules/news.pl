#!/usr/local/bin/perl
package News;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );

use HTML::Entities;
use JSON;
use Template;

# Home grown
use lib 'modules';

require "htmlhelper.pl"; # HTMLHelper::setHeaderVars and htmlEditFormat

#
# PLEASE NOTE
# This is a lightweight module, so please do not put anything in here that uses
# Users.pl, Opcos.pl or any other modules that requires jobs.pl
# Khalids change to show how to use github flow:
# https://guides.github.com/introduction/flow/
##############################################################################
# the template object 
##############################################################################
my $tt = Template->new({
	RELATIVE => 1,
}) || die "$Template::ERROR\n";

##############################################################################
#       AddNews ()
#		Add some news
##############################################################################
sub AddNews {
	my ( $query, $session ) = @_;

	my $sidemenu	= $query->param( "sidemenu" );
	my $sidesubmenu	= $query->param( "sidesubmenu" );
	my $formPosted	= $query->param( 'formPosted' ) ||"";
	my $newsitem	= $query->param( 'newsitem' );
	my $subject	= $query->param( 'subject' );

	my $roleid	= $session->param( 'roleid' );
	my $userid	= $session->param( 'userid' );
	
	$subject = HTMLHelper::htmlEditFormat( $subject ); # Avoid CGI warning

	my %error;
	my $errorMessage = "";

	if ( $formPosted eq "yes" ) {

		%error = validateNews( $subject, $newsitem );

		if ( $error{ oktogo } == 1 ) {
			$Config::dba->process_oneline_sql(
				"INSERT INTO NEWS ( STATUS, USERID, SUBJECT, CONTENT, REC_TIMESTAMP ) VALUES ( 1, ?, ?, ?, now() )",
				[ $userid, $subject, $newsitem ] );
		}
	}

	my $vars = {
		subject		=> $subject,
		newsitem	=> $newsitem,
		formPosted	=> $formPosted,
		error 		=> \%error,
		sidemenu	=> $sidemenu,
		sidesubmenu	=> $sidesubmenu,	
		roleid		=> $roleid,
	};

	HTMLHelper::setHeaderVars( $vars, $session );

	$tt->process('ecm_html/addnews.html', $vars ) || die $tt->error(), "\n";
}


##############################################################################
#       validateNews ()
##############################################################################

sub validateNews {
	my ( $subject, $newsitem ) = @_;

	my %error;
	$error{ oktogo } = 1;

	#validate params
	if ( length( $subject ) < 1 ) {
		$error{ subject } = "Subject is required.";
		$error{ oktogo } = 0;
	}
	if ( length( $newsitem ) < 1 ) {
		$error{ newsitem } = "News item is requried.";
		$error{ oktogo } = 0;
	}

	return %error;
}

# replaces getNewsItems and getNewItem previously in general.pl

sub getNewsItems {

	my ( $id ) = @_;   # can be undef (in which case return 1st 25 news items)

	my @values;
	my $whereClause	= "";
	if ( defined $id ) {
		$whereClause	= " WHERE id = ?";
		push( @values, [ $id ] );
	}

	my $sql = "SELECT id, subject, content, rec_timestamp FROM NEWS $whereClause order by rec_timestamp desc";
	my $rows = $Config::dba->hashed_process_sql( $sql, @values );

	$rows = () unless defined $rows;

	my @newsitems;
	foreach my $r ( @$rows ) {
		push( @newsitems, { id => $r->{id}, subject => $r->{subject}, content => $r->{content}, timestamp => $r->{rec_timestamp} } );
	}
	return @newsitems;
}

##############################################################################
#       ListNews ()
#		List news
##############################################################################
sub ListNews {
	my ( $query, $session ) = @_;

	my $sidemenu   = $query->param( "sidemenu" );
	my $sidesubmenu= $query->param( "sidesubmenu" );
	my $roleid     = $session->param( 'roleid' );

	my @newsitems = getNewsItems();

	my $vars = {
		newsitems	=> \@newsitems,
		sidemenu	=> $sidemenu,
		sidesubmenu	=> $sidesubmenu,
		roleid		=> $roleid,
	};

	HTMLHelper::setHeaderVars( $vars, $session );
	
	$tt->process('ecm_html/listnews.html', $vars ) || die $tt->error(), "\n";
}

##############################################################################
#       NewsSummary ()
##############################################################################
sub NewsSummary {
	my ( $query, $session ) = @_;

	my $sidemenu   = $query->param( "sidemenu" );
	my $sidesubmenu= $query->param( "sidesubmenu" );
	my $roleid     = $session->param( 'roleid' );

	my @newsitems	= getNewsItems();

	my $vars = {
		newsitems	=> \@newsitems,
		sidemenu	=> $sidemenu,
		sidesubmenu	=> $sidesubmenu,
		roleid		=>$roleid,
	};

	HTMLHelper::setHeaderVars( $vars, $session );

	$tt->process('ecm_html/newssummary.html', $vars ) || die $tt->error(), "\n";
}

##############################################################################
#       EditNews ()
#		
##############################################################################
sub EditNews {
	my ( $query, $session ) = @_;

	my $id		= $query->param( "id" );
	my $formPosted 	= $query->param( "formPosted" ) || "";

	my $subject;
	my $content; 
	my %error;

	if ( $formPosted eq "yes" ) { 	#get from form
		$subject 		= $query->param( "subject" );
		$subject		= HTMLHelper::htmlEditFormat( $subject ) if defined $subject;
		$content 		= $query->param( "content" );
	} else {						#get from db
		my @newsitem		= getNewsItems( $id );
		$subject		= $newsitem[0]{ subject };
		$content 		= $newsitem[0]{ content };
	}

	if ( $formPosted eq "yes" ) { #update the user

		%error = validateNews( $subject, $content, "yes" );

		return 0 unless looks_like_number( $id );

		if ( $error{oktogo} == 1 ) {

			$Config::dba->process_sql( "UPDATE NEWS SET subject = ?, content = ? WHERE id = ?", [ $subject, $content, $id ] );
		}
	}

	my $vars = {
		id		=> $id,
		subject		=> $subject,
		content		=> $content,
		formPosted	=> $formPosted,
		error		=> \%error,
	};

	HTMLHelper::setHeaderVars( $vars, $session );

	$tt->process('ecm_html/editnews.html', $vars ) || die $tt->error(), "\n";
}

1;
