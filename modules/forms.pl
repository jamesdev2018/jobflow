#!/usr/local/bin/perl
package Forms;

use strict;
use warnings;

use Template;
use Crypt::CBC;
use CGI::Session;
use CGI::Cookie;
use CGI;
use strict;
use Scalar::Util qw( looks_like_number );
use Encode qw(decode encode);
use JSON;
use File::Path;
use File::Basename;
use File::Copy;
use Net::FTP::File;

use lib 'modules';

require "audit.pl";
require "opcos.pl";
require "companies.pl";
require "general.pl";
require "config.pl";
require "Mailer.pl";
require "databaseUser.pl";
require "databaseOpcos.pl";
require "databaseJob.pl";
require "Checklist.pl";
require "databaseForms.pl";
require "jobs.pl";

use CTSConstants;

use constant DEFAULT_SMTP_GATEWAY => 'gateway.dhl.com';
use constant DEFAULT_FROM_ADDRESS => 'jobflow@tagworldwide.com';

# global session
my $userSession;

#################################################################
# from config // Khalid has changed this comment again 16:24
#################################################################

my $jobflowServer   = Config::getConfig("jobflowserver") || 'https://jobflow.tagworldwide.com';

my $abinbevprodrecipients = Config::getConfig("abinbevprodrecipients" );

my $applicationurl  = $jobflowServer . "/jobflow.pl";

my $upload_dir      = Config::getConfig("uploaddirectory");
my $formstasgpfspath	= Config::getConfig("formstasgpfspath");

my $tasserver	= Config::getConfig("tasserver");
my $tasusername	= Config::getConfig("tasusername");
my $taspassword	= Config::getConfig("taspassword");
my $taspath		= Config::getConfig("taspath");
my $tagmasterpath = Config::getConfig("abinbev_tag_master");
my $scannablebarcodepath = Config::getConfig("abinbev_scan_path");

##############################################################################
# the template object 
#
##############################################################################
my $tt = Template->new({
    RELATIVE => 1,
}) || die "$Template::ERROR\n";

##############################################################################
# CreateSession 
#
# pass in the cookie sid or undef
##############################################################################
sub CreateSession {
    my $sid = $_[0];
    my $query = $_[1];

	$userSession = new CGI::Session( "driver:File", $sid, { Directory => '/tmp' } );
    $userSession->expire( Config::getConfig( 'sessionexpiry' ) );

	my $cookie = $query->cookie( -name =>'CGISESSID', -value => $userSession->id, -httponly => 1 );

	print $query->header( -cookie => $cookie );

    return $userSession;
}


##############################################################################
# ViewForm
#
##############################################################################
sub ViewForm {
	my $query = $_[0];
    my $session = $_[1];
	my $vars = { };

	my $roleid     = $session->param( 'roleid' );

	$vars = {
        forms       => 1,
		roleid	=> $roleid,
    };

    General::setHeaderVars( $vars, $session );

    $tt->process( 'ecm_html/forms.html', $vars )
            || die $tt->error(), "\n";
}

##############################################################################
# ShowAbForm
#
##############################################################################
sub ShowAbForm{
	my $query   = $_[0];
    my $session = $_[1];

	my $mode = $query->param( "mode" );
    my $roleid = $session->param( 'roleid' );

	my @lookups = ECMDBForms::getLookUps();

	my $vars = {
        roleid	=>  $roleid,
		lookups	=> \@lookups,
		tag_master => $tagmasterpath,
		scannable_bc => $scannablebarcodepath,
	};

	if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# GetBrandDetails
#
##############################################################################
sub GetBrandDetails{
    my $query   = $_[0];
    my $session = $_[1];

    my $mode = $query->param( "mode" );
    my $roleid = $session->param( 'roleid' );
	my $brandname = $query->param( "brandname" );

    my @branddetails = ECMDBForms::getBrandDetails($brandname);

    my $vars = {
        roleid  =>  $roleid,
        details => \@branddetails,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# GetAllergensUnderlined
#
##############################################################################
sub GetAllergensUnderlined{
    my $query   = $_[0];
    my $session = $_[1];
	my $allergensUnderlined = "";

    my $mode = $query->param( "mode" );
    my $roleid = $session->param( 'roleid' );
    my $brandname = $query->param( "brandname" );

    $allergensUnderlined = ECMDBForms::getAllergensUnderlined($brandname);

    my $vars = {
        roleid  =>  $roleid,
        allergens_underlined => $allergensUnderlined,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# GetPrinterDetails
#
##############################################################################
sub GetPrinterDetails {
	my $query   = $_[0];
    my $session = $_[1];

    my $mode = $query->param( "mode" );
    my $roleid = $session->param( 'roleid' );
    my $printer = $query->param( "printer" );

    my @printerdetails = ECMDBForms::getPrinterDetails($printer);

    my $vars = {
        roleid  =>  $roleid,
        details => \@printerdetails,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# GetTemplateRefDetails
#
##############################################################################
sub GetTemplateRefDetails {
	my $query   = $_[0];
    my $session = $_[1];

    my $mode = $query->param( "mode" );
    my $roleid = $session->param( 'roleid' );
    my $content = $query->param( "content" );

    my @details = ECMDBForms::getTemplateRefDetails($content);

    my $vars = {
        roleid  =>  $roleid,
        details => \@details,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# GetCutterRefDetails
#
##############################################################################
sub GetCutterRefDetails {
    my $query   = $_[0];
    my $session = $_[1];

    my $mode = $query->param( "mode" );
    my $roleid = $session->param( 'roleid' );
    my $cutter = $query->param( "content" );

    my @details = ECMDBForms::getCutterRefDetails($cutter);

    my $vars = {
        roleid  =>  $roleid,
        details => \@details,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# GetPackFormatDetails
#
##############################################################################
sub GetPackFormatDetails {
    my $query   = $_[0];
    my $session = $_[1];

    my $mode = $query->param( "mode" );
    my $roleid = $session->param( 'roleid' );
    my $content = $query->param( "content" );
	my $primaryformat = $query->param( "primaryformat" );

    my @details = ECMDBForms::getPackFormatDetails($content, $primaryformat);

    my $vars = {
        roleid  =>  $roleid,
        details => \@details,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# GetBrandOwnerDetails
#
##############################################################################
sub GetBrandOwnerDetails {
	my $query   = $_[0];
    my $session = $_[1];

    my $mode = $query->param( "mode" );
    my $roleid = $session->param( 'roleid' );
    my $name = $query->param( "name" );

    my @details = ECMDBForms::getBrandOwnerDetails($name);

    my $vars = {
        roleid  =>  $roleid,
        details => \@details,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }

}

##############################################################################
# OpenFileUpload
#
##############################################################################
sub OpenFileUpload {
	my $query   = $_[0];
    my $session = $_[1];

    my $mode = $query->param( "mode" );
    my $roleid = $session->param( 'roleid' );
    my $formid = $query->param( "formid" );
	my $jobnumber = $query->param( "jobnumber" );

    my $vars = {
        roleid  =>  $roleid,
		formid => $formid,
		jobnumber => $jobnumber,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# UploadTasDocument
#
##############################################################################
sub UploadTasDocument {
	my $query   = $_[0];
    my $session = $_[1];

    my $mode	= $query->param( "mode" );
    my $roleid	= $session->param( 'roleid' );
    my $formid	= $query->param( "formid" );
    my $jobnumber	= $query->param( "jobnumber" );
	my $userid		= $session->param( 'userid' );
	my $username	= ECMDBUser::db_getUsername( $userid );
	my $myfile		= $query->param( "formuploadfile" );
	my $message = "";
	my $result = "";
	my $tasDocumentFolder = $taspath;
	my $uploaded = "";

	my $documentName        = $query->param( "uploadDoc" );        #document Name
    $documentName           =~ s/\s+|-/_/g;

	if ( $documentName ) {
		my ( $docNamePrefix, $docExt ) = Jobs::getFileNameAndExt( $documentName );

		# my $result = copy("$upload_dir/$documentName", "$formstasgpfspath/$formid.$docExt");

		# if( $result ) {
        #	system("chmod 777 $formstasgpfspath/$formid.$docExt");
		#}

		my $ftp = Net::FTP->new( $tasserver, Debug => 0 ) or die "Cannot connect to FTP server $@";
		
		if($ftp){
            $ftp->login( $tasusername, $taspassword ) or die "Cannot login ", $ftp->message;

			if($jobnumber eq "0" || $jobnumber eq ""){
				$ftp->site( "chmod 777 $formstasgpfspath" );
				$ftp->binary();
            	$ftp->put( "$upload_dir/$documentName", "$formstasgpfspath/$formid.$docExt" ) or die "put failed for jobnumber 0, ", $ftp->message;
				my $code = $ftp->code();
				my $ftpmessage = $ftp->message;

				if($code && $code eq "226"){ # 226 - Transfer complete
                	$result = 1;
            	}

				if ( $result ) {
					$uploaded = "YES";
            		$ftp->site("chmod 0777 $formstasgpfspath/$formid.$docExt");
            		Audit::auditLog( $userid, "abinbevforms", $formid, "Forms: TAS document has been uploaded to temp path ($formid.$docExt) - $formid (Job: $jobnumber) by $username ( $userid )" );

					unlink( "$upload_dir/$documentName" );
				}
				else{
                    Audit::auditLog( $userid, "abinbevforms", $formid, "Forms: TAS document upload Failed ($jobnumber.$docExt) - $formid (Job: $jobnumber) by $username ( $userid )" );
                }
			}
			else{
				my $destRootDir = Jobs::getAssetRepositoryJobPath( $jobnumber );
				my $assetRepoJobFldr = `dirname $destRootDir`;
                chomp( $assetRepoJobFldr );

				my $dirExist = $ftp->cwd( "$destRootDir" );
				
				if ( $dirExist eq "" ) {
                    $ftp->mkdir ( "$destRootDir$tasDocumentFolder", "true") or die "Directory creation failed, $destRootDir,", $ftp->message;
                } else {
                    my $dirExist = $ftp->cwd( "$tasDocumentFolder" );
                    if ( $dirExist eq "" ) {
                        $ftp->mkdir ( "$tasDocumentFolder", "true") or die " digital Directory creation failed, $tasDocumentFolder,", $ftp->message;
                    }
                }
                $ftp->site( "chmod 777 $destRootDir" );
                $ftp->site( "chmod 777 $destRootDir$tasDocumentFolder" );
                $ftp->binary();

				# get TAS file name
				my $tasfilename = ECMDBForms::getTASFileName($formid);
				my ( $tasName, $tasExt ) = Jobs::getFileNameAndExt( $tasfilename );

				$ftp->put( "$upload_dir/$documentName", "$destRootDir$tasDocumentFolder/$tasName.$docExt" ) or die "put failed, ", $ftp->message;
                my $code = $ftp->code();
                my $ftpmessage = $ftp->message;

                if($code && $code eq "226"){ # 226 - Transfer complete
                    $result = 1;
                }

                if ( $result ) {
					$uploaded = "YES";
                    $ftp->site("chmod 0777 $destRootDir$tasDocumentFolder/$jobnumber.$docExt");
                    Audit::auditLog( $userid, "abinbevforms", $formid, "Forms: TAS document upload Success ($destRootDir$tasDocumentFolder/$tasName.$docExt) - $formid (Job: $jobnumber) by $username ( $userid )" );

                    unlink( "$upload_dir/$documentName" );
                }
				else{
					Audit::auditLog( $userid, "abinbevforms", $formid, "Forms: TAS document upload Failed ($destRootDir$tasDocumentFolder/$tasName.$docExt) - $formid (Job: $jobnumber) by $username ( $userid )" );
				}
			}
        }
	}
	else{
		$uploaded = "";
	}

    my $vars = {
        roleid  =>  $roleid,
        formid => $formid,
        jobnumber => $jobnumber,
		message => $message,
		upLoaded    => $uploaded,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# UploadTasDocumentToTemp
#
##############################################################################
sub UploadTasDocumentToTemp {

    my $query                   = $_[0];
    my $session                 = $_[1];
    my $documentName            = $query->param( "docName" );        #filename 
    $documentName               =~ s/\s+|-/_/g;
    my $documentSize            = $query->param("docSize");        #size of file
    my $documentType            = $query->param("docType");        #type of filie
    my $jobid                   = $query->param( "jobid" );
    my $mode                    = $query->param( "mode" );
	my $formid  = $query->param( "formid" );

    my ( $splitDocName, $docExt ) = Jobs::getFileNameAndExt( $documentName );
    my $bak_docName = sprintf("%s.%s", "$splitDocName", "$docExt");

    umask( 0 );

    my $upload_DocHandle = $query->upload( "doc" );

    if ( ! -e "$upload_dir" ) {
        mkpath( "$upload_dir", { mode => 0777, verbose => 0 } );
        chmod( 0777, "$upload_dir" );
    }

    open( UPLOADFILE, ">$upload_dir/$bak_docName" ) or die "$!";
    binmode UPLOADFILE;

    while ( <$upload_DocHandle> ) {
        print UPLOADFILE;
    }
    close UPLOADFILE;
    chmod ( 0777, "$upload_dir/$bak_docName" );

	# update TAS file name to DB
	my $updateStatus = ECMDBForms::updateTASFileName($jobid, $formid, $documentName);

    my $vars = {
        docUploadSucc    => "YES",
		updateStatus	=> $updateStatus,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# DeleteDocumentsFromTemp
#
##############################################################################
sub DeleteDocumentsFromTemp {

    my $query               = $_[0];
    my $session             = $_[1];
    my $documentName        = $query->param( "uploadDoc" );
	my $formid  = $query->param( "formid" );
    my $jobnumber   = $query->param( "jobnumber" );

	my $updateStatus = ECMDBForms::deleteTASFileName($formid);

    unlink( "$upload_dir/$documentName" );
}

##############################################################################
# SaveAbInbevForm
#
##############################################################################
sub SaveAbInbevForm{
    my $query   = $_[0];
    my $session = $_[1];
	my $formid = 0;

    my $mode = $query->param( "mode" );
    my $roleid = $session->param( 'roleid' );
	my $userid = $session->param( 'userid' );
	my $TagJobNo = $query->param( "TagJobNo" ) || 0;
	my $id = $query->param( "id" );
	my $updatestatus = "saved";
	my $username    = ECMDBUser::db_getUsername( $userid );

	my $insertstatus = ECMDBForms::saveAbInbevForm($query, $session, $tagmasterpath, $scannablebarcodepath);

	if( $insertstatus == 1){
		if($id eq "0"){
			$updatestatus = "saved";
			$formid = ECMDBForms::getLastFormId( $userid );
		}
		else{
			$updatestatus = "updated";
			$formid = $id;
		}

		ECMDBJob::db_recordJobAction( $formid, $userid, CTSConstants::SAVE_FORM );

		if($TagJobNo != "0"){
			Audit::auditLog( $userid, "TRACKER3PLUS", $TagJobNo, "Forms: Form $formid (Job: $TagJobNo) has been $updatestatus by $username ( $userid )" );
		}
        
		Audit::auditLog( $userid, "abinbevforms", $formid, "Forms: Form $formid has been $updatestatus by $username ( $userid )" );
	}

    my $vars = {
        roleid  =>  $roleid,
		formid => $formid,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# ListForms
#
##############################################################################
sub ListForms {
    my $query = $_[0];
    my $session = $_[1];

    my $roleid     = $session->param( 'roleid' );
	my $formid = $query->param( "formid" );

	my @forms = ECMDBForms::listForms($formid);

	my $vars = {
        roleid  =>  $roleid,
		forms => \@forms,
		formid => $formid,
    };

	General::setHeaderVars( $vars, $session );

    $tt->process( 'ecm_html/listforms.html', $vars )
            || die $tt->error(), "\n";

 #   if ( $mode eq "json")
 #   {
 #       my $json = encode_json ( $vars );
 #       print $json;
 #       return $json;
 #   }
}

##############################################################################
# EditForm
#
##############################################################################
sub EditForm {
    my $query = $_[0];
    my $session = $_[1];

    my $roleid     = $session->param( 'roleid' );
    my $mode = $query->param( "mode" );
	my $formid = $query->param( "formid" );

    my @formdata = ECMDBForms::editForm($formid);
	my @lookups = ECMDBForms::getLookUps();

    my $vars = {
        roleid  =>  $roleid,
        formid => $formid,
		formdata => \@formdata,
		lookups => \@lookups,
		tag_master => $tagmasterpath,
        scannable_bc => $scannablebarcodepath,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

############################################################################
# CommitToProduction
#
############################################################################
sub CommitToProduction{
    my $query   = $_[0];
    my $session = $_[1];

    my $mode = $query->param( "mode" ) || '';
	my $id = $query->param( "id" );
	my $TagJobNo = $query->param( "jobnumber" ) || 0;

    my $roleid = $session->param( 'roleid' );
    my $userid = $session->param( 'userid' );
	my $username = $session->param( 'username' );

	my $fromaddress = Config::getConfig('fromemail') || DEFAULT_FROM_ADDRESS;

	my @email_arr = split( ',', $abinbevprodrecipients );

	my $body = "Form $id is ready for production.\n\n " . "Click here to go to forms: ". $applicationurl ."?action=listforms&formid=$id&sidemenu=ListForms&sidesubmenu=";
	my $smtp_gateway    = Config::getConfig("smtp_gateway") || DEFAULT_SMTP_GATEWAY;

	for my $toaddress ( @email_arr ) {
		 Mailer::send_mail ( $fromaddress, $toaddress, $body, "AB InBev form ready for CMD", { host => $smtp_gateway } );
	}

	Audit::auditLog( $userid, "TRACKER3PLUS", $TagJobNo, "Forms: Form $id (Job: $TagJobNo) - commit to production has been completed by $username ( $userid )" );
	Audit::auditLog( $userid, "abinbevforms", $id, "Forms: Form $id - commit to production has been completed by $username ( $userid )" );	

    my $vars = {
        roleid  =>  $roleid,
		jobflowURL => $applicationurl,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

############################################################################
# sendXml
#
############################################################################
sub sendXml {
	my $query   = $_[0];
    my $session = $_[1];

    my $mode = $query->param( "mode" );
    my $roleid = $session->param( 'roleid' );
    my $userid = $session->param( 'userid' );
    my $id = $query->param( "id" );
	my $jobnumber = $query->param( "jobnumber" );
	my $message = "";
	my $TagJobNo = $query->param( "jobnumber" ) || 0;
	my $username    = ECMDBUser::db_getUsername( $userid );

	my $insertstatus = ECMDBForms::sendXml($jobnumber, $userid, $id);

	if($insertstatus == 1){
		$message = "XML Sent Successfully";

		Audit::auditLog( $userid, "TRACKER3PLUS", $TagJobNo, "Forms: Form $id (Job: $TagJobNo) - send XML has been initiated by $username ( $userid )" );
		Audit::auditLog( $userid, "abinbevxmlqueue", $id, "Forms: Form $id - send XML has been initiated by $username ( $userid )" );	
		Audit::auditLog( $userid, "abinbevxmlqueuehistory", $id, "Forms: Form $id - send XML has been initiated by $username ( $userid )" );
	}

	if($insertstatus == 0){
        $message = "XML is already in queue";
    }

	my $vars = {
        roleid  =>  $roleid,
		message => $message,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

#############################################################################
##############################################################################
#       end of file
##############################################################################
##############################################################################
1;
