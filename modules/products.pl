#!/usr/local/bin/perl
package Products;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use File::Path;
use File::Copy;
use LWP::Simple;
use JSON qw( decode_json );
use Net::FTP::File;
use URI::Escape;
use URI::URL;

use Template;
use Data::UUID;
use Data::Dumper;
use Date::Simple ('date', 'today');
use JSON;
use POSIX qw/strftime/;
use Time::Local;
use Encode qw(decode encode);
use Cwd;
use POSIX qw( strftime );
use Carp qw/carp/;

# Home grown
use lib 'modules';

use Validate;

require "databaseJob.pl";
require "databaseOpcos.pl";
require "databaseAudit.pl";
require "databaseCustomerUser.pl";
require "databaseUser.pl";
require 'databaseProducts.pl';
require 'databasegroups.pl';

require "config.pl";
require "Checklist.pl";
require "Watchlist.pl";
require "general.pl";
require "jobpath.pl";

our %productstatuses = (
    None		=> '0',
    Pending		=> '1',
    InProgress	=> '2',
    Paused		=> '3',
    Completed	=> '4',
);

sub elapsedToHHMMSS {
	my ( $elapsed ) = @_;

	my $timehms = "00:00:00";
	if ( defined $elapsed ) {
		my $hh =  int( $elapsed / ( 60*60 ) );
		my $mm = int( ( $elapsed / 60) % 60 );
		my $ss = int( $elapsed % 60 );
		$timehms = sprintf( "%02d:%02d:%02d", $hh, $mm, $ss );
	}
	return $timehms;
}

##########################################################################
#      isJobRepositoryEnabled - copied from jobs.pl
##########################################################################
sub isJobRepositoryEnabled {
	my $job_number	= $_[0];

	my $isEnabled	= 0;	# assume not enabled by default
 		
	# for this job - is it enabled in the customer? 
	my $customerId	= ECMDBOpcos::getCustomerId( $job_number );

	# if we have a customer
	if ( $customerId > 0 ) {
		$isEnabled =  ECMDBOpcos::customerJobRepositoryEnabled( $customerId );
	}
	return $isEnabled;

} # isJobRepositoryEnabled


##############################################################################
#   AssignProduct()
#	Assign this product to an operator
#	The operators are retrieved from the CUSTOMER attached to the LOCATION.
#	These MUST BE SET UP!!!
#
##############################################################################
sub AssignProduct {
	my ( $query, $session ) = @_;

    my $mode        = $query->param( "mode" ) ||'';
	my $jobnumber 	= $query->param("jobnumber");
	my $lineitemid 	= $query->param("lineitemid");

	my $userid		= $session->param( 'userid' );
	my $username	= $session->param( 'username' );
	my $roleid		= $session->param( 'roleid' );
    my $opcoid      = $session->param( 'opcoid' );

	my $messagecode = 0;
	my $message 	= "";
    my $oktogo  	= 1;

	my $customerid	= ECMDBOpcos::getCustomerId( $jobnumber ); # TODO not the usual way ...

	my $location	= ECMDBJob::db_getLocation($jobnumber);
    my $jobopcoid	= ECMDBJob::db_getJobOpcoid( $jobnumber );
	my @users		= ECMDBProducts::getCustomerUsersProducts( $customerid, $jobnumber, $jobopcoid, $location );

	my $mediatypeId	= Checklist::getMediatypeId( $jobnumber );

	my $tousername = "";
	my $touserid = "";

	if($roleid == 1){
		$tousername = $username;
        $touserid = $userid;
	}else{
		$tousername    = $query->param( "tousername" );
        $touserid  = ECMDBUser::db_getUserId($tousername);
	}

	# Assign Job
	my $parentopcoid    = ECMDBJob::db_getJobOpcoid( $jobnumber );
	my $ispublishing    = ECMDBOpcos::isPublishing( $parentopcoid );
	my $timetoadd       = 0;
	my $assigneeid		= ECMDBProducts::getProductAssigneeId( $jobnumber, $lineitemid );

	my $maxjobversion	= ECMDBProducts::getMaxJobVersion( $jobnumber );
	my $currentjobversion = ECMDBProducts::getCurrentJobVersion( $jobnumber, $lineitemid );
	my $productname		= ECMDBProducts::getProductName( $jobnumber, $lineitemid );

	my ($currentProductId, $currentproductName, $currentJobNo) = ECMDBProducts::userCurrenProductDetails( $touserid );
	
	# Job version Check
	if($currentjobversion < $maxjobversion){
		$oktogo = 0;
	}

#	if ($touserid == $assigneeid){
#		$oktogo = 0;
#    }

	#if(($assigneeid == $touserid) && (ECMDBJob::isJobAssignedToUser( $jobnumber, $touserid ))){
		# Do Nothing
	#}else{
	#	if( ! ECMDBProducts::isProductWorkedOnByUser( $jobnumber, $touserid )){
	#	if (! ECMDBJob::isJobAssigned( $jobnumber )) {
	#		if (! ECMDBJob::isJobBeingWorkedOn( $jobnumber ) ) {
#		if ( $oktogo == 1) {
#			ECMDBJob::assignJob( $jobnumber, $touserid, $timetoadd, $userid, $ispublishing, $lineitemid );
#			my $success = Watchlist::Watchlistnotification( $userid, $opcoid, "Job $jobnumber has been auto-assigned form Product $productname to $username by $tousername", $jobnumber, "ASSIGNED" );
#			ECMDBAudit::db_auditLog( $userid, "TRACKER3", $jobnumber, "Job $jobnumber has been auto-assigned form Product $productname to $tousername by $username" );
#		}
	#		}
	#		else{
	#			$oktogo = 0;
    #            $message = "Job has been already worked on.";
	#		}
	#	}
	#	else{
	#		if ( $assigneeid == $touserid ) {
				# Do nothing
	#		}else{
	#			$oktogo = 0;
	#			$message = "Job has been assigned to someone else.";
	#		}
	#	}
	#	}else{
    #        $oktogo = 0;
    #        $messagecode = 0;
	#		$message = "Cannot assign product - $tousername is already working on product $currentproductName from job $currentJobNo";
    #    }
	#}
	# End - Assign Job

=com
	# Take Job
	my $agencyref   = "";
    my $version = 1;
    my $currentjob = ECMDBJob::getCurrentOperatorJob( $touserid );
	my $jobopcoid   = ECMDBJob::db_getJobOpcoid( $jobnumber );
	my $gogobaby = 1;	
	$assigneeid = ECMDBJob::getAssigneeId( $jobnumber );

	if(($assigneeid == $touserid) && (ECMDBJob::isJobWorkedOnByUser( $jobnumber, $touserid ))){
        # Do Nothing
    }else{
		if( ! ECMDBProducts::isProductWorkedOnByUser( $jobnumber, $touserid )){
		if (! ECMDBJob::isJobBeingWorkedOn( $jobnumber ) ) {
			if ( $currentjob == $jobnumber ) {
        		$gogobaby   = 0;
        		$oktogo = 0;
        		$message    = "Can't take a Job that you are already working on";
    		}

    		if ( $assigneeid != $touserid ) {
        		$message    =  "You are not assigned this job: $jobnumber - hence you can't 'Take' it. Sorry.";
        		$gogobaby   = 0;
    		}
	
			if ( $gogobaby == 1 ) {
    			ECMDBJob::unWorkJob( $currentjob, $touserid );

    			if ( $jobopcoid == 11463 || $jobopcoid == 15555551 ) { # Archant
    				$agencyref = ECMDBJob::getAgencyRef( $jobnumber );
    				$version = ECMDBJob::getVersion( $jobnumber );
    			}

    			ECMDBJob::recordStop( $currentjob, $touserid );
    			ECMDBJob::setCurrentOperatorJob( $touserid, $jobnumber );
    			ECMDBJob::recordStart( $jobnumber, $jobopcoid, $customerid, $agencyref, $version, $touserid );
    			ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "$userid has auto-taken form Products $lineitemid - Job: $jobnumber and put on hold job: $currentjob" );
			}
		}else{
			$oktogo = 0;
			$message = "Job has been already worked on";
		}
		}else{
            $oktogo = 0;
            $messagecode = 0;
			$message = "Cannot assign product - $tousername is already working on product $currentproductName from job $currentJobNo";
        }
	}
	# End - Take Job
=cut

	my $hasworkedonbefore = ECMDBProducts::hasWorkedOnBefore( $lineitemid, $touserid );

	# rules/checks ... when can we NOT assign a product to a user ...
	my $currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );

	if ( $currentProductStatus == 2 ) {
		$message = "Cannot assign product while In Progress";
		$oktogo = 0;
		$messagecode = 0;
	}

	if ( $currentProductStatus == 3 ) {
		$message = "Cannot assign product while Paused";
		$oktogo = 0;
		$messagecode = 0;
	}

	#my $assigneeId = ECMDBProducts::getProductAssigneeId( $jobnumber, $lineitemid );

	#if ( $touserid != $assigneeId ) {
		# $message = "Job must be assigned to user before you can assign products";
    #    $oktogo = 0;
    #    $messagecode = 0;
	#}

	if( ECMDBProducts::isProductAssignedWorkedOn( $jobnumber, $lineitemid )){
		$oktogo = 0;
        $messagecode = 0;
		$message = "Cannot assign product. Product already worked on by someone else";
	}

	# form posted, assign the product to the user (and set product status = 1 (Pending))
	if ( $oktogo == 1) {
		# Delete previous assign
		if(ECMDBProducts::isProductAssigned( $jobnumber, $lineitemid )){
            ECMDBProducts::updateProductAssign( $assigneeid, $jobnumber, $lineitemid );
        }

		# Assign Job
		ECMDBJob::assignJob( $jobnumber, $touserid, $timetoadd, $userid, $ispublishing, $lineitemid );
        my $success = Watchlist::Watchlistnotification( $userid, $opcoid, "Job $jobnumber has been auto-assigned form Product $productname to $username by $tousername", $jobnumber, "ASSIGNED" );
        ECMDBAudit::db_auditLog( $userid, "TRACKER3", $jobnumber, "Job $jobnumber has been auto-assigned form Product $productname to $tousername by $username" );

		ECMDBProducts::assignProduct( $touserid, $productstatuses{ Pending }, $lineitemid );
		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) has been assigned to user: $tousername by $username" );
		ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $productname (Job: $jobnumber) has been assigned to user: $tousername by $username" );

		if($roleid == 1){
			# my $currentjobversion = ECMDBProducts::getCurrentJobversion( $lineitemid );
			# ECMDBProducts::updateQcStatusForAllProducts( $jobnumber, 0, $currentjobversion );  # QC Status = 0 (None)
			$message = "Product has been self assigned by $username";
		}else{
			$message = "Product has been assigned to $tousername by $username";
		}

		$messagecode = 1;
	}

	if($currentjobversion < $maxjobversion){
		$message = "Product assign failed. Select latest version of product to assign.";
	}

	if ($touserid == $assigneeid) {
		if ($touserid == $userid){
			$message = "Product already assigned to you";
		} else {
			$message = "Product already assigned to $tousername";
		}
	}

	$currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
	my $currentuserid = ECMDBProducts::getProductAssignee( $lineitemid );
	my $isenableproducts = ECMDBProducts::isProductsEnabled( $customerid );
	my @currentProductDetails = ECMDBProducts::db_currentProductDetails($jobnumber, $lineitemid);
	
	my $elapsedtime = ECMDBProducts::getElapsedTime( $lineitemid, $currentuserid );

	my $laststoppedtime;

	if ( $hasworkedonbefore > 0 ) {
		$laststoppedtime = $elapsedtime;
	} else {
		$laststoppedtime = ECMDBProducts::getLastStoppedTime( $lineitemid, $currentuserid );
	}

	my $laststoppedtimehms = elapsedToHHMMSS( $laststoppedtime );

	my @productDetails = ECMDBProducts::db_productDetails($jobnumber);
	my @comments = (ECMDBJob::getJobCommentHeaders($jobnumber, $lineitemid));
	#$jobcomment    = uri_escape_utf8( $jobcomment ) if defined $jobcomment;

	my $vars = {
		message		=> $message,
		assigneeid	=> $userid,
		userid		=> $userid,
		messagecode => $messagecode,
        	status		=> $currentProductStatus,
		lineitemid	=> $lineitemid,
		jobnumber	=> $jobnumber,
		users       => \@users,
		currentuserid => $currentuserid,
		productDetails  => \@productDetails,
		isenableproducts    => $isenableproducts,
		hasworkedonbefore => $hasworkedonbefore,
		currentProductDetails => \@currentProductDetails,
		currentProductDetails => \@currentProductDetails,
		elapsedtime => $elapsedtime,
		laststoppedtimehms => $laststoppedtimehms,
		mediatypeId => $mediatypeId,
		customerid => $customerid,
		opcoid => $opcoid,
		roleid => $roleid,
		comments => \@comments,
	};

	if ( $mode eq "json" ){
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

##############################################################################
##   UnassignProduct()
##   Unassign this product from an operator
##
##############################################################################
sub UnassignProduct {
	my ( $query, $session ) = @_;

    my $mode        = $query->param( "mode" ) || '';
    my $jobnumber   = $query->param("jobnumber");
    my $lineitemid  = $query->param("lineitemid");
    my $assigneeid  = $query->param( 'assigneeid' );

    my $userid      = $session->param( 'userid' );
    my $username	= $session->param( 'username' );
	my $roleid		= $session->param( 'roleid' );
    my $opcoid      = $session->param('opcoid');

    my $messagecode = 0;
    my $message     = "";
    my $oktogo      = 1;

    my $customerid  = ECMDBOpcos::getCustomerId( $jobnumber );
    my @users       = ECMDBCustomerUser::getCustomerUsers( $customerid );
	my $mediatypeId  = Checklist::getMediatypeId( $jobnumber );

    my $tousername  = $query->param( "tousername" );
    my $touserid    = ECMDBUser::db_getUserId($tousername);

    ECMDBProducts::unassignProduct( $lineitemid );
    ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $lineitemid (Job: $jobnumber) has been unassigned from user: $tousername by $username" );
	ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $lineitemid (Job: $jobnumber) has been unassigned from user: $tousername by $username" );
    $message = "Product has been unassigned from $tousername";
    $messagecode = 1;

    my $currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
    my $currentuserid = ECMDBProducts::getProductAssignee( $lineitemid );
	my @productDetails      = ECMDBProducts::db_productDetails($jobnumber);
    my $isenableproducts = ECMDBProducts::isProductsEnabled( $customerid );

    my $vars = {
        message     => $message,
        assigneeid  => $userid,
        userid      => $touserid,
        messagecode => $messagecode,
        status      => $currentProductStatus,
        lineitemid  => $lineitemid,
        jobnumber   => $jobnumber,
        users       => \@users,
        currentuserid => $currentuserid,
        userid => $userid,
		productDetails  => \@productDetails,
        isenableproducts    => $isenableproducts,
		mediatypeId => $mediatypeId,
        customerid => $customerid,
        opcoid => $opcoid,
		roleid => $roleid,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
#	StartProduct
#	Record Start working on a product
##############################################################################
sub StartProduct {
	my ( $query, $session ) = @_;

	my $mode        = $query->param( "mode" ) || '';
	my $lineitemid 	= $query->param("lineitemid");
	my $jobnumber 	= $query->param("jobnumber");

	my $userid		= $session->param( 'userid' );
	my $username	= $session->param( 'username' );
	my $roleid		= $session->param( 'roleid' );
	my $opcoid		= $session->param('opcoid');

	my $oktogo	= 1;
	my $message	= "Started working on task";
	my $messagecode = 0;


	my $currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
	my $location = ECMDBJob::db_getLocation($jobnumber);
	my $currentuserid = ECMDBProducts::getProductAssignee( $lineitemid );
	my $currenthouserevision = ECMDBProducts::getCurrentHouseRevision( $lineitemid );

	my $customerid  = ECMDBOpcos::getCustomerId( $jobnumber );

    my $jobopcoid   = ECMDBJob::db_getJobOpcoid( $jobnumber );
    my @users       = ECMDBProducts::getCustomerUsersProducts( $customerid, $jobnumber, $jobopcoid, $location );

	my $mediatypeId  = Checklist::getMediatypeId( $jobnumber );
	my ($currentProductId, $currentproductName, $currentJobNo) = ECMDBProducts::userCurrenProductDetails($userid);
	my $productname = ECMDBProducts::getProductName( $jobnumber, $lineitemid );

	#if( ! ECMDBProducts::isProductWorkedOnByUser( $jobnumber, $userid )){
	# Take Job
	my $agencyref   = "";
    my $version = 1;
    my $currentjob = ECMDBJob::getCurrentOperatorJob( $userid );
    my $gogobaby = 1;
    # my $assigneeid = ECMDBJob::getAssigneeId( $jobnumber );
 	my $assigneeid = ECMDBProducts::getProductAssigneeId( $jobnumber, $lineitemid );

   # if(($assigneeid == $userid) && (ECMDBProducts::isProductWorkedOnByUser( $jobnumber, $userid ))){
   # }else{
        #if( ! ECMDBProducts::isProductWorkedOnByUser( $jobnumber, $userid )){
        if (! ECMDBProducts::isProductBeingWorkedOn( $jobnumber, $lineitemid ) ) {
            if ( $currentjob == $jobnumber ) {
                $gogobaby   = 0;
                # $oktogo = 0;
                $message    = "Can't take a Job that you are already working on";
            }

            if ( $assigneeid != $userid ) {
                $message    =  "You are not assigned this job: $jobnumber - hence you can't 'Take' it. Sorry.";
                $gogobaby   = 0;
				$oktogo = 0;
            }

            if ( $gogobaby == 1 ) {
                ECMDBJob::unWorkJob( $currentjob, $userid );

                if ( $jobopcoid == 11463 || $jobopcoid == 15555551 ) { # Archant
                    $agencyref = ECMDBJob::getAgencyRef( $jobnumber );
                    $version = ECMDBJob::getVersion( $jobnumber );
                }

                ECMDBJob::recordStop( $currentjob, $userid );
                ECMDBJob::setCurrentOperatorJob( $userid, $jobnumber,$lineitemid );
                ECMDBJob::recordStart( $jobnumber, $jobopcoid, $customerid, $agencyref, $version, $userid );
                ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "$username has auto-taken Job: $jobnumber form Product $productname and put on hold job: $currentjob" );
            }
        }else{
            $oktogo = 0;
            $message = "Product has been already worked on";
        }
       # }else{
       #     $oktogo = 0;
       #     $messagecode = 0;
       #     $message = "Cannot start product - $username is already working on product $currentproductName from job $currentJobNo";
       # }
   # }
#	}
#	else{
#		$oktogo = 0;
#		$message = "Cannot start product - $username is already working on product $currentproductName from job $currentJobNo";
#	}
	# End - Take Job

	# Houserevision check
	if ( $currenthouserevision == 1 ) {
		$currenthouserevision = 1;
	}
	else{
		my $hasworkedonbefore = ECMDBProducts::hasWorkedOnBefore( $lineitemid, $userid );
		
		if($hasworkedonbefore > 0){
			$currenthouserevision = 1;
		}
	}

	# checks - reject start of product under certain conditions
	# a) can't start a product if it is in state 0 (unassigned) or In Progress (already started)
	if ( $currentProductStatus == 0 ) {
		$message = "Can't start while unassigned";
		$messagecode = 0;
		$oktogo = 0;
	}

	if ( $currentProductStatus == 2) {
		$message = "This task has already been started, can't start again until Paused or Complete";
		$messagecode = 0;
		$oktogo = 0;
	}

	my $extendedmsg = "";
	
	if ( $oktogo == 1 ) {
		if(ECMDBProducts::isProductWorkedOnByUser( $jobnumber, $userid )){
			ECMDBProducts::pauseProduct( $currentProductId, $userid );
        	ECMDBProducts::setProductStatus($productstatuses{ Paused }, $currentProductId );  # 3 = Paused
        	ECMDBProducts::updateJobAssignmentHistory($userid, $currentJobNo, $currentProductId);
        	ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $currentJobNo, "Product: $currentproductName (Job: $currentJobNo) has been paused by user: $username" );
        	ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $currentProductId, "Product: $currentproductName (Job: $currentJobNo) has been paused by user: $username" );
			$extendedmsg = "and Product $currentproductName From Job $currentJobNo has been paused";
		}

		# start recording the time on this product (and mark the product status to "InProgress")
		# ka todo
		ECMDBProducts::startProduct( $lineitemid, $userid, $jobnumber, $jobopcoid, $customerid, $location, $currenthouserevision );
		ECMDBProducts::setProductStatus($productstatuses{ InProgress }, $lineitemid );	# 2 = In Progress
		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) has been started by user: $username" );
		ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $productname (Job: $jobnumber) has been started by user: $username" );

		$message = "Product Started $extendedmsg";
		$messagecode = 1;
	}

	$currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
	my @historylist       = ECMDBProducts::viewProductHistory( $lineitemid );
	my $isenableproducts = ECMDBProducts::isProductsEnabled( $customerid );

	my @currentProductDetails = ECMDBProducts::db_currentProductDetails($jobnumber, $lineitemid);

	my $elapsedtime = 0;
	$elapsedtime = ECMDBProducts::getElapsedTime( $lineitemid, $currentuserid ) if $currentuserid;
	my $laststoppedtime = ECMDBProducts::getLastStoppedTime( $lineitemid, $currentuserid );
	my $laststoppedtimehms = elapsedToHHMMSS( $laststoppedtime );

	my @productDetails = ECMDBProducts::db_productDetails($jobnumber);
	my @comments = (ECMDBJob::getJobCommentHeaders($jobnumber, $lineitemid));

	my $vars = {
		StartProduct	=> 1,
		message			=> $message,
		messagecode		=> $messagecode,
		status			=> $currentProductStatus,
		lineitemid		=> $lineitemid,
		jobnumber		=> $jobnumber,
		users			=> \@users,
		currentuserid		=> $currentuserid,
		userid			=> $userid,
		historylist		=> \@historylist,
		productDetails		=> \@productDetails,
		isenableproducts	=> $isenableproducts,
		currentProductDetails	=> \@currentProductDetails,
		elapsedtime 		=> $elapsedtime,
		laststoppedtimehms	=> $laststoppedtimehms,
		mediatypeId		=> $mediatypeId,
		customerid		=> $customerid,
		opcoid			=> $opcoid,
		roleid			=> $roleid,
		comments		=> \@comments,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }

}

##############################################################################
# AddRevisionReason
# Update Product Restart Reason
##############################################################################
sub AddRevisionReason {
	my ( $query, $session ) = @_;

    my $mode        = $query->param( "mode" ) || '';
    my $lineitemid	= $query->param("lineitemid");
    my $jobnumber	= $query->param("jobnumber");
	
	my $restartreasonid = $query->param("restartreasonid");
	my $restartreason = $query->param("restartreason");

    my $opcoid		= $session->param('opcoid');
    my $userid		= $session->param( 'userid' );
	my $roleid		= $session->param( 'roleid' );

    my $oktogo  = 1;
    my $message = "Started working on task";
    my $messagecode = 0;

    my $currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
    my $location = ECMDBJob::db_getLocation($jobnumber);
    my $currentuserid = ECMDBProducts::getProductAssignee( $lineitemid );
    my $currenthouserevision = ECMDBProducts::getCurrentHouseRevision( $lineitemid );

    my $customerid  = ECMDBOpcos::getCustomerId( $jobnumber );

    my $jobopcoid   = ECMDBJob::db_getJobOpcoid( $jobnumber );
    my @users       = ECMDBProducts::getCustomerUsersProducts( $customerid, $jobnumber, $jobopcoid, $location );

	my $mediatypeId  = Checklist::getMediatypeId( $jobnumber );
	my $username    = ECMDBUser::db_getUsername( $userid );
	my ($currentProductId, $currentproductName, $currentJobNo) = ECMDBProducts::userCurrenProductDetails( $userid );
	my $productname = ECMDBProducts::getProductName( $jobnumber, $lineitemid );

	#if( ! ECMDBProducts::isProductWorkedOnByUser( $jobnumber, $userid )){
	# Take Job
	my $agencyref   = "";
    my $version = 1;
    my $currentjob = ECMDBJob::getCurrentOperatorJob( $userid );
    my $gogobaby = 1;
    # my $assigneeid = ECMDBJob::getAssigneeId( $jobnumber );
	my $assigneeid = ECMDBProducts::getProductAssigneeId( $jobnumber, $lineitemid );

    #if(($assigneeid == $userid) && (ECMDBJob::isJobWorkedOnByUser( $jobnumber, $userid ))){
    #}else{
        #if( ! ECMDBProducts::isProductWorkedOnByUser( $jobnumber, $userid )){
		if (! ECMDBProducts::isProductBeingWorkedOn( $jobnumber, $lineitemid ) ) {        
            if ( $currentjob == $jobnumber ) {
                $gogobaby   = 0;
                #$oktogo = 0;
                $message    = "Can't take a Job that you are already working on";
            }

            if ( $assigneeid != $userid ) {
                $message    =  "You are not assigned this job: $jobnumber - hence you can't 'Take' it. Sorry.";
                $gogobaby   = 0;
                $oktogo = 0;
            }

            if ( $gogobaby == 1 ) {
                ECMDBJob::unWorkJob( $currentjob, $userid );

                if ( $jobopcoid == 11463 || $jobopcoid == 15555551 ) { # Archant
                    $agencyref = ECMDBJob::getAgencyRef( $jobnumber );
                    $version = ECMDBJob::getVersion( $jobnumber );
                }

                ECMDBJob::recordStop( $currentjob, $userid );
                ECMDBJob::setCurrentOperatorJob( $userid, $jobnumber,$lineitemid );
                ECMDBJob::recordStart( $jobnumber, $jobopcoid, $customerid, $agencyref, $version, $userid );
                ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "$username has auto-taken Job: $jobnumber form Product $productname and put on hold job: $currentjob" );
            }
        }else{
            $oktogo = 0;
            $message = "Product has been already worked on";
        }
        #}else{
        #    $oktogo = 0;
        #    $messagecode = 0;
        #    $message = "Cannot start product - $username is already working on product $currentproductName from job $currentJobNo";
        #}
   # }
   # }
    #else{
    #    $oktogo = 0;
    #    $message = "Cannot start product - $username is already working on product $currentproductName from job $currentJobNo";
   # }
	# End - Take Job

	# Houserevision check
	if ( $currenthouserevision == 1 ) {
        $currenthouserevision = 1;
    }
    else{
        my $hasworkedonbefore = ECMDBProducts::hasWorkedOnBefore( $lineitemid, $userid );

        if($hasworkedonbefore > 0){
            $currenthouserevision = 1;
        }
    }
	
	if ( $currentProductStatus == 0 ) {
        $message = "Can't start while unassigned";
        $messagecode = 0;
        $oktogo = 0;
    }

    if ( $currentProductStatus == 2) {
        $message = "This task has already been started, can't start again until Paused or Complete";
        $messagecode = 0;
        $oktogo = 0;
    }

	my $extendedmsg = "";

    if ( $oktogo == 1 ) {
		if(ECMDBProducts::isProductWorkedOnByUser( $jobnumber, $userid )){
            ECMDBProducts::pauseProduct( $currentProductId, $userid );
            ECMDBProducts::setProductStatus($productstatuses{ Paused }, $currentProductId );  # 3 = Paused
            ECMDBProducts::updateJobAssignmentHistory($userid, $currentJobNo, $currentProductId);
            ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $currentJobNo, "Product: $currentproductName (Job: $currentJobNo) has been paused by user: $username" );
            ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $currentProductId, "Product: $currentproductName (Job: $currentJobNo) has been paused by user: $username" );
            $extendedmsg = "and Product $currentproductName From Job $currentJobNo has been paused";
        }

		ECMDBProducts::startProduct( $lineitemid, $userid, $jobnumber, $jobopcoid, $customerid, $location, $currenthouserevision );
		ECMDBProducts::addRevisionReason( $restartreasonid, $lineitemid, $userid, $jobnumber, $jobopcoid, $customerid );
        ECMDBProducts::setProductStatus($productstatuses{ InProgress }, $lineitemid );   # 2 = In Progress

		#To set Job 'QC Status' as '-' when 'QC Rejected' task is reprocessed.
		my $currProductQCStatus	= ECMDBProducts::db_getCurrentProductQCStatus( $lineitemid );
		if ( $currProductQCStatus	== 3 ) {
			ECMDBProducts::db_updateJobQcStatus( $jobnumber, 0 );
		}

		ECMDBProducts::setProductQcStatus(0, $lineitemid);
        ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) has been restarted by user: $username for this reason: $restartreason" );
        ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $productname (Job: $jobnumber) has been restarted by user: $username for this reason: $restartreason" );

        $message = "Product restarted $extendedmsg";
        $messagecode = 1;
    }

    $currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
    my @historylist       = ECMDBProducts::viewProductHistory( $lineitemid );
    my $isenableproducts = ECMDBProducts::isProductsEnabled( $customerid );
	
	my @currentProductDetails = ECMDBProducts::db_currentProductDetails($jobnumber, $lineitemid);
	my $elapsedtime = ECMDBProducts::getElapsedTime( $lineitemid, $currentuserid );

	my $laststoppedtime = ECMDBProducts::getLastStoppedTime( $lineitemid, $currentuserid );
	my $laststoppedtimehms = elapsedToHHMMSS( $laststoppedtime );

	my @productDetails = ECMDBProducts::db_productDetails($jobnumber);
	my @comments = (ECMDBJob::getJobCommentHeaders($jobnumber, $lineitemid));

    	my $vars = {
	StartProduct    => 1,
        message         => $message,
        messagecode     => $messagecode,
        status          => $currentProductStatus,
        lineitemid      => $lineitemid,
        jobnumber       => $jobnumber,
        users           => \@users,
        currentuserid => $currentuserid,
        userid => $userid,
        historylist => \@historylist,
        productDetails  => \@productDetails,
        isenableproducts    => $isenableproducts,
		currentProductDetails => \@currentProductDetails,
        elapsedtime => $elapsedtime,
        laststoppedtimehms => $laststoppedtimehms,
		mediatypeId => $mediatypeId,
        customerid => $customerid,
        opcoid => $opcoid,
		roleid => $roleid,
		comments => \@comments,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# GetProductRestartReasons
# Get product reasons
##############################################################################
sub GetProductRestartReasons {
	my ( $query, $session ) = @_;

    my $mode        = $query->param( "mode" ) || '';
    my $lineitemid  = $query->param("lineitemid");
    my $jobnumber   = $query->param("jobnumber");

    my $userid      = $session->param( 'userid' );

    my $customerid  = ECMDBJob::db_getCustomerIdJob( $jobnumber );
    my $currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
    my $currentuserid = ECMDBProducts::getProductAssignee( $lineitemid );
    my @productDetails      = ECMDBProducts::db_productDetails($jobnumber);
    my $isenableproducts = ECMDBProducts::isProductsEnabled( $customerid );

	my @productReasons = ECMDBProducts::getProductRestartReasons();

	my $vars = {
        GetProductRestartReasons    => 1,
        status          => $currentProductStatus,
        lineitemid      => $lineitemid,
        jobnumber       => $jobnumber,
        currentuserid => $currentuserid,
        userid => $userid,
        productDetails  => \@productDetails,
        isenableproducts    => $isenableproducts,
		productReasons => \@productReasons,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
# finishProductAssignToDept
#
##############################################################################
sub finishProductAssignToDept {
	my ( $query, $session ) = @_;

    my $mode		= $query->param( "mode" ) || '';
    my $lineitemid	= $query->param("lineitemid");
    my $jobnumber	= $query->param("jobnumber");

    my $userid		= $session->param( 'userid' );
	my $roleid		= $session->param( 'roleid' );
	my $opcoid      = $session->param( 'opcoid' );

    my $customerid  = ECMDBJob::db_getCustomerIdJob( $jobnumber );
    my $currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
    my $currentuserid = ECMDBProducts::getProductAssignee( $lineitemid );
    my @productDetails      = ECMDBProducts::db_latestProductDetails($jobnumber);
    my $isenableproducts = ECMDBProducts::isProductsEnabled( $customerid );

    my @departments = ECMDBOpcos::db_getdepartments();
	my @teams = ECMDBOpcos::db_getteams();
	my @rejectreasons = ECMDBProducts::getRejectReasons();
	my @advisory      = ECMDBOpcos::db_getquerytypes($opcoid);

    my $vars = {
        finishProductAssignToDept    => 1,
        status          => $currentProductStatus,
        lineitemid      => $lineitemid,
        jobnumber       => $jobnumber,
        currentuserid => $currentuserid,
        userid => $userid,
        productDetails  => \@productDetails,
        isenableproducts    => $isenableproducts,
        departments => \@departments,
		teams => \@teams,
		rejectreasons => \@rejectreasons,
		roleid => $roleid,
		advisory => \@advisory,
    };
    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
#   PauseProduct()
# 	User has paused this porduct to do something else. He is STILL assigned to it
# 	becasue he will (May) come back to it later.
##############################################################################
sub PauseProduct {
	my ( $query, $session ) = @_;

	my $mode        = $query->param( "mode" ) || '';
    my $lineitemid  = $query->param("lineitemid");
    my $jobnumber   = $query->param("jobnumber");

    my $userid		= $session->param( 'userid' );
	my $username	= $session->param( 'username' );
	my $roleid     = $session->param( 'roleid' );
    my $opcoid		= $session->param('opcoid');

	my $oktogo		= 1;
	my $message		= "Work on this Task Paused";
	my $messagecode = 0;

    my $currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
    my $location = ECMDBJob::db_getLocation($jobnumber);
	my $currentuserid = ECMDBProducts::getProductAssignee( $lineitemid );
	my $productname = ECMDBProducts::getProductName( $jobnumber, $lineitemid );

	my $customerid  = ECMDBOpcos::getCustomerId( $jobnumber );

    my $jobopcoid   = ECMDBJob::db_getJobOpcoid( $jobnumber );
    my @users       = ECMDBProducts::getCustomerUsersProducts( $customerid, $jobnumber, $jobopcoid, $location );

	my $mediatypeId  = Checklist::getMediatypeId( $jobnumber );	

	# checks - can ONLY pause a product if it is in "In Progress" 
	if ( $currentProductStatus != 2 ) {
		$message = "Task $lineitemid is not in 'In Progress' state, cannot pause (sorry)";
		$messagecode = 0;
		$oktogo = 0;
	}

	if ( $oktogo == 1 ) { 	# we can pause it
		ECMDBProducts::pauseProduct( $lineitemid, $userid );
		ECMDBProducts::setProductStatus($productstatuses{ Paused }, $lineitemid );	# 3 = Paused
		ECMDBProducts::updateJobAssignmentHistory($userid, $jobnumber, $lineitemid);
		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) has been paused by user: $username" );
		ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $productname (Job: $jobnumber) has been paused by user: $username" );
		$messagecode = 1;
		$message = "Product Paused";
	}

	$currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
	my @historylist       = ECMDBProducts::viewProductHistory( $lineitemid );
	my $isenableproducts = ECMDBProducts::isProductsEnabled( $customerid );

	my @currentProductDetails = ECMDBProducts::db_currentProductDetails($jobnumber, $lineitemid);
	my $elapsedtime = ECMDBProducts::getElapsedTime( $lineitemid, $currentuserid );

	my $laststoppedtime = ECMDBProducts::getLastStoppedTime( $lineitemid, $currentuserid );
	my $laststoppedtimehms = elapsedToHHMMSS( $laststoppedtime );

	my @productDetails = ECMDBProducts::db_productDetails($jobnumber);
	my @comments = (ECMDBJob::getJobCommentHeaders($jobnumber, $lineitemid));

	my $vars = {
        PauseProduct => 1,
        message => $message,
        messagecode => $messagecode,
		status => $currentProductStatus,
		lineitemid => $lineitemid,
		jobnumber => $jobnumber,
		users       => \@users,
		currentuserid => $currentuserid,
		userid => $userid,
		historylist => \@historylist,
		productDetails  => \@productDetails,
		isenableproducts    => $isenableproducts,
		currentProductDetails => \@currentProductDetails,
		elapsedtime => $elapsedtime,
		laststoppedtimehms => $laststoppedtimehms,
		mediatypeId => $mediatypeId,
		customerid => $customerid,
		opcoid => $opcoid,
		roleid => $roleid,
		comments => \@comments,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

# ka todo : StopProduct
##############################################################################
#   StopProduct()
# 	stop the clock AND unassign - thats one round of edits done.
##############################################################################
sub StopProduct {
	my ( $query, $session ) = @_;

	my $mode        = $query->param( "mode" ) ||'';
    my $lineitemid  = $query->param("lineitemid");
    my $jobnumber   = $query->param("jobnumber");
	my $deptid      = $query->param( 'deptid' ) || 0;
    my $depname     = $query->param( 'depname' );
	my $teamname    = $query->param( 'teamname' );
	my $teamid		= $query->param( 'teamid' ) || 0;
	my $assigntoqc	= $query->param( 'assigntoqc' ) || 0;
	my $advisoryid	= $query->param( 'advisoryid' );
	my $advisoryname= $query->param( 'advisoryname' );
	my $qcopcoid	= 0;
	my $lastlocationid = 0;
	my $lastlocation_name = "";

    my $opcoid		= $session->param('opcoid');
	my $userid		= $session->param( 'userid' );
	my $username	= $session->param( 'username' );
	my $roleid		= $session->param( 'roleid' );

	my $oktogo	= 1;
	my $message	= "Work on this Task Paused";
	my $messagecode = 0;

	my $jobopcoid = ECMDBJob::db_getJobOpcoid( $jobnumber );
    my $currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
    my $location = ECMDBJob::db_getLocation($jobnumber);
	my $currentuserid = ECMDBProducts::getProductAssignee( $lineitemid );

	my $customerid  = ECMDBOpcos::getCustomerId( $jobnumber );
    my @users       = ECMDBProducts::getCustomerUsersProducts( $customerid, $jobnumber, $jobopcoid, $location );

	my $mediatypeId  = Checklist::getMediatypeId( $jobnumber );

	my $productname = ECMDBProducts::getProductName( $jobnumber, $lineitemid );

	my $rejectreasonid	= $query->param( 'rejectreasonid' ) || 0;
    my $rejectreason	= $query->param( 'rejectreason' );
	my $type			= $query->param( 'type' );
	my $actiontype		= $query->param( 'actiontype' );

	my @selectedproducts = split( ',', $query->param( "selectedproducts" )||'' );

	# checks - can ONLY pause a product if it is in "In Progress" 
	if ( $currentProductStatus != 2 ) {
		$message = "Task $lineitemid is not in 'In Progress' state, cannot stop (sorry)";
		$oktogo = 0;
		$messagecode = 0;
	}

	if ( $currentProductStatus == 3 ) {
		$oktogo = 1;
	}

	if ( $oktogo == 1 ) { 	# we can pause it
		ECMDBProducts::stopProduct( $lineitemid, $userid );
		ECMDBProducts::setProductStatus( $productstatuses{ Completed }, $lineitemid );	# 4 = Completed

		# update QC status of a product if it is completed by operator
		if( $roleid == 1 ){
			ECMDBProducts::setProductQcStatus(1, $lineitemid);
		}

		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) has been stopped by user: $username" );
		ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $productname (Job: $jobnumber) has been stopped by user: $username" );

		# Unassign user from the Job
		# ECMDBJob::unassignJob( $jobnumber );

		# Assign to department
		my $reposync = 0;

		#if($type =~ /^DEPARTMENT$/){
			ECMDBGroups::db_deptstatusupdate( $deptid, $jobnumber );
			ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $productname (Job: $jobnumber) was put into department $depname by user: $username" );
			ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) was put into department $depname by user: $username" );
		#}

		# Assign to Team
		ECMDBGroups::db_teamstatusupdate( $teamid, $jobnumber );
        ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $productname (Job: $jobnumber) was put into team $teamname by user: $username" );
        ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) was put into team $teamname by user: $username" );

		#Apply to advisory
		ECMDBGroups::db_updatequerytype( $advisoryid, $jobnumber );
		ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $productname (Job: $jobnumber) advisory has been set to $advisoryname by user: $username" );
        ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) advisory has been set to $advisoryname by user: $username" );


		#if($type =~ /^JOBPOOL$/){
		#	ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $productname (Job: $jobnumber) was put into job pool by user: $username" );
		#	ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) was put into job pool by user: $username" );
		#}

		# Assign to QC monitor
		$qcopcoid = ECMDBProducts::getQcOpcoid();
		if($assigntoqc && ( $assigntoqc == 1 )){
			# get QC opcoid

			# update job location and last location
			ECMDBProducts::updateJobLocation( $jobnumber, $qcopcoid );
			ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) has been moved to QC Monitor by user: $username" );
			ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $productname (Job: $jobnumber) has been moved to QC Monitor by user: $username" );
		}

		if($actiontype =~ /^QCREJECT$/){
			for my $selectedproduct ( @selectedproducts ) {
				my $selproductname = ECMDBProducts::getProductName( $jobnumber, $selectedproduct );

				ECMDBProducts::updateRejectReason( $selectedproduct, $userid, $rejectreasonid );
				ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $selectedproduct, "Product: $selproductname (Job: $jobnumber) was QC REJECTED by user: $username for reason: $rejectreason" );
				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $selproductname (Job: $jobnumber) was QC REJECTED by user: $username for reason: $rejectreason" );

				ECMDBProducts::updateQcStatus( $selectedproduct, 3 );  # QC Status = 3 (Decline)
			}
			$reposync = 1;
		}


		my $qcapprovecount = 0;
		if($actiontype =~ /^QCAPPROVE$/){
			for my $selectedproduct ( @selectedproducts ) {
				$qcapprovecount += 1;
				my $selproductname = ECMDBProducts::getProductName( $jobnumber, $selectedproduct );

            	ECMDBProducts::updateQcStatus( $selectedproduct, 2 );  # QC Status = 2 (Approve)
            	ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $selectedproduct, "Product: $selproductname(Job: $jobnumber) was QC APPROVED by user: $username" );
				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $selproductname(Job: $jobnumber) was QC APPROVED by user: $username" );
			}
        }

		# Remove job from QC monitor and revert to previous location
		if ( ( $qcopcoid == $opcoid ) && ( ( $actiontype eq 'QCREJECT' ) || ( $actiontype eq 'QCAPPROVE' ) ) ){
			$lastlocationid = ECMDBProducts::getJobLastLocation( $jobnumber );
			my $lastlocation_name = '';
			$lastlocation_name = ECMDBOpcos::getOpcoName( $lastlocationid ) if ( ( defined $lastlocationid ) && ( $lastlocationid != 0 ) );
			if ( ( $lastlocation_name ne '' ) && ( $lastlocation_name ne 'QC_MONITOR' ) )  {
				ECMDBProducts::updateJobLocation( $jobnumber, $lastlocationid );
				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) moved from QC Monitor to $lastlocation_name by user: $username" );
				ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "Product: $productname (Job: $jobnumber) moved from QC Monitor to $lastlocation_name by user: $username" );
			}
		}

		#Update Job QC status
		my @jobProdQCStatus_array_ref	= ECMDBProducts::db_getJobProductQCStatus( $jobnumber );
		if ( scalar @jobProdQCStatus_array_ref ) {
			my %distinctJobProdQCStatus;
			foreach my $row ( @jobProdQCStatus_array_ref ) {
				$distinctJobProdQCStatus{$row->{qcstatus}} =	1;
			}

			if ( exists $distinctJobProdQCStatus{1} ) { #First priority to 'QC Required'
				ECMDBProducts::db_updateJobQcStatus($jobnumber, 1);
			} elsif ( exists $distinctJobProdQCStatus{3} ) { #Second priority to 'QC Reject'
				ECMDBProducts::db_updateJobQcStatus($jobnumber, 3);
			} elsif ( exists $distinctJobProdQCStatus{2} ) { #Next priority to 'QC Approved'
				ECMDBProducts::db_updateJobQcStatus($jobnumber, 2);
			} else {
				ECMDBProducts::db_updateJobQcStatus($jobnumber, 0);
			}
		}

		# Sync job to repository
		if($reposync == 0){			# On QC Reject no need to send job for repository sync Q
			if ( isJobRepositoryEnabled( $jobnumber ) && ECMDBOpcos::db_isRepoSyncEnabled (ECMDBOpcos::getCustomerId( $jobnumber )) ) {
        		ECMDBJob::requestSync( $jobnumber, ECMDBOpcos::getCustomerId( $jobnumber ), $jobopcoid, $userid);  # send custid also for better join
				ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $lineitemid, "(Job: $jobnumber) added to sync Q by completion of product: $productname by user: $username" );
				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "(Job: $jobnumber) added to sync Q by completion of product: $productname by user: $username" );
    		}
		}

		# Complete Job
		ECMDBJob::completeJob( $jobnumber, $userid, $lineitemid );
		$messagecode = 1;
        $message = "Product stopped";
	}

	$currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
	my @historylist       = ECMDBProducts::viewProductHistory( $lineitemid );
	my $isenableproducts = ECMDBProducts::isProductsEnabled( $customerid );

	my @currentProductDetails = ECMDBProducts::db_currentProductDetails($jobnumber, $lineitemid);
	my $elapsedtime = ECMDBProducts::getElapsedTime( $lineitemid, $currentuserid );

	my $laststoppedtime = ECMDBProducts::getLastStoppedTime( $lineitemid, $currentuserid );
	my $laststoppedtimehms = elapsedToHHMMSS( $laststoppedtime );

	my @productDetails = ECMDBProducts::db_productDetails($jobnumber);
	my @comments = (ECMDBJob::getJobCommentHeaders($jobnumber, $lineitemid));

    my $vars = {
        StopProduct => 1,
        message => $message,
        messagecode => $messagecode,
        status => $currentProductStatus,
        lineitemid => $lineitemid,
        jobnumber => $jobnumber,
		users       => \@users,
		currentuserid => $currentuserid,
        userid => $userid,
		historylist => \@historylist,
		productDetails  => \@productDetails,
        isenableproducts    => $isenableproducts,
		currentProductDetails => \@currentProductDetails,
        elapsedtime => $elapsedtime,
        laststoppedtimehms => $laststoppedtimehms,
		mediatypeId => $mediatypeId,
        customerid => $customerid,
        opcoid => $opcoid,
		roleid => $roleid,
		comments => \@comments,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
##   ViewProductHistory()
##   
##############################################################################
sub ViewProductHistory {
	my ( $query, $session ) = @_;

    my $message = "View History";
    my $messagecode = 0;

    my $mode        = $query->param( "mode" ) ||'';
    my $lineitemid  = $query->param("lineitemid");
    my $jobnumber   = $query->param("jobnumber");

	my $userid      = $session->param( 'userid' );
	my $opcoid      = $session->param('opcoid');

    my $customerid  = ECMDBOpcos::getCustomerId( $jobnumber );
    my @users       = ECMDBCustomerUser::getCustomerUsers( $customerid );
	my $mediatypeId  = Checklist::getMediatypeId( $jobnumber );

	my @historylist       = ECMDBProducts::viewProductHistory( $lineitemid );
	
	my $currentProductStatus = ECMDBProducts::getProductStatus( $lineitemid );
	my $currentuserid = ECMDBProducts::getProductAssignee( $lineitemid );

    my $vars = {
        ViewProductHistory => 1,
        message => $message,
        messagecode => $messagecode,
        status => $currentProductStatus,
        lineitemid => $lineitemid,
        jobnumber => $jobnumber,
        users       => \@users,
		historylist => \@historylist,
        currentuserid => $currentuserid,
        userid => $userid,
		mediatypeId => $mediatypeId,
        customerid => $customerid,
        opcoid => $opcoid,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##########################################################################
# viewTimeCapture
#
##########################################################################
sub viewTimeCapture{
	my ( $query, $session ) = @_;

    my $mode		= $query->param( "mode" ) || '';
    my $jobnumber	= $query->param("jobnumber");

	my @timecapturedata = ECMDBProducts::ViewTimeCapture( $jobnumber );

	my $vars = {
        jobnumber       => $jobnumber,
        timecapturedata => \@timecapturedata,
    };
    if ( $mode eq "json") {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
} #viewTimeCapture

##########################################################################
# getLatestVersionProducts
#
##########################################################################
sub getLatestVersionProducts {
	my ( $query, $session ) = @_;
	my $vars;

	my $mode        = $query->param( "mode" ) || '';
	my $jobnumber   = $query->param("jobnumber");
	my $roleid      = $session->param( 'roleid' );

	eval {
		my ( $jobPath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobnumber );
		my @productDetails = ECMDBProducts::db_getLatestVersionProducts($jobnumber);
		my $isenableproducts = ECMDBProducts::isProductsEnabled( $customerid );
		my $task_level  =   ECMDBProducts::checkJobLevel( $jobnumber );
		$vars = {
			roleid			=> $roleid,
			jobnumber       => $jobnumber,
			productDetails  => \@productDetails,
			isenableproducts    => $isenableproducts,
			tasklevel		=> $task_level
		};
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => "getLatestVersionProducts: ".$error };
	}

	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

##########################################################################
# excludeProduct
#
##########################################################################
sub excludeProduct{
	my ( $query, $session ) = @_;
	my $vars;
	my $eye_state = "excluded";

	my $mode        = $query->param( "mode" ) || '';
	my $jobnumber   = $query->param("jobnumber");
	my $productid	= $query->param("productid");
	my $isactive_to_update = $query->param("isactive_to_update");
	
	my $userid      = $session->param( 'userid' );
	my $username    = $session->param( 'username' );
	my $roleid      = $session->param( 'roleid' );

	eval {	
		my $productname = ECMDBProducts::getProductName( $jobnumber, $productid );

		if($isactive_to_update eq "1"){
			$eye_state = "included";
		}

		my $laststate = ECMDBProducts::db_getLastStateOfProduct( $productid );

		if($laststate eq $isactive_to_update){
			my $error = "Product($productname) already $eye_state, Job: $jobnumber";
			$vars = {
				error => $error,
			}
		}
		else{
			my $taskstate = ECMDBProducts::db_updateTaskState($productid, $isactive_to_update);

			if($taskstate){
				ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $jobnumber, "Product: $productname (Job: $jobnumber) has been $eye_state by user: $username" );
				ECMDBAudit::db_auditLog( $userid, "PRODUCTS", $productid, "Product: $productname (Job: $jobnumber) has been $eye_state by user: $username" );
			}

			my ( $jobPath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobnumber );
			my @productDetails = ECMDBProducts::db_getLatestVersionProducts($jobnumber);
			my $isenableproducts = ECMDBProducts::isProductsEnabled( $customerid );

			$vars = {
				roleid			=> $roleid,
				jobnumber       => $jobnumber,
				productDetails  => \@productDetails,
				isenableproducts    => $isenableproducts,
			};
		}
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		die "excludeProduct: ".$error;
	}

	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

1;
