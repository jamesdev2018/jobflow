#!/usr/local/bin/perl
package ECMDBJoblist;

use strict;
use warnings;

# use Switch; # doesnt use switch statement
use Scalar::Util qw( looks_like_number );
use Carp qw/carp cluck/;
use Time::HiRes qw/time/;
use Data::Dumper;

# Home grown

use lib 'modules';

use Validate;

require "databaseSearch.pl";		# checkSearchTypeAndContents
require "databaseCustomerUser.pl";	# getUserCustomerIdsByOpco
require "filters.pl";			# filter_where_clause, matchCondition and getMatchConditionSymbols

require "config.pl";			# getConfig
# eg  my $rowscount		= Config::getConfig("jobrowslimit") || 1000;

use constant DEFAULT_ORDERBY => 't3p.duedate desc'; 

use constant OPCO_MULTIPLE_LOCATIONS => 17;

use constant DEFAULT_MYJOBS_ORDERBY => 'CASE ' .
			" WHEN t3p.operatorstatus = 'W' THEN 0 " .
			" WHEN t3p.operatorstatus = 'A' THEN 1 " .
			" WHEN t3p.operatorstatus = 'C' THEN 2 " .
			' ELSE 3 END, t3p.duedate DESC';

our @column_list = (
	't.job_number', 't.agency', 't.client', 't.project', 't.publication', 't.pathtype', 't.agencyref', 't.HEADLINE', 't.DESCRIPTION', 
	't.businessunitref', 't.version', 
	't.formatsize AS mediatype', 'trim(t.formatsize) as formatsize', 't.SPLITNUMBER', 't.mediachoiceid', 
	#'STR_TO_DATE(t.MATDATE,\'%d/%m/%Y\') AS MATDATE', 
	't.MATDATE AS MATDATE',
	't.workflow AS actiontype','t.requiredDate', 't.bookedby', 't.accounthandler', 't.productionstatus', 't.producer', 't.worktype as worktype', 't.duration as timelength',
	't3p.opcoid AS myopcoid',
	't3p.DEPARTMENTVALUE', 't3p.TEAMVALUE', 't3p.rec_timestamp AS coverdate', 't3p.preflightstatus AS preflightid', 't3p.PREFLIGHTDATE', 't3p.compareready', 
	't3p.assetscreated', 't3p.location AS remoteopcoid', 't3p.checkoutstatus AS checkoutstatusid', 't3p.dispatchdate', 't3p.qcstatus AS qcstatusid', 
	"COALESCE(t3p.duedate, '2000-01-01 00:00:00') AS duedate",
	'ROUND(TIME_TO_SEC(TIMEDIFF(t3p.duedate, now())) / 60) AS timeleft', 
	't3p.operatorstatus', 't3p.creative AS isCreative', 't3p.duedate < now() AS isOverdue',
	't3p.lastsynced', 't3p.estimate', 't3p.adgatestatus', 't3p.status as jobstatus',
	'IF(t3p.location=17, 1, 0) AS multiple_locations',
	'IF(t3p.status=6, 1, 0) AS isJobArchived',
	'acm.advisory_colour', 
	'a.standard as audiostd',
	'btp.ORDERDATE',  'btp.DELIVERYDATE', 'btp.scheduledate', 'btp.EDITCODE as editcode', 'btp.SMOKECMD AS status_note',
	'ct.country',
	'cs.status AS checkoutstatus', 
	'dp.name', 
	'op.opconame AS opcompany', 'op2.opconame AS location',
	'os.status as op_status',
	'pfs.status AS preflight',
	'pr.name as producerresponsible',
	'qc.status AS qcstatus', 
	'qt.querytype', 
	'tp.name AS teamname', 
	'u.username AS assignee',' u.id AS userid',
	'v.standard as videostd',
); 

our @inner_joins = ( 
		# INNER JOIN tracker3plus t3p ON t3p.job_number=temp_table.job_number
		'INNER JOIN tracker3 t ON t.job_number=t3p.job_number',
		'INNER JOIN operatingcompany op ON op.opcoid=t3p.opcoid',
		'INNER JOIN operatingcompany op2 ON op2.opcoid=t3p.location',
		'INNER JOIN checkoutstatus cs ON cs.id=t3p.checkoutstatus',
		'INNER JOIN preflightstatus pfs ON pfs.id=t3p.preflightstatus',
		'INNER JOIN qcstatus qc ON qc.id=t3p.qcstatus',
		'INNER JOIN departments dp ON dp.id=t3p.DEPARTMENTVALUE',
		'INNER JOIN teams tp ON tp.id=t3p.TEAMVALUE',
		'INNER JOIN querytypes qt ON qt.id = t3p.QUERYTYPE',
		'LEFT OUTER JOIN OPERATORSTATUS os ON os.id=t3p.operatorstatus',
		'LEFT OUTER JOIN advisory_colour_mapping acm ON acm.opcoid = t3p.opcoid AND acm.advisory_id=t3p.QUERYTYPE',
		'LEFT OUTER JOIN Broadcasttracker3 btp ON t.job_number = btp.job_number',
		'LEFT OUTER JOIN producerresponsible pr ON btp.PRODUCERRESP=pr.id',
		'LEFT OUTER JOIN countries ct ON btp.country=ct.id',
		'LEFT OUTER JOIN users u ON u.id=t3p.assignee',
		'LEFT OUTER JOIN videostandard v ON btp.VIDEOSTD=v.id',
		'LEFT OUTER JOIN audiostandard a ON btp.AUDIOSTD=a.id' 
	);

our @archant_columns = (
	'ac.description',
	'dc.styledesc',
);

our @archant_joins = (
		'LEFT OUTER JOIN archantclassification ac ON t.pubcollect=ac.classification',
		'LEFT OUTER JOIN designclassification dc ON t.collectproof=dc.styleid'
	);

# CASE used to handle the proper timestamp when it comes from CMD once the system is updated as well as current simple date format.
our @kanban_traffic_columns = (
"CASE
   WHEN t.matdate IS NULL OR t.matdate = 'UNKNOWN' THEN NULL
   WHEN POSITION('/' IN t.matdate) = '5' THEN DATE_FORMAT(TIMESTAMP (REPLACE (t.matdate,'/','-')), '%Y-%m-%d %H:%i')
   WHEN INSTR (t.matdate, '/') THEN DATE_FORMAT(TIMESTAMP (STR_TO_DATE(t.matdate, '%d/%m/%Y')), '%Y-%m-%d %H:%i')
   ELSE DATE_FORMAT(TIMESTAMP( t.matdate ), '%Y-%m-%d %H:%i')
END as matdate_datetime"
);

our $order_clauses = {
	0 =>  't3p.duedate', 
	1 => 't3p.rec_timestamp', 
	2 => 't3p.duedate', 
	3 => 't3p.job_number', 
	4 => 'u.username', 
	5 => 't.agency',
	6 => 'location', 
	7 => 't3p.qcstatus', 
	8 => 't.agencyref',
	9 => 't.project', 
	10 => 'op.opconame',
	11 => 't3p.operatorstatus',
	12 => 't.requiredDate', 
	13 => 't.accounthandler', 
	14 => 't.bookedby', 
	15 => 't.productionstatus',
	16 => 't3p.departmentvalue', 
	17 => 't3p.teamvalue', 
	18 => 't.formatsize',       # $archantChk ? 't.workflow' : 't.formatsize', 
	19 => 'qt.querytype', 
	20 => 't.mediachoiceid',
	21 => 'ct.country',
	22 => 't.producer', 
	23 => "STR_TO_DATE(t.MATDATE,\'%d/%m/%Y\')",
	24 => 'btp.scheduledate',
	25 => 't3p.dispatchdate',
	26 => 't.formatsize',
	27 => 't.PUBLICATION',
	98 => 't.project desc, t3p.QUERYTYPE ',
	99 => 't.mediachoiceid desc, t3p.QUERYTYPE '
};

# Read contents of Order By table (eg to draw Sort Menu), used by Dashboard_page and DAshboard_refdata
# So for the former, we are only interested in those with cm.sortmenu == 1 but in the latter
# we also want to include the exotics, group by Media id, Project which can also appear on the Sort menu
# if so configured, but should be included irrespectively.

sub getSortByList {
	my ( $opflags, $roleid, $rolename, $relaxroles ) = @_;

	my $symbol_table = Filters::getMatchConditionSymbols( $opflags, $roleid, $rolename, $relaxroles );

	my $rows = $Config::dba->hashed_process_sql(
		"SELECT cm.column_id, cm.column_label, cm.column_name, cm.orderbyid, cm.group_header_label, cm.condition, cm.type, cm.default_val, cm.null_equiv, cm.sortmenu, cm.orderbyid " .
		" FROM column_mappings cm " .
		" WHERE (cm.orderbyid > 0) ORDER BY cm.column_label ASC" );
	my @result;

	# DONT include rows where condition is defined but does NOT match, please.

	if ( defined $rows ) {
		foreach my $row ( @$rows ) {
			my $condition = $row->{condition};
			if ( ( defined $condition ) && ( $condition ne '' ) ) {
				my $match = Filters::matchCondition( $condition, $symbol_table );
				next unless $match == 1;
			}
			push( @result, $row );	
		}
	}
	return @result;
}


# Convert orderby/sortby parameters into a clause for use in SQL, called by db_getMyJobs etc
# Inputs
# - orderby - integer 1..27 + 98, 99
# - sortby - text, asc or desc for use if the corresponding orderby lookup does not supply asc or desc
# - vars, and hence access to opflags, roleid, rolename, relaxroles
# Returns
# clause for appending to "ORDER BY " (can be undef in some cases)

sub getOrderBy {
	my ( $orderbyid, $sortby, $vars, $opflags ) = @_; 

	my $orderClause; 
	if ( ( defined $orderbyid ) && ( looks_like_number( $orderbyid ) ) ) {

		my $symbol_table = Filters::getMatchConditionSymbols( $opflags, $vars->{roleid}, $vars->{rolename}, $vars->{relaxroles} );

		my $orderClauseList = $Config::dba->hashed_process_sql(
			"SELECT cm.orderclause, cm.condition FROM column_mappings cm WHERE cm.orderbyid = ?", [ $orderbyid ] ) || [];
		if ( defined $orderClauseList ) {
			foreach my $row ( @{ $orderClauseList } ) {
				# If there are conditions, make sure they match before we use the clause
				if ( defined $row->{condition} && $row->{condition} ne '' ) {
					my $match = Filters::matchCondition( $row->{condition}, $symbol_table );
					next unless $match == 1;
				}
				$orderClause = $row->{orderclause};
			}
		}

		# Add appropriate asc/desc if missing from order clause(s)
		if ( defined $orderClause ) {
			my @list = split /,/, $orderClause;
			foreach my $entry ( @list ) {
				if ( $entry !~ /(asc|desc)$/ ) {
					$entry .= ' ' . $sortby;
				}
			}
			$orderClause = join( ', ', @list );
		}

	}
	return $orderClause;
}

# Query table column_mappings joined with usersettings (column manager settings)
# with condition (n column_mappings) evaluated using supplied flags ...

sub getVisibleColumns {
	my ( $userid, $opcoid, $symbol_table ) = @_;

	my %columns;

	my $column_mapping_rows = $Config::dba->hashed_process_sql(
			"SELECT cm.column_id, cm.column_label, cm.column_name, cm.condition, cm.type, cm.default_val, cm.null_equiv, cm.sortmenu, cm.orderbyid, cm.orderclause, " .
				" 0 as visibility, 0 as sort_order " .
			" FROM column_mappings cm " .
			" WHERE (cm.column_name IS NOT NULL) AND (cm.column_name <> '') " );
	die "No columns found" unless defined $column_mapping_rows;

	foreach my $row ( @$column_mapping_rows ) {
		my $condition = $row->{condition};
		if ( ( defined $condition ) && ( $condition ne '' ) ) {
			my $match = Filters::matchCondition( $condition, $symbol_table );
			# warn "Column " . $row->{column_id} . " label " . $row->{column_label} . " condition " . $condition . " match $match";
			next unless $match == 1;
		}
		$columns{ $row->{column_id} } = $row; # Create hash of columns, with column_id as key
	}
	#warn "Count of rows from column_mappings " . scalar( @$column_mapping_rows );

	die "Userid is not defined" unless defined $userid;
	my $column_manager_rows	= $Config::dba->hashed_process_sql(
    	"SELECT us.column_id, us.width, us.visibility, us.sort_order " .
   		" FROM usersettings us " .
   		" WHERE us.userid=? AND us.opcoid=?", [ $userid, $opcoid ] );
	if ( ! defined $column_manager_rows ) {
	 	#Get defaults(column manager settings) for new users.
		$column_manager_rows    = $Config::dba->hashed_process_sql( 
			"SELECT cmd.column_id, cmd.width, cmd.visibility, cmd.sort_order ". 
			" FROM column_manager_defaults cmd ");
	}

	if ( defined $column_manager_rows ) {
		foreach my $row ( @$column_manager_rows ) {
			my $column_id = $row->{column_id};
			if ( exists $columns{ $column_id } ) {
				my $column_list = $columns{ $column_id };
				$column_list->{visibility} = $row->{visibility};
				$column_list->{width} = $row->{width};
				$column_list->{sort_order} = $row->{sort_order};
			}
		}
		#warn "Count of rows from Column manager " . scalar( @$column_manager_rows );
	}
	# print Dumper( \%columns );

	# Now convert hash to array, with entries ordered by sort_order
	#my @mappings = sort by_column_sort_order %columns;

	my @keys = sort { $columns{$a}->{sort_order} <=> $columns{$b}->{sort_order} } keys( %columns );
	my @mappings = @columns{ @keys };
	# print "Mappings in sort order\n";
	# print Dumper( \@mappings );

	#my $rows = $Config::dba->hashed_process_sql(
	#		"SELECT cm.column_id, cm.column_label, cm.column_name, cm.condition, cm.type, cm.default_val, cm.null_equiv, cm.sortmenu, cm.orderbyid, cm.orderclause, " .
	#		" us.width, us.visibility, us.sort_order " .
	#		" FROM column_mappings cm LEFT OUTER JOIN usersettings us ON us.column_id = cm.column_id " .
	#		" WHERE (cm.column_name IS NOT NULL) AND us.userid=? AND us.opcoid=? AND (cm.column_name <> '') AND us.userid=?  " .
	#		" ORDER BY us.sort_order", [ $userid, $opcoid ] );

	return \@mappings;
}

##############################################################################
#   db_getMyJobs
#
#   Arguments
#   a) userid
#   b) orderById, defaults to 0, valid values 0..27, 98 or 99
#   c) sortby, ie sort direction, defaults to '', 'asc' or 'desc' or ''
#   d) vars
#
#	General query structure
#	SELECT <column list> FROM FROM  tracker3plus t3p
#	INNER JOIN assignmenthistory ah ON ah.job_number = t3p.job_number 
#	<other INNER and OUTER joins>
#	WHERE ah.status in ( 'A', 'W', 'P', 'R' ) AND ( ah.assigneeid = ? )
#	   AND ( opcoid = <user opcoid> OR location = (multiple locations) OR location = <user opcoid >
#	ORDER BY ... LIMIT ...;
#
#	Which means adding check after the query for those jobs with location = 17
#	to see if they have any shards at the users opcoid.
#
#	NOTE: the existing code in databaseJobs, db_getJobs, does a union to get the jobs
#	where tracker3plus.assignee matches the userid, BUT this makes the query VERY slow.
#	So it would be better to fix the problem at source, ie fix assignmenthistory, ie
#	add rows to assignmenthistory for rows on tracker3plus assigned to the user but not in assignmenthistory
#	If you do insist on ignoring this, please at least include (t3p.status != 6)
#
#   For info only:
#	Query to find jobs which are assigned to a user, but which dont appear in assignment history
#
#   SELECT t3p.job_number, t3p.assignee FROM tracker3plus t3p 
#    INNER JOIN tracker3 t3 ON t.job_number = t3p.job_number 
#    LEFT OUTER JOIN assignmenthistory ah ON ah.job_number = t3p.job_number AND t3p.assignee = ah.assigneeid 
#	 WHERE t3p.assignee IS NOT NULL AND t3p.assignee <> 0 AND ah.assigneeid IS NULL;
#
#   returns 4 rows on Staging, 5 rows on Live query took 624ms
#
##############################################################################
sub db_getMyJobs {
	my ( $userid, $orderbyid, $sortby, $opflags, $vars ) = @_;

	$orderbyid = 0 unless defined $orderbyid;

	$sortby = '' unless defined $sortby; # "asc" | "desc"
	$userid = '' unless defined $userid; # Should be numeric
	my $limit = Config::getConfig("myjobslimit") || 1000;
	my $opcoid = $vars->{opcoid};

	my @columns = @column_list; # Start with Standard list
	my $archantChk = $opflags->{ archantChk } || 0;
	push( @columns, @archant_columns ) if ( $archantChk == 0 );

	my $sql = 'SELECT ' . join( ', ', @columns ) . ' FROM  tracker3plus t3p ';
	$sql .= ' ' . join( ' ', @inner_joins );
	$sql .= ' ' . join( ' ', @archant_joins ) if ( $archantChk == 0 );
	$sql .= " WHERE t.job_number IN ( SELECT DISTINCT ah.job_number FROM assignmenthistory ah WHERE ( ah.status IN ( 'A', 'W', 'P', 'R' ) AND ( ah.assigneeid = ? ) ) ) ";
	$sql .= ' AND ( ( t3p.opcoid = ? ) OR ( t3p.location = ? ) ';
	$sql .= '    OR ( ( t3p.location = ? ) AND EXISTS ( SELECT 1 FROM shards s WHERE s.job_number = t3p.job_number AND s.location = ? ) ) ) ';

	my $orderByClause = getOrderBy(  $orderbyid, $sortby,  $vars, $opflags  ) || DEFAULT_MYJOBS_ORDERBY;

	$sql .= ' ORDER BY ' . $orderByClause . " LIMIT $limit";

	my $start_time = time();

	my $rows = $Config::dba->hashed_process_sql( $sql, [ $userid, $opcoid, $opcoid, OPCO_MULTIPLE_LOCATIONS, $opcoid ] );
	my $end_time = time();

	my $count = 0;

	#my @final_rows;

	# The rationale for POST query filtering like this is that it wil be quicker than doing a JOIN on shards
	# which is only currently used for DHL jobs.
	#if ( ! defined $rows ) {
	#	warn "db_getMyJobs: No jobs returned on initial query\n";
	#} else {
	#	warn "db_getMyJobs: Before filtering shards, rows = " . scalar( @{$rows} ) . "\n";
	#	foreach my $row ( @{$rows} ) {
	#		if ( $row->{remoteopcoid} == OPCO_MULTIPLE_LOCATIONS ) {   # TODO this isnt location id
	#			if ( ! ECMDBSearch::hasJobShardAtLocation( $row->{job_number}, $opcoid ) ) {
	#				warn "db_getMyJobs: Excluding job " . $row->{job_number} . "\n";
	#				next;
	#			}
	#		}
	#		push( @final_rows, $row );
	#	}
	#}
	#$count = scalar @final_rows;

	$count = scalar @$rows if ( defined $rows );
	my $elapsed = int(($end_time - $start_time) * 1000) / 1000;
	# warn "db_getMyJobs: elapsed $elapsed, count $count\n";

	# return ( \@final_rows, $sql );
	return ( $rows, $sql );
}

################################################################
#
# db_getAssignedtomecount - get count of jobs assigned to me
# used to display initial count in Left Hand nav
#
###############################################################
sub db_getAssignedtomecount {
	my ( $userid, $opcoid) = @_;

	Validate::opcoid( $opcoid );
	Validate::userid( $userid );

	my ($assigntome) = $Config::dba->process_oneline_sql(
		'SELECT COUNT( t3p.job_number ) FROM  tracker3plus t3p ' .
		' WHERE ' .
		"   t3p.job_number IN ( SELECT DISTINCT ah.job_number FROM assignmenthistory ah WHERE ( ah.status IN ( 'A', 'W', 'P', 'R' ) AND ( ah.assigneeid = ? ) ) ) " .
		' AND ( ( t3p.opcoid = ? ) OR ( t3p.location = ? ) ' .
		'   OR ( ( t3p.location = ? ) AND EXISTS ( SELECT 1 FROM shards s WHERE s.job_number = t3p.job_number AND s.location = ? ) ) ) ',
		[ $userid, $opcoid, $opcoid, OPCO_MULTIPLE_LOCATIONS, $opcoid ] );

	return $assigntome;
}


##############################################################################
#   db_getWatchedJobs
#
#   Arguments
#   - userid, ie users session.userid
#   - orderById, defaults to 0, valid values 0..27, 98 or 99
#   - sortby, ie sort direction, defaults to '', 'asc' or 'desc' or ''
#   - opflags, conatisn op specific flags including ArchantChk
#   - filters, list of filter descriptors (see modules/userfilter.pl)
#   - filter_values, hash with current settings of filters
#
#	Note this code ignores fromId and archived and that is intentional
#	otherwise it uses all the standard filter values.
#
#	General SQL query format
#	SELECT <column list> FROM  tracker3plus t3p 
#	INNER JOIN jobnotification  jn ON jn.job_number = t3p.job_number
#	<INNER and OUTER joins>
#	WHERE ( ( jn.userid = ? ) AND <filter predicates>
#	ORDER BY ... LIMIT ... 
#
##############################################################################

sub db_getWatchedJobs {
	my ( $userid, $orderbyid, $sortby, $opflags, $vars, $filters, $filter_values ) = @_;

	$orderbyid = 0 unless defined $orderbyid;
	$sortby = '' unless defined $sortby; # "asc" | "desc"
	$userid = '' unless defined $userid; # Should be numeric

	my $limit = Config::getConfig("watchedjobslimit") || 1000;
	my $archantChk = $opflags->{ArchantChk} || 0;

	my @columns = @column_list; # Start with Standard list
	push( @columns, @archant_columns ) if ( $archantChk == 0 );

	my $sql = 'SELECT ' . join( ', ', @columns ) . ' FROM  tracker3plus t3p ';
	$sql .= 'INNER JOIN jobnotification  jn ON jn.job_number = t3p.job_number ';
	$sql .= ' ' . join( ' ', @inner_joins );
	$sql .= ' ' . join( ' ', @archant_joins ) if ( $archantChk == 0 );

	my @where_list;
	push( @where_list, '( jn.userid = ? )' );
	if (( defined $filter_values ) && (scalar keys %$filter_values )) {
		my $extra_clause = Filters::filter_where_clause(  $filters, $filter_values ); # Returns an array ref
		# warn "db_getWatchedJobs extra clause " . Dumper( $extra_clause ) . "\n";
		push( @where_list, @$extra_clause );
		# warn "db_getWatchedJobs where list " . Dumper( \@where_list ) . "\n";
	}
	$sql .= ' WHERE ' . join( ' AND ', @where_list );

	my $orderByClause = getOrderBy(  $orderbyid, $sortby, $vars, $opflags ) || DEFAULT_ORDERBY;
	$sql .= ' ORDER BY ' . $orderByClause;
	$sql .= " LIMIT $limit";

	# warn "SQL $sql\n";

	my $start_time = time();
	my $rows = $Config::dba->hashed_process_sql( $sql, [ $userid ] );
	my $end_time = time();

	my $count = 0;
	$count = scalar @$rows if ( defined $rows );

	my $elapsed = int(($end_time - $start_time) * 1000) / 1000;
	# warn "db_getWatchedJobs: elapsed $elapsed, count $count\n";

	return ( $rows, $sql );
}

##############################################################################
#	db_getFilteredJobs
#
#	Arguments
#	- userid, ie users session.userid
#	- orderById, defaults to 0, valid values 0..27, 98 or 99
#	- sortby, ie sort direction, defaults to '', 'asc' or 'desc' or ''
#	- opflags, opco specific flags, includes archantChk,
#	- vars
#	- filters, list of filter descriptors (see modules/userfilter.pl)
#	- filter_values, hash with current settings of filters
#	- opcoid
#	- fromId (? is in filter values ...)
#
##############################################################################
sub db_getFilteredJobs {
	my ( $userid, $orderbyid, $sortby, $opflags, $vars, $filters, $filter_values, $opcoid, $fromId ) = @_;

	$orderbyid = 0 unless defined $orderbyid;
	$sortby = '' unless defined $sortby; # "asc" | "desc"
	$userid = '' unless defined $userid; # Should be numeric

	my $limit = Config::getConfig("jobrowslimit") || 1000;
	my $archantChk = $opflags->{ArchantChk} || 0;

	my @columns = @column_list; # Start with Standard list
	push( @columns, @archant_columns ) if ( $archantChk == 0 );

	if ( $vars->{subaction} eq 'kanban' ) {
		push( @columns, @kanban_traffic_columns  );
	}

	my $sql = 'SELECT ' . join( ', ', @columns ) . ' FROM (';
	$sql .=   'SELECT DISTINCT job_number FROM( ';
	$sql .=   '  SELECT  job_number FROM tracker3plus WHERE opcoid=? OR location=? ';
	$sql .=   '  UNION ';
	$sql .=   '  SELECT DISTINCT job_number FROM shards WHERE location=?';
	$sql .=   ') AS qq ) AS temp_table ';
	$sql .=   'INNER JOIN TRACKER3PLUS t3p ON t3p.job_number=temp_table.job_number ';
	$sql .=   ' ' . join( ' ', @inner_joins );
	$sql .=   ' ' . join( ' ', @archant_joins ) if ( $archantChk == 0 );

	my @where_list;
	my $list = ECMDBCustomerUser::getUserCustomerIdsByOpco( $userid, $opcoid );
	die "No customers visible" unless ( defined $list ) && ( scalar @{$list} );

	#warn "visible customers: ". Dumper( $list ) . "\n";
	#warn "userid: $userid, opcoid: $opcoid\n";

	push( @where_list, 't3p.customerid in ( ' . join( ',', @{$list} ) . ')' );
	push( @where_list, '( t3p.status !=6 )' ) unless ( ( exists $filter_values->{archived} ) && ( $filter_values->{archived} == 1 ) );
	push( @where_list, ' ( t3p.rec_timestamp > DATE_ADD(now(), INTERVAL -(' . $fromId . ') DAY) ) ' );
	push( @where_list, "( round( time_to_sec ( timediff ( t3p.duedate, now() ) ) / 60) >= -1000000 )" ) if ( looks_like_number( $orderbyid ) && ( $orderbyid == 2 ) );

	if (( defined $filter_values ) && (scalar keys %$filter_values )) {

		my $extra_clause = Filters::filter_where_clause(  $filters, $filter_values ); # Returns an array ref
		# warn "db_getFilteredJobs extra clause " . Dumper( $extra_clause ) . "\n";
		push( @where_list, @$extra_clause ) if ( defined $extra_clause );
		# warn "db_getFilteredJobs where list " . Dumper( \@where_list ) . "\n";
	}

	$sql .= ' WHERE ' . join( ' AND ', @where_list );

	my $orderByClause = getOrderBy(  $orderbyid, $sortby, $vars, $opflags ) || DEFAULT_ORDERBY;
	$sql .= ' ORDER BY ' . $orderByClause;
	$sql .= ' LIMIT '. $limit;

	$vars->{sql} = $sql;

	my $start_time = time();
	my $rows = $Config::dba->hashed_process_sql( $sql, [ $opcoid, $opcoid, $opcoid ] );
	my $end_time = time();
	my $elapsed = int(($end_time - $start_time) * 1000) / 1000;

	my $count = 0;
	$count = scalar @$rows if defined $rows;

	# warn "db_getFilteredJobs: elapsed $elapsed, count $count\n";

	return ( $rows, $sql );
}

##############################################################################
#	db_getResourceJobs
#
#	Arguments
#	- userid, ie users session.userid
#	- orderById, defaults to 0, valid values 0..27, 98 or 99
#	- sortby, ie sort direction, defaults to '', 'asc' or 'desc' or ''
#	- opflags, opco specific flags, includes archantChk,
#	- vars
#	- filters, list of filter descriptors (see modules/userfilter.pl)
#	- filter_values, hash with current settings of filters
#	- opcoid
#
##############################################################################
sub db_getResourceJobs {
	my ( $userid, $orderbyid, $sortby, $opflags, $vars, $filters, $filter_values, $opcoid ) = @_;

	$orderbyid = 0 unless defined $orderbyid;
	$sortby = '' unless defined $sortby; # "asc" | "desc"
	$userid = '' unless defined $userid; # Should be numeric

	die "Missing filters" unless defined $filters;
	die "Missing filter values" unless defined $filter_values;
	die "Missing Selected Date" unless defined $filter_values->{selecteddate};

	#warn "filters " . Dumper( $filters ) . "\n";
	#warn "filter values " . Dumper( $filter_values ) . "\n";

	my $limit = Config::getConfig("jobrowslimit") || 1000;
	my $archantChk = $opflags->{ArchantChk} || 0;

	my @columns = @column_list; # Start with Standard list
	push( @columns, @archant_columns ) if ( $archantChk == 0 );

	my $sql = 'SELECT ' . join( ', ', @columns ) . ' FROM (';
	$sql .=   'SELECT DISTINCT job_number FROM( ';
	$sql .=   '  SELECT  job_number FROM tracker3plus WHERE opcoid=? OR location=? ';
	$sql .=   '  UNION ';
	$sql .=   '  SELECT DISTINCT job_number FROM shards WHERE location=?';
	$sql .=   ') AS qq ) AS temp_table ';
	$sql .=   'INNER JOIN TRACKER3PLUS t3p ON t3p.job_number=temp_table.job_number ';
	$sql .=   ' ' . join( ' ', @inner_joins );
	$sql .=   ' ' . join( ' ', @archant_joins ) if ( $archantChk == 0 );

	my @where_list;
	my $list = ECMDBCustomerUser::getUserCustomerIdsByOpco( $userid, $opcoid );
	die "No customers visible" unless ( defined $list ) && ( scalar @{$list} );

	#warn "visible customers: ". Dumper( $list ) . "\n";
	#warn "userid: $userid, opcoid: $opcoid\n";

	push( @where_list, 't3p.customerid in ( ' . join( ',', @{$list} ) . ')' );
	push( @where_list, "( round( time_to_sec ( timediff ( t3p.duedate, now() ) ) / 60) >= -1000000 )" ) if ( looks_like_number( $orderbyid ) && ( $orderbyid == 2 ) );

	#push( @where_list, '( t3p.status !=6 )' ) unless ( ( exists $filter_values->{archived} ) && ( $filter_values->{archived} == 1 ) );

	if (( defined $filter_values ) && (scalar keys %$filter_values )) {

		my $box = $filter_values->{ 'box' };  # One of pending,inprogress,reject,waitqc,totaljobs,over
		if ( ( defined $box ) && ( $box ne '' ) ) {

			my $boxselecteddate = $filter_values->{ 'selecteddate' }; # Not a real filter as such, used by Dashboard_view to pass value (used when box is totaljobs or over)
			push( @where_list, "(DATE(t.requireddate) = \'$boxselecteddate\')" ) if ( ( defined $boxselecteddate ) && ( $boxselecteddate ne '' ) );

			# Map box to one or more filter settings
			if ( $box eq "ontrack" ) {
				push( @where_list, "(t3p.qcstatus = 2)" ); # QC Approved

			} elsif ( $box eq "waitqc" ) {
				push( @where_list, "(t3p.qcstatus = 1)" ); # QC Required

			} elsif ( $box eq "pending" ) {
				push( @where_list, "( t3p.operatorstatus IN ('U','A') )" ); # Unassigned or Assigned

			} elsif ( $box eq "inprogress" ) {
				push( @where_list, "( t3p.operatorstatus IN ('W','P') )" ); # Working or Processing

			} elsif ( $box eq "reject" ) {
				# So Operator status is Rejected OR QC Status is QC Rejected
				push( @where_list, "((t3p.operatorstatus = 'R') OR (t3p.qcstatus = '3'))" );

			} elsif ( $box eq "totaljobs" ) {
				# So (Operator status is Unassigned, Assigned, Working, Rejected, or Processing) OR (QC status is Required or Rejected)
				push( @where_list, "( t3p.operatorstatus IN ('W', 'P', 'A', 'U', 'R') OR t3p.qcstatus IN ('1','3') )" );

			} elsif ( $box eq "over" ) {
				# So Required date is in the past and QC status not Approved. 
				push( @where_list, "( (now() > t.requireddate) AND (t3p.qcstatus != 2) )" );

			} else {
				warn "Dont know how to handle box=$box";
			}
		}

		my $extra_clause = Filters::filter_where_clause(  $filters, $filter_values ); # Returns an array ref
		# warn "db_getResourceJobs extra clause " . Dumper( $extra_clause ) . "\n";
		push( @where_list, @$extra_clause ) if ( defined $extra_clause );
		# warn "db_getResourceJobs where list " . Dumper( \@where_list ) . "\n";
	}

	$sql .= ' WHERE ' . join( ' AND ', @where_list );

	my $orderByClause = getOrderBy(  $orderbyid, $sortby, $vars, $opflags ) || DEFAULT_ORDERBY;
	$sql .= ' ORDER BY ' . $orderByClause;
	$sql .= ' LIMIT '. $limit;

	$vars->{sql} = $sql;

	my $start_time = time();
	my $rows = $Config::dba->hashed_process_sql( $sql, [ $opcoid, $opcoid, $opcoid ] );
	my $end_time = time();
	my $elapsed = int(($end_time - $start_time) * 1000) / 1000;

	my $count = 0;
	$count = scalar @$rows if defined $rows;

	# warn "db_getResourceJobs: elapsed $elapsed, count $count\n";

	return ( $rows, $sql );
}

##############################################################################
#   db_getCreatedByMe - return jobs created by me
#
#	Similar to db_getMyJobs, but
#	a) uses the tracker3.employee column to match employeeid 
#	   (tracker3.bookedby is full user name, sort of, so we use the more specific employeeid)
#	b) limits the date range to 30 days
#
#   Arguments
#   a) userid
#   b) orderById, defaults to 0, valid values 0..27, 98 or 99
#   c) sortby, ie sort direction, defaults to '', 'asc' or 'desc' or ''
#   d) vars
#
#	General query structure
#	SELECT <column list> FROM tracker3plus t3p
#    INNER JOIN tracker3 t
#	<other INNER and OUTER joins>
#	WHERE t.employeeid = <employee id>
#	   AND ( opcoid = <user opcoid> OR location = (multiple locations) OR location = <user opcoid >
#	ORDER BY ... LIMIT ...;
#
#	Which means adding check after the query for those jobs with location = 17
#	to see if they have any shards at the users opcoid.
#
##############################################################################

sub db_getCreatedByMe {
	my ( $employeeid, $orderbyid, $sortby, $opflags, $vars ) = @_;

	$orderbyid = 0 unless defined $orderbyid;

	$sortby = '' unless defined $sortby; # "asc" | "desc"
	my $limit = Config::getConfig("createdbymelimit") || 1000;
	my $opcoid = $vars->{opcoid};

	my @columns = @column_list; # Start with Standard list
	my $archantChk = $opflags->{ archantChk } || 0;
	push( @columns, @archant_columns ) if ( $archantChk == 0 );

	my $sql = 'SELECT ' . join( ', ', @columns ) . ' FROM  tracker3plus t3p ';
	$sql .= ' ' . join( ' ', @inner_joins );
	$sql .= ' ' . join( ' ', @archant_joins ) if ( $archantChk == 0 );
	$sql .= ' WHERE ( t.employeeid = ? ) ';
	$sql .= ' AND ( ( t3p.opcoid = ? ) OR ( t3p.location = ? ) ';
	$sql .= '   OR ( ( t3p.location = ? ) AND EXISTS ( SELECT 1 FROM shards s WHERE s.job_number = t3p.job_number AND s.location = ? ) ) ) ';
	$sql .= ' AND ( t3p.rec_timestamp >= DATE_SUB(CURDATE(), INTERVAL 30  DAY ) ) ';

	my $orderByClause = getOrderBy(  $orderbyid, $sortby,  $vars, $opflags  ) || DEFAULT_MYJOBS_ORDERBY;
	$sql .= ' ORDER BY ' . $orderByClause . " LIMIT $limit";

	my $start_time = time();

	my $rows = $Config::dba->hashed_process_sql( $sql, [ $employeeid, $opcoid, $opcoid, OPCO_MULTIPLE_LOCATIONS, $opcoid ] );
	my $end_time = time();

	my $count = 0;
	$count = scalar @$rows if defined $rows;

	my $elapsed = int(($end_time - $start_time) * 1000) / 1000;
	# warn "db_getCreatedByMe: elapsed $elapsed, count $count\n";

	return ( $rows, $sql );
}

########################################################################################
####  db_getCreatedbycount - used to display initial count in Left hand nav
#
########################################################################################

sub db_getCreatedbycount {
	my ( $userid, $employeeid, $opcoid ) = @_;

	Validate::opcoid( $opcoid );
	Validate::userid( $userid );

   	my ($getcount) = $Config::dba->process_oneline_sql(
			'SELECT count(t.job_number) AS createdbycount FROM tracker3 t ' .
			' INNER JOIN tracker3plus t3p ON t3p.job_number = t.job_number ' .
			' WHERE t.employeeid = ? ' .
			'  AND ( ( t3p.opcoid = ? ) OR ( t3p.location = ? ) ' .
			'      OR ( ( t3p.location = ? ) AND EXISTS ( SELECT 1 FROM shards s WHERE s.job_number = t3p.job_number AND s.location = ? LIMIT 1 ) ) ) ' . 
			'  AND t3p.rec_timestamp >= DATE_SUB(CURDATE(), INTERVAL 30  DAY) ', 
			[ $employeeid, $opcoid, $opcoid, OPCO_MULTIPLE_LOCATIONS, $opcoid  ]);

    return $getcount;
}


##############################################################################
#   db_searchJobs
#	Returns job details for supplied list of job numbers
#
#   Arguments
#	- userid, ie users session.userid
#   - orderById, defaults to 0, valid values 0..27, 98 or 99
#   - sortby, ie sort direction, defaults to '', 'asc' or 'desc' or ''
#   - opflags
#   - vars
#	- jobs, list of job numbers (result of initial search using searchTypeId and searchContent)
#
##############################################################################

sub db_searchJobs {
	my ( $userid, $opcoid, $orderbyid, $sortby, $opflags, $vars, $jobs ) = @_;

	$userid = '' unless defined $userid; # Should be numeric
	$orderbyid = 0 unless defined $orderbyid;
	$sortby = '' unless defined $sortby; # "asc" | "desc"
	my $archantChk = $opflags->{archantChk} || 0;

	my $limit = Config::getConfig("searchjobslimit") || 1000;

 	# Both job number and media choice id are numeric and require an exact match
	# The other types can be wildcarded (so if the use uses *, we use LIKE)

	my $column = "t3p.job_number";

	# db_checkSearchContent in databaseSearch.pl, starts with tables tracker3plus and tracker3
	# then adds INNER JOINs to the tables needed to match $column eg 
	# Broadcasttracker3 btp if column is btp.editcode

	# The column list is the same as My Jobs, Watch List

	my @columns = @column_list; # Start with Standard list
	push( @columns, @archant_columns ) if ( $archantChk == 0 );

	# This function is different in that it includes many more columns and tables, 
	# some with INNER JOINS, some with LEFT OUTER JOINs
	
	# So INNER JOINs will always be so, but if we have a LEFT OUTER JOIN and we are trying
	# to match against a column on that table, replace it with an INNER JOIN.

	my @joins = @inner_joins;
	push( @joins, @archant_joins ) if ( $archantChk == 0 );
	my @modified_joins;

	# We could be clever here, but the only tables used for matching are tracker3plus, tracker3 and Broadcasttracker3 
	# but we only use a LEFT OUTER JOIN for Broadcasttracker3 (alias btp)

	foreach my $join ( @joins ) {
		if ( ( $join =~ /LEFT OUTER/ ) && ( $column =~ /btp\./ ) ) {
			$join =~ s/LEFT OUTER/INNER/;
		}
		push( @modified_joins, $join );
	}

	my $sql = 'SELECT ' . join( ', ', @columns ) . ' FROM  tracker3plus t3p ';
	$sql .= join( ' ', @modified_joins );

	# WHERE clause includes a) search term, b) opcoid limitation c)customer limitation
	my @where;
	my @values;

	push( @where, "(t3p.job_number IN ( $jobs ))" );

	# Select jobs where the home location matches the opcoid, or the remote location matches the opcoid (or 17, meaning Multiple Locations)
	push( @where, "((t3p.opcoid = ?) OR (t3p.location=?) OR (t3p.location=17))" );
	push( @values, $opcoid, $opcoid );

	# Then finally customer limitation
	my $customer_ids = ECMDBCustomerUser::getUserCustomerIdsByOpco( $userid, $opcoid );
	die "No customers visible for user $userid, opco $opcoid" unless ( (defined $customer_ids ) &&  ( scalar @$customer_ids ) );
	push( @where, "t3p.customerid IN (" . join(',', @$customer_ids ) . ")");

	$sql .= " WHERE " . join( ' AND ', @where );

	my $orderByClause = getOrderBy(  $orderbyid, $sortby,  $vars, $opflags ) || DEFAULT_ORDERBY;
	$sql .= ' ORDER BY ' . $orderByClause . ' LIMIT ' . $limit;
	# $vars->{sql} = $sql;

	my $start_time = time();
	my $rows = $Config::dba->hashed_process_sql( $sql, [ @values ] );

	my $elapsed = int((time - $start_time) * 1000) / 1000;
	my $count = 0;
	$count = scalar @$rows if ( defined $rows );

	# warn "db_searchJobs: after main query, elapsed $elapsed, count $count\n";
	# warn "db_searchJobs: $sql\n";

	# Post processing to check whether jobs at "Multiple locations" are visible
	# The list of customers ids created ealier will allow the user to view a customer at any location
	# but if the job.opcoid is NOT visible and job.location=17 (Multiple locations) then we have
	# to check the shard locations.

	# Loop from last to first, so we can delete entries we want to eliminate
	my @results;
	if ( $count > 0 ) {
		foreach my $job ( @$rows ) {
			if ( ( $job->{myopcoid} != $opcoid ) && ( $job->{multiple_locations} == 1 ) ) {
				# warn "Job " . $job->{job_number} . " is at multiple locations and cant see at home location";
				my $canSeeShard = ECMDBSearch::hasJobShardAtLocation( $job->{job_number}, $opcoid );
				next if ( ! $canSeeShard );
			}
			push( @results, $job );
		}
		my $count2 = scalar @results;
		my $elapsed = int((time - $start_time) * 1000) / 1000;
		# warn "db_searchJobs: after multiple_location check, elapsed $elapsed, count $count2, previous $count\n";
		$rows = \@results;
		$count = $count2;
	}
	# die "No jobs returned" unless $count;

	return ( $rows, $sql );
}

# Get list of filter constraints, given a specific fromId (day range)
# Returns array of hash.

sub get_dashboard_constraints {
	my ( $fromId ) = @_;

	my $rows = $Config::dba->hashed_process_sql(
			"SELECT min_days, max_days, constraint_name, filter_name, min_value, max_value, description FROM dashboard_constraints WHERE ? >= min_days AND ? <= max_days",
			[ $fromId, $fromId ] );
	return $rows;
}

##############################################################################
#### db_getidnamebytablename (lifted from databaseJob.pl)
###############################################################################
sub db_getidnamebytablename {
	my ( $tablename, $columnname, $options ) = @_;
	my @values = ();

	my $id_col = 'id';
	my $orderby = '';

	if ( (defined $options) && ( ref($options) eq "HASH" ) ) {
		if ( exists $options->{id} ) {
			$id_col = $options->{id}; # Allow alternative to id column
		}
		if ( exists $options->{orderby} ) {
			$orderby = 'ORDER BY ' . $options->{orderby};
		}
	}
	my $sql = "SELECT $id_col AS id, $columnname AS name FROM $tablename $orderby";
	# warn "db_getidnamebytablename: table $tablename, column $columnname, sql $sql\n";

	my $rows = $Config::dba->hashed_process_sql( $sql );
	die "db_getidnamebytablename: returned no rows for table $tablename" unless defined $rows;

	return $rows; # Return ref to array of { id, name }
}

# db_userlist - lifted from databaseJob.pl

sub db_userlist  {
    my ($opcoid) = @_;

    my ( $id, $username, @userslist );

    my $rows = $Config::dba->hashed_process_sql(
		"SELECT u.id AS id, u.username AS username FROM (" .
		" SELECT DISTINCT(assignee), opcoid, location FROM tracker3plus t3p GROUP BY assignee, opcoid, location) t3p " .
		" INNER JOIN users u ON t3p.assignee=u.id " .
		" WHERE t3p.opcoid=? OR t3p.location=? " .
		" GROUP BY username ORDER BY username ASC", [ $opcoid, $opcoid ]);

    foreach my $row (@$rows) {
        push(@userslist, { id => "ASS_" . ($row->{id} || "0"),   name => ($row->{username}) || "-" });
	}
    return \@userslist;
}

########################################################################################
# db_getEmployeeid - get employeeid, given userid
#
########################################################################################

sub db_getEmployeeid {
	my ( $userid ) = @_;

    Validate::userid( $userid );
    my ($employeeid) = $Config::dba->process_oneline_sql("SELECT employeeid FROM users WHERE id = ? ", [ $userid ]);
    return $employeeid;
}

sub get_operating_company_flags {
	my ( $opcoid ) = @_;

	# @TODO could replace these calls by a call to ECMDBOpcos::getOperatingCompany, would need to change that to return all columns though.
	# could currently use it for publishing, adgatesend, thumbview, estimateon,

	my $opflags = { };

    my $rows = $Config::dba->hashed_process_sql(
			"SELECT publishing, opconame, usejobrepository, estimateon, jobassignment, adgatesend, thumbview FROM operatingcompany WHERE OPCOID=? ", 
			[ $opcoid ] );

	die "Invalid opcoid '$opcoid'" if ( ! defined $rows );
	die "> 1 row for opcoid $opcoid" if ( scalar @$rows ) > 1;
	my $row = @{$rows}[0];

	$opflags->{publishing} = $row->{publishing} || 0;				# ECMDBOpcos::isPublishing( $opcoid );

	$opflags->{ArchantChk} = 1; # returns 0 if opconame is Archant, 1 otherwise
	$opflags->{ArchantChk} = 0 if ( $row->{opconame} =~ /Archant/i ); # ECMDBOpcos::isArchant( $opcoid );

	$opflags->{usejobrepository} = $row->{usejobrepository} || 0;	# ECMDBOpcos::db_usejobrepository( $opcoid );
 
	$opflags->{estimateon} = $row->{estimateon};					# ECMDBOpcos::getOpcoEstimateOn( $opcoid );

	$opflags->{isjobassignment} = $row->{jobassignment};			# ECMDBOpcos::isJobAssignment( $opcoid );

	$opflags->{isadgatesend} = $row->{adgatesend};					# ECMDBOpcos::db_adgateSend( $opcoid );

	$opflags->{isthumbview}	= $row->{thumbview};					# ECMDBOpcos::db_thumbview( $opcoid );

	my ( $bookingform ) = $Config::dba->hashed_process_sql(
		"SELECT bookingform FROM imageworkflow WHERE opcoid = ?", [ $opcoid ] );
	$opflags->{bookingformnav} = $bookingform || 0;					# ECMDBImageworkflow::db_isImageBookingForm( $opcoid );

	return $opflags;
}

sub test_getVisibleColumns {

	my $symbol_table = {};
	my $userid = 330;
	my $opcoid = 8432;

	my $mappings = getVisibleColumns( $userid, $opcoid, $symbol_table );
	print Dumper( $mappings );
}

# test_getVisibleColumns();

# get product assignee details based on retrieved job list
sub getProductAssignee {
	my ($job_numbers)	=	@_;
	my $rows;
	if($job_numbers ne "")
	{
		#status => 4 consider as product is completed
		$rows = $Config::dba->hashed_process_sql( "	SELECT  job_number,
													GROUP_CONCAT( CASE WHEN status NOT IN (4) AND userid > 0 THEN userid  END ) userid,
													SUM(CASE WHEN status NOT IN (4) AND userid > 0 THEN 1 ELSE 0 END) product_asisgned_user_count,
													SUM(CASE WHEN status IN (4) AND userid = 0 THEN 1 ELSE 0 END) completed_product_count  
													FROM    products where job_number IN ($job_numbers) GROUP BY job_number" 
												);
	}
	return $rows ? @$rows : ();
}

# get product Op Status details based on retrieved job list
sub getProductOpStatus {
	my ($job_numbers)   =   @_;
	my $rows;

	if($job_numbers ne ""){
		$rows = $Config::dba->hashed_process_sql( " SELECT job_number,
													SUM(CASE WHEN status >= 0 THEN 1 ELSE 0 END) product_count,
													SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END) none_count,
													SUM(CASE WHEN status IN (1,3) AND userid > 0 THEN 1 ELSE 0 END) product_asisgned_count,
													SUM(CASE WHEN status IN (2) AND userid >  0 THEN 1 ELSE 0 END) product_working_count,
													SUM(CASE WHEN status = 4 THEN 1 ELSE 0 END) product_completed_count
													FROM products where job_number IN ($job_numbers) GROUP BY job_number"
												);
	}

	return $rows ? @$rows : ();
}

#If there is only one product is assigned to any user, update that user as job assignee
sub updateJobAssignee {
	my ($userid,$job_number)	=	@_;
	Validate::userid( $userid );
	Validate::job_number($job_number);
	$Config::dba->process_sql("UPDATE TRACKER3PLUS SET assignee = $userid WHERE job_number=?", [ $job_number ]);
}

##############################################################################
# db_getproducernamesbyopco - Get Producer names from tracker3 table to load under Producers in Filter.
# In reality we only need to get t3.producer from jobs whose home opcoid is one of Smoke_and_mirrors or Smoke_at_Tag
# We are only interested in jobs which have a HOME opcoid which is Smoke related
# so it doesnt matter if the job is checked out elsewhere.
#
###############################################################################
sub db_getproducernamesbyopco {

	my ($opcoid) = @_;

	my $broadcast_opcos = Config::getConfig("broadcast_opcos") || ''; # List of opcoids, for Smoke_and_mirrors etc
	my @producers;

	if ( $broadcast_opcos ne '' ) {
		my $t0 = time();
		my $rows = $Config::dba->hashed_process_sql(
			"SELECT DISTINCT t3.producer FROM tracker3 t3 " .
			"INNER JOIN tracker3plus t3p ON t3p.job_number=t3.job_number " .
			"WHERE (t3p.opcoid IN ( $broadcast_opcos )) ORDER BY producer" ); 

    	if ( defined $rows ) {
			# warn Dumper( $rows ) . "\n";

			# Note we treat undefined and '' carefully, so that the user can distinguish and select

			foreach my $res ( @$rows ) {
				# push ( @producers, { id => $res->{producer}, name => $res->{producer} || "-" } );

				next if ( ! defined $res->{producer} ); # Add "undef" at end of list
				if ( $res->{producer} =~ /^\s*$/ ) {
					push ( @producers, { name => "(blank)" } );
				} else {
					push ( @producers, { name => $res->{producer} } );
				}
			}
			my $elapsed = time() - $t0;
			#warn "t3.producer query, took $elapsed secs\n";
			#warn Dumper( \@producers ) . "\n";
		}
	}
	push( @producers, { name => "undef" } ); # Add at end of list for consistency

	return \@producers;
}
1;
