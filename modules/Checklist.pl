#!/usr/local/bin/perl
package Checklist;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use JSON;

use Template;

# Home grown
use lib 'modules';

use CTSConstants;

require "config.pl";
require "news.pl";
require "htmlhelper.pl";
require 'databaseJob.pl';
require 'databaseProducts.pl';
require 'databaseOpcos.pl';
##############################################################################
###               Checklist
################################################################################
sub CheckList {

        my ( $query, $session, $duplicateflag ) = @_;

	my $sidemenu   	= $query->param( "sidemenu" );
        my $sidesubmenu	= $query->param( "sidesubmenu" );
        my $mode     	= $query->param( 'mode' ) ||"";

        my $roleid     	= $session->param( 'roleid' );

	my @datarows;
        my @newsitems   = News::getNewsItems();  # Get all (or at least 25) news items

    	my $rows = $Config::dba->hashed_process_sql(
			"SELECT CD.ID, OP.OPCOID, CS.ID as custid, MD.ID as mediaid, OP.OPCONAME, CS.CUSTOMER, MD.TYPE, CD.rec_timestamp FROM CHECKLISTDATA CD
   			INNER JOIN OPERATINGCOMPANY OP on OP.OPCOID=CD.OPCOID
        		INNER JOIN CUSTOMERS CS on CS.ID=CD.CUSTID
			INNER JOIN MEDIATYPES MD on MD.ID=CD.MEDIAID WHERE CD.ISSTAGE = 0 AND CD.PARENTID = 0 ORDER BY OPCONAME,CUSTOMER,TYPE", undef );

	@datarows = @$rows if ( $rows );

        my $vars        = {
                newsitems   => \@newsitems,
                sidemenu    => $sidemenu,
                sidesubmenu => $sidesubmenu,
                roleid      => $roleid,
		datarows    => \@datarows,
		duplicateflag => $duplicateflag,
        };

        HTMLHelper::setHeaderVars( $vars, $session );

	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

	my $tt = Template->new({ RELATIVE => 1, }) || die "$Template::ERROR\n";
	$tt->process('ecm_html/checklist.html', $vars )
			|| die $tt->error(), "\n";
}


##############################################################################
###    Showchecklistedit
###      
################################################################################

sub Showchecklistedit
{
    my $query       = $_[0];
    my $session     = $_[1];

    my $mode        = $query->param( "mode" );
    my $rowid      	= $query->param( "rowid" );
	my $isstage		= $query->param( "isstage" );
	my $productflag	= $query->param( "productflag" );
	my $jobnumber	= $query->param( "jobnumber" );
	my $userid      = $session->param( 'userid' );
	my $opcoid		= $query->param( "opcoid" );
	my $custid		= $query->param( "custid" );
	my $mediaid		= $query->param( "mediaid" );
	my $userID      = $session->param( 'userid' );
	my $lineItemID  = $query->param("lineitemid");
	my $location 	= ECMDBJob::db_getLocation($jobnumber);
	my $currentUserID = "";
	my $houseRevision = "";
	my $opconame	= ECMDBOpcos::getOpcoName($opcoid);
	my ($cusname,$cuslogo)    	= ECMDBOpcos::getCustomerName($custid);
	my $medianame   = ECMDBOpcos::getMedianame($mediaid);
	if ( $productflag == 1)
	{
		
		$currentUserID 	= ECMDBProducts::getProductAssignee( $lineItemID );
		$houseRevision 	= ECMDBProducts::getCurrentHouseRevision( $lineItemID );
	}
	my @editrows    = ();
	 #For CTS report.
	 ECMDBJob::db_recordJobAction($jobnumber, $userid, CTSConstants::CHECKLIST);

     my $rows = $Config::dba->hashed_process_sql(
		"SELECT OP.OPCOID,OP.OPCONAME,CD.ID,CS.ID AS CUSTID,CS.CUSTOMER,MD.ID AS MEDIAID,MD.TYPE FROM CHECKLISTDATA CD
		INNER JOIN OPERATINGCOMPANY OP on OP.OPCOID=CD.OPCOID
		INNER JOIN CUSTOMERS CS on CS.ID=CD.CUSTID
		INNER JOIN MEDIATYPES MD on MD.ID=CD.MEDIAID WHERE CD.description = 'LISTDATA' and CD.OPCOID = ? and CD.CUSTID = ? and CD.MEDIAID =? ",
		 [$opcoid, $custid, $mediaid]);

	if ($rows)
       	{
                @editrows = @$rows;
			#Inserted when 'Checklist' button is clicked.
		    if ( $productflag == 1)
		    {   
				my $checkListID = $editrows[0]->{'ID'};
				ECMDBProducts::startProduct( $lineItemID ,$currentUserID, $jobnumber, $opcoid, $custid, $location, $houseRevision, $checkListID );
			}
       	}
	my @displaydesc = displaydescdetails($query, $session);
        my $vars        = {
                editrows    		=> \@editrows,
				rowid				=> $rowid,
				isstage     		=> $isstage,
				displaydescription 	=> \@displaydesc,
				productflag			=> $productflag,
				jobnumber			=> $jobnumber,
				opconame			=> $opconame,
				cusname				=> $cusname,
				medianame			=> $medianame,		
        };
		if ( $mode eq "json") 
        {
                my $json = encode_json ( $vars );
                print $json;    
                return $json;   
        }

}


#################################################################################
#####    Deletetaskstage - To update isactive field as O when click delete option
#####      
##################################################################################

sub Deletetaskstage {

    my $query       = $_[0];
    my $session     = $_[1];
    my $rowid       = $query->param( "rowid" );
    my $opcomid     = $query->param( "opcoid" );
    my $customerid  = $query->param( "custid" );
    my $mediaid     = $query->param( "mediaid" );
    my $productflag = $query->param( "productflag" );
    my $jobnumber   = $query->param( "jobnumber" );
    #my $desctext	= $query->param( "desctext" );
    my $mode        = $query->param( "mode" );
    my $rows = $Config::dba->process_sql("UPDATE checklistdata SET isactive = 0 WHERE id=? OR parentid=?" , [ $rowid,$rowid ]);
    my @savedescdet = displaydescdetails($query, $session);

    my $vars        = {
        Deletetaskstage    	=> 1,
        rowid       		=> $rowid,
        opcoid      		=> $opcomid,
        custid      		=> $customerid,
        mediaid    			=> $mediaid,
		displaydescription  => \@savedescdet,
	};
	
	if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

####################################################################################
########   Editchecklist - To display the edited content when click edit option
########      
#####################################################################################

sub Editchecklist {
	my ( $query, $session ) = @_;

	my $mode        = $query->param( "mode" );
	my $rowid       = $query->param( "rowid" );
	my $isstage     = $query->param( "isstage" );
	my $productflag = $query->param( "productflag" );
	my $jobnumber   = $query->param( "jobnumber" );
	my $userid      = $session->param( 'userid' );
	my $opcoid      = $query->param( "opcoid" );
	my $custid      = $query->param( "custid" );
	my $mediaid     = $query->param( "mediaid" );

	my @disdesc;

	my $rows  = $Config::dba->hashed_process_sql(
		"SELECT id, isstage, description, ismandatory FROM checklistdata WHERE id = ?", [ $rowid ] );
	if ( $rows ) {
		foreach my $r ( @$rows ) {
			push( @disdesc, { id => $r->{id}, isstage => $r->{isstage}, description => $r->{description}, ismandatory => $r->{ismandatory},
				opcoid => $opcoid, cusid => $custid, mediaid => $mediaid } );
		}
	}
	
      if ( $mode eq "json" ){
        my $vars = {
            editchecklist => \@disdesc,
        };
     my $json = encode_json ( $vars );
     print $json;
     return $json;
    }

}

##############################################################################
#######   ApplyChecklistReorder-save reordering functionality in checklist
#######      
####################################################################################

sub ApplyChecklistReorder{
	my ( $query, $session ) = @_;

	my $mode        = $query->param( "mode" );
	my $updatestr   = $query->param( "updatestr" );
	my $userid      = $session->param( 'userid' );

	my @updatestrArray = split(",", $updatestr);
	for my $colstr ( @updatestrArray ) {
		my ( $id, $sortorder ) = split /~/, $colstr;
		$Config::dba->process_sql( "UPDATE CHECKLISTDATA SET sortorder = ? WHERE id=?", [ $sortorder, $id ] );
	}

	my $vars = {
		ApplyReorder => 1,
	};

	if ( $mode eq "json" ){
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

} #ApplyChecklistReorder


##############################################################################
######    Updatetaskstage
######      
###################################################################################
sub Updatetaskstage {

    my $query       = $_[0];
    my $session     = $_[1];
    my $userid      = $session->param( 'userid' );
    my $mode        = $query->param( "mode" );
    my $opcoid      = $query->param( "opcoid" );
    my $custid      = $query->param( "custid" );
    my $mediaid     = $query->param( "mediaid" );
    my $rowid       = $query->param( "rowid" );
    my $desctxt     = $query->param( "desctext" );
    my $isstage     = $query->param( "isstage" );
    my $checkboxstatus  =$query->param( "checkboxstatus" );

    my $rows = $Config::dba->process_sql("UPDATE checklistdata SET description = ?, ismandatory =?  WHERE id=?" , [ $desctxt, $checkboxstatus, $rowid ]);
    my @savedescdet = displaydescdetails($query, $session);
    my $vars        = {
        Updatetaskstage     => 1,
        rowid               => $rowid,
        opcoid              => $opcoid,
        custid              => $custid,
        mediaid             => $mediaid,
        displaydescription  => \@savedescdet,
    };

    if ( $mode eq "json")
    {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
####    getMediatypeId
####      
#################################################################################

sub getMediatypeId {
    my $jobid       = $_[0];

    my ( $mediatypeId ) = $Config::dba->process_oneline_sql(
		"SELECT m.id FROM tracker3 t, mediatypes m WHERE t.JOB_NUMBER = ? AND UPPER(t.WORKFLOW) = UPPER(m.type)", [ $jobid ], 1 );

    return $mediatypeId;
}



###################################################################################
#####    displaydescdetails - Displaying all the saved details from table as array
#####      
####################################################################################

sub displaydescdetails {

    my $query		= $_[0];
    my $session		= $_[1];
    my $userid		= $session->param( 'userid' );
    my $rowid		= $query->param( "rowid" );
    my $opcomid		= $query->param( "opcoid" );	
    my $customerid	= $query->param( "custid" );
    my $mediaid		= $query->param( "mediaid" );
    my $productflag	= $query->param( "productflag" );
    my $jobnumber	= $query->param( "jobnumber" );
    my @getdesc;
    my $rows;

    if( $productflag == 0 ) {
        $rows  = $Config::dba->hashed_process_sql(
			"SELECT id, description, isstage, isactive, ismandatory, parentid,sortorder FROM checklistdata " . 
			" WHERE opcoid=? and custid=? and mediaid=? and description not in ('LISTDATA') and isactive = 1 order by sortorder", 
			[$opcomid, $customerid, $mediaid]);
    } else {
        $rows = $Config::dba->hashed_process_sql(
		"SELECT CD.id,CD.description,CD.isstage,CD.isactive,CD.ismandatory,CD.parentid,CD.sortorder,CD.opcoid,CD.custid,CD.mediaid,JC.job_number,JC.chklineitemid as rowid,JC.ischecked " .
                " FROM checklistdata as CD " .
		" LEFT JOIN jobschecklist as JC ON CD.id = JC.chklineitemid and JC.job_number = ? and JC.userid =? " .
		" WHERE CD.OPCOID = ? and CD.CUSTID = ? and CD.MEDIAID = ? " .
		"   AND CD.description NOT IN ('LISTDATA') AND (isactive = 1) " .
		" GROUP BY CD.id ORDER BY CD.sortorder", 
		[$jobnumber, $userid, $opcomid, $customerid, $mediaid]);
    }
	@getdesc = @$rows  if ($rows);
	return @getdesc;
}

#############################################################################
##       getMediatypes
##      
###############################################################################
sub getMediatypes {
	my ( $query, $session ) =@_;

	my @media;

	my $rows = $Config::dba->hashed_process_sql( "SELECT id, type FROM MEDIATYPES" );
	if ( $rows ) {
		foreach my $r ( @$rows ) {
			 push ( @media, { mediaid => $r->{id}, medianame => $r->{type} }  )
		}
	}
	return @media;
}

##############################################################################
#####       maxSortOrder: To update the sortorder initially in DB in checklist
##################################################################################
sub maxSortOrder {
	my ( $query, $session ) =@_;

	my $opcoid      = $query->param( "opcoid" );
	my $custid      = $query->param( "custid" );
	my $mediaid     = $query->param( "mediaid" );
	my $rowid       = $query->param( "rowid" );
	my $isstage     = $query->param( "isstage" );
	my $sortorder 			= undef;

    if  ( $isstage == 1 && $rowid == 0 )
    {
        $sortorder = $Config::dba->hashed_process_sql("SELECT max(sortorder) AS MAXSORTORDER 
                                                    FROM checklistdata 
                                                    WHERE OPCOID =? and CUSTID =? and MEDIAID =? and ISSTAGE = ?",
                                                    [$opcoid,$custid,$mediaid,$isstage]
                                                    );
    }
    elsif($isstage == 2 && $rowid != 0 )
    {
        $sortorder  = $Config::dba->hashed_process_sql("SELECT max(sortorder) AS MAXSORTORDER 
						   FROM checklistdata
						   WHERE OPCOID =? and CUSTID =? and MEDIAID =? and PARENTID = ?",
						   [$opcoid,$custid,$mediaid,$rowid]
						 );
    }
	return $sortorder || 0;

} #maxSortOrder

##############################################################################
####       Savedescriptiondata: To save descrition of 'TASK' or 'STAGE' in DB.
#################################################################################
sub Savedescriptiondata {
	my ( $query, $session ) = @_;


    my $mode        = $query->param( "mode" );
    my $opcoid      = $query->param( "opcoid" );
    my $custid      = $query->param( "custid" );
    my $mediaid     = $query->param( "mediaid" );
    my $rowid      	= $query->param( "rowid" );
    my $desctxt		= $query->param( "desctext" );
    my $isstage		= $query->param( "isstage" );
    my $checkboxstatus	= $query->param( "checkboxstatus" );

    my $userid      = $session->param( 'userid' );

	my $getmax = maxSortOrder( $query, $session );
	my $maxsortorder = $getmax->[0]->{MAXSORTORDER} || 0;
	$maxsortorder = $maxsortorder + 1;

	$Config::dba->process_sql(
		" INSERT INTO checklistdata (opcoid, custid, mediaid, description, isstage, isactive, ismandatory, parentid, userid, rec_timestamp, sortorder) " .
		" VALUES ( ?, ?, ?, ?, ?, 1, ?, ?, ?, now(), ? )",
		[  $opcoid, $custid, $mediaid, $desctxt, $isstage, $checkboxstatus, $rowid, $userid, $maxsortorder ] );

	my @savedescdet = displaydescdetails($query, $session);
    
	my $vars        = {
		Savedescriptiondata => 1,
		rowid       => $rowid,
		isstage    	=> $isstage,
		opcoid		=> $opcoid,
		custid		=> $custid,
		mediaid		=> $mediaid,
		displaydescription  => \@savedescdet,
	};
    
	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}

} #Savedescriptiondata

##############################################################################
###y       Savejobchecklist
####      
#################################################################################
sub Savejobchecklist{
	my ( $query, $session ) = @_;

	my $userid      		= $session->param( 'userid' );
	my $mode        		= $query->param( "mode" );
	my $jobnumber			= $query->param( "jobnumber" );
	my $splitvalues 		= $query->param( "splitvalues" );
	my $rowid				= undef;
	my $checkboxstatus		= undef;
	my @splitarray			= split("~",$splitvalues);

	foreach my $splitstr ( @splitarray )
	{
		#my @rowidval = split("-", $splitstr))[0,1];
		my ( $rowid, $checkboxstatus ) = (split("-", $splitstr))[0,1];
		#$rowid	= $rowidval[0];
		#$checkboxstatus	= $rowidval[1];
		# TODO this is a BAD way to update the checklist (same done elsewhere and needs to be fixed NOT copied)
		$Config::dba->process_sql(
			"DELETE FROM jobschecklist WHERE chklineitemid =? AND job_number =? AND userid =?", [$rowid,$jobnumber,$userid] );
		$Config::dba->process_sql(
			"INSERT INTO jobschecklist ( chklineitemid, job_number, ischecked, userid, rec_timestamp ) 
				VALUES ( ?, ?, ?, ?, now() ) ", [ $rowid, $jobnumber, $checkboxstatus, $userid ]);
	}

	my $vars = {
		Savejobchecklist =>1,
	};
	if ( $mode eq "json" ) {
		my $json = encode_json($vars);
		print $json;
		return $json;
	}
}

##############################################################################
##y       saveChecklistdata
###      
################################################################################
sub saveChecklistdata {
	my ( $query, $session ) = @_;

	my $mode        = $query->param( "mode" );
	my $opcoid 	= $query->param( "opcoid" );
	my $custid     	= $query->param( "custid" );
	my $mediaid     = $query->param( "mediaid" );
	my $userid      = $session->param( 'userid' );

        my ( $exists ) = $Config::dba->process_oneline_sql(
			"SELECT ID FROM checklistdata where OPCOID =? and CUSTID =? and MEDIAID =?", [ $opcoid, $custid, $mediaid ] );

	my $duplicateflag = 0;
	if ( $exists ) {
		$duplicateflag = 1;
	} else {
		my $sortorder = 0;
		$Config::dba->process_oneline_sql(
			"INSERT INTO checklistdata (opcoid, custid, mediaid, description, isstage, isactive, parentid, userid, rec_timestamp,sortorder) " .
			" VALUES (?, ?, ?, 'LISTDATA', 0, 1, 0, ?, now(), ? ) ",
			[ $opcoid , $custid , $mediaid, $userid, $sortorder ] );

	}
	CheckList( $query, $session, $duplicateflag );
}

##############################################################################
###       getchecklistvalues
###      
################################################################################

sub getchecklistvalues {
	my ( $query, $session ) = @_;

	my $mode	= $query->param( "mode" );
	my $opcoid      = $query->param( "opcomid" );
	my $custid      = $query->param( "customerid" );
	my $mediaid     = $query->param( "mediaid" );
	
	my $duplicateflag = 0;
	
	my ( $exists ) = $Config::dba->process_oneline_sql(
		"SELECT ID FROM checklistdata where OPCOID =? and CUSTID =? and MEDIAID =?",[$opcoid,$custid,$mediaid]);

	if ($exists ) {
		$duplicateflag=1;
	}

	my $vars = {
		duplicateflag => $duplicateflag,
	};
	if ( $mode eq "json") {   
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

##############################################################################
##       getcustomernamebyopcoid
##      
###############################################################################

sub getcustomernamebyopcoid {
	my ( $query, $session ) = @_;

	my $mode = $query->param( "mode" ) || "";
	my $opcoid = $query->param( "opcoid" );

	my @customer;
	my $vars;
	eval {
		Validate::opcoid( $opcoid ); 
		my $rows = $Config::dba->hashed_process_sql( "SELECT id, customer FROM CUSTOMERS WHERE opcoid = ?", [ $opcoid ] );

		if ( $rows ) {
			foreach my $r ( @$rows ) {
				push ( @customer, { id => $r->{id}, customer => $r->{customer} } );
			}
		} 
        };
        my @mediatypes = getMediatypes();
        $vars = { customers => \@customer, media => \@mediatypes };

	if ( $@ ) {
		$vars = { error => $@ };
	}
	if ( $mode eq "json" ){
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
	return @customer;
}   

sub companyList {
	my ( $query, $session ) = @_;

	my @opcos = ECMDBOpcos::getOperatingCompanies();
	my $vars = { opcos => \@opcos };
	my $json = encode_json ( $vars );
	print $json;

}

sub productchecklistroles {

    my ( $query, $session ) = @_;


    my $roleid      = $session->param( 'roleid' );
    my $access      = undef;

    #my ( $checklistaccess ) = $Config::dba->process_oneline_sql("SELECT roles FROM permissions WHERE entity = ?", [ "checklistaccess" ] );    
    if ($roleid =~ /^1$|^3$|^4$|^6$/) 
    {
        $access  =   1;
    }
    else
    {
        $access  =   0;
    }
    print $access;

}


##############################################################################
#       end of file
#      
##############################################################################

1;
