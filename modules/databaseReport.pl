	#!/usr/local/bin/perl
package ECMDBReports;

use warnings;
use strict;

use lib 'modules';

require "config.pl";
require "general.pl";



sub addReport
{
	my ($title, $formats, $sourcefile, $updatedbyuser) = @_;
	
	return if !General::isAlphaNumeric($title);
	$formats = "" unless $formats =~ m/^(xml|xls|xlsx|pdf|htm|html| )+$/;
	
	$Config::dba->process_sql("INSERT INTO reports (title, formats, sourcefile,
						updatedbyuser, rec_timestamp)
						VALUES (?, ?, ?, ?, now())",
						[ $title, $formats, $sourcefile, $updatedbyuser ]);
	
	my ($id) = $Config::dba->process_oneline_sql("SELECT LAST_INSERT_ID()");
	return $id;
}



sub editReport
{
	my ($reportid, $title, $formats) = @_;
	
	return if !General::is_valid_id($reportid);
	return if !General::isAlphaNumeric($title);
	$formats = "" unless $formats =~ m/^(xml|xls|xlsx|pdf|htm|html| )+$/;
	
	
	$Config::dba->process_sql("UPDATE reports SET title=?, formats=?
						WHERE id=?", [ $title, $formats, $reportid ]);
}


sub getReports
{
	my ($reporttitle) = @_;
	
	#DEFAULT TO % IF WE AREN'T GIVEN A SEARCHSTRING
	return () if $reporttitle && !General::isAlphaNumeric($reporttitle);
	$reporttitle = "%" unless defined($reporttitle);
	
	my $reports = $Config::dba->hashed_process_sql("SELECT r.id, r.title, r.formats,
						r.sourcefile, u.username AS updatedbyuser,
						r.rec_timestamp AS timestamp FROM reports r
						INNER JOIN users u ON u.id=r.updatedbyuser
						WHERE title LIKE ?
						ORDER BY r.title", [ $reporttitle ]);
						
	return $reports ? @$reports : ();
}


sub getReport
{
	my ($reportid) = @_;
	
	return {} if !General::is_valid_id($reportid);
	
	my $reports = $Config::dba->hashed_process_sql("SELECT r.id, r.title, r.formats,
						r.sourcefile, u.username AS updatedbyuser,
						r.rec_timestamp AS timestamp FROM reports r
						INNER JOIN users u ON u.id=r.updatedbyuser
						WHERE r.id=?", [ $reportid ]);
						
	return $reports ? @{ $reports }[0] : {};
}


sub deleteReport
{
	my ($reportid) = @_;
	
	return if !General::is_valid_id($reportid);
	
	$Config::dba->process_sql("DELETE FROM reports WHERE id=?", [ $reportid ]);
}











1;
