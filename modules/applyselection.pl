#!/usr/local/bin/perl
package Applyselection;

use strict;
use warnings;
use Scalar::Util qw( looks_like_number );
use JSON qw( decode_json );
use Data::Dumper;
use Date::Simple ('date', 'today');
use JSON;
use POSIX qw/strftime/;
use Time::Local;
use Encode qw(decode encode);
# Home grown
use lib 'modules';

use XMLResponse qw ( :statuses );
use CTSConstants;
use Validate;

use JFElastic;

require "databaseJob.pl";
require "databaseOpcos.pl";
require "config.pl";
require "general.pl";
require "databaseAudit.pl";
require "Watchlist.pl";


##########################################################################################
###Applytoselection
##########################################################################################

sub ApplytoSelection{

    my $query       = $_[0];
    my $session     = $_[1];

    my $mode        = $query->param( "mode" ) || '';
    my $jobnumber   = $query->param( 'jobslist' );
    my $opcoid      = $session->param( "opcoid" );
    my $action		= $query->param( 'action' );
    my $subaction   = $query->param( 'subaction' );
	my $isdeveloper = $session->param('isdeveloper');
    my $broadcast   = 1;
	my $environment     = Config::getConfig("environment");
    ####here we have get advisory details
    my @querytypechecklist      = ECMDBOpcos::db_getquerytypes($opcoid);
    ###check the broadcast or not
    my @jobsplit    = split(/\,/, $jobnumber);
    foreach my $splitjobid (@jobsplit)
    {
        my $mediatype = ECMDBJob::db_getWorkflow($splitjobid);
        if ($mediatype ne "BROADCAST")
        {
                $broadcast = 0;
        }
    }
    my $vars = {
        querytypechecklist  =>\@querytypechecklist,
        broadcast           =>   $broadcast,
        jobnumber           => $jobnumber,
		isdeveloper			=> $isdeveloper,
		environment			=> $environment,
    };

    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }

}


###########################################################################################
####
#updateApplytoSelection
###########################################################################################

sub updateApplytoSelection {
    my ($query, $session)       = @_;

    my $advisoryid              = $query->param('Advisoryid');
    my $advisoryname            = $query->param('Advisoryname');
    my $joblist                 = $query->param('jobnumber');
#    my $estimationtime          = $query->param('estimationtime'); # PD-3500 estimation is no longer available
    my $scheduleDateTime        = $query->param('scheduleDateTime');
    my $dispatchDateTime        = $query->param('dispatchDateTime');
    my $commentsdata            = $query->param('commentsdata');
    my $broadcast               = $query->param('broadcastflag');
    my $mode                    = $query->param('mode') || '';
    my $watchlistflg			= $query->param('watchlistflg');
	my $requireddate			= $query->param('requireddate');
	my $materialdate			= $query->param('materialdate');
	my $opcoid					= $session->param('opcoid');
	my $userid					= $session->param('userid');
	my $username				= $session->param( 'username' );

    my $advisorystatus          = "";
	my $vars;
	my @history;

    #here updated advisory based on jobs
    my @jobs = split(/,/, $joblist);

	eval {

		# If the view is Watchlist, then it may contain jobs from different opcos
		# but this is true of Assigned to me, created by me too

		push( @history, "Watchlistflg '$watchlistflg'" ) if defined $watchlistflg;
		$advisoryid = '' unless defined $advisoryid;
		push( @history, "Advisory id '$advisoryid'" );
		$advisoryname = '' unless defined $advisoryname;

		my $loc_mismatch = 0;
		if ( ( defined $watchlistflg ) && ( $watchlistflg == 1 ) ) {
			foreach my $jobnumber (@jobs){
				my $location = ECMDBJob::db_getLocation($jobnumber);
				if ( $location != $opcoid ){
					$loc_mismatch = 1;
					push( @history, "Job $jobnumber at different location" );
				}
			}	
		}

		my $advisory_success = undef;
		if ( ($loc_mismatch == 0) && ($advisoryid ne '') ) {
            $advisorystatus = ECMDBGroups::db_updatequerytype( $advisoryid, $joblist );
			push( @history, "Setting advisory $advisorystatus ($advisoryid) to '$advisoryname' on $joblist" );
            foreach my $jobnumber (@jobs){
                ECMDBAudit::db_auditLog($userid, "querytype", $jobnumber, "Job: $jobnumber Advisory has been set to $advisoryname" );
                ECMDBJob::db_recordJobAction($jobnumber, $userid, CTSConstants::QUERYTYPEUPDATE);
				$advisory_success = Watchlist::Watchlistnotification( $userid, $opcoid, "Job $jobnumber, Advisory has been set to $advisoryname by $username", $jobnumber, "ADVISORY" );
            }
    	}

		#here we have update comments data
		#update_applycommentData($joblist,$commentsdata);

		my $lineitemid      = "";

		if ( $commentsdata ) {
			push( @history, "Updating comments" );
		}

		foreach my $job_number ( @jobs ) {
			push( @history, "Updating job $job_number" );
			if($commentsdata) {
        		my $parentopcoid    = ECMDBJob::db_getJobOpcoid( $job_number );
				my $customerid      = ECMDBJob::db_getCustomerIdJob( $job_number );
				my $isenabledelete  =  ECMDBJob::isDeleteEnabled( $customerid, $parentopcoid );
				ECMDBJob::addJobComment( $userid, $job_number, $lineitemid, $commentsdata );
				#Index in Elastic Search server
				$commentsdata =~ s/\{//;
				JFElastic::indexBrief( $parentopcoid, $job_number, $commentsdata );
				#Headers returns an array sorted in (sequence-)descending order,
				#so our freshly-added one should be at the top
				ECMDBJob::getJobCommentHeaders( $job_number, $lineitemid );
			}

			# Update Dispatch date/time
			if ( defined $dispatchDateTime && $dispatchDateTime ne '' ) {
				push( @history, "Updating dispatch date/time '$dispatchDateTime'" );
				ECMDBJob::db_updatedatetime( $job_number, $dispatchDateTime );
			}

			# Update Scheduled date/time
			if ( $broadcast && (defined $scheduleDateTime) && ($scheduleDateTime ne '') ) {
				push( @history, "Updating scheduled date/time '$scheduleDateTime'" );
				ECMDBJob::db_update_broadcast( $job_number, { scheduledate => $scheduleDateTime } );
			}
	
			 # Update Required date/time
            if ( defined $requireddate && $requireddate ne '' ) {
                push( @history, "Updating required date/time '$requireddate'" );
                ECMDBJob::db_update_requiredate( $job_number, $requireddate );
            }

			# Update Material date/time
			if ( $broadcast && (defined $materialdate) && ($materialdate ne '') ) {
				push( @history, "Updating material date/time '$materialdate'" );
				ECMDBJob::db_update_matdate( $job_number, $materialdate );
			}
		}
		$vars = { advisorystatus => $advisorystatus, history => \@history, }
	};
	if ( $@ ) {
		my $error = $@;
		warn "updateApplytoSelection: $error\n";
		$error =~ s/ at .*//gs;
		$vars = { error => $error, history => \@history };
	}
    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

1;

