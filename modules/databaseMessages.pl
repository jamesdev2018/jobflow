package ECMDBMessages;

use strict;
use warnings;


use Scalar::Util qw( looks_like_number );

use Scalar::Util qw( looks_like_number );

use lib 'modules';

require "config.pl";

##############################################################################
###   db_insertMessage
#############################################################################
sub db_insertMessage {

	my ( $jobid, $fromuserid, $touserid, $msgcontent, $recipient_ids, $group_conversation_id ) = @_;

    return 0 if $jobid && !looks_like_number($jobid);
    return 0 if $fromuserid && !looks_like_number($fromuserid);
    return 0 if $touserid && !looks_like_number($touserid);

    my $subject 			= "";
    my $status 				= 0;
	my $bindColumn			= undef;
	my $columnLists			= undef;
	my $valuesColumn		= undef;

	if ($group_conversation_id) {
		$columnLists	= 'job_number, fromuserid, touserid, subject, content, status, rec_timestamp, recipient_ids, group_conversation_id';
		$valuesColumn	= '?, ?, ?, ?, ?, ?, now(), ?, ?';
		$bindColumn		= [ $jobid, $fromuserid, $touserid, $subject, $msgcontent, $status, $recipient_ids, $group_conversation_id ];
	} else {

		$columnLists	= 'job_number, fromuserid, touserid, subject, content, status, rec_timestamp, recipient_ids ';
		$valuesColumn	= '?, ?, ?, ?, ?, ?, now(), ?';
		$bindColumn		= [ $jobid, $fromuserid, $touserid, $subject, $msgcontent, $status, $recipient_ids ];
	}

	$Config::dba->process_sql("INSERT INTO messages ( $columnLists )
                                VALUES ( $valuesColumn )",
                                $bindColumn);
	my ( $lastInsertID )	= $Config::dba->process_oneline_sql("SELECT LAST_INSERT_ID()");	
    return $lastInsertID;
}

###############################################################################
# db_getUsersForLookup
#
###############################################################################
sub db_getUsersForLookup
{

	my $opcoid	= $_[0];
	my $whereClause		= "WHERE STATUS=1 AND FNAME <> \'\' AND LNAME <> \'\'";
	my $bindingColumns	= [];

	if ( $opcoid ){
		$whereClause	= "$whereClause" . " AND opcoid = ?";
		$bindingColumns	= [ $opcoid ];
	}
    my $rows = $Config::dba->hashed_process_sql("SELECT ID,(CONCAT(FNAME, ' ', LNAME, ', ', USERNAME)) USERNAME FROM users $whereClause ORDER BY ID",$bindingColumns);
    return $rows ? @$rows : ();
}
###############################################################################
## db_getGroupUsersForLookup
##
################################################################################
sub db_getGroupUsersForLookup
{

    my $rows = $Config::dba->hashed_process_sql("SELECT ID, (CONCAT(FNAME, ' ', LNAME, ', ', USERNAME)) USERNAME 
												FROM users 
												WHERE STATUS = 1 AND FNAME <> '' AND LNAME <> '' ORDER BY ID");
    my $grows=$Config::dba->hashed_process_sql("SELECT ID, (CONCAT('group', ' ', 'name', ', ', message_group_name)) AS USERNAME  
												FROM message_group_name");
    push(@$rows,@$grows) if ( $grows );
    return $rows ? @$rows : ();
}

##############################################################################
####   db_getMessageCount
##
##############################################################################
sub db_getMessageCount
{
        my ($userid,$viewmessagedays) = @_;

        my ($messagecount) = $Config::dba->process_oneline_sql("SELECT COUNT(messageid) FROM messages WHERE touserid=? AND status=0 AND rec_timestamp >= DATE_SUB(CURDATE(), INTERVAL $viewmessagedays  DAY)", [ $userid ]);
        return $messagecount || 0;
}
##############################################################################
#####  db_getLatestJobnumber 
###
###############################################################################
sub db_getLatestJobnumber
{
        my ($touserid) = @_;
        my ($jobno) = $Config::dba->process_oneline_sql("SELECT job_number FROM messages WHERE touserid=? AND status=0 order by messageid desc limit 1", [ $touserid ]);
        return $jobno || 0;
}

##############################################################################
#####   db_getMessageList
###
###############################################################################
sub db_getMessageList
{
	my ( $userid, $relatedMessageJobNumber, $omitSentMessage, $viewmessagedays ) = @_;

	my $sql = "SELECT m.messageid, m.job_number, m.fromuserid,(CONCAT(u.FNAME, ' ', u.LNAME)) username, " .
        " m.content, m.rec_timestamp, m.status,  CASE WHEN m.fromuserid = ? THEN 'YES' END AS sentByselfFlag " .
		" FROM messages m, users u " .
		" WHERE m.fromuserid = u.id AND m.touserid = ? AND m.status in (0,1) ";
	my $bindColumns     = [ $userid, $userid ];

	if ( $relatedMessageJobNumber ne '' ) {
		$sql .= '  AND (m.job_number = ?) ';
		push( @{$bindColumns}, $relatedMessageJobNumber );
	}

	if ( lc( $omitSentMessage ) eq 'yes' ) { # If set, ignore messages sent by current user to self
		$sql .= ' AND (m.fromuserid <> ?) ';
		push( @{$bindColumns}, $userid );
	}

	$sql .= " AND m.rec_timestamp >= DATE_SUB(CURDATE(), INTERVAL $viewmessagedays  DAY) ORDER BY m.messageid DESC;";
	
	my $rows = $Config::dba->hashed_process_sql( $sql, $bindColumns );

    return $rows ? @$rows : ();
}

##############################################################################
######   db_updateMessage
####
################################################################################
sub db_updateMessage
{
    my ($messageid) = @_;

    return 0 if $messageid && !looks_like_number($messageid);

$Config::dba->process_sql("UPDATE messages SET read_timestamp=now(), status=1  WHERE messageid=? ",[ $messageid ]);
    return 1;
}

################################################################################
#######   db_deleteMessage
#####
#################################################################################
sub db_deleteMessage
{
    my ($messageid) = @_;

	$Config::dba->process_sql("UPDATE messages SET status=2  WHERE messageid in $messageid ");
    return 1;
}

##############################################################################
######   db_getMessageView
####
################################################################################
sub db_getMessageView
{
    my ( $messageid, $sentByselfFlag ) = @_;

	my $whereClause			= '';
	if ( ( defined $sentByselfFlag ) && ( lc($sentByselfFlag) ne 'yes' ) ) {
		$whereClause	= ' AND (m.fromuserid <> touserid) ';
	}

	my $rows = $Config::dba->hashed_process_sql(
		"SELECT m.messageid, m.job_number, m.fromuserid,(CONCAT(u.FNAME, ' ', u.LNAME)) username, m.content, m.rec_timestamp, m.status " .
		"FROM messages m " .
        "JOIN users u on fromuserid = u.id " .
		"WHERE ( m.messageid = ? OR m.group_conversation_id = ? ) $whereClause " .
		"GROUP BY m.content ORDER BY rec_timestamp DESC", 
		[ $messageid, $messageid ] );

    return $rows ? @$rows : ();
}

##############################################################################
#######   get MAX message id from DB
#####
#################################################################################
sub getMessageID
{
        my ($jobid) = @_;

        my ($rowid) = $Config::dba->process_oneline_sql("SELECT MAX(messageid) AS ID FROM messages WHERE job_number=?", [ $jobid ]);
        return $rowid;
}
##############################################################################
####   db_insertMessagegroup
##############################################################################
sub db_insertMessagegroup
{
    my ( $groupname, $touserid, $fromuserid, $msgGroupID ) = @_;

   # return 0 if $groupname && !looks_like_number($groupname);
    return 0 if $touserid && !looks_like_number($touserid);

	$Config::dba->process_sql("INSERT INTO messagegroup( USERID, GROUPNAME, REC_TIMESTAMP, ownerid, msg_group_ID ) 
								VALUES ( ?, ?, now(), ?, ? )", [ $touserid, $groupname,$fromuserid, $msgGroupID ]);
    return 1;
}
##############################################################################
#####   db_deletegroup
###############################################################################
sub db_deletegroup
{
    my ($groupname) = @_;
    $Config::dba->process_sql("DELETE FROM  messagegroup  WHERE groupname =? ", [ $groupname ]);
    return 1;
}
##############################################################################
#	db_checkGroupname: To ensure group name already exists or not.
###############################################################################

sub db_checkGroupname {

	my ( $groupname ) 	= @_;
	my ( $userscount ) 	= $Config::dba->process_oneline_sql("SELECT COUNT(USERNAME) FROM users WHERE USERNAME=? ", [ $groupname ]);
	my ( $groupcount ) 	= $Config::dba->process_oneline_sql("SELECT COUNT(message_group_name) FROM message_group_name  WHERE message_group_name=? ", [ $groupname ]);

	if ( $userscount == 0 && $groupcount == 0 ) {
		return $userscount;
	}
	return 1;
}


###############################################################################
### db_getGroupName
###
#################################################################################
sub db_getGroupName
{
    my ($owner_id) = @_;
    my $rows=$Config::dba->hashed_process_sql("SELECT DISTINCT(groupname) AS groupname,'g' as type  FROM messagegroup where ownerid=?",[$owner_id]);
    return $rows ? @$rows : ();
}
###############################################################################
###### db_editGroupName
######
####################################################################################
sub db_editGroupName
{
    my ($oldgroupname,$newgroupname) = @_;
    $Config::dba->process_sql("UPDATE messagegroup SET GROUPNAME =? WHERE groupname =? ", [ $newgroupname,$oldgroupname ]);
    return 1;
}

###############################################################################
#### db_deleteGroupName
####
##################################################################################
sub db_deleteGroupName
{
    my ($groupname) = @_;
    $Config::dba->process_sql("DELETE  FROM messagegroup WHERE groupname =? ", [ $groupname ]);
    $Config::dba->process_sql("DELETE  FROM message_group_name WHERE message_group_name =? ", [ $groupname ]); #Delete from master table.
    return 1;
}
###############################################################################
##### db_getUserName
#####
###################################################################################
sub db_getUserName
{
    my ($userid)   = @_;
     my ($username) = $Config::dba->process_oneline_sql("SELECT USERNAME FROM users WHERE ID=?", [ $userid ]);
        return $username;
}
#############################################################################
######db_getGroupDetails()
####
#############################################################################
sub db_getGroupDetails
{
   my ($groupname,$owner_id) = @_;
   my $rows=$Config::dba->hashed_process_sql("SELECT m.ID,m.USERID,M.GROUPNAME,u.username as uname,(CONCAT(u.FNAME, ' ', u.LNAME, ', ', u.USERNAME)) USERNAME   FROM messagegroup m INNER JOIN users u ON u.ID=m.USERID WHERE GROUPNAME=? AND ownerid = ?",[$groupname, $owner_id]);
   return $rows ? @$rows : ();
}

#############################################################################
# db_getRecipientLists(): To get the list of recipients lists to send reply message.
#############################################################################
sub db_getRecipientLists {
   	my ( $message_ID, $user_ID ) = @_;

   	my ( $recipient_IDs )    = $Config::dba->process_oneline_sql( "SELECT recipient_ids USERNAME   FROM messages m
                                                WHERE messageid= ?",[ $message_ID ] );
	$recipient_IDs = '' unless defined $recipient_IDs;

	# warn "db_getRecipientLists: entry messageid $message_ID, userid $user_ID, recips $recipient_IDs\n";

	my @other_users;
	my @groups;

	foreach my $id ( split /,/, $recipient_IDs ) {
		if ( looks_like_number( $id ) ) {
			push( @other_users, $id ) if ( $id != $user_ID ); # Eliminate current user from recipient list
		} elsif ( $id =~ m/[0-9]+grp/ ) {
			my ( $group ) = $id =~ m/([0-9]+)grp/;
			push( @groups, $group );
		}
	}
	$recipient_IDs = join( ',', @other_users );
	my $groupMessageUsers = join(',',@groups );

	# warn "db_getRecipientLists: after split recips $recipient_IDs, groups $groupMessageUsers\n";

	my @combined;
	if ( $recipient_IDs ) {
    	my ( $recipientNameLists ) = $Config::dba->process_oneline_sql( 
			"SELECT GROUP_CONCAT(FNAME, ' ', LNAME, ', ', USERNAME SEPARATOR ';') as USERNAME FROM USERS U WHERE U.id IN ( $recipient_IDs )" );
		push( @combined, $recipientNameLists ) if defined $recipientNameLists;
	}
	if ( $groupMessageUsers ) {
		my ( $groupRecipientLists )	= $Config::dba->process_oneline_sql(
			"SELECT GROUP_CONCAT('group', ' ', 'name', ', ', message_group_name  separator ';') AS USERNAME " .
			" FROM message_group_name WHERE ID IN ( $groupMessageUsers )" );
		push( @combined, $groupRecipientLists ) if defined $groupRecipientLists;
	}
	# warn "db_getRecipientLists: exit combined " . join( ';', @combined ) . "\n";
	return join( ';', @combined );

} #db_getRecipientLists

#############################################################################
# db_getMessageParentConversationID(): To get the conversation parrent ID.
#############################################################################

sub db_getMessageParentConversationID {

	my ( $messageID )   = @_;
	my ( $parentConversationID ) = $Config::dba->process_oneline_sql("SELECT M.group_conversation_id 
										FROM messages M
										WHERE M.messageid =? ", [ $messageID ]);
	return $parentConversationID ? $parentConversationID : $messageID;
}# db_getMessageParentConversationID


#############################################################################
# db_getReleateMessageJobnumber(): 
#############################################################################
sub db_getReleateMessageJobnumber {

    my ( $messageID )   = @_;
    my ( $releateMessageJobnumber ) = $Config::dba->process_oneline_sql("SELECT M.job_number
                                        FROM messages M
                                        WHERE M.messageid =? ", [ $messageID ]);
    return $releateMessageJobnumber;
}# db_getReleateMessageJobnumber

#############################################################################
# db_getReleateMessageJobnumber(): 
#############################################################################
sub db_createMsgGrpName {

	my $groupAdminID			= $_[0];
	my $message_group_name		= $_[1];	
	$Config::dba->process_sql("INSERT INTO message_group_name ( message_group_name, group_admin_ID, REC_TIMESTAMP )
                                VALUES ( ?, ?, now() )", [ $message_group_name, $groupAdminID ]);
	my ( $lastInsertID )    = $Config::dba->process_oneline_sql("SELECT LAST_INSERT_ID()");
	return $lastInsertID;
} #db_createMsgGrpName

#############################################################################
# db_getGroupMsgRecipientIDs(): 
#############################################################################
sub db_getGroupMsgRecipientIDs {

	my $msgGrpID		= $_[0];

	my ( $releateMessageJobnumber ) = $Config::dba->process_oneline_sql("SELECT 1 FROM message_group_name WHERE ID=?", [ $msgGrpID ] );

	if ( $releateMessageJobnumber ) {
		my $rows=$Config::dba->hashed_process_sql("select USERID from messagegroup WHERE msg_group_ID = ?", [ $msgGrpID ]);
    	return $rows ? @$rows : ();		
	} else {
		return 0;
	}
}#db_getGroupMsgRecipientIDs

#############################################################################
# db_getMsgGroupID():
#############################################################################
sub db_getMsgGroupID {

    my $msgGroupName        = $_[0];

    my ( $msgGroupNameID ) = $Config::dba->process_oneline_sql("SELECT id FROM message_group_name WHERE message_group_name=?", [ $msgGroupName ] );

	return $msgGroupNameID;
}#db_getMsgGroupID


1;
