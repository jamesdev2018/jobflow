#!/usr/local/bin/perl
package Outsource;


use strict;
use warnings;

use Template;

use lib 'modules';

require "databaseOutsource.pl";

require "companies.pl";
require "opcos.pl";
require "general.pl";

my $usersession;

#######################################
## Template Object
#######################################
my $tt = Template->new({
	RELATIVE => 1,
}) || die "$Template::ERROR\n";



#######################################
# List outsources
#######################################
sub ListOutsources
{
	my $query = $_[0];
	my $session = $_[1];
	my $srcopco_filter = $query->param("srcopcofilter");
	my $dstopco_filter = $query->param("dstopcofilter");
	my $ttpfolder_filter = $query->param("ttpfolderfilter");
	my $pairing_filter = $query->param("pairingfilter");
    my $sidemenu   = $query->param( "sidemenu" );
    my $sidesubmenu= $query->param( "sidesubmenu" );
    my $roleid     = $session->param( 'roleid' );

	
	my @outsources = ECMDBOutsource::db_getOutsources($srcopco_filter, $dstopco_filter, $ttpfolder_filter, $pairing_filter);
	my @opcos = Opcos::getOperatingCompanies();
	
	my $vars = {
		outsource => \@outsources,
		opcos	  => \@opcos,
        sidemenu    => $sidemenu,
        sidesubmenu => $sidesubmenu,
    	roleid       =>$roleid,
	};

	General::setHeaderVars( $vars, $session );
	
	$tt->process('ecm_html/listoutsources.html', $vars)
		|| die $tt->error(), "\n";
}



######################################
# Add outsources
######################################
sub AddOutsource
{
	my $query = $_[0];
	my $session = $_[1];

	my $sidemenu   = $query->param( "sidemenu" );
	my $sidesubmenu= $query->param( "sidesubmenu" );
	my $roleid     = $session->param( 'roleid' );
	
	my $srcopco = $query->param("srcopcoid");
	my $dstopco = $query->param("dstopcoid");
	my $userid = $session->param("userid");
	my $ttpfolder = General::htmlEditFormat($query->param("ttpfolder"));
	my $formposted = $query->param("formPosted") ||'';

	my $okaytogo = 1;
	my %error;
	$error{ okaytogo } = 1;
	
	if( $formposted eq "yes" )
	{
		%error = ValidateOutsource($srcopco, $dstopco, $ttpfolder);
		
		if( $error{ oktogo } == 1)
		{
			ECMDBOutsource::addOutsource($userid, $srcopco, $dstopco, $ttpfolder);

			my $vars = { };  # template has no variables embedded
			General::setHeaderVars( $vars, $session );

			$tt->process('ecm_html/outsourceaddedsuccess.html', $vars)
				|| die $tt->error(), "\n";
			return;
		}
	}
	
	my @opcos = Opcos::getOperatingCompanies( );
	
	my $vars = {
		opcos		=> \@opcos,
		srcopcoid	=> $srcopco,
		dstopcoid	=> $dstopco,
		ttpfolder	=> $ttpfolder,
		error		=> \%error,
		sidemenu	=> $sidemenu,
		sidesubmenu	=> $sidesubmenu,
		roleid		=> $roleid,
	};
	
	General::setHeaderVars( $vars, $session );

	$tt->process('ecm_html/addoutsource.html', $vars)
		|| die $tt->error(), "\n";
}


###################################
## Validate outsources
###################################
sub ValidateOutsource
{
	my ( $srcopco, $dstopco, $ttpfolder, $update ) = @_;
	my %error;
	
	$error{ oktogo } = 1;
	
	if(length($ttpfolder) < 1)
	{
		$error{ ttpfolder } = "TTP Folder must be longer than 0 characters";
		$error{ oktogo } = 0;
	}
	elsif (!($ttpfolder =~ /^\/.*/))
	{
		$error{ ttpfolder } = "TTP Folder must be in the form of an absolute path (i.e. /folder)";
		$error{ oktogo } = 0;
	}

	if(!($update eq "update") and PairingExists($srcopco, $dstopco))
	{
		$error{ dstopco } = "OpCo pairing for this direction already exists.";
		$error{ oktogo } = 0;
	}	
	
	return %error;
}



##################################
## Check if outsource pairing exists
##################################
sub PairingExists
{
	my $srcopco = $_[0];
	my $dstopco = $_[1];
	
	return ECMDBOutsource::pairingExists($srcopco, $dstopco);
}

##################################
## Edit an outsource pairing
##################################
sub EditOutsource
{
	my $query = $_[0];
	my $session = $_[1];
	
	my (@opcos, $formposted, %error, $srcopcoid, $dstopcoid, $srcopconame, $dstopconame, $userid, $ttpfolder);
	$srcopcoid = $query->param("srcopcoid");
	$dstopcoid = $query->param("dstopcoid");
	$userid = $session->param("userid");
	$formposted = $query->param("formPosted") ||'';
	
	my %pair = ECMDBOutsource::getOutsource($srcopcoid, $dstopcoid);
	$srcopconame = $pair{ srcopconame };
	$dstopconame = $pair{ dstopconame };
	
	if($formposted eq "yes")
	{
		$ttpfolder = General::htmlEditFormat( $query->param("ttpfolder") );
	}
	else
	{
		$ttpfolder = $pair{ ttpfolder };
	}
	
	if($formposted eq "yes" )
	{
		%error = ValidateOutsource($srcopcoid, $dstopcoid, $ttpfolder, "update");
	
		if( $error{ oktogo } == 1)
		{
			ECMDBOutsource::updateOutsource($srcopcoid, $dstopcoid, $ttpfolder, $userid);
		}
	}
	
	
	my $vars = {
		opcos		=> \@opcos,
		srcopcoid	=> $srcopcoid,
		dstopcoid	=> $dstopcoid,
		srcopconame	=> $srcopconame,
		dstopconame	=> $dstopconame,
		ttpfolder	=> $ttpfolder,
		error		=> \%error,
		};

	General::setHeaderVars( $vars, $session );
	
	$tt->process('ecm_html/editoutsource.html', $vars)
		or die $tt->error(), "\n";
}


##################################
## Delete Outsource
##################################
sub DeleteOutsource
{
	my $query = $_[0];
	my $srcopco = $query->param("srcopcoid");
	my $dstopco = $query->param("dstopcoid");

	return ECMDBOutsource::deleteOutsource($srcopco, $dstopco);
}
