﻿#!/usr/local/bin/perl

use strict;
use warnings;

package LEGACY;

#----!-IMPORTANT-!-IMPORTANT-!-IMPORTANT-!----#
#                                             #
##    ABSOLUTELY DO *NOT* TOUCH THIS FILE    ##
##    UNLESS YOU KNOW  WHAT YOU ARE DOING    ##
##   AND HAVE CONSULTED A SENIOR DEVELOPER   ##
#                                             #
#----!-IMPORTANT-!-IMPORTANT-!-IMPORTANT-!----#



#  - This file is for temporary hacks whilst bad code
#    is phased out of use.
#  - Comment and date ALL additions, including a
#    description of its purpose with a ticket number.



## PD-2404, Brian Dalton [2016-07-11]
##-----------------------------------
## A mapping from the old 'actions' list to the newer
## 'action/subaction' list until all of the old links
## have been phased out. Keeping it here so people aren't
## tempted to keep using the old form whilst this is done.
sub upgrade_action_map
{
	my ($action) = @_;
	
	my $mapping = {	# ordered by category

		broadcastapplypopup				=> 'broadcast/get_details',
		broadcastapplyupdate			=> 'broadcast/apply_update',
		updatebroadcasttable			=> 'broadcast/save',
		update_status_notes				=> 'broadcast/update_status_notes',
		broadcastupdatedatetime			=> 'broadcast/updatedatetime',

		# cdn bundle
		sendemailbundle					=> 'bundle/sendemail',
		downloadbundle					=> 'bundle/download',
		findassets						=> 'bundle/find_assets',
		makebundle						=> 'bundle/make_bundle',
	
		applychecklistreorder			=> 'checklist/reorder_template',
		savedescriptiondata				=> 'checklist/update_template',
		savejobchecklist				=> 'checklist/save_checklist',
		editchecklist					=> 'checklist/view_template',

		# and some more checklist from the legacy list (just set the subaction to the action name)
		checklistcustomers				=> 'checklist/checklistcustomers',
		checklist						=> 'checklist/checklist',
		updatedescriptiondata			=> 'checklist/updatedescriptiondata',
        savecombovalues                 => 'checklist/savecombovalues',
		deletestagetask					=> 'checklist/deletestagetask',
		showchecklistedit				=> 'checklist/showchecklistedit',
		checklistcompanylist				=> 'checklist/companylist',
		checklistroles    				=> 'checklist/checklistroles',

		addjobcomment					=> 'comment/add',
		updatejobcomment				=> 'comment/save',

		listcompanies					=> 'company/list',
		editcompany						=> 'company/save',
		addcompany						=> 'company/add',

		contentcompare					=> 'compare/content',
		escompare						=> 'compare/es',

		addcustomer						=> 'customer/add',
		editcustomer					=> 'customer/save',
		listcustomers					=> 'customer/list',
		addcustomeruser					=> 'customer/link_user',
		removecustomeruser				=> 'customer/unlink_user',
		addcustomerftp					=> 'customer/link_server',
		editcustomerftp					=> 'customer/save_server_link',
		changeftp						=> 'customer/save_server_alternate',

		dashboard						=> 'jobdashboard/view',

		kanbanboard						=> 'kanban/kanbanboard_view',
		updatekanbanboard				=> 'kanban/updatekanbanboard',
		swimlaneconfig					=> 'kanban/swimlaneconfig',
		getswimlaneorderbyvalues		=> 'kanban/getswimlaneorderbyvalues',
		updateswimlane					=> 'kanban/updateswimlane',
		filterowneraccess				=> 'kanban/filterowneraccess',
		swimlaneuserlist				=> 'kanban/swimlaneuserlist', 
		checkqcstatus					=> 'kanban/checkqcstatus',
		kanban_get_comments				=> 'kanban/kanban_get_comments',

		newdashboard					=> 'dashboard/view',
		dashboardcalendar				=> 'dashboard/view_calendar',
		dashboardcalendaredit			=> 'dashboard/edit_calendar',
		dasshboard_filter				=> 'dashboard/dasshboard_filter',
		updateresource					=> 'dashboard/update_resource',

		constructassetdetails			=> 'digitalasset/constructassetdetails',
		reviewdigitalasset				=> 'digitalasset/reviewdigitalasset',
		deleteallassets					=> 'digitalasset/remove_all',

		alterestimate					=> 'estimate/alterestimate',
		updateestimate					=> 'estimate/save',
		processnewestimate				=> 'estimate/processnewestimate',

		downloaddoc					=> 'file/download_document',
		deletecache					=> 'file/remove_cache',
		sendrenderedpdftocmd		=> 'file/send_pdf_to_cmd',
		renderpdf					=> 'file/renderpdf',

		listforms					=> 'form/list',
		editform					=> 'form/view',
		forms						=> 'form/view_alternate',
		getbranddetails				=> 'form/get_brand',
		getbrandownerdetails		=> 'form/get_brandowner',
		formsendxml					=> 'form/send_xml',
		showabform					=> 'form/view_abinbev',
		saveabinbevform					=> 'form/save_abinbev',
		committoproduction				=> 'form/commit_to_production',
		getprinterdetails				=> 'form/get_printer',
		openfileupload					=> 'form/open_fileupload',
		uploadtas						=> 'form/upload_tas_document',
		uploadtastemp					=> 'form/upload_tas_document_temp_location',
		deletetemptasdocument			=> 'form/delete_temp_tas_document',
		getcutterrefdetails				=> 'form/get_cutter_ref',
		getpackformatdetails			=> 'form/get_packformat_details',
		gettemplaterefdetails			=> 'form/get_templateref_details',
		getallergensunderlined			=> 'form/get_allergens_underlined',

		checkgroupname					=> 'group/checkgroupname',
		sendgrouplist					=> 'group/sendgrouplist',
		showcreategroup					=> 'group/showcreategroup',
		viewgroupname					=> 'group/viewgroupname',
		viewgroupdetails				=> 'group/viewgroupdetails',
		deletegroupname					=> 'group/deletegroupname',

		bookimage					=> 'imageworkflow/add',
		editimageworkflow				=> 'imageworkflow/save',

		restorejob					=> 'job/restore',
		requestjob					=> 'job/request',
		takejob						=> 'job/take',
		assignjobs					=> 'job/assign',
		unlock						=> 'job/unlock',
		completejob					=> 'job/complete',
		rejectjob					=> 'job/reject',
		viewhistory					=> 'job/view_history',
		activity					=> 'job/view_activity',
		showlogs					=> 'job/view_log',
		checkin						=> 'job/checkin',
		changelocation					=> 'job/changelocation',
		qc_status					=> 'job/qc_status',
		showtaskprocessdialog				=> 'job/showtaskprocessdialog',
		processcheckin					=> 'job/processcheckin',
		setlocation					=> 'job/setlocation',
		recordopenjob					=> 'job/recordopenjob',

		updatedepartment				=> 'legacy/updatedepartment',
		updateteam						=> 'legacy/updateteam',
		updatequerytype					=> 'legacy/updatequerytype',

		getlanguagelist					=> 'legacy/getlanguagelist',  # TODO1
		rejectjobsacctman				=> 'legacy/rejectjobsacctman',

		checkinqueue					=> 'maintenance/list_repo_checkin',
		checkoutqueue					=> 'maintenance/list_repo_checkout',
		checkinqueuehistory				=> 'maintenance/list_repo_history',
		daemons							=> 'maintenance/list_daemons',

		masters							=> 'masters/masters',
		mastersqueue					=> 'masters/mastersqueue',
		jrs								=> 'masters/jrs',

		viewmessage						=> 'message/view',
		deletemessage					=> 'message/remove',
		sendmessage					=> 'message/add',
		showmessagelist					=> 'message/list',
		getmessagecount					=> 'message/get_count',
		showsendmessage					=> 'message/showsend',
		checkjoblocation				=> 'message/checkjoblocation',

		addnews						=> 'news/add',
		newssummary					=> 'news/list',
		listnews					=> 'news/list_alternate',
		editnews					=> 'news/save',

		#One2Edit
		one2edit					=> 'one2edit/action_handler',

		listoperatingcompanies				=> 'opco/list',
		viewoperatingcompany				=> 'opco/view',
		addoperatingcompany				=> 'opco/add',
		editoperatingcompany				=> 'opco/save',
		adduserstoopco					=> 'opco/link_user',
		editoperatingcompanyvendors			=> 'opco/save_vendor',
		getmediaestimation				=> 'opco/getmediaestimation',

		archivescheduler				=> 'opcocmd/archivescheduler',
		opcoadmincmd					=> 'opcocmd/opcoadmincmd',

		addoutsource					=> 'outsource/add',
		listoutsource					=> 'outsource/list',
		editoutsource					=> 'outsource/save',
		deleteoutsource					=> 'outsource/remove',

		startproduct					=> 'product/start',
		pauseproduct					=> 'product/pause',
		stopproduct					=> 'product/stop',
		getproductrestartreasons			=> 'product/list_reasons',
		addrevisionreason				=> 'product/addrevisionreason',
        updateproductendtime            => 'product/updateProductChecklistEndTime',
		viewtimecapture					=> 'product/view',
		viewproducthistory				=> 'product/view_history',
		assignproduct					=> 'product/link_operator',
		finishprdassigntodept			=> 'product/link_department',
		excludeproduct					=> 'product/exclude_product',
		getlatestversionproducts		=> 'product/get_latest_version_products',
	
		listreports					=> 'report/list',
		showreport					=> 'report/view',
		addreport					=> 'report/add',
		editreport					=> 'report/save',
		deletereport					=> 'report/remove',

		reposendjobs					=> 'repository/send_opts',
		repocheckin					=> 'repository/checkin_opts',
		repocheckout					=> 'repository/checkout_opts',
		processreposendjobs				=> 'repository/send',
		processrepocheckin				=> 'repository/checkin',
		processrepocheckout				=> 'repository/checkout',
		retryjob					=> 'repository/retry',
		resetrepojob					=> 'repository/reset',
		fixrepojob					=> 'repository/fix',
		fixjobs						=> 'repository/list',   # TODO check	 whether this is right list v fixjobs => RepoJobs::FixRepoJobs
											# from jobflow.pl back in June 2016	# and now
											# action				# action => category => function
											# fixJobs => RepoJobs::FixRepoJobs	# fixJobs => repository/list => RepoJobs::FixRepoJobs
											# fixrepojob => RepoJobs::BaseRepoJob	# fixrepojob => repository/fix => RepoJobs::BaseRepoJob

		searchjobs					=> 'search/searchjobs',

		approve						=> 'sendjobs/approve',
		vendorassetdownload				=> 'sendjobs/download_asset',
		getlowreszip					=> 'sendjobs/download_lowres',
		viewlowres					=> 'sendjobs/view_lowres',
		viewpreflight					=> 'sendjobs/viewpreflight',
		gethires					=> 'sendjobs/gethires',
		choosedestination                               => 'sendjobs/choosedestination',
		sendbyemail					=> 'sendjobs/sendbyemail',
		sendbyemailclient				=> 'sendjobs/sendbyemailclient',
		sendbyemailmediaowner				=> 'sendjobs/sendbyemailmediaowner',
		sendbyemailvendor				=> 'sendjobs/sendbyemailvendor',
		sendbyemaildigital				=> 'sendjobs/sendbyemaildigital',
		sendbyemailindex				=> 'sendjobs/sendbyemailindex',
		sendbyftp					=> 'sendjobs/sendByftp',
		adgatedeliver					=> 'sendjobs/adgatedeliver',
		sendbyproject				=> 'sendjobs/sendbyproject',	

		showjob						=> 'showjob/showjob',
		uploadselectedallassets		=> 'showjob/upload_asset',
		removetempdocuments				=> 'showjob/removetempdocuments',
        checkcampaign               => 'showjob/checkcampaign',

		showversion					=> 'document/show_version',
		rollback					=> 'document/roll_back',
		deleteasset					=> 'document/delete_asset',
		deletedocument				=> 'document/delete_document',

		digitalshowversion			=> 'digitaltab/show_version',
		digitalrollback				=> 'digitaltab/roll_back',
		digitaldeleteasset			=> 'digitaltab/delete_asset',
		staticfileupload			=> 'digitaltab/static_file_upload',
		digitaldeletefile			=> 'digitaltab/digital_delete_file',

		support						=> 'support/support',

		upload						=> 'upload/upload',

		applycolumnmanager				=> 'usercolumnmanager/apply',
		updatecolumnmanager				=> 'usercolumnmanager/update',
		showcolumnmanagerdialog				=> 'usercolumnmanager/showcolumnmanagerdialog',

		listusers					=> 'useredit/list',
		#adduser					=> 'useredit/add',
		addvendor					=> 'useredit/add_vendor',
		edituser					=> 'useredit/save',
		settings					=> 'useredit/view_settings',
		deleteuser					=> 'useredit/remove',
		dashboardeditresource		=> 'useredit/dashboardeditresource',
		#									# TODO1 Missing user/save_settings

		savefilter					=> 'userfilter/save_filter',
		deletefilter				=> 'userfilter/remove_filter',
		renamefilter				=> 'userfilter/rename_filter',
		loadfilter					=> 'userfilter/get_filter',
		showsavefilterdialog		=> 'userfilter/showsavefilterdialog',
		updatefilterpinning			=>	'userfilter/filterpinning',

		processlogin					=> 'userlogin/login',
		logout						=> 'userlogin/logout',
		changeroleid					=> 'userlogin/rolechange', # TODO is really part of Account Settings so should be elsewhere
		sessiondetails					=> 'userlogin/sessiondetails', # Ditto

		addwatchlist					=> 'watchlist/add',
		stopwatchlist					=> 'watchlist/remove',

		digitalindexpage				=> 'digital/show',
		
		viewjobimport				=> 'jobimport/view',
		searchjobimport				=> 'jobimport/search',
		viewjobimportsettings		=> 'jobimport/viewsettings',
		editjobimportfield			=> 'jobimport/editjobimportfield',
		savejobimportfield			=> 'jobimport/savejobimportfield',
		deletejobimportfield		=> 'jobimport/deletejobimportfield',
		viewjisubstitutions		=> 'jobimport/viewimportsubstitutions',
		editjisubstitution		=> 'jobimport/editjobimportsubstitution',
		savejisubstitution		=> 'jobimport/savejobimportsubstitution',
		deletejisubstitution	=> 'jobimport/deletejobimportsubstitution',

	
		#applyselection
		updateapplytoselection => 'job/updateapplytoselection',
		applyselection  => 'job/applyselection',
		
		

	};
	
	return $action if($action =~ m/\//);
	return $mapping->{$action} || "legacy/$action";
};					
##// END PD-2404 //

1;
