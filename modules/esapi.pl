#!/usr/local/bin/perl
package ESAPI;

use strict;
use warnings;

use JSON;
use Data::Dumper;
use REST::Client;
use MIME::Base64;

# These constants should be moved to a properties file

use constant DEFAULT_HOST => 'http://172.27.93.190:8080';

use constant DEFAULT_RPC_PATH => '/Esprit/public/Interface/rpc';

use constant DEFAULT_USER_PROFILE => "Customer";

my $jsondebug = 0;
my $debug = 0;
my $saved_auth;

my $rpcpath = ""; # default is DEFAULT_RPC_PATH, set by connect otherwise

# -- Helper functions

# Basic connect (creates instance of REST::Client)

sub connect {

	my ( $host, $path, $user, $pass, $timeout ) = @_;

	$host = DEFAULT_HOST unless defined $host;

	$path = DEFAULT_RPC_PATH unless ( ( defined $path ) && ( $path ne "" ) );
	$rpcpath = $path;

	die "User not supplied" if ( ( ! defined $user ) || ( $user eq "" ) );
	die "Pass not supplied" if ( ( ! defined $pass ) || ( $pass eq "" ) );

	# Default to 10 second timeout
	$timeout = 10 unless $timeout;

	my $client = REST::Client->new();
	$client->setHost( $host );
	$client->addHeader( "Content-Type",  "application/json" );
	my $auth = "Basic " . MIME::Base64::encode( $user . ":" . $pass );
	chomp $auth;	
	$client->addHeader( "Authorization", $auth );

	$saved_auth = $auth; # Used in doPost for diagostic message

	return $client;
}

# Find the element in the array where hash key exists and has value specified

sub findElem {
	my ( $result, $key, $value ) = @_; # array ref returned by doPost and method/id to find

	my $found;
	foreach my $elem ( @$result ) {
		if ( ( exists $elem->{$key} ) && ( $elem->{$key} eq $value ) ) {
			$found = $elem;
			last;
		}
	}
	return $found;
}

# Extract the error text if {error} is found in the response

sub errorText {
	my ( $error ) = @_;   # Hash

	return $error->{data}->{longMessage} if ( ( exists $error->{data} ) && exists $error->{data}->{longMessage} );

	return Dumper( $error );
}

# Helper POST method

sub doPost {
	my ( $client, $request ) = @_; # Request is array ref

	my @payload; # Used to construct JSON payload
	my @actual;  # Actual response to caller
	my $login;
	my $logout;
	my $json_request;
	my $json_response;
	my $response;
	my $actual_response; # Decoded JSON response
	my $rc;

	push ( @payload, { id => 1, method => 'admin.login' } );
	foreach my $elem ( @$request ) {
		push ( @payload, $elem );
	} 
	push ( @payload, { id => 2, method => 'admin.logout' } );

	eval {
		$json_request = encode_json( \@payload );

		print "JSON Request: $json_request\n" if $jsondebug;

		$response = $client->POST( $rpcpath, $json_request );

		$rc = $client->responseCode();
		die "POST returned HTTP $rc" unless ( $rc eq "200" );

		$json_response = $client->responseContent();
		die "No JSON response" unless ( ( defined $json_response ) && ( $json_response ne "" ) );

		print "JSON Response: $json_response\n" if $jsondebug;
		$actual_response = decode_json( $json_response );
		print "Response: " . Dumper( $actual_response ) . "\n" if $jsondebug;
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//g unless ( $jsondebug );
		if ( ! defined $json_request ) {
			die "doPOST, $error, error encoding " . Dumper( $request );
		} elsif ( ( $rc ne "200" ) || ( ! defined $json_response ) || ( $json_response eq '' ) ) {
			die "doPOST, $error, request '$json_request', host: " . $client->getHost();
		} else {
			die "doPOST, $error, request '$json_request', JSON response '$json_response', host: " . $client->getHost();
		}
	}

	foreach my $elem ( @$actual_response ) {
		if ( ( exists $elem->{method} ) && ( $elem->{method} =~ /^admin\.(login|logout)$/ ) ) {
			if ( $elem->{method} eq "admin\.login" ) {
				$login = $elem;
			} else {
				$logout = $elem;
			}
		} else {
			push ( @actual, $elem )
		}
	}
	die "Method 'admin.login' not seen in response" unless defined $login;
	die "Login not ok" unless ( ( defined $login->{result} ) && ( $login->{result}->{status} eq 'loggedIn' ) );

	die "Method 'admin.logout' not seen in response" unless defined $logout;
	die "Login not ok" unless ( ( defined $logout->{result} ) && ( $logout->{result}->{status} eq 'loggedOut' ) );

	return ( \@actual, $login, $logout );
}

# Wrapper for doPost to check for the response to the actual method requested, check for the result
# object and handle presence of the error object.

sub doPostAndGetResult {
	my ( $client, $method, $params ) = @_; 

	die "Client arg missing" unless ( defined $client );
	die "Params arg missing" unless ( defined $params );
	die "Method arg missing" unless ( defined $method );
	
	my ( $response ) = doPost( $client, [ { method => $method, params => $params } ] );
	die "$method: Empty response" if ( (scalar @$response) == 0 );

	my $wanted = findElem( $response, 'method', $method );
	die "$method failed to return expected method " . Dumper( $response ) if ( ! defined $wanted );

	die "$method returned error " . errorText( $wanted->{error} ) if ( exists $wanted->{error} );
	die "$method returned no result " . Dumper( $wanted ) unless ( exists $wanted->{result} );

	return $wanted->{result};
}


# getSessionID, return the sessionID from the admin.login 

sub getSessionID {
	my ( $client ) = @_;

	die "Client not defined" unless ( defined $client );

	my ( $response, $login ) = doPost( $client, [ ] ); # Gets bracketed by admin.login, logout - $response will be empty

	return $login->{result}->{sessionID};
}

# -- Admin functions (getVersion, login, logout) - login & logout added in doPost

# adminGetVersion - uses method admin.getVersion
# Reurns object eg { "version": "n", "subVersion"" "n" }

sub adminGetVersion {
	my ( $client ) = @_;

	my $method = 'admin.getVersion';

	die "$method: Client not defined" unless ( defined $client );

	my $params = {};
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# -- Customer  - customer.(create, delete, edit, get)

# Create customer with supplied name and return ID

sub createCustomer {
	my ( $client, $params ) = @_;

	my $method = 'customer.create';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Customer name not supplied" unless ( defined $params->{name} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub deleteCustomer {
	my ( $client, $params ) = @_;

	my $method = 'customer.delete';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Parameters ID or path not supplied" unless ( defined $params->{ID} ) || ( defined $params->{path} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub editCustomer {
	my ( $client, $params ) = @_;

	my $method = 'customer.edit';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Parameters ID or path not supplied" unless ( defined $params->{ID} ) || ( defined $params->{path} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

#
# getCustomer, lookup a customer by either ID or path
# returns hash (results) with keys defaultProjectTemplateID, description, name (as in customer name), ID, class (:customer), code, 
#    lastModificationUser, lastModificationDate, creationUser, creationDate,
#    projectTemplateList (an array of hash (projectTemplateID, projectTemplateName )

sub getCustomer {
	my ( $client, $params ) = @_;

	my $method = 'customer.get';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Neither customer ID nor path supplied" unless ( ( defined $params->{ID} ) || ( defined $params->{path} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# -- Directory (19 methods), split over objects (Customer, User, Group, Organization, roles, search, userProfile)

# -- user related directory.(createUser, deleteUser, editUser) and directory.list (used for users, groups or OU)

# Add user to Organizational unit

sub addCustomerToOU {
	my (  $client, $params ) = @_;

	my $method = 'directory.addCustomerToOU';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Missing ID or customerList" unless ( ( defined $params->{ID} ) && ( $params->{customerList} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# addUserToGroup

sub addUserToGroup {
	my (  $client, $params ) = @_;

	my $method = 'directory.addUserToGroup';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Missing ID or customerList" unless ( ( defined $params->{ID} ) && ( defined $params->{userList} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# createGroup 
 
sub createGroup {
	my ( $client, $params ) = @_;

	my $method = "directory.createGroup";

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Missing parameters" unless ( defined $params );
	die "$method: Missing orgUnit or name parameters" unless ( defined $params->{orgUnit} ) && ( defined $params->{name} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# createOrganisation

# Inputs( $client, $params ) 
# Mandatory params ( name, orgUnit )
# Returns Org ID in result { ID, class }
# Throws error if Org doesnt exist

sub createOrganization {
	my ( $client, $params ) = @_;

	my $method = 'directory.createOrganization';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Missing parameters" unless ( defined $params );
	die "$method: Missing name parameter" unless ( defined $params->{name} );
	die "$method: Missing orgUnit parameter" unless ( defined $params->{orgUnit} );

	# Do POST and throw an error unless we get the result object
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}


# createUser

# If successful creates an object with ID same as the supplied username
# Returns ... a string

sub createUser {
	my ( $client, $params ) = @_;

	my $method = 'directory.createUser';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Missing parameters" unless ( defined $params );
	die "$method: Missing name parameter" unless ( defined $params->{name} );
	die "$method: Missing orgUnit parameter" unless ( defined $params->{orgUnit} );

	$params->{defaultProfile} = DEFAULT_USER_PROFILE unless defined ( $params->{defaultProfile} );
	$params->{eMail} = "" unless ( defined $params->{eMail} );
	$params->{lang} = "en" unless ( defined $params->{lang} );
	$params->{userCanLog} = "true" unless ( defined $params->{userCanLog} );
	$params->{login} = $params->{name} unless ( defined $params->{login} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# deleteGroup 

sub deleteGroup {
	my ( $client, $params ) = @_;

	my $method = 'directory.deleteGroup';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Missing parameters" unless ( defined $params );
	die "$method: Missing ID parameter" unless ( defined $params->{ID} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# deleteOrganisation

# Inputs( $client, $params ) 
# Mandatory params ( ID )
# Returns result object eg { "status": "OrganizationUnit ORGA2 deleted" }
# Throws error if Org doesnt exist

sub deleteOrganization {
	my ( $client, $params ) = @_;

	my $method = 'directory.deleteOrganization';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Missing parameters" unless ( defined $params );
	die "$method: Missing ID parameter" unless ( defined $params->{ID} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# Delete user

# Returns result object eg { "status": "User CSC3 deleted" }

sub deleteUser {
	my ( $client, $params ) = @_;

	my $method = 'directory.deleteUser';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Missing parameters" unless ( defined $params );

	# Allow user to pass username as scalar or hash with key ID

	if ( ref( $params ) eq "HASH" ) {
		die "$method: Missing ID parameter" unless ( defined $params->{ID} );
	} else {
		my $username = $params;
		$params = { ID => $username };
	}

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# editOrganization

# Returns result object ...
# Throws error if Org doesnt exist

sub editOrganization {
	my ( $client, $params ) = @_;

	my $method = 'directory.editOrganization';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Missing parameters" unless ( defined $params );
	die "$method: Missing ID parameter" unless ( defined $params->{ID} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# Edit User

sub editUser {
	my ( $client, $params ) = @_;

	my $method = 'directory.editUser';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Missing parameters" unless ( defined $params );
	die "$method: Missing ID parameter" unless ( defined $params->{ID} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# Get Group

sub getGroup {

	my ( $client, $params ) = @_;

	my $method = "directory.getGroup";

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Missing parameters" unless ( defined $params );
	die "$method: Missing ID parameter" unless ( defined $params->{ID} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# getOrganization

# Returns result object ...
# Throws error if Org doesnt exist

sub getOrganization {
	my ( $client, $params ) = @_;

	my $method = 'directory.getOrganization';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Missing parameters" unless ( defined $params );
	die "$method: Missing ID parameter" unless ( defined $params->{ID} );

	# Do POST and throw an error unless we get the result object
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# getUser Get user details

# Returns hash of user details
# Throws exception if user not found

sub getUser {
	my ( $client, $params ) = @_;

	my $method = 'directory.getUser';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: ID not supplied" unless ( defined $params->{ID} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# listDirectory Returns the list of all children of a directory object (Organizational unit, User, Group)

sub listDirectory {
	my ( $client, $params ) = @_;

	my $method = 'directory.list';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: class not supplied" unless ( defined $params->{class} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# removeCustomerFromOU - Removes one or several customers from an organizational unit.

sub removeCustomerFromOU {
	my ( $client, $params ) = @_;

	my $method = 'directory.removeCustomerFromOU';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: ID not supplied" unless ( defined $params->{ID} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# removeUserFromGroup - Removes one or several users from an existing group.

sub removeUserFromGroup {
	my ( $client, $params ) = @_;

	my $method = 'directory.removeUserFromGroup';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: ID not supplied" unless ( defined $params->{ID} );
	die "$method: userList not supplied" unless ( defined $params->{userList} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# roles - directory.roles Returns the list of existing user roles.

sub roles {
	my ( $client ) = @_;

	my $method = 'directory.roles';

	die "$method: Client not defined" unless ( defined $client );

	my $result = doPostAndGetResult( $client, $method, {} );

	return $result;
}

# searchDirectory - directory.search retrieves directory items (users, customers, groups...) according to a Lucene query.

sub searchDirectory {
	my ( $client, $params ) = @_;

	my $method = 'directory.search';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: query not supplied" unless ( defined $params->{query} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# userProfiles - directory.userProfiles Returns the list of existing user profiles

sub userProfiles {
	my ( $client, $params ) = @_;

	my $method = 'directory.userProfiles';

	die "$method: Client not defined" unless ( defined $client );

	my $result = doPostAndGetResult( $client, $method, {} );

	return $result;
}

# -- Document, split into 7 GET requests and 15 POST requests (only POSTs here)

sub addNote {
	my ( $client, $params ) = @_;

	my $method = 'document.addNote'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Neither ID nor path supplied" unless ( ( defined $params->{ID} ) || ( defined $params->{path} ) );
	die "$method: note not supplied" unless ( defined $params->{note} ) ;

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub approvalStatus {
	my ( $client, $params ) = @_;

	my $method = 'document.approvalStatus'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Neither ID nor path supplied" unless ( ( defined $params->{ID} ) || ( defined $params->{path} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub approve {
	my ( $client, $params ) = @_;

	my $method = 'document.approve'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Neither ID nor path supplied" unless ( ( defined $params->{ID} ) || ( defined $params->{path} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# createDocument - document.createDocument
# Arguments
# Params - hash containing key/values, as per document.create in Dalim API
# Returns document ID created or undef

sub createDocument {
	my ( $client, $params ) = @_;

	my $method = 'document.create'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Neither jobID nor jobPath supplied" unless ( ( defined $params->{jobID} ) || ( defined $params->{jobPath} ) );
	die "$method: Document name not supplied" unless ( defined $params->{name} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# Delete document
# Args ( client, { ID or path }, )
# ID is document ID or path "/customer/job/document"
# Returns object eg { "status": "Document 1590 deleted" }
# or throws exception "Cannot find PageOrder" if document doesnt exist

sub deleteDocument {
	my ( $client, $params ) = @_;

	my $method = 'document.delete'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Missing ID or path" if ( ( !defined $params->{ID} ) && ( !defined $params->{path} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub deleteNote {
	my ( $client, $params ) = @_;

	my $method = 'document.deleteNote'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Missing ID or path" if ( ( !defined $params->{ID} ) && ( !defined $params->{path} ) );
	die "$method: Missing noteID" if ( !defined $params->{noteID} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub dialogueView {
	my ( $client, $params ) = @_;

	my $method = 'document.dialogueView'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Missing ID or paths" if ( ( !defined $params->{ID} ) && ( !defined $params->{paths} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub editDocument {
	my ( $client, $params ) = @_;

	my $method = 'document.edit'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Missing ID or path" if ( ( !defined $params->{ID} ) && ( !defined $params->{path} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub editDocumentNote {
	my ( $client, $params ) = @_;

	my $method = 'document.editNote'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Missing ID or path" if ( ( !defined $params->{ID} ) && ( !defined $params->{path} ) );
	die "$method: Missing noteID" if ( !defined $params->{noteID} );
	die "$method: Missing note" if ( !defined $params->{note} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# getDocument, lookup document by Document ID or path
# Throws error "Cannot find PageOrder" if document doesnt exist

sub getDocument {
	my ( $client, $params ) = @_;

	my $method = 'document.get'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Neither ID nor path supplied" unless ( ( defined $params->{ID} ) || ( defined $params->{path} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# getDocumentNotes - returns annotations added to Document

sub getDocumentNotes {
	my ( $client, $params ) = @_;

	my $method = 'document.getNotes'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Missing ID or path" if ( ( !defined $params->{ID} ) && ( !defined $params->{path} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub getDocumentWorkflows {
	my ( $client, $params ) = @_;

	my $method = 'document.getWFLs'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Missing ID or path" if ( ( !defined $params->{ID} ) && ( !defined $params->{path} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# getDocumentXMP - Extensible Metadata Platform (XMP) 
# see https://en.wikipedia.org/wiki/Extensible_Metadata_Platform

sub getDocumentXMPs {
	my ( $client, $params ) = @_;

	my $method = 'document.getXMP'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Missing ID or path" if ( ( !defined $params->{ID} ) && ( !defined $params->{path} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# registerDocument - Register a document revision
# Arguments
# - params - parameters as per Dalim ES API for document.register
# Returns
# undef - no value returned by ES API on success, just empty array

sub registerDocument {
	my ( $client, $params ) = @_;	# arguments hash, expect at least jobID/jobPath and URL

	my $method = 'document.register'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Missing job ID or path" if ( ( !defined $params->{jobID} ) && ( !defined $params->{jobPath} ) );
	die "$method: Missing URL or URLs" if ( ( !defined $params->{URL} ) && ( !defined $params->{URLs} ) );

	my $result = doPostAndGetResult( $client, $method, $params ); # Will be just { }

	return $result;
}

sub rejectDocument {
	my ( $client, $params ) = @_;	# arguments hash, expect at least jobID/jobPath and URL

	my $method = 'document.reject'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Missing job ID or path" if ( ( !defined $params->{jobID} ) && ( !defined $params->{jobPath} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# -- Folder ( addObjects, create, delete, edit, removeObjects )

# addFolderObjects, add objects (folder, job etc) to a folder

sub addFolderObjects {
	my ( $client, $params ) = @_;

	my $method = 'folder.addObjects';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );

	die "$method: Neither ID nor path supplied" unless ( ( defined $params->{ID} ) || ( $params->{path} ) );
	die "$method: objectList not supplied" unless ( defined $params->{objectList} );

	# TODO check objectList is an array an each element has members ID or path
	
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# createFolder, creates a subfolder or a collection folder if the class "Job" is not specified as argument.

sub createFolder {
	my ( $client, $params ) = @_;

	my $method = 'folder.create';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );

	die "$method: class must be supplied with ID" unless ( ( defined $params->{class} ) || ( ! defined $params->{ID} ) );
	die "$method: name not supplied" unless ( defined $params->{name} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# deleteFolder, deletes an existing collection folder

sub deleteFolder {
	my ( $client, $params ) = @_;

	my $method = 'folder.delete';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );

	die "$method: ID or path must be supplied" unless ( ( defined $params->{path} ) || ( defined $params->{ID} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# editFolder, edit an existing folder

sub editFolder {
	my ( $client, $params ) = @_;

	my $method = 'folder.edit';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );

	die "$method: ID or path must be supplied" unless ( ( defined $params->{path} ) || ( defined $params->{ID} ) );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# removeFolderObjects, remove objects (folder, job etc) from a folder

sub removeFolderObjects {
	my ( $client, $params ) = @_;

	my $method = 'folder.removeObjects';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );

	die "$method: Neither ID nor path supplied" unless ( ( defined $params->{ID} ) || ( $params->{path} ) );
	die "$method: objectList not supplied" unless ( defined $params->{objectList} );

	# TODO check objectList is an array an each element has members ID+class or path
	
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# -- Job ( createJob, deleteJob, edit, getJob, getWFLS, getXMP )
#        along with 4 GET methods (dvlProjectVersionView, dvlProjectView, jobPreview, jobThumbnail)

# CreateteJob - create a job or find existing job 

sub createJob {
	my ( $client, $params ) = @_;

	my $method = 'job.create';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );

	die "$method: JobName not supplied" unless ( defined $params->{jobName} );
	die "$method: Neither CustomerName or CustomerID supplied" unless ( ( defined $params->{customerName} ) || ( $params->{customerID} ) );

	# Dalim ES API doc says customerName or customerID, but get error "Missing parameter CustomerID or CustomerName"

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# deleteJob

#  nb job.delete only accepts path, not ID (as of Jan 2016), maybe doc error, so ID is jobID perhaps

sub deleteJob {
	my ( $client, $params ) = @_;

	my $method = 'job.delete';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: JobPath not supplied" unless ( ( defined $params->{path} ) || ( defined $params->{jobID} ) || ( defined $params->{ID} ) );
	
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub editJob {
	my ( $client, $params ) = @_;

	my $method = 'job.edit';

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Job ID or path not supplied" unless ( ( defined $params->{path} ) || ( defined $params->{ID} ) );
	
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}


# job.get - expects either ID or path
# Returns the results structure if found, $result->{ID} is the job ID
# Throws exception otherwise, including
# - "Cannot find Job at path: <job path>" if job not found

sub getJob {
	my ( $client, $params ) = @_;

	my $method = 'job.get'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Neither ID nor JobPath supplied" unless ( ( defined $params->{ID} ) || ( defined $params->{path} ) );
	
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub getJobWorkflows {
	my ( $client, $params ) = @_;

	my $method = 'job.getWFLs'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Neither ID nor JobPath supplied" unless ( ( defined $params->{ID} ) || ( defined $params->{path} ) );
	
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub getJobXMP {
	my ( $client, $params ) = @_;

	my $method = 'job.getXMP'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Neither ID nor JobPath supplied" unless ( ( defined $params->{ID} ) || ( defined $params->{path} ) );
	
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# -- Monitoring ( activity )

# activityMonitoring - monitor queue activity
# input queueID is optional

sub activityMonitoring {
	my ( $client, $params ) = @_;

	my $method = 'monitoring.activity'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}



# -- Production ( colorSpaces,executeSQL, list, projectTemplates, search, select, shareObject, smartView, toApprove, viewingConditions, workflows )

# listProduction, Arguments class and ID are optional

sub listProduction {
	my ( $client, $params ) = @_;

	my $method = 'production.list'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	die "$method: Param sql not defined" unless ( defined $params->{sql} );
	
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub executeSQL {
	my ( $client, $params ) = @_;

	my $method = 'production.executeSQL'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	
	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

# -- Workflow ( exportWFL, get, importWFL, reject, restart, start, startUserAction, stop, validate )

sub exportWFL {
	my ( $client, $params ) = @_;

	my $method = 'workflow.exportWFL'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	
	die "$method: Missing workflow name" if ( !defined $params->{name} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub getWorkflow {
	my ( $client, $params ) = @_;

	my $method = 'workflow.get'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	
	die "$method: Missing ID and class, or path" unless ( ( ( defined $params->{ID} ) && ( defined $params->{class} ) ) || ( defined $params->{path} ) );
	die "$method: Missing workflow name" if ( !defined $params->{name} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub importWFL {
	my ( $client, $params ) = @_;

	my $method = 'workflow.importWFL'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	
	die "$method: Missing param workflow" if ( !defined $params->{workflow} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub rejectWorkflow {
	my ( $client, $params ) = @_;

	my $method = 'workflow.reject'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	
	die "$method: Missing ID and class, or path" unless ( ( defined $params->{ID} && $params->{class} ) || ( $params->{path} ) );
	die "$method: Missing workflow name" if ( !defined $params->{name} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub restartWorkflow {
	my ( $client, $params ) = @_;

	my $method = 'workflow.restart'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	
	die "$method: Missing ID and class, or path" unless ( ( defined $params->{ID} && $params->{class} ) || ( $params->{path} ) );
	die "$method: Missing workflow name" if ( !defined $params->{name} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}


# Start workflow (on document or project)
# Args ( client, params { {ID + class} OR path }, workflow name )
# Returns result hash eg
# "workflowName": "WFL_DIALOG", "status": "WFL_DIALOG started on object 1532", "ID": "1532", "class": "PageOrder"

sub startWorkflow {
	my ( $client, $params ) = @_;

	my $method = 'workflow.start'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	
	die "$method: Missing ID and class, or path" unless ( ( defined $params->{ID} && $params->{class} ) || ( $params->{path} ) );
	die "$method: Missing workflow name" if ( !defined $params->{name} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub startUserAction {
	my ( $client, $params ) = @_;

	my $method = 'workflow.startUserAction'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	
	die "$method: Missing ID or IDs" unless ( ( defined $params->{ID} ) || ( $params->{IDS} ) );
	die "$method: Missing param name" if ( !defined $params->{name} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub stopWorkflow {
	my ( $client, $params ) = @_;

	my $method = 'workflow.stop'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	
	die "$method: Missing ID and class, or path" unless ( ( defined $params->{ID} && $params->{class} ) || ( $params->{path} ) );
	die "$method: Missing workflow name" if ( !defined $params->{name} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

sub validateWorkflow {
	my ( $client, $params ) = @_;

	my $method = 'workflow.validate'; 

	die "$method: Client not defined" unless ( defined $client );
	die "$method: Params not defined" unless ( defined $params );
	
	die "$method: Missing ID and class, or path" unless ( ( defined $params->{ID} && $params->{class} ) || ( $params->{path} ) );
	die "$method: Missing workflow name" if ( !defined $params->{name} );
	die "$method: Missing param stepID" if ( !defined $params->{stepID} );

	my $result = doPostAndGetResult( $client, $method, $params );

	return $result;
}

1;

