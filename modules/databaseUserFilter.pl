#!/usr/local/bin/perl
package ECMDBUserFilter;

use warnings;
use strict;

use Scalar::Util qw( looks_like_number );

use CGI::Carp qw/confess cluck croak/;

# Home grown
use lib 'modules';

use Validate;

require "config.pl";


##############################################################################
###      getUserFilterSettings 
###
################################################################################
sub getUserFilterSettings {
	my ($userid, $opcoid) = @_;

	my @filterlist;
	my ($pinningstatus, $pinningID, $ownertype);

	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );
		#@descriptions = $Config::dba->process_onerow_sql(
		#	"SELECT DISTINCT(filterdesc) FROM userfiltersettings WHERE userid=? AND opcoid=? AND status=0 ORDER BY id", [ $userid, $opcoid ]);
		my ( $rows ) = $Config::dba->hashed_process_sql(
"#Not shared filters created by self
	SELECT DISTINCT ufs.filterdesc AS filtername, u.username AS `owner`, ufs.status, ufs.userid, NULL as sharedfilterid, 'NO' AS shared, DATE_FORMAT(ufs.rec_timestamp, '\%d/\%m/\%Y') AS created
	FROM userfiltersettings ufs
	INNER JOIN users u ON (u.id=ufs.userid)
	WHERE NOT EXISTS (
 		SELECT sf.id FROM sharedfilters sf 
  		WHERE (sf.sharedfiltername = ufs.filterdesc) AND (sf.owneruserid=ufs.userid) AND (sf.opcoid=ufs.opcoid) AND (sf.status=1) AND (ufs.status=0)
	) 
	AND (ufs.opcoid=?) AND (ufs.userid=?) AND (ufs.status = 0)

#Shared filters created by self
	UNION
	SELECT DISTINCT ufs.filterdesc AS filtername, u.username AS `owner`, ufs.status, ufs.userid, sf.id as sharedfilterid, 'YES' AS shared, DATE_FORMAT(ufs.rec_timestamp, '\%d/\%m/\%Y') AS created
	FROM userfiltersettings ufs
	INNER JOIN users u ON u.id=ufs.userid
	INNER JOIN sharedfilters sf ON (sf.sharedfiltername = ufs.filterdesc) AND (sf.owneruserid=ufs.userid) AND (sf.opcoid=ufs.opcoid) AND (sf.status=1)
	WHERE EXISTS (
  		SELECT sf.id FROM sharedfilters sf 
  		WHERE sf.sharedfiltername = ufs.filterdesc AND sf.owneruserid=ufs.userid AND sf.opcoid=ufs.opcoid AND sf.status=1 
	) 
	AND (ufs.opcoid=?) AND (ufs.userid=?) AND (ufs.status = 0)
#shared filters shared with self
	UNION
	SELECT DISTINCT ufs.filterdesc AS filtername, u.username AS `owner`, ufs.status, ufs.userid, sf.id as sharedfilterid, 'YES' AS shared, DATE_FORMAT(sf.rec_timestamp, '\%d/\%m/\%Y') AS created
	FROM userfiltersettings ufs
	INNER JOIN users u ON (u.id=ufs.userid)
	INNER JOIN sharedfilters sf ON (sf.sharedfiltername=ufs.filterdesc) AND (sf.owneruserid=ufs.userid) AND (sf.opcoid=ufs.opcoid) AND (ufs.status=0) AND (sf.status=1)
	INNER JOIN sharedfilterusers sfu ON (sfu.sharedfilterid = sf.id) AND (sfu.shareduserid != sf.owneruserid) AND (sfu.status=1)
	WHERE (ufs.opcoid=?) AND (sfu.shareduserid=?)
	ORDER BY filtername",[ $opcoid,$userid,$opcoid,$userid,$opcoid,$userid ]);
		
		if ( defined $rows ) {
                	foreach my $r ( @$rows ) {

							($pinningstatus, $pinningID, $ownertype)=db_getFilterpinning($r->{filtername},$userid, $opcoid, $r->{userid});
                        	# logged_userid used in filter.js to differentiate the filter owner and current user	
							push ( @filterlist, { filtername => $r->{filtername}, owner => $r->{owner}, created => $r->{created}, shared => $r->{shared}, userid => $r->{userid}, sharedfilterid => $r->{sharedfilterid}, pinningstatus => $pinningstatus, pinningID=>$pinningID, ownertype=>$ownertype , logged_userid=> $userid } );
                	}
		}
	};
	if ( $@ ) {
		confess "getUserFilterSettings: userid: " . ( $userid || 0 ) . ", opcoid: " . ( $opcoid || 0 ) . ' ' . $@;
	}
	return @filterlist;
}

##############################################################################
####      getFilterData
####
##############################################################################
sub getFilterData {
	my ($userid, $opcoid, $filterdesc) = @_;

	my @values;
	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );
		@values = $Config::dba->process_onerow_sql(
			#"SELECT CONCAT(TRIM(filtertype), '~', TRIM(filterdata)) FROM userfiltersettings WHERE userid=? AND opcoid=? AND status=0 AND filterdesc=?",
			"SELECT DISTINCT (CONCAT(TRIM(ufs.filtertype), '~', TRIM(ufs.filterdata))) FROM userfiltersettings ufs WHERE ufs.userid=?  AND ufs.opcoid=? AND ufs.status=0 AND ufs.filterdesc= ?",
			[ $userid, $opcoid, $filterdesc ]);
	};
	if ( $@ ) {
		confess "getFilterData: userid: " . ( $userid || 0 ) . ", opcoid: " . ( $opcoid || 0 ) . ' ' . $@;
	}
	return @values;
}


##############################################################################
###      db_saveFilter
###
################################################################################
sub db_saveFilter{
	my ($userid, $params, $opcoid, $filterdesc, $sharedusers) = @_;

	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );
	
		foreach my $columnlist (split(/~~/, $params))
		{
			my @columns = split(/~/, $columnlist);
			
			$Config::dba->process_sql(
				"INSERT INTO userfiltersettings (userid, opcoid, filterdesc, filtertype, filterdata, rec_timestamp) " .
				" VALUES (?, ?, ?, ?, ?, now())",
				[ $userid, $opcoid, $filterdesc, $columns[0], $columns[1] ]);
		}
		if($sharedusers)
		{
			#here we have inserted sharefiltername
			$Config::dba->process_sql( "INSERT INTO sharedfilters ( SHAREDFILTERNAME, OWNERUSERID, OPCOID ) " . " VALUES (?, ?, ?)",
                                [ $filterdesc, $userid, $opcoid ]);
			#here we have get last insert id
			my ( $last_insert_id ) = $Config::dba->process_onerow_sql("SELECT LAST_INSERT_ID()");
            #here we have inserted sharedfilteruser list
			foreach my $shareduserlist (split(/,/,$sharedusers))
			{
				$Config::dba->process_sql("INSERT INTO sharedfilterusers (SHAREDFILTERID,SHAREDUSERID ) " ." VALUES (?, ?)",
                                [  $last_insert_id, $shareduserlist ]);
			}
			
		}
	};
	if ( $@ ) {
		my $error = $@;
		warn "db_saveFilter: $error\n";
		$userid = 'undef' unless defined $userid;
		$opcoid = 'undef' unless defined $opcoid;
		$params = 'undef' unless defined $params;
		$sharedusers = 'undef' unless defined $sharedusers;
		die "db_saveFilter: $error, userid $userid, opcoid $opcoid params $params sharedusers $sharedusers\n";
	}
}


##############################################################################
####      db_renameFilter
####
##############################################################################
sub db_renameFilter{
	my ($userid, $filtername, $opcoid, $new_filtername,$addusers,$deleteusers,$sharedusers_flag,$sharedfilterid) = @_;
	eval {
			Validate::userid( $userid );
			Validate::opcoid( $opcoid );
			#Here we have ckecked Sharedfilter 
			if($sharedusers_flag =~ /^YES$/i ){	
				#here we have update Filter Name
				if($filtername ne $new_filtername){	
					db_updateShareFilterStatus($new_filtername, $userid, $opcoid, $filtername);
					#here we have update shared filter table filter name
					$Config::dba->process_sql(
                        "UPDATE sharedfilters SET SHAREDFILTERNAME=? WHERE OWNERUSERID=? AND opcoid=? AND SHAREDFILTERNAME=? AND status = 1",
                        [ $new_filtername, $userid, $opcoid, $filtername ]);
				}
				#Here we have added shared user
				if($addusers){
					db_addUsers($addusers,$sharedfilterid);
				}

				#here we have updated shared users
				if ( $deleteusers ){
					# here we have inserted sharefiltername
					db_updateSharedUsersStatus( $sharedfilterid, $deleteusers,"update",0);
					my ($deleteallsharedusers) = $Config::dba->process_onerow_sql("SELECT COUNT(id) from sharedfilterusers WHERE SHAREDFILTERID=? AND STATUS=1",
                														             [ $sharedfilterid ]);
					#here we need to update filters table	
					if($deleteallsharedusers == 0){
						db_updateShareFilterNameStatus(0, $sharedfilterid,$opcoid );
					}
           		}
			}else{

				# here we have update Filter Name
                if($filtername ne $new_filtername){
					db_updateShareFilterStatus($new_filtername, $userid, $opcoid, $filtername);
                }
				if($addusers){

					 my  $last_insert_id 	= undef;
					#here we have check filter name present or not
					if ( $sharedfilterid ne "" && $sharedfilterid  !~ /null/i ) {
						db_updateShareFilterNameStatus(1, $sharedfilterid,$opcoid );
					} else {
	            		$Config::dba->process_sql( "INSERT INTO sharedfilters ( SHAREDFILTERNAME, OWNERUSERID, OPCOID ) " . " VALUES (?, ?, ?)",
                                [ $new_filtername, $userid, $opcoid ]);
        	    		( $last_insert_id )  = $Config::dba->process_onerow_sql("SELECT LAST_INSERT_ID()");
					}
					db_addUsers($addusers,$sharedfilterid,$last_insert_id);
				}
			}
			#here we have update pinned filter name
			if($sharedfilterid) {
        		$Config::dba->process_sql( "UPDATE filter_pinning  SET  FILTERNAME = ?  WHERE   OPCOID = ? AND FILTERNAME=? AND OWNERUSERID=? 
								AND USERID IN(SELECT SHAREDUSERID FROM sharedfilterusers WHERE SHAREDFILTERID=?)",
                                [ $new_filtername, $opcoid, $filtername, $userid,$sharedfilterid ]);

    		} else {
        		$Config::dba->process_sql( "UPDATE filter_pinning  SET  FILTERNAME = ?  WHERE   OPCOID = ? AND FILTERNAME=? AND OWNERUSERID=?",
                                [ $new_filtername, $opcoid, $filtername, $userid ]);
    		}
	};
	if ( $@ ) {
		warn "db_renameFilter: $@";
		my $error = $@;
		$error =~ s/ at .*//gs;
		die "db_renameFilter: $error";
	}
}


##############################################################################
#####      db_deleteFilter
#####
###############################################################################
sub db_deleteFilter {
	my ( $userid, $filtername, $opcoid, $sharedfilterid, $unshared_flag , $filteruserid) = @_;

	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );

		# warn "db_deleteFilter: userid $userid, filtername $filtername, opcoid $opcoid, sharedfilterid $sharedfilterid, unshared_flag $unshared_flag\n";
		# Delete from userfiltersettings table if the user is owner
		if( $userid ==	$filteruserid ) 
		{
		## Disable the original filter and any shares (if we own this filter)
			$Config::dba->process_sql("UPDATE userfiltersettings ufs
					LEFT OUTER JOIN sharedfilters sf ON sf.sharedfiltername=ufs.filterdesc AND sf.OWNERUSERID=ufs.userid
					LEFT OUTER JOIN sharedfilterusers sfu ON sfu.sharedfilterid=sf.id
					SET ufs.status=1, sf.status=0, sfu.status=0
					WHERE ufs.userid=? AND ufs.filterdesc=? AND ufs.opcoid=?;",
				[ $userid, $filtername, $opcoid ]);
		}		
		## Disable the relevant share (if we don't own this filter)
		$Config::dba->process_sql("UPDATE sharedfilters sf
					INNER JOIN sharedfilterusers sfu ON sfu.sharedfilterid=sf.id
					SET sfu.status=0
					WHERE sfu.shareduserid=? AND sharedfiltername=? AND sf.opcoid=? AND sf.OWNERUSERID=?; ",
				[ $userid, $filtername, $opcoid,$filteruserid ]);

		## Disable the pinning status
		$Config::dba->process_sql("UPDATE filter_pinning SET STATUS=0
					  where USERID=? AND FILTERNAME=? AND OPCOID=? AND OWNERUSERID=?;",
				[ $userid, $filtername, $opcoid, $filteruserid]);

		## Disable the pinning status for shared users if owner is deleting it
		$Config::dba->process_sql("UPDATE filter_pinning SET STATUS=0
					  where OWNERUSERID=? AND FILTERNAME=? AND OPCOID=?;",
				[ $filteruserid, $filtername, $opcoid, ]);
	};
	if ( $@ ) {
		my $error = $@;
		warn "db_deleteFilter: $error\n";
		$error =~ s/ at .*//gs;
		die "db_deleteFilter: $error";
	}
}


################################################################################
#### 	getSharedFilterData
####
################################################################################
sub getSharedFilterData{

	my ( $userid, $opcoid ) = @_;
	my @sharedfilter;
        eval {
                Validate::userid( $userid );
                Validate::opcoid( $opcoid );
                my ( $rows ) = $Config::dba->hashed_process_sql(
				
                        "SELECT sf.SHAREDFILTERNAME as filtername, u.username as createdby FROM sharedfilterusers sfu INNER JOIN sharedfilters sf ON 
			sf.ID=sfu.sharedfilterID INNER JOIN users u ON u.id=sf.OWNERUSERID and sf.OPCOID=? WHERE sfu.SHAREDUSERID = ?",
                        [  $opcoid, $userid ]);
		if ( defined $rows ) {
                foreach my $r ( @$rows ) {
                        push ( @sharedfilter, { filtername => $r->{filtername}, createdby => $r->{createdby}  } );
                }
        }

        };
        if ( $@ ) {
                confess "getsharedFilterData: userid: " . ( $userid || 0 ) . ", opcoid: " . ( $opcoid || 0 ) . ' ' . $@;
        }
        return @sharedfilter;

}

#####################################################################################
### getSharedFilterUser
###
####################################################################################
sub getSharedFilterUser{

	my ( $filtername, $opcoid , $userid) = @_;
        my @shareduser;
        eval {
                Validate::opcoid( $opcoid );
                my ( $rows ) = $Config::dba->hashed_process_sql(

                        "SELECT u.ID,(CONCAT(u.FNAME, ' ', u.LNAME, ', ', u.USERNAME)) USERNAME,u.USERNAME uname  FROM sharedfilterusers sfu INNER JOIN sharedfilters sf ON
                        sf.ID=sfu.SHAREDFILTERID AND sf.OWNERUSERID=$userid INNER JOIN users u ON u.id=sfu.SHAREDUSERID and sf.OPCOID=? WHERE sf.STATUS=1 AND sfu.STATUS=1 AND sf.SHAREDFILTERNAME = ?",
                        [  $opcoid, $filtername ]);
                if ( defined $rows ) {
                foreach my $r ( @$rows ) {
                        push ( @shareduser, { USERNAME => $r->{USERNAME}, ID => $r->{ID}, uname => $r->{uname}  } );
                }
        }

        };
        if ( $@ ) {
                confess "getsharedFilterUser: opcoid: " . ( $opcoid || 0 ) . ' ' . $@;
        }
        return @shareduser;
}


#####################################################################################
#### db_updateShareFilterStatus
####
#####################################################################################
sub db_updateShareFilterStatus{

	my($new_filtername, $userid, $opcoid, $filtername) = @_;	
	
	eval {
			Validate::opcoid( $opcoid );
			Validate::userid( $userid );
			$Config::dba->process_sql(
                	        "UPDATE userfiltersettings SET filterdesc=? WHERE userid=? AND opcoid=? AND filterdesc=? AND status = 0",
                    	    [ $new_filtername, $userid, $opcoid, $filtername ]);
	};
	if( $@ ) {
		confess "getsharedFilterData: userid: " . ( $userid || 0 ) . ", opcoid: " . ( $opcoid || 0 ) . ' ' . $@;
	}
	return 1;
}

#####################################################################################
#### db_updateSharedUsersStatus
####
#####################################################################################

sub db_updateSharedUsersStatus {

	my ( $sharedfilterid , $shareduserid, $type, $status ) = @_;
	eval {
			if($type eq "update")
			{
	 			$Config::dba->process_sql("UPDATE sharedfilterusers SET STATUS = ? WHERE SHAREDFILTERID = ? AND  SHAREDUSERID IN( '" . ($shareduserid =~ s/,/','/rg) . "' )",
                                  [$status, $sharedfilterid ]);
			} else {

				$Config::dba->process_sql("INSERT INTO sharedfilterusers (SHAREDFILTERID,SHAREDUSERID ) " ." VALUES (?, ?)",
                                  [ $sharedfilterid, $shareduserid ]);
			}
	};
	if( $@ ) {
        confess "db_updateSharedUsersStatus: shareduserid: " . ( $shareduserid || 0 ) . ", filterid: " . ( $sharedfilterid || 0 ) . ' ' . $@;
    }
    return 1;
}
#####################################################################################
#### db_updateShareFilterNameStatus
####
#####################################################################################
sub  db_updateShareFilterNameStatus{
	
	my ($status, $sharedfilterid, $opcoid) = @_; 
	eval {
            Validate::opcoid( $opcoid );
			$Config::dba->process_sql( "UPDATE sharedfilters  SET  STATUS = ?  WHERE  ID=? AND OPCOID = ?",
            		            [ $status, $sharedfilterid, $opcoid ]);
	};
    if( $@ ) {
        confess "db_updateShareFilterNameStatus: filterid: " . ( $sharedfilterid || 0 ) . ", opcoid: " . ( $opcoid || 0 ) . ' ' . $@;
    }
    return 1;
}

#####################################################################################
##### db_addUsers
#####
######################################################################################
sub db_addUsers{
	my($addusers,$sharedfilterid,$last_insert_id ) = @_;
	my	$rows=$Config::dba->hashed_process_sql("SELECT SHAREDFILTERID,SHAREDUSERID from sharedfilterusers WHERE SHAREDFILTERID=? ",
											 [ $sharedfilterid]);
	my %sharedusers;
	foreach my $row (@$rows){
		 $sharedusers{ $row->{SHAREDUSERID} } = $row->{SHAREDFILTERID};
	}
	foreach my $shareduserid (split(/,/,$addusers)){

		if ( $last_insert_id ) {
			#db_insertSharedFilterUsers( $last_insert_id, $shareduserid );
			db_updateSharedUsersStatus( $last_insert_id, $shareduserid,"insert");
		} else {

			if(defined $sharedusers{$shareduserid}){
				db_updateSharedUsersStatus( $sharedfilterid, $shareduserid,"update", 1);
			}else{

				#db_insertSharedFilterUsers( $sharedfilterid, $shareduserid );
				db_updateSharedUsersStatus( $sharedfilterid, $shareduserid,"insert");
			}
		}
   }
	return 1;
}


##############################################################################
# getSharedUserDetails: To get the FNAME, LNAME, EMAIL addres of user
##############################################################################
sub getSharedUserDetails {

    my ( $sharedUserIDs_ref )	= @_;

    my $rows	= "";

	my $inClause	= "u.id IN ('"         . join("','", split(/,/, $$sharedUserIDs_ref))            . "')";
    eval {
        $rows	= $Config::dba->hashed_process_sql("SELECT (CONCAT(u.FNAME, ' ', u.LNAME)) USERNAME, u.email
                									FROM users u  WHERE $inClause");
    };
    if ( $@ ) {
        cluck "getUser: userid " . ( $$sharedUserIDs_ref || 0 ) . ' ' . $@;
    }
	return $rows ? @$rows : ();
} #getSharedUserDetails
sub db_updateFilterpinning{
	my ( $filtername, $userid, $opcoid, $ownertype, $enable, $filteruserid ) = @_;	

	eval {
		    Validate::userid( $userid );
        	Validate::opcoid( $opcoid );	

			my ($exists) = $Config::dba->process_oneline_sql("SELECT 1 FROM filter_pinning WHERE USERID=? AND OPCOID=? AND FILTERNAME=? AND OWNERUSERID=?", [ $userid, $opcoid, $filtername, $filteruserid]);

			if ($exists)
			{
					$Config::dba->process_sql( "UPDATE filter_pinning  SET  STATUS = ?  WHERE  USERID=? AND OPCOID = ? AND FILTERNAME=? AND OWNERUSERID=?",
                                [ $enable, $userid, $opcoid, $filtername, $filteruserid ]);
			}
			else
			{
					$Config::dba->process_sql("INSERT INTO filter_pinning ( FILTERNAME,USERID,OPCOID,OWNERTYPE,OWNERUSERID,STATUS ) " ." VALUES (?, ?, ?, ?, ?, ?)",
                                  [ $filtername, $userid, $opcoid, $ownertype, $filteruserid, $enable ]);

			}
	};
	if ( $@ ) {
            confess "db_updateFilterpinning: ( $filtername, $userid, $opcoid, $ownertype, $enable ) errors " . $@;
    }
	return 1;
}
sub db_getFilterpinning{
	my ( $filtername, $userid, $opcoid,$filteruserid ) = @_;
	my ($status, $id, $ownertype);
	eval {
		    Validate::userid( $userid );
            Validate::opcoid( $opcoid );

            ($status,$id,$ownertype) = $Config::dba->process_oneline_sql("SELECT status, id, ownertype FROM filter_pinning WHERE USERID=? AND OPCOID=? AND FILTERNAME=? AND OWNERUSERID=?", [ $userid, $opcoid, $filtername, $filteruserid]);

	};
    if ( $@ ) {
            confess "db_getFilterpinning: ( $filtername, $userid, $opcoid ) errors " . $@;
    }

	return ($status, $id, $ownertype);
}

1;

