#!/usr/local/bin/perl
package ECMDBUserNotification;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use JSON;
use Carp qw/carp cluck/;
use Encode qw(decode encode);

use lib 'modules';

use CTSConstants;
use Validate;

require "config.pl";

sub Getusernotification {
	my $userid		= $_[0];
	# my $opcoid	= $_[1];
	my $event		= $_[2];

	return 0 if $userid && !looks_like_number($userid);
	#return 0 if $opcoid && !looks_like_number($opcoid);

	my ( $status, $msg, $email ) = $Config::dba->process_oneline_sql(
	#	"SELECT $event, MESSAGE, EMAIL FROM usernotification WHERE USERID=? AND OPCOID=?", [ $userid, $opcoid ]);
		"SELECT $event, MESSAGE, EMAIL FROM usernotification WHERE USERID=?", [ $userid ]);

	return $status, $msg, $email;
}

sub Getwholeusernotification {
	my $userid	= $_[0];
	#my $opcoid	= $_[1];
	my $mode	= $_[2];

	return 0 if $userid && !looks_like_number($userid);
	#return 0 if $opcoid && !looks_like_number($opcoid);

	# my ( $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $message, $email ) = $Config::dba->process_oneline_sql(
	#	"SELECT ASSIGNED, QCREQUIRE, QCAPPROVE, QCREJECT, REJECT, DISPATCH, MESSAGE, EMAIL FROM usernotification WHERE USERID=? AND OPCOID=?", 
	#		[ $userid, $opcoid ]);
	my ( $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched,$teams, $message, $email, $advisory ) = $Config::dba->process_oneline_sql(
		"SELECT ASSIGNED, QCREQUIRE, QCAPPROVE, QCREJECT, REJECT, DISPATCH,TEAMS, MESSAGE, EMAIL, ADVISORY FROM usernotification WHERE USERID=?", 
			[ $userid ] );

	return $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched,$teams, $message, $email, $advisory;
}

sub checkandupdatenotification {
	#my ( $userid, $opcoid, $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $message, $email ) = @_;
	my ( $userid, $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $teams, $message, $email, $advisory) = @_;
	my $id = "";

	return 0 if $userid && !looks_like_number($userid);
#	return 0 if $opcoid && !looks_like_number($opcoid);

	( $id )= $Config::dba->process_oneline_sql(
	#	"SELECT ID FROM usernotification WHERE USERID=? AND OPCOID=?", [ $userid, $opcoid ]);
		"SELECT ID FROM usernotification WHERE USERID=?", [ $userid ]);

	if ( $id ) {
		#updatenotification($userid, $opcoid, $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $message, $email, $id);
        updatenotification($userid, $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched,$teams, $message, $email, $id, $advisory);
	} else {
		#insertnotification($userid, $opcoid, $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $message, $email);
        insertnotification($userid, $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $teams, $message, $email, $advisory);
	}
}

sub insertnotification {

#	my ( $userid, $opcoid, $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $message, $email) = @_;
	my ( $userid, $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $teams, $message, $email, $advisory) = @_;

#	$Config::dba->process_sql(
#		"INSERT INTO usernotification (USERID, OPCOID, ASSIGNED, QCREQUIRE, QCAPPROVE, QCREJECT, REJECT, DISPATCH, MESSAGE, EMAIL) VALUES ( ?,?,?,?,?,?,?,?,?,?)", 
#				[ $userid, $opcoid,$assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $message, $email ]);

	$Config::dba->process_sql(
		"INSERT INTO usernotification (USERID, ASSIGNED, QCREQUIRE, QCAPPROVE, QCREJECT, REJECT, DISPATCH,TEAMS, MESSAGE, EMAIL, ADVISORY) VALUES ( ?,?,?,?,?,?,?,?,?,?,?)", 
				[ $userid, $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched,$teams, $message, $email, $advisory ]);

}

sub updatenotification {
	# my ( $userid, $opcoid, $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $message, $email, $id ) = @_;
	my ( $userid, $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $teams, $message, $email, $id, $advisory) = @_;

	$Config::dba->process_sql(
		"UPDATE usernotification SET ASSIGNED=?, QCREQUIRE=?, QCAPPROVE=?, QCREJECT=?, REJECT=?, DISPATCH=?, TEAMS=?, MESSAGE=?, EMAIL=?, ADVISORY=? where ID=?", 
			[ $assignjob, $qcrequired, $qcapproved, $qcreject, $jobreject, $dispatched, $teams, $message, $email, $advisory, $id ]);
}

1;

