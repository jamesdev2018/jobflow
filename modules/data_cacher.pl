#!/usr/loca/bin/perl
use strict;
use warnings;

package DataCache;

use File::Path qw(make_path remove_tree);
use Digest::MD5 qw(md5_hex);
use File::stat;
use Time::localtime;

use constant DEFAULT_MAX_CACHE_UNIT_SIZE => 1024 * 1024 * 2;	#Probably don't want bigger than 2MB files loaded into memory
#use constant $DEBUG_WARNS => 0;	# Print debug statements to apache error log so we can prove we're caching data

my $dir;
my $cache;
my $DEBUG_WARNS = 0;
my $MAX_CACHE_UNIT_SIZE = DEFAULT_MAX_CACHE_UNIT_SIZE;

## Make sure the cache directory exists and that expired caches are expunged
sub initialise_cache {
	my ($cache_dir) = @_;
	
	$DEBUG_WARNS = Config::getConfig('job_import_debug_warns');
	$MAX_CACHE_UNIT_SIZE = Config::getConfig('cache_max_individual_size');
	
	$dir = $cache_dir;
	warn "Initialising cache dir '$dir'" if $DEBUG_WARNS;
	return if !$dir;		## Initialise with an empty $cache_dir to disable caching
	
	$dir .= '/' if $dir !~ m/\/$/;
	make_path($dir);
	
	my $clear_all = 1;
	
	
	## Go through the files in the cache directory and clean up expired ones
	opendir(DH, $dir);
		my @files = readdir(DH);
	closedir(DH);
	
	foreach my $file (@files)
	{
		next if($file =~ /^\.+$/); # skip . and ..
		
		open my $handle, '<', "$dir/$file";
			chomp(my $expiration = <$handle>);
		close $handle;
		if((is_expired(stat("$dir/$file")->mtime, $expiration) && $expiration) || $clear_all) {
			unlink "$dir/$file";
		}
	}
	
	## Go through the local memory cache and clean that up
	warn "Clearing cache (" . join(', ', keys %$cache) . ")" if $DEBUG_WARNS;
	foreach my $key (keys %$cache) {
		if(is_expired($cache->{$key}->{timestamp}, $cache->{$key}->{expiration_limit}) || $clear_all) {
			delete $cache->{$key}
		}
	}
}



## Get data for this api call from local memory, storage or the URI call (in this order)
sub get_data_from_api {
	my ($api_uri, $expiration_limit) = @_;
	my $hash = md5_hex($api_uri);
	warn "Getting data for '$api_uri'" if $DEBUG_WARNS;
	
	if($dir && $expiration_limit) {
		## Check if we have it in local memory
		if($cache->{$hash}) {{
			if(my $etime = is_expired($cache->{$hash}->{timestamp}, $expiration_limit)) {
				warn "Cache data exists, but expired $etime seconds ago" if $DEBUG_WARNS;
				last;
			}
			
			warn "Read from local memory" if $DEBUG_WARNS;
			return $cache->{$hash}->{data};
		}}
	
	
		## Check if we have it on disk
		if(-e "$dir/$hash") {{
			if(-s "$dir/$hash" >= $MAX_CACHE_UNIT_SIZE) { warn "Saved data exists, but is too big to load" if $DEBUG_WARNS; last; }
			if((my $etime = is_expired(stat("$dir/$hash")->mtime, $expiration_limit))) { warn "Saved data exists, but expired $etime seconds ago." if $DEBUG_WARNS; last; }
			
			warn "Read from disk" if $DEBUG_WARNS;
			open(my $handle, '<', "$dir/$hash") or die "Can't open '$dir/$hash' ($!)";
				my $data = '';
				<$handle>;	## Burn the first line, which is just the expiration time (for cleanup purposes)
				while (my $row = <$handle>) { $data .= $row; }
			close $handle;
			
			## Add it to local memory so we don't always have to go to disk
			$cache->{$hash} = {	timestamp => get_timestamp(),
								expiration_limit => $expiration_limit,
								data => $data },
		
			return $data;
		}}
	} else {
		warn "Caching has been disabled" if $DEBUG_WARNS;
	}
	
	
	## Otherwise get actual data
	my $command = "curl -s";
	$command .= " -k" if Config::getConfig('jf_ssl_hack');		## Ignore security certificate if we're in "ignore self-signed [read: hacky] mode"
	$command .= " '$api_uri'";
	my $data = `$command`;
	warn "Getting actual data (" . length($data) . ") from '" . $command . "'" if $DEBUG_WARNS;
	
	## Save the data if we're cache-enabled and it's not too much data
	if($dir && $expiration_limit && (length($data) < $MAX_CACHE_UNIT_SIZE)) {
		## Update the hashlist
		$cache->{$hash} = {	timestamp => get_timestamp(),
							expiration_limit => $expiration_limit,
							data => $data,
						};
		
		## Update the file
		open my $handle, '>', "$dir/$hash" or die "Couldn't open file '$dir/$hash' ($!)";
			print $handle $expiration_limit . "\n";
			print $handle $data;
		close $handle;
	}
	
	return $data;
}


## Return 0 if time hasn't expired, or the number of seconds since expiration
sub is_expired {
	my ($time, $expiration_limit) = @_;
	
	my $difference = get_timestamp() - ($time + $expiration_limit);
	return $difference > 0 ? $difference : 0;
}

## Return the current time as a numerical string
sub get_timestamp {
	return "" . time;
}

1;
