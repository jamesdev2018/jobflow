#!/usr/local/bin/perl
package JobsWS;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use Data::Dumper;

# Home grown

use lib 'modules';

use Validate;

require "databaseJob.pl";
require "databaseOpcos.pl";

# Contains
# GetJobPathWS
# GetJobDetailsWS
# GetJobShortPathWS
# GetJobLocationWS
# SetCheckoutStatusWS
# GetUploadInputFolderWS
# autoRepoSendJobs -- commented out
# autoRepoIsFinished -- missing
# updateRepoJobWS
# updateJobStatusWS
# RecordLibrarySyncActionWS
# getCustomerCampaignsWS
# getCustomerClientsWS
# getCustomerClientProjectsWS
# getJobsInCampaignWS
#
#############################################################################

sub GetJobPathWS {
	my $query 		= $_[0];
	my $jobid		= $query->param( "jobid" );
	my $jobPath		= ECMDBJob::db_jobPath( $jobid ) ||'';
	my $fromopcoid		= ECMDBJob::db_getJobRemoteOpcoid( $jobid ); 	# this is the remote
	my $toopcoid		= ECMDBJob::db_getJobOpcoid( $jobid );			# this is the local
	my $ttpfolder		= ECMDBOpcos::db_getTTPFolder( $fromopcoid, $toopcoid ) ||'';
	
	print $query->header();
	print qq(<job><path>$jobPath</path><ttpfolder>$ttpfolder</ttpfolder></job>);
	
}

sub GetJobDetailsWS {
	my $query		= $_[0];

	my $error;
	my $jobid = $query->param("jobid");

	if ( ! defined $jobid ) {
		$error = "No job number (undef)";
		$jobid = "undefined";
		} elsif ( ref ( $jobid ) ne "" ) {
		$error = "No job number (ref type " . ref( $jobid ) . ")";;
	} else {	
		my $values = ECMDBJob::db_getJobPathInfo($jobid);
		if ( ! defined $values ) {
			$error = "getJobPathInfo returned undef";
	
		} elsif ( ref( $values ) eq "HASH" ) {
			print $query->header();
			print "<job>\n";
			print "   <jobid>"		. $values->{jobnumber}	. "</jobid>\n";
			print "   <opcoid>"		. $values->{opcoid}		. "</opcoid>\n";
			print "   <opco>"		. $values->{opco}		. "</opco>\n";
			print "   <rootpath>"	. $values->{rootpath}	. "</rootpath>\n";
			print "   <letter>"		. $values->{letters}||''	. "</letter>\n";
			print "   <agency>"		. $values->{agency}		. "</agency>\n";
			print "   <client>"		. $values->{client}		. "</client>\n";
			print "   <project>"		. $values->{project}		. "</project>\n";
			print "   <proofing>"	. $values->{proofing}	. "</proofing>\n";
			print "   <ucr>"			. $values->{ucr}			. "</ucr>\n";
			print "   <workflow>"	. $values->{workflow}	. "</workflow>\n";
			print "</job>\n";
		}  else {
			$error = "getJobPathInfo returned $values";
		}
	}
	if ( defined $error ) {
		print $query->header();
		print "<job>\n";
		print "   <jobid>$jobid</jobid>\n";
		print "   <error>$error</error>\n";
		print "</job>\n";
	}
}

##############################################################################
# get short path (For Repository) - it doesn't need the root
##############################################################################
sub GetJobShortPathWS {
	my $query 		= $_[0];
	my $jobid		= $query->param( "jobid" );
	my $jobPath		= ECMDBJob::db_jobShortPath( $jobid );
	
	print $query->header();
	print qq(<job><path>$jobPath</path></job>);
	
	
}


sub GetJobLocationWS {
	my $query = $_[0];
	my $jobid = $query->param("jobid");
	my $shard = $query->param("shard") || '';
	my $values = ECMDBJob::db_getJobLocation($jobid, $shard);
	my $info = ECMDBJob::db_getJobInfo($jobid);
	my $jobpath = ECMDBJob::db_jobShortPath($jobid);				# Don't worry about the shardname here, it is constant for the whole job
	
	print $query->header();
	print "<job>\n";
	print "   <jobid>"     . $values->{jobid}          . "</jobid>\n";
	print "   <shard>"     . $values->{shard_name}||'' . "</shard>\n";
	print "   <jobpath>"   . $jobpath                  . "</jobpath>\n";
	print "   <inrepo>"    . $values->{isinrepo}       . "</inrepo>\n";
	print "   <mediatype>" . $info->{workflow}         . "</mediatype>\n";
	
	print "   <current>\n";
	print "      <opcoid>" . $values->{current_opcoid} . "</opcoid>\n";
	print "      <opco>"   . $values->{current_opco}   . "</opco>\n";
	print "      <opcoip>" . $values->{current_opcoip} . "</opcoip>\n";
	print "      <opcowip>". $values->{current_opcowip}. "</opcowip>\n";
	print "   </current>\n";
	
	print "   <origin>\n";
	print "      <opcoid>" . $values->{origin_opcoid}  . "</opcoid>\n";
	print "      <opco>"   . $values->{origin_opco}    . "</opco>\n";
	print "      <opcoip>" . $values->{origin_opcoip}  . "</opcoip>\n";
	print "      <opcowip>". $values->{origin_opcowip} . "</opcowip>\n";
	print "   </origin>\n";
	
	print "</job>\n";
}

sub SetCheckoutStatusWS {
	my $query 		= $_[0];
	my $jobid		= $query->param( "jobid" );
	my $shard		= $query->param('shard');
	my $statusid		= $query->param( "statusid" );
	
	#change checkout status of each job to 'Sending' (from '')
	ECMDBJob::setCheckoutStatus ( $jobid, $shard, $statusid ); # 1 is 'Checking Out'
	
	print $query->header();
	print qq(<job><result>ok</result></job>);
}

##############################################################################
# GetUploadInputFolderWS
##############################################################################
sub GetUploadInputFolderWS {
	my $query 		= $_[0];
	my $jobid		= $query->param( "jobid" );

	my $tempfolder		= ECMDBOpcos::getUploadTempFolder( $jobid );
	my $dropfolder		= ECMDBOpcos::getUploadDropFolder( $jobid );
	
	print $query->header();
	print qq(<job><tempfolder>$tempfolder</tempfolder><dropfolder>$dropfolder</dropfolder></job>);

}


sub setJobShards
{
	my $query = $_[0];
	my $jobid = $query->param("jobid");
	my $shards = $query->param("shard");
	my $studioid = $query->param("studioid");
	
	ECMDBJob::db_setJobShards($jobid, $shards, $studioid);
	
	print $query->header();
	print qq(<job><result>ok</result></job>);
}

##############################################################################
#   autoRepoSendJobs()
#	Add the jobs to the database to be processed for CHECKIN followed 
#	by CHECKOUT by batch.
#	Essentially we make the Checkin and then Checkout as an atomic operation 
#	as far as the user is concerned.
#
##############################################################################
#sub autoRepoSendJobs {
#	my ($query, $session) 	= @_;
#
#	my $destinationOpcoid = 0;
#
#	my $userid 		= $session->param( 'userid' );
#	my @jobs		= split( ',', $query->param( "jobs" ) );
#	my $destinationOpcoid	= $query->param( "opcoid" );
#
#	print "<queuedjobs>\n";
#	foreach my $job ( @jobs ) {
#		ECMDBJob::enQueueRepoSendJob( $job, $destinationOpcoid, $userid );
#
#		#audit this		
#		Audit::auditLog( $session->param( 'userid' ), "TRACKER3PLUS", $job, "Job: $job has been queued for Sending to $destinationOpcoid by $userid" );
#		
#		print "   <job>\n";
#		print "      <id>$job</id>\n";
#		print "      <destinationopco>$destinationOpcoid</destinationopco>\n";
#		print "   </job>\n";
#	}
#	print "</queuedjobs>\n";
#}


sub splitRepoJobWS {
	my ($query) = @_;
	
	my $jobid = $query->param("jobid");
	my $shards = $query->param("shards");
	
	ECMDBJob::db_splitRepoJobQueues($jobid, $shards);
	
	print $query->header();
	print qq(<job><result>ok</result></job>);
}

##############################################################################
# updateRepoJobWS
# 	mark this Job as checked in to the Repo
#
##############################################################################
sub updateRepoJobWS {

	# checkin / checkout / sync

	my $query 		= $_[0];
	my $subaction		= $query->param( "subaction" );
	my $jobid		= $query->param( "jobid" );
	my $shard_name	= $query->param("shard") || '';
	my $opcoid		= $query->param( "studioid" );
	my $jobsize		= $query->param( "jobsize" );
	my $success		= ($query->param('failure') || "") eq "";
	
	if(!$success)
	{
		ECMDBJob::db_disableInCheckInQueue($jobid) if $subaction eq 'checkin';
		ECMDBJob::db_disableInCheckOutQueue($jobid) if $subaction eq 'checkout';
		ECMDBJob::db_disableInSyncQueue($jobid) if $subaction eq 'sync';
	}
	else
	{
		if ( $subaction eq "checkin" ) {
			ECMDBJob::db_removeFromCheckInQueue( $jobid, $shard_name );
			ECMDBJob::setLocation( $jobid, $shard_name, 16 );	# 16 is 'Repository'
			ECMDBJob::setCheckoutStatus( $jobid, $shard_name, 4 ); # checkedIn
			ECMDBJob::db_setRepoCheckedIn( $jobid );
		} elsif ( $subaction eq "checkout" ) {
			   ECMDBJob::db_removeFromCheckOutQueue( $jobid, $shard_name ); # make sure to get destination FIRST!!
			   ECMDBJob::setLocation( $jobid, $shard_name, $opcoid );
			   ECMDBJob::setCheckoutStatus( $jobid, $shard_name, 2 ); # checkedOut
			   ECMDBJob::db_setRepoCheckedOut($jobid);
		   	   ECMDBJob::db_setRepoCheckedIn($jobid);
		} elsif ( $subaction eq "sync" ) {
		   		ECMDBJob::db_removeFromSyncQueue( $jobid );
		}

		if (looks_like_number( $jobsize) ) {
			ECMDBJob::db_setJobSize( $jobid, $jobsize );
		}
	}

	print $query->header();
	print qq(<job><result>ok</result></job>);

}

##############################################################################
# updateJobStatusWS
#       mark this Job as checked in to the Repo
#
##############################################################################
sub updateJobStatusWS {

	my $query               = $_[0];
	my $jobid               = $query->param( "jobid" );
	my $preflightstatus     = $query->param( "preflightstatus" );
	my $qcstatus            = $query->param( "qcstatus" );
	my $assetscreated       = $query->param( "assetscreated" );
	my $hires               = $query->param( "hires" );

	print $query->header();
	ECMDBJob::db_updateJobStatus( $jobid, $preflightstatus, $qcstatus, $assetscreated, $hires );

	print qq(<job><result>All done for $jobid</result></job>);
}


sub RecordLibrarySyncActionWS
{
	my ($query) = @_;
	
	my $nodeid		= $query->param("nodeid");
	my $opconame		= $query->param("opconame");
	my $customername	= $query->param("customername");
	my $username		= $query->param("username");
	my $size		= $query->param("size");
	
	
	my $retval = "";
	eval { $retval = ECMDBJob::db_recordLibrarySyncAction($nodeid, $opconame, $customername, $username, $size); };
	
	print $query->header();
	if($@)
	{
		print "<status>Error</status>\n";
		
		print "<error>$@</error>\n";
	}
	else
	{
		print "<status>Success</status>\n";
	}
	
	print "<values>\n";
	print "   <nodeid>"			. ($nodeid || "[missing]")		. "</nodeid>\n";
	print "   <opconame>"		. ($opconame || "[missing]")		. "</opconame>\n";
	print "   <customername>"	. ($customername || "[missing]")	. "</customername>\n";
	print "   <username>"		. ($username || "[missing]")		. "</username>\n";
	print "   <size>"			. ($size || "0") . "mb</size>\n";
	print "</values>\n";
}

# Job from One2Edit
# Args: 
# document.id, One2Edit Version document id
# document.name, Document name, typically <job number>.indd
# 

sub jobfrmOne2edit {
	my ( $query ) = @_;

	eval { 
		my $doc_id   = $query->param("document.id");
		die "Invalid document id" unless defined $doc_id && looks_like_number( $doc_id );

		my $doc_name = $query->param("document.name");
		die "Invalid document name" unless defined $doc_name;

		my ($jobnumber) = $doc_name =~  /(\d*)\.indd/i;
		die "Could not get job number from filename $doc_name" unless defined $jobnumber;
		Validate::job_number( $jobnumber );

		# Handle potential proxy between Staging and Dev (after the initial validation)

		my $o2e_proxy = $query->param("o2e_proxy");
		if ( defined $o2e_proxy ) {
			# form curl POST request to o2e_proxy (either jobflow.dev or a vhost)
			my %post_args = $query->Vars;
			my @list;
			foreach my $k ( keys %post_args ) {
				next if ( $k eq 'o2e_proxy' ); # Dont forward the proxy itself
				push( @list, $k . '=' . $post_args{ $k } );
			}
			my $cmd = 'curl -s -d ' . join( ' -d ', @list ) . ' "' . $o2e_proxy . '"';
			# warn 'curl request ' . $cmd;
			my $result = `$cmd`;
			print $query->header();
			if ( $result =~ /\<status\>Success/ ) {
				print $result . "\n";
			} else {
				print "<status>Forwarded to $o2e_proxy, result '$result'</status>\n";
			}
			# audit will be done on proxy target server
		} else {
			ECMDBJob::db_insertOne2edit($jobnumber, $doc_id, $doc_name); 
			ECMDBAudit::db_auditLog( 2, "one2edit", $jobnumber, "Workflow complete, document id $doc_id, name $doc_name" );
			print $query->header();
			print "<status>Success</status>\n";
		}
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at module.*//gs; # remove the traceback
		warn $error; # warn writes the error and traceback to the apache error log

		my %post_args = $query->Vars;
		my @list;
		foreach my $k ( keys %post_args ) {
			push( @list, $k . '=' . $post_args{ $k } );
		}
		ECMDBAudit::db_auditLog( 2, "one2edit", 0, "Workflow error, $error, " . join(', ', @list ) );

		print $query->header();
		print "<status>Error</status>\n";
		print "<error>$error</error>\n";
	}
}

# Web service to enumerate the campaigns for a customer
# As this may be called by Masters, have to accept either customerid or (studio name, customer name)
 
sub getCustomerCampaignsWS {
	my ( $query ) = @_;

	my $customer_id = $query->param("customerid");

	eval {
	
		if ( ! defined $customer_id ) {
			# See if customer name, studio name supplied
			my $customer_name = $query->param("customername");
			my $studio_name = $query->param("studioname");

			# lookup studio and customer name to get customer id
			if ( ( $studio_name ne "" ) && ( $customer_name ne "" ) ) {
				my $opcoid = ECMDBOpcos::findOperatingCompany( $studio_name );
				$customer_id = ECMDBOpcos::findCustomer( $opcoid, $customer_name ) if ( defined $opcoid );
				die "Could not resolve studio '$studio_name', customer '$customer_name'" if ( ! defined $customer_id );
			}
		}

		die "Customer id not supplied" if ( $customer_id eq "" );

		my @values = ECMDBJob::db_getCustomerCampaigns( $customer_id );
		my $all_text = "";  # Accumulated XML
		
		foreach my $h ( @values ) {
			my $text = "";
			foreach my $k ( 'jobnumber', 'agency', 'client', 'project' ) { # agency usually same as customer name
				$text .= "$k='" . $h->{$k} . "' ";
			}
			$all_text .= "<campaign $text/>";
		}
		print $query->header();
		print "<campaigns customerid='$customer_id'>\n<status>Success</status>\n$all_text\n</campaigns>\n";
		
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/at modules.*//g;
		print $query->header();
		$customer_id = 0 unless defined $customer_id;
		print "<campaigns customerid='$customer_id'>\n<status>Error</status><error>$error</error></campaigns>\n";
	}

}

# Web service to enumerate the clients for a customer
# As this may be called by Masters, have to accept either customerid or (studio name, customer name)
 
sub getCustomerClientsWS {
	my ( $query ) = @_;

	my $customer_id = $query->param("customerid");
	my $customer_name = $query->param("customername") || '';
	my $studio_name = $query->param("studioname") || '';
	my $t0 = time();

	eval {
	
		if ( defined $customer_id ) {
			die "Invalid customer id '$customer_id'" if  !looks_like_number( $customer_id );

		} elsif ( ( $studio_name eq '' ) || ( $customer_name eq '' ) ) {
			die "Please specify either customerid or both studioname and customername";

		} else { # lookup studio and customer name to get customer id
			my $opcoid = ECMDBOpcos::findOperatingCompany( $studio_name );
			die "Invalid studio name '$studio_name'" unless defined $opcoid;

			$customer_id = ECMDBOpcos::findCustomer( $opcoid, $customer_name ) if ( defined $opcoid );
			die "Could not find customer '$customer_name' at operating company '$studio_name'" if ( ! defined $customer_id );
		}

		my $clients = ECMDBJob::db_getCustomerClients( $customer_id );

		my $all_text = '<client>' . join( '</client><client>', @$clients ) . '</client>';
		my $elapsed = time() - $t0;

		print $query->header();
		print "<clients customerid='$customer_id' elapsed='$elapsed'>\n<status>Success</status>\n$all_text\n</clients>\n";
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/at modules.*//g;
		chomp $error;
		print $query->header();
		$customer_id = 0 unless defined $customer_id;
		print "<clients customerid='$customer_id'>\n<status>Error</status><error>$error</error></clients>\n";
	}
}

# Web service to enumerate the projects for a customer+client
# As this may be called by Masters, have to accept either customerid or (studio name, customer name)
 
sub getCustomerClientProjectsWS {
	my ( $query ) = @_;

	my $customer_id = $query->param("customerid");
	my $customer_name = $query->param("customername") || '';
	my $studio_name = $query->param("studioname") || '';
	my $client = $query->param("client") || '';
	my $t0 = time();

	eval {
	
		if ( defined $customer_id ) {
			die "Invalid customer id '$customer_id'" if  !looks_like_number( $customer_id );

		} elsif ( ( $studio_name eq '' ) || ( $customer_name eq '' ) ) {
			die "Please specify either customerid or both studioname and customername";

		} else { # lookup studio and customer name to get customer id
			my $opcoid = ECMDBOpcos::findOperatingCompany( $studio_name );
			die "Invalid studio name '$studio_name'" unless defined $opcoid;

			$customer_id = ECMDBOpcos::findCustomer( $opcoid, $customer_name ) if ( defined $opcoid );
			die "Could not find customer '$customer_name' at operating company '$studio_name'" if ( ! defined $customer_id );
		}
		die "Invalid client" unless $client ne '';

		my $projects = ECMDBJob::db_getCustomerClientProjects( $customer_id, $client );

		my $all_text = '<project>' . join( '</project><project>', @$projects ) . '</project>';
		my $elapsed = time() - $t0;

		print $query->header();
		print "<projects customerid='$customer_id' elapsed='$elapsed'>\n<status>Success</status>\n$all_text\n</projects>\n";
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/at modules.*//g;
		chomp $error;
		print $query->header();
		$customer_id = 0 unless defined $customer_id;
		print "<projects customerid='$customer_id' client='$client'>\n<status>Error</status><error>$error</error></projects>\n";
	}
}

# Web service to find the jobs in a campaign (customer+client)
# As this may be called by Masters, have to accept either customerid or (studio name, customer name)
 
sub getJobsInCampaignWS {
	my ( $query ) = @_;

	my $customer_id = $query->param("customerid");
	my $customer_name = $query->param("customername") || '';
	my $studio_name = $query->param("studioname") || '';
	my $client = $query->param("client") || '';
	my $project = $query->param("project") || '';

	my $t0 = time();

	eval {
	
		if ( defined $customer_id ) {
			die "Invalid customer id '$customer_id'" if  !looks_like_number( $customer_id );

		} elsif ( ( $studio_name eq '' ) || ( $customer_name eq '' ) ) {
			die "Please specify either customerid or both studioname and customername";

		} else { # lookup studio and customer name to get customer id
			my $opcoid = ECMDBOpcos::findOperatingCompany( $studio_name );
			die "Invalid studio name '$studio_name'" unless defined $opcoid;

			$customer_id = ECMDBOpcos::findCustomer( $opcoid, $customer_name ) if ( defined $opcoid );
			die "Could not find customer '$customer_name' at operating company '$studio_name'" if ( ! defined $customer_id );
		}
		die "Invalid client" unless $client ne '';
		die "Invalid project" unless $project ne '';

		my $jobs = ECMDBJob::db_getJobsInCampaign( $customer_id, $client, $project );
		# warn "getJobsInCampaignWS: " . Dumper( $jobs );

		my $all_text = '<job>' . join( '</job><job>', @$jobs ) . '</job>';
		my $elapsed = time() - $t0;

		print $query->header();
		print "<jobs customerid='$customer_id' client='$client' project='$project' elapsed='$elapsed'>\n<status>Success</status>\n$all_text\n</jobs>\n";
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/at modules.*//g;
		chomp $error;
		print $query->header();
		$customer_id = 0 unless defined $customer_id;
		$client = '' unless defined $client;
		$project = '' unless defined $project;
		print "<jobs customerid='$customer_id' client='$client' project='$project' >\n<status>Error</status><error>$error</error></jobs>\n";
	}
}

##############################################################################
1;
