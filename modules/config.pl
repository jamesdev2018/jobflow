#!/usr/local/bin/perl
package Config;

use warnings;
use strict;

use lib 'Utils';
use lib '../modules/Utils';
use lib 'modules/Utils';

use DBAgent;

my $environment = $ENV{"TAG_ENV"}	|| "DEV";
my %configitems;

## !! !!
## !! SETTINGS HAVE MOVED TO THE jobflow_settings TABLE IN THE JOBFLOW DATABASE !!
## !! !!
## !! DO NOT ADD CONFIGURATION ITEMS HERE !!
## !! !!

## Note that setting values can now refer to each other in the database
## e.g.
##  setting: 'web_protocol'			value: 'https://'
##	setting: 'server_address'		value: 'jobflow.dev.tagworldwide.com'
##	setting: 'web_address'			value: '${web_protocol}${server_address}/jobflow.pl'	-> resolves to	'https://jobflow.dev.tagworldwide.com/jobflow.pl'


## Load the basic database settings
	if($environment eq "DEV")
	{
		$configitems{ 'databaseserver' }		= "172.27.93.115";
		$configitems{ 'databaseusername' }		= "production";
		$configitems{ 'databasepassword' }		= 'production';
		$configitems{ 'databaseschema' }		= "ecm";
	}
	elsif($environment eq "STAGING")
	{
		$configitems{ 'databaseserver' }		= "172.27.100.27"; # Percona was on 172.27.100.36
		$configitems{ 'databaseusername' }		= "production";
		$configitems{ 'databasepassword' }		= 'pr0duction$52';
		$configitems{ 'databaseschema' }		= "prod_staging";
	}
	elsif ($environment eq "LIVE")
	{
		$configitems{ 'databaseserver' }		= "10.2.4.108";
		$configitems{ 'databaseusername' }		= "production";
		$configitems{ 'databasepassword' }		= '4TI993$85Z2x20X';
		$configitems{ 'databaseschema' }		= "prod";
	}
	else 
	{
		die "Invalid value of TAG_ENV $environment";
	}


## Connect to the database
	our $dba;
	
sub initialise_config
{
	$dba = DBAgent->new('jobflow_web', getConfig("databaseschema"), getConfig("databaseserver"), getConfig("databaseusername"), getConfig("databasepassword"));
	$dba->new_connection();

	## Load the jobflow settings from the database
		my $settings = $dba->hashed_process_sql("SELECT setting, value FROM jobflow_settings");
	
		foreach my $setting (@$settings)
		{
			$configitems{$setting->{setting}} = $setting->{value};
		}



	## Loop through and substitute self-referring values
		foreach my $key (keys %configitems)
		{
			my $value = $configitems{$key};
			while( $value =~ m/^(.*)\$\{(.+)\}(.*)$/ )
			{
				last if ( ( ! defined $2 ) || ( $2 eq "" ) || ( $2 eq $key ) );
				my $newval = $configitems{ $2 };
				last unless ( defined $newval );
				$value = $1 . $newval . $3;
			}
			$configitems{$key} = $value;
		}
}

# Getter for single property

sub getConfig {
	my $key = $_[0];

	return $configitems{ $key };
}

# Return cloned copy of config hash (so changes to the cloned copy dont damage the copy read)
# Useful if using Config::properties split or the like

sub clone {
	my %config2;
	foreach my $key (keys %configitems) {
		$config2{ $key } = $configitems{ $key };
	}
	return %config2;
}

sub test_getConfig {
	print "test_getConfig\n";

	# Should say something like
	# Key web_protocol: value 'http://'
	# Key serveraddress: value 'jobflow.dev.tagworldwide.com'
	# Key jobflowserver: value 'http://jobflow.dev.tagworldwide.com'

	foreach my $key ( "web_protocol", "serveraddress", "jobflowserver" ) {
		my $value = getConfig( $key );
		print "Key $key: value '$value'\n";
	} 
}

# To test, remove the comment before exit __PACKAGE__->test_...
# and type TAG_ENV=DEV perl config.pl
# but remember to add the comment back before committing any change.

#exit __PACKAGE__->test_getConfig() unless caller();

##############################################################################
##############################################################################

1;
