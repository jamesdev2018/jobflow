#!/usr/local/bin/perl

package UserSession;

use strict;
use warnings;

use Template;
use Crypt::CBC;
use CGI::Session;
use CGI::Cookie;
use CGI;
use strict;
use Scalar::Util qw( looks_like_number );
use Encode qw(decode encode);
use JSON;

# Home grown - please do NOT include jobs.pl or anything that includes jobs.pl
use lib 'modules';

require "config.pl";

require "databaseUser.pl";

use CTSConstants;

# global session
my $userSession;

##################################################################
# from config // Khalid has changed this comment again 16:24
##################################################################

my $developers 		= Config::getConfig("developers");
my $jobflowServer 	= Config::getConfig("jobflowserver");

my $applicationurl 	= $jobflowServer . "/jobflow.pl";


##############################################################################
# the template object 
#
##############################################################################
my $tt = Template->new({
	RELATIVE => 1,
}) || die "$Template::ERROR\n";

##############################################################################
# CreateSession 
#
# pass in the cookie sid or undef
##############################################################################
sub CreateSession {
	my $sid = $_[0];
	my $query = $_[1];
	
	# retrieve existing session - or create a new one.
	$userSession = new CGI::Session( "driver:File", $sid, { Directory => '/tmp' } );
	$userSession->expire( Config::getConfig( 'sessionexpiry' ) );
	
	# make a cookie
	my $cookie = $query->cookie( -name =>'CGISESSID', -value => $userSession->id, -httponly => 1 );
	# send the cookie to the browser
	
	print $query->header( -cookie => $cookie );

	return $userSession;
}
##############################################################################
#       Listusers
#
##############################################################################
sub UpdateSession {
        my $userid              = $_[0];
        my $sessionid   = $_[1];

        ECMDBUser::db_UpdateSession( $userid, $sessionid );
}
