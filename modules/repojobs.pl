#!/usr/local/bin/perl
package RepoJobs;

use strict;
use warnings;

use Template;

use lib 'modules';

require "databaseRepoJobs.pl";

##################################################################
# the template object 
##################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";


##############################################################################
#       Dashboard
#
##############################################################################
sub FixRepoJobs
{
	my ($query, $session) = @_;
	
	my @jobs = ECMDBRepoJob::db_getBrokenJobs();
	
	my $vars = {
		jobs => \@jobs,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/fixrepojobs.html', $vars )
    		|| die $tt->error(), "\n";
}


sub RetryRepoJob
{
	my ($query, $session) = @_;
	
	my @jobids;
	my $jobid = $query->param("jobid");
	if($jobid) { push(@jobids, $jobid); } else { @jobids = $query->param("jobid[]"); }
	
	foreach my $ji (@jobids)
	{
		my $info = ECMDBRepoJob::db_get_job_info($ji);
	
		#1 = CHECKING IN
		ECMDBRepoJob::db_set_checked_out($ji, $info->{curopcoid}) if $info->{checkoutstatus} == 1;
		ECMDBRepoJob::db_checkin($ji, $info->{curopcoid}) if $info->{checkoutstatus} == 1;
	
		#3 = CHECKING OUT
		ECMDBRepoJob::db_set_checked_in($ji, 16) if $info->{checkoutstatus} == 3;
		ECMDBRepoJob::db_checkout($ji, $info->{curopcoid}) if $info->{checkoutstatus} == 3;
	}
	
	FixRepoJobs();
}


sub BaseRepoJob
{
	my ($query, $session) = @_;
	
	my @jobids;
	my $jobid = $query->param("jobid");
	if($jobid) { push(@jobids, $jobid); } else { @jobids = $query->param("jobid[]"); }
	
	foreach my $ji (@jobids)
	{
		ECMDBRepoJob::db_set_checked_in($ji, 16);
	}
	
	FixRepoJobs();
}


sub ResetRepoJob
{
	my ($query, $session) = @_;
	
	my @jobids;
	my $jobid = $query->param("jobid");
	if($jobid) { push(@jobids, $jobid); } else { @jobids = $query->param("jobid[]"); }
	
	foreach my $ji (@jobids)
	{
		ECMDBRepoJob::db_non_repo($ji);
	}
	
	FixRepoJobs();
}

1;
