#!/usr/local/bin/perl
package HTMLHelper;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use HTML::Entities;
use JSON;
use Template;

require 'users.pl';

##############################################################################
# the template object 
##############################################################################
my $tt = Template->new( { RELATIVE => 1, } ) || die "$Template::ERROR\n";

# setHeaderVars - set variables in vars hash for any variable that appears
# in ecm_html/header.html, configSettings div

# PS do not add anything sensitive to this list as the user can easily inspect the html

##############################################################################
# set HeaderVars, set variables commonly used from config
############################################################################## 
sub setHeaderVars {
	my ( $vars, $session ) = @_;

	foreach my $key ( 'adsendurl', 'archivejoburl', 'mongooseserver', 'supportemail', 'amonurl',
			'jobbag_url', 'viewrenderedpdf_url', 'preflight_url', 'viewcompare_url', 'viewhighres_url',
			'viewlowres_url', 'documentdownload_url', 'twist_monitor_url', 'one2edit_ext_url', 'insertion_cmd_api', 'pdf_enhancer_url', 'cmd_pdf_url', 'cmd_hm_pdf_url', 'ttp_master_url' ) {
		$vars->{ $key } = Config::getConfig( $key );
	}
	if ( defined $session ) {
		$vars->{roleid} = $session->param( 'roleid' );
		$vars->{rolename} = $session->param( 'rolename' );
		$vars->{userid} = $session->param( 'userid' );
		$vars->{username} = $session->param( 'username' );
		$vars->{fname} = $session->param( 'fname' );
		$vars->{lname} = $session->param( 'lname' );
	}
	
	$vars->{is_developer} = Users::isDeveloper($vars->{userid});
	$vars->{is_explorer} = Users::isExplorer($vars->{userid});
}

##############################################################################
#       htmlEditFormat ()
#		Strip HTML Tags
##############################################################################
sub htmlEditFormat {
	my $text	= $_[0];

	if ( defined $text ) {
		$text		=~ s|<.+?>+||g; 	#enhanced
		#$text 		=~ s/[^A-Za-z0-9 ]*/ /g;
	}
	return $text;
}

1;

