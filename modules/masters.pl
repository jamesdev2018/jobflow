#!/usr/local/bin/perl
package Masters;

use strict;
use warnings;

use Template;
use Crypt::CBC;
use CGI::Session;
use CGI::Cookie;
use CGI;

# Home grown
use lib 'modules';

require "databaseMasters.pl";
require 'databaseUser.pl';
require "databaseOpcos.pl";

##############################################################################
# the template object 
#
##############################################################################
my $tt = Template->new({
	RELATIVE => 1,
}) || die "$Template::ERROR\n";

sub masters {
	my ($query, $session) = @_;

	my $url;
	eval {
		SupplyCredentials( $session, $query );
		my $masterinstall = Config::getConfig('masterinstall');
		die "config::masterinstall not defined" unless defined $masterinstall;
		$url = $masterinstall . '?tsid=' . $session->id . '&rnd_no=' . $session->param('rnd_no');
	};
	if ( $@ ) {
		die "Masters::masters, $@";
	}
	return $url;
}

sub masterstudios {
	my ( $query, $session ) = @_;

	my $url;
	eval {
		SupplyCredentials( $session, $query );
		my $masterinstall = Config::getConfig('masterinstall');
		die "config::masterinstall not defined" unless defined $masterinstall;
		$url = $masterinstall . '?action=view&customerid=-1&tsid=' . $session->id . '&rnd_no=' . $session->param('rnd_no');
	};
	if ( $@ ) {
		die "Masters::masterstudios, $@";
	}
	return $url;
}

sub jrs {
	my ( $query, $session ) = @_;

	my $url;
	eval {
		SupplyCredentials( $session, $query );
		my $jrcmsinstall = Config::getConfig('jrcmsinstall');
		die "config::jrcmsinstall not defined" unless defined $jrcmsinstall;
		$url = $jrcmsinstall . '?tsid=' . $session->id . '&rnd_no=' . $session->param('rnd_no');
	};
	if ( $@ ) {
		die "Masters::jrs, $@";
	}
	return $url;
}

##############################################################################
# SupplyCredentials
#
# Sets several session variables which are passed to Masters and/or Repository
# - uname, username for user
# - email, email address for user
# - visiblecustomers, list of unique customer names
# - primaryopco, currently select opco name
# - role, current role name 
#
##############################################################################
sub SupplyCredentials {
	my ($session, $query) = @_;
	
	my $userid = $session->param('userid');
	my $roleid = $session->param('roleid');

	eval {
		Validate::userid( $userid );
		Validate::roleid( $roleid );
	
		my ( $opcoid, $primaryopco ) = ECMDBMasters::db_getUserPrimaryOpco($userid); # Use Default not Current opcoid

		my $visiblecustomers;
		if ( defined $opcoid ) {
			$visiblecustomers = ECMDBMasters::db_getVisibleCustomers( $userid, $opcoid );
		}
		
		$primaryopco = "" unless defined $primaryopco;
		$visiblecustomers = \[] unless defined $visiblecustomers;


		# If we have a role id in the session, get the role, otherwise get the default role (from users.role)
		my $rolename;
		if ( ! defined $roleid ) { 
			$rolename = ECMDBMasters::db_getCustomerPermissions( $userid ); 
		} else	{
			# use the roleid from the session to give rolename
			$rolename = ECMDBUser::getRoleName( $roleid );
		}

		$session->param(-name => "primaryopco",		-value => $primaryopco );
		$session->param(-name => "visiblecustomers",    -value => $visiblecustomers );

		$session->param(-name => "uname", -value => $session->param( "username" ) );
		# $session->param(-name => "email", ... already set by Jobflow

		$session->param( -name => "role", -value => $rolename ); # doesnt clash with JF roleid/rolename

		# Add rnd_no for future use by Masters/JRS, see modules/streamhelper.pl
		my $rnd_no = int( rand( 1000000 ) ); # use a random number 0..999999
		$session->param( 'rnd_no', $rnd_no );
		$session->flush();

	};
	if ( $@ ) {
		my $error = $@;
		die "SupplyCredentials: userid: " . ( $userid || 'undef' ) . ', roleid: ' . ($roleid || 'undef') . " $error";
	}	
	return 0;
}


##############################################################################
##############################################################################
#       end of file
##############################################################################
##############################################################################
1;
