#!/usr/local/bin/perl
package ECMDBGroups;
use lib 'modules';

sub db_deptstatusupdate
{
        my $deptid=$_[0];
        my $jobs=$_[1];

        if(length($jobs))           { $jobs = "('" . ($jobs =~ s/,/','/rg) . "') "; }

        $Config::dba->process_sql("UPDATE TRACKER3PLUS SET DEPARTMENTVALUE=? WHERE job_number in $jobs",[$deptid]);
        return 1;
}
sub db_teamstatusupdate
{
        my $teamid=$_[0];
        my $jobs=$_[1];

        if(length($jobs))           { $jobs = "('" . ($jobs =~ s/,/','/rg) . "') "; }

        $Config::dba->process_sql("UPDATE TRACKER3PLUS SET TEAMVALUE=? WHERE job_number in $jobs",[$teamid]);
        return 1;
}

sub db_updatequerytype
{
        my $querytypeid=$_[0];
        my $jobs=$_[1];

        if(length($jobs))           { $jobs = "('" . ($jobs =~ s/,/','/rg) . "') "; }

        $Config::dba->process_sql("UPDATE TRACKER3PLUS SET QUERYTYPE=? WHERE job_number in $jobs",[$querytypeid]);
        return 1;
}
1;
