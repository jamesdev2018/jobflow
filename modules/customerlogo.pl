#!/usr/local/bin/perl
package Customerlogo;

use strict;
use warnings;
use Template;
use Scalar::Util qw( looks_like_number );
use JSON;
use Encode qw(decode encode);
use Carp qw /carp/;
use Data::Dumper;
use File::Path;
use File::Copy;
use File::Basename;

require "config.pl";
require "databaseOpcos.pl";


sub upload_logo {

	my ($opcoid,$customerName,$customerLogo,$logo_repository) 	= @_;
	my $file_name;
	my $opconame					= ECMDBOpcos::getOpcoName( $opcoid );
	my $logoName 					= $1;
        my $logoExt  					= $2;
        my $logowrite 					=undef;
	eval {
        	my $UPLOAD_FH = $customerLogo;
                $file_name = sprintf("%s_%s.%s","$opconame","$customerName","$logoExt");
                my $logoTempPath = sprintf ( "%s/%s", "/temp", "customerlogo" );
                if ( -d "$logoTempPath")
                {
                       # No process
                }
                else
                {
                        mkdir( "$logoTempPath", 0777 );
                }
                my $filename = "$logoTempPath/$file_name";
                open( CUSTOMERLOGO, ">$filename" ) || die "Failed to open output file '$filename', $!";
                binmode CUSTOMERLOGO;
                while ( <$UPLOAD_FH> ) {
                    print CUSTOMERLOGO;
                }
                close ( CUSTOMERLOGO );
                chmod 0777, $filename;
                my ( $ftpServer, $ftpUserName, $ftpPassword ) = ECMDBOpcos::db_getFTPDetails( $opcoid );
                die "Invalid FTP server for opcoid '$opcoid'" unless ( $ftpServer );
                my $ftp = Net::FTP->new( $ftpServer, Debug => 0 ) or die "Cannot connect to FTP server $@";
                if ( $ftp ) {
                        $ftp->login( $ftpUserName, $ftpPassword ) or die "Cannot login", $ftp->message;
                        $ftp->binary();
                        
                        my $destRootDir=$logo_repository;
                        my $digitalAssetsDest  = sprintf( "%s/%s/%s", $destRootDir, 'customerlogo', $file_name );
                        my $assetRepoJobFldr = dirname( $destRootDir );
                        my $dirExist = $ftp->cwd( "$destRootDir" );
                        if ( $dirExist eq "" ) {
                                       $ftp->mkdir ( "$destRootDir/customerlogo", "true") or die "Directory creation failed, $destRootDir,", $ftp->message;
                        } else {
                                        my $dirExist = $ftp->cwd( "customerlogo" );
                                        if ( $dirExist eq "" ) {
                                                $ftp->mkdir ( "customerlogo", "true") or die " digital Directory creation failed, brandedlogo,", $ftp->message;
                                        }
                        }
                        $ftp->site( "chmod 777 $destRootDir" );
                        $ftp->site( "chmod 777 $destRootDir/customerlogo" );
                        $ftp->binary();
                        $ftp->put( "$filename", "$digitalAssetsDest" ) or die "put failed, ", $ftp->message;
                        $ftp->site("chmod 777 $digitalAssetsDest");
                        $ftp->quit;
                        $ftp = undef;
          	}
                 $logowrite = "Uploaded Successfully";
                
 	};
	if ( $@ ) {
        	my $error = $@;
            	carp "EditCustomer: $error";
            	$error =~ s/ at .*//gs;
            	$logowrite = $error;
	}
	return $file_name,$logowrite;

}


1;
