#!/usr/local/bin/perl

package XMLResponse;

use strict;
use warnings;

use lib "Utils";

use constant STATUS_UNKNOWN		=> "unknown";
use constant STATUS_RECEIVED		=> "received";
use constant STATUS_IN_PROGRESS 	=> "inprogress";
use constant STATUS_DISPATCHED		=> "dispatched";
use constant STATUS_ERROR		=> "error";
use constant STATUS_REJECTED		=> "rejected";
use constant STATUS_ADVISORY		=> "advisory";



use base 'Exporter';

our @EXPORT = qw (STATUS_UNKNOWN STATUS_RECEIVED STATUS_IN_PROGRESS STATUS_DISPATCHED STATUS_ERROR STATUS_REJECTED STATUS_ADVISORY write_status);

our %EXPORT_TAGS = ( statuses => [ 'STATUS_UNKNOWN', 'STATUS_RECEIVED', 'STATUS_IN_PROGRESS', 'STATUS_DISPATCHED', 'STATUS_ERROR', 'STATUS_REJECTED', 'STATUS_ADVISORY' ],
					 write_status => [ 'write_status' ]);


#PASS IN THE FILE PATH YOU WANT TO SAVE THE XML FILE AS
#A STATUS WORD (ONE OF THE ABOVE) AND A CUSTOM DESCRIPTION

sub write_status
{
	my ($filename, $urn, $obanum, $statuscode, $description) = @_;
	
	#VALIDATION CHECKING
	$description = "" unless $description;
	$statuscode = STATUS_UNKNOWN if $statuscode ne STATUS_RECEIVED && $statuscode ne STATUS_ERROR &&
								 $statuscode ne STATUS_IN_PROGRESS && $statuscode ne STATUS_DISPATCHED &&
								 $statuscode ne STATUS_REJECTED && $statuscode ne STATUS_ADVISORY;
	
	
	#GENERATE A FILENAME			
	$filename =~ m/^(.*?)(\.[^.]*)?$/;
	my $fname = $1 || "";
	my $fext  = $2 || "";
	$filename = "$fname$obanum-$urn-$statuscode$fext" if $fname eq "";
	
	
	#WRITE THE XML FILE
	open OUTPUT, ">$filename";
		
		print OUTPUT "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
		print OUTPUT "<!-- Written by XMLResponse -->\n";
		
		print OUTPUT "<response>\n";
			print OUTPUT "\t<urn>$urn</urn>\n";
			print OUTPUT "\t<oba>$obanum</oba>\n";
			print OUTPUT "\t<statuscode>$statuscode</statuscode>\n";
			print OUTPUT "\t<description><![CDATA[$description]]></description>\n";
		print OUTPUT "</response>";
	
	close OUTPUT;
	
	return $filename
}

1;
