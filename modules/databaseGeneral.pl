#!/usr/local/bin/perl
package ECMDBGeneral;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );

# Please do NOT use this module as a dumping ground
# putting code in something called General is the most unhelpful thing you can do.
# Yes, you know who you are.

# Home grown

# addNews, updateNews, getNewsItems, getNewsItem absorbed into news.pl

# getMaxCheckInLimit, getMaxCheckOutLimit, updateMaxCheckIns, updateMaxCheckOuts, getDaemons, updateDaemonOn moved to daemonpanel.pl

# db_getOperatorStatuses + db_getPRODStatuses moved to databaseJob.pl

# graphCalc moved to modules/graphcalc.pl

# getMaxCheckInLimit, getMaxCheckOutLimit, updateMaxCheckIns, updateMaxCheckOuts,
# getDaemons, updateDaemonOn moved to daemons.pl 

1;

