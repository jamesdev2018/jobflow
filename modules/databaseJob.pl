#!/usr/local/bin/perl
package ECMDBJob;

use strict;
use warnings;

# use Switch; # doesnt use switch statement
use Scalar::Util qw( looks_like_number );
use JSON;
use Carp qw/carp cluck/;
use Time::HiRes qw/time/;
use Encode qw(decode encode);
use utf8;
use URI::Escape;
use URI::URL;
use Data::Dumper;

# Home grown

use constant COMMENT_DELETED => 0;
use constant COMMENT_ACTIVE => 1;
use constant COMMENT_PREVIOUS_REVISION => 2;

use lib 'modules';

use CTSConstants;
use Validate;

require "config.pl";
require "Mailer.pl";
require "general.pl";

#require "databaseAudit.pl"; # just for db_recordJobAction TODO is it worth doing this to share common copy ? NO

##############################################################################
#      db_getJobOpcoid 
#
##############################################################################
sub db_getJobOpcoid
{
	my ($jobid) = @_;
	return 0 if $jobid && !looks_like_number($jobid);	## VALIDATE INPUT
	my ($opcoid) = $Config::dba->process_oneline_sql("SELECT opcoid FROM TRACKER3 WHERE job_number=?", [ $jobid ]);

	return $opcoid;
}

##############################################################################
#      db_getLocation 
#	get the current location (the opcoid) of a job or its shards
##############################################################################
sub db_getLocation
{
	my ($jobid, $shardname) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);	## VALIDATE INPUT
	
	my $location = 0;
	if($shardname)	{ ($location) = $Config::dba->process_oneline_sql("SELECT location FROM SHARDS WHERE job_number=? AND shard_name=?", [ $jobid, $shardname ]); }
	else				{ ($location) = $Config::dba->process_oneline_sql("SELECT location FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]); }

	return $location;
}
##############################################################################
#      db_getJobRemoteOpcoid 
#
##############################################################################
sub db_getJobRemoteOpcoid
{
	my ($jobid) = @_;

	return 0 if $jobid && !looks_like_number($jobid);	## VALIDATE INPUT
	my ($opcoid) = $Config::dba->process_oneline_sql("SELECT location FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ] );

	return $opcoid;
}
##############################################################################
#      db_getCustomerIdJob
#       get the customer id on the JOB itself
##############################################################################
sub db_getCustomerIdJob
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);	## VALIDATE INPUT
	my ($customerid) = $Config::dba->process_oneline_sql("SELECT customerid FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	
    return $customerid;
}
##############################################################################
#      db_getCustomerid 
#
##############################################################################
sub db_getCustomerid
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);	## VALIDATE INPUT
	##It has to refer the custid of current location (t3.location) instead of origin custid
	my ($customerid) = $Config::dba->process_oneline_sql("SELECT id FROM CUSTOMERS
												WHERE opcoid=(SELECT opcoid FROM TRACKER3 WHERE job_number=?)
												AND customer=(SELECT agency FROM TRACKER3 WHERE job_number=?)", [ $jobid, $jobid ]);
												
	return $customerid;
}


##############################################################################
#      db_getProjectJob
#       get the customer id on the JOB itself
##############################################################################
sub db_getProjectJob
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);	## VALIDATE INPUT
	my ($projectname) = $Config::dba->process_oneline_sql("SELECT project FROM TRACKER3 WHERE job_number=?", [ $jobid ]);
	
    return $projectname;
}

##############################################################################
#      db_getCustomer 
#	get the customer name from the jobid
##############################################################################
sub db_getCustomer
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);	## VALIDATE INPUT
	my ($customer) = $Config::dba->process_oneline_sql("SELECT agency FROM TRACKER3 WHERE job_number=?", [ $jobid ]);

	return $customer;
}

##############################################################################
#      db_getJobPathInfo 
#	get the Job Path Info
##############################################################################
sub db_getJobPathInfo
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);	## VALIDATE INPUT
	my $retval = $Config::dba->hashed_process_sql("SELECT t3.job_number AS jobnumber, t3.rootpath, t3.letter, t3.agency, t3.client, t3.project, t3.proofing, t3.ucr, t3.workflow, oc.opcoid, oc.opconame AS opco
											FROM TRACKER3 t3
											INNER JOIN OPERATINGCOMPANY oc ON oc.opcoid=t3.opcoid
											INNER JOIN TRACKER3PLUS t3p ON t3p.job_number=t3.job_number
										WHERE t3.job_number=?", [ $jobid ]);
	return @{ $retval }[0];
}

sub getJobQueryType {
	my ( $job_number ) = @_;
 
	my ( $qt_id, $qt_name ) = $Config::dba->process_oneline_sql(
		"SELECT qt.id, qt.querytype FROM tracker3plus t3p LEFT OUTER JOIN querytypes qt ON qt.id = t3p.querytype WHERE t3p.job_number=?",
		[ $job_number ] );
	$qt_name = '' unless defined $qt_name;
	return (  $qt_id, $qt_name );
}

##############################################################################
#      db_getJobLocation 
#	get the Job location
##############################################################################
sub db_getJobLocation
{
	my ($jobid, $shard) = @_;
	my ($locopcoid, $locopconame, $locip, $locwip, $originopcoid, $originopconame, $originip, $originwip, $isinrepo);
	my ($current_opcoid, $current_opco, $current_opcoip, $current_opcowip, $origin_opcoid, $origin_opco, $origin_opcoip, $origin_opcowip);
	
	return 0 if $jobid && !looks_like_number($jobid);
	$shard ||= '';
	
	## Figure out where the job is
	my $retvals = $Config::dba->hashed_process_sql("SELECT t3p.job_number AS jobid, s.shard_name, t3p.opcoid, IFNULL(s.location, t3p.location) AS location, IFNULL(rciq.opcoid, rcoq.opcoid) AS queuelocation,
														IFNULL(s.checkoutstatus, t3p.checkoutstatus) != 0 AS isinrepo FROM TRACKER3PLUS t3p
													LEFT OUTER JOIN REPOCHECKINQUEUE rciq ON rciq.job_number=t3p.job_number AND rciq.shard_name=?
													LEFT OUTER JOIN REPOCHECKOUTQUEUE rcoq ON rcoq.job_number=t3p.job_number AND rcoq.shard_name=?
													LEFT OUTER JOIN SHARDS s ON s.job_number=t3p.job_number AND s.shard_name=?
													WHERE t3p.job_number=?", [ $shard, $shard, $shard, $jobid ]);
	my $jobinfo = @{ $retvals }[0];
	
	##If queuelocation value is not defined but location is set as 15 then it results in split error. So we have applied this fix to overwrite the location value by queue location when is set as 15 only when queuelocation value is present
	if ($jobinfo->{queuelocation})
	{
		$jobinfo->{location} = $jobinfo->{queuelocation} if ($jobinfo->{location}||'') eq "15";
	}
	$jobinfo->{location} = $jobinfo->{opcoid} if ($jobinfo->{location}||'') eq "16";
	$jobinfo->{curloc} = $jobinfo->{location};
	
	## Get opco information for these locations
	my $origininfo   = $Config::dba->hashed_process_sql("SELECT opcoid AS origin_opcoid,  opconame AS origin_opco,  domainname AS origin_opcoip,  wippath AS origin_opcowip  FROM OPERATINGCOMPANY WHERE opcoid=?", [ $jobinfo->{opcoid} ]);
	my $locationinfo = $Config::dba->hashed_process_sql("SELECT opcoid AS current_opcoid, opconame AS current_opco, domainname AS current_opcoip, wippath AS current_opcowip FROM OPERATINGCOMPANY WHERE opcoid=?", [ $jobinfo->{location} ]);
	
	my %combination = (%$jobinfo, %{ @{ $origininfo }[0] }, %{ @{ $locationinfo }[0] });
	return \%combination;
}

sub db_getJobInfo
{
	my ($jobid) = @_;
	
	my $retvals = $Config::dba->hashed_process_sql("SELECT t.workflow, mt.twist_tmp_path FROM TRACKER3 t
		LEFT OUTER JOIN mediatypes mt ON mt.type=t.workflow
			WHERE t.job_number=?", [ $jobid ]);
	
	return @{ $retvals }[0];
}



#   removed    db_getJobs

##############################################################################
# Switch a flag
##############################################################################
sub db_setActiveFlag
{
	my ($jobid, $columnname, $value) = @_;

	## VALIDATE INPUTS
	return 0 if $value !~ m/^(0|1)$/;							## ACCEPT ONLY 1 OR 0
	return 0 if $jobid && !looks_like_number($jobid);			## ACCEPT ONLY [JOB] NUMBERS
	return 0 if $columnname !~ m/^(colourmanage|optimise)$/i;	## ACCEPT ONLY CERTAIN COLUMN NAMES
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET $columnname" . "_isactive=? WHERE job_number=?", [ $value, $jobid ]);
	
	return 1;
}

##############################################################################
# update creative flag
##############################################################################
sub db_setCreative
{
	my ($jobid, $creative) = @_;

	## VALIDATE INPUTS
	return 0 if $creative !~ m/^(0|1)$/;							## ACCEPT ONLY 1 OR 0
	return 0 if $jobid && !looks_like_number($jobid);			## ACCEPT ONLY [JOB] NUMBERS
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET creative=? WHERE job_number=?", [ $creative, $jobid ]);
	
	return 1;
}

##############################################################################
# update estimate of a job
##############################################################################
sub db_updateEstimate
{
	my ($jobid, $estimate) = @_;
	
	## VALIDATE INPUTS
	return 0 if $estimate && !looks_like_number($estimate);
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET estimate=? WHERE job_number=?", [ $estimate, $jobid ]);
	
	return 1;
}

sub db_update_broadcast {
	my ( $job_number, $values ) = @_;

	die "Invalid job number" unless defined $job_number;
	die "Invalid job number '$job_number'" unless looks_like_number( $job_number );
	die "Invalid values" unless ref ( $values ) eq 'HASH';

	# warn "db_update_broadcast: job $job_number, values " . Dumper( $values ) . "\n";

	my @list_of_columns;
	my @insert_values;
	my @placeholders;

	my $atlas_fields = 'country|language|videoratio|videostd|videoframerate|videoletterbox|audiostd|editcode|bdccode';

	my ( $editcode, $country ) = $Config::dba->process_oneline_sql(
		"SELECT editcode, country FROM broadcasttracker3 WHERE job_number = ?",[ $job_number ] );

	if ( ! defined $country ) { # country is a NOT NULL column
		# Assert row doesnt exist yet on broadcasttracker3

		# Check workflow on tracker3 to be sure
		my ( $cmdid, $workflow ) = $Config::dba->process_oneline_sql( "SELECT cmdid, workflow FROM tracker3 WHERE job_number = ?",[ $job_number ] );
		die "Job '$job_number' doesnt exist in tracker3" if ( ! defined $cmdid ); # cmdid is NOT NULL, so good test for row present
		die "Job '$job_number' is not a broadcast job" unless ( $workflow eq 'BROADCAST' );

		# Most columns are nullable with default values, however, country defaults to 0 but with no mapping for that value
		# 1st entry in country has id 1, => 'None'

		$values->{country} = 1 if ( ( ! defined $values->{country} ) || ( ! looks_like_number( $values->{country} ) ) || ( $values->{country} == 0 ) );

		# Also, bizarrely, all the date columns are VARCHAR

		$values->{job_number} = $job_number;
		foreach my $key ( keys %{$values} ) {
			next unless defined $values->{ $key };
			push( @list_of_columns, $key );
			push( @insert_values, $values->{ $key } );
			push( @placeholders, '?' );
		}
		push( @list_of_columns, 'rec_timestamp' );
		push( @placeholders, 'now()' );
		my $sql = 'INSERT INTO broadcasttracker3 (' . join( ',', @list_of_columns ) . ') VALUES( ' . join( ',', @placeholders ) . ') ';
		# warn "db_update_broadcast: $sql\n";
		$Config::dba->process_sql( $sql, \@insert_values );

	} else {
		# check to see if editcode is set (something other than undef, blank or - must contain at least 1 digit)
		# If so, dont allow update of those fields populated by Atlas (country, language, ... audio standard)
		my $atlas_update = 0;
		if ( defined $editcode ) {
			my $copy = $editcode;
			$copy =~ s/\s*//gs;
			# warn "db_update_broadcast: editcode is '$editcode', clean copy '$copy'\n";
			$atlas_update = 1 if ( length($copy) > 1 );
		}

		foreach my $key ( keys %{$values} ) {
			next if ( $atlas_update && $key =~ /$atlas_fields/ );

			push( @list_of_columns, "$key = ?" );
			push( @insert_values, $values->{ $key } );
		}
		my $sql = 'UPDATE broadcasttracker3 SET ' . join( ',', @list_of_columns ) . ' WHERE job_number = ?';
		# warn "db_update_broadcast: $sql\n";
		push( @insert_values, $job_number );
		$Config::dba->process_sql( $sql, \@insert_values );

	} # endif
}

##############################################################################
#       db_getJob
#
# Get the job details for an individual job, called by
# modules/sendjobs.pl:		my @job	= ECMDBJob::db_getJob( $jobid );
# modules/showjob.pl:		my @job = ECMDBJob::db_getJob( $myjob );
# modules/upload.pl:		my @job	= ECMDBJob::db_getJob( $jobid );
# Returns an arry of jobs, even though it can only actually return one job
# but changing this now is a big change.
#      
##############################################################################
sub db_getJob {
	my ( $jobid ) = @_;

	#return () if $jobid && !looks_like_number($jobid);
	die "db_getJob: jobid undefined" unless ( defined $jobid );
	die "db_getJob: jobid '$jobid' invalid" unless looks_like_number( $jobid );

	# Rather than faff around trying to work out if this is a Broadcast job and if so whether it has a row on broadcasttracker3 yet
	# a) Do the normal query of tracker3 without a JOIN on Broadcasttracker3
	# b) look at the workflow returned to determine if we need to ...
	# c) If it is a broadcast job, just query broadcasttracker3 with inner joins on all tables except country (use left outer)
	# d) If c) returns data, merge it into the results from a)

    my $rows = $Config::dba->hashed_process_sql(
		"SELECT t.job_number, t.CUSTOMERORDERNUMBER, 
			t.agency, t.HEADLINE, t.DESCRIPTION, t.MATDATE, t.requiredDate, tp.REC_TIMESTAMP, 
			t.client, t.project, t.publication, t.duration, t.formatsize, t.worktype,
			t.colour, t.agencyref, t.pathtype, t.workflow AS actiontype, t.producer, t.mediachoiceid,
			tp.locked, CASE tp.locked WHEN 1 THEN 'Yes' ELSE 'No' END AS islocked, tp.locked_time AS lockedtime,
			tp.assetscreated, tp.status AS statusid,
			tp.twist, tp.bw, tp.bwcropped, tp.lowrescropped, tp.lowres, tp.hires, tp.proofonly,
			tp.rgb, tp.rgbcropped, tp.rgbjpeg, tp.hirescroppedbleedbox, tp.hirescroppedtrimbox,
			tp.colourmanage, tp.colourmanage_isactive, tp.optimise, tp.optimise_isactive, tp.pdfcompare,
			tp.compareready, tp.dispatchdate,tp.qcstatus, tp.assignee, tp.duedate, tp.estimate, tp.operatorstatus,
			tp.creative, tp.brief, tp.comparefolder AS jobsize,
			cu.bwdef, cu.bwcroppeddef, cu.lowrescroppeddef, cu.lowresdef, cu.hiresdef, cu.proofonlydef,
			cu.rgbdef, cu.rgbcroppeddef, cu.rgbjpegdef, cu.hirescroppedbleedboxdef, cu.hirescroppedtrimboxdef,
			op.colourmanagedef, op.colourmanage_isactivedef, op.optimisedef, op.optimise_isactivedef, op.pdfcomparedef, 
			pfs.status AS preflightstatus, 
			pds.status As productionstatus, 
			u.username AS lastuser,
			EXISTS(SELECT 1 FROM shards s WHERE (s.job_number = t.job_number) AND location != tp.location) AS multiple_locations
		FROM  TRACKER3 t 
			INNER JOIN TRACKER3PLUS tp  ON t.job_number = tp.job_number    
			INNER JOIN CUSTOMERS cu  ON tp.customerid = cu.id              
			INNER JOIN OPERATINGCOMPANY op ON op.opcoid = tp.opcoid
			INNER JOIN OPERATINGCOMPANY op2 ON op2.opcoid = tp.location
			INNER JOIN PREFLIGHTSTATUS pfs ON tp.preflightstatus = pfs.id
			INNER JOIN PRODUCTIONSTATUS pds ON t.productionStatus = pds.id
			LEFT OUTER JOIN USERS u ON u.id = tp.lastuser
		WHERE  t.job_number= ?
		GROUP BY t.job_number", [ $jobid ]);

	if ( defined $rows ) {

		# Get the shard names (comma separated list)

		foreach (@{$rows} ) {

			# Get list of shard names for job

			if ( $_->{multiple_locations} ) {
				# warn "db_getJob: getting shard names\n";
				( $_->{shards} ) = $Config::dba->process_oneline_sql(
					"SELECT GROUP_CONCAT(CONCAT(s.shard_name, ' [', IFNULL(oc.opconame, '[unknown]'), ']') SEPARATOR '<br>') AS shard FROM shards s
					INNER JOIN OPERATINGCOMPANY oc ON oc.opcoid=s.location
					WHERE s.job_number=? AND EXISTS(SELECT 1 FROM shards ss WHERE s.location != ss.location);", [ $_->{job_number} ]);
			}
			# See if we have a Broadcast job
			# use LEFT OUTER JOIN on countries in case we have btp.country = 0 i
			# Also (PD-3492) we get the producer from tracker3.producer rather than joining with producerresponsible
			# so removed "pr.name AS prname" from the query along with "LEFT OUTER JOIN producerresponsible pr ON btp.PRODUCERRESP = pr.id"

			if ( $_->{actiontype} eq 'BROADCAST' ) {
				# warn "db_getJob: getting broadcast data\n";
				my $bcrows = $Config::dba->hashed_process_sql(
					"SELECT btp.ACC_MANAGER, btp.ORDERDATE, btp.AGENCY_JOBNO, btp.DELIVERYDATE, btp.scheduledate, 
						btp.EDITCODE, btp.INTERNAL_BLOCKNO, btp.BDCCODE, btp.SMOKECMD, btp.PRODUCERRESP, ct.country AS country, lg.language, 
						vr.ratio AS videoratio, vs.standard AS videostd, vfr.ratio AS frameratio, vbx.ratio AS videoletterbox, aud.standard AS audiostd
					FROM Broadcasttracker3 btp
					LEFT OUTER JOIN countries ct ON btp.country = ct.id
					INNER JOIN languages lg ON btp.language = lg.id
					INNER JOIN videoratio vr ON btp.videoratio = vr.id
					INNER JOIN videostandard vs ON btp.videostd = vs.id
					INNER JOIN videoframerate vfr ON btp.videoframerate = vfr.id
					INNER JOIN videoletterbox vbx ON btp.videoletterbox = vbx.id
					INNER JOIN audiostandard aud ON btp.audiostd = aud.id
					WHERE btp.job_number = ?", [ $jobid ] );
				if ( (defined $bcrows) && ( scalar( @{$bcrows} ) > 0 ) ) {
					# Merge
					# warn "Broadcast data present\n";
					my $r = @{$bcrows}[ 0 ]; # First row is all we want
					# warn "Broadcast data " . Dumper( $r ) . "\n";
					foreach my $key ( keys %{$r} ) {
						$_->{ $key } = $r->{ $key };
					}
					$_->{SMOKECMD} = uri_escape_utf8( $_->{SMOKECMD} ) if exists $_->{SMOKECMD};
				} else {
					#warn "Broadcast data not present\n";
				}
			}
		}		
	}

	my @jobs = $rows ? @$rows : ();
	return @jobs;
}

##############################################################################
#       db_updateJob
#      
# 	14/06/12 - after discussing with RT, we are not NOT allowing the user to save 
#  	back the download types
# 	- these are now readonly and fixed.
#
##############################################################################

sub db_updateJob {

	my ( $jobid, $qcstatus ) = @_;

	eval {
		Validate::job_number( $jobid );

		# qcstatus arg can be scalar (in which case assume it is the QC status) or a HASH ref

		my $localref;
		die "Invalid qcstatus" unless defined $qcstatus;
		if ( ref( $qcstatus ) eq 'HASH' ) {
			$localref = $qcstatus;
		} else {
			die "Invalid qcstatus" unless ( looks_like_number($qcstatus) );
			$localref = { qcstatus => $qcstatus, };
		} 

		my @columns;
		my @values;
		foreach my $k ( keys %$localref ) {
			push( @columns, $k . ' = ?' );
			my $v = $localref->{ $k };
			$v = undef if $v eq 'NULL'; # Hack as we cant pass hash key value with value = undef
			push( @values, $v );
		}
		my $updateColDetails = join( ', ', @columns );

		$Config::dba->process_sql(
				"UPDATE TRACKER3PLUS SET $updateColDetails WHERE job_number=?", [ @values, $jobid ]  );
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "db_updateJob: $error";
		return 0;
	}
	return 1;
}

##############################################################################
#       db_lockJob
#      
##############################################################################
sub db_lockJob
{
	my ($jobid, $locked) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $locked && !looks_like_number($locked);
	
	$Config::dba->process_sql("UPDATE TRACKER3PLUS SET locked=?, locked_time=now() WHERE job_number=?", [ $locked, $jobid ]);
	return 1;
}

##############################################################################
#       db_showAssets
#      
##############################################################################
sub db_showAssets
{
	my ($jobid, $assetsCreated) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $assetsCreated && !looks_like_number($assetsCreated);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET assetscreated=? WHERE job_number=?", [ $assetsCreated, $jobid ]);
	return 1;
}




##############################################################################
#       db_touchJob
#      
##############################################################################
sub db_touchJob
{
	my ($jobid, $lastuser) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $lastuser && !looks_like_number($lastuser);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET lastuser=?, mod_timestamp=now() WHERE job_number=?", [ $lastuser, $jobid ]);
	return 1;
}


##############################################################################
#		db_updateAssetsCreated
#		Toggle assetscreated flag in tbl TRACKER3PLUS
# 
##############################################################################
sub db_updateAssetsCreated
{
	my ($jobid, $assetscreated) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $assetscreated && !looks_like_number($assetscreated);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET assetscreated=? WHERE job_number=?", [ $assetscreated, $jobid ]);
	return 1;
}

##############################################################################
#		db_updateCompare
#		Toggle compareready flag in tbl TRACKER3PLUS
# 
##############################################################################
sub db_updateCompare
{
	my ($jobid, $compareflag) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $compareflag && !looks_like_number($compareflag);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET compareready=? WHERE job_number=?", [ $compareflag, $jobid ]);
	return 1;
}


##############################################################################
#       db_jobExists - does this job exist in TRACKER3PLUS?
#      
##############################################################################
sub db_jobExists
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($jobcount) = $Config::dba->process_oneline_sql("SELECT COUNT(*) FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	return $jobcount;
}


##############################################################################
#       db_jobPassed - has this job passed preflight?
#      - a job has passed if it is EITHER passed(2) or dispatched(6).
##############################################################################
sub db_jobPreflightforproject
{
        my ($jobid) = @_;


        return 0 if $jobid && !looks_like_number($jobid);

        my ($preflightstatus) = $Config::dba->process_oneline_sql("SELECT preflightstatus FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
        return 1 if $preflightstatus == 2 || $preflightstatus == 6 || $preflightstatus == 7;
        return 0;
}
##############################################################################
#       db_jobPassed - has this job passed preflight?
#      - a job has passed if it is EITHER passed(2) or dispatched(6).
##############################################################################
sub db_jobPassed
{
	my ($jobid) = @_;
	
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($preflightstatus) = $Config::dba->process_oneline_sql("SELECT preflightstatus FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	return 1 if $preflightstatus == 2 || $preflightstatus == 6;
	return 0;
}


##############################################################################
#       db_jobQCApproved - has this job passed quality control?
#      
##############################################################################
sub db_jobQCApproved
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($qcstatus) = $Config::dba->process_oneline_sql("SELECT qcstatus FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	return 1 if $qcstatus == 2;
	return 0;
}


##############################################################################
#       db_jobQCRequired - Is this job set to QC Required?
#      
##############################################################################
sub db_jobQCRequired
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($qcstatus) = $Config::dba->process_oneline_sql("SELECT qcstatus FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	return 1 if $qcstatus == 1;
	return 0;
}




##############################################################################
#       db_assetsCreated
#      - have the assets been created for this job?
##############################################################################
sub db_assetsCreated
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($assetscreated) = $Config::dba->process_oneline_sql("SELECT assetscreated FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	return $assetscreated || 0;
}


##############################################################################
#       db_getQCStatus
#      - what is the QC status of this job?
##############################################################################
sub db_getQCStatus
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($qcstatus) = $Config::dba->process_oneline_sql("SELECT qcstatus FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	return $qcstatus || 0;
}


##############################################################################
#       db_isLegacy
#      - is this a legacy job?
##############################################################################
sub db_isLegacy
{
	
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($islegacy) = $Config::dba->process_oneline_sql("SELECT pathtype = 10 FROM TRACKER3 WHERE job_number=?", [ $jobid ]);
	return $islegacy || 0;
}

##############################################################################
#       isEnhancedFTP
#      - does this job belong to a customer that has Enhanced FTP enabled? 
##############################################################################
sub isEnhancedFTP
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($isEnhancedFTP) = $Config::dba->process_oneline_sql("SELECT c.enhancedftp FROM CUSTOMERS c
													INNER JOIN TRACKER3PLUS t3p ON t3p.customerid=c.id
													WHERE t3p.job_number=?", [ $jobid ]);
	return $isEnhancedFTP || 0;
}

##############################################################################
#       getAgencyRef
##############################################################################
sub getAgencyRef
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($agencyref) = $Config::dba->process_oneline_sql("SELECT agencyref FROM TRACKER3 WHERE job_number=?", [ $jobid ]);
	return $agencyref || '';
}

##############################################################################
#       getVersion
##############################################################################
sub getVersion
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($version) = $Config::dba->process_oneline_sql("SELECT version FROM TRACKER3 WHERE job_number=?", [ $jobid ]);
	return $version || 0;
}

##############################################################################
#       getOBANumber
##############################################################################
sub getOBANumber
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($obanumber) = $Config::dba->process_oneline_sql("SELECT size FROM TRACKER3 WHERE job_number=?", [ $jobid ]);
	return $obanumber || 0;
}

##############################################################################
#       db_updateJobPreflightStatus
#
##############################################################################
sub db_updateJobPreflightStatus
{
	my ($jobid, $preflightstatus) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $preflightstatus && !looks_like_number($preflightstatus);
	$Config::dba->process_oneline_sql("UPDATE TRACKER3PLUS SET preflightstatus=?, PREFLIGHTDATE=now() WHERE job_number=?", [ $preflightstatus, $jobid ]);
}


##############################################################################
#       db_getPreflight
#      
##############################################################################
sub db_getPreflight
{
	my $rows = $Config::dba->hashed_process_sql("SELECT id, profile FROM PREFLIGHT ORDER BY id");
	return $rows ? @$rows : ();
}



##############################################################################
#       db_getPreflightStatusValues
#      
##############################################################################
sub db_getPreflightStatusValues
{
	my $rows = $Config::dba->hashed_process_sql("SELECT id, status FROM PREFLIGHTSTATUS ORDER BY status asc");
	my @prestatus=();

	foreach my $row (@$rows)
	{
		## PREPARE FILTER DISPLAY VALUES FOR DASHBOARD
		push(@prestatus, { id => "PRE_" . $row->{id},   name => $row->{status} });
	}
	return @prestatus;
}

# db_getOperatorStatuses and db_getPRODStatuses moved from databaseGeneral.pl
# whilst these dont use tracker3plus or tracker3, they are better off here.

##############################################################################
#	db_getOperatorStatuses()
#
##############################################################################
sub db_getOperatorStatuses {

	my @operatorstatuses;

	my $rows = $Config::dba->hashed_process_sql("SELECT id, status FROM OPERATORSTATUS order by status asc" );
	if ( defined $rows ) {
		foreach my $r ( @$rows ) {
			push ( @operatorstatuses, { id => $r->{id}, status => $r->{status}, name => $r->{status} } );
		}
	}

	return @operatorstatuses;	
}

sub db_getPRODStatuses {

        my @prodstatus;

	my $rows = $Config::dba->hashed_process_sql("SELECT id, status FROM productionstatus order by status asc" );

	if ( defined $rows ) {
		foreach my $r ( @$rows ) {
			my $rmvspaceinstatus = $r->{status};
			$rmvspaceinstatus =~ s/\s/\_/g;
			$rmvspaceinstatus =~ s/\-/None/g;
			push ( @prodstatus, { id => "PROD_" . $r->{id}, status => $r->{status}, spid => $rmvspaceinstatus, name => $r->{id} } );
		}
	}
	
        return @prodstatus;
}

##############################################################################
#       db_getJobstatus
#      
##############################################################################
sub db_getJobstatus
{
	my $rows = $Config::dba->hashed_process_sql("SELECT id, status FROM JOBSTATUS ORDER BY id");
	return $rows ? @$rows : ();
}


##############################################################################
#       db_getQCStatuses
#      
##############################################################################
sub db_getQCStatuses
{
	my $rows = $Config::dba->hashed_process_sql("SELECT id, status FROM QCSTATUS ORDER BY status asc");
	return $rows ? @$rows : ();
}


##############################################################################
#       db_getClients
#      
##############################################################################
sub db_getClients
{
	my ($opcoid, $agencyFilter, $fromId, $projectFilter, $userid) = @_;
	
	return () if $opcoid && !looks_like_number($opcoid);
	
	my $filterClause = "";

	if(length($agencyFilter))			{ $filterClause	.= " AND t3.agency IN ('" . ($agencyFilter =~ s/,/','/rg) . "') ";					}
	if(length($projectFilter))			{ $filterClause	.= " AND t3.project IN ('" . ($projectFilter =~ s/,/','/rg) . "') ";					}

	$filterClause .= " AND t3p.rec_timestamp > DATE_ADD(now(), INTERVAL - $fromId DAY) " if length($fromId);
	$filterClause .= " AND ( c.opcoid = t3p.opcoid OR c.opcoid=t3p.location ) AND uc.userid = $userid ";
	my $rows = $Config::dba->hashed_process_sql("SELECT DISTINCT(t3.client) AS client FROM TRACKER3 t3
									INNER JOIN tracker3plus t3p ON t3p.job_number=t3.job_number
                                    INNER JOIN customers c ON c.id=t3p.customerid
                                    LEFT OUTER JOIN customeruser uc ON uc.customerid=c.id
									WHERE (t3p.opcoid=? OR t3p.location=?)
									$filterClause
									ORDER BY t3.client", [ $opcoid, $opcoid ]);
	my @clients;
	my $index = 0;
	foreach my $row (@$rows)
	{
		$index++;
		push(@clients, { id => "CLIENT_$index", client => $row->{client}, name => $row->{client} });
	}
	
	return @clients;
}

##############################################################################
#       db_getAgencies
#      
##############################################################################
sub db_getAgencies
{
	my ($opcoid) = @_;
	
	return () if $opcoid && !looks_like_number($opcoid);
	
	my $rows = $Config::dba->hashed_process_sql("SELECT DISTINCT(agency) AS agency FROM TRACKER3
									WHERE opcoid=?
									ORDER BY agency", [ $opcoid ]);
	
	return $rows ? @$rows : ();
}

##############################################################################
#       db_getAgencies2
#      
##############################################################################
sub db_getAgencies2
{
	my ($opcoid, $fromId, $userid,$flag, $clientFilter, $projectFilter) = @_;
	
	return () if $opcoid && !looks_like_number($opcoid);
	
	my $filterclause = "";
	$filterclause = " AND t3p.rec_timestamp > DATE_ADD(now(), INTERVAL - $fromId DAY) " if length($fromId);
	#$filterclause .= " AND ( c.opcoid = t3p.opcoid OR c.opcoid=t3p.location ) AND uc.userid = $userid ";

	if(length($clientFilter))			{ $filterclause	.= " AND t3.client IN ('" . ($clientFilter =~ s/,/','/rg) . "') ";					}
	if(length($projectFilter))			{ $filterclause	.= " AND t3.project IN ('" . ($projectFilter =~ s/,/','/rg) . "') ";					}

	my $usercondition = "";
     	if($flag eq "")
       	{
          $usercondition = "cu.userid='$userid' AND";
       	}

	##Getting the list of customers related to that opcompany
	##Out of that filtering the customers based on userid
	##finally filtering based on userid against customer id in usercustomer table towards customer id in customer table
	##QUERY UPDATED AS PER DASHBOARD VIEW QUERY TO GET THE LIST AS PER VIEW
	my $rows = $Config::dba->hashed_process_sql(
			"SELECT DISTINCT t3.agency as agency FROM TRACKER3 t3 " .
			"	INNER JOIN tracker3plus t3p ON t3p.JOB_NUMBER = t3.job_number " . 
			#"	INNER JOIN customers c ON c.id=t3p.customerid " .
			#"	LEFT OUTER JOIN customeruser uc ON uc.customerid=c.id " .
			" WHERE   (t3p.LOCATION = ? or t3p.OPCOID = ?) " .
			"       AND t3p.customerid IN (SELECT id FROM customers c1 WHERE c1.customer IN " .
			"       (SELECT DISTINCT c2.customer FROM customeruser cu INNER JOIN customers c2 ON c2.id = cu.customerid WHERE ".$usercondition." c2.opcoid=?) ) " .
			$filterclause .
			" GROUP BY t3.agency ORDER BY t3.agency", [ $opcoid, $opcoid, $opcoid ]);
										
	my $index = 0;
	my @agencies;
	foreach my $row (@$rows)
	{
		$index++;
		push(@agencies, { id => "CUST_$index", agency => $row->{agency}, name => $row->{agency} });
	}
	
	return @agencies;
}

# Get distinct set of agency, client and project names
#
# The values are taken from tracker3, using the same date constraint (fromId days) as is used in db_getJobs)
#
# Returns a list of 3 array references, for agencies, clients and projects
# Each array, is an array of anon hash, with keys id, name & (one of agency, client or project)
# 
sub db_getDistinctAgencyClientProject {

	my ( $opcoid, $fromId, $userid, $agencyFilter, $clientFilter, $projectFilter, $mediatypeFilter, $archantChk, $opcompanyFilter, $worktypeFilter, $formatsizeFilter,$newpublicationFilter,$newLocationFilter, $sidemenu) = @_;

	return ( undef, undef, undef, undef, undef, undef, undef, undef, undef ) if ( ! defined $opcoid ) || !looks_like_number( $opcoid );

	my @inner_where;
	my @outer_where;

	# For jobs in the Watchlist, the user should be able to see them whenever, so no point applying the customer viz check
	# and arguably, should not apply the date range either, although left that in for the time being and the range forced to 10 years
	# as perveresly, have the check is still faster than leaving it out, needs review esp if we migrate to MySQL 5.7

	if($sidemenu eq "SidepanelWatchJob")
	{
		push( @inner_where, " t3p.job_number IN (SELECT job_number FROM jobnotification jn WHERE jn.userid=$userid)" );
		$fromId = 3600;
	}
	else
	{
		push( @inner_where, "( (t3p.LOCATION=$opcoid) OR (t3p.OPCOID=$opcoid) )" );

		# Add the customer match to outer where. for straight filter but not for Watchlist (& arguably not MyJobs either)
		push( @outer_where, 
			"t3p.customerid IN (SELECT id FROM customers c1 " .
				" WHERE c1.customer IN  (SELECT DISTINCT c2.customer FROM customeruser cu INNER JOIN customers c2 ON c2.id = cu.customerid " .
					" WHERE (cu.userid=$userid) AND (c2.opcoid = $opcoid) ) )" );
	}

	push( @inner_where,  "( t3p.rec_timestamp > DATE_ADD(now(), INTERVAL -$fromId DAY) )" ) if length( $fromId );

	my $t0 = time;

	my $sql =  "SELECT DISTINCT t3.agency AS agency, t3.client AS client, t3.project AS project, " . ($archantChk ? 't3.workflow' : 't3.formatsize') . " as mediatype, ";
	$sql .=  "oc.opconame as opconame, oc2.opconame AS location, t3.publication as publication, t3.worktype as worktype, trim(t3.formatsize) as formatsize ";
	$sql .= " FROM  (SELECT job_number, customerid, opcoid, location FROM TRACKER3PLUS t3p WHERE  " . join( " AND ", @inner_where) . ") AS t3p ";
	$sql .= " INNER JOIN operatingcompany oc ON oc.opcoid=t3p.opcoid ";
	$sql .= " INNER JOIN operatingcompany oc2 ON oc2.opcoid=t3p.location ";
	$sql .= " INNER JOIN tracker3 t3 ON t3p.JOB_NUMBER = t3.job_number ";
	$sql .= " WHERE " . join( " AND ", @outer_where ) if ( scalar @outer_where );

	my $rows = $Config::dba->hashed_process_sql($sql);

	my $t_elapsed = time - $t0;
	my $count = 0;
	$count = scalar @$rows if ( defined $rows );

	warn "db_getDistinctAgencyClientProject: return $count rows, elapsed $t_elapsed";

	my %agencies_hash;
	my %clients_hash;
	my %projects_hash;
	my %mediatypes_hash;
	my %opcompany_hash;
	my %worktype_hash;
	my %formatsize_hash;
	my %publication_hash;
	my %location_hash;

	foreach my $row ( @$rows ) {
		$agencies_hash{ $row->{agency} } = 1 if $row->{agency};
		$clients_hash{ $row->{client} } = 1 if $row->{client};
		$projects_hash{ $row->{project} } = 1 if $row->{project};
		$mediatypes_hash{ $row->{mediatype} } = 1 if $row->{mediatype};
		$opcompany_hash { $row->{opconame} } = 1 if $row->{opconame};
		$worktype_hash { $row->{worktype} } = 1 if $row->{worktype};
		$formatsize_hash { $row->{formatsize} } = 1 if $row->{formatsize};
		$publication_hash { $row->{publication} } = 1 if $row->{publication};
		$location_hash { $row->{location} } = 1 if $row->{location};
	}

	my $index = 0;
	my @agencies;
	my @clients;
	my @projects;
	my @mediatypes;
	my @opcompany;
	my @worktype;
	my @formatsize;
	my @publication;
	my @location;

	$index = 0;
	foreach my $agency ( sort keys %agencies_hash ) {
		$index++;
		push(@agencies, { id => "CUST_$index", agency => $agency, name => $agency });
	}

	$index = 0;
	foreach my $client ( sort keys %clients_hash ) {
		$index++;
		push(@clients, { id => "CLIENT_$index", client => $client, name => $client });
	}

	$index = 0;
	foreach my $project ( sort keys %projects_hash ) {
		$index++;
		push(@projects, { id => "PROJECT_$index", project => $project, name => $project });
	}

	$index = 0;
	foreach my $mediatype ( sort keys %mediatypes_hash ) {
		$index++;
		push(@mediatypes, { id => "MEDIATYPE_$index", mediatype => $mediatype, name => $mediatype });
	}

	$index = 0;
	foreach my $opcompany ( sort keys %opcompany_hash ) {
		$index++;
		push(@opcompany, { id => "OPCOMP_$index", opcompany => $opcompany, name => $opcompany });
	}

	$index = 0;
	foreach my $worktype ( sort keys %worktype_hash ) {
		$index++;
		push(@worktype, { id => "WORKTYPE_$index", worktype => $worktype, name => $worktype });
	}

	$index = 0;
    foreach my $formatsize ( sort keys %formatsize_hash ) {
        $index++;
        push(@formatsize, { id => "FORMATSIZE_$index", formatsize => $formatsize, name => $formatsize });
    }

	$index = 0;
	foreach my $publication ( sort keys %publication_hash ) {
		$index++;
		push(@publication, { id => "PUBLICATION_$index", publication => $publication, name => $publication });
	}

	$index = 0;
	foreach my $location ( sort keys %location_hash ) {
		$index++;
		push(@location, { id => "LOCATION_$index", location => $location, name => $location });
	}

	return ( \@agencies, \@clients, \@projects, \@mediatypes, \@opcompany, \@worktype,\@formatsize, \@publication, \@location);

}

sub db_getCustomerClients {
	my ( $customer_id ) = @_;

	my @clients;
	eval {
		die "Invalid customer id" unless ( defined $customer_id ) && looks_like_number( $customer_id );
 
		my $sql = "SELECT DISTINCT t3.client AS client FROM TRACKER3 t3 " .
			" INNER JOIN TRACKER3PLUS t3p ON t3p.job_number=t3.job_number " .
			" WHERE t3p.customerid = ? "; 

		my $rows = $Config::dba->hashed_process_sql( $sql, [ $customer_id ] );
		foreach my $row ( @$rows ) {
			push( @clients, $row->{client} );
		}
	};
	if ( $@ ) {
		my $error = $@;
		$customer_id = 0 unless defined $customer_id;
		die "db_getCustomerClients: cust $customer_id, $error";
	}
	return \@clients;
}

sub db_getCustomerClientProjects {
	my ( $customer_id, $client ) = @_;

	my @projects;
	eval {
		die "Invalid customer id" unless ( defined $customer_id ) && looks_like_number( $customer_id );
 		die "Invalid client" unless defined $client;

		my $sql = "SELECT DISTINCT t3.project AS project FROM TRACKER3 t3 " .
			" INNER JOIN TRACKER3PLUS t3p ON t3p.job_number=t3.job_number " .
			" WHERE t3p.customerid = ? AND t3.client = ? "; 

		my $rows = $Config::dba->hashed_process_sql( $sql, [ $customer_id, $client ] );
		foreach my $row ( @$rows ) {
			push( @projects, $row->{project} );
		}
	};
	if ( $@ ) {
		my $error = $@;
		$customer_id = 0 unless defined $customer_id;
		$client = '' unless defined $client;
		die "db_getCustomerClientProjects: cust $customer_id, client $client, $error";
	}
	return \@projects;
}

sub db_getJobsInCampaign {
	my ( $customer_id, $client, $project ) = @_;

	my @jobs;
	eval {
		die "Invalid customer id" unless ( defined $customer_id ) && looks_like_number( $customer_id );
 		die "Invalid client" unless defined $client;

		my $sql = "SELECT t3.job_number FROM TRACKER3 t3 " .
			" INNER JOIN TRACKER3PLUS t3p ON t3p.job_number=t3.job_number " .
			" WHERE t3p.customerid = ? AND t3.client = ? AND t3.project = ? ORDER BY job_number"; 

		my $rows = $Config::dba->hashed_process_sql( $sql, [ $customer_id, $client, $project ] );
		warn "db_getJobsInCampaign: " . Dumper( $rows );

		foreach my $row ( @$rows ) {
			push( @jobs, $row->{job_number} );
		}
	};
	if ( $@ ) {
		my $error = $@;
		$customer_id = 0 unless defined $customer_id;
		$client = '' unless defined $client;
		$project = '' unless defined $project;
		die "db_getJobsInCampaign: cust $customer_id, client $client, project $project, $error";
	}
	return \@jobs;
}

sub db_getCustomerCampaigns {
	my ( $customer_id ) = @_;

	return() if $customer_id && !looks_like_number($customer_id);

	# The extra join with customers is to eliminate jobs where t3.agency doesnt agree with t3p.customerid
	# which happens a lot on staging but also on live.

	my $sql = "SELECT t3.job_number AS jobnumber, t3.agency AS agency, t3.client AS client, t3.project AS project " .
		" FROM TRACKER3 t3 INNER JOIN TRACKER3PLUS t3p ON t3p.job_number=t3.job_number INNER JOIN CUSTOMERS c ON c.id=t3p.customerid  " .
                " WHERE t3p.customerid=? AND t3.agency = c.customer AND t3.opcoid = c.opcoid"; 
	# TODO add ORDER BY

	my $rows = $Config::dba->hashed_process_sql( $sql, [ $customer_id ] );

	my @campaigns;
	foreach my $row ( @$rows ) {
		push( @campaigns, { jobnumber => $row->{jobnumber}, agency => $row->{agency}, client => $row->{client}, project => $row->{project}  });
	}
	
	return @campaigns;
}


##############################################################################
#       job path
#      
##############################################################################
sub db_jobPath
{
	my ($jobid) = @_;
	
	return "" if $jobid && !looks_like_number($jobid);
	
	my ($rootpath, $letter, $agency, $client, $project, $headline, $publication, $workflow, $opcoid, $pathtype)
			 = $Config::dba->process_oneline_sql("SELECT t.rootpath, t.letter, t.agency, t.client, t.project, t.headline,
										t.publication, t.workflow, t.opcoid, o.pathtype FROM TRACKER3 t
									INNER JOIN OPERATINGCOMPANY o ON o.opcoid=t.opcoid
									WHERE t.job_number=?", [ $jobid ]);
	
	## HACK FOR ZURICH								
	$rootpath = "/gpfs/STUDIOS/TAG_ZURICH_NAC/PRODUCTION/" if $opcoid eq "10722";
	
	my $path = $rootpath;
	if($pathtype == 1 && length($letter))	{ $path .= "$letter/$agency/$client/$project/$headline/"; }
	elsif($pathtype == 1)					{ $path .= "$agency/$client/$project/$headline/"; }
	elsif($pathtype == 3)					{ $path .= "$agency/$client/$project/$headline/$workflow/"; }
	
	$path .= $jobid . "_";
	$path .= $publication if length($publication) && (lc($publication) ne "null");
	
	return $path;
}


##############################################################################
#       open folder path
#      
##############################################################################
sub db_openFolderPath
{
	my ($jobid) = @_;
	
	my $joblocation = db_getJobLocation($jobid);
	my $current_opcoid = $joblocation->{current_opcoid};
	
	my ($wippath, $domainname, $mountpath) = $Config::dba->process_oneline_sql("SELECT wippath, domainname, mountpath FROM OPERATINGCOMPANY
																	WHERE opcoid=?", [ $current_opcoid ]);
	my ($rootpath, $letter, $agency, $client, $project, $headline, $publication, $workflow, $opcoid, $pathtype, $opconame)
			 = $Config::dba->process_oneline_sql("SELECT t.rootpath, t.letter, t.agency, t.client, t.project, t.headline,
										t.publication, t.workflow, t.opcoid, o.pathtype, o.opconame FROM TRACKER3 t
									INNER JOIN OPERATINGCOMPANY o ON o.opcoid=t.opcoid
									WHERE t.job_number=?", [ $jobid ]);
	
	
	my $path = "";
	if($pathtype == 1 && length($letter))	{ $path = "$letter/$agency/$client/$project/$headline/"; }
	elsif($pathtype == 1)					{ $path = "$agency/$client/$project/$headline/"; }
	elsif($pathtype == 3)					{ $path = "$agency/$client/$project/$headline/$workflow/"; }
	
	$path .= $jobid . "_";
	$path .= $publication if length($publication) && (lc($publication) ne "null");
	
	
	
	my $mount = "//$domainname$mountpath";
	$mount .= "/$opconame" if ($current_opcoid != $opcoid) && ($current_opcoid !~ m/(261|11463)$/);
	
	return "$mount/$path";
}


##############################################################################
#       job short path (for Repository)
#      
##############################################################################
sub db_jobShortPath
{
	my ($jobid, $shard) = @_;
	
	return "" if $jobid && !looks_like_number($jobid);
	
	my ($rootpath, $letter, $agency, $client, $project, $headline, $publication, $workflow, $opcoid, $pathtype)
			 = $Config::dba->process_oneline_sql("SELECT t.rootpath, t.letter, t.agency, t.client, t.project, t.headline, t.publication, t.workflow, t.opcoid, o.pathtype FROM TRACKER3 t
									INNER JOIN OPERATINGCOMPANY o ON o.opcoid=t.opcoid
									WHERE t.job_number=?", [ $jobid ]);
	
	
	my $path = "/";
	if($pathtype == 1 && length($letter))	{ $path .= "$letter/$agency/$client/$project/$headline/"; }
	elsif($pathtype == 1)					{ $path .= "$agency/$client/$project/$headline/"; }
	elsif($pathtype == 3)					{ $path .= "$agency/$client/$project/$headline/$workflow/"; }
	
	$path .= $jobid . "_";
	$path .= $publication if length($publication) && (lc($publication) ne "null");
	
	return $path;
}

##############################################################################
#       db_getLockedJobs
#      
##############################################################################
sub db_getLockedJobs
{
	my $rows = $Config::dba->hashed_process_sql("SELECT job_number AS jobid, preflightstatus AS status, locked_time AS lockedtime FROM TRACKER3PLUS
									WHERE LOCKED = 1
										 AND ((preflightstatus IN (2,4) AND locked_time < DATE_SUB(now(), INTERVAL 30 MINUTE))
									    		  OR (preflightstatus = 3   AND locked_time < DATE_SUB(now(), INTERVAL 24 HOUR))  )");
	
	return $rows ? @$rows : ();
}


##############################################################################
#       db_getOperatingCompanyFromJob
#      
##############################################################################
sub db_getOperatingCompanyFromJob
{
	my ($jobid) = @_;
	
	return "" if $jobid && !looks_like_number($jobid);
	
	my ($opcoid) = $Config::dba->process_oneline_sql("SELECT opcoid FROM TRACKER3 WHERE job_number=?", [ $jobid ]);
	return $opcoid;
}
#############################################################################
# Get filename for a specific Job version
# Nb this uses the job number and id field, rather than version
#
##############################################################################

sub db_getJobVersionFilename {

        my ( $job, $version ) = @_;

	die "Invalid job number" unless ( ( defined $job ) && ( looks_like_number( $job ) ) );
	die "Invalid version number" unless ( ( defined $version ) && ( looks_like_number( $version ) ) );

        my ( $filename ) = $Config::dba->process_oneline_sql( "SELECT filename FROM jobversions WHERE job_number = ? AND id = ?", [ $job, $version ], 1 );

        return $filename;
}

#############################################################################
#       setLocation()
#
##############################################################################
sub setLocation
{
	my ($jobid, $shard, $location) = @_;
	
	
	
	
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $location && !looks_like_number($location);
	
	## Update the shard's location if we're provided one
	if($shard)
	{
		my ($exists) = $Config::dba->process_oneline_sql("SELECT 1 FROM SHARDS WHERE job_number=? AND shard_name=?", [ $jobid, $shard ]);
		$Config::dba->process_sql("UPDATE SHARDS SET location=? WHERE job_number=? AND shard_name=?", [ $location, $jobid, $shard ]) if $exists;
		$Config::dba->process_sql("INSERT INTO SHARDS (location, job_number, shard_name) VALUES (?, ?, ?)", [ $location, $jobid, $shard ]) if !$exists;
		
		## Don't update the job location if we're in multiple locations
		my ($displaced) = $Config::dba->process_oneline_sql("SELECT COUNT(DISTINCT(location)) FROM shards WHERE job_number=?", [ $jobid ]);
		return if $displaced > 1;
	}
	
	$Config::dba->process_sql("UPDATE TRACKER3PLUS SET location=? WHERE job_number=?", [ $location, $jobid ]);
}

##############################################################################
#       setCheckoutStatus - set the current checkout status?
##############################################################################
sub setCheckoutStatus
{
	my ($jobid, $shard, $statusid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $statusid && !looks_like_number($statusid);
	
	if(!$shard)
	{
		$Config::dba->process_sql("UPDATE TRACKER3PLUS SET checkoutstatus=? WHERE job_number=?", [ $statusid, $jobid ]);
	}
	else
	{
		$Config::dba->process_sql("UPDATE shards SET checkoutstatus=? WHERE job_number=? AND shard_name=?", [ $statusid, $jobid, $shard ]);
	}
}


##############################################################################
#       enQueueOutsource - put this job on the outsource queue to be sent
##############################################################################
sub enQueueOutsource
{
	my ($jobid, $opcoid, $userid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $opcoid && !looks_like_number($opcoid);
	
$Config::dba->process_sql("INSERT INTO OUTSOURCEQUEUE (job_number, opcoid, userid, rec_timestamp) VALUES (?, ?, ?, now())", [ $jobid, $opcoid, $userid ]);
}


##############################################################################
#       enQueueRepoCheckIn - put this job on the repo queue to be checkedin
##############################################################################
sub enQueueRepoCheckIn
{
	my ($jobid, $shard, $opcoid, $userid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $opcoid && !looks_like_number($opcoid);
	return 0 if $opcoid == 15 || $opcoid == 16;
	return 0 if isJobOnCheckInQueue($jobid) == 1;
	
$Config::dba->process_sql("INSERT INTO REPOCHECKINQUEUE (job_number, shard_name, opcoid, userid, checkingin, rec_timestamp, priority)
					VALUES (?, ?, ?, ?, 0, now(), 0)", [ $jobid, $shard || "", $opcoid, $userid ]);
$Config::dba->process_sql("INSERT INTO REPOCHECKINQUEUEHISTORY (job_number, shard_name, opcoid, userid, rec_timestamp)
					VALUES (?, ?, ?, ?, now())", [ $jobid, $shard || "", $opcoid, $userid ]);
}


##############################################################################
#       enQueueRepoCheckOut - put this job on the repo queue to be checked OUT
##############################################################################
sub enQueueRepoCheckOut
{
	my ($jobid, $shard, $opcoid, $userid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $opcoid && !looks_like_number($opcoid);
	return 0 if $opcoid == 15 || $opcoid == 16;
	
$Config::dba->process_sql("INSERT INTO REPOCHECKOUTQUEUE (job_number, shard_name, opcoid, userid, checkingout, rec_timestamp)
					VALUES (?, ?, ?, ?, 0, now())", [ $jobid, $shard || "", $opcoid, $userid ]);
$Config::dba->process_sql("INSERT INTO REPOCHECKOUTQUEUEHISTORY (job_number, shard_name, opcoid, userid, rec_timestamp)
					VALUES (?, ?, ?, ?, now())", [ $jobid, $shard || "", $opcoid, $userid ]);
}


##############################################################################
#       enQueueRepoSendJob - put this job on the repo Send Jobs queue to be 
#	sent to a destination opcoid
##############################################################################
sub enQueueRepoSendJob
{
	my ($jobid, $shard, $location, $opcoid, $userid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $opcoid && !looks_like_number($opcoid);
	return 0 if $opcoid == 15 || $opcoid == 16;
	return 0 if isJobOnRepoSendJobsQueue($jobid, $shard);
	
$Config::dba->process_sql("INSERT INTO REPOSENDJOBSQUEUE (job_number, shard_name, status, fromopcoid, opcoid, userid, checkingin, checkedin, rec_timestamp)
					VALUES (?, ?, '', ?, ?, ?, 0, 0, now())", [ $jobid, $shard || "", $location, $opcoid, $userid ]);
}


#############################################################################
#       isJobOnRepoSendJobsQueue()
#
##############################################################################
sub isJobOnRepoSendJobsQueue
{
	my ($jobid, $shard) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($inqueue) = $Config::dba->process_oneline_sql("SELECT 1 FROM REPOSENDJOBSQUEUE WHERE job_number=? AND shard_name=?", [ $jobid, $shard || "" ]);
	
	return $inqueue || 0;
}


##############################################################################
#       enQueueRepoAutoArchive - put this job on the repo AutoArchive queue to be 
#	sent to Repository for Archive
# 	ECMDBJob::enQueueRepoAutoArchive( $job, $opcoid, $userid, 14 ); # 14 days ...
##############################################################################
sub enQueueRepoAutoArchive
{
	my ($jobid, $opcoid, $userid, $days) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $opcoid && !looks_like_number($opcoid);
	return 0 if $userid && !looks_like_number($userid);
	return 0 if $days && !looks_like_number($days);
	
$Config::dba->process_sql("INSERT INTO REPOAUTOARCHIVEQUEUE (job_number, opcoid, userid, rec_timestamp)
		VALUES (?, ?, ?, DATE_ADD(now(), INTERVAL $days DAY))", [ $jobid, $opcoid, $userid ]);
}

#############################################################################
#       isJobOnRepoAutoArchiveQueue()
#
##############################################################################
sub isJobOnRepoAutoArchiveQueue
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($inqueue) = $Config::dba->process_oneline_sql("SELECT 1 FROM REPOAUTOARCHIVEQUEUE WHERE job_number=?", [ $jobid ]);
	return $inqueue || 0;
}

#############################################################################
#       isJobOnCheckInQueue()
#
##############################################################################
sub isJobOnCheckInQueue
{
	my ($jobid, $shardname) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	my ($inqueue) = $Config::dba->process_oneline_sql("SELECT 1 FROM REPOCHECKINQUEUE WHERE job_number=? AND shard_name=?", [ $jobid, $shardname || "" ]);
	return $inqueue || 0;
}

#############################################################################
#       isJobOnCheckOutQueue()
#
##############################################################################
sub isJobOnCheckOutQueue
{
	my ($jobid, $shardname) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($inqueue) = $Config::dba->process_oneline_sql("SELECT 1 FROM REPOCHECKOUTQUEUE WHERE job_number=? AND shard_name=?", [ $jobid, $shardname || "" ]);
	return $inqueue || 0;
}

#############################################################################
#       isJobArchived()
#
##############################################################################
sub isJobArchived
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($isarchived) = $Config::dba->process_oneline_sql("SELECT 1 FROM TRACKER3PLUS WHERE job_number=? AND status=6", [ $jobid ]);
	return $isarchived || 0;
}

#############################################################################
#       db_updateAdgateStatus()
#
##############################################################################
sub db_updateAdgateStatus
{
	my ($jobid, $status) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $status && !looks_like_number($status);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET adgatestatus=? WHERE job_number=?", [ $status, $jobid ]);
}


##############################################################################
#       getSJobhards 
##############################################################################
sub db_getJobShards
{
	my ($jobid) = @_;
	
	return () if $jobid && !looks_like_number($jobid);
	
	
	my ($shardsenabled) = $Config::dba->process_oneline_sql("SELECT c.enablejobshards FROM TRACKER3PLUS t3p INNER JOIN customers c ON c.id=t3p.customerid WHERE t3p.job_number=?", [ $jobid ]);
	return () if !$shardsenabled;
	
	my $shards = $Config::dba->hashed_process_sql("SELECT shard_name FROM shards WHERE job_number=?", [ $jobid ]);
	return () if !$shards;
	
	my @s;
	foreach(@$shards) { push(@s, $_->{shard_name}); }
	return @s
}


##############################################################################
#       setJobShards 
##############################################################################
sub db_setJobShards
{
	my ($jobid, $shards, $studioid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);

	
	my ($shardsenabled, $joblocation) = $Config::dba->process_oneline_sql("SELECT c.enablejobshards, t3p.location FROM TRACKER3PLUS t3p INNER JOIN customers c ON c.id=t3p.customerid WHERE t3p.job_number=?", [ $jobid ]);
	return 0 if !$shardsenabled;
	
	foreach my $shard (split(/,/, $shards))
	{
		my ($exists) = $Config::dba->process_oneline_sql("SELECT 1 FROM shards WHERE job_number=? AND shard_name=?", [ $jobid, $shard ]);
		if(!$exists) { $Config::dba->process_sql("INSERT INTO shards (job_number, shard_name, location) VALUES (?, ?, ?)", [ $jobid, $shard, $studioid || $joblocation ]); }
		else			{ $Config::dba->process_sql("UPDATE shards SET location=? WHERE job_number=? AND shard_name=?", [ $studioid || $joblocation, $jobid, $shard ]); }
	}
	
	## Update the main job's location to the placeholder (17//MULTIPLE_LOCATIONS) so we know when shards are checked out to different locations
	## If it's not scattered, set it to the given ID, which you'd expect be correct if everything's in the same place and we *just* used it
	my ($scattered_shards) = $Config::dba->process_oneline_sql("SELECT COUNT(DISTINCT(location)) FROM shards WHERE job_number=?", [ $jobid ]);
	$Config::dba->process_sql("UPDATE tracker3plus SET location=? WHERE job_number=?", [ $scattered_shards > 1 ? 17 : ($studioid || $joblocation), $jobid ]);
	
	db_splitRepoJobQueues($jobid, $shards);
}


sub db_splitRepoJobQueues
{
	my ($jobid, $shardlist) = @_;
	my @shards = split(/,/, $shardlist);
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($shardsenabled) = $Config::dba->process_oneline_sql("SELECT c.enablejobshards FROM TRACKER3PLUS t3p INNER JOIN customers c ON c.id=t3p.customerid WHERE t3p.job_number=?", [ $jobid ]);
	return 0 if !$shardsenabled;
	
	
	## Replace checkout job with separate shards
		my $checkout_info = $Config::dba->hashed_process_sql("SELECT opcoid, userid, checkingout, rec_timestamp, priority FROM repocheckoutqueue WHERE job_number=? AND shard_name=''", [ $jobid ]);
		if($checkout_info)
		{
			my $coi = @{ $checkout_info }[0];
			my $placeholders = "";
			my @values = ();
			foreach my $shard (@shards)
			{
				push(@values, ($jobid, $shard, $coi->{opcoid}, $coi->{userid}, $coi->{checkingout}, $coi->{rec_timestamp}, $coi->{priority}) );
				$placeholders .= ($placeholders ? ', ' : '') . "(?, ?, ?, ?, ?, ?, ?)";
			}
		
			$Config::dba->process_sql("INSERT INTO repocheckoutqueue (job_number, shard_name, opcoid, userid, checkingout, rec_timestamp, priority) VALUES "
									. $placeholders, [ @values ]);
			$Config::dba->process_sql("DELETE FROM repocheckoutqueue WHERE job_number=? AND shard_name=''", [ $jobid ]);
		}
	
	
	## Replace checkin job with separate shards
		my $checkin_info = $Config::dba->hashed_process_sql("SELECT opcoid, userid, checkingin, rec_timestamp, priority FROM repocheckinqueue WHERE job_number=? AND shard_name=''", [ $jobid ]);
		if($checkin_info)
		{
			my $cii = @{ $checkin_info }[0];
			my $placeholders = "";
			my @values = ();
			foreach my $shard (@shards)
			{
				push(@values, ($jobid, $shard, $cii->{opcoid}, $cii->{userid}, $cii->{checkingin}, $cii->{rec_timestamp}, $cii->{priority}) );
				$placeholders .= ($placeholders ? ', ' : '') . "(?, ?, ?, ?, ?, ?, ?)";
			}
		
			$Config::dba->process_sql("INSERT INTO repocheckinqueue (job_number, shard_name, opcoid, userid, checkingin, rec_timestamp, priority) VALUES "
									. $placeholders, [ @values ]);
			$Config::dba->process_sql("DELETE FROM repocheckinqueue WHERE job_number=? AND shard_name=''", [ $jobid ]);
		}
	
	
	## Replace send job with separate shards
		my $send_info = $Config::dba->hashed_process_sql("SELECT fromopcoid, opcoid, userid, checkingin, checkedin, rec_timestamp, priority FROM reposendjobsqueue WHERE job_number=? AND shard_name=''", [ $jobid ]);
		if($send_info)
		{
			my $si = @{ $send_info }[0];
			my $placeholders = "";
			my @values = ();
			foreach my $shard (@shards)
			{
				push(@values, ($jobid, $shard, $si->{fromopcoid}, $si->{opcoid}, $si->{userid}, $si->{checkingin}, $si->{checkedin}, $si->{rec_timestamp}, $si->{priority}) );
				$placeholders .= ($placeholders ? ', ' : '') . "(?, ?, ?, ?, ?, ?, ?, ?, ?)";
			}
			
			$Config::dba->process_sql("INSERT INTO reposendjobsqueue (job_number, shard_name, fromopcoid, opcoid, userid, checkingin, checkedin, rec_timestamp, priority) VALUES "
									. $placeholders, [ @values ]);
			$Config::dba->process_sql("DELETE FROM reposendjobsqueue WHERE job_number=? AND shard_name=''", [ $jobid ]);
		}
}

##############################################################################
#       getCheckedoutStatus 
##############################################################################
sub getCheckedOutStatus
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	my ($checkedoutstatus) = $Config::dba->process_oneline_sql("SELECT checkoutstatus FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	return $checkedoutstatus;
}


#############################################################################
#       isJobOnOutsourceQueue()
#
##############################################################################
sub isJobOnOutsourceQueue
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($inqueue) = $Config::dba->process_oneline_sql("SELECT 1 FROM OUTSOURCEQUEUE WHERE job_number=?", [ $jobid ]);
	return $inqueue || 0;
}


#############################################################################
#       isCheckedOut()
#
##############################################################################
sub isCheckedOut
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($status) = $Config::dba->process_oneline_sql("SELECT checkoutstatus FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	return $status == 2 ? 1 : 0;
}


#############################################################################
#       isJobInTransit()
#
##############################################################################
sub isJobInTransit
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($status) = $Config::dba->process_oneline_sql("SELECT checkoutstatus FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	return ($status == 1 || $status == 3) ? 1 : 0;
}

#############################################################################
#       isCreative()
#
##############################################################################
sub isCreative
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($creative) = $Config::dba->process_oneline_sql("SELECT creative FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	return $creative;
}

##############################################################################
#       enQueueFtp - put this job on the ftp queue to be sent
#       nb assets is now a comma separted list of asset types eg "high_res,lr_asset"
##############################################################################
sub enQueueFtp
{
	my ( $jobid, $filename, $ftpid, $userid, $recipientlist, $message, $assets ) = @_;

	my $rc;

	eval {
		Validate::job_number( $jobid );
		die "Invalid ftpid" unless (defined $ftpid) && looks_like_number($ftpid);

		cluck "ECMDBJob::enQueueFtp: jobid '$jobid', filename '$filename', ftpid '$ftpid', userid '$userid', reciplist '$recipientlist', mess '$message', assets '$assets'";
		$Config::dba->process_sql(
			"INSERT INTO FTPQUEUE (job_number, filename, ftpid, userid, recipientlist, message, assets, rec_timestamp) VALUES (?, ?, ?, ?, ?, ?, ?, now() )",
			[ $jobid, $filename, $ftpid, $userid, $recipientlist, $message, $assets ]);
		$rc = 1;
	};
	if ( $@ ) {
		cluck "ECMDBJob::enQueueFtp: $@";
		$rc = 0;
	}
	cluck "ECMDBJob::enQueueFtp: rc $rc";
	return $rc;
}


#ECMDBJob::enQueueAdgate( $jobid, $userid, $publicationCountry, $publicationCode, $adspecSection, 
#	$adspecSizeDescription1, $adspecSizeDescription2, $adspecHeight, $adspecWidth, $adspecColor, $deliveryMethod);

##############################################################################
#       enQueueAdgate - put this job on the adgate queue to be sent
##############################################################################
sub enQueueAdgate
{
	my ($jobid, $userid, $accountname, $filename, $replacement, $publicationcountry, $publicationcode,
		$publicationname, $adspecsection, $adspecsizedesc1, $adspecsizedesc2, $adspecheight, $adspecwidth,
		$adspeccolour, $deliverymethod, $comments, $insertdate) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("INSERT INTO ADGATEQUEUE (job_number, userid, accountname, filename, replacement, publicationcountry, publicationcode,
											publicationname, adspecsection, adspecsizedescription1, adspecsizedescription2, adspecheight,
											adspecwidth, adspeccolor, deliverymethod, comments, insertiondate, rec_timestamp)
									VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now())",
									[ $jobid, $userid, $accountname, $filename, $replacement, $publicationcountry, $publicationcode,
									$publicationname, $adspecsection, $adspecsizedesc1, $adspecsizedesc2, $adspecheight,
									$adspecwidth, $adspeccolour, $deliverymethod, $comments, $insertdate ]);
									
	return 1;
}

#############################################################################
#       unlockJob()
#		- we set the PREFLIGHT STATUS TO 'NONE' and also the QC Status to '-'
# 		- REGARDLESS OF WHAT STATUS THE JOB is in! This is like a 'hard reset'
##############################################################################
sub unlockJob
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET locked=?, preflightstatus=?, qcstatus=?, assignee=?, operatorstatus=?, DEPARTMENTVALUE=?, TEAMVALUE=?
					WHERE job_number=?", [ 0, 5, 0, 0, 'U',0,0, $jobid ]);	
$Config::dba->process_sql("DELETE FROM ASSIGNMENTHISTORY WHERE job_number=? AND status IN ('A', 'W')", [ $jobid ]);
}

#############################################################################
#       db_setTwistServer()
#
##############################################################################
sub db_setTwistServer
{
	my ($jobid, $twistserver) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET twist=? WHERE job_number=?", [ $twistserver, $jobid ]);
}

#############################################################################
#       db_setEstimate()
#
#############################################################################
sub db_setEstimate
{
	my ($jobid, $estimate) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET estimate=? WHERE job_number=?", [ $estimate, $jobid ]);
}



#############################################################################
#       db_getActivity()
#
##############################################################################
sub db_getActivity
{
	my $rows = $Config::dba->hashed_process_sql("SELECT t3p.job_number AS jobid, t3p.lastuser, t3p.mod_timestamp AS modified, IF(t3p.locked = 1, 'Yes', 'No') AS locked, t3p.locked_time AS lockedtime,
											oc.opconame, u.username, pfs.status AS preflightstatus FROM TRACKER3PLUS t3p USE INDEX (MOD_TIMESTAMP)
										LEFT OUTER JOIN USERS u ON t3p.lastuser=u.id
										INNER JOIN TRACKER3 t ON t.job_number=t3p.job_number
										INNER JOIN OPERATINGCOMPANY oc ON oc.opcoid=t.opcoid
										INNER JOIN PREFLIGHTSTATUS pfs ON pfs.id=t3p.preflightstatus
										ORDER BY t3p.mod_timestamp DESC
										LIMIT 100");
	
	foreach (@$rows) { $_->{assetPath} = db_jobPath($_->{jobid}); }
	
	return $rows ? @$rows : ();
}


##############################################################################
#       db_requestCompare
#
##############################################################################
sub db_requestCompare
{
	my ($jobid, $request) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET comparerequested=? WHERE job_number=?", [ $request, $jobid ]);
}


##############################################################################
#       sendToTwist
#
##############################################################################
sub db_sendToTwist
{
	my ($jobid, $request) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET sendtotwist=? WHERE job_number=?", [ $request, $jobid ]);
}


#############################################################################
#       db_getJobFields()
#
##############################################################################
sub db_getJobFields
{
	my ($jobid) = @_;
	
	return 0 if !defined $jobid || !looks_like_number($jobid);
	
	my $rows = $Config::dba->hashed_process_sql("SELECT agency, client, project, letter, publication, size, format, jpeg, cmyk, dpi, proofing, icc_curve, crop, path, apdf, ucr,
			headline, lowrespath, releasepath, workflow, szpercent, proofcurve, formatsize, szwidth, szheight,
			bleedt, bleedb, bleedl, bleedr, safetyt, safetyb, safetyl, safetyr, trim, matdate, coverdate, postdate,
			opcoid, pnums, agencyref, gmg, approvalstatus, szwidthdps, szheightdps,
			fprelname, pubcollect, collectproof, rootpath, pathtype, version, businessunitref AS businessref, colour
			FROM TRACKER3 WHERE job_number=?", [ $jobid ]);
	
	return $rows ? @$rows : ();		## THE ORIGINAL FUNCTION PUT THE ONE ROW INTO AN ARRAY, SO WHATEVER
}


##############################################################################
#       assignJob()
#
##############################################################################
sub assignJob
{
	my ($jobid, $assigneeid, $timetoadd, $assignerid, $ispublishing, $lineitemid, $isassigned, $productid, $productstatus) = @_;
	my $status = 'A';
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $assigneeid && !looks_like_number($assigneeid);
	return 0 if $timetoadd && !looks_like_number($timetoadd);
	return 0 if $assignerid && !looks_like_number($assignerid);
	$lineitemid = 0 if( (! defined $lineitemid) || ($lineitemid eq "null") || ($lineitemid eq ""));


	$Config::dba->process_sql("UPDATE TRACKER3PLUS SET assignee=?, operatorstatus=?" . ($ispublishing == 0 ? ", duedate = DATE_ADD(now(), INTERVAL $timetoadd MINUTE)" : "") .
					" WHERE job_number=?", [ $assigneeid, $status, $jobid ]);

	$Config::dba->process_sql("INSERT INTO ASSIGNMENTHISTORY (status, job_number, assigneeid, assignerid, assigned_timestamp, due_timestamp, LINEITEMID)
                                       VALUES (?, ?, ?, ?, now(), DATE_ADD(now(), INTERVAL $timetoadd MINUTE), ?)", [ $status, $jobid, $assigneeid, $assignerid, $lineitemid ]);
	# check the job has single assigned product and status not in "Paused"
	if ( (defined $isassigned) && (defined $productstatus) && ( $isassigned == 1) && ($productstatus!=3) )
	{
		$Config::dba->process_sql("UPDATE products SET userid=$assigneeid WHERE job_number=? AND id=?",[$jobid,$productid]);
	}

}

##############################################################################
#       reassignJob()
#		# if it IS assigned to a user then REASSIGN it to NEW user 
# 		(even if it is the same user) - ignore jobs that are being worked on
##############################################################################
sub reassignJob
{
	my ($jobid, $assigneeid, $timetoadd, $assignerid, $ispublishing, $isassigned, $productid, $productstatus) = @_;
	my $status = 'A';
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $assigneeid && !looks_like_number($assigneeid);
	return 0 if $timetoadd && !looks_like_number($timetoadd);
	return 0 if $assignerid && !looks_like_number($assignerid);
	
	$Config::dba->process_sql("UPDATE TRACKER3PLUS SET assignee=?, operatorstatus=?" . ($ispublishing == 1 ? ", duedate = DATE_ADD(now(), INTERVAL $timetoadd MINUTE)" : "") .
					" WHERE job_number=?", [ $assigneeid, $status, $jobid ]);
	
	$Config::dba->process_sql("UPDATE assignmenthistory SET assigneeid=?, assignerid=?, due_timestamp=DATE_ADD(now(), INTERVAL $timetoadd HOUR)
					WHERE job_number=? AND status='A'", [ $assigneeid, $assignerid, $jobid ]);
	# check the job has single assigned product and status not in "Paused"
	if($isassigned == 1 && $productstatus!=3)
	{
		$Config::dba->process_sql("UPDATE products SET userid=$assigneeid WHERE job_number=? AND id=?",[$jobid,$productid]);
	}
}


##############################################################################
#       unWorkJob()
#		# set a Job from being Worked on to Simply Assigned for a user
#	This is used for when am Operator 'Takes' a new job.
#	The current job being worked on is set back to Assign for him 
##############################################################################
sub unWorkJob
{
	my ($jobid, $assigneeid) = @_;
	my $status = 'A';
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $assigneeid && !looks_like_number($assigneeid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET operatorstatus=? WHERE job_number=? AND assignee=? AND operatorstatus='W'",
					[ $status, $jobid, $assigneeid ]);
$Config::dba->process_sql("UPDATE ASSIGNMENTHISTORY SET status=? WHERE job_number=? AND assigneeid=? AND status='W'",
					[ $status, $jobid, $assigneeid ]);
}

##############################################################################
#       unassignJob()
#		# remove the assignee from a Job (e.g. when QC Approved)
# 
##############################################################################
sub unassignJob
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET assignee=0 WHERE job_number=?", [ $jobid ]);
}


#############################################################################
#       getCurrentOperatorJob()
#		return the current job that the operator is working on (or ZERO)
##############################################################################
sub getCurrentOperatorJob
{
	my ($userid) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	
	my ($jobid) = $Config::dba->process_oneline_sql("SELECT job_number FROM ASSIGNMENTHISTORY WHERE assigneeid=? AND status='W' LIMIT 1", [ $userid ]);
	return $jobid || 0;
}


#############################################################################
#       getAssigneeId()
#		Get the id of the operator assigned to this Job
##############################################################################
sub getAssigneeId
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($userid) = $Config::dba->process_oneline_sql("SELECT assignee FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	return $userid || 0;
}


#############################################################################
#       inProgress()
#		Is this job in Progress? (being preflighted?)
##############################################################################
sub inProgress
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($status) = $Config::dba->process_oneline_sql("SELECT preflightstatus FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
	return $status == 1 ? 1 : 0;
}


##############################################################################
#       setCurrentOperatorJob()
#		set the current job that the operator is working on (or ZERO)
##############################################################################
sub setCurrentOperatorJob
{
	my ($userid, $jobid, $lineitemid) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	return 0 if $jobid && !looks_like_number($jobid);

	$lineitemid = 0 if ( ( ! defined $lineitemid ) || ( $lineitemid eq "null" ) || ( $lineitemid eq "" ) );
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET operatorstatus='W', assignee=? WHERE job_number=?", [ $userid, $jobid ]);

if($lineitemid == 0){
	$Config::dba->process_sql("UPDATE ASSIGNMENTHISTORY SET status='W', requested_timestamp=now() WHERE job_number=? AND status in ('A', 'R') AND assigneeid=?",
					[ $jobid, $userid ]);
}else{
	$Config::dba->process_sql("UPDATE ASSIGNMENTHISTORY SET status='W', requested_timestamp=now() WHERE job_number=? AND status in ('A', 'R') AND assigneeid=? AND LINEITEMID = ?",
                    [ $jobid, $userid, $lineitemid ]);
}
	
	return 1;
}

##############################################################################
#       setOperatorJobProcessing()
#		mark the job as 'File Uploaded (Processing)' in the Job and Assignment History	
##############################################################################
sub setOperatorJobProcessing
{
	my ($userid, $jobid) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET operatorstatus='P' WHERE job_number=?", [ $jobid ]);
$Config::dba->process_sql("UPDATE ASSIGNMENTHISTORY SET status='P' WHERE job_number=? AND status='W' AND assigneeid=?", [ $jobid, $userid ]);
	return 1;
}

##############################################################################
#       rejectJob()
#	operator rejects the job
##############################################################################
sub rejectJob
{
	my ($jobid, $userid, $commentid, $rejectionreason) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET operatorstatus='R' WHERE job_number=?", [ $jobid ]);
$Config::dba->process_sql("UPDATE ASSIGNMENTHISTORY SET status='R', rejected_timestamp=now() WHERE job_number=? AND status='W'", [ $jobid ]);
$Config::dba->process_sql("INSERT INTO JOBNOTES (commentid, job_number, notetype, userid, notetext, rec_timestamp)
					VALUES (?, ?, 1, ?, ?, now())", [ $commentid, $jobid, $userid, $rejectionreason ]);
	return 1;
}


##############################################################################
#       completeJob()
#	operator complete the job
##############################################################################
sub completeJob
{
	my ($jobid, $userid, $lineitemid) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	return 0 if $jobid && !looks_like_number($jobid);

	$lineitemid = 0 if ( ( ! defined $lineitemid ) || ( $lineitemid eq "null" ) || ( $lineitemid eq "" ) );

	my ($count)	=	 $Config::dba->process_oneline_sql("SELECT count(*) FROM products WHERE job_number=? AND userid > 0", [ $jobid ]);
	#Remove assignee when all the products are completed
	my $condition	=	"";
	if($count == 0) {
		$condition	=	",assignee=0";
	}

	$Config::dba->process_sql("UPDATE TRACKER3PLUS SET operatorstatus='C' $condition  WHERE job_number=?", [ $jobid ]);

	if ( $lineitemid == 0 ) {
		$Config::dba->process_sql("UPDATE ASSIGNMENTHISTORY SET status='C', submittedqc_timestamp=now() WHERE job_number=? AND status in ('W','A') AND assigneeid=?",
				[ $jobid, $userid ]);
	} else {
		$Config::dba->process_sql("UPDATE ASSIGNMENTHISTORY SET status='C', submittedqc_timestamp=now() WHERE job_number=? AND status in ('W','A') AND assigneeid=? AND LINEITEMID = ?",
                		[ $jobid, $userid, $lineitemid ]);
	}
	return 1;
}

##############################################################################
#       getNextAssignedJob()
#		get the next job that the user should be working on
##############################################################################
sub getNextAssignedJob
{
	my ($userid) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	
	my ($jobid) = $Config::dba->process_oneline_sql("SELECT job_number FROM ASSIGNMENTHISTORY
										WHERE assigneeid=? AND status='A'
										ORDER BY due_timestamp ASC
										LIMIT 1", [ $userid ]);
	
	return $jobid || 0;
}

##############################################################################
#       getJobFromPool()
#		get the next unassigned job that the user should be working on
#	U - unassinged job
##############################################################################
sub getJobFromPool
{
	my ($location) = @_;
	
	return 0 if $location && !looks_like_number($location);
	
	my ($jobid) = $Config::dba->process_oneline_sql("SELECT t3p.job_number FROM TRACKER3PLUS t3p
										INNER JOIN OPERATINGCOMPANY oc ON t3p.opcoid=oc.opcoid
										WHERE t3p.operatorstatus IN ('U', 'B')
											AND t3p.qcstatus NOT IN (1, 2)
											AND t3p.location=?
											AND oc.jobpoolassign = 1
										ORDER BY t3p.duedate, t3p.job_number
										LIMIT 1", [ $location ]);
										
	return $jobid || 0;
}


##############################################################################
#       isJobAssigned()
#		is this job assigned OR in PROGRESS to any user?
#		A job cannot be assigned if it is already assiged or is being worked on 
# status ->
# 
# U	- Unassigned
# A	- Assigned
# W	- being worked on
# S	- Submitted for QC
# R	- Rejected
# C 	- Closed
# 
##############################################################################
sub isJobAssigned
{
	my ($jobid) = @_;
	
	return 1 if $jobid && !looks_like_number($jobid);
	
	my ($isassigned) = $Config::dba->process_oneline_sql("SELECT 1 FROM assignmenthistory WHERE job_number=? AND status IN ('A', 'W')", [ $jobid ]);
	return $isassigned || 0;
}

sub isJobAssignedToOthers {

	my ($jobid) = @_;

	my ($isassigned,$productid,$productname,$status) = $Config::dba->process_oneline_sql("SELECT count(*),ID,productname,status FROM products WHERE job_number=? AND userid!=0 ", [ $jobid ]);
	return $isassigned || 0,$productid || 0, $productname, $status;
}
##############################################################################
#       isJobBeingWorkedOn()
#		is this job being worked on
##############################################################################
sub isJobBeingWorkedOn
{
	my ($jobid) = @_;
	
	return 1 if $jobid && !looks_like_number($jobid);
	
	my ($isworkedon) = $Config::dba->process_oneline_sql("SELECT 1 FROM assignmenthistory WHERE job_number=? AND status IN ('W')", [ $jobid ]);
	return $isworkedon || 0;
}

##############################################################################
# isJobBeingWorkedOnForProdcuts
#
##############################################################################
sub isJobBeingWorkedOnForProdcuts
{
    my ($jobid) = @_;

    return 1 if $jobid && !looks_like_number($jobid);

    my ($isworkedon) = $Config::dba->process_oneline_sql("SELECT 1 FROM assignmenthistory WHERE job_number=? AND status IN ('W', 'A')", [ $jobid ]);
    return $isworkedon || 0;
}

##############################################################################
# isJobAssignedToUser
#
##############################################################################
sub isJobAssignedToUser
{
    my ($jobid, $userid) = @_;

    return 1 if $jobid && !looks_like_number($jobid);
	return 1 if $userid && !looks_like_number($userid);

    my ($isassigned) = $Config::dba->process_oneline_sql("SELECT 1 FROM ASSIGNMENTHISTORY WHERE job_number=? AND ASSIGNEEID = ? AND status IN ('A')", [ $jobid, $userid ]);
    return $isassigned || 0;
}

##############################################################################
# isJobWorkedOnByUser
#
##############################################################################
sub isJobWorkedOnByUser
{
    my ($jobid, $userid) = @_;

    return 1 if $jobid && !looks_like_number($jobid);
    return 1 if $userid && !looks_like_number($userid);

    my ($isworkedon) = $Config::dba->process_oneline_sql("SELECT 1 FROM ASSIGNMENTHISTORY WHERE job_number=? AND ASSIGNEEID = ? AND status IN ('W')", [ $jobid, $userid ]);
    return $isworkedon || 0;
}

##############################################################################
#       markJobComplete() 
#		set this job as COMPLETE and remove assignee from TRACKER3PLUS
##############################################################################
sub markJobComplete
{
	my ($jobid, $userid) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET assignee=0 WHERE job_number=?", [ $jobid ]);
$Config::dba->process_sql("UPDATE ASSIGNMENTHISTORY SET status='C', submittedqc_timestamp=now()
					WHERE job_number=? AND status='W' AND assigneeid=?",  [ $jobid, $userid ]);
					
	return 1;
}

##############################################################################
#       db_getJobNotes() 
#	get the notes associated with this job	
##############################################################################
sub getJobNotes
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my $rows = $Config::dba->hashed_process_sql("SELECT jn.notetext, jn.rec_timestamp AS timestamp, u.username, cl.comment FROM JOBNOTES jn
										INNER JOIN USERS u ON u.id=jn.userid
										INNER JOIN COMMENTSLIST cl ON cl.id=jn.commentid
									WHERE jn.job_number=?
									ORDER BY jn.rec_timestamp DESC
									LIMIT 20", [ $jobid ]);
									
	return $rows ? @$rows : (); 	## ORIGINAL FUNCTION RETURNED AN ARRAY, SO DON'T BOTHER REDUCING IT
}

##############################################################################
#       addComment()
#	operator rejects the job
##############################################################################
sub addComment
{
	my ($jobid, $userid, $commenttype, $comment) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $commenttype && !looks_like_number($commenttype);
	
$Config::dba->process_sql("INSERT INTO JOBNOTES (commentid, job_number, notetype, userid, notetext, rec_timestamp)
					VALUES (?, ?, 1, ?, ?, now())", [ $commenttype, $jobid, $userid, $comment ]);
					
	return 1;
}

#############################################################################
#       db_getCommentsList()
#
##############################################################################
sub db_getCommentsList
{
	my $rows = $Config::dba->hashed_process_sql("SELECT id, ctype, comment FROM COMMENTSLIST ORDER BY ctype DESC, comment");
	return $rows ? @$rows : ();
}

#############################################################################
#       getCommentText()
#
##############################################################################
sub getCommentText
{
	my ($commentid) = @_;
	
	my ($comment) = $Config::dba->process_oneline_sql("SELECT comment FROM COMMENTSLIST WHERE id=?", [ $commentid ]);
	return $comment;
}


#############################################################################
#       getCommentType()
#
##############################################################################
sub getCommentType
{
	my ($commentid) = @_;
	
	my ($ctype) = $Config::dba->process_oneline_sql("SELECT ctype FROM COMMENTSLIST WHERE id=?", [ $commentid ]);
	return $ctype;
}


#############################################################################
#       recordStart()
#	We record when a user has 'started' a job (He/She hit 'Request')
##############################################################################
sub recordStart
{
	my ($jobid, $opcoid, $customerid, $agencyref, $version, $userid) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	return 0 if $jobid && !looks_like_number($jobid);
	
	eval {
		$Config::dba->process_sql("INSERT INTO TIMECAPTURE (job_number, opcoid, customerid, agencyref, version, userid, starttime)
						VALUES (?, ?, ?, ?, ?, ?, now())", [ $jobid, $opcoid, $customerid, $agencyref, $version, $userid ]);
	};
	if ( $@ ) {
		my $host = Config::getConfig("smtp_gateway");
		my $fromemail = Config::getConfig("fromemail");
		my $supportemail2 = Config::getConfig("supportemail2");
		Mailer::send_mail($fromemail, $supportemail2, $@, "TIMECAPTURE missing values: $jobid, $opcoid, $customerid, $agencyref, $version, $userid", { host => $host } );
	}
	return 1;
}


#############################################################################
#       recordStop()
#	We record when a user has 'stopped' a job (He/She hit 'Complete/Reject')
##############################################################################
sub recordStop
{
	my ($jobid, $userid) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TIMECAPTURE SET endtime=now() WHERE job_number=? AND userid=? AND endtime IS NULL ORDER BY id DESC LIMIT 1", [ $jobid, $userid ]);
	
	return 1;
}

#############################################################################
#       validStringFilter()
#
##############################################################################
sub validStringFilter
{
	my ($filter) = @_;
	
	return $filter =~ m/[^a-zA-Z0-9_,]/ ? 0 : 1;
}
#############################################################################
#       validStringFilter()
#
##############################################################################
sub validMysqlStringFilter
{
	my ($filter) = @_;
	
	return $filter =~ m/['"]/ ? 0 : 1;
}


#############################################################################
#       db_recordJobAction()
# TODO could call ECMDBAUdit::db_recordJobAction
#
##############################################################################
sub db_recordJobAction
{
	my ($jobid, $userid, $action, $destination) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	return 0 if $jobid  && !looks_like_number($jobid);
	
	$Config::dba->process_sql("INSERT INTO JOBEVENTS (job_number, userid, action, destination, rec_timestamp)
					VALUES (?, ?, ?, ?, now())", [ $jobid, $userid, $action, $destination ]);
	
	return 1;
}


#############################################################################
#       db_recordLibrarySyncAction()
#
# THIS HAS NOTHING TO DO WITH JOBS
#
##############################################################################
sub db_recordLibrarySyncAction
{
	my ($nodeid, $opconame, $customername, $username, $size) = @_;
	
	## BLOCKING ERRORS
		die "nodeid_not_number\n"	if $nodeid	&& !looks_like_number($nodeid);
		die "size_not_number\n"		if $size		&& !looks_like_number($size);
	
	
	$opconame =~ s/\"//g;
	my ($opcoid) = $Config::dba->process_oneline_sql("SELECT opcoid FROM OPERATINGCOMPANY WHERE opconame=? LIMIT 1", [ $opconame ]);
	die "opco_not_found\n" if !defined $opcoid;
	
	$customername =~ s/\"//g;
	my ($customerid) = $Config::dba->process_oneline_sql("SELECT id FROM CUSTOMERS WHERE customer=? AND opcoid=? LIMIT 1", [ $customername, $opcoid ]);
	die "customer_not_found\n" if !defined $customerid;
	
	my $userid;
	if($username)
	{
		($userid) = $Config::dba->process_oneline_sql("SELECT id FROM USERS WHERE username=? LIMIT 1", [ $username ]);
		die "user_not_found\n" if !defined $userid;
	}
	
$Config::dba->process_sql("INSERT INTO LIBRARYSYNCEVENTS (nodeid, customerid, userid, size)
					VALUES (?, ?, ?, ?)", [ $nodeid, $customerid, $userid, $size ]);
	
	return 1;
}


##############################################################################
# db_removeFromCheckInQueue
# remove this job from the Repo CheckIn Queue (taken from PollCheckIn)
##############################################################################
sub db_removeFromCheckInQueue
{
	my ($jobid, $shardname) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
$Config::dba->process_sql("DELETE FROM REPOCHECKINQUEUE WHERE job_number=? AND shard_name LIKE ? AND checkingin=1", [ $jobid, $shardname || "%" ]);

    return $jobid;
}


##############################################################################
# db_removeFromCheckOutQueue
# remove this job from the Repo CheckOut Queue (taken from PollCheckOut)
##############################################################################
sub db_removeFromCheckOutQueue
{
	my ($jobid, $shardname) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
$Config::dba->process_sql("DELETE FROM REPOCHECKOUTQUEUE WHERE job_number=? AND shard_name LIKE ? AND checkingout=1", [ $jobid, $shardname || "%" ]);
	
    return $jobid;
}


##############################################################################
# db_removeFromSyncQueue
# remove this job from the Sync Queue
##############################################################################
sub db_removeFromSyncQueue
{
	my ($jobid) = @_;
	
	return 0 if !$jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("DELETE FROM REPOSYNCQUEUE WHERE job_number=? AND syncing=1", [ $jobid ]);
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET LASTSYNCED=now() WHERE job_number=?", [ $jobid ]);
	
    return $jobid;
}

##############################################################################
# db_setRepoCheckedIn 
# Mark this job as checked in to the Repo
##############################################################################
sub db_setRepoCheckedIn
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET repocheckedin=1 WHERE job_number=?", [ $jobid ]);
}

##############################################################################
# db_setRepoCheckedOut 
# Mark this job as checked Out from the Repo
##############################################################################
sub db_setRepoCheckedOut
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET repocheckedout=1 WHERE job_number=?", [ $jobid ]);
} # db_setRepoCheckedOut

##############################################################################
# db_setJobSize 
##############################################################################
sub db_setJobSize
{
	my ($jobid, $jobsize) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $jobsize && !looks_like_number($jobsize);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET comparefolder=? WHERE job_number=?", [ $jobsize, $jobid ]);
} # db_setJobSize


##############################################################################
# db_updateJobStatus 
# Update the job statuss via the web service
##############################################################################
sub db_updateJobStatus
{
	my ($jobid, $preflightstatus, $qcstatus, $assetscreated, $hires) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $preflightstatus && !looks_like_number($preflightstatus);
	return 0 if $qcstatus && !looks_like_number($qcstatus);
	return 0 if $assetscreated && !looks_like_number($assetscreated);
	return 0 if $hires && !looks_like_number($hires);
	
$Config::dba->process_sql("UPDATE TRACKER3PLUS SET preflightstatus=?, qcstatus=?, assetscreated=?, hires=? WHERE job_number=?",
					[ $preflightstatus, $qcstatus, $assetscreated, $hires, $jobid ]);
}

#############################################################################
#       isJobOnRepoSyncQueue()
#
##############################################################################
sub isJobOnRepoSyncQueue
{
	my ($jobid) = @_;
	
	my ($inqueue) = $Config::dba->process_oneline_sql("SELECT 1 FROM REPOSYNCQUEUE WHERE job_number=?", [ $jobid ]);
	return $inqueue || 0;
}

##############################################################################
# requestSync 
# Add a job to the Sync Queue
##############################################################################
sub requestSync
{
	my ($jobid, $customerid, $opcoid, $userid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $customerid && !looks_like_number($customerid);
	return 0 if $opcoid && !looks_like_number($opcoid);
	return 0 if $userid && !looks_like_number($userid);
	
	return 0 if isJobOnRepoSyncQueue($jobid);
	
$Config::dba->process_sql("INSERT INTO REPOSYNCQUEUE (job_number, customerid, opcoid, userid, syncing, status, synctime, rec_timestamp)
					VALUES (?, ?, ?, ?, 0, '', now(), now())", [ $jobid, $customerid, $opcoid, $userid ]);
$Config::dba->process_sql("INSERT INTO REPOSYNCQUEUEHISTORY (job_number, customerid, opcoid, userid, rec_timestamp)
					VALUES (?, ?, ?, ?, now())", [ $jobid, $customerid, $opcoid, $userid ]);
}


sub getPublisherName
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	my ($publication) = $Config::dba->process_oneline_sql("SELECT publication FROM TRACKER3 WHERE job_number=?", [ $jobid ]);
	
	return $publication || "";
}


##############################################################################
# getCheckInQueue 
#	a list of the current jobs in the Check In Queue
##############################################################################
sub getCheckInQueue
{

	my ($job_number, $customer, $opconame, $toopconame, $username, $checkingin, $rec_timestamp, $timeonqueue, $priority, $jobsize, @checkinqueue );

	my $rows = $Config::dba->hashed_process_sql("SELECT q.job_number, c.customer, o.opconame, oc2.opconame AS toopconame, u.username, q.checkingin, q.rec_timestamp,
										q.priority, TIMEDIFF(now(), q.rec_timestamp) AS timeonqueue, t3p.comparefolder AS jobsize FROM repocheckinqueue q
										INNER JOIN OPERATINGCOMPANY o ON o.opcoid = q.opcoid
										INNER JOIN TRACKER3PLUS t3p ON t3p.job_number = q.job_number
										INNER JOIN CUSTOMERS c ON c.id = t3p.customerid
										INNER JOIN USERS u ON q.userid = u.id
										LEFT OUTER JOIN REPOSENDJOBSQUEUE sj ON q.job_number = sj.job_number
										LEFT OUTER JOIN OPERATINGCOMPANY oc2 ON oc2.opcoid = sj.opcoid
									ORDER BY rec_timestamp DESC
									LIMIT 500");
	return $rows ? @$rows : ();
} # getCheckInQueue


##############################################################################
# getCheckOutQueue 
#	a list of the current jobs in the Check Out Queue
##############################################################################
sub getCheckOutQueue
{
	my $rows = $Config::dba->hashed_process_sql("SELECT q.job_number, c.customer, o.opconame AS toopconame, u.username, q.checkingout, q.rec_timestamp, q.priority, q.shard_name,
											TIMEDIFF(now(), q.rec_timestamp) AS timeonqueue, comparefolder AS jobsize FROM repocheckoutqueue q
										INNER JOIN operatingcompany o ON o.opcoid = q.opcoid
										INNER JOIN tracker3plus t3p ON t3p.job_number = q.job_number
										INNER JOIN customers c ON c.id = t3p.customerid
										INNER JOIN users u ON q.userid = u.id
										ORDER BY rec_timestamp DESC
									LIMIT 500");
	return $rows ? @$rows : ();
} # getCheckOutQueue

##############################################################################
# getCheckInQueueHistory
##############################################################################
sub getCheckInQueueHistory
{
	my ($fromdate, $todate) = @_;
	
	my $rows = $Config::dba->hashed_process_sql("SELECT rh.job_number, rh.opcoid, o.opconame, c.customer, c.id as customerid, rh.userid, u.username, rh.rec_timestamp, rh.shard_name
											FROM REPOCHECKINQUEUEHISTORY rh
										INNER JOIN OPERATINGCOMPANY o ON o.opcoid=rh.opcoid
										INNER JOIN USERS u ON rh.userid=u.id
										INNER JOIN TRACKER3PLUS t ON t.job_number=rh.job_number
										INNER JOIN CUSTOMERS c ON c.id=t.customerid
									WHERE rh.rec_timestamp > '$fromdate'
										AND rh.rec_timestamp < '$todate'
									ORDER BY rh.rec_timestamp DESC
									LIMIT 500");
	return $rows ? @$rows : ();
} # getCheckInQueueHistory


##############################################################################
# changeJobQueuePriority 
##############################################################################
sub changeJobQueuePriority 
{
	my ($jobid, $priority) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $priority && !looks_like_number($priority);
	
$Config::dba->process_sql("UPDATE REPOCHECKINQUEUE SET priority=? WHERE job_number=?", [ $priority, $jobid ]);
$Config::dba->process_sql("UPDATE REPOSENDJOBSQUEUE SET priority=? WHERE job_number=?", [ $priority, $jobid ]);
} # changeJobQueuePriority   



##############################################################################
# changeJobCOQueuePriority 
##############################################################################
sub changeJobCOQueuePriority 
{
	my ($jobid, $priority) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $priority && !looks_like_number($priority);
	
$Config::dba->process_sql("UPDATE REPOCHECKOUTQUEUE SET priority=? WHERE job_number=?", [ $priority, $jobid ]);
} # changeJobCOQueuePriority   


##############################################################################
#       db_getWorkflow
#      
##############################################################################
sub db_getWorkflow
{
	my ($jobid) = @_;
	
	return "" if $jobid && !looks_like_number($jobid);
	
	my ($workflow) = $Config::dba->process_oneline_sql("SELECT workflow FROM TRACKER3 WHERE job_number=?", [ $jobid ]);
	
	return $workflow || "";
}


##############################################################################
# delayJobQueueSubmit 
##############################################################################
sub delayJobQueueSubmit
{
	my ($jobid, $delaytime) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $delaytime && !looks_like_number($delaytime);
	
$Config::dba->process_sql("UPDATE REPOCHECKINQUEUE SET rec_timestamp=DATE_ADD(now(), INTERVAL ? HOUR)
					WHERE job_number=?", [ $delaytime, $jobid ]);
} # delayJobQueueSubmit   


##############################################################################
# delayJobCOQueueSubmit 
##############################################################################
sub delayJobCOQueueSubmit
{
	my ($jobid, $delaytime) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	return 0 if $delaytime && !looks_like_number($delaytime);
	
$Config::dba->process_sql("UPDATE REPOCHECKOUTQUEUE SET rec_timestamp=DATE_ADD(now(), INTERVAL ? HOUR)
					WHERE job_number=?", [ $delaytime, $jobid ]);
} # delayJobCOQueueSubmit   

##############################################################################
#  getJobVersions
#       ret the versions of this Job
###############################################################################
sub getJobVersions
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my $rows = $Config::dba->hashed_process_sql("SELECT id AS versionid, filename FROM JOBVERSIONS WHERE job_number=? ORDER BY id DESC", [ $jobid ]);
	return $rows ? @$rows : ();
} # getJobVersions


##############################################################################
##  getMaxJobVersion
##       Get the max version of this job
###############################################################################
sub getMaxJobVersion
{
	my ($jobid) = @_;
	
	return 0 if $jobid && !looks_like_number($jobid);
	
	my ($maxversion) = $Config::dba->process_oneline_sql("SELECT MAX(id) FROM jobversions WHERE job_number=?", [ $jobid ]);
	return $maxversion || 0;
}


##############################################################################
#  enQueueCompare
#	add versions to compare onto compare Q
###############################################################################
sub enQueueCompare
{
	my ($userid, $jobid1, $version1, $jobid2, $version2) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	return 0 if $jobid1 && !looks_like_number($jobid1);
	return 0 if $version1 && !looks_like_number($version1);
	return 0 if $jobid2 && !looks_like_number($jobid2);
	return 0 if $version2 && !looks_like_number($version2);
	
$Config::dba->process_sql("INSERT INTO COMPAREQUEUE (userid, job_number1, version1, job_number2, version2, sentforcompare, comparefolder, rec_timestamp)
					VALUES (?, ?, ?, ?, ?, 0, '', now())", [ $userid, $jobid1, $version1, $jobid2, $version2 ]);
$Config::dba->process_sql("INSERT INTO COMPAREQUEUEHISTORY (userid, job_number1, version1, job_number2, version2, rec_timestamp)
					VALUES (?, ?, ?, ?, ?, now())", [ $userid, $jobid1, $version1, $jobid2, $version2 ]);
}



##############################################################################
# is this compare aleady on the queue? 
###############################################################################
sub isCompareOnQueue
{
	my ($userid, $jobid1, $version1, $jobid2, $version2) = @_;
	
	return 0 if $userid && !looks_like_number($userid);
	return 0 if $jobid1 && !looks_like_number($jobid1);
	return 0 if $version1 && !looks_like_number($version1);
	return 0 if $jobid2 && !looks_like_number($jobid2);
	return 0 if $version2 && !looks_like_number($version2);
	
	my ($inqueue) = $Config::dba->process_oneline_sql("SELECT 1 FROM COMPAREQUEUE WHERE userid=? AND job_number1=? AND version1=? AND job_number2=? AND version2=?",
										 [ $userid, $jobid1, $version1, $jobid2, $version2 ]);
	
	return $inqueue || 0;
}




sub db_getmedia
{
	my ($opcoid, $fromId) = @_;
	
	return () if $opcoid && !looks_like_number($opcoid);
	
	my $filterclause = "";
	$filterclause .= "AND t3p.rec_timestamp > DATE_ADD(now(), INTERVAL -$fromId DAY) " if length($fromId);
	
	my $rows = $Config::dba->hashed_process_sql("SELECT DISTINCT(t3.workflow) AS workflow FROM TRACKER3 t3
										INNER JOIN TRACKER3PLUS t3p ON t3p.job_number=t3.job_number
									WHERE t3.opcoid=? AND t3.workflow is not null $filterclause
									ORDER BY t3.workflow", [ $opcoid ]);
	
	my $index = 0;
	my @medias;
	foreach my $row (@$rows)
	{
		$index++;
		push(@medias, { id => "WORKFLOW_$index", WORKFLOW => $row->{workflow} });
	}
	
	return @medias;
}

sub db_userlist 
{
    my ($opcoid) = @_;
    my ( $id, $username, @userslist );
    my $rows = $Config::dba->hashed_process_sql("SELECT u.id AS id, u.username AS username FROM (SELECT DISTINCT(assignee), opcoid, location FROM tracker3plus t3p GROUP BY assignee, opcoid, location) t3p
	INNER JOIN users u ON t3p.assignee=u.id
	WHERE t3p.opcoid=? OR t3p.location=?
	GROUP BY username
	ORDER BY username ASC", [ $opcoid, $opcoid ]);

    foreach my $row (@$rows)
    {
        push(@userslist, { id => "ASS_" . ($row->{id} || "0"),   name => ($row->{username}) || "-", "userid" => ($row->{id} || "0") });
	}

    return \@userslist;
}

##############################################################################
### loadFile: To insert upload the digital selected files.
##############################################################################
sub loadFile {
	my ( $jobid, $customerid, $uploaderid, $weburl, $version, $filename, $filetype, $filesize, $approved, $approverid, $current, $dimension,$campaflag, $convert_flag ) = @_;

	return 0 if $uploaderid && !looks_like_number($uploaderid);
	return 0 if $jobid  && !looks_like_number($jobid);

	$Config::dba->process_sql("INSERT INTO JOBASSETS (job_number, customerid, uploaderid, weburl, version, current, filename, filetype, 
									filesize, approved, approverid, approved_timestamp, rec_timestamp, dimension, static_img,campaign_approval,convert_flag )
								VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now(), ?, ?, ?, ?)", 
							[ $jobid, $customerid, $uploaderid, $weburl, $version, $current, $filename, $filetype, $filesize, $approved, $approverid, $dimension, '0',$campaflag, $convert_flag ]);

	return 1;
}


##############################################################################
## Get the file details using job-id
###############################################################################
sub db_filedetails {
	my ( $job_number, $digitalFileName ) = @_;

	my @filelist;
	my $filedetails;

	eval {
		Validate::job_number( $job_number );
		# die "Invalid digital file name" unless defined $digitalFileName;
		$digitalFileName = '' unless defined $digitalFileName;

		$digitalFileName 	=~ s/\s+|-/_/g;  # Change spaces or hyphen to underscore

		if ( $digitalFileName ne "" ) {
			($filedetails) = $Config::dba->hashed_process_sql("SELECT ja.id, version, filename, filesize, jaas.status, ja.static_img  
									FROM jobassets as ja 
									INNER JOIN jobassetsapprovalstatus jaas on ja.approved = jaas.id
									WHERE ja.job_number=? AND ja.current=1 
									AND ja.version=1 AND ja.filename 
									IN ('" . ($digitalFileName =~ s/,/','/rg) . "')", [ $job_number ]);
		} else {
			($filedetails) = $Config::dba->hashed_process_sql("SELECT ja.id, version, filename, filesize, jaas.status, ja.static_img   FROM jobassets as ja 
									INNER JOIN jobassetsapprovalstatus jaas on ja.approved = jaas.id
									WHERE job_number=? and current=1", [ $job_number ]);
		}
		if ( defined $filedetails ) {
			foreach my $files (@$filedetails) {
				my $filename = $files->{filename};
				$filename = uri_escape_utf8( $filename ) if defined $filename;

				push(@filelist, {
					filename => ( $filename ) || "-",
					filesize => ($files->{filesize}) || "-", 
					version => ($files->{version}) || "-", 
					status => ($files->{status}) || "-", 
					static_img => ($files->{static_img}), 
					assetId => ($files->{id}) || "-"});
			}
		}
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "db_filedetails: $error";
	}
	return \@filelist;

}#db_filedetails

##############################################################################
## Get the file versions using job-id, file-size and file-name
###############################################################################
sub db_fileversion
{
        my ($job_number, $filename) = @_;
        my @filelist;

        my ($fileversion) = $Config::dba->hashed_process_sql("SELECT max(version) as version FROM jobassets WHERE (job_number=? and filename =?)", [ $job_number, $filename ]);

	if(defined $fileversion){
		return $fileversion;
	}
	else{
		return 0;
	}
}

##############################################################################
# getVersionHistory
##############################################################################
sub getVersionHistory
{
        my ($jobid, $filename) = @_;

        my @list;

        my $rows = $Config::dba->hashed_process_sql("SELECT job_number, customerid, uploaderid, weburl, version, current, filename, filetype, filesize, approved, approverid, approved_timestamp, rec_timestamp,id from jobassets WHERE job_number= ? and filename =?", [$jobid, $filename] );

        if ( defined $rows ) {
            foreach my $files (@$rows) {
				my $filename = $files->{filename};
				$filename = uri_escape_utf8( $filename ) if defined $filename;
                push(@list, {
                    filename => ( $filename) || "-",
                    job_number => ($files->{job_number}) || "-",
                    customerid => ($files->{customerid}) || "-",
                    uploaderid => ($files->{uploaderid}) || "-",
                    current => ($files->{current}) || "-",
                    filetype => ($files->{filetype}) || "-",
                    filesize => ($files->{filesize}) || "-",
                    approved => ($files->{approved}) || "-",
                    approverid => ($files->{approverid}) || "-",
                    approved_timestamp => ($files->{approved_timestamp}) || "-",
                    rec_timestamp => ($files->{rec_timestamp}) || "-",
                    id => ($files->{id}) || "-",
                    weburl => ($files->{weburl}),
                    version => ($files->{version}) || "-"});
            }
        }


        return @list;
} # getVersionHistory

##############################################################################
# getdocVersionHistory
##############################################################################
sub getdocVersionHistory
{
        my ($jobid, $filename) = @_;
		my @list;
        my $rows = $Config::dba->hashed_process_sql("SELECT jd.job_number, jd.documentname as filename, jd.documenttype as filetype, jd.documentsize as filesize,   
                             jd.version, jd.current, jd.rec_timestamp, u.username, jd.id
                             FROM
                             JOBDOCUMENTS jd
                             INNER JOIN users u
                             WHERE
                             jd.job_number=?
                             AND u.id=jd.uploaderid
                             AND jd.documentname =?", [$jobid, $filename] );

		if ( defined $rows ) {
			foreach my $files (@$rows) {
				my $filename = $files->{filename};
				$filename = uri_escape_utf8( $filename ) if defined $filename;
                push(@list, {
                    filename => ( $filename) || "-",
                    job_number => ($files->{job_number}) || "-",
                    filetype => ($files->{filetype}) || "-",
                    filesize => ($files->{filesize}) || "-",
                    rec_timestamp => ($files->{rec_timestamp}) || "-",
                    id => ($files->{id}) || "-",
                    username => ($files->{username}) || "-",
                    current => ($files->{current}),
                    version => ($files->{version}) || "-"});
            }
        }


        return @list;
}

##############################################################################
# updateCurrentValue
##############################################################################
sub updateCurrentValue
{
        my ($jobid, $filename) = @_;

	$Config::dba->process_sql("UPDATE jobassets SET current= 0 WHERE job_number=? and filename =?", [ $jobid, $filename ]);

        return 1;


} # set current value to 0 other than current file.

###############################################################################
# To set 'QC REQUIRED'  whenever assets versions are changed.
###############################################################################

sub rollBack {

	my ($jobid, $filename, $version, $tablename, $filenamecolumn) = @_;
	$Config::dba->process_sql("UPDATE $tablename SET current= 0 WHERE job_number=? and $filenamecolumn =?", [ $jobid, $filename ]);
	$Config::dba->process_sql("UPDATE $tablename SET current= 1 WHERE job_number=? and $filenamecolumn =? and version =?", [ $jobid, $filename, $version ]);
	if ( $tablename =~ /^jobassets$/i ) {
		$Config::dba->process_sql("UPDATE $tablename SET approved = 0 WHERE job_number=? and $filenamecolumn =? and version =?", [ $jobid, $filename, $version ]);
	}

	return 1;
} #rollBack  set current value to 0 other than current file.

##############################################################################
# delete file from DB
##############################################################################
sub erasefromDB
{
        my ($jobid, $filename) = @_;

        ## VALIDATE INPUTS
        return 0 if $jobid && !looks_like_number($jobid);
		$Config::dba->process_sql("delete from jobassets WHERE job_number=? and filename=?", [ $jobid, $filename ]);

        return 1;
}

#############################################################################
#       get max notes ID from DB
#
##############################################################################
sub getNotesID
{
        my ($jobid) = @_;

        my ($rowid) = $Config::dba->process_oneline_sql("SELECT MAX(ID) AS ID FROM JOBNOTES WHERE JOB_NUMBER=?", [ $jobid ]);
        return $rowid;
}

#############################################################################
#       get comments for reject job
#
##############################################################################
sub getCommentList
{
        my ($jobid, $rowid) = @_;

        my ($commentList, $username, $datetime) = $Config::dba->process_oneline_sql("SELECT a.NOTETEXT,(CONCAT(b.FNAME, ' ', b.LNAME)) USERNAME, a.REC_TIMESTAMP  FROM JOBNOTES a, users b  WHERE a.JOB_NUMBER=? and a.ID=? and a.USERID = b.ID ", [ $jobid, $rowid ]);
        return $commentList, $username, $datetime;
}


##############################################################################
### loadDocument: To insert document details into DB.
##############################################################################
sub loadDocument {

	my ( $jobid, $customerid, $uploaderid, $weburl, $version, $documentName, $documentType, $documentSize, $approved, $approverid, $current ) = @_;

	return 0 if $uploaderid && !looks_like_number($uploaderid);
	return 0 if $jobid  && !looks_like_number($jobid);

	$Config::dba->process_sql("INSERT INTO JOBDOCUMENTS (job_number, customerid, uploaderid, weburl, version, current, documentname, documenttype, documentsize, approved, approverid, approved_timestamp, rec_timestamp)
                                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now())", [ $jobid, $customerid, $uploaderid, $weburl, $version, $current, $documentName, $documentType, $documentSize, $approved, $approverid ]);
    return 1;
}

##############################################################################
#### eraseDocDBDetails(): To delete document details from DB.
###############################################################################
sub eraseDocDBDetails {

	my ( $jobid, $documentName ) = @_;

	# VALIDATE INPUTS
	return 0 if $jobid && !looks_like_number($jobid);

	$Config::dba->process_sql("DELETE FROM JOBDOCUMENTS WHERE job_number=? and documentname=?", [ $jobid, $documentName ]);
	
}

sub db_documentsDetails {

	my ($job_number) = @_;
    my @documentList;

    my ( $documentDetails ) = $Config::dba->hashed_process_sql("SELECT version, documentname, documentsize, jaas.status as qcstatus  FROM JOBDOCUMENTS as ja 
                                                                                                INNER JOIN jobassetsapprovalstatus jaas on ja.approved = jaas.id
																								WHERE job_number=? and current=1", [ $job_number ]);
	foreach my $doc ( @$documentDetails ) {
		my $docname = $doc->{documentname};
		$docname = uri_escape_utf8( $docname ) if defined $docname;
		push(@documentList, {documentname => $docname || "-", documentsize => ($doc->{documentsize}) || "-", version => ($doc->{version}) || "-", qcstatus => ($doc->{qcstatus}) || "-" });
	}
	return \@documentList;
}

  
##############################################################################
### Get the documents versions using job-id, document-name
################################################################################
sub db_DocumentVersion {

	my ($job_number, $documentName ) = @_;

	my ( $documentVersion ) = $Config::dba->hashed_process_sql( "SELECT max(version) as version FROM JOBDOCUMENTS WHERE (job_number=? and documentname =?)", [ $job_number, $documentName ]);

	if( defined ( $documentVersion ) ){
		return $documentVersion;
	} else {
		return 0;
	}
}

sub updateDocumentCurrentValue {

        my ($jobid, $documentName) = @_;

        $Config::dba->process_sql("UPDATE JOBDOCUMENTS SET current= 0 WHERE job_number=? and documentname =?", [ $jobid, $documentName ]);

        return 1;

}

# moved addwatchlist and stopwatchlist to Watchlist.pl
# moved Getusernotification, Getwholeusernotification, checkandupdatenotification, insertnotification and updatenotification to databaseUserNotification.pl

sub watchuserslist
{
        my $jobid       = $_[0];
        my @userslist   = ();
        my $rows = $Config::dba->hashed_process_sql("SELECT DISTINCT(USERID) as userid FROM jobnotification WHERE job_number=?",[$jobid]);
        foreach my $row (@$rows)
        {
                my $id = $row->{userid};
                push @userslist, $id;
        }
        return @userslist;
}



## Add a comment to either a job, or a job's task (product/lineitem)
#  called by various functions in modules/jobs.pl

sub addJobComment
{
	my ($userid, $job_number, $lineitemid, $comment, $id , $sequence, $rejectFlag) = @_;

	my $last_insert_id	= undef;
	$lineitemid ||= 0;
	$rejectFlag = 0 unless defined $rejectFlag;
	eval {
		Validate::userid( $userid );
		Validate::job_number( $job_number );
		die "Invalid Line Item ID ('$lineitemid')" if !General::is_valid_id($lineitemid);
		die "Invalid comment" unless defined $comment;

		## Get the next entry in the sequence
		my ($seq_max) = $Config::dba->process_oneline_sql(
					"SELECT sequence FROM comments
					WHERE job_number=?  ORDER BY sequence DESC
					LIMIT 1", [ $job_number ]);
		$seq_max = -1 if !defined $seq_max;

		my $sequence_no = ++$seq_max;	

		# TODO why do this here AFTER doing the query above??? PD-2188
		if ( ( defined $id ) && ( $id ne '' ) &&( $id > 0 ) ) {
			die "Invalid sequence" unless ( defined $sequence ) && looks_like_number( $sequence );
			$sequence_no = $sequence;
		}
		$comment =~ s/<a\s*href="(http[^"]*)"\s*>/<a target="_blank" href="$1">/g;
		## Add the comment relative to the list
		$Config::dba->process_sql(
			"INSERT INTO comments (userid, job_number, lineitemid, sequence, content, rec_timestamp, rejectflag)
				VALUES (?, ?, ?, ?, ?, now(), ?)",
				[ $userid, $job_number, $lineitemid, $sequence_no , $comment, $rejectFlag ] );
		( $last_insert_id ) = $Config::dba->process_onerow_sql("SELECT LAST_INSERT_ID()");	
		db_recordJobAction($job_number, $userid, CTSConstants::JOB_BRIEF_COMMENTS );
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		carp "addJobComment: $error";
	}	
	return $last_insert_id;
}

sub updateJobComment
{
	my ( $commentID, $job_number ) = @_;

	$Config::dba->process_sql("UPDATE comments SET status=2 WHERE id=?", [ $commentID ] );
	my ( $currentCommentID ) = $Config::dba->process_oneline_sql( "SELECT MAX(id) FROM comments WHERE job_number=?", [ $job_number ] );

	return $currentCommentID;
}

## Get a list of the comments available for a given job/product/lineitem
sub getJobCommentHeaders
{
	my ($jobid, $lineitemid, @statuses) = @_;
	@statuses = (COMMENT_ACTIVE) if !scalar @statuses;		## Default to active (non-deleted/not old revision) comments
	$lineitemid ||= 0;
	
	return '' if !General::is_valid_id($jobid);

	## Get comment descriptions; only limit by product if a product has been specified
	my $comments = $Config::dba->hashed_process_sql(
                "SELECT u.fname, u.lname, c.job_number, c.lineitemid, c.sequence,c.content, p.productname,
                                DATE_FORMAT(c.rec_timestamp,'\%a, \%d \%b \%Y \%H:\%i:\%s') AS timestamp, c.id, c.status, c.rejectflag
                        FROM comments c
                        INNER JOIN users u ON u.id=c.userid
                        LEFT OUTER JOIN PRODUCTS p ON p.id=c.lineitemid
                        WHERE c.job_number=? AND c.status IN (" . join(', ', @statuses) . ") " .
                        ($lineitemid ? " AND c.lineitemid=? " : "") .
                        "ORDER BY c.sequence DESC, c.status asc, c.rec_timestamp desc ",
                        [ $jobid, $lineitemid ? $lineitemid : ()]);
	if ( defined $comments ){
		foreach my $commentdetails (@$comments) {
			my $content = $commentdetails->{content};
			$content = uri_escape_utf8( $content ) if defined $content;
			$commentdetails->{content} = $content;
		}
	}
	return $comments ? @$comments : ();
}

# Bit of a bodge I'm sure, but avoids importing constants via a *.pl file
# Called by Showjob to get the task comments for the job

sub getJobCommentHeadersFormatted {
	my ( $job ) = @_;

	return getJobCommentHeaders( $job, 0, ( COMMENT_ACTIVE, COMMENT_DELETED, COMMENT_PREVIOUS_REVISION ) );
}


sub getJobCommentActiveCount
{
	my ($jobid, $lineitemid) = @_;
	$lineitemid ||= 0;
	
	return '' if !General::is_valid_id($jobid);


	my ($count)= $Config::dba->process_oneline_sql("SELECT count(*)  FROM comments
		                                WHERE job_number=? " . ($lineitemid ? " AND lineitemid=? " : "") .
		                                "AND status=?",
		                                [ $jobid, $lineitemid ? $lineitemid : (), COMMENT_ACTIVE ]);


	return $count || 0;

}

## Get a specific comment
sub getJobComment
{
	my ($jobid, $lineitemid, $sequence) = @_;
	
	$lineitemid ||= 0;
	$sequence ||= 0;
	
	return '' if !General::is_valid_id($jobid);
	return '' if !General::is_valid_id($lineitemid);
	return '' if !General::is_valid_number($sequence);
	
	my ($comment)= $Config::dba->process_oneline_sql("SELECT content FROM comments
								WHERE job_number=?
									AND lineitemid=?
									AND sequence=?
								GROUP BY rec_timestamp
								DESC LIMIT 1", [ $jobid, $lineitemid, $sequence ]);

	return $comment || "";
}

sub getComment
{
	my ($commentid) = @_;
	
	return '' if !General::is_valid_id($commentid);
	
	my ($comment)= $Config::dba->process_oneline_sql("SELECT content FROM comments
								WHERE id=?", [ $commentid ]);

	return $comment || "";
}


##############################################################################
### To get the Max document size to be upload.
###############################################################################
sub db_getDocMaxSize {

	my $custID              = $_[0];

	my ( $docMaxSize, $digitalmaxsize, $docMaxSize_arr_ref,  $docMaxSize_Hash_ref ) = undef;

	eval { # hashed_process_sql can return undef
		$docMaxSize_arr_ref		= $Config::dba->hashed_process_sql("SELECT docmaxsize, digitalmaxsize FROM customers WHERE id=?", [$custID]);
		if ( defined $docMaxSize_arr_ref ) {
			$docMaxSize_Hash_ref	= @{$docMaxSize_arr_ref}[0];
			$docMaxSize				= $docMaxSize_Hash_ref->{docmaxsize};
			$digitalmaxsize			= $docMaxSize_Hash_ref->{digitalmaxsize};
		}
	};
	if ( $@ ) {
		carp "db_getDocMaxSize: $@";
	}
	return ( $docMaxSize, $digitalmaxsize );

}

sub db_getWatchlistcount
{
        my $userid  = $_[0];
        my $opcoid = $_[1];

        return 0 if $userid && !looks_like_number($userid);
        return 0 if $opcoid && !looks_like_number($opcoid);

        my ( $count )= $Config::dba->process_oneline_sql("SELECT count(*)  FROM jobnotification WHERE USERID=?", [ $userid ]);

        return $count;
}

sub db_disableInCheckInQueue
{
	my ($jobid, $shardname) = @_;
	
	$Config::dba->process_oneline_sql("UPDATE repocheckinqueue SET priority=-1 WHERE job_number=? AND shard_name=?", [ $jobid, $shardname || "" ]);
}

sub db_disableInCheckOutQueue
{
	my ($jobid, $shardname) = @_;
	
	$Config::dba->process_oneline_sql("UPDATE repocheckoutqueue SET priority=-1 WHERE job_number=? AND shard_name=?", [ $jobid, $shardname || "" ]);
}

sub db_disableInSyncQueue
{
	my ($jobid, $shardname) = @_;
	
	$Config::dba->process_oneline_sql("UPDATE reposyncqueue SET priority=-1 WHERE job_number=?", [ $jobid, $shardname || "" ]);
}

##############################################################################
## Get the file name using job-id
###############################################################################
sub db_getfilename
{
	my ( $job_number, $showAssetsFlg ) = @_;
    my @filelist    = ();
    my $filedetails = undef;
    my $current     = "1";

    if ( $showAssetsFlg =~ /^ShowAsset$/i ) {

        ($filedetails) = $Config::dba->hashed_process_sql("SELECT ja.id, filename, filetype, dimension, filesize, jaas.status, version FROM jobassets as ja
                                                                                                 INNER JOIN jobassetsapprovalstatus jaas on ja.approved = jaas.id       
                                                                                                 WHERE job_number=? and current=?", [ $job_number, $current ]);
    } else {
        ($filedetails) = $Config::dba->hashed_process_sql("SELECT ja.id, filename, filetype, dimension, filesize, jaas.status FROM jobassets as ja
                                                                                                 INNER JOIN jobassetsapprovalstatus jaas on ja.approved = jaas.id       
                                                                                                 WHERE job_number=?", [ $job_number ]);
    }

    return $filedetails ? @$filedetails : ();
}

##############################################################################
## renderPdf
##############################################################################
sub db_renderPdf
{
    my $jobID		= $_[0];
    my $userID		= $_[1];
	my $sentforPdf	= "0";

    return 0 if $jobID && !looks_like_number($jobID);
    return 0 if $userID && !looks_like_number($userID);

    my ( $count )= $Config::dba->process_oneline_sql("SELECT count(*)  FROM digitalpdfqueue WHERE USERID=? AND JOB_NUMBER=?", [ $userID, $jobID ]);
	if ( $count >= 1 ) {
		return 0;	
	}
	else{
		my $rowInserted = $Config::dba->process_sql("INSERT INTO digitalpdfqueue ( USERID, JOB_NUMBER, SENTFORPDF,REC_TIMESTAMP ) VALUES ( ?, ?, ?, now() ) ", [ $userID, $jobID, $sentforPdf ]);
	}

	return 1;
}

##############################################################################
## Update QC Status
###############################################################################
sub db_updateqc
{

        my ($job_number,$filename,$fileid,$width,$height,$value) = @_;

                my $dimension   = $width."*".$height;
                my ($approved)  = $Config::dba->process_oneline_sql("SELECT id from jobassetsapprovalstatus WHERE status=?", [ $value ]);

        $Config::dba->process_sql("UPDATE jobassets SET dimension=?, approved=? WHERE job_number=? and filename =? and id =?", [ $dimension, $approved, $job_number, $filename, $fileid ]);

        return 1;

}

##############################################################################
## view rendered pdf
###############################################################################
sub db_viewrenderpdf
{
	my ( $job_number ) = @_;

	my ( $viewrenderedpdf )  = $Config::dba->process_oneline_sql("SELECT RENDEREDPDF from TRACKER3PLUS WHERE job_number=?", [ $job_number ]);

	return $viewrenderedpdf;

}

##############################################################################
## db_updateViewRenderedPdf
###############################################################################
sub db_updateViewRenderedPdf
{

	my ( $job_number, $assetDownloadFlag ) 		= @_;
	my $viewRenderedPdfFlg	= 0; #Which shows the render is in progress.

	$Config::dba->process_sql("UPDATE TRACKER3PLUS SET renderedpdf =? WHERE job_number =?", [ $viewRenderedPdfFlg, $job_number ]);
	$Config::dba->process_sql("UPDATE JOBASSETS SET download_asset_flag = ? WHERE job_number =? AND current = 1 AND approved = 1 ", [ $assetDownloadFlag, $job_number ]);
					#("UPDATE TRACKER3PLUS SET creative=? WHERE job_number=?", [ $creative, $jobid ])
	return 1;
}

##############################################################################
## view rendered pdf
###############################################################################
sub db_getQCApprovedFileCounts
{
    my ( $job_number ) 	= @_;
	my $qcApproved		= "1";

	
    my ( $db_getQCApprovedFileCounts )  = $Config::dba->process_oneline_sql( "SELECT count(*) from jobassets WHERE job_number = ? and approved = ? and current = 1 ", [ $job_number, $qcApproved ] );

    return $db_getQCApprovedFileCounts;

} #db_getQCApprovedFileCounts


##############################################################################
### db_getStaticFileCount
################################################################################
sub db_getStaticFileCount
{
    my ( $job_number )  = @_;
    my $qcApproved      = "1";


    my ( $staticFileCount )  = $Config::dba->process_oneline_sql( "SELECT count(*) AS assetcount FROM jobassets WHERE JOB_NUMBER = ? AND approved = 1 AND static_img NOT IN (2) ", [ $job_number ] );

    return $staticFileCount;

} #db_getStaticFileCount


##############################################################################
# db_deleteAsset  from DB
##############################################################################
sub db_deleteAsset {

    my ($jobID, $fileName, $deleteFileVersion, $tablename, $filenamecolumn) = @_;

    ## VALIDATE INPUTS
    return 0 if $jobID && !looks_like_number($jobID);

    $Config::dba->process_sql("delete from $tablename WHERE job_number=? and $filenamecolumn=? and version=?", [ $jobID, $fileName, $deleteFileVersion ]);

    return 1;
}

##############################################################################
## db_getAssetID: To get the id of currently deleted digital asset file
###############################################################################
sub db_getAssetID {

    my ( $job_number, $fileName, $deleteFileVersion, $tablename, $filenamecolumn ) = @_;

    my ( $assetID )  = $Config::dba->process_oneline_sql("SELECT id FROM $tablename WHERE job_number=? and $filenamecolumn=? and version=?", [ $job_number, $fileName, $deleteFileVersion ]);

    return $assetID;
}

##############################################################################
### db_updateStaticUpload : update the static file upload status.
##############################################################################

sub db_updateStaticUpload {

	my ( $jobid, $movingObjName, $staticImgFlg ) = @_;

	return 0 if $jobid  && !looks_like_number($jobid);

	$Config::dba->process_sql("UPDATE jobassets SET static_img=? where job_number=? AND filename =?", [ $staticImgFlg, $jobid, $movingObjName ] );

	return 1;
} #db_updateStaticUpload

##############################################################################
## db_getStaticFlag: 
###############################################################################

sub db_getStaticFlag {

    my ( $job_number, $fileName ) = @_;

    my ( $currStaticFlag )  = $Config::dba->process_oneline_sql("SELECT static_img FROM jobassets WHERE job_number=? and filename=? and current=1", [ $job_number, $fileName ]);

    return $currStaticFlag;
} #db_getStaticFlag

# Get the first (most recent) DocID for the compare done with the specified job number
# Return either a valid integer docID or undef
# Throws an exception if the job number is invalid

sub db_getESCompareIDForJob {
	my ( $job_number ) = @_;
	die "Invalid job number" unless ( ( defined $job_number ) && ( looks_like_number( $job_number ) ) );
	my $docID;
	eval {
		( $docID ) = $Config::dba->process_oneline_sql(
			"SELECT docID FROM escomparehistory WHERE (job1 = ? OR job2 = ?) AND deleted IS NULL ORDER BY REC_TIMESTAMP DESC LIMIT 1", [ $job_number, $job_number ], 1 );
	};

	return $docID;
}
##############################################################################
#### db_getidnamebytablename
#### We can use this function resuability if we need to get only id and name
#### from status tables to avoid same functions again and again
###############################################################################
sub db_getidnamebytablename{
	my $tablename 	= $_[0];
	my $columnname 	= $_[1];	
	my @values		=();

    my $rows = $Config::dba->hashed_process_sql("SELECT id, $columnname FROM $tablename");
    foreach my $row (@$rows)
    {
        push(@values, { id => $row->{id},   name => $row->{$columnname} });
    }
    return @values;
}
##############################################################################
#### db_getcountries 
###############################################################################
sub db_getcountries {
    my $rows = $Config::dba->hashed_process_sql("SELECT id, country FROM countries");
    my @countries=();

    foreach my $row (@$rows)
    {
        push(@countries, { id => $row->{id},   name => $row->{country} });
    }
    return @countries;
}
##############################################################################
##### db_getlanguagelist 
################################################################################
sub db_getlanguagelist{
	my $cid       = $_[0];
	my @languagelist = ();

    my $rows = $Config::dba->hashed_process_sql("SELECT C.lid as id, L.language as lang FROM COUNTRYLANGUAGE C 
												 INNER JOIN LANGUAGES L	
												 WHERE L.id=C.lid and C.cid=?", [$cid]);

    foreach my $row (@$rows)
    {
        push(@languagelist, { id => $row->{id},   name => $row->{lang} });
    }

	return @languagelist;
}
##############################################################################
###### db_getlanguagelist 
#################################################################################
sub db_languagebycountry
{
	my $job_num=$_[0];
	my @languagelist=();

 	my $rows = $Config::dba->hashed_process_sql("SELECT CL.lid as id, lg.language as lang FROM Broadcasttracker3 bt
												INNER JOIN COUNTRYLANGUAGE CL
												INNER JOIN languages lg
												WHERE bt.job_number=?
												AND CL.cid=bt.country
												AND lg.id=CL.lid", [$job_num]);	

	foreach my $row (@$rows)
    {
        push(@languagelist, { id => $row->{id},   name => $row->{lang} });
    }

	return @languagelist;

}

#############################################################################o
### db_getdigitalAssetCount: When click 'Review' button in 'Digital' tab that time check assets present or not in DB.
##############################################################################
sub db_getdigitalAssetCount {
	my ( $job_number ) = @_;
	my $digitalAssetReviewFlag = 0;

	die "Invalid job number" unless ( ( defined $job_number ) && ( looks_like_number( $job_number ) ) );

	my ( $digitalAssetCounts ) = $Config::dba->process_oneline_sql("SELECT count(*) FROM jobassets WHERE job_number = ? ", [ $job_number ]);

	if ( $digitalAssetCounts > 0 ) {
		$digitalAssetReviewFlag = 1;	
	} 
	return  $digitalAssetReviewFlag;
} #db_getdigitalAssetCount

##############################################################################
### db_deleteAllAssets: To delete all uploaded assets from DB and '/gpfs' locations.
##############################################################################
sub db_deleteAllAssets {
    my ( $job_number ) = @_;

    die "Invalid job number" unless ( ( defined $job_number ) && ( looks_like_number( $job_number ) ) );
	$Config::dba->process_sql("DELETE FROM JOBASSETS WHERE job_number=? ", [ $job_number ]);

} #db_deleteAllAssets

##############################################################################
### db_getAssetDownloadFlag: To allow external url to download asset based on this flag.
##############################################################################
sub db_getAssetDownloadFlag {
    my ( $job_number ) = @_;

    die "Invalid job number" unless ( ( defined $job_number ) && ( looks_like_number( $job_number ) ) );
	
    my ( $assetDownloadFlag ) = $Config::dba->process_oneline_sql("SELECT download_asset_flag FROM jobassets WHERE job_number = ? AND current = 1 AND approved = 1 LIMIT 1 ", [ $job_number ]);

    return  $assetDownloadFlag;
} #db_getAssetDownlaodFlag

##############################################################################
#### db_updateCampaignFlag: To update campaign flag in jobassets table.
###############################################################################
sub db_updateCampaignFlag {

    my ( $campjobnum , $campaignflag ) = @_;

#    die "Invalid job number" unless ( ( defined $campjobnum ) && ( looks_like_number( $campjobnum ) ) );

    my ( $result ) = $Config::dba->process_sql("UPDATE jobassets SET campaign_approval = ? WHERE job_number =? ", [ $campaignflag,$campjobnum ]);

    return  $result;
} #db_updateCampaignFlag

##############################################################################
##### db_getcampaignFlag: To select the campaign flag from jobassets table
################################################################################
sub db_getcampaignFlag {
    my ( $campjobnum ) = @_;

    die "Invalid job number" unless ( ( defined $campjobnum ) && ( looks_like_number( $campjobnum ) ) );

    my ( $resultflag ) = $Config::dba->process_oneline_sql("SELECT campaign_approval FROM jobassets WHERE job_number = ? ", [ $campjobnum ]);

    return  $resultflag;
} #db_getcampaignFlag

##############################################################################
### db_getexternalUrlJobRef:  To get the 24digit unique id for external url.
##############################################################################
sub db_getexternalUrlJobRef {
    my ( $job_number ) = @_;

    die "Invalid job number" unless ( ( defined $job_number ) && ( looks_like_number( $job_number ) ) );

    my ( $externalUrlJobRef ) = $Config::dba->process_oneline_sql("SELECT EXTERNALURL FROM tracker3plus WHERE job_number = ? ", [ $job_number ]);

    return  $externalUrlJobRef;
} #db_getexternalUrlJobRef

#############################################################################
### updateProductEndTime:  To get the 24digit unique id for external url.
##############################################################################
sub updateProductEndTime {
	my $query       	= $_[0];
	my $session     	= $_[1];
	my $lineItemID     	= $query->param( 'lineItemID' );
	my $userID			= $session->param( 'userid' );
	my $mode			= $query->param( 'mode' );
	my $custID			= $query->param( 'custID' );
	my $opcoID			= $query->param( 'opcoID' );
	my $mediaID			= $query->param( 'mediaID' );
	my $checkListID 	= undef;

	my $rows = $Config::dba->hashed_process_sql("SELECT CD.ID FROM CHECKLISTDATA CD
													INNER JOIN OPERATINGCOMPANY OP on OP.OPCOID=CD.OPCOID
													INNER JOIN CUSTOMERS CS on CS.ID=CD.CUSTID
													INNER JOIN MEDIATYPES MD on MD.ID=CD.MEDIAID WHERE CD.description = 'LISTDATA' and CD.OPCOID = ? and CD.CUSTID = ? and CD.MEDIAID =? ", [$opcoID, $custID, $mediaID]);
	if ($rows) {
		my @editrows = @$rows;
		$checkListID = $editrows[0]->{'ID'};
	}

	$Config::dba->process_sql("UPDATE productstimecapture SET ENDTIME=now() WHERE productid=? AND USERID=? AND ENDTIME IS NULL AND ISCHECKLIST = 1 AND CHECKLISTID=?", [ $lineItemID, $userID, $checkListID ]);

	my $vars = {
		returnVal=>'Success',
	};

	if ( $mode eq "json" ){
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

#############################################################################
### db_getFileExtensions:  To get db configured file extension for upload.
##############################################################################
sub db_getFileExtensions {
	my $tableName	= $_[0];
	my $columnName	= $_[1];
	my  $fileExt_ref = 	$Config::dba->hashed_process_sql("SELECT $columnName FROM $tableName");
	return $fileExt_ref;
} #db_getFileExtensions

#############################################################################
# getUserJobs
#
#############################################################################
sub getUserJobs{

    my ($userid) = @_;

    my $userJobs_ref = $Config::dba->hashed_process_sql("SELECT Job_number from assignmenthistory where STATUS IN ( 'A', 'W') and ASSIGNEEID = ( ? )",[ $userid ]);

	my @userJobs_arrayref 	= ();
	my @userJobs 			= ();

	if ( $userJobs_ref ) {
		@userJobs_arrayref = @$userJobs_ref;
	}
	
	foreach my $job_hashref ( @userJobs_arrayref ) {
		push ( @userJobs, $job_hashref->{'Job_number'} );
	}
	
	if ( @userJobs ) {
    	return  join(',', @userJobs);
	} else {
    	return -1;
	}
}

#############################################################################
### db_getDepartment:  To get db  departments based on filter category.
#############################################################################

sub db_getDepartment {

    my ($opcoid, $customer, $fromId) = @_;

    return () if $opcoid && !looks_like_number($opcoid);

    my $filterclause = "";
    $filterclause .= "AND t3p.rec_timestamp > DATE_ADD(now(), INTERVAL -$fromId DAY) " if length($fromId);

    my $rows = $Config::dba->hashed_process_sql("SELECT distinct(dept.name) AS DEPARTMENT, dept.id AS DEPTID from DEPARTMENTS AS dept 
												INNER JOIN TRACKER3PLUS t3p ON t3p.departmentvalue=dept.id
												INNER JOIN TRACKER3 t3  ON t3p.job_number=t3.job_number
												WHERE t3.opcoid=?  AND t3.agency=? AND t3.workflow != 'null' $filterclause", [ $opcoid, $customer ]);

    my @departments;
    foreach my $row (@$rows) {
        push(@departments, { DEPTID => $row->{DEPTID}, DEPARTMENTS => $row->{DEPARTMENT} });
    }

    return @departments;
} #db_getDepartment

#############################################################################
### db_getCurrDepartment:  To get current department
#############################################################################

sub db_getCurrDepartment {

    my ($departmentID) = @_;

    return () if $departmentID && !looks_like_number($departmentID);

    my ($department) = $Config::dba->process_oneline_sql("SELECT  dept.name
														FROM DEPARTMENTS dept 
														WHERE dept.id=? ", [ $departmentID ]);
    return $department;
} #db_getCurrDepartment

#############################################################################
### db_getCommetsData:  To get all comments..
#############################################################################

sub db_getCommetsData {

    my ($job_number)           = @_;
    my @commentsData          = ();

    return () if $job_number && !looks_like_number($job_number);

    my $rows = $Config::dba->hashed_process_sql("SELECT C.id, C.content, U.USERNAME, C.rec_timestamp, P.PRODUCTNAME, C.type_Of_Entry
											  	FROM comments C 
											  	INNER JOIN users U ON U.id=C.userid
												LEFT JOIN products P ON P.ID = C.lineitemid
											  	WHERE C.job_number=? 
											  	ORDER BY C.rec_timestamp DESC", [ $job_number ]);

    foreach my $row (@$rows) {
		my $rec_timestamp	= $row->{rec_timestamp};
		$rec_timestamp	= `TZ=GMT date -d "$rec_timestamp"`;
		my $content = $row->{content};
		$content = uri_escape_utf8( $content ) if defined $content;
        push(@commentsData, { COMMENTID => $row->{id}, CONTENT => $content, USERNAME => $row->{USERNAME}, PRODUCTNAME => $row->{PRODUCTNAME}, TYPE_OF_COMMENT => $row->{type_Of_Entry}, REC_TIMESTAMP => $rec_timestamp});
    }

    return \@commentsData;
}#  db_getCommetsData


########################################################################################
###      db_updatedatetime ( UPDATE DISPATCH DATE IN TRACKER3PLUS TABLE FROM ACTIONS MENU)
# For just a single job, please dont change it to use a job list
########################################################################################

sub db_updatedatetime {
    my($jobnumber, $dateval) = @_;

	my $datetime;
	if ( ( ! defined $dateval ) || ( $dateval eq '' ) ) { # Cater for blank Dispatch date, the column is NULLable
		($datetime) = $Config::dba->process_sql("UPDATE tracker3plus SET dispatchdate = NULL WHERE job_number = ?", [ $jobnumber ] );
	} else {	
		($datetime) = $Config::dba->process_sql("UPDATE tracker3plus SET dispatchdate = ? WHERE job_number = ?", [ $dateval, $jobnumber ] );
	}
    return $datetime;
}

########################################################################################
####      isDeleteEnabled ( Select enabledelete status from customers table based on id)
#########################################################################################

sub isDeleteEnabled {

    my ($customerid,$parentopcoid) = @_;

    return 0 if !General::is_valid_id($customerid);
    return 0 if !General::is_valid_id($parentopcoid);

    my ($enabled) = $Config::dba->process_oneline_sql("SELECT enabledelete
                        FROM CUSTOMERS WHERE id=? and opcoid = ?", [ $customerid,$parentopcoid ]);

    return $enabled;


}# isDeleteEnabled

#########################################################################################
# resetCheckInJob
#
#########################################################################################
sub resetCheckInJob
{
    my ($jobid) = @_;

    return 0 if $jobid && !looks_like_number($jobid);

$Config::dba->process_sql("UPDATE repocheckinqueue set checkingin = 0 where job_number = ?", [ $jobid ]);

    return 1;
}

#########################################################################################
# resetCheckOutJob
#
#########################################################################################
sub resetCheckOutJob
{
    my ($jobid) = @_;
    
    return 0 if $jobid && !looks_like_number($jobid);

$Config::dba->process_sql("UPDATE repocheckoutqueue set checkingout = 0 where job_number = ?", [ $jobid ]);
    
    return 1;
}  

#########################################################################################
# db_getDigitalIndexCustoLogName
#########################################################################################

sub db_getDigitalIndexCustoLogName {
	my ( $opcoID, $custID ) = @_;
    my ($customerlogo) = $Config::dba->process_oneline_sql("SELECT customerlogo FROM customers where id = ? and  opcoid = ?", [ $custID, $opcoID ]);
    return $customerlogo;
} #db_getDigitalIndexCustoLogName

sub db_getSingelAssetName {
    my $assetID                 = $_[0];
    my $type                    = $_[1] || '';
    my $singelAssetName_ref;
    if( $type ne 'Document')
    {
        ($singelAssetName_ref) = $Config::dba->process_oneline_sql("SELECT filename FROM jobassets where id = ? ",[ $assetID ]);
    }
    else
    {
        ($singelAssetName_ref) = $Config::dba->process_oneline_sql("SELECT documentname FROM JOBDOCUMENTS where id = ? ",[ $assetID ]);
    }
    return $singelAssetName_ref;
} #db_getSingelAssetName
sub getLastBriefComment {
	my ($job_number) = @_;
	my ($comment) = $Config::dba->process_oneline_sql("SELECT content FROM comments where job_number = ?  AND status = ?",[ $job_number, 1 ]);
	return $comment;
}	

########################################################################################
####  db_getCreatedbycount
#
########################################################################################

sub db_getCreatedbycount {

	my ( $userid, $opcoid) = @_;
	Validate::opcoid( $opcoid );
	Validate::userid( $userid );
    my ($getcount) = $Config::dba->process_oneline_sql("SELECT count(t.job_number) as createdbycount   FROM users u INNER JOIN tracker3 t ON t.employeeid = u.employeeid
                       INNER JOIN tracker3plus t3p ON t3p.job_number= t.job_number 
					   WHERE u.id= ?  AND t.opcoid = ? AND t3p.rec_timestamp >= DATE_SUB(CURDATE(), INTERVAL 30  DAY) ", [ $userid,$opcoid ]);
    return $getcount;

}

########################################################################################
#### db_getEmployeeid
#
########################################################################################

sub db_getEmployeeid {
	
	my ( $userid ) = @_;
    Validate::userid( $userid );
    my ($employeeid) = $Config::dba->process_oneline_sql("SELECT employeeid FROM users WHERE id = ? ", [ $userid ]);
    return $employeeid;
}

################################################################
# getJobQcMonitorStatus
#
################################################################
sub getJobQcMonitorStatus
{
	my ($jobid) = @_;

	return 0 if $jobid && !looks_like_number($jobid);
	my ($status) = $Config::dba->process_oneline_sql("SELECT count(JOB_NUMBER) 
																FROM tracker3plus 
																WHERE JOB_NUMBER = ?
																AND LOCATION = (
																	SELECT OPCOID
																	FROM operatingcompany
																	WHERE OPCONAME = 'QC_MONITOR'
																);", [ $jobid ]);
	return $status;
}
########################################################################################
###      db_update_requiredate ( UPDATE REQUIRED DATE IN TRACKER3 TABLE FROM DEVELOPERS)
########################################################################################

sub db_update_requiredate {
    my($jobnumber, $dateval) = @_;

    my $datetime;
    if ( ( ! defined $dateval ) || ( $dateval eq '' ) ) { # Cater for blank required date, the column is NULLable
        ($datetime) = $Config::dba->process_sql("UPDATE tracker3 SET requireddate = NULL WHERE job_number = ?", [ $jobnumber ] );
    } else {
        ($datetime) = $Config::dba->process_sql("UPDATE tracker3 SET requireddate = ? WHERE job_number = ?", [ $dateval, $jobnumber ] );
    }
    return $datetime;
}
########################################################################################
###      db_update_matdate ( UPDATE MATERIAL DATE IN TRACKER3 TABLE FROM DEVELOPERS)
########################################################################################

sub db_update_matdate {
    my($jobnumber, $dateval) = @_;

    my $datetime;
    if ( ( ! defined $dateval ) || ( $dateval eq '' ) ) { # Cater for blank material date, the column is NULLable
        ($datetime) = $Config::dba->process_sql("UPDATE tracker3 SET matdate = NULL WHERE job_number = ?", [ $jobnumber ] );
    } else {
        ($datetime) = $Config::dba->process_sql("UPDATE tracker3 SET matdate = ? WHERE job_number = ?", [ $dateval, $jobnumber ] );
    }
    return $datetime;
}
#update producer name in tracker 3 table
sub db_updateProducer {
	my($jobnumber, $producer) = @_;
	eval {
		Validate::job_number( $jobnumber );
		
		$Config::dba->process_sql("UPDATE tracker3 SET producer = ? WHERE job_number = ?", [ $producer, $jobnumber ] );
	};
    if ( $@ ) {
        cluck "ECMDBJob::db_updateProducer: $@";
    }
}
sub db_insertOne2edit {
	my($jobnumber, $doc_id, $doc_name) = @_; 	

    eval {
        Validate::job_number( $jobnumber );
		my ($exists) = $Config::dba->process_oneline_sql("SELECT 1 FROM one2edit_queue WHERE job_number=?", [ $jobnumber ]);
		$Config::dba->process_sql("delete from one2edit_queue WHERE job_number=?", [ $jobnumber ]) if ($exists);		
        $Config::dba->process_sql("INSERT into one2edit_queue (job_number, document_id, document_name, rec_timestamp) values (?,?,?,now())", [ $jobnumber, $doc_id, $doc_name ] );
    };  
    if ( $@ ) {
        die "ECMDBJob::db_insertOne2edit: $@";
    }
}
################################################################################
# get assignee id and qc status for given job
################################################################################
sub get_assigneeid_and_qcstatus
{
    my ($jobid) = @_;

    return 0 if $jobid && !looks_like_number($jobid);

    my ($userid,$qcstatus) = $Config::dba->process_oneline_sql("SELECT assignee,qcstatus FROM TRACKER3PLUS WHERE job_number=?", [ $jobid ]);
    return $userid,$qcstatus;
}

1;
