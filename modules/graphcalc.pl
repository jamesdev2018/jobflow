#!/usr/local/bin/perl
package GraphCalc;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );

# Home grown

use lib 'modules';

require "config.pl";

use DBAgent;

sub graphCalc
{
	my ($opcoid, $cust, $media, $status) = @_;
	
	##-----
	## CALCULATE THE DATES FOR DAYS RELATIVE TO TODAY
	my @days;
	my $beginoffset	= -3;
	my $endoffset	= +3;
	for(my $offset = $beginoffset; $offset <= $endoffset; $offset++)
	{
		my ($d, $m, $y) = (localtime(time() + (60 * 60 * 24 * $offset)))[3, 4, 5];
		push(@days, sprintf '%d-%02d-%2d', $y + 1900, $m + 1, $d);
	}
	
	##-----
	## GENERATE THE FILTER
	my $filterclause = "";
	$filterclause .= " AND t3.agency = '$cust' " if ( defined $cust );
	$filterclause .= " AND t3.WORKFLOW = '$media' " if ( defined $media );
	$filterclause .= " AND CAST(duedate AS DATE) IN ('" . join("', '", @days) . "') GROUP BY CAST(duedate AS DATE)" if (($status || "") eq "estimated");

	##-----
	## RUN THE QUERY
	my $rows = \();
	if($status eq "estimated")
	{
        $rows = $Config::dba->process_sql("SELECT ROUND(SUM(estimate) / 0.6) AS estimate, t3p.duedate AS duedate FROM TRACKER3PLUS t3p
					INNER JOIN TRACKER3 t3 ON t3p.job_number = t3.job_number
				WHERE (t3p.opcoid=? OR t3p.location=?) $filterclause
				ORDER BY t3p.rec_timestamp DESC", [ $opcoid, $opcoid ]);
	}
	elsif($status eq "actual")
	{
        	$rows = $Config::dba->process_sql("SELECT ROUND(SUM(estimate) / 0.6) AS estimate, t3p.duedate AS duedate FROM TRACKER3PLUS t3p
					INNER JOIN TRACKER3 t3 ON t3p.job_number = t3.job_number
					WHERE (t3p.opcoid=? OR t3p.location=?) $filterclause
					ORDER BY t3p.rec_timestamp DESC", [ $opcoid, $opcoid ]);
	}
	
	## COMPILE INTO THE OUTPUT PARAMETER
	my $output = "";  # will be "" if status (4th arg) is neither estimated or actual 
	if ( defined $rows ) {
		for(my $i = 0; $i < scalar(@$rows); $i++) {
			my ($time) = @{ get_row($rows, $i) };
			$output .= $time if defined $time;
		}
	}
	
	return $output;
}

1;

