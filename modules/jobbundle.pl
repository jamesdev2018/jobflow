#!/usr/local/bin/perl
package JobBundle;

use strict;
use warnings;

use Scalar::Util qw( looks_like_number );
use File::Path;
use File::Copy;
use LWP::Simple;
use JSON qw( decode_json );
use Net::FTP::File;
use URI::Escape;
use URI::URL;

use Template;
use Crypt::CBC;
use CGI::Session;
use CGI::Cookie;
use CGI;
use Data::UUID;
use Data::Dumper;
use Date::Simple ('date', 'today');
use JSON;
use POSIX qw/strftime/;
use Time::Local;
use Encode qw(decode encode);
use Cwd;
use POSIX qw( strftime );
use Carp qw/carp/;

# Home grown
use lib 'modules';

use Validate;

require "config.pl";
require "Mailer.pl";
require "streamhelper.pl";
require "jobpath.pl";
require 'databaseJobBundle.pl';

##########################################################################
# Template object
#
##########################################################################
my $tt = Template->new( { RELATIVE => 1, } ) || die "$Template::ERROR\n";

##########################################################################
# sendEmail
#
##########################################################################
sub sendEmail {
	my ( $query, $session ) = @_;
    my $vars;
	my $url	= Config::getConfig("bundle_download_url") || 'bundle_download_url_undef';
	my $bundle_id		= $query->param( "bundle_id" );
	my $jobid			= $query->param( "jobid" );
	my $sessionid		= $query->param( "sessionid" );
	my $rnd_no			= $query->param( "rnd_no" );
	my $email			= $session->param( 'email' );
	my $username		= $session->param( 'username' );
	my $fullname		= $session->param( 'fullname' );
	my $fromaddress		= 'jobflow@tagworldwide.com';
	my $subject			= "Bundle download link";
	my $smtp_gateway    = Config::getConfig("smtp_gateway");
	my $url_link = "";

	eval {
		die "Invalid bundle uuid" unless defined $bundle_id;
		Validate::job_number( $jobid );
		Validate::email( $email );

		# form the url link
		$url_link = $url."?bundleid=".$bundle_id."&action=downloadbundle";

		# form the email body
		my $body = "Hi $fullname, \n\n";
		$body .= "Here is the bundle download link for Job number:$jobid \n\n";
		$body .= "$url_link\n\n";
		$body .= "Thanks and Regards, \n";
		$body .= "JobFlow Team";

		# send the email with defined parameters
		Mailer::send_mail ( $fromaddress, $email, $body, $subject, { host => $smtp_gateway } );	

		$vars = {
			bundle_id => $bundle_id,
        };
	};
    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        $bundle_id = 'undef' unless defined $bundle_id;
        $vars = { error => $error, bundle_id => $bundle_id };
    }
    my $json = encode_json ( $vars );
    print $json;
    return $json;
}

##########################################################################
# bundleDownload
#
##########################################################################
sub bundleDownload {
	my ( $query, $session ) = @_;
	my ( $asset_types, $label, $vars, $job_number ) = undef;
	my @asset_list;
	my $bundle_id = $query->param( "bundleid" );
	my $jobbundle_url = Config::getConfig("jobbundle_url") || 'jobbundle_url_undef';
	my $creator = undef;
	my $userid = $session->param( 'userid' );

	# get session id and random number
	my $rnd_no = StreamHelper::createKey( $session );
    my $sessionid = $session->id;
	
	eval {
		die "Invalid bundle uuid" unless defined $bundle_id;

		# get bundle data using bundle id
		my @bundle_data = ECMDBJobBundle::getBundle( $bundle_id );
		die "Invalid bundle id: '$bundle_id'" unless ( scalar @bundle_data );
		
		foreach ( @bundle_data ) {
			$job_number = $_->{job_number};
        	$asset_types = $_->{asset_types} unless defined $asset_types;
			$creator = $_->{creator};
    	}

		# die "You are not allowed to browse this bundle as you are not the owner of the bundle." if ( $creator != $userid );

		# split asset types and keep it in an array
		my @assets = split( ',', $asset_types );

		# for each asset form asset_list with "asset" and "label"
		for my $asset ( @assets ) {
			$label = ECMDBJobBundle::getAssetTypeName( $asset );

			push( @asset_list, {
                        asset => $asset, label => $label,
            } );
		}

		# sort the asset list
		my @sorted_asset_list = sort { $a->{label} cmp $b->{label} } @asset_list;

		$vars = {
        	bundle_id	=> $bundle_id,
			bundle_url	=> $jobbundle_url,
			rnd_no		=> $rnd_no,
			sessionid	=> $sessionid,
			assets		=> \@sorted_asset_list,
			job_number	=> $job_number,
    	};

	};
    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        $bundle_id = 'undef' unless defined $bundle_id;
        $vars = { error => $error, bundle_id => $bundle_id };
    }

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/downloadbundle.html', $vars )
		|| die $tt->error(), "\n";
}

##########################################################################
# findAssets, find available job assets for job.
# Arguments ( query, session )
# Inputs
# - query.jobid
# Returns hash ref, keys are asset type, value hash { checked, enabled, label, asset_exists, files }
# where checked, enabled, asset_exists are 0 or 1,
# label is asset type label text
# files is list of files for asset type
##########################################################################
sub findAssets {
	my ( $query, $session ) = @_;
	my $vars;

	my $jobid = $query->param( "jobid" );

	eval {
		Validate::job_number( $jobid );

		# @TODO could change checkedAssetsExist to call getJobAssetTypes directly

		my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobid );
		my $asset_types = ECMDBJobBundle::getJobAssetTypes( $jobid, $customerid ); # ps doesnt need customerid arg

		# asset_types is hash ref, key is asset type, value is anon hash { checked, enabled, label }

		my $assets = ECMDBJobBundle::checkedAssetsExist( $jobid, $jobpath, $jobopcoid, $asset_types );

		# assets is hash ref, key is asset type, value is anon hash { asset_exists, files }
		# Merge the 2 hashes, so $assets is the combined copy

		my @asset_list;

		foreach my $k ( keys %$asset_types ) {
			my $h1 = $assets->{ $k };
			my $h2 = $asset_types->{ $k };

			push( @asset_list, {
					type => $k, checked => $h2->{checked}, enabled => $h2->{enabled}, label => $h2->{label},
					asset_exists => $h1->{asset_exists}, files => $h1->{files},
			} );
		}

		# sort the elements in @asset_list by label
		my @sorted_asset_list = sort { $a->{label} cmp $b->{label} } @asset_list;

		$vars = {
			asset_types => \@sorted_asset_list,
			jobid => $jobid,
		};
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$jobid = 'undef' unless defined $jobid;
		$vars = { error => $error, jobid => $jobid };
	}

	my $json = encode_json ( $vars );
	print $json;
	return $json;
}

##########################################################################
# makeBundle
# Handle makeBundle ajax call, when user clicks Download button
# Arguments ( query, session )
# Inputs
# - session.userid (can also extract username if needed)
# - query.jobid - job number
# - query.<asset type> - 0..n asset types (for each check box set)
#
##########################################################################
sub makeBundle {
	my ( $query, $session ) = @_;
	my $vars;
	my $assets = "";

	my $mode    = $query->param( "mode" ) || '';
	my $jobid   = $query->param( "jobid" );
	my $creator = $session->param( 'userid' );
	my $expiry_dt = Config::getConfig( "download_expiry" ) || 1;  # Expiry date (default +1 day)
	my $jobbundle_url = Config::getConfig("jobbundle_url") || 'jobbundle_url_undef';
	my @errormessages;

	# get session id and random number
	my $rnd_no = StreamHelper::createKey( $session );
	my $sessionid = $session->id;

	eval {
		Validate::job_number( $jobid );
		# get the job path
		# my ( $jobpath, $jobopcoid, $customerid ) = JobPath::getJobPath( $jobid );

		my ( $jobpath, $jobopcoid, $customerid, $location, $remoteCustomerId ) = JobPath::getJobPath( $jobid );
		die "Job $jobid does not exist" unless defined $jobpath;

		# Check user can see jobs customer
		StreamHelper::assert_customer_visible( $creator, $jobopcoid, $customerid, $location, $remoteCustomerId );

		my @jobs;
		push(@jobs, $jobid);

		# get job asset types
		my ($asset_types) = ECMDBJobBundle::getJobAssetTypes($jobid, $customerid);

		my %assets_arr_ref  = %$asset_types;

		# loop through job specific asset types
		my @asset_list;
		my @assets;

		foreach my $asset_type ( keys %$asset_types ) {
			my $h = $asset_types->{ $asset_type };

			my $a = $query->param( $asset_type ) || '';
			my $l = $h->{label};
			push( @assets, $asset_type ) if ( $a eq 'true' );

			#$asset_list{ $asset_type  } = { asset => $asset_type, label => $l } if ( $a eq 'true' );

			push( @asset_list, {
					asset => $asset_type, label => $l,
			} ) if ( $a eq 'true' );
		}

		die "No assets selected" unless ( scalar keys @asset_list );
		my $assets = join( ',', @assets );

		my $ug      = new Data::UUID;
		my $uuid    = $ug->create();
		my $uuidstr = $ug->to_string( $uuid );

		# call create bundle with selected assets
		my ( $bundle_id, $bundle_uuid ) = ECMDBJobBundle::create( $jobid, $creator, $expiry_dt, $assets, $uuidstr );

		# No need to add recipient, getjobbundle should match bundle creator id.
		# ECMDBJobBundle::addRecipient( $bundle_id, $email, $username, $password );

		# @TODO need to add rows to job_bundle_files, using addFileToBundle( $bundle_id, $asset_type, $filename )
		# Would only need this for aux_files, where there is an indetermined list of files in the directory
		# so getjobbundle needs to know which files, but for the time being assume all files.

		# Add row to job_bundle_history ?? no function exists yet, need something to
		# insert a row into job_bundle_history ( id, bundle_id, email )
		# its intended to record when the user has downloaded the bundle.

		# Add auditlog entry
		ECMDBAudit::db_auditLog( $creator, "JOBBUNDLE", $jobid, "Created job bundle $bundle_id, assets $assets " );

		# sort the elements in @asset_list by label
		my @sorted_asset_list = sort { $a->{label} cmp $b->{label} } @asset_list;

		$vars = {
			jobid => $jobid,
			bundle_id => $bundle_uuid,
			assets => \@sorted_asset_list,
			url => $jobbundle_url,
			rnd_no => $rnd_no,
			sessionid => $sessionid,
			postValue => "gethires",
		};
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$jobid = 'undef' unless defined $jobid;
		$vars = { error => $error, jobid => $jobid };
	}

	my $json = encode_json ( $vars );
	print $json;
	return $json;
}

1;

