#!/usr/local/bin/perl
package ECMDBCustomerUser;

use warnings;
use strict;

use Scalar::Util qw( looks_like_number );

use CGI::Carp qw/cluck/;

# Home grown

use lib 'modules';

use Validate;

require "config.pl";

##############################################################################
#		getCustomerUsers
#		get the users belonging to this customer 
#
##############################################################################
sub getCustomerUsers {
	my ($customerid) = @_;

	my $users;
	eval {
		Validate::customerid( $customerid );
		$users = $Config::dba->hashed_process_sql(
			"SELECT u.id AS userid, u.username, u.fname, u.lname, r.role FROM users u
				INNER JOIN customeruser cu ON u.id=cu.userid INNER JOIN roles r ON r.id=u.role
				WHERE cu.customerid=? AND u.STATUS=1 ORDER BY u.username", [ $customerid ]);
	};
	if ( $@ ) {
		cluck "getCustomerUsers: customerid " . ( $customerid || 0 ) . ' ' . $@;
	}
	return $users ? @$users : ();
}

sub getUsersCustomers {
	my ( $userid ) = @_;

	my $rows;
	eval {
		Validate::userid( $userid );

		$rows = $Config::dba->process_sql( "SELECT customerid FROM customeruser WHERE userid = ?", [ $userid ] );
	};
	if ( $@ ) {
		$userid = 0 unless defined $userid;
		carp "getUsersCustomers: userid $userid, $@";
	}
	return () unless defined $rows;
	return @$rows;
}


##############################################################################
#      isUserAssignedToCustomer 
#
##############################################################################
sub isUserAssignedToCustomer {
	my ($userid, $customerid) = @_;

	my $exists;
	eval {
		Validate::userid( $userid );
		Validate::customerid( $customerid );
	
		($exists) = $Config::dba->process_oneline_sql(
			"SELECT 1 FROM customeruser WHERE userid=? AND customerid=?",
			[ $userid, $customerid ]);
	};
	if ( $@ ) {
		cluck "isUserAssignedToCustomer: $@";
		$exists = 0;
	}
	return $exists || 0;
}

##############################################################################
#		addCustomerUser
#		add this user to this customer
##############################################################################
sub addCustomerUser {
	my ($customerid, $userid) = @_;
	
	eval {
		Validate::userid( $userid );
		Validate::customerid( $customerid );
		$Config::dba->process_sql("INSERT INTO customeruser (customerid, userid) VALUES (?, ?)", [ $customerid, $userid ]);
	};
	if ( $@ ) {
		cluck "addCustomerUser: $@";
		return 0;
	}
	return 1;
}

##############################################################################
#		removeCustomerUser
#
##############################################################################
sub removeCustomerUser {
	my ( $customerid, $userid ) = @_;
	
	eval {
		Validate::userid( $userid );
		Validate::customerid( $customerid );
		$Config::dba->process_sql(
			"DELETE FROM customeruser WHERE customerid=? AND userid=?", [ $customerid, $userid ]);
	};
	if ( $@ ) {
		cluck "removeCustomerUser: $@";
		return 0;
	}
	return 1;
}

##############################################################################
#       getOpcoOperators
#      
##############################################################################
sub getOpcoOperators {
	my ($opcoid) = @_;

	my $operators;
	eval {
		Validate::opcoid( $opcoid );	
		$operators = $Config::dba->hashed_process_sql(
			"SELECT DISTINCT(cu.userid) AS id, u.username FROM customeruser cu
			INNER JOIN users u ON u.id=cu.userid
			INNER JOIN customers c ON c.id=cu.customerid
			WHERE c.opcoid=? ORDER BY u.username", [ $opcoid ]);
	};
	if ( $@ ) {
		cluck "getOpcoOperators: opcoid: " . ( $opcoid || 0 ) . ' ' . $@;
	}	
	return $operators ? @$operators : ();
}

##############################################################################
#       getUsersCustomerNamesByOpco
#		returns a list of customer names, that the user can select at the current Opco
#
##############################################################################

sub getUsersCustomerNamesByOpco {
	my ( $userid, $opcoid ) = @_;

	my $rows;
	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );

		$rows = $Config::dba->process_sql( 
			"SELECT DISTINCT customer FROM customers c INNER JOIN customeruser cu ON c.id = cu.customerid WHERE cu.userid = ? AND c.opcoid = ?", 
				[ $userid, $opcoid ] );
	};
	if ( $@ ) {
		$userid = 0 unless defined $userid;
		$opcoid = 0 unless defined $opcoid;
		carp "getUsersCustomerNamesByOpco: userid $userid, opcoid $opcoid $@";
	}
	$rows = \() unless defined $rows;
	return $rows;
}

# Get a list of customer ids that match the set of customer names
# that the user is allowed to see at the current Opco.
# eg if the user is at Remote Hub One and can see customers AMERICAN_EXPRESS and TAG_LONDON
#    then they can see any job with either of those customers at any Opco
# 
# Arguments
# - userid, used with table customeruser to get initial list of customer ids (they can see)
# - opcoid, used with customer table to get set of customer ids for current opco
#    (hence result of 1st join is set of customerids that the user can see at opcoid)
#    This is used to get the set of customer names, then expanded to get the set of all 
#    customerids than have those names.
# Returns 
# - array (ref) of customerid

sub getUserCustomerIdsByOpco {
	my ( $userid, $opcoid ) = @_;

	my @ids = $Config::dba->process_onerow_sql(
		"SELECT id FROM customers c1 WHERE c1.customer IN " .
			"(SELECT DISTINCT c2.customer FROM customeruser cu INNER JOIN customers c2 ON c2.id = cu.customerid " .
			" WHERE cu.userid=? AND c2.opcoid=?)",
		[ $userid, $opcoid ] );
	return \@ids;
}
sub getUserCustomerIdsByUser {
        my ( $userid, $opcoid ) = @_;

        my @ids = $Config::dba->process_onerow_sql(
                "SELECT id FROM customers c1 WHERE c1.customer IN " .
                        "(SELECT DISTINCT c2.customer FROM customeruser cu INNER JOIN customers c2 ON c2.id = cu.customerid " .
			" WHERE cu.userid=? )",
                [ $userid ] );
        return \@ids;
}

1;

