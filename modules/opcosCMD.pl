#!/usr/local/bin/perl
package OpcosCMD;

use strict;
use warnings;

use Template;
use Scalar::Util qw( looks_like_number );
use XML::Simple;

use lib 'modules';

require "config.pl";
require "general.pl";

my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");

my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");

my $coldFusionServer	= Config::getConfig("coldFusionServer" );

##############################################################################
# the template object 
##############################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";


##############################################################################
#       OpcoAdminCMD
#
##############################################################################
sub OpcoAdminCMD {
	my $query 		= $_[0];
	my $session 	= $_[1];
	my $opcoid		= $query->param( "opcoid" );
	my @opcos;
	my $sidemenu   = $query->param( "sidemenu" );
	my $sidesubmenu= $query->param( "sidesubmenu" );
	my $roleid     = $session->param( 'roleid' );
	
	my $xml  = new XML::Simple;
	
	#default to Tag London
	if ( ( ! defined $opcoid ) || ( $opcoid eq '' ) ) {
	    $opcoid = "5621";
	}
	
	my $curlString  = "curl -sS '$coldFusionServer/tagcmd/Tracker/excel/accounts_reports/Batch/opcoAdminService.cfm?opcoid=$opcoid'";
	my $curlString_ = "curl -sS '$coldFusionServer/tagcmd/Tracker/excel/accounts_reports/Batch/OpcoService.cfm'";
	
	my $response = `$curlString`;
	my $response_ = `$curlString_`;
	
	my $data = $xml->XMLin($response, SuppressEmpty => '');

	#my @opcos = ECMDBOpcos::getOperatingCompanies();
	my $opcos = $xml->XMLin($response_);
	
	
	   foreach my $opco (@{$opcos->{opcoCMD}}) {
	      my %row = (
			opcoid 		=> $opco->{opcoID},
			opconame 	=> $opco->{opcoName},
		  );
		push ( @opcos, \%row );
	   }	
	
	
	my $vars = {
		opcos				=> \@opcos,
		opCoID				=> $data->{opCoID},
		opCustID			=> $data->{opCustID},
		opType				=> $data->{opType},
		opName				=> $data->{opName},
		opServer			=> $data->{opServer},
		opProdRoot			=> $data->{opProdRoot},
		opMBRoot			=> $data->{opMBRoot},
		opMBTemplate			=> $data->{opMBTemplate},
		opArchiveRoot			=> $data->{opArchiveRoot},
		opDaysRetainedBeforeArchive	=> $data->{opDaysRetainedBeforeArchive},
		opManifestPathExport		=> $data->{opManifestPathExport},
		opManifestPathImport		=> $data->{opManifestPathImport},
		opArchiveRestoreEmail		=> $data->{opArchiveRestoreEmail},
		opArchiveServer			=> $data->{opArchiveServer},
		opArchiveRestorePath		=> $data->{opArchiveRestorePath},
		opPdfWorkFlowSettings		=> $data->{opPdfWorkFlowSettings},
		sidemenu			=> $sidemenu,
		sidesubmenu			=> $sidesubmenu,
		roleid				=> $roleid,
		coldfusionserver		=> $coldFusionServer,
	};

	General::setHeaderVars( $vars, $session );

	$tt->process( 'ecm_html/editoperatingcompany_CMD.html', $vars )
    		|| die $tt->error(), "\n";
}


##############################################################################
#       ArchiveScheduler
#
##############################################################################
sub ArchiveScheduler {
	my $query 		= $_[0];
	my $session 	= $_[1];
	my $formPosted  = $query->param( "formPosted" ) ||'';
	my $updated     = 0;
	my $opcoid_scheduled;
	my $task;
	my $queued;
	my $qOpco;
	my @opcos;
	my @formResults;
	my @schedules;
	my $sidemenu   = $query->param( "sidemenu" );
	my $sidesubmenu= $query->param( "sidesubmenu" );
	my $roleid     = $session->param( 'roleid' );

	if ( $formPosted eq "yes" ) { 	
		@formResults = $query->param( "opcoid" );
		 
		my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
		my $sql = qq{ delete from archiverschedule };			
		my $sth = $dbh->prepare( $sql );
		$sth->execute();
		 
		#Remove previous schedule and re-create // opcos cannot be duplicated
		foreach (@formResults)
		{
			$sql = qq{ INSERT INTO archiverschedule (opcoid, archive) values (?, 1) ON DUPLICATE KEY UPDATE archive = 1 };
			$sth = $dbh->prepare( $sql );
			$sth->execute($_);
		}
		$sth->finish();
		$dbh->disconnect();
		$updated = 1;
	}
	
	my $dbh_ = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql_ = qq{ SELECT opcoid, task FROM archiverschedule where archive =1};
	my $sth_ = $dbh_->prepare( $sql_ );
	$sth_->execute();

	$sth_->bind_columns( \$opcoid_scheduled, \$task );
	
	  while ( $sth_->fetch() ) {
		my %row = (
			opcoid_scheduled => $opcoid_scheduled,
			task             => $task,
		);
		push ( @schedules, \%row );

	}
	
	$sth_->finish();
	$dbh_->disconnect();
	
	my $xml  = new XML::Simple;
	
	#my @opcos = ECMDBOpcos::getOperatingCompanies();
	my $curlString_ = "curl -sS '$coldFusionServer/tagcmd/Tracker/excel/accounts_reports/Batch/OpcoService.cfm'";
	my $response_   = `$curlString_`;
	
	my $opcos = $xml->XMLin($response_, SuppressEmpty => '');
	
	   my ($dbha, $sqla, $stha);
	   
	   foreach my $opco (@{$opcos->{opcoCMD}}) {  
	   
	      $qOpco = $opco->{opcoID};
		  
	      $dbha = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	      $sqla = qq{ SELECT count(*) as queued FROM Archiver INNER JOIN tracker3plus on Archiver.jobid = tracker3plus.job_number WHERE tracker3plus.opcoid = ? AND State = 1};
	      $stha = $dbha->prepare( $sqla );
	      $stha->execute($qOpco);
	      $stha->bind_columns( \$queued );
		  
		  if ( $stha->fetch() ) {
		      $queued = $queued;
	      } else {
		      $queued = 0;
		  }
		  
		  $stha->finish();
	      $dbha->disconnect();
		  
	   
	      my %row = (
			opcoid 		=> $opco->{opcoID},
			opconame 	=> $opco->{opcoName},
			retention   => $opco->{opcoDaysRetainedBeforeArchive},
			queued      => $queued,
		  );
		push ( @opcos, \%row );
	   }	
	   
	
	my $vars = {
	   opcos       => \@opcos,
		updated     => $updated,
		schedules   => \@schedules,
      sidemenu    => $sidemenu,
      sidesubmenu => $sidesubmenu,
      roleid      => $roleid,
	};

	General::setHeaderVars( $vars, $session );
	
	$tt->process( 'ecm_html/archivescheduler.html', $vars )
    		|| die $tt->error(), "\n";
}


##############################################################################
##############################################################################
1;
