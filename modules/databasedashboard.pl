#!/usr/local/bin/perl
package ECMDBDashboard;

use strict;
use warnings;

# use Switch; # doesnt use switch statement
use Scalar::Util qw( looks_like_number );
use JSON;
use Carp qw/carp cluck/;
use Time::HiRes qw/time/;
use Encode;
use utf8;

use lib 'modules';

use CTSConstants;
use Validate;

require "config.pl";
require "Mailer.pl";
require "audit.pl";
require "general.pl";

my $database        = Config::getConfig("databaseschema");
my $data_source     = Config::getConfig("databaseserver");
my $db_username     = Config::getConfig("databaseusername");
my $db_password     = Config::getConfig("databasepassword");
my $fromemail           = Config::getConfig("fromemail");
my $supportemail2       = Config::getConfig("supportemail2");
my $longQueryPeriod = Config::getConfig("longqueryperiod") || 10; # Log SQL query in db_getJobs if > this period in seconds
my $rowscount           = Config::getConfig("jobrowslimit") || 1000;


#######################################################################################
###   db_getcalendarEventLists: To get all calender events from DB.
#######################################################################################

sub db_getcalendarEventLists {
	my ($event_id) = @_;
    my @calendarEvent   = ();
	my $calendarEvents_comment = undef;
    eval {
        my $calendarEvents_ref		= $Config::dba->hashed_process_sql( "SELECT ID, EVENT_NAME FROM calendar_events" );
        foreach my $event_ref ( @$calendarEvents_ref ) {
            push ( @calendarEvent, { EVENT_ID => $event_ref->{ID}, EVENT_TYPE => $event_ref->{EVENT_NAME} } );
        }
		if ($event_id != ''){
 	    	($calendarEvents_comment)   = $Config::dba->process_oneline_sql( "SELECT comments FROM user_calendar_mapping WHERE id = ?",[$event_id] );
		}
    };
    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        carp "db_getcalendarEventLists: $error";
        return 0;
    }
	if ($event_id != ''){
    	return \@calendarEvent,$calendarEvents_comment;
	}else{
	    return \@calendarEvent;
	}
} #db_getcalendarEventLists


############################################################################
### db_getCalendarEvents:
#############################################################################

sub db_getCalendarEvents {

    my $opcoid                  = $_[0];
    my $selectedUserSkillID     = $_[1];
    my $whereClause             = "";
    my $bindColumn              = undef;
    my @event                   = ();

    if ( $selectedUserSkillID !~ /^ALL$|^$/i ) {
        $whereClause = " AND USM.SKILLID IN ( $selectedUserSkillID ) AND USM.OPTIONS = 1";
    }
    my $rows = $Config::dba->hashed_process_sql( 
		"SELECT distinct(UCM.id), UCM.userid, CE.event_name, CE.event_color, UCM.description, UCM.start, UCM.end, UCM.allday, UCM.who, UCM.description, UCM.comments
			FROM user_calendar_mapping UCM
			LEFT OUTER JOIN userskillsmapping USM ON USM.USERID = UCM.userid
			INNER JOIN calendar_events CE ON CE.ID = UCM.event_title
			WHERE UCM.OPCOID = ? $whereClause ", [ $opcoid ] );

    foreach my $row (@$rows) {
        push(@event, {  userid => $row->{userid}, eventid => $row->{id}, title => $row->{event_name}, description => $row->{description},
                        start => $row->{start}, end => $row->{end}, allday => $row->{allday},color => $row->{event_color}, who => $row->{who}, projdesc => $row->{description}, projcomments => $row->{comments} }
        );
    }

    return \@event;
}#db_getCalendarEvents

##############################################################################
##       Calendar Add Event
###############################################################################
sub db_addcalendarevent {

    my ( $who, $title, $start, $end, $allday, $gusername, $opcoID, $projectDesc, $projectComment ) = @_;

    $Config::dba->process_sql(
		"INSERT INTO user_calendar_mapping (userid,event_title,start,end,allday,added_timestamp,who, OPCOID,description,comments )
		VALUES (?, ?, ?, ?, ?,now(),?, ?, ?, ? ) ", 
		[ $who, $title, $start, $end, $allday,$gusername, $opcoID, $projectDesc, $projectComment ] );
}

##############################################################################
##       Calendar update Event
###############################################################################
sub db_updatedcalendarevent {
	my ( $title, $start, $end, $allday, $eventid, $projectdesc, $comments ) = @_;

	$Config::dba->process_sql(
		"UPDATE user_calendar_mapping SET event_title=?,start=?,end=?, allday=?, added_timestamp=now(), description=?, comments=? WHERE id=? ", 
			[ $title, $start, $end, $allday, $projectdesc, $comments, $eventid ] );

}

##############################################################################
##       Delete Event db_dashboardcalendardelete
###############################################################################
sub db_dashboardcalendardelete{
    my ($event_delete) = @_;

    $Config::dba->process_sql("DELETE FROM user_calendar_mapping WHERE id=?", [ $event_delete ] );

}

sub boxcountshow
{
    my ( $opcoid, $cust, $dept, $media, $status,$selectedDay ) = @_; 
    my $filterclause = "";
    if ($status)
    {
        if($status eq "R")
        {
            $filterclause .= "AND ( t3p.qcstatus = '3' OR t3p.operatorstatus = 'R' ) AND t3.requireddate LIKE '%$selectedDay%'";
        }
        elsif($status eq "1" )
        {
            $filterclause .= "AND t3p.qcstatus = '$status' AND t3.requireddate LIKE '%$selectedDay%'";
        }
        elsif($status eq "Over")
        {
            $filterclause .= "AND now() > t3.requireddate AND t3.requireddate LIKE '%$selectedDay%' AND t3p.qcstatus NOT in ('2')";
        }
        elsif($status eq "U,A")
        {
            $filterclause .= "AND (t3p.operatorstatus = 'U' OR t3p.operatorstatus = 'A') AND t3.requireddate LIKE '%$selectedDay%'";
        }
        elsif($status eq "W,P")
        {
            $filterclause .= "AND (t3p.operatorstatus = 'W' OR t3p.operatorstatus = 'P') AND t3.requireddate LIKE '%$selectedDay%'";
        }
    elsif ($status eq "Totjobs")
    {   
        $filterclause .= "AND (t3p.operatorstatus IN ('U','A','W','P','R') OR t3p.qcstatus in ('1','3')) AND t3.requireddate LIKE '%$selectedDay%'";
    }   

    }

    if ($cust && $cust !~ /^null$/i )   { $filterclause .= " AND t3.agency IN('"           . join("','", split(/,/, $cust))            . "')"; } 
    if ($dept && $dept !~ /^null$/i )   { $filterclause .= "AND t3p.departmentvalue IN('"           . join("','", split(/,/, $dept))            . "')"; }
    if ($media && $media !~ /^null$|^ALL$/i )   { $filterclause .= "AND t3.WORKFLOW IN('"           . join("','", split(/,/, $media))            . "')"; }

    my ( $numjobs, $estimatedHoursCount ) = $Config::dba->process_oneline_sql("SELECT COUNT(t3.job_number), SUM(t3p.ESTIMATE) FROM TRACKER3 t3
                                                       INNER JOIN TRACKER3PLUS t3p ON t3p.job_number=t3.job_number
                                                       WHERE (t3p.location=? ) $filterclause AND t3.productionStatus NOT IN  (3, 4, 8, 9, 12) 
                                                       AND t3p.status NOT IN (4, 5, 6)
                                                       ORDER BY t3.job_number DESC", [ $opcoid ]);
return $numjobs, $estimatedHoursCount;
}
sub db_avlresources
{
    my ( $opcoid, $selectedUserSkill,$selectedDay ) = @_;
    my $totalResource   = 0;
    my $whereClause     = "";
    my $bindColumns     = "";
    my @db_resources    = ();

    $bindColumns    =  $opcoid ;
    if ( $selectedUserSkill && $selectedUserSkill !~ /^ALL$/i && $selectedUserSkill !~ /^NULL$/i) {
        $whereClause    = "AND (usm.SKILLID IN ($selectedUserSkill) AND usm.OPTIONS = 1)";
    }

    eval {
        Validate::opcoid( $opcoid );
        die "Invalid selected user skill" unless defined $selectedUserSkill;
        die "Invalid date" unless defined $selectedDay;
		# PD-3516 Added (u.RESOURCESTATUSID!=6 AND u.RESOURCESTATUSID!=0) to hide the users thar are not logged into the system. 
		# Some users having default value as 0. Some users having "6" which means Workstation Off. So both cases are inactive users.
		# RESOURCESTATUSID used to track the user current status. i.e Jobflow Active/JobFlow Logged In/Workstation Unlocked/Workstation On/Workstation Locked/Workstation Off
        my $rows = $Config::dba->hashed_process_sql(
            "SELECT IFNULL(SUM(t3p.estimate), '0.00')  AS estimate, u.id AS USERID,
                                            CASE WHEN (u.RESOURCESTATUSID='2') THEN
                                                CASE WHEN CAST(TIME_TO_SEC(TIMEDIFF(NOW(),u.LAST_LOGGEDIN)) / 3600 AS UNSIGNED) >= 24 THEN DATE_FORMAT(u.LAST_LOGGEDIN, '%H:%i (%a %D)')
                                                     ELSE DATE_FORMAT(u.LAST_LOGGEDIN, '%H:%i')
                                                END
                                            END AS LAST_LOGGEDINTIME,
                                                CASE WHEN CAST(TIME_TO_SEC(TIMEDIFF(NOW(),u.LAST_OS_LOGGEDIN)) / 3600 AS UNSIGNED) >= 24 THEN DATE_FORMAT(u.LAST_OS_LOGGEDIN, '%H:%i (%a %D)')
                                                     ELSE DATE_FORMAT(u.LAST_OS_LOGGEDIN, '%H:%i')
                                                END AS LAST_OS_LOGGEDIN,
                                                CASE WHEN rs.colour IS NULL OR rs.colour = '' 
                                                    THEN 'red' 
                                                    ELSE rs.colour 
                                                END AS colour,  
                                                CASE 
                                                    WHEN ACH.ADJUSTEDHOURSSTARTS <> '' && ACH.ADJUSTEDHOURSENDS <> '' && DATEDIFF(\'$selectedDay\',ACH.REC_TIMESTAMP) = '' then 
                                                        CASE 
                                                            WHEN DATEDIFF(\'$selectedDay\',CURDATE()) > 0 THEN timediff(U.WORKHOURSENDS,U.WORKHOURSSTARTS)
                                                            WHEN timediff(ACH.ADJUSTEDHOURSENDS,CURTIME()) <= 0 THEN '00:00'
                                                            WHEN timediff(CURTIME(),ACH.ADJUSTEDHOURSSTARTS) <= 0 THEN '00:00'
                                                            ELSE timediff(ACH.ADJUSTEDHOURSENDS,CURTIME())
                                                        END
                                                    ELSE 
                                                        CASE 
                                                            WHEN DATEDIFF(\'$selectedDay\',CURDATE()) > 0 THEN timediff(U.WORKHOURSENDS,U.WORKHOURSSTARTS)
                                                            WHEN timediff(U.WORKHOURSENDS,CURTIME()) <= 0 THEN '00:00'
                                                            WHEN timediff(CURTIME(),U.WORKHOURSSTARTS) <= 0 THEN '00:00'
                                                        ELSE timediff(U.WORKHOURSENDS,CURTIME())
                                                        END
                                                END AS AVAILABLE_HOURS,
                                                timediff(U.WORKHOURSENDS,U.WORKHOURSSTARTS) AS RESOURCE_HOURS,
                                                u.username AS username, u.update_time AS userlogin, u.COST_CENTER, OC.OPCONAME, CONCAT(u.fname,' ',u.lname) as user_full_name     FROM USEROPCOS us
                                    INNER JOIN USERS u ON u.id=us.userid AND (u.RESOURCESTATUSID!=6 AND u.RESOURCESTATUSID!=0) 
                                    LEFT OUTER JOIN TRACKER3PLUS t3p ON t3p.assignee=u.id
                                    LEFT OUTER JOIN RESOURCESTATUS rs ON u.RESOURCESTATUSID=rs.id
                                    LEFT OUTER JOIN adjustedcontractualhours ACH ON U.id = ACH.USERID
                                    LEFT OUTER JOIN userskillsmapping usm on usm.USERID=us.userid
                                    INNER JOIN operatingcompany OC ON OC.OPCOID = u.opcoid
                                    WHERE (u.opcoid=?) and u.RESOURCESTATUSID IN (1,2) and u.status=1 $whereClause
                                    GROUP BY u.username
                                    ORDER BY colour ASC, u.username ASC", [$bindColumns] );

my $unavail = $Config::dba->process_sql("select userid from user_calendar_mapping UCM where '$selectedDay' between UCM.start and UCM.end and UCM.allday = 1 and UCM.event_title = 2");
    my $selectedDaytime = $selectedDay." 00:00:00";
    my @planner_userIDs = ();
    my $plannerdays =undef;
    my $plannersec=undef;
#   my $arrasize = @$plannerassign;
    my @unavailable_userIDs = ();
    foreach my $userid_ref (  @$unavail) {
        push (@unavailable_userIDs, $userid_ref->[0] );
    }

    foreach my $row ( @$rows ) {
	}
	

    foreach my $row ( @$rows ) {
        #my $validUser           = 1;
        my $db_enabledUsrSkill  = undef;
        my $db_assigned_hours   = undef;
        my $userID              = $row->{USERID};
        ## Make sure the userID isn't in this list (replacement for the ~~ experimental smartmatch)
        if (!grep(/^$userID$/, @unavailable_userIDs)) {
                if ( $userID ) {
                    ( $db_enabledUsrSkill ) = $Config::dba->process_oneline_sql(
                        "SELECT GROUP_CONCAT(CONCAT(US.type) ORDER BY US.type ASC SEPARATOR ',') AS userSkill
                            FROM userskillsmapping USM
                            JOIN user_skills US ON US.id = USM.SKILLID
                            WHERE USM.userid=? AND USM.OPTIONS=1", [ $userID ]);

                    ( $db_assigned_hours ) = $Config::dba->process_oneline_sql(
                        "SELECT REPLACE(SUM(estimate),'.',':') 
                        FROM tracker3plus t3p  INNER JOIN tracker3 t3 ON t3.job_number=t3p.job_number
                        WHERE t3p.assignee=? AND t3p.operatorstatus IN ('A', 'W') AND  t3.requireddate like '%$selectedDay%'  ;", [ $userID ]);
                }

         ($plannerdays,$plannersec) = $Config::dba->process_oneline_sql("select timestampdiff(day,start,end),timestampdiff(second,start,end) from user_calendar_mapping where event_title=7 and userid=? and '$selectedDaytime' between start and end",[$userID]);
    if($plannerdays !=0 && $plannersec !=0){
        $plannersec /= $plannerdays;
    }
    my $plannerhrs = convert_hhmm_to_seconds($db_assigned_hours);
    $plannersec += $plannerhrs;
    my $assined_plannersec=$plannersec;
    $db_assigned_hours = convert_seconds_to_hhmm($plannersec);

    if ( ( ! defined $db_assigned_hours ) || ( $db_assigned_hours == 0 ) ) {
               $db_assigned_hours = "00:00";
        } elsif($db_assigned_hours < 10) {
            $db_assigned_hours = "0".$db_assigned_hours;
          }
    $db_assigned_hours=time_format($db_assigned_hours);
    $row->{userSkill} = $db_enabledUsrSkill;

    #we need display HH:MM only.
    if ( $row->{RESOURCE_HOURS} ne "" && $row->{RESOURCE_HOURS} =~ /(.*):(.*):(.*)/ ) {
        $row->{RESOURCE_HOURS}  = "$1" . ":" . "$2" if ( $1 ne "" && $2 ne "" );
    } else {
            $row->{RESOURCE_HOURS}  = "00:00";
           }

    if (  $row->{AVAILABLE_HOURS} ne "" && $row->{AVAILABLE_HOURS} =~ /(.*):(.*):(.*)/ ) {
        $row->{AVAILABLE_HOURS} = "$1" . ":" . "$2" if ( $1 ne "" && $2 ne "" );
    } else {
            $row->{AVAILABLE_HOURS}  = "00:00";
           }

    # Here calculated avilable hour based on assigned hours
    my $availablehour=$row->{AVAILABLE_HOURS};
    $plannersec=0;
    my $available_plannerhrs = convert_hhmm_to_seconds($availablehour);
        $plannersec += $available_plannerhrs;
    my $availableplan=$plannersec-$assined_plannersec;
    my $available_hours;
    if($availableplan < 0 )
        {
        $available_hours="00:00";
    }
    else
    {
         $available_hours = convert_seconds_to_hhmm($availableplan);
    }
    $available_hours=time_format($available_hours);
               push(@db_resources, {
                    estimate => $row->{estimate},
                    USERID => $row->{USERID},
                    LAST_LOGGEDINTIME => $row->{LAST_LOGGEDINTIME},
                    RESOURCE_HOURS => $row->{RESOURCE_HOURS},
                    userSkill => $db_enabledUsrSkill,
                    ASSIGNED_HOURS => $db_assigned_hours,
                    AVAILABLE_HOURS => $available_hours,
                    username => $row->{user_full_name},
                    userlogin => $row->{userlogin},
                    colour => $row->{colour},
                    COST_CENTER => $row->{COST_CENTER},
                    OPCONAME => $row->{OPCONAME},
                    LAST_OS_LOGGEDIN => $row->{LAST_OS_LOGGEDIN} } ); #if ( $validUser == 1 );

                $totalResource = $totalResource + 1 if ( $row->{userlogin} ne "" );
            }
        }
    };
    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        carp "db_avlresources: $error";
    }
    return $totalResource, \@db_resources;
}
############################################################################
#   db_getDepartments()
#
###########################################################################
sub db_getDepartments {
    my @departments;

    my $opcoid  = $_[0];
    my $rows = $Config::dba->hashed_process_sql("SELECT d.id,d.name FROM departments d INNER JOIN  departmentdetails dt ON dt.departmentid=d.id where opcoid=?", [ $opcoid]);
    if ( defined $rows ) {
        push( @{ $rows }, { id => '0', name => 'NONE'} );
    }
    return $rows ? @$rows : ();
}
###########################################################################
#  db_getCustomers()
#
###########################################################################
sub db_getCustomers{

    my $opcoid  = $_[0];

    my $rows = $Config::dba->hashed_process_sql("SELECT id, customer as agency  FROM CUSTOMERS WHERE opcoid = ?", [ $opcoid]);
        my $index = 0;
        my @agencies;
        foreach my $row (@$rows)
        {
                $index++;
                push(@agencies, { id => "CUST_$index", agency => $row->{agency}, name => $row->{agency} });
        }

        return @agencies;


}
##############################################################################
#        db_resourceGetjobs
#
##############################################################################
sub db_resourceGetjobs {
    my ($opcoid, $userid, $boxselecteddate, $agencyFilter, $locationfilter, $deptFilter, $mediatypeFilter, $boxpending, $boxqcreject, $boxqcwait,$boxtotaljobs, $boxoversch ) = @_;

    $agencyFilter = '' unless defined $agencyFilter;
    $boxpending = '' unless defined $boxpending;
    $boxqcreject = '' unless defined $boxqcreject;
    $locationfilter = '' unless defined $locationfilter;
    $deptFilter = '' unless defined $deptFilter;
    $mediatypeFilter = '' unless defined $mediatypeFilter;
    $opcoid = '' unless defined $opcoid;

        return () if $opcoid && !looks_like_number($opcoid);
        return () if length($agencyFilter) && !validMysqlStringFilter($agencyFilter);

        my @filters = ();
     my @preliminary_lists = ();
     push(@preliminary_lists, "SELECT job_number FROM tracker3plus WHERE  location=$opcoid");
         push(@preliminary_lists, "SELECT job_number FROM shards WHERE location=$opcoid");

        my $saved_opcoid = $opcoid; # Preserve a copy for use later when writing to get_jobs_logs

        if($agencyFilter)           { push(@filters, "t.agency IN ('"           . join("','", split(/,/, $agencyFilter))            . "')");        }
        if($deptFilter ne '')               { push(@filters, "t3p.DEPARTMENTVALUE IN ('"    . join("','", split(/,/, $deptFilter))          . "')");        }
        if($mediatypeFilter)            { push(@filters, "t.workflow IN ('" . join("','", split(/,/, $mediatypeFilter)) . "')"); }
        if($boxpending)   { push(@filters, "t3p.operatorstatus in ('" . join("','", split(/,/, $boxpending))    . "')");        }
    if($boxqcwait ne '')             { push(@filters, "t3p.qcstatus  IN ('1')"); }
        if($locationfilter)         { push(@filters, "t3p.location IN ('"       . ECMDBOpcos::db_getopcoidfromcompany($locationfilter) . "')");     }
    if($boxqcreject == 1)              { push(@filters, "(t3p.operatorstatus = 'R' OR t3p.qcstatus = '3')"); }
    my $sql = "SELECT t.job_number, t.agency, t.client, t.project, t.publication, t.pathtype, t.agencyref, t.HEADLINE, t.DESCRIPTION, t.opcoid AS myopcoid, t.businessunitref, t.version, t.formatsize AS mediatype, t.SPLITNUMBER, t.mediachoiceid, btp.ORDERDATE, t.MATDATE, btp.DELIVERYDATE, btp.scheduledate, ct.country," .
                " t.workflow AS actiontype,t.requiredDate, t.bookedby, t.accounthandler, t.productionstatus, t.producer, t3p.DEPARTMENTVALUE,t3p.TEAMVALUE, t3p.rec_timestamp AS coverdate, t3p.preflightstatus AS preflightid, t3p.PREFLIGHTDATE, t3p.compareready, t3p.assetscreated, t3p.location AS remoteopcoid, " .
                " t3p.checkoutstatus AS checkoutstatusid,t3p.dispatchdate,t3p.qcstatus AS qcstatusid, COALESCE(t3p.duedate, '2000-01-01 00:00:00') AS duedate, " .
                " ROUND(TIME_TO_SEC(TIMEDIFF(t3p.duedate, now())) / 60) AS timeleft, t3p.operatorstatus, t3p.creative AS isCreative, t3p.duedate < now() AS isOverdue, " .
                " t3p.lastsynced, t3p.estimate, t3p.adgatestatus, t3p.status as jobstatus, EXISTS(SELECT 1 FROM shards s WHERE s.job_number=t.job_number AND location != t3p.location AND EXISTS(SELECT 1 FROM shards ss WHERE s.location != ss.location)) AS multiple_locations, " .
                " op.opconame AS opcompany, op2.opconame AS location, cs.status AS checkoutstatus, dp.name, tp.name AS teamname, qt.querytype, acm.advisory_colour, pfs.status AS preflight, qc.status AS qcstatus, u.username AS assignee, u.id AS userid,a.standard as audiostd,btp.EDITCODE as editcode,t.duration as timelength,v.standard as videostd,t.worktype as worktype,trim(t.formatsize) as formatsize" .
                ' FROM ';
     $sql .= "(SELECT DISTINCT job_number FROM (\n";
         $sql .= join(' UNION ', @preliminary_lists);
         $sql .= ") AS qq) AS temp_table\n";
        #$sql .= (scalar(@preliminary_filters) ? ' WHERE ' . join(' AND ', @preliminary_filters) : '') . ") AS temp_table\n";
        ## JOINS
        $sql .= "       INNER JOIN TRACKER3PLUS t3p ON t3p.job_number=temp_table.job_number
                        INNER JOIN TRACKER3 t ON t.job_number=t3p.job_number
                        INNER JOIN OPERATINGCOMPANY op ON op.opcoid=t3p.opcoid
                        INNER JOIN OPERATINGCOMPANY op2 ON op2.opcoid=t3p.location
                        INNER JOIN CHECKOUTSTATUS cs ON cs.id=t3p.checkoutstatus
                        INNER JOIN PREFLIGHTSTATUS pfs ON pfs.id=t3p.preflightstatus
                        INNER JOIN QCSTATUS qc ON qc.id=t3p.qcstatus
                        INNER JOIN DEPARTMENTS dp ON dp.id=t3p.DEPARTMENTVALUE
                        INNER JOIN TEAMS tp ON tp.id=t3p.TEAMVALUE
                        INNER JOIN querytypes qt on qt.id = t3p.QUERYTYPE
                        LEFT OUTER JOIN ADVISORY_COLOUR_MAPPING acm on acm.opcoid = t3p.opcoid AND acm.advisory_id=t3p.QUERYTYPE
                        LEFT OUTER JOIN Broadcasttracker3 btp on t.job_number = btp.job_number
                        LEFT OUTER JOIN COUNTRIES ct ON btp.country=ct.id
                        LEFT OUTER JOIN USERS u ON u.id=t3p.assignee
                        LEFT OUTER JOIN videostandard v ON btp.VIDEOSTD=v.id
                        LEFT OUTER JOIN audiostandard a ON btp.AUDIOSTD=a.id ";
            # t3p.job_number condition added by Anbu to display products aggiend jobs
                $sql .= '
                      WHERE ' . join(' AND ', @filters) if scalar(@filters);
                if($boxoversch == 1)
                {
            if (scalar(@filters))
            {
                        $sql .= 'AND now() > t.requireddate AND t.requireddate like \'%'.$boxselecteddate.'%\' AND t3p.qcstatus NOT in (\'2\')';
            }
            else
            {
                        $sql .= 'WHERE now() > t.requireddate AND t.requireddate like \'%'.$boxselecteddate.'%\' AND t3p.qcstatus NOT in (\'2\')';
            }
                }
                else
        {
            if( $boxtotaljobs == 1 && @filters != '')
            {
                        $sql .= "AND  t.requireddate like \'%$boxselecteddate%\' AND (t3p.operatorstatus in ('W','P', 'A','U','R') OR t3p.qcstatus in ('1','3'))";
            }
            elsif ($boxtotaljobs == 1)
            {
            $sql .= "WHERE  t.requireddate like \'%$boxselecteddate%\' AND (t3p.operatorstatus in ('W','P', 'A','U','R') OR t3p.qcstatus in ('1','3'))";
            }
            else
            {
            $sql .= 'AND  t.requireddate like \'%'.$boxselecteddate.'%\'';
            }
                }
         $sql .= ' AND t.productionStatus NOT IN  (3, 4, 8, 9, 12) AND t3p.status NOT IN (4, 5, 6) ORDER BY t.job_number DESC';
# Not to include limit into the query, in future to remove limit set -1 in DB
            if($rowscount != -1){
                $sql .=' '.'LIMIT '.$rowscount;
            }
            ## EXECUTE
   my $start_time = time();
            my $rows = $Config::dba->hashed_process_sql($sql);
            my $end_time = time();
            my $elapsed = int(($end_time - $start_time) * 1000) / 1000;
            my $count = 0;
            $count = (scalar @$rows) if ( defined $rows );
            if ( $elapsed > $longQueryPeriod ) {
                carp "db_getJobs: elapsed $elapsed secs, $count rows";
                carp "db_getJobs: SQL $sql"; # only carp with the SQL if its a long one
            }
            # Log the query and elapsed time ( & rows ?)
            eval {
                #$sql =~ s/\s+/ /g; # Convert multi spaces and tabs to one space (unfortunately it also removes line breaks too)
                $sql =~ s/\t+/ /g;
                $sql =~ s/ +/ /g;
                $sql = substr( $sql, 0, 4093 ) || '...' if length( $sql ) > 4096;  # Column is VARCHAR(4096)
                $Config::dba->process_oneline_sql(
                    "INSERT INTO get_jobs_log ( userid, opcoid, rec_timestamp, elapsed_tm, row_count, query_s ) VALUES ( ?, ?, now(), ?, ?, ? )",
                         [ $userid, $saved_opcoid, $elapsed, $count, $sql ] );
            };
            if ( $@ ) {
                carp "Error writing to get_jobs_log, $@";
            }
            my $index = 0;
            my ( @jobs, @client, @agency, @project, @opstatus, @qcstatus, @locationstatus, @prestatus, @assignstatus, @bookedby, @accounthandler, @mediatype, @querytype, @mediachoice, @country, @producer );
            foreach my $row (@$rows)
            {
                $index++;
                $row->{isJobArchived} = $row->{jobstatus} == 6; # Could have added join on table jobstatus and matched status = 'Archived' (column tinyint is not nullable)
                ## PREPARE FILTER DISPLAY VALUES FOR DASHBOARD
                push(@client,           { id => "CLIENT_$index",    name => $row->{client} } );
                push(@agency,           { id => "CUST_$index",  name => $row->{agency} } );
                push(@project,          { id => "PROJ_$index",  name => $row->{project} } );
                push(@opstatus,         { id => "OP_$index",        name => $row->{operatorstatus} } );
                push(@qcstatus,         { id => "QC_$index",        name => $row->{qcstatusid} } );
                push(@locationstatus,   { id => "LOC_$index",   name => $row->{location} });
                push(@prestatus,            { id => "PRE_$index",   name => $row->{preflight} });
                push(@assignstatus,     { id => "ASS_" . ($row->{userid} || 0), name => $row->{assignee} || "-" });
                push(@bookedby,         { id => "BOOK_$index", name => $row->{bookedby} || "-" });
                push(@accounthandler,   { id => "ACC_$index", name => $row->{accounthandler} || "-" });
                push(@mediatype,            { id => "MEDIATYPE_$index",    name => $row->{actiontype} } );
                push(@querytype,            { id => "MEDIATYPE_$index",    name => $row->{querytype}  });
                push(@mediachoice,      { id => "MEDIACHOICEID_$index",    name => $row->{mediachoiceid} || "-"   });
                push(@country,          { id => "COUNRY_$index",    name => $row->{country} || "-"   }) if $row->{country};
                push(@producer,         { id => "PRODUCER_$index",    name => $row->{producer} || "-"   });
                push(@jobs, $row);
            }
    return (\@jobs, \@client, \@agency, \@project, \@opstatus, \@qcstatus, \@locationstatus, \@prestatus, \@assignstatus, \@bookedby, \@accounthandler, \@mediatype, \@mediachoice, \@country, \@producer);
}
sub convert_seconds_to_hhmm {
        my $hourz=int($_[0]/3600);
        my $leftover=$_[0] % 3600;
        my $minz=int($leftover/60);
        my $planhrs = "$hourz:$minz";
        return $planhrs;
}

sub convert_hhmm_to_seconds {
        my $assignhrs = $_[0];
        my ($hr, $min) = split /:/, $assignhrs;
        my $elapsed = ($hr * 3600) + ($min * 60);
        return $elapsed;
}
sub time_format {
        my $hrs = $_[0];
        my ($hr, $min) = split /:/, $hrs;
        my $exacthours = sprintf("%02d",$hr).":".sprintf("%02d",$min);
        return $exacthours;
}

1;
