#!/usr/local/bin/perl

package ECMDBForms;

use strict;
use warnings;

use lib 'modules';

require 'config.pl';
require 'general.pl';

#############################################################################
# getLookUps
#
#############################################################################
sub getLookUps {
    my ($lookups) = $Config::dba->hashed_process_sql("SELECT al.ID, al.PARENT, al.DESCRIPTION, afo.DESCRIPTION AS FORM, afi.DESCRIPTION AS FIELD   
													   FROM abinbev_lookups al, abinbev_forms afo, abinbev_fields afi
													  WHERE al.FORMID = afo.ID
														AND al.FIELDID = afi.ID 
													ORDER BY al.DESCRIPTION, al.FIELDID ");

    return $lookups || [];
}

#############################################################################
# getBrandDetails
#
#############################################################################
sub getBrandDetails {
	my $brandname = $_[0];
	my $f = "'";
	my $r = "\\'";
	$brandname =~ s/$f/$r/g;

    my ($branddetails) = $Config::dba->hashed_process_sql("SELECT bd.id, bd.brand, bd.alcoholvolume as AlcVol, bd.legalname, bd.ingredients,
															 bd.allergens, bd.address, bd.webaddress, bd.consumerhelpline, bd.herocopy, bd.herocopy1, bd.herocopy2,
															 bd.colour1 as Colour1, bd.colour2 as Colour2, bd.colour3 as Colour3, bd.colour4 as Colour4,
															 bd.colour5 as Colour5, bd.colour6 as Colour6, bd.colour7 as Colour7, bd.colour8 as Colour8, bd.tag_master as TagMaster
                                                       FROM brand_details bd 
													  WHERE bd.brand = '$brandname'");

    return $branddetails || [];
}

#############################################################################
# getAllergensUnderlined
#
#############################################################################
sub getAllergensUnderlined {
	my $brandname = $_[0];	
	my $f = "'";
    my $r = "\\'";
    $brandname =~ s/$f/$r/g;

	my ($allergensUnderlined) = $Config::dba->process_oneline_sql("SELECT allergens_underlined FROM brand_details
                        WHERE brand = '$brandname'");

    return $allergensUnderlined || "";
}

#############################################################################
# getPrinterDetails
#
#############################################################################
sub getPrinterDetails {
    my $printer = $_[0];
	my $f = "'";
    my $r = "\\'";
    $printer =~ s/$f/$r/g;

    my ($printerdetails) = $Config::dba->hashed_process_sql("SELECT bwr, magnification, process
                                                       FROM printer_details 
                                                      WHERE printer = '$printer'");

    return $printerdetails || [];
}

#############################################################################
# getCutterRefDetails
#
#############################################################################
sub getCutterRefDetails {
    my $cutter = $_[0];
	my $f = "'";
    my $r = "\\'";
    $cutter =~ s/$f/$r/g;

    my ($details) = $Config::dba->hashed_process_sql("SELECT height AS Height, width AS Width
                                                       FROM cutter_ref_details 
                                                      WHERE cutter_ref = '$cutter'");

    return $details || [];
}

#############################################################################
# getPackFormatDetails
#
#############################################################################
sub getPackFormatDetails {
    my $packformat = $_[0];
	my $primaryformat = $_[1];

    my ($details) = $Config::dba->hashed_process_sql("SELECT content_size AS ContentSize, pack_units AS UnitsPack
                                                       FROM packformat_details 
                                                      WHERE pack_format LIKE '$packformat' AND primary_format = '$primaryformat' LIMIT 1");

    return $details || [];
}

#############################################################################
# getTemplateRefDetails
#
#############################################################################
sub getTemplateRefDetails{
	my $templateref = $_[0];

    my ($details) = $Config::dba->hashed_process_sql("SELECT tag_master AS TagMaster
                                                       FROM template_ref_details 
                                                      WHERE template_ref_name LIKE '$templateref' LIMIT 1");

    return $details || [];
}

#############################################################################
# getBrandOwnerDetails
#
#############################################################################
sub getBrandOwnerDetails {
	my $name = $_[0];

    my ($details) = $Config::dba->hashed_process_sql("SELECT email
                                                       FROM brandowner_details 
                                                      WHERE name = '$name'");

    return $details || [];
}

#############################################################################
# listForms
#
#############################################################################
sub listForms {
	my $forms;
	my $formid = $_[0] || "";

	if( $formid eq "null" || $formid eq ""){
    	eval {
        	$forms = $Config::dba->hashed_process_sql("SELECT af.id, af.component_name, af.job_number, af.userid, af.rffs_no, af.ftp_date,
                                                            af.brand, af.pack_format, af.primary_format, af.material_ref, af.brand_owner, 
                                                            af.barcode_ean, u.USERNAME, af.rec_timestamp
                                                       FROM abinbevforms af, users u
                                                      WHERE af.userid = u.ID
                                                        ORDER BY af.id DESC");
    	};
	}
	else{
		eval {
            $forms = $Config::dba->hashed_process_sql("SELECT af.id, af.component_name, af.job_number, af.userid, af.rffs_no, af.ftp_date,
                                                            af.brand, af.pack_format, af.primary_format, af.material_ref, af.brand_owner, 
                                                            af.barcode_ean, u.USERNAME, af.rec_timestamp
                                                       FROM abinbevforms af, users u
                                                      WHERE af.userid = u.ID
														AND af.id = ? 
                                                        ORDER BY af.id DESC", [ $formid ]);
        };
	}

	return $forms ? @$forms : ();
}

#############################################################################
# editForm
#
#############################################################################
sub editForm {
    my $formid = $_[0];

    my ($formdata) = $Config::dba->hashed_process_sql(" SELECT id, component_name, job_number, userid, rffs_no, ftp_date, brand, pack_format, primary_format, material_ref,
																material_type, brand_owner, printer, print_process, is_pre_print, is_post_print, converter, cutter_ref, tas_ref, based_on_design,
																design_agency, barcode_ean, is_ean2_require, ean2, barcode_itf, barcode_upc, ean_size, ean2_size, itf_size, upc_size,
																alc_vol, content_size, units_pack, alc_units, legal_name, consumer_helpline, address, web_address, ingredients, allergens,
																underlined, gmg_proof, check_2d_render, check_3d_render, scannable_barcodes, brief_detail, tag_job_number, version, job_tilte, refer_to_job,
																colour_selection, colour1, colour2, colour3, colour4, colour5, colour6, colour7, colour8, code1,
																code2, code3, media_strip, tag_master, tag_cmd_path, scannable_bc, manual_ip_tag_master, spec_printer, spec_process, spec_packing_type,
																spec_bwr, spec_magnification_per, width, height, front, back, side_a, side_b, base, pm,
																bg, created_date, modified_date, tag_pm_bg_version, is_scannable_barcodes, sb_brand, sb_pack_foramt, sb_product_code, comp_campaign, sb_bar_code_ean1,
																ean_location1, sb_bar_code_itf1, itf_location1, sb_bar_code_ean2, ean_location2, sb_bar_code_itf2, itf_location2, sb_rffs_no, sb_brand_owner, email,
																graphic_number, graphic_name, customer_item_no, template_ref, ball_pack_colour1, ball_pack_colour2, ball_pack_colour3, ball_pack_colour4, ball_pack_colour5, ball_pack_colour6,
																ball_pack_colour7, ball_pack_colour8, rec_timestamp,
																perml_pri,energy_per100_pri,energy_perml_pri,perml_sec,energy_per100_sec,
																energy_perml_sec,fat_per100_sec,fat_perml_sec,carbohyderate_per100_sec,carbohyderate_perml_sec,
																protein_per100_sec,protein_perml_sec,salt_per100_sec,salt_perml_sec,pack_format_type,pack_format_variant,web_address2,tasfilename,
																herocopy, herocopy1, herocopy2,image_link,image_link_2,image_link_3,image_link_4,image_link_5,image_link_6,image_link_7,image_link_8
														   FROM abinbevforms
                                                      	  WHERE id = ?", [ $formid ]);

    return $formdata || [];
}

#############################################################################
# saveAbInbevForm
#
#############################################################################
sub saveAbInbevForm{
	my $query   = $_[0];
	my $session = $_[1];
	my $tagmasterpath = $_[2];
	my $scannablebarcodepath = $_[3];
	
	my $userid = $session->param( 'userid' );
	my $CompName = $query->param( "CompName" );
	my $RffsNo = $query->param( "RffsNo" );
	my $FtpDate = $query->param( "FtpDate" ) || '';
	my $brand = $query->param( "brand" );
	my $PackFormat = $query->param( "PackFormat" );
	my $primaryformat = $query->param( "primaryformat" );
	my $MaterialRef = $query->param( "MaterialRef" );
	my $MaterialType = $query->param( "MaterialType" );
	my $BrandOwner = $query->param( "BrandOwner" );
	my $Printer = $query->param( "Printer" );
	my $PrintProcess = $query->param( "PrintProcess" );
	my $checkPrePrint = $query->param( "checkPrePrint" );
	my $checkPostPrint = $query->param( "checkPostPrint" );
	my $Converter = $query->param( "Converter" );
	my $CutterRef = $query->param( "CutterRef" );
	my $TasRef = $query->param( "TasRef" );
	my $BasedOnDesign = $query->param( "BasedOnDesign" );
	my $DesignAgency = $query->param( "DesignAgency" );
	my $be = $query->param( "be" );
	my $beansize = $query->param( "beansize" );
	my $checkEan2 = $query->param( "checkEan2" );
	my $ean = $query->param( "ean" );
	my $ean2size = $query->param( "ean2size" );
	my $bitf = $query->param( "bitf" );
	my $itfsize = $query->param( "itfsize" );
	my $bupc = $query->param( "bupc" );
	my $upcsize = $query->param( "upcsize" );
	my $AlcVol = $query->param( "AlcVol" );
	my $ContentSize = $query->param( "ContentSize" );
	my $UnitsPack = $query->param( "UnitsPack" );
	my $alcoholvolume = $query->param( "alcoholvolume" );
	my $legalname = $query->param( "legalname" );
	my $consumerhelpline = $query->param( "consumerhelpline" );
	my $address = $query->param( "address" );
	my $webaddress = $query->param( "webaddress" );
	my $ingredients = $query->param( "ingredients" );
	my $allergens = $query->param( "allergens" );
	my $Underlined = $query->param( "Underlined" );
	my $checkGmgProof = $query->param( "checkGmgProof" );
	my $check2dRender = $query->param( "check2dRender" );
	my $check3dRender = $query->param( "check3dRender" );
	my $checkScanBarcode = $query->param( "checkScanBarcode" );
	my $brief = $query->param( "brief" );
	my $TagJobNo = $query->param( "TagJobNo" ) || 0;
	my $Version = $query->param( "Version" ) || 0;
	my $JobTitle = $query->param( "JobTitle" );
	my $ReferToJob = $query->param( "ReferToJob" ) || "";
	my $Colour1 = $query->param( "Colour1" );
	my $Colour2 = $query->param( "Colour2" );
	my $Colour3 = $query->param( "Colour3" );
	my $Colour4 = $query->param( "Colour4" );
	my $Colour5 = $query->param( "Colour5" );
	my $Colour6 = $query->param( "Colour6" );
	my $Colour7 = $query->param( "Colour7" );
	my $Colour8 = $query->param( "Colour8" );
	my $Code1 = $query->param( "Code1" );
	my $Code2 = $query->param( "Code2" );
	my $Code3 = $query->param( "Code3" );
	my $MediaStrip = $query->param( "MediaStrip" ) || '';
	my $TagMaster = $query->param( "TagMaster" ) || '';
	my $TagCmdPath = $query->param( "TagCmdPath" ) || '';
	my $ScanBc = $query->param( "ScanBc" ) || '';
	my $ManualInput = $query->param( "ManualInput" ) || '';
	my $SpPrinter = $query->param( "SpPrinter" );
	my $SpProcess = $query->param("SpProcess");
	my $SpPackType = $query->param( "SpPackType" );
	my $SpBwr = $query->param( "SpBwr" );
	my $Mangnification = $query->param( "Mangnification" );
	my $Width = $query->param( "Width" ) || 0;
	my $Height = $query->param( "Height" ) || 0;
	my $Front = $query->param( "Front" );
	my $Back = $query->param( "Back" );
	my $SideA = $query->param( "SideA" );
	my $SideB = $query->param( "SideB" );
	my $Base = $query->param( "Base" );
	my $Pm = $query->param( "Pm" );
	my $Bg = $query->param( "Bg" );
	my $CreatedDt = $query->param( "CreatedDt" ) || '';
	my $ModifiedDt = $query->param( "ModifiedDt" ) || '';
	my $PmBgVersion = $query->param( "PmBgVersion" ) || 0;
	my $selectbarcode = $query->param( "selectbarcode" );
	my $SbBrand = $query->param( "SbBrand" );
	my $SbPackFormat = $query->param( "SbPackFormat" );
	my $SbProductCode = $query->param( "SbProductCode" );
	my $CompCampaign = $query->param( "CompCampaign" );
	my $SbEan = $query->param( "SbEan" );
	my $EanLocation1 = $query->param( "EanLocation1" );
	my $SbItf = $query->param( "SbItf" );
	my $ItfLocation1 = $query->param( "ItfLocation1" );
	my $Sb2Ean = $query->param( "Sb2Ean" );
	my $EanLocation2 = $query->param( "EanLocation2" );
	my $Sb2Itf = $query->param( "Sb2Itf" );
	my $ItfLocation2 = $query->param( "ItfLocation2" );
	my $SbRffsNo = $query->param( "SbRffsNo" );
	my $SbBrandOwner = $query->param( "SbBrandOwner" );
	my $Email = $query->param( "Email" );
	my $GraphicNo = $query->param( "GraphicNo" );
	my $GraphicName = $query->param( "GraphicName" );
	my $CustomerItemNo = $query->param( "CustomerItemNo" );
	my $TemplateRef = $query->param( "TemplateRef" );
	my $BpColour1 = $query->param( "BpColour1" );
	my $BpColour2 = $query->param( "BpColour2" );
	my $BpColour3 = $query->param( "BpColour3" );
	my $BpColour4 = $query->param( "BpColour4" );
	my $BpColour5 = $query->param( "BpColour5" );
	my $BpColour6 = $query->param( "BpColour6" );
	my $BpColour7 = $query->param( "BpColour7" );
	my $BpColour8 = $query->param( "BpColour8" );
	my $colour_selection = $query->param( "colour_selection" );
	my $id = $query->param( "id" );

	my $perml_pri = $query->param( "perml_pri" ) || 0;
	my $energy_per100_pri = $query->param( "energy_per100_pri" ) || 0;
	my $energy_perml_pri = $query->param( "energy_perml_pri" ) || 0;
	my $perml_sec = $query->param( "perml_sec" ) || 0;
	my $energy_per100_sec = $query->param( "energy_per100_sec" ) || 0;
	my $energy_perml_sec = $query->param( "energy_perml_sec" ) || 0;
	my $fat_per100_sec = $query->param( "fat_per100_sec" ) || 0;
	my $fat_perml_sec = $query->param( "fat_perml_sec" ) || 0;
	my $carbohyderate_per100_sec = $query->param( "carbohyderate_per100_sec" ) || 0;
	my $carbohyderate_perml_sec = $query->param( "carbohyderate_perml_sec" ) || 0;
	my $protein_per100_sec = $query->param( "protein_per100_sec" ) || 0;
	my $protein_perml_sec = $query->param( "protein_perml_sec" ) || 0;
	my $salt_per100_sec = $query->param( "salt_per100_sec" ) || 0;
	my $salt_perml_sec = $query->param( "salt_perml_sec" ) || 0;
	my $pack_format_type = $query->param( "pack_format_type" );
	my $pack_format_variant = $query->param( "pack_format_variant" );
	my $webaddress2 = $query->param( "webaddress2" );
	my $tasfilename = $query->param( "tasfilename" ) || "";

	my $herocopy = $query->param( "herocopy" ) || "";
	my $herocopy1 = $query->param( "herocopy1" ) || "";
	my $herocopy2 = $query->param( "herocopy2" ) || "";

	my $image_link = $query->param( "image_link" ) || "";
	my $image_link_2 = $query->param( "image_link_2" ) || "";
	my $image_link_3 = $query->param( "image_link_3" ) || "";
	my $image_link_4 = $query->param( "image_link_4" ) || "";
	my $image_link_5 = $query->param( "image_link_5" ) || "";
	my $image_link_6 = $query->param( "image_link_6" ) || "";
	my $image_link_7 = $query->param( "image_link_7" ) || "";
	my $image_link_8 = $query->param( "image_link_8" ) || "";

	# get tag cmd path
	if( $TagJobNo > 0){
		$TagCmdPath	= getTagCmdPath($TagJobNo);
	}

	if($TagMaster == ""){
		$TagMaster = $tagmasterpath;
	}

	if($ScanBc == ""){
		$ScanBc = $scannablebarcodepath;
	}

	if ($id eq "0"){
	$Config::dba->process_sql("INSERT INTO abinbevforms(ftp_date,created_date,modified_date,rec_timestamp,component_name,job_number,userid,rffs_no,brand,pack_format,
														primary_format,material_ref,material_type,brand_owner,printer,print_process,is_pre_print,is_post_print,converter,cutter_ref,
														tas_ref,based_on_design,design_agency,barcode_ean,is_ean2_require,ean2,barcode_itf,barcode_upc,ean_size,ean2_size,
														itf_size,upc_size,alc_vol,content_size,units_pack,alc_units,legal_name,consumer_helpline,address,web_address,
														ingredients,allergens,underlined,gmg_proof,check_2d_render,check_3d_render,scannable_barcodes,brief_detail,tag_job_number,version,
														job_tilte,refer_to_job,colour_selection,colour1,colour2,colour3,colour4,colour5,colour6,colour7,
														colour8,code1,code2,code3,media_strip,tag_master,tag_cmd_path,scannable_bc,manual_ip_tag_master,spec_printer,
														spec_process,spec_packing_type,spec_bwr,spec_magnification_per,width,height,front,back,side_a,side_b,
														base,pm,bg,tag_pm_bg_version,is_scannable_barcodes,sb_brand,sb_pack_foramt,sb_product_code,comp_campaign,sb_bar_code_ean1,
														ean_location1,sb_bar_code_itf1,itf_location1,sb_bar_code_ean2,ean_location2,sb_bar_code_itf2,itf_location2,sb_rffs_no,sb_brand_owner,email,
														graphic_number,graphic_name,customer_item_no,template_ref,ball_pack_colour1,ball_pack_colour2,ball_pack_colour3,ball_pack_colour4,ball_pack_colour5,ball_pack_colour6,
														ball_pack_colour7,ball_pack_colour8,
														perml_pri,energy_per100_pri,energy_perml_pri,perml_sec,energy_per100_sec,
														energy_perml_sec,fat_per100_sec,fat_perml_sec,carbohyderate_per100_sec,carbohyderate_perml_sec,
														protein_per100_sec,protein_perml_sec,salt_per100_sec,salt_perml_sec,pack_format_type,pack_format_variant,web_address2,tasfilename,
														herocopy,herocopy1,herocopy2,image_link,image_link_2,image_link_3,image_link_4,image_link_5,image_link_6,image_link_7,image_link_8)
                        VALUES(?, ?, ?, now(), ?, ?,?,?,?,?,
								?,?,?,?,?,?,?,?,?,?,
								?,?,?,?,?,?,?,?,?,?,
								?,?,?,?,?,?,?,?,?,?,
								?,?,?,?,?,?,?,?,?,?,
								?,?,?,?,?,?,?,?,?,?,
								?,?,?,?,?,?,?,?,?,?,
								?,?,?,?,?,?,?,?,?,?,
								?,?,?,?,?,?,?,?,?,?,
								?,?,?,?,?,?,?,?,?,?,
								?,?,?,?,?,?,?,?,?,?,
								?,?,
								?,?,?,?,?,
								?,?,?,?,?,
								?,?,?,?,?,?,?,?,
								?,?,?,?,?,?,?,?,?,?,?)", [ $FtpDate, $CreatedDt, $ModifiedDt, $CompName, $TagJobNo, $userid, $RffsNo, $brand, $PackFormat,
										$primaryformat, $MaterialRef, $MaterialType, $BrandOwner, $Printer, $PrintProcess, $checkPrePrint, $checkPostPrint, $Converter, $CutterRef,
										$TasRef, $BasedOnDesign, $DesignAgency, $be, $checkEan2, $ean, $bitf, $bupc, $beansize, $ean2size,
										$itfsize, $upcsize, $AlcVol, $ContentSize, $UnitsPack, $alcoholvolume, $legalname, $consumerhelpline, $address, $webaddress,
										$ingredients, $allergens, $Underlined, $checkGmgProof, $check2dRender, $check3dRender, $checkScanBarcode, $brief, $TagJobNo, $Version,
										$JobTitle, $ReferToJob, $colour_selection, $Colour1, $Colour2, $Colour3, $Colour4, $Colour5, $Colour6, $Colour7,
										$Colour8, $Code1, $Code2, $Code3, $MediaStrip, $TagMaster, $TagCmdPath, $ScanBc, $ManualInput, $SpPrinter,
										$SpProcess, $SpPackType, $SpBwr, $Mangnification, $Width, $Height, $Front, $Back, $SideA, $SideB,
										$Base, $Pm, $Bg, $PmBgVersion, $selectbarcode, $SbBrand, $SbPackFormat, $SbProductCode, $CompCampaign, $SbEan,
										$EanLocation1, $SbItf, $ItfLocation1, $Sb2Ean, $EanLocation2, $Sb2Itf, $ItfLocation2, $SbRffsNo, $SbBrandOwner, $Email,
										$GraphicNo, $GraphicName, $CustomerItemNo, $TemplateRef, $BpColour1, $BpColour2, $BpColour3, $BpColour4, $BpColour5, $BpColour6,
										$BpColour7, $BpColour8,
										$perml_pri, $energy_per100_pri, $energy_perml_pri, $perml_sec, $energy_per100_sec,
                                        $energy_perml_sec, $fat_per100_sec, $fat_perml_sec, $carbohyderate_per100_sec, $carbohyderate_perml_sec,
                                        $protein_per100_sec, $protein_perml_sec, $salt_per100_sec, $salt_perml_sec, $pack_format_type, $pack_format_variant, $webaddress2, $tasfilename,
										$herocopy, $herocopy1, $herocopy2, $image_link, $image_link_2, $image_link_3, $image_link_4, $image_link_5, $image_link_6, $image_link_7, $image_link_8 ]);
	}else{

	$Config::dba->process_sql("DELETE FROM abinbevforms WHERE id=?", [ $id ]);

	$Config::dba->process_sql("INSERT INTO abinbevforms(id,ftp_date,created_date,modified_date,rec_timestamp,component_name,job_number,userid,rffs_no,brand,pack_format,
                                                        primary_format,material_ref,material_type,brand_owner,printer,print_process,is_pre_print,is_post_print,converter,cutter_ref,
                                                        tas_ref,based_on_design,design_agency,barcode_ean,is_ean2_require,ean2,barcode_itf,barcode_upc,ean_size,ean2_size,
                                                        itf_size,upc_size,alc_vol,content_size,units_pack,alc_units,legal_name,consumer_helpline,address,web_address,
                                                        ingredients,allergens,underlined,gmg_proof,check_2d_render,check_3d_render,scannable_barcodes,brief_detail,tag_job_number,version,
                                                        job_tilte,refer_to_job,colour_selection,colour1,colour2,colour3,colour4,colour5,colour6,colour7,
                                                        colour8,code1,code2,code3,media_strip,tag_master,tag_cmd_path,scannable_bc,manual_ip_tag_master,spec_printer,
                                                        spec_process,spec_packing_type,spec_bwr,spec_magnification_per,width,height,front,back,side_a,side_b,
                                                        base,pm,bg,tag_pm_bg_version,is_scannable_barcodes,sb_brand,sb_pack_foramt,sb_product_code,comp_campaign,sb_bar_code_ean1,
                                                        ean_location1,sb_bar_code_itf1,itf_location1,sb_bar_code_ean2,ean_location2,sb_bar_code_itf2,itf_location2,sb_rffs_no,sb_brand_owner,email,
                                                        graphic_number,graphic_name,customer_item_no,template_ref,ball_pack_colour1,ball_pack_colour2,ball_pack_colour3,ball_pack_colour4,ball_pack_colour5,ball_pack_colour6,
                                                        ball_pack_colour7,ball_pack_colour8,
														perml_pri,energy_per100_pri,energy_perml_pri,perml_sec,energy_per100_sec,
                                                        energy_perml_sec,fat_per100_sec,fat_perml_sec,carbohyderate_per100_sec,carbohyderate_perml_sec,
                                                        protein_per100_sec,protein_perml_sec,salt_per100_sec,salt_perml_sec,pack_format_type,pack_format_variant,web_address2,tasfilename,
														herocopy,herocopy1,herocopy2,image_link,image_link_2,image_link_3,image_link_4,image_link_5,image_link_6,image_link_7,image_link_8)
                        VALUES(?,?, ?, ?, now(), ?, ?,?,?,?,?,
                                ?,?,?,?,?,?,?,?,?,?,
                                ?,?,?,?,?,?,?,?,?,?,
                                ?,?,?,?,?,?,?,?,?,?,
                                ?,?,?,?,?,?,?,?,?,?,
                                ?,?,?,?,?,?,?,?,?,?,
                                ?,?,?,?,?,?,?,?,?,?,
                                ?,?,?,?,?,?,?,?,?,?,
                                ?,?,?,?,?,?,?,?,?,?,
                                ?,?,?,?,?,?,?,?,?,?,
                                ?,?,?,?,?,?,?,?,?,?,
                                ?,?,
								?,?,?,?,?,
                                ?,?,?,?,?,
                                ?,?,?,?,?,?,?,?,
								?,?,?,?,?,?,?,?,?,?,?)", [ $id, $FtpDate, $CreatedDt, $ModifiedDt, $CompName, $TagJobNo, $userid, $RffsNo, $brand, $PackFormat,
                                        $primaryformat, $MaterialRef, $MaterialType, $BrandOwner, $Printer, $PrintProcess, $checkPrePrint, $checkPostPrint, $Converter, $CutterRef,
                                        $TasRef, $BasedOnDesign, $DesignAgency, $be, $checkEan2, $ean, $bitf, $bupc, $beansize, $ean2size,
                                        $itfsize, $upcsize, $AlcVol, $ContentSize, $UnitsPack, $alcoholvolume, $legalname, $consumerhelpline, $address, $webaddress,
                                        $ingredients, $allergens, $Underlined, $checkGmgProof, $check2dRender, $check3dRender, $checkScanBarcode, $brief, $TagJobNo, $Version,
                                        $JobTitle, $ReferToJob, $colour_selection, $Colour1, $Colour2, $Colour3, $Colour4, $Colour5, $Colour6, $Colour7,
                                        $Colour8, $Code1, $Code2, $Code3, $MediaStrip, $TagMaster, $TagCmdPath, $ScanBc, $ManualInput, $SpPrinter,
                                        $SpProcess, $SpPackType, $SpBwr, $Mangnification, $Width, $Height, $Front, $Back, $SideA, $SideB,
                                        $Base, $Pm, $Bg, $PmBgVersion, $selectbarcode, $SbBrand, $SbPackFormat, $SbProductCode, $CompCampaign, $SbEan,
                                        $EanLocation1, $SbItf, $ItfLocation1, $Sb2Ean, $EanLocation2, $Sb2Itf, $ItfLocation2, $SbRffsNo, $SbBrandOwner, $Email,
                                        $GraphicNo, $GraphicName, $CustomerItemNo, $TemplateRef, $BpColour1, $BpColour2, $BpColour3, $BpColour4, $BpColour5, $BpColour6,
                                        $BpColour7, $BpColour8,
										$perml_pri, $energy_per100_pri, $energy_perml_pri, $perml_sec, $energy_per100_sec,
                                        $energy_perml_sec, $fat_per100_sec, $fat_perml_sec, $carbohyderate_per100_sec, $carbohyderate_perml_sec,
                                        $protein_per100_sec, $protein_perml_sec, $salt_per100_sec, $salt_perml_sec, $pack_format_type, $pack_format_variant, $webaddress2, $tasfilename,
										$herocopy, $herocopy1, $herocopy2, $image_link, $image_link_2, $image_link_3, $image_link_4, $image_link_5, $image_link_6, $image_link_7, $image_link_8 ]);
	}


    return 1;
}

##############################################################################
# getTagCmdPath
#
##############################################################################
sub getTagCmdPath {
	my $jobnumber = $_[0];
	my $tagcmdpath = "";
	my $letter = "";
	my $agency = "";
	my $client = "";
	my $project = "";
	my $headline = "";

    my ($wippath) = $Config::dba->process_oneline_sql("select WIPPATH from operatingcompany where opcoid = (select opcoid from tracker3 where job_number = ?) ", [ $jobnumber ]);

	my $tracker3fields = $Config::dba->hashed_process_sql("SELECT LETTER,AGENCY,CLIENT,PROJECT,HEADLINE,JOB_NUMBER
                                                       FROM tracker3
                                                      WHERE job_number = ? ", [ $jobnumber ]);
	foreach (@{ $tracker3fields || [] })
    {
		$letter = $_->{LETTER};
		$agency = $_->{AGENCY};
		$client = $_->{CLIENT};
		$project = $_->{PROJECT};
		$headline = $_->{HEADLINE};
		$jobnumber = $_->{JOB_NUMBER};
	}

	if( $wippath ne "" && $letter ne "" && $agency ne "" && $client ne "" && $project ne "" && $headline ne "" && $jobnumber ne ""){
		$tagcmdpath = sprintf("%s/%s/%s/%s/%s/%s/%s_", "$wippath", "$letter", "$agency","$client", "$project", "$headline", "$jobnumber");
	}

    return $tagcmdpath;
}

##############################################################################
# getLastFormId
#
##############################################################################
sub getLastFormId {
	my $userid = $_[0];

	my ($insertedid) = $Config::dba->process_oneline_sql("SELECT max(id) FROM abinbevforms
                        WHERE userid=? ", [ $userid ]);

    return $insertedid;
}

##############################################################################
# sendXml
#
##############################################################################
sub sendXml {
	my $jobnumber = $_[0];
	my $userid = $_[1];
	my $id = $_[2];

	my ($count) = $Config::dba->process_oneline_sql("SELECT count(job_number) FROM abinbevxmlqueue
                        WHERE job_number=? ", [ $jobnumber ]);
	if( $count == "0"){
		$Config::dba->process_sql("INSERT INTO abinbevxmlqueue
                        (userid, job_number, formid, processingxml, rec_timestamp)
                    VALUES (?, ?, ?, 0, now())",
                    [ $userid, $jobnumber, $id ]);	

		$Config::dba->process_sql("INSERT INTO abinbevxmlqueuehistory
                        (userid, job_number, formid, rec_timestamp)
                    VALUES (?, ?, ?, now())",
                    [ $userid, $jobnumber, $id ]); 
		return 1;
	}else{
		return 0;
	}

}

##############################################################################
# updateTASFileName
#
##############################################################################
sub updateTASFileName 
{
	my $formid = $_[1];
	my $tasfilename = $_[2];

    $Config::dba->process_sql("UPDATE abinbevforms SET tasfilename = ?
                        WHERE id = ? ",
                        [ $tasfilename, $formid ]);

    return 1;
}

##############################################################################
# deleteTASFileName
#
##############################################################################
sub deleteTASFileName 
{
    my $formid = $_[1];

    $Config::dba->process_sql("UPDATE abinbevforms SET tasfilename = ''
                        WHERE id = ? ",
                        [ $formid ]);

    return 1;
}

##############################################################################
# getTASFileName
#
##############################################################################
sub getTASFileName {
    my $formid = $_[0];

    my ($tasfilename) = $Config::dba->process_oneline_sql("SELECT tasfilename FROM abinbevforms
                        WHERE id=? LIMIT 1 ", [ $formid ]);

    return $tasfilename;
}
