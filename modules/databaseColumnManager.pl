#!/usr/local/bin/perl
package ECMDBColumnManager;

use warnings;
use strict;

use Scalar::Util qw( looks_like_number );

use CGI::Carp qw/confess cluck croak/;

use Data::Dumper;

# Home grown, please do not include another other big modules, such as jobs.pl, databaseJob.pl, databaseOpcos.pl

use lib 'modules';

use Validate;

require "config.pl";
require "filters.pl"; # matchCondition and getMatchConditionSymbols

# getCurrentColumns - Return list of columns to display in the Column Manager dialog
#
# The Column Manager dialog displays the list of columns in sort order, so all the user can do is
# a) change the order in the list, hence the sort_order
# b) check or uncheck each column, changes visibility, which determines if the column is displayed in the job list
#
# Returns an array of hash { column_id, column_label, condition, sort_order, visibility, default }
#
# In addition, rather than waiting for the CM update to merge any defaults from table column_manager_defaults
# merge these in here, so the user can see them.
#
# Rows in column_settings have conditions (a column called condition). The condition is evaluated
# after the SQL query has returned the row, the variables in the condition include roleid, rolename etc
# There can be more than one row with the same column_id (but hopefully equal and opposite conditions).
#
# Column Manager displays an ordered list of Column Labels
# so need the column_id (unique key), column_label and sort_order.

sub getCurrentColumns {
	my ( $userid, $opcoid, $opflags, $roleid, $rolename, $relaxroles ) = @_;

	my @mappings;
	my %found;

	my $symbol_table = Filters::getMatchConditionSymbols( $opflags, $roleid, $rolename, $relaxroles );

	# Get the current settings and apply the conditions to the JOINed table to see which columns the user
	# is allowed to see for that opcoid.

	my $rows = $Config::dba->hashed_process_sql(
			"SELECT cm.column_id, cm.column_label, cm.condition, " .
			" us.visibility, us.sort_order " .
			" FROM column_mappings cm INNER JOIN usersettings us ON us.column_id = cm.column_id " .
			" WHERE cm.column_name IS NOT NULL AND us.userid=? AND us.opcoid=? " .
			# " AND cm.column_name <> '' " .
			" ORDER BY us.sort_order", [ $userid, $opcoid ] );

	if ( defined $rows ) {

		# warn "Initial set " . Dumper( $rows ) . "\n";
		foreach my $row ( @$rows ) {
			my $condition = $row->{condition};
			if ( ( defined $condition ) && ( $condition ne '' ) ) {
				my $match = Filters::matchCondition( $condition, $symbol_table );
				# warn "Column " . $row->{column_id} . " label " . $row->{column_label} . " condition " . $condition . " match $match";
				next unless $match == 1;
			}
			$row->{default} = 0; # Indicate this is an existing setting in usersettings
			push( @mappings, $row );
			$found{ $row->{column_id} } = 1;
		}
	}

	# warn "List after condition filter " . Dumper( \@mappings ) . "\n";

	# Repeat the exercise, but use the column_manager_defaults

	my $rows2 = $Config::dba->hashed_process_sql(
			"SELECT cm.column_id, cm.column_label, cm.condition, " .
			" cmd.visibility, cmd.sort_order " .
			" FROM column_mappings cm INNER JOIN column_manager_defaults cmd ON cmd.column_id = cm.column_id " .
			" WHERE cm.column_name IS NOT NULL  " .
			# " AND cm.column_name <> '' " .
			" ORDER BY cmd.sort_order" );

	if ( defined $rows2 ) {

		# warn "Found " . scalar( @{$rows2} ) . " rows from defaults\n";

		foreach my $row ( @$rows2 ) {
			my $condition = $row->{condition};
			if ( ( defined $condition ) && ( $condition ne '' ) ) {
				my $match = Filters::matchCondition( $condition, $symbol_table );
				# warn "Column " . $row->{column_id} . " label " . $row->{column_label} . " condition " . $condition . " match $match";
				next unless $match == 1;
			}
			# warn " found default " . $row->{column_id} . "\n";
			if ( ! exists $found{ $row->{column_id} } ) {
				# warn "Adding default " . $row->{column_id} . "\n";
				$row->{default} = 1;
				push( @mappings, $row );
				$found{ $row->{column_id} } = 1;
			}
		}
	}
	# warn Dumper( \@mappings );

	return \@mappings;

}

##############################################################################
##      getUserSettings 
##
###############################################################################
sub getUserSettings {
	my ($userid, $opcoid) = @_;

	my @settings;
	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );

		setUserSettings( $userid, $opcoid );

		@settings = $Config::dba->process_onerow_sql(
			"SELECT CONCAT(column_id, '~', column_name, '~', width, '~',visibility, '~', sort_order) FROM usersettings WHERE userid=? AND opcoid=?
						ORDER BY sort_order", [ $userid, $opcoid ]);
	};
	if ( $@ ) {
		my $error = $@;
		confess "getUserSettings: $error";
		$error =~ s/ at .*//gs;
		$userid = 'undef' unless defined $userid;
		$opcoid = 'undef' unless defined $opcoid;
		die "getUserSettings( $userid, $opcoid): $error";
	}
	return @settings;
}

##############################################################################
##      Fill in default column settings where a user hasn't had that
##		column configured yet
###############################################################################
sub setUserSettings(){
	my ($userid, $opcoid) = @_;

	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );

		my $rows = $Config::dba->hashed_process_sql( "SELECT column_id, column_name, width, visibility, sort_order FROM column_manager_defaults" );
		if ( defined $rows ) {
			foreach my $r ( @$rows ) {
				my ($setting_exists) = $Config::dba->process_oneline_sql(
					"SELECT 1 FROM usersettings WHERE userid=? AND opcoid=? AND column_id=?",[ $userid, $opcoid, $r->{column_id} ] );
				next if $setting_exists;
				$Config::dba->process_sql(
					"INSERT INTO usersettings (opcoid, userid,column_id, column_name, width, visibility, sort_order) VALUES (?, ?, ?, ?, ?, ?, ?)",
					[ $opcoid, $userid, $r->{column_id}, $r->{column_name}, $r->{width}, $r->{visibility}, $r->{sort_order} ] ); 
			}
		}
	};
	if ( $@ ) {
		confess "setUserSettings: userid: " . ( $userid || 0 ) . ", opcoid: " . ( $opcoid || 0 ) . ' ' . $@;
	}
}

##############################################################################
##      db_applyColumnManager
##
###############################################################################
sub db_applyColumnManager{
	my ( $userid, $updatestring, $opcoid ) = @_;

	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );
		foreach my $columnlist ( split(/,/, $updatestring) ) {
			my @columns = split(/~/, $columnlist); # forms array ( column_id, visibility, sort_order )
			
			$Config::dba->process_sql(
				"UPDATE usersettings SET visibility=?, sort_order=? WHERE userid=? AND column_id=? AND opcoid=?",
				[ $columns[1], $columns[2], $userid, $columns[0], $opcoid ]);
		}
	};
	if ( $@ ) {
		my $error = $@;
		confess "db_applyColumnManager: $error";
		$error =~ s/ at .*//gs;
		$userid = 'undef' unless defined $userid;
		$opcoid = 'undef' unless defined $opcoid;
		die "db_applyColumnManager( $userid, '$updatestring', $opcoid ) : $error";
	}
}

##############################################################################
###      db_updateColumnManager
###
################################################################################
sub db_updateColumnManager{
	my ( $userid, $updatestring, $opcoid ) = @_;
	
	eval {
		Validate::userid( $userid );
		Validate::opcoid( $opcoid );
		die "Missing column list" unless defined $updatestring;
	
		foreach my $columnlist (split(/,/, $updatestring)) {
			my @columns = split(/~/, $columnlist); # convert to array ( column_id, width )
			
			$Config::dba->process_sql("UPDATE usersettings SET width=? WHERE userid=? AND column_id=? AND opcoid=?",
							[ $columns[1], $userid, $columns[0], $opcoid ]);
		}
	};
	if ( $@ ) {
		my $error = $@;
		confess "db_updateColumnManager: $error";
		$error =~ s/ at .*//gs;
		$userid = 'undef' unless defined $userid;
		$opcoid = 'undef' unless defined $opcoid;
		$updatestring = 'undef' unless defined $updatestring;
		die "db_updateColumnManager( $userid, '$updatestring', $opcoid ) : $error";
	}
}

1;

