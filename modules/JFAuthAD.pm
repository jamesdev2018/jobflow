#!/usr/local/bin/perl

package JFAuthAD;

use warnings;
use strict;

use Carp qw/croak cluck confess carp/;

use Net::LDAP;
use Data::Dumper;

# Home grown
use lib 'modules';

use parent 'JFAuthBase';

use constant DEFAULT_SERVICE => "ldap://172.27.88.66:389";

use constant DEFAULT_BASE => 'OU=GB,DC=prg-dc,DC=dhl,DC=com';

use constant DEFAULT_SAM_FILTER => '(&(samAccountName=<principal>)(objectCategory=Person)(objectClass=user))';

use constant DEFAULT_UPN_FILTER => '(&(mail=<principal>))';

use constant DEFAULT_ATTRIB_SET => 'samaccount=cn,country=c,fname=givenName,lname=sn,displayname=displayName,employeeid=employeeNumber,name=name,employeeid2=employeeID,email=mail';

use constant DEFAULT_DOMAIN => 'prg-dc';

use constant DEFAULT_ASSOCIATE_TABLE => 'auth_dhl_ad_assoc';

# After Bind and Search match details against entry in auth_dhl_ad_assoc using column=key
use constant DEFAULT_ASSOCIATE_MATCH => 'distinguished_name=dn,email=email,user_principal_name=userPrincipalName,sam_account_name=samaccount,default';

use constant DEFAULT_EXCLUDE_BY_ROLE => 'Vendor';

# Default for list used in get_potential_matches
use constant DEFAULT_AD_ASSOC_LIST => "email,username=samaccount,username=name,employeeid,employeeid=employeeid2,default"; # Adding default just shows that default is being used or not

# Note: Can't use print to output debug info as we defer writing the HTTP header for Login action, in case the follow
#       on action is a redirect such as masters/jrs

my $debug = 0;

# Use new in JFAuthBase.pm

# Authenticate user, given credentials (principal + password)
# details = authenticate( username, password );
#
# return hash with details (or undef if not authenticated)
# - userid	userid (query what happens for temp users, userid = 2, tempuser set to userstemp.id)
#		for Active Directory this can be undef, meaning the credentials are not associated with a Jobflow userid
# - username	username (can be present even if userid is not)
# - fname	first name
# - lname	last name
# - email	email address
# - rolename	role name (depends on userid)
# - roleid	role id   (depends on userid)
# - tempuser	0 (if userid from users table) or > 0 if id from userstemp 
# - enabled	always 1, but checked again if bound to Jobflow userid
#
# Returns undef if authentication failed.
#

sub authenticate {
	my $class = shift;
	my ( $principal, $password, $history ) = @_;

	# Use Net::LDAP to bind and search to get user credentials
	my $ldap;
	my $details;

	my @local_history;
	$history = \@local_history unless $history;  # Make history an array ref if not supplied

	my $debug = 0;

	eval {
		$debug = $class->{debug} || 0;  # override global debug

		# Read settings from ad_dhl_profile before using settings
		unless ( $class->{settings} ) {
			die "Auth id undefined" unless defined $class->{auth_id};
			die "No auth_table defined for auth id $class->{auth_id}, $class->{auth_name}" unless defined $class->{auth_table};

			$class->SUPER::init();  # in JFAuthBase
			$class->{settings} = 1;
		}

		my $min_username_len = $class->{minUsernameLength} || 3;
		die "No username" unless ( ( defined $principal ) && ( length( $principal ) > $min_username_len ) );

		my $service = $class->{service} || DEFAULT_SERVICE;
		$service =~ s/^\s*//g;
		$service =~ s/\s*$//g;
		push( @$history, "Connecting to LDAP server '$service', auth name '" . $class->{auth_name} . "'" );
		$ldap = Net::LDAP->new( $service, debug => $debug ) or die "Failed to connect to LDAP server '$service', $@";

		# The principal is either the SAM account name eg 'stevtrean' which may be qualified by the domain name eg 'prg-dc\stevtrean'
		# OR the User Principal Name which is the legacy email address eg 'streanor@tagworldwide.com';

		# carp "JFAuthAD: Self " . Dumper( $class );

		my $samFilter = $class->{samFilter} || DEFAULT_SAM_FILTER; # eg '(&(samAccountName=<principal>)(objectCategory=Person)(objectClass=user))';
		my $upnFilter = $class->{upnFilter} || DEFAULT_UPN_FILTER; # eg '(&(email=<principal>))'; or (&(userPrincipalName=<principal>))'; OR mix of both

		my $filter;
		my $default_domain = $class->{default_domain} || DEFAULT_DOMAIN;  # Most likely prg-dc
		my $domain = $default_domain;
		my $username;
		if ( $principal =~ /\@/ ) {
			$filter = $upnFilter;
			$username = $principal; # Bind and Search using email address
		} elsif ( $principal =~ /\\/ ) {
			( $domain, $username ) = split /\\/, $principal;
			$domain = $default_domain unless ( $domain );
			$filter = $samFilter;
		} else {
			$filter = $samFilter;
			$username = $principal;
			$principal = $default_domain . '\\' . $username;
		}

		# The base determines where in the LDAP structure to start so can vary say between DHL and Smoke and also according to domain
		my $base = $class->{base} || DEFAULT_BASE;  # eg 'OU=GB,DC=<domain>,DC=dhl,DC=com', OR 'c=UK'
		$base =~ s/\<domain\>/$domain/g;

		# The bind accepts the UPN (email) or the domain qualified SAM account name
		push( @$history, "Doing BIND using principal '$principal'" );
		my $mesg = $ldap->bind( $principal, password => $password );

		die "Bind error: LDAP bind failed" unless ( defined $mesg );
		die "Bind error: " . $mesg->{errorMessage} unless ( $mesg->{errorMessage} eq "" );

		# Bind ok, returned object of type 'Net::LDAP::Bind' and no errorMessage
		# Use LDAP search to find the logged on user and return a defined set of attributes
		# push( @$history, "Bind result " . Dumper( $mesg ) ) if $debug;

		# Attributes defines the set of LDAP attributes to return
		my $attrib_set = $class->{attrib_set} || DEFAULT_ATTRIB_SET;
		$attrib_set =~ s/ //g; # Remove any spaces

		my %h; # hash of key value from attribute list
		foreach my $a ( split /,/, $attrib_set ) {
			if ( $a =~ /\=/ ) {
				my ( $k, $v ) = split /=/, $a; # key is JF attr name, value is AD attr name eg samaccount=cn
				$h{ $k } = $v;
			} else {
				$h{ $a } = $a; # case when key appears on its own eg userPrincipalname
			}
		}
		# $h{ dn } = 'dn'; # always include distinguished name

		#my $attrs = [ 'cn', 'mail', 'sn', 'givenName', 'displayName', 'c', 'name', 'employeeNumber', 'employeeType', 'employeeID' ];
		my @attr_list = values %h; # values gives a list of AD attribute names
		push( @$history, "Search attrib list " . join( ",", @attr_list ) ) if $debug;

		$filter =~ s/\<principal\>/$username/gs;

		push( @$history, "Doing search with filter '$filter', base '$base', attr '" . join( ',', @attr_list ) . "'"  );

		$mesg = $ldap->search(
				base => $base,
				filter => $filter,
				attrs => \@attr_list,
		);

		die "LDAP search returned undef" unless defined $mesg;

		$mesg->code && die "LDAP Search returned error " . $mesg->error;

		die "LDAP search returned no match" if ! exists $mesg->{entries};

		# Attributes returned by AD look like
		# dn:CN=stevtrean,OU=LON-BakersYard,OU=Users,OU=WilliamsLea,OU=GB,DC=prg-dc,DC=dhl,DC=com
		#            cn: stevtrean
		#            sn: Treanor
		#             c: GB
		#     givenName: Steve
		#   displayName: Steve Treanor (TAG GB)
		# employeeNumber: 105...62
		#  employeeType: Employee
		#          name: stevtrean
		#    employeeID: 123456
		#          mail: steve.treanor@wlt.com
		# userPrincipalName: streanor@tagworldwide.com

		my $dn; # Distinguished name comes from objectName in one of the entries

		# carp "Examining LDAP search response " . Dumper( $mesg );

		# $mesg is Net::LDAP::Entry
		my $max = $mesg->count;
		my $details_2 = { };
		for ( my $i = 0 ; $i < $max ; $i++ ) {
			my $entry = $mesg->entry ( $i );
			$dn = $entry->dn() unless $dn; # Get objectName from first entry that has it

			foreach my $attr ( $entry->attributes ) {
				my $val = $entry->get_value( $attr );
				# carp " -- $attr => $val";
				$details_2->{ $attr } = $val;
			}	
		}

		foreach my $k ( keys %h ) {
			my $v = $h{ $k }; # key is JF attrib name, value is ad attrib name
			$details->{ $k } = $details_2->{ $v } || '';
		}
		$details->{dn} = $dn; # dn is special case (see objectName in dump)

		$details->{enabled} = 1;

		# push( @$history, Dumper( $details ) );  # ** DEBUG **
		push( @$history, "Check to see if user is already associated to a Jobflow account" );

		# Check to see if this user is bound to Jobflow userid
		my $associate_table = $class->{associate_table} || DEFAULT_ASSOCIATE_TABLE; 
		my $auth_id = $class->{auth_id};

		my $userid = bind_search( $class, $details, $history );
		push( @$history, "Associated with userid $userid" ) if ( defined $userid );
		# push( @$history, "Failed to associate user '$username', by SAM " . $details->{ samaccount } . ", dn " . $details->{ dn } . " or email " . $details->{ email } ) if ( ! defined $userid );
		$details->{userid} = $userid;

		$mesg = $ldap->unbind;   # take down session
		$ldap = undef;
	};
	if ( $@ ) {
		my $error = $@;
		$ldap->unbind if ( defined $ldap );
		$details = undef;
		$error =~ s/ at .*//gs;
		push( @$history, "Exception: $error" );
		my $reason = 'Authenticate failed';
		if ( $error =~ /No username/ ) { # failed minimum username length check
			$reason = 'No username';
			$error = 'Please enter a username';

		} elsif ( $error =~ /No password/ ) { # Bind error: No password, did you mean noauth or anonymous ?
			$reason = 'No password';
			$error = 'Please enter a password';

		} elsif ( $error =~ /AcceptSecurityContext error/ ) { # Bind error: 80090308: LdapErr: DSID-0C0903D0, comment: AcceptSecurityContext error, data 52e, v2580,
			$reason = 'Password invalid';
			$error = 'Invalid Username/Password';

		} else {
			$error =~ s/ at mod.*//gs;
			$reason = $error;
			$error = 'Not Authenticated';
		} 
		$class->auth_failure( $class->{auth_id}, $principal, $reason );

		if ( $debug ) {
			foreach my $msg ( @$history ) {
				warn $msg;
			}
		}
		die $error;
	}
	return $details;
}

# Match the details returned from the LDAP search against the table auth_dhl_Ad_assoc (or related)
# If row found, return the corresponding userid otherwise return undef

sub bind_search {
	my ( $class, $details, $history ) = @_; 

	my $userid; # return value

	my $associate_table = $class->{associate_table} || DEFAULT_ASSOCIATE_TABLE; 
	my $associate_match = $class->{associate_match} || DEFAULT_ASSOCIATE_MATCH;
	$associate_match =~ s/ //g; # Remove spurious spaces

	my $auth_id = $class->{auth_id};

	my @list = split /,/, $associate_match;
	my @cols;
	my @vals;
	my @p;
	foreach my $a ( @list ) {
		my $k; my $v;
		if ( $a =~ /\=/ ) {
			( $k, $v ) = split /\=/, $a;
		} else {
			$k = $a; $v = $a;
		}
		$v = $details->{ $v };
		push( @cols, "($k = ?)" );
		push( @vals, $v );
		push( @p, "($k = '$v')" );
	}

	eval {
		push( @$history, "SELECT user_id FROM $associate_table WHERE (auth_id = $auth_id) AND ( " . join( " OR ", @p ) . ")" );
		( $userid ) = $Config::dba->process_oneline_sql( 
			"SELECT user_id FROM $associate_table WHERE (auth_id = ?) AND ( " . join( " OR ", @cols ) . ")", [ $auth_id, @vals ] );
	};
	if ( $@ ) {
		my $error = $@;
		die "bind_search failed, table '$associate_table', authid '$auth_id', $error";
	}
	return $userid;
}

# Bind/Associate a Jobflow userid with a specific AD account
# called in modules/users.pl, associate_user

sub bind_auth {
	my $class = shift;
	my ( $userid, $email, $user_principal_name, $sam_account_name, $distinguished_name ) = @_;

	die "bind_auth: userid is undef" unless defined $userid;
	die "bind_auth: UPN is undef" unless defined $user_principal_name;
	die "bind_auth: SAM is undef" unless defined $sam_account_name; 
	die "bind_auth: email is undef" unless defined $email;

	# The table name is taken from the auth_systems table rather than hard coded
	# eg for auth_dhl_ad the columns are ( id, auth_id, user_id, user_principal_name, sam_account_name )
	my $associate_table = $class->{associate_table} || DEFAULT_ASSOCIATE_TABLE; 
	my $auth_id = $class->{auth_id};

	eval {
		# Check not already associated
		my ( $existing_userid ) = $Config::dba->process_oneline_sql(
			"SELECT user_id FROM $associate_table WHERE auth_id=? AND distinguished_name=?",
			[ $auth_id, $distinguished_name ] );
		die "Userid $existing_userid is already associated to $distinguished_name" if defined $existing_userid;

		$Config::dba->process_oneline_sql(
			"INSERT INTO $associate_table ( auth_id, user_id, email, user_principal_name, sam_account_name, distinguished_name ) VALUES ( ?, ?, ?, ?, ?, ? )",
			[ $auth_id, $userid, $email, $user_principal_name, $sam_account_name, $distinguished_name ] );
	};
	if ( $@ ) {
		die "JFAuthAD::bind_auth: auth_id $auth_id, userid $userid, email $email, upn '$user_principal_name' sam '$sam_account_name'" . $@;
	}
}

sub unbind_auth {
	my $class = shift;
	my ( $userid ) = @_;

	die "JFAuthAD: unbind_auth not valid for JFAuthAD package";
}

# Query the users table using the details returned by LDAP search
# to find a list of candidates to associate to.
# eg if ad_assoc_match: email,username=samaccountname,username=name,username=cn,employeeid,employeeid=employeeid2
# keys on their own are treated as if the value is the key, so email=email
# The key is the column name in table users, the value is the key in the LDAP details

sub get_potential_matches {
	my $class = shift;
	my ( $details, $history ) = @_;

	die "Missing details arg" unless defined $details;

	# Use details (key value pairs) returned by LDAP to decide which values are meaningful
	# and hence build WHERE clause. The values supplied may be absent or blank

	my $cross_match = $class->{ad_assoc_match}; # || DEFAULT_AD_ASSOC_LIST;
	$cross_match =~ s/ //g; # Strips spurious spaces

	push( @$history, "ad_assoc_match: $cross_match" );
	# push( @$history, "Details " . Dumper( $details ) );

	my @list = split /,/, $cross_match;
	my @looked_at;
	my @where;
	my @values;
	
	foreach my $kp ( @list ) {
		my $colname = $kp;
		my $k = $kp;
		if ( $kp =~ /=/ ) {
			( $colname, $k ) = split /=/, $kp;
		}
		my $v = $details->{ $k };
		next if ( ( ! defined $v ) || ( $v eq '' ) );
		next if ( $colname eq 'employeeid' ) && ( ( $v eq '123456' ) || ( $v eq '123456789' ) || ( $v eq '0' ) );

		push( @where, "$colname = ?" );
		push( @values, $v );
	}

	if ( ( scalar @where ) == 0 ) {
		push( @$history, "Supplied details " . Dumper( $details ) );
		push( @$history, "Cross match list " . Dumper( \@list ) );
		die "No details supplied for match ";
	}

	my $sql = "SELECT u.id, status AS enabled, fname, lname, username, CONCAT(u.fname, ' ', u.lname) AS fullname, email, employeeid, r.role AS rolename FROM users u " . 
			" INNER JOIN roles r ON r.id = u.role  WHERE (" . join( " OR ", @where ) . ") AND (status = 1)";

	# push( @$history, "get_potential_match: $sql" );
	# push( @$history, "Values " . Dumper( \@values ) );

	push( @$history, "Check to see if AD credentials match existing Jobflow user details" );

	my $rows = $Config::dba->hashed_process_sql( $sql, \@values );

	# Vendors / External_Users (forms) are by their very nature external, so should not have access to the DHL Active Directory

	my $exclude_by_role = Config::getConfig('userassoc_excl_by_role') | DEFAULT_EXCLUDE_BY_ROLE;  # eg "Vendor"
	# carp "userassoc_excl_by_role: '$exclude_by_role'<br>\n";

	my @assoc_ids;
	my @candidates;

	if ( ( defined $rows ) && ( scalar @{ $rows } ) ) {
		foreach my $r ( @{ $rows } ) {
			next if ( $r->{enabled} != 1 );
			next if ( $r->{rolename} =~ /$exclude_by_role/ ); # eg exclude "Vendor", "External User"
			push( @candidates, $r );
		}
	}
	if ( scalar @candidates ) { 
		push( @$history, "Candidates: " );
		foreach my $r ( @candidates ) {
			push( @$history, " - " . $r->{fname} . " " . $r->{lname} . " " . $r->{username} . " " . $r->{email} );
		}
	} else {
		push( @$history, "No candidates found" );
	}
	return \@candidates;

}

1;

