#!/usr/local/bin/perl

package JF_WSAPI;
 
use warnings;
use strict;

use Data::Dumper;
#use XML::Simple qw(:strict);

# Home grown
use lib 'modules';

require 'config.pl';  # so Config::getConfig( '...' )

# Convert hash of key values into POST argument list ie { -d key=value } *

sub getOptions {
	my ( $options ) = @_;

	my $args = "";

	foreach my $k ( keys %$options ) {
		$args .= " -d $k=" . $options->{ $k };
	}

	return $args;
}

# Get url and ssl_hack setting for the named setting

sub getSettings {
	my ( $setting ) = @_;

	my $url = Config::getConfig( $setting );
	die "No value for URL setting '$setting'" unless defined $url;

	my $ssl_hack = Config::getConfig( $setting . "_ssl_hack" ) || 0;

	return ( $url, $ssl_hack ); 
}

# Common curl options used
	# -s/--silent, Silent or quiet mode. Don't show progress meter or error messages.
	# -S/--show-error, When used with -s it makes curl show an error message if it fails.
	# -k/--insecure allow curl  to  perform  "insecure" SSL connections and transfers.

# doCurl
#
# Arguments
# - setting, name of url setting
# - url, URL after domain name substitution
# - ssl_hack, 0 or 1, if 1, adds -k option to curl
# - curlopts, general curl options, defaults to '-sS' if curlopts is undef
#
# Returns an array with 2 element
# - curl response
# - curl request ie parameters as per the options supplied (each preceded by -d), plus the URL

sub doCurl {
	my ( $setting, $url, $ssl_hack, $options, $curlopts ) = @_;

	$curlopts = '-sS' unless ( defined $curlopts );
	$curlopts .= ' -k' if ( $ssl_hack ) && ( $url =~ /https\:/ );

	my $postargs = getOptions( $options );

	my $part = $curlopts . " " . $postargs . ' "' . $url . '"';
	my $curlCommand = "curl $part 2>&1";
		
	# Log request if needed

	my $response = `$curlCommand`; 
	# Log response if needed

	die "Certificate error,\n $response" if ( $response =~ /certificate verify failed/m );

	return ( $response, $part ); # ie results of curl and the POST parameters and URL 
}
	
# Used by Jobs::requestJobCheckIn
# jfcheckinother is probably https://%domain%/cgi-bin/jobflow_checkin.cgi

sub jobflowCheckinOther {
	my ( $domainname, $options, $curlopts ) = @_;

	my $setting = 'jfcheckinother';
	my ( $url, $ssl_hack ) = getSettings( $setting );
	$url =~ s/\%domain\%/$domainname/g;

	return doCurl( $setting, $url, $ssl_hack, $options, $curlopts );
}

# ttp curl, ttpuploadother is probably https://%domain%/cgi-bin/jobflow_ttpupload.cgi

sub ttpUploadOther {
	my ( $domainname, $options, $curlopts  ) = @_;
	my $setting = 'ttpuploadother';
	my ( $url, $ssl_hack ) = getSettings( $setting );
	$url =~ s/\%domain\%/$domainname/g;
	return doCurl( $setting, $url, $ssl_hack, $options, $curlopts );
}

# archive job curl, archivejoburl is probably http://jobflowb1.production.tag.internal/cgi-bin/jf_archive.pl
# Used by Jobs::RestoreJobs

sub archiveJob {
	my ( $options, $curlopts ) = @_;

	my $setting = 'archivejoburl';
	my ( $url, $ssl_hack ) = getSettings( $setting );
	
	$curlopts = '--connect-timeout 3 --max-time 3';

	return doCurl( $setting, $url, $ssl_hack, $options, $curlopts );
}

# batchscripturl, something like http://jobflowb1.production.tag.internal/cgi-bin/jobflow_scripts/jf_batch_webservice.pl?action=%ACTION%&%CUSTOMPARAMETERS%

sub batchWebservice {
	my ( $options, $curlopts ) = @_;

	my $setting = 'batchscripturl';
	my ( $url, $ssl_hack ) = getSettings( $setting );
	# Lose the placemarkers, everything from ? onwards (as we are doing a POST)
	$url =~ s/\?.*//g; # Just want  URL not parameter place markers

	return doCurl( $setting, $url, $ssl_hack, $options, $curlopts );
}

sub fileopsOther {
	my ( $domainname, $options, $curlopts ) = @_;

	my $setting = 'fileopsother';   # http://%domain%/cgi-bin/fileops.cgi
	my ( $url, $ssl_hack ) = getSettings( $setting );
	$url =~ s/\%domain\%/$domainname/g;

	return doCurl( $setting, $url, $ssl_hack, $options, $curlopts );
}

sub exifToolOther {
	my ( $domainname, $options, $curlopts ) = @_;

	my $setting = 'exiftoolother';   # http://%domain%/cgi-bin/exiftool.cgi
	my ( $url, $ssl_hack ) = getSettings( $setting );
	$url =~ s/\%domain\%/$domainname/g;

	return doCurl( $setting, $url, $ssl_hack, $options, $curlopts );
}

sub mastersOther {
	my ( $domainname, $options, $curlopts ) = @_;

	my $setting = 'mastersother';   # http://%domain%/cgi-bin/master/imnavigate2.pl
	my ( $url, $ssl_hack ) = getSettings( $setting );
	$url =~ s/\%domain\%/$domainname/g;

	return doCurl( $setting, $url, $ssl_hack, $options, $curlopts );
}

sub test_jobflowWS {

	my $tag_env = lc( $ENV{TAG_ENV} || "DEV" );

	my $jobno = '104000005';
	$jobno = '65000050' if ( $tag_env eq 'staging' );

	#$jc->load_file( "jrs_config", $tag_env );
        #$jc->new_connection_from_configloader();
        #$jc->load_global_settings();


	eval {
		#my $response = jobflowWS( '', { action => "getjobdetails", jobid => $jobno } );
		#print "Call to jobflowWS ok, returned '$response'\n";
	};
	if ( $@ ) {
		#print "Call to jobflowWS failed, $@\n";
	}
	return 1;
}

# exit __PACKAGE__->test_jobflowWS() unless caller();

1;
