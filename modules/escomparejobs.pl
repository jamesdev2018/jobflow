#!/usr/local/bin/perl
package ESCompareJobs;

use strict;
use warnings;

use JSON;
use Data::Dumper;
use REST::Client;
use Config::Properties;
use File::Basename;
use Data::UUID;
use Net::FTP;
use URI::Escape;
use Scalar::Util qw( looks_like_number );

use lib 'modules';
use lib '.';

#use DBAgent;
use CTSConstants;
use Validate;

# Please do not add jobs.pl

require "config.pl";  # Clash package name Config with Config::properties
require "databaseJob.pl";
require "databaseUser.pl";
require "databaseOpcos.pl";
require "jobpath.pl";
require "databaseAudit.pl";
require "esapi.pl";

use constant CONFIG_PROPS => "etc/esconfig.props";

use constant CONFIG_KEY_HOST => "host";				# ES Dalim host
use constant CONFIG_KEY_ADU => "adu";
use constant CONFIG_KEY_ADP => "adp";
use constant CONFIG_KEY_JF_ORG => "jf_org";			# Organisation (under Main Organisation)
use constant CONFIG_KEY_DEFAULT_PROFILE => "default_profile";	# Default Profile
use constant CONFIG_KEY_JF_NAME_PREFIX => "jf_name_prefix";	# Jobflow name prefix eg jfd_ on DEV
use constant CONFIG_KEY_VIEWER_PATH => "viewerpath"; 		# URL for dialogue viewer applet
use constant CONFIG_KEY_RPC_PATH => "rpcpath";			# URL for RPC calls
use constant CONFIG_KEY_TIMEOUT => "timeout";	 		# REST::Client connection timeout
use constant CONFIG_KEY_WORKFLOW => "workflow";	 		# Workflow to use when creating document
use constant CONFIG_KEY_DALIM_FTP_SERVER => "dalim_ftp_server";	# FTP server for es_user (temp dir in gpfs)
use constant CONFIG_KEY_DALIM_FTP_USER => "dalim_ftp_user";	
use constant CONFIG_KEY_DALIM_FTP_PASS => "dalim_ftp_pass";
use constant CONFIG_KEY_DALIM_FTP_PATH => "dalim_ftp_path";	# chroot path as need to give Dalim full path

use constant APPROVAL_WORKFLOW_STEP => "ApprovalWFL";

our $tag_env; # One of DEV, Staging or Live

our $esconfig; # Hash contains config settings for ES

our $testuserid = 2;

our $debug = 0;

# Pick up key document states from JF config TODO

# Document state that the document should be in after we create the document
my $ES_DOCSTATUS_HELD = Config::getConfig("esdocstatus_held") || "Held,ToApprove";

# Document state that the document should be in after the user accepts the document
my $ES_DOCSTATUS_COMPLETED = Config::getConfig("esdocstatus_completed") || "Completed";

# All other settings come from Jobflow Settings but use settings in esconfig for defaults

my $es_config_host = Config::getConfig("eshost") || readConfig( CONFIG_KEY_HOST );
my $es_config_rpc_path = Config::getConfig("esrpcpath" ) || readConfig( CONFIG_KEY_RPC_PATH );
my $es_config_admin_user = Config::getConfig("esadminuser" ) || readConfig( CONFIG_KEY_ADU );
my $es_config_admin_password = Config::getConfig("esadminpassword" ) || readConfig( CONFIG_KEY_ADP );

my $es_config_timeout = Config::getConfig("estimeout") || readConfig( CONFIG_KEY_TIMEOUT ); # Timeout (secs) for REST Client
my $es_config_jobflow_name_prefix = Config::getConfig("esjfnameprefix") || readConfig( CONFIG_KEY_JF_NAME_PREFIX ); # Used for user names, groups, customers to make unique
my $es_config_jf_org = Config::getConfig("esjforg") || readConfig( CONFIG_KEY_JF_ORG ); # eg "Jobflow (Live
my $es_config_viewer_path = Config::getConfig("esviewerpath") || readConfig( CONFIG_KEY_VIEWER_PATH );

my $es_config_ftp_server = Config::getConfig("esftpserver") || readConfig( CONFIG_KEY_DALIM_FTP_SERVER );
my $es_config_ftp_user = Config::getConfig("esftpuser") || readConfig( CONFIG_KEY_DALIM_FTP_USER );
my $es_config_ftp_pass = Config::getConfig("esftppass") || readConfig( CONFIG_KEY_DALIM_FTP_PASS );
my $es_config_ftp_path = Config::getConfig("esftppath") || readConfig( CONFIG_KEY_DALIM_FTP_PATH );

my $es_config_workflow = Config::getConfig("esworkflow") || readConfig( CONFIG_KEY_WORKFLOW );
my $es_config_default_profile = Config::getConfig("esdefaultprofile") || readConfig( CONFIG_KEY_DEFAULT_PROFILE );

sub readConfig {

	my ( $key ) = @_;

	die "Invalid key" unless ( ( defined $key ) && ( $key ne "" ) );

	if ( ! defined $esconfig ) {

		$tag_env = $ENV{"TAG_ENV"};
		die "TAG_ENV not defined" unless ( defined $tag_env );

		open my $fh, '<', CONFIG_PROPS or die "Unable to open configuration file " . CONFIG_PROPS;
  		my $properties = Config::Properties->new();
  		$properties->load($fh);
		$fh->close();

		my $tree = $properties->splitToTree();
		$esconfig = $tree->{ lc($tag_env) };

		die "ES config doesnt define host" unless ( exists $esconfig->{host} );
	}

	return $esconfig->{ lc( $key ) };;
}

# Getter/Setter for debug

sub setDebug {
	( $debug ) = @_;
}

sub getDebug {
	return $debug;
}

# Connect using admin

sub getAdminClient {
	my $client = ESAPI::connect( $es_config_host, $es_config_rpc_path, $es_config_admin_user, $es_config_admin_password, $es_config_timeout );
	return $client;
}

# Connect using a specific user

sub getUserClient {
	my ( $user, $pass ) = @_;

	my $client = ESAPI::connect( $es_config_host, $es_config_rpc_path, $user, $pass, $es_config_timeout );
	return $client;
}

# Handler for action "escompare"

sub actionHandler {
	my ( $query, $session ) = @_;

	my $subaction = $query->param( "subaction" ) || "";
	my $results;

	eval {
		if ( $subaction eq "getversions" ) {
			my @jobslist = $query->multi_param("jobslist[]");	
			$results = getVersions( $session->param( 'userid' ), \@jobslist );

		} elsif ( $subaction eq "getcredentials" ) {
			$results = getCredentials( $session->param( 'userid' ), $session->param( 'username' ) );

		} elsif ( $subaction =~ /(setup|create|register)/ ) { 
			my $job1 = $query->param( 'job1' );
			my $job2 = $query->param( 'job2' );
			my $ver1 = $query->param( 'ver1' );
			my $ver2 = $query->param( 'ver2' );
			my $docID = $query->param( 'docID' );
			$results = compare( $session->param( 'userid' ), $session->param( 'username' ), $subaction, $job1, $ver1, $job2, $ver2, $docID );

		} elsif ( $subaction eq "review" ) {
			my $job = $query->param('job');
			$results = getReview( $session->param( 'userid' ), $session->param( 'username' ), $job,
						$session->param( 'fname' ), $session->param( 'lname' ), $session->param( 'email' ) );

		} elsif ( $subaction eq "compareexists" ) {
			my $job1 = $query->param( 'job1' );
			my $job2 = $query->param( 'job2' );
			my $ver1 = $query->param( 'ver1' );
			my $ver2 = $query->param( 'ver2' );
			my $timestamp1 = $query->param( 'timestamp1' );
			my $timestamp2 = $query->param( 'timestamp2' );
			$results = checkCompareExists( $job1, $ver1, $timestamp1, $job2, $ver2, $timestamp2 );

		} elsif ( $subaction eq "getdocstatus" ) {
			my $docid = $query->param( 'docID' );
			$results = getDocumentStatus( $docid );

		} elsif ( $subaction eq "getannotations" ) {
			my $docid = $query->param( 'docid' );
			$results = getAnnotations( $session->param( 'userid' ), $session->param( 'username' ), $docid );

		} elsif ( $subaction eq "logview" ) {
			my $docid = $query->param( 'docID' );
			$results = logView( $session->param( 'userid' ), $session->param( 'username' ), $docid );

		} else {
			die "Unrecognised subaction '$subaction' for action escompare";
		}
	};
	if ( $@ ) {
		my $error = $@;

		#print "Error: $error\n";

		my $vars = { error => $error, params => $query->Vars };
		$results = encode_json( $vars );
	}
	print "$results\n";
	
}

# Test to see if user has access to customer

sub userCanViewCustomer {
	my ( $userid, $customerId ) = @_;

	my ( $canView ) = $Config::dba->process_oneline_sql(
		"SELECT 1 FROM customers c, customers c2, customeruser cu " .
		" WHERE cu.userid = ? AND cu.customerid = c2.id AND c2.customer = c.customer AND c.id = ? LIMIT 1",
		[ $userid, $customerId ] );

	return $canView || 0;
}

# get directory contents (using FTP ls)

sub remoteFtpLs {
	my ( $ftpserver, $ftpusername, $ftppassword, $directory, $pattern ) = @_;

	my $ftp;
	my @filelist;
	eval {
		$ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to FTP server '$ftpserver' : $@";
		$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login to FTP server '$ftpserver' as '$ftpusername'", $ftp->message;
		$ftp->cwd( $directory ) or die $ftp->message . " to $directory";
		if ( defined $pattern ) {
			@filelist = $ftp->ls( $pattern );
		} else {
			@filelist = $ftp->ls();
		}
		$ftp->close();
	};
	if ( $@ ) {
		my $error = $@;
		$ftp->close() if ( defined $ftp ); $ftp = undef;
		die "No directory, '$directory'" if ( $error =~ /^Failed to change directory/ );
		die "FTP error on connect or ls: $error";
	}
	return @filelist;
}

# Get versions for the supplied job numbers

sub getVersions {

	my ( $userid, $jobslist ) = @_;

	my @results;

	foreach my $jobid ( @$jobslist ) {

		# getJobVersions returns 0 if jobid is invalid, an empty array if there are no versions
		# or an array of hash { versionid, filename }

		my @history;
		my $jobArchived = 0;
		my $path;

		eval {
			die "Invalid job number" unless ( ( defined $jobid ) && looks_like_number( $jobid ) );

			# Could use 
			# my @versions = ECMDBJob::getJobVersions( $jobid );
			# but this returns the rows in descending id order (so oldest last)
			# The id column is unique (the version column is no longer used) - snippet from databaseJob.pl, getJobVersions

			my ( $customerId, $jobStatus ) = $Config::dba->process_oneline_sql( "SELECT customerid, status FROM tracker3plus WHERE job_number = ?", [ $jobid ] );
			die "Job doesnt exist" unless defined $customerId;

			# Verify user has access to customer and hence job
			die "Customer not visible" unless userCanViewCustomer( $userid, $customerId );

			# Going to use FTP whether the job is archived or not

			my $opcoid = ECMDBOpcos::getCustomerOpcoid( $customerId ); # SELECT opcoid FROM CUSTOMERS WHERE id = ?

			my ( $ftpserver, $ftpusername, $ftppassword ) = ECMDBOpcos::db_getFTPDetails( $opcoid );
			die "FTP Server not defined for Operating company ($opcoid)" if ( ( ! defined $ftpserver ) || ( $ftpserver eq "" ) );

			# So job exists (though may be has been archived) and user can view job
			$jobArchived = 1 if ( $jobStatus == 6 );

			if ( $jobArchived ) {
				push( @history, "Job exists but archived" );

				# If job has been archived, then contents of VERSIONS sub directory will no longer exist,
				# although OUTPUT_FILE (containing hi-res) should still exist.

				( $path ) = JobPath::getJobOutputFilePath( $jobid, $customerId );
				my $highResFile = $jobid . ".pdf";				# Look for hires file <job number>.pdf
				push( @history, "Using path $path, file $highResFile" );

				my @filelist= remoteFtpLs( $ftpserver, $ftpusername, $ftppassword, $path, $highResFile );
				push( @history, "ftp ls returned " . (scalar @filelist) . " file(s)" );

				if ( ( scalar @filelist ) == 1 ) {
					push( @results, { jobid => $jobid, archived => 1, path => $path, file => $highResFile, found => 1, history => \@history } );
				} else {
					push( @history, "Archived but no High Res '$highResFile' in OUTPUT_FILE folder" );
					push( @results, { jobid => $jobid, archived => 1, path => $path, file => $highResFile, found => 0, history => \@history } );
				}

			} else {
				push( @history, "Job exists and online" );
				( $path ) = JobPath::getJobVersionPath( $jobid, $customerId );
				push( @history, "Using path $path" );

				my @filelist= remoteFtpLs( $ftpserver, $ftpusername, $ftppassword, $path );
				push( @history, "ftp ls returned " . (scalar @filelist) . " file(s)" );

				push( @history, "Getting versions" );
				my $versions = $Config::dba->hashed_process_sql(
					"SELECT id AS versionid, version, filename, rec_timestamp FROM JOBVERSIONS WHERE job_number = ? ORDER BY id ASC", [ $jobid ]);
				push( @history, "Found " . (scalar @$versions) . " row(s)" );

				# Match up @listlist with $versions, set found flag for those that match
				# convert @filelist to hash to make lookup easier.
				my %contents;
				foreach my $f ( @filelist ) {
					$contents { $f } = 1;
				}
				my $versionsExist = 0;
				foreach my $v ( @$versions ) {
					my $filename = $v->{filename};
					# Shorten the filename (remove the jobnumber- prefix)
					$filename =~ s/^$jobid\-//g;
					$v->{shortfilename} = $filename;
					if ( exists $contents{ $v->{filename} } ) {
						$v->{found} = 1;
						$versionsExist++;
					} else {
						$v->{found} = 0;
					}
				}

				push( @results, { jobid => $jobid, archived => 0, versions => $versions, 
						filelist => \@filelist, history => \@history, versionsexist => $versionsExist } );
			}
		};
		if ( $@ ) {
			# /^No directory/ - directory doesnt exist in remoteFtpLs (so no VERSIONS or OUTPUT_FILE directory)
			# /Job doesnt exist/ - job doesnt exist on tracker3plus
			# /Customer not visible/ - user doesnt have right to view customer of job
			my $error = $@;
			if ( $error =~ /No directory/ ) {  # from ftp ls
				if ( $jobArchived ) {
					push( @history, "Job is archived, but OUTPUT_FILE directory doesnt exist" );
					push( @results, { jobid => $jobid, archived => 1, found => 0, path => $path, history => \@history } ); # No path/file keys
				} else {
					push( @history, "Job is online, but VERSIONS directory doesnt exist" );
					push( @results, { jobid => $jobid, archived => 0, found => 0, path => $path, history => \@history } ); # No versions key
				}
			} else {
				push( @history, "Some other error, $error" );
				$error =~ s/at module.*//g;
				push( @results, { jobid => $jobid, error => $error, found => 0, path => $path, history => \@history } );
			}
		}
	}

	my $vars = { versions => \@results };
        my $json = encode_json ( $vars );
        return $json;
}

# Dont want to drag the Users package in as well, so replicate getTempUsername from modules/users.pl

# Generate temporary userid and password 
# see http://www.perlmonks.org/?node_id=233023

sub rndStr{ join'', @_[ map{ rand @_ } 1 .. shift ] }
	
sub getTempUsername {

	my $username_len = 8 + int(rand(4)); # So userid is between 8..12 chars
	my $username = rndStr $username_len, 'A'..'Z', 'a'..'z', '0'..'9';

	return $username;
}

# Ensure user exists in both Jobflow and Dalim.

sub ensureESUserExists {
	my ( $client, $jf_user, $es_user, $orgUnit, $defaultProfile, $history ) = @_;

	my $es_pass;
	my $userDetails;

	print "ensureESUserExists: jfuser $jf_user, esuser $es_user, orgUnit $orgUnit, profile $defaultProfile\n" if $debug;

	# Find user in Jobflow

	push( @$history, "Looking up user '$jf_user' in JF users" );
	my ( $userid, $fname, $lname, $email ) = $Config::dba->process_oneline_sql("SELECT id, fname, lname, email FROM users WHERE username=?", [ $jf_user ], 4 );
	die "Not a valid Jobflow user '$jf_user'" if ( ! defined $userid );
	print "ensureESUserExists: jfuser $jf_user, userid $userid, fname '$fname', lname '$lname', email '$email'\n" if $debug;

	# See if user exists in Dalim

	my $existsInDalim = 0;
	eval {
		$userDetails = ESAPI::getUser( $client, { ID => $es_user } );
		push( @$history, "User '$es_user' exists in Dalim" );
		$existsInDalim = 1;
	};
	if ( $@ ) {
		die "Error looking for user '$es_user' in Dalim, $@" if ( $@ !~ /Cannot find User/ );
		push( @$history, "User '$es_user' doesnt exist in Dalim" );
	}

	# Then check JF esusers table to see if user has already been created in Dalim ES

	my $existsInJobflow = 0;
	( $es_pass ) = $Config::dba->process_oneline_sql("SELECT password FROM esusers WHERE username=?", [ $es_user ], 1 );
	if ( defined $es_pass ) {
		push( @$history, "User '$es_user' exists in JF esusers table" );
		$existsInJobflow = 1;
	}

	if ( $existsInDalim ) {
		if ( $existsInJobflow == 0 ) {
			# Delete from Dalim as we don't know the password (would be nice if we could just reset its password)

			ESAPI::deleteUser( $client, $es_user ); 
			push( @$history, "Deleted user '$es_user' in Dalim, as no credentials in Jobflow" );
			$existsInDalim = 0;

		} else {
			# Try and authenticate with credentials from Jobflow
			eval {
				my $userClient = getUserClient( $es_user, $es_pass );
				my $adminVersion = ESAPI::adminGetVersion( $userClient ); # Should throw error if authentication fails
				push( @$history, "Authenticated user '$es_user' in Dalim" );
			};
			if ( $@ ) {
				my $error = $@;
				push( @$history, $error );
				if ( $error =~ /HTTP 401/ ) {
					# Delete from Dalim as the password we have in Jobflow isnt recognised
					push( @$history, "Deleting user '$es_user' in Dalim, password mismatch" );
					ESAPI::deleteUser( $client, $es_user ); 
					push( @$history, "Deleted user '$es_user' in Dalim" );
					$existsInDalim = 0;
				} else {
					push( @$history, "Error $error, trying to authenicate" );
					die $error;
				}
			}
		}
	}

	if ( $existsInJobflow == 0 ) { # implies doesnt exist in Dalim either, so add to both

		# Add to JF esusers table
		$es_pass = getTempUsername();  # Create random password
		$Config::dba->process_oneline_sql("INSERT INTO esusers ( username, password, userid ) VALUES ( ?, ?, ? )", [ $es_user, $es_pass, $userid ] );
		push( @$history, "Created user '$es_user' in JF esusers table" );
	}

	if ( $existsInDalim == 0 ) {

		$fname = $fname || "";
		$lname = $lname || "";
		$email = $email || "";
		eval {
			ESAPI::createUser( $client, { name => $es_user, orgUnit => $orgUnit, defaultProfile => $defaultProfile,
							login => $es_user, password => $es_pass, 
							firstName => $fname, lastName => $lname, eMail => $email } );
			push( @$history, "Created user '$es_user' in Dalim" );
		};
		if ( $@ ) {
			if ( $@ =~ /AlreadyExists/ ) {
				push( @$history, "Failed to create user '$es_user' in Dalim, already exists in Dalim" );
				die "User '$es_user' already exists in Dalim ES";
			} elsif ( $@ =~ /DirectoryException/ ) { 
				# error message probably means that $orgUnit doesnt exist
				die "Error creating user '$es_user', orgUnit '$orgUnit', defaultProfile '$defaultProfile', login '$es_user',  $@";
			} else {
				push( @$history, "Failed to create user '$es_user' in Dalim, $@" );
				die "Error creating user '$es_user' in Dalim,  $@";
			}
		}
	}
	return ( $es_pass );
}

# Get ES username and password for current user
# Used by dalim/dalimproxy.pl, getJobflowCredentials() which takes the CGISESSID, passes it to Jobflow, which thus has the userid, username
# then uses that to query esusers and find the row matching by username prefixed with the JF prefix.

sub getCredentials {
	my ( $userid, $username ) = @_;

	my $vars;
	my $es_user = $es_config_jobflow_name_prefix . $username;
	eval {
		my ( $es_pass ) =  $Config::dba->process_oneline_sql("SELECT password FROM esusers WHERE username = ?", [ $es_user ], 1 );
		die "No row on esusers" if ( ! defined $es_pass );

		$vars = { userid => $userid, username => $es_user, password => $es_pass, };
	};
	if ( $@ ) {
		$vars = { userid => $userid, error => "Failed to get credentials for ES user $es_user, $@" };
	}
        my $json = encode_json ( $vars );
        return $json;
}

# Helper to add a user to a group, creating the group if required

sub ensureUserInGroup {
	my ( $adminClient, $user, $groupName, $orgUnit, $desc, $history ) = @_;

	my $results; # results from getGroup

	push( @$history, "ensureUserInGroup: user $user, group $groupName" );

	eval {
		$results = ESAPI::getGroup( $adminClient, { ID => $groupName } );
		# Group exists (results->{ID} is group name, results->userList is array of hash { ID = username, class => "User" } 
		print "getGroup returned " . Dumper( $results ) . "\n" if $debug; 
	};
	if ( $@ ) {
		die "Error getting group '$groupName' $@" unless ( $@ =~ /Directory server/ ); #  Directory server: Unkown Entry
	}

	if ( defined $results ) { # Group exists, but is user in group
		push( @$history, "group $groupName exists" );
		eval {
			my $foundUser = 0;
			foreach my $h ( @{$results->{userList}} ) {
				if ( $h->{ID} eq $user ) {
					$foundUser = 1;
					push( @$history, "user $user exists in group $groupName" ); 
					last;
				}
			}
			if ( $foundUser == 0 ) {
				ESAPI::addUserToGroup( $adminClient, { ID => $groupName, userList => [ $user ] } );
				push( @$history, "Added user $user to group $groupName" );
			}

		};
		if ( $@ ) {
			push( @$history, "Failed to add user $user to group $groupName, $@" );
			die "Error getting adding user $user to group '$groupName' $@"; 
		}

	} else { # Group doesnt exist
		eval {
			push( @$history, "Creating group $groupName with user $user" );
			ESAPI::createGroup( $adminClient, { orgUnit => $orgUnit, 
				name => $groupName, description => $desc, userlist => [ 'admin', $user ] } );
			push( @$history, "Created group $groupName with user $user" );
		};
		if ( $@ ) {
			push( @$history, "Failed to create group $groupName, $@" );
			die "Failed to create group $groupName, $@";
		}
	} 

}

sub ensureCustomerExists {
	my ( $adminClient, $orgUnit, $esCustomer, $history ) = @_;

	my $custID;
	eval {
		my $results = ESAPI::getCustomer( $adminClient, { path => $esCustomer } );
		$custID = $results->{ID};
		push( @$history, "Customer $esCustomer ($custID) exists" );
	};
	if ( $@ ) {
		die "Error from getCustomer, $@\n" unless ( $@ =~ /Cannot find Customer at path/ );
		eval {
			my $results = ESAPI::createCustomer( $adminClient, { name => $esCustomer });
			$custID = $results->{ID};
			push( @$history, "Customer $esCustomer ($custID) created" );

			# Add customer to orgUnit
			ESAPI::addCustomerToOU( $adminClient, { ID => $orgUnit, customerList => [ $esCustomer ] } );
		};
		if ( $@ ) {
			die "Error from createCustomer or addCustomerOU, $@";
		}
	}
	return $custID;
}

sub ensureJobExists {
	my ( $client, $esCustomer, $custID, $job, $workflow, $history ) = @_;

	my $jobID;
	my $jobPath = "/$esCustomer/$job";
	print "jobPath $jobPath workflow $workflow\n" if $debug;

	eval {
		my $results = ESAPI::getJob( $client, { path => $jobPath } ); # Throws exception if job not exists
		if ( ( defined $results ) && ( exists $results->{ID} ) ) {
			$jobID = $results->{ID};
			if ( ( exists $results->{customerID} ) && ( $results->{customerID} ne $custID ) ) {
				push( @$history, "Deleting Job $job ($jobID) as its customerid " . $results->{customerID} . " doesnt match $custID" );
				ESAPI::deleteJob( $client, { path => $jobPath } ); # job.delete only accepts job path, not jobID
				push( @$history, "Deleted job $job ($jobID)" );
				$jobID = undef;
			} else {
				push( @$history, "Job $job ($jobID) exists" );
			}
		}
	};
	if ( $@ ) {
		die "Exception from getJob or deleteJob, job '$job', customer '$esCustomer', $@\n" unless $@ =~ /Cannot find Job/;
	}
	if ( ! defined $jobID ) {
		push( @$history, "Creating Job $job" );
		eval {
			my $results = ESAPI::createJob( $client, { customerName => $esCustomer, jobName => $job, documentWorkflow => $workflow } );
			$jobID = $results->{ID};
			push( @$history, "Created Job $job ($jobID)" );
		};
		if ( $@ ) {
			die "Error from createJob, job '$job', customer '$esCustomer', workflow '$workflow', $@";
		}
	}
	return $jobID;
}

# getDocWorkflowStep - Look at the document workflow and find the named step
# If found returns a hash ref with { stepID, status }, otherwise throw exception
# status is one of "Held", "Waiting", "Completed" or "Aborted" although there could be other states too.
# In Dalim ES 5.0, there are additional states
# "Design Approved", "Zip Uploaded", "In Production", "Indesign On Revision",
# "In Approval", "Approved", "Aborted",  "Empty", "Unknown", "Check preflight", "FullEd", 

sub getDocWorkflowStep {
	my ( $client, $docID, $workflow, $stepName ) = @_;

	my $results = ESAPI::getWorkflow( $client, { ID => $docID, class => "PageOrder", name => $workflow } );
	die "No stepList for workflow '$workflow' docID '$docID'" unless ( exists $results->{stepList} );
	die "No workflowName for workflow '$workflow' docID '$docID'" unless ( exists $results->{workflowName} );
	
	print "getDocWorkflowStep for doc $docID, workflowName " . $results->{workflowName} . ", ID " . $results->{ID} . ", class " . $results->{class} . "\n" if $debug;
	
	my $stepList = $results->{stepList};
	my $m;

	foreach my $s ( @{ $stepList } ) {
		if ( $s->{stepName} eq $stepName ) {
			$m = $s;
			last;
		}
	}
	die "No step '$stepName' in $workflow" if ( ! defined $m );
	return $m;
}


# Helper for compare, use job number and version id (from jobversions.id) to get the version number, filename, timestamp and full path
# Arguments
# - job - job number
# - versionid - id from row in jobversions table (or undef if job archived)
# Returns
# - filename, name of PDF 
# - version, major version number OR undef if job archived
# - timestamp, jobversions.timestamp OR undef if job archived
# - path, path to filename (but excluding filename)
# - desc, short description, eg "version <n> <timestamp>" OR "Hi-Res" if archived
# - part, part of the final ES doc name, based on the filename

sub getJobVersionDetails {
	my ( $job, $versionid ) = @_;

	my ( $filename, $version, $timestamp, $path, $desc, $part );
 
	if ( ( $versionid || "" ) eq "" ) {

		# Compared job is archived, so look for high res file in ouput file folder

		( $path ) = JobPath::getJobOutputFilePath( $job );
		$filename = $job . ".pdf";
		$version = undef;
		$timestamp = undef;
		$desc = "Hi-Res";
		$part = "_hires";
	} else {
 
		( $filename, $version, $timestamp ) = $Config::dba->process_oneline_sql(
			 "SELECT filename, version, rec_timestamp FROM jobversions WHERE job_number = ? AND id = ?", [ $job, $versionid ], 3 );

		( $path ) = JobPath::getJobVersionPath( $job );
		$desc = "version " . $version . " " . $timestamp;

		# Shorten the filename, eg 104000004-v2-2015-03-25-15-34-00.pdf => v2-2015-03-25-15-34-00
		$part = $filename;
		$part =~ s/^$job\-//g;
		$part =~ s/\.pdf$//g;
	}

	return ( $filename, $version, $timestamp, $path, $desc, $part );
}

# Given 2 pairs of ( job, version, timestamp ), find matching row on escomparehistory

sub findComparison {
	my ( $job1, $version1, $timestamp1, $job2, $version2, $timestamp2  ) = @_;

	my $sql = "SELECT h.id, h.userid, u.username, h.docid, h.rec_timestamp FROM escomparehistory AS h " .
			" INNER JOIN users AS u ON u.id = h.userid " .
			" WHERE (h.job1 = ?) AND (h.job2 = ?) AND (h.deleted IS NULL) ";
	my @values;
	push( @values, $job1, $job2 );

	if ( defined $version1 ) {
		$timestamp1 = uri_unescape( $timestamp1 ) if defined $timestamp1;
		$sql .= " AND (version1 = ?) AND (timestamp1 = ?) ";
		push( @values, $version1, $timestamp1 );
	} else {
		$sql .= " AND (version1 IS NULL) AND (timestamp1 IS NULL) ";
	}

	if ( defined $version2 ) {
		$timestamp2 = uri_unescape( $timestamp2 ) if defined $timestamp2;
		$sql .= " AND (version2 = ?) AND (timestamp2 = ?) ";
		push( @values, $version2, $timestamp2 );
	} else {
		$sql .= " AND (version2 IS NULL) AND (timestamp2 IS NULL) ";
	}

	my ( $histId, $cr_userid, $cr_username, $docID, $cr_timestamp ) = $Config::dba->process_oneline_sql( $sql, \@values );

	return ( $histId, $cr_userid, $cr_username, $docID, $cr_timestamp );
}

# Mark the history record (found by findComparison) as deleted

sub deletePreviousHistory {
	my ( $id, $history ) = @_;
	eval {
		push( @$history, "Marking previous comparison as deleted, id $id" );
		$Config::dba->process_oneline_sql(
			"UPDATE escomparehistory SET deleted = CURRENT_TIMESTAMP WHERE id = ?", [ $id ] );
	};
	if ( $@ ) {
		my $error = $@;
		# $error =~ s/at module.*//g;
		push( @$history, "Error marking previous history deleted $error, id $id" );
	}
}

# Helper for compare

sub transfer_from_gpfs_to_holding {
	my ( $tempdir, $jobid, $jobVersionPath, $filename ) = @_;

	die "transfer_from_gpfs_to_holding: tempdir '$tempdir' doesnt exist" unless -e $tempdir;

	# Use job, get customerid, opcoid, job version path, ftp creds
	my $customerId = ECMDBJob::db_getCustomerIdJob( $jobid );
	my $opcoid = ECMDBOpcos::getCustomerOpcoid( $customerId );

	my ( $ftpserver, $ftpusername, $ftppassword ) = ECMDBOpcos::db_getFTPDetails( $opcoid );
	die "FTP Server not defined for Operating company ($opcoid)" if ( ( ! defined $ftpserver ) || ( $ftpserver eq "" ) );

	# Use FTP to transfer version file from studio GPFS to $tempdir

	my $ftp;
	my @filelist;
	eval {
		$ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to FTP server '$ftpserver', opcoid $opcoid : $@";
		$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login to FTP server '$ftpserver' as '$ftpusername'", $ftp->message;
		$ftp->binary();
		$ftp->cwd( $jobVersionPath ) or die $ftp->message . " to $jobVersionPath";
		$ftp->get( $filename, "$tempdir/$filename" ); 
		$ftp->close();
		$ftp = undef;
	};
	if ( $@ ) {
		my $error = $@;
		$ftp->close() if ( defined $ftp ); $ftp = undef;
		die "No VERSIONS directory, '$jobVersionPath'" if ( $error =~ /^Failed to change directory/ );
		die "FTP error on connect or ls: $error";
	}
	die "Failed to get version $jobid, $tempdir$filename" unless -e "$tempdir$filename";
}

sub transfer_to_local_gpfs {
	my ( $tempdir, $filename, $subdir, $ftpserver, $ftpusername, $ftppassword ) = @_;
 
	# Use FTP to transfer file back to Dalim holding directory on local gpfs
	# Note the account is chrooted, so the jobVersionPath is relative

	my $ftp;
	eval {
		$ftp = Net::FTP->new( $ftpserver, Debug => 0 ) or die "Cannot connect to FTP server '$ftpserver' : $@";
		$ftp->login( $ftpusername, $ftppassword ) or die "Cannot login to FTP server '$ftpserver' as '$ftpusername'", $ftp->message;
		$ftp->binary();
		$ftp->mkdir( $subdir, 1 ) or die $ftp->message . " to $subdir"; # Mkdir and all sub dirs 
		$ftp->cwd( $subdir ) or die $ftp->message . " to $subdir"; 
		# nb put doesnt return undef on error unlike get
		$ftp->put( "$tempdir/$filename" ); 
		my @ls = $ftp->ls();
		die "Contents of $subdir/ : '" . join( ",", @ls ) . "'" unless ( ( scalar @ls ) == 1 );
		$ftp->close();
		$ftp = undef;
	};
	if ( $@ ) {
		my $error = $@;
		$ftp->close() if ( defined $ftp ); $ftp = undef;
		die "FTP error on connect or ls: '$error'";
	}
}

# Handler for  "compare" - well actually "setup", "create" and "register" sub actions
# Arguments 
# - userid (id from jf users
# - username unix style username
# - job1 1st job number
# - versionid_1 (jobversions.id of 1st version) or undef (if archived)
# - job2 2nd job number
# - versionid_2 (jobversions.id of 2nd version) or undef (if archived)
#
# Returns path of version in /gpfs (which is mounted on Dalim ES server)
# Throws exception if:
# a) corresponding userid not in esusers (front end deals with calling adduser subaction)
#
# use FTP again to copy the version files from gpfs to a different folder on gpfs, then specify that folder to Dalim,
#  when using document.create and document.register (as Dalim appears to delete the files or move them elsewhere)
# Need to add extra fields in etc/esconfig.props to hold the FTP credentials (its a mess as we are using live gpfs for dev/staging testing)
# So for the copy from the studio, use the FTP credentials from the database, 
# but for the onward copy, need the live gpfs credentials and a local on gpfs to copy to (& pass to Dalim)
# Agreed with Ross that we will create a directory under the jobs VERSIONS directory with the 2 versions to import.

sub compare {
	my ( $userid, $username, $subaction, $job1, $versionid_1, $job2, $versionid_2, $docID ) = @_;

	my @paths; # array of 1 or 2 job paths
	my $vars;
	my $jobID;
	my $custID;
	my @history; 
	my $fileURL;  # used for Document.create and .register

	eval {
		die "Userid is invalid" if ( ! $userid );
		die "Username is invalid" if ( ! $username );

		# Get constants from esconfig.props

		my $jf_prefix = $es_config_jobflow_name_prefix; # Used for user names, groups, customers to make unique
		my $orgUnit = $es_config_jf_org; # eg "Jobflow (Live)"
		my $docURL = $es_config_viewer_path;
		my $timeout = $es_config_timeout;
		my $rpcpath = $es_config_host . $es_config_rpc_path;
		my $gpfs_ftp_server = $es_config_ftp_server;
		my $gpfs_ftp_user = $es_config_ftp_user;
		my $gpfs_ftp_pass = $es_config_ftp_pass;
		my $gpfs_ftp_path = $es_config_ftp_path;
		my $workflow = $es_config_workflow;
		my $defaultProfile = $es_config_default_profile;
	
		# Get info from jobversions, nb version1 and version2 are taken from jobversions.id 

		push( @history, "Getting job details" );

		my ( $v1_filename, $v1_version, $v1_timestamp, $job1_version_path, $v1_desc, $v1_part ) = getJobVersionDetails( $job1, $versionid_1 );
		my ( $v2_filename, $v2_version, $v2_timestamp, $job2_version_path, $v2_desc, $v2_part ) = getJobVersionDetails( $job2, $versionid_2 );

		push( @paths, $job1_version_path . $v1_filename );
		push( @paths, $job2_version_path . $v2_filename );

		# Get customer id, name from job1 (so document is created under that customer in Dalim ES)

		my $job = $job1; 	# Use 1st job number supplied for customer lookup
		my $customerId = ECMDBJob::db_getCustomerIdJob( $job );
		die "Customer id invalid for job $job" unless defined $customerId;
		my ( $customer ) = $Config::dba->process_oneline_sql("SELECT customer FROM customers WHERE id=? ", [ $customerId ], 1 );
		die "Customer id $customerId invalid for job $job" unless defined $customer;

		# Use users username to get ES username

		my $adminClient = getAdminClient();

		my $es_user = $jf_prefix . $username;
		my $es_pass;
		if ( $subaction eq "setup" ) {
			( $es_pass ) = ensureESUserExists( $adminClient, $username, $es_user, $orgUnit, $defaultProfile, \@history  );
		} else {
			( $es_pass ) = $Config::dba->process_oneline_sql("SELECT password FROM esusers WHERE username=?", [ $es_user ], 1 );
			die "No entry for user '$es_user'" unless ( defined $es_pass );
		}

		# Check Jobflow users group exists and that user is in the group

		my $jfGroupName = uc( $jf_prefix ) . "users";   # Group eg jfl_users, set of all Jobflow users
		print "Group name '$jfGroupName'\n" if $debug;
		ensureUserInGroup( $adminClient, $es_user, $jfGroupName, $orgUnit, "Jobflow users group", \@history ) if ( $subaction eq "setup" );

		# Check whether customer exists, create if not (created by user, not admin)

		my $esCustomer = $jf_prefix . $customer; # Ensure customer name isnt reused by something else

		# Check that <customer> group exists and add user to it (in this instance orgUnit is the customer)
		# actual add a bit further down.

		my $esCustomerGroup = uc( $jf_prefix ) . "c_" . $customer;  # Need the customer group name for the approval list

		# Everything created under the customer should be accessible by any user who has been added to the group associated with the customer.

		my $client = getUserClient( $es_user, $es_pass );

		# Create document name for Dalim ES

		my $description = "Comparing job $job1 $v1_desc and job $job2 $v2_desc";
		my $docName = $job1 . "_" . $v1_part . "_v_" . $job2 . "_" . $v2_part;
		my $docStatus = ""; # Set for register

		if ( ( $subaction eq "setup" ) || ( $subaction eq "create" ) ) {

			# Check customer exists in Dalim ES

			$custID = ensureCustomerExists( $adminClient, $orgUnit, $esCustomer, \@history );
			ensureUserInGroup( $adminClient, $es_user, $esCustomerGroup, $esCustomer, "Jobflow customer group", \@history ) if ( $subaction eq "setup" );

			# Check job exists, create if not

			$jobID = ensureJobExists( $client, $esCustomer, $custID, $job, $workflow, \@history );

		} elsif ( $subaction eq "register" ) {

			die "Invalid docID supplied" unless ( ( defined $docID ) && ( looks_like_number( $docID ) ) );

			# Get the document details and hence jobID, name

			my $results = ESAPI::getDocument( $client, { ID => $docID } );
			die "Document $docID doesn't exist" unless ( defined $results );
			$docName = $results->{name};
			$jobID = $results->{jobID};
			$workflow = $results->{documentWorkflow};
			$description = $results->{description};

			# Get the WFL_DIALOG workflow for this document is running and check that the Approval workflow step is Held
			# - Throws exception if not found

			my $m = getDocWorkflowStep( $client, $docID, $workflow, APPROVAL_WORKFLOW_STEP );
			my $approvalStepID = $m->{stepID};
			my $approvalStepStatus = $m->{status};
			push( @history, "Found workflow $workflow step " . APPROVAL_WORKFLOW_STEP . ", stepID '$approvalStepID', status '$approvalStepStatus'" );

			# Really, the workflow should have been started and the approval step should be Held at this point, but if things are slow
			# then the approval step may be "Waiting" or even ""

			# Throw this exception if not in the correct state, gives front end the option f waiting and retrying later
			die "Approval step is in state '$approvalStepStatus'" unless ( $approvalStepStatus eq "Held" );

			#if ( $approvalStepStatus eq "Waiting" ) {
			#	# Start the workflow
			#	push( @history, "Starting Workflow $workflow" );
			#	ESAPI::startWorkflow( $client, { ID => $docID, class => "PageOrder", name => $workflow } );
			#	die "Approval step is 'Waiting'";
			#
			#} elsif ( $approvalStepStatus ne "Held" ) {
			#	# Restart the Approval workflow step (was Completed or Aborted)
			#	push( @history, "Starting Workflow $workflow step " . APPROVAL_WORKFLOW_STEP );
			#	ESAPI::restartWorkflow( $client, { ID => $docID, class => "PageOrder", name => $workflow, stepID => $approvalStepID } ); 
			#	die "Approval step is '$approvalStepStatus'";
			#}

		}

		if ( $subaction eq "setup" ) {

			# See if row exists in escomparehistory for this job/version combo

			my ( $existHistory ) = findComparison( $job1, $v1_version, $v1_timestamp, $job2, $v2_version, $v2_timestamp );
			push( @history, "Existing document history id $existHistory found" ) if ( defined $existHistory );

			# Delete the document in Dalim (it may exists even if at odds with JF database)

			my $docPath = "/$esCustomer/$job/$docName";
			my $docID;
			push( @history, "See if document $docName exists in Dalim ES" );
			eval {
				my $results = ESAPI::getDocument( $client, { path => $docPath } );
				$docID = $results->{ID};
				push( @history, "Document $docPath exists, docid $docID" );
			};
			if ( $@ ) {
				my $error = $@;
				$error =~ s/at module.*//g;
				die "Error from getDocument, docPath '$docPath', error $error" unless ( $error =~ /Cannot find PageOrder/ );
				push( @history, "Document $docPath does not exist" );
			}
			if ( defined $docID ) {
				eval {
					ESAPI::deleteDocument( $client, { ID => $docID } ); # could use path = $docPath
					push( @history, "Deleted document $docPath ($docID)" );
					ECMDBAudit::db_auditLog( $userid, "ESCOMPAREHISTORY", $job1, "ES doc delete, user $username, doc $docID, path $docPath" );
				};
				if ( $@ ) {
					my $error = $@;
					# $error =~ s/at module.*//g;
					die "Error from deleteDocument, docPath '$docPath', error $error"; # unless ( $error =~ /Cannot find PageOrder/ );
				}
			}
			# Mark previous history row as deleted if there is one
			deletePreviousHistory( $existHistory, \@history ) if ( defined $existHistory );

		} else {
			# The setup subaction will also have deleted any previous instance of this document

			# Need a temp directory with a unique name, to hold a temporary copy of the version files
			# We copy them from the <job path>/JOB_ASSETS/VERSIONS directory or <job path>/OUTPUT_FILE if archived
 
			push( @history, "Creating temp directory" );

			# Create temp directory
			my $ug		= new Data::UUID;
			my $uuid	= $ug->create();
			my $targetDir	= $ug->to_string( $uuid );

			my $tempdir	= "/temp/$targetDir/";
			mkdir ( $tempdir ) or die "$!";
			die "Temp directory doesnt exist" unless -e $tempdir;
			push( @history, "Temp directory $tempdir" );

			# Call helper for each version to ftp to temp dir
			my $filename;

			# Copy is here because if we do it any sooner, Dalim deletes the files in situ
			# The ftp uses a chrooted userid, so we create a temp directory (using UUID)  and put the file there
			# then use the $gpfs_ftp_path to get the full path to the file.

			if ( $subaction eq "create" ) {
				push( @history, "Transferring 1st version to temp" );
				transfer_from_gpfs_to_holding( $tempdir, $job1, $job1_version_path, $v1_filename );
				$filename = $v1_filename;
				push( @history, "ls of temp: " . `ls $tempdir` );
				push( @history, "Transferring 1st version from temp $tempdir to Dalim directory" );
				transfer_to_local_gpfs( $tempdir, $v1_filename, $targetDir, $gpfs_ftp_server, $gpfs_ftp_user, $gpfs_ftp_pass );
	
			} else { # subaction is register
				push( @history, "Transferring 2nd version from studio to temp" );
				transfer_from_gpfs_to_holding( $tempdir, $job2, $job2_version_path, $v2_filename );
				$filename = $v2_filename;
				push( @history, "ls of temp: " . `ls $tempdir` );
				push( @history, "Transferring 2nd version from temp $tempdir to Dalim directory $targetDir" );
				transfer_to_local_gpfs( $tempdir, $v2_filename, $targetDir, $gpfs_ftp_server, $gpfs_ftp_user, $gpfs_ftp_pass );
			}

			# Clean up temp dir (the files are now on the live gpfs)
			unlink( "$tempdir/$filename" );
			rmdir ( $tempdir );

			# Convert the relative (chrooted) path to its absolute path

			$gpfs_ftp_path =~ s/\/$//g;   # Strip trailing slash
			$fileURL = $gpfs_ftp_path . "/" . $targetDir . "/" . $filename;

		}

		if ( $subaction eq "create" ) {

			# Create document
			# nb using jobPath + class gives error 'missing parameter "class"'
	
			push( @history, "Creating document $docName, URL $fileURL" );

			my @approvers = [
				#{ user => $es_user, type => "Approver" },
				{ group => $esCustomerGroup, type => "Approver", approverCount => 1, keepApprovalState => 1 },
				#{ user => $es_user, type => "Creator", approverCount => 1 } 
			];

			my $result = ESAPI::createDocument( $client, {
				jobID => $jobID, # Alternative is jobPath + class, but there is an error in ES API, says missing class, even if supplied
				#jobPath => $jobPath, class => "PageOrder",
				name => $docName,  
				description => $description,
				# colorSpaceName ..., viewingConditions ...
				URL => $fileURL, 
				moveFile => "false",
				metadatas => [],		# metadatas ...
				approvals => [ { stepName => APPROVAL_WORKFLOW_STEP, approvers => @approvers } ],
				class => "PageOrder", } );

			print "createDocument " . Dumper( $result ) . "\n" if $debug;
			$docID = $result->{ID};
			ECMDBAudit::db_auditLog( $userid, "ESCOMPAREHISTORY", $job1, "ES doc create, user $username, doc $docID, $description" );

			# The update of escomparehistory is done in register() after adding the 2nd version

			# Check that the WFL_DIALOG workflow is running and that the Approval workflow step is Held

			my $m = getDocWorkflowStep( $client, $docID, $workflow, APPROVAL_WORKFLOW_STEP );

			my $approvalStepID = $m->{stepID};
			my $approvalStepStatus = $m->{status};
			push( @history, "Found workflow $workflow step " . APPROVAL_WORKFLOW_STEP . ", stepID '$approvalStepID', status '$approvalStepStatus'" );

			if ( $approvalStepStatus eq "Waiting" ) {
				# Start the workflow
				push( @history, "Starting Workflow $workflow" );
				ESAPI::startWorkflow( $client, { ID => $docID, class => "PageOrder", name => $workflow } );

			} elsif ( $approvalStepStatus ne "Held" ) {
				# Restart the Approval workflow step (was Completed or Aborted)
				#push( @history, "Starting Workflow $workflow step " . APPROVAL_WORKFLOW_STEP );
				#ESAPI::restartWorkflow( $client, { ID => $docID, class => "PageOrder", name => $workflow, stepID => $approvalStepID } ); 
			}

		} elsif ( $subaction eq "register" ) {
			# Do the document.register

			push( @history, "Registering revision $docName, URL $fileURL" );

			my $results = ESAPI::registerDocument( $client, { 
				jobID => "$jobID", 			# alternative is jobPath => ?, 
				URL => $fileURL,			# alternative is URLS => ?, # if registering more than one doc change
				documentName => $docName,		# Alternative is documentNames => ?, # if URLS supplied  
				moveFile => "false",
				metadatas => [], 			# metadatais => ?,
			} ); 
			$docStatus = getDocStatusHelper( $client, $docID );

			# Add row to escomparehistory. recording who did it and the current docId

			push( @history, "Adding row to escomparehistory, docID $docID" );

			$Config::dba->process_oneline_sql(
				"INSERT INTO escomparehistory ( userid, job1, version1, timestamp1, job2, version2, timestamp2, docId ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )", 
							[ $userid, $job1, $v1_version, $v1_timestamp, $job2, $v2_version, $v2_timestamp, $docID ] );

			push( @history, "Adding row to jobevents for CTS report" );
			logCompare( $userid, $job1 );

			ECMDBAudit::db_auditLog( $userid, "ESCOMPAREHISTORY", $job2, "ES doc register, user $username, doc $docID, $description" ); 
		}

		$vars = { 
			user => $es_user, custID => $custID, jobID => $jobID, 
			docid => $docID, docurl => $docURL, docName => $docName, docStatus => $docStatus,
			paths => \@paths, rpcurl => $rpcpath, timeout => $timeout, history => \@history };
	};
	if ( $@ ) {
		my $error = $@;
		if ( $error =~ /Cannot read metadata/ ) {
			push( @history, $error );
			$error = "Failed to find PDF for one of the versions";
		}
		$vars = { error => $error, history => \@history };
	}
        my $json = encode_json ( $vars );
        return $json;
}

# handle "review" subaction, get list of comparisons existing for this job

sub getReview {
	my ( $userid, $username, $job, $fname, $lname, $email ) = @_;

	my $vars;
	my $docURL;
	my @history;
	
	eval {
		# Check user has an es_user id and password (in case they try to view a comparison before creating one)

		die "Username is invalid" unless ( ( defined $username ) && ( $username ne "" ) );

		my $es_user = $es_config_jobflow_name_prefix . $username;
		my $orgUnit = $es_config_jf_org;
		my $defaultProfile = $es_config_default_profile;
	
		my $client = getAdminClient();

		my ( $es_pass ) = ensureESUserExists( $client, $username, $es_user, $orgUnit, $defaultProfile, \@history  );

		# Look in escomparehistory for a match by job (as either job1 or job2), ignoring deleted compares

		push( @history, "Query on escomparehistory for job $job" );

		my $versions = $Config::dba->hashed_process_sql( 
			"SELECT e.userid, u.username AS comparedby, e.rec_timestamp AS comparedon, " .
				" e.docid, e.job1, e.version1, e.timestamp1, e.job2, e.version2, e.timestamp2, " .
				" a.userid AS approval_userid, a.state AS approval_state, a.rec_timestamp AS approved_at, au.username AS approver_name " .
			" FROM escomparehistory AS e " .
			" INNER JOIN users AS u ON u.id = e.userid " .
			" LEFT OUTER JOIN esapprovals AS a ON a.compareid = e.id " .
			" LEFT OUTER JOIN users AS au ON au.id = a.userid " .
			" WHERE (job1 = ? OR job2 = ?) AND deleted IS NULL", [ $job, $job ] );

		if ( ! defined $versions ) {
			push( @history, "No prior versions found" );
			$versions = [];
		} else {
			push( @history, "Found " . scalar( @{ $versions } ) . " version(s)" );
		}

		my $docURL = $es_config_viewer_path;

		# Get customer for job, check this user can access the customer in ES

		$vars = { job => $job, docurl => $docURL, versions => $versions, history => \@history };
	};
	if ( $@ ) {
		$vars = { error => $@, history => \@history };
	}
        my $json = encode_json ( $vars );
        return $json;
}

# checkCompareExists, handler for "compareexists" subaction
# Used by js/ecompare.js when user clicks on [ Compare ] having selected 2 versions
# or a version and an archived hires from another job.
#
# Checks
# a) whether a comparison has been done for the 2 jobs/versions selected, and if so which docID
# b) if the comparison exists, and we have a docID, get the document status
# 
# Inputs
# ( job, version, timestamp ) for both jobs
# - either entry to be "archived" ie null version and timestamp
#
# Returns (JSON encoded) hash,
# - found - 0 or 1
# - job1, version1, timestamp1,job2, version2, timestamp2 (if found == 0)
# - userid, username, docID, created, docurl, docstatus (if found == 1)
# - error, if exception thrown

# TODO1 if docstatus is "Completed" restart the workflow 

sub checkCompareExists {
	my ( $job1, $version1, $timestamp1, $job2, $version2, $timestamp2  ) = @_;

	my $vars;
	my @history;

	eval {
		my $docURL = $es_config_viewer_path;
		my $workflow = $es_config_workflow;

		# Look in escomparehistory for a match by job (as either job1 or job2), ignoring deleted compares

		my ( $histId, $cr_userid, $cr_username, $docID, $cr_timestamp ) = findComparison( $job1, $version1, $timestamp1, $job2, $version2, $timestamp2 );

		if ( ! defined $docID ) {
			push( @history, "No comparison exists" );
			$vars = { found => 0, job1 => $job1, job2 => $job2, version1 => $version1, version2 => $version2, timestamp1 => $timestamp1, timestamp2 => $timestamp2, history => \@history };
		} else {

			# Get the doc status for $docID
			my $client = getAdminClient();
			my $docStatus = getDocStatusHelper( $client, $docID );
			push( @history, "Comparison exists, document $docID, status $docStatus, comparehistory id $histId" );

			# Get the approval workflow status

			my $results = ESAPI::getWorkflow( $client, { ID => $docID, class => "PageOrder", name => $workflow } );
 			push( @history, "getWorkflows for doc, workflowName " . $results->{workflowName} . ", ID " . $results->{ID} . ", class " . $results->{class} );

			my $m = getDocWorkflowStep( $client, $docID, $workflow, APPROVAL_WORKFLOW_STEP );
			my $approvalStepID = $m->{stepID};
			my $approvalStepStatus = $m->{status} || "";

			die "No " . APPROVAL_WORKFLOW_STEP . " step in $workflow" if ( $approvalStepStatus eq "" );

			push( @history, "Workflow $workflow step " . APPROVAL_WORKFLOW_STEP . ", stepID '$approvalStepID', status '$approvalStepStatus'" );

			if ( $approvalStepStatus ne "Held" ) {

				# Restart the Approval workflow step (was Completed or Aborted), which resets the document status to 'ToApprove'

				push( @history, "Starting Workflow $workflow step " . APPROVAL_WORKFLOW_STEP );
				ESAPI::restartWorkflow( $client, { ID => $docID, class => "PageOrder", name => $workflow, stepID => $approvalStepID } ); 

				# Get revised docstatus and approvalstepstatus

				$docStatus = getDocStatusHelper( $client, $docID );
 	 			push( @history, "docstatus now $docStatus" );

				$results = ESAPI::getWorkflow( $client, { ID => $docID, class => "PageOrder", name => $workflow } );
				my $m = getDocWorkflowStep( $client, $docID, $workflow, APPROVAL_WORKFLOW_STEP );
				$approvalStepID = $m->{stepID};
				$approvalStepStatus = $m->{status} || "";

				push( @history, "Workflow $workflow step " . APPROVAL_WORKFLOW_STEP . ", stepID '$approvalStepID', status '$approvalStepStatus'" );
			}

			$vars = { found => 1, userid => $cr_userid, username => $cr_username, 
					approvalstepid => $approvalStepID, approvalstepstatus => $approvalStepStatus,
					docID => $docID, created => $cr_timestamp, docurl => $docURL, docstatus => $docStatus, history => \@history };
		}
	};
	if ( $@ ) {
		$vars = { error => $@ };
	}
        my $json = encode_json ( $vars );
        return $json;
}

sub getDocStatusHelper {
	my ( $client, $docid ) = @_;

	my $docStatus = "";
	eval {
		my $results = ESAPI::executeSQL( $client, { sql => "select status from document where ID = $docid" } );
		# results->{headers} may exist but results->{objectList} holds the rows returned
		die "Document doesnt exist" unless ( ( defined $results ) && ( exists $results->{objectList} ) );

		my $objectList = $results->{objectList};
		my $docEntry = @{$objectList}[ 0 ];
		$docStatus = @{$docEntry}[ 0 ];
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/at modu.*//g;
		die "getDocStatus for docid $docid, $error";
	}
	return $docStatus;
}

# Check whether the document for a supplied docID exists or not
# Throws an exception if it doesnt, otherwise returns its document status.

sub getDocumentStatus {
	my ( $docid ) = @_;

	my $vars;
	eval {
		die "Docid not defined" unless ( defined $docid );
		my $client = getAdminClient();
		my $docStatus = getDocStatusHelper( $client, $docid );
		$vars = { docStatus => $docStatus };
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/at modu.*//g;
		$vars = { error => $error };
	}
        my $json = encode_json ( $vars );
        return $json;
}

# getAnnotations, called after Document Viewer has been closed.
# Main purpose is to get annotations added by user, but at the moment (JF R2.09), just leave them in Dalim ES.
# However, we do track the approval status.

sub getAnnotations {
	my ( $userid, $username, $docID ) = @_;

	my $vars;
	my @history;

	eval {
		my $client = getAdminClient();
		my $workflow = $es_config_workflow;

		# Get workflow for document, identify Approval workflow step and restart it if not held

		die "getAnnotations: docID undefined" unless defined $docID;

		my $results = ESAPI::getWorkflow( $client, { ID => $docID, class => "PageOrder", name => $workflow } );
 		print "getWorkflows for doc, workflowName " . $results->{workflowName} . ", ID " . $results->{ID} . ", class " . $results->{class} . "\n" if $debug;
		my $m = getDocWorkflowStep( $client, $docID, $workflow, APPROVAL_WORKFLOW_STEP );
		my $approvalStepID = $m->{stepID};
		my $approvalStepStatus = $m->{status} || "";

		die "No " . APPROVAL_WORKFLOW_STEP . " step in $workflow" if ( $approvalStepStatus eq "" );

		push( @history, "Found workflow $workflow step " . APPROVAL_WORKFLOW_STEP . ", stepID '$approvalStepID', status '$approvalStepStatus'" );

		#if ( $approvalStepStatus eq "Waiting" ) {
		#	# Start the workflow
		#	push( @history, "Starting Workflow $workflow" );
		#	ESAPI::startWorkflow( $client, { ID => $docID, class => "PageOrder", name => $workflow } );
		#} elsif ( $approvalStepStatus ne "Held" ) {
		#	# Restart the ApprovalWFL step (was Completed or Aborted)
		#	push( @history, "Starting Workflow $workflow step " . APPROVAL_WORKFLOW_STEP );
		#	ESAPI::restartWorkflow( $client, { ID => $docID, class => "PageOrder", name => $workflow, stepID => $approvalStepID } ); 
		#}

		# See if this document has already been approved/rejected
		my $approval_state; # Current approval state
		my ( $compare_id ) = $Config::dba->process_oneline_sql( "SELECT id FROM escomparehistory WHERE deleted IS NULL and docID = ?", [ $docID ], 1 );
		if ( ( defined $compare_id ) && ( $approvalStepStatus =~ /(Completed|Aborted)/ ) ) {
			# See whether someone has already approved or rejected the document comparison
			( $approval_state ) = $Config::dba->process_oneline_sql( "SELECT state FROM esapprovals WHERE compareid = ?", [ $compare_id ], 1 );
			if ( ! defined $approval_state ) {
				my $desired_state = "Approved";
				$desired_state = "Rejected" if ( $approvalStepStatus eq "Aborted" );
				$Config::dba->process_oneline_sql( "INSERT INTO esapprovals ( userid, state, compareid ) VALUES ( ?, ?, ? )",
						[ $userid, $desired_state, $compare_id ] );
			}
		}

		$vars = { history => \@history };
	};
	if ( $@ ) {
		$vars = { error => $@ };
	}
        my $json = encode_json ( $vars );
        return $json;

}

# handle logview subaction, ie add audit entry for CTS to record user viewing prior comparison

sub logView {
	my ( $userid, $username, $docId ) = @_;

	my $vars;
	my @history;

	eval {
		Validate::userid( $userid );

		die "logView: docID undefined" unless defined $docId;

		my ( $job ) = $Config::dba->process_oneline_sql( "SELECT job1 FROM escomparehistory WHERE docid = ? AND deleted IS NULL ", [ $docId ] );
		die "No job found for document $docId" unless defined $job;

		push( @history, "Updating audit log for $job" );

		ECMDBJob::db_recordJobAction($job, $userid, CTSConstants::ES_REVIEW );
		ECMDBAudit::db_auditLog( $userid, "TRACKER3PLUS", $job, "$userid ($username) reviewing job $job using Compare ES" ); 

		$vars = { history => \@history };
	};
	if ( $@ ) {
		$vars = { error => $@ };
	}
        my $json = encode_json ( $vars );
        return $json;
}

sub logCompare {
	my ( $userid, $job1 ) = @_;

	ECMDBJob::db_recordJobAction( $job1, $userid, CTSConstants::ES_COMPARE );
}



sub test_getVersions() {

	my $userid = 330;
	my @jobs = ( "104000020", "104000018", "104000004", "9993" );
	my $results = getVersions( $userid, \@jobs );
	print "getVersions: $results\n";

	@jobs = ( "9993" );
	$results = getVersions( $userid, \@jobs );
	print "getVersions: $results\n";
}

sub test_compare() {

	$debug = 1;

	my $results = compare( $testuserid, "streanor", "setup", "104000004", "2", "104000004", "1" );
	print "compare: $results\n";

	return 0;
}

sub test_ensureUserInGroup() {

	$debug = 1;

	my $adminClient = getAdminClient();
	my $user = "jfd_bdalton";
	my $groupName = "jfd_c_AA_PRODUCTION_COMPANY";
	my $orgUnit = "Test Organisation";
	my @history;
	eval {
		ensureUserInGroup( $adminClient, $user, $groupName, $orgUnit, "Test desc", \@history );
		print "ensureUserInGroup history " . Dumper( @history ) . "\n";
	};
	if ( $@ ) {
		print "Unexp error from ensureUserInGroup, $@\n";
	}
	return 0;
}

sub test_getReview {

	my $debug = 1;
	my $userid = 330;
	my $username = "streanor";
	my $job = "104000004";
	my $docid = 1;
	my $results;

	eval {
		$results = getReview( $userid, $username, $job, $docid );
		print "getReview returned " . Dumper( $results ) . "\n";
	};
	if ( $@ ) {
		print "getReview failed, exception $@\n";
	}
	return 0;
}

sub test_logView {

	my $debug = 1;
	my $userid = 330;
	my $username = "jfd_streanor";
	my $job = "104000004";
	my $docid = 1;
	my $results;

	eval {
		$results = logView( $userid, $username, $job, $docid );
		print "logView returned " . Dumper( $results ) . "\n";
	};
	if ( $@ ) {
		print "logView failed, exception $@\n";
	}
	return 0;
}

sub test_logCompare {

	my $debug = 1;
	my $userid = 330;
	my $job = "104000004";

	eval {
		logCompare( $userid, $job );
		print "logCompare ok\n";
	};
	if ( $@ ) {
		print "logCompare failed, exception $@\n";
	}
	return 0;
}


sub test_checkCompareExists {


	my $job1 = '104000004'; my $version1 = '2'; my $timestamp1 = '2015-03-09 17:37:04';
	my $job2 = '104000004'; my $version2 = '2'; my $timestamp2 = '2015-03-10 11:13:31';
	
	my $results;
	eval {
		$results = checkCompareExists( $job1, $version1, $timestamp1, $job2, $version2, $timestamp2 );
		print "checkCompareExists returned " . Dumper( $results ) . "\n";
	};
	if ( $@ ) {
		print "checkCompareExists failed, exception $@\n";
	}
	return 0;
}

sub test_executeSQL {
	my $results;

	my $docID = 18721; # Needs to be updated as can be deleted
	eval {
		my $client = ESAPI::connect( $es_config_host, $es_config_rpc_path, $es_config_admin_user, $es_config_admin_password, $es_config_timeout );

		$results = ESAPI::executeSQL( $client, { sql => "select ID, name,status,currentRevision from document where name = 'docName'" } );

		print "executeSQL returned " . Dumper( $results ) . "\n";
	};
	if ( $@ ) {
		print "executeSQL failed, exception $@\n";
	}
	return 0;
}

sub test_getDocumentStatus {
	my $results;

	my $docID = 25077; # Needs to be updated as can be deleted
	my $docStatus;
	eval {
		$results = getDocumentStatus( $docID );

		print "getDocumentStatus returned " . Dumper( $results ) . "\n";
	};
	if ( $@ ) {
		print "getDocumentStatus failed, exception $@\n";
	}
	return 0;
}

sub test_userCanView {
	my $tag_env = $ENV{'TAG_ENV'} || "DEV";

	my $userid = 330;
	my $customerId = 18; # TESTKHALIDCUST
	# $CUSTOMERID = 26; # CANCER_RESEARCH

	if ( lc($tag_env) eq "staging" ) { 
		$userid = 1357; # For Staging
		$customerId = 277;
	}

	print "Calling userCanView, userid $userid, customerid $customerId\n";
	eval {
		my $rc = userCanViewCustomer( $userid, $customerId );
		print "userCanView returned $rc\n";
	};
	if ( $@ ) {
		print "userCanView failed, exception $@\n";
	}
	return 0;
}

#exit __PACKAGE__->test_getVersions() unless caller();
#exit __PACKAGE__->test_compare() unless caller();
#exit __PACKAGE__->test_ensureUserInGroup() unless caller();
#exit __PACKAGE__->test_getReview() unless caller();
#exit __PACKAGE__->test_checkCompareExists() unless caller();
#exit __PACKAGE__->test_executeSQL() unless caller();
#exit __PACKAGE__->test_getDocumentStatus() unless caller();
#exit __PACKAGE__->test_logView() unless caller();
#exit __PACKAGE__->test_logCompare() unless caller();
#exit __PACKAGE__->test_userCanView() unless caller();


1;

