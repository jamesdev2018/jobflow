#!/usr/local/bin/perl
package ECMDBOutsource;

use strict;
use warnings;
use Scalar::Util qw( looks_like_number );

use lib 'modules';

require "config.pl";
require "general.pl";


my $database = Config::getConfig("databaseschema");
my $data_source = Config::getConfig("databaseserver");

my $db_username = Config::getConfig("databaseusername");
my $db_password = Config::getConfig("databasepassword");




############################
## Get a list of outsources
############################
sub db_getOutsources
{
	### POSSIBLE TODO: IMPLEMENT PAIRING FILTER FOR COMPLETE/INCOMPLETE BIDIRECTIONAL ENTRIES [NON-TRIVIAL, I EXPECT]
	my ($srcopco_filter, $dstopco_filter, $ttpfolder_filter, $pairing_filter) = @_;
	
	my @outsources;
	my ($srcop, $dstop, $srcopid, $dstopid, $username, $userid, $ttpfolder, $status, $timestamp);
	my ($dbh, $sql, $sth);
	
	
	$dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	$sql = qq { SELECT OC.OPCONAME,  OC2.OPCONAME, OD.FROMOPCOID, OD.TOOPCOID, U.USERNAME, OD.USERID, OD.TTPFOLDER, OD.STATUS, OD.REC_TIMESTAMP
			FROM OUTSOURCEDESTINATIONS AS OD
			INNER JOIN OPERATINGCOMPANY AS OC
				ON OC.OPCOID=OD.FROMOPCOID
			INNER JOIN OPERATINGCOMPANY AS OC2
				ON OC2.OPCOID=OD.TOOPCOID
			INNER JOIN USERS AS U
				ON U.ID=OD.USERID };
	

	### DYNAMICALLY GENERATE FILTER LIST BECAUSE OTHERWISE $sth->execute() COMPLAINS ABOUT
	### HAVING THE WRONG NUMBER OF INPUTS
	my $filter;
	my @filterarray;
	
	if($srcopco_filter || $dstopco_filter || $ttpfolder_filter)
	{
		if($srcopco_filter)
		{
			push(@filterarray, $srcopco_filter);
			$filter .= "OD.FROMOPCOID=? ";
		}
		if($dstopco_filter)
		{
			push(@filterarray, $dstopco_filter);
			$filter .= ($filter ? "AND " : "") . "OD.TOOPCOID=? ";
		}
		if($ttpfolder_filter)
		{
			push(@filterarray, $ttpfolder_filter);
			$filter .= ($filter ? "AND " : "") . "OD.TTPFOLDER LIKE ?";
		}
		$sql .= " WHERE " . $filter;
	}
	
	### COMPLETE SQL QUERY, EXECUTE IT
	$sql .= qq { ORDER BY OD.REC_TIMESTAMP DESC };
	$sth = $dbh->prepare($sql);
	$sth->execute(@filterarray);
	
	$sth->bind_columns(\$srcop, \$dstop, \$srcopid, \$dstopid, \$username, \$userid, \$ttpfolder, \$status, \$timestamp );
	
	while( $sth->fetch() )
	{
		my %row = (
			srcopco => $srcop,
			dstopco => $dstop,
			srcopcoid => $srcopid,
			dstopcoid => $dstopid,
			username => $username,
			userid => $userid,
			ttpfolder => $ttpfolder,
			status => $status,
			timestamp => $timestamp,
		);
		push(@outsources, \%row)
	}
	
	$sth->finish();
	$dbh->disconnect();
	
	return @outsources;
}



##################################
## Add the outsource
##################################
sub addOutsource
{
	my ($userid, $srcopco, $dstopco, $ttpfolder) = @_;
	my ($dbh, $sql, $sth);
	
	$dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1} );
	$sql = qq { INSERT INTO OUTSOURCEDESTINATIONS ( FROMOPCOID, TOOPCOID, USERID, TTPFOLDER, STATUS, REC_TIMESTAMP) VALUES (?, ?, ?, ?, ?, now() ) };
	$sth = $dbh->prepare($sql);
	$sth->execute($srcopco, $dstopco, $userid, $ttpfolder, 1);

	$sth->finish();
	$dbh->disconnect();
	
	return 1;
}


##################################
## Delete an outsource pairing
##################################
sub deleteOutsource
{
	my ($srcopco, $dstopco) = @_;
	my ($dbh, $sql, $sth);
	
	
	$dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { Raiseerror => 1} );
	$sql = qq { DELETE FROM OUTSOURCEDESTINATIONS WHERE FROMOPCOID=? AND TOOPCOID=? };
	$sth = $dbh->prepare($sql);
	$sth->execute($srcopco, $dstopco);
	
	$sth->finish();
	$dbh->disconnect();
	
	return 1;
}

##################################
## Edit an outsource pairing
##################################
sub updateOutsource
{
	my ($srcopcoid, $dstopcoid, $ttpfolder, $userid) = @_;
	my ($dbh, $sql, $sth);
	
	if($srcopcoid && (! looks_like_number($srcopcoid))) { return 0; }
	if($dstopcoid && (! looks_like_number($dstopcoid))) { return 0; }
	if($userid && (! looks_like_number($userid))) { return 0; }
	if($ttpfolder && !($ttpfolder =~ /^\/.*/)) { return 0; }
	
	$dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1} );
	$sql = qq { UPDATE OUTSOURCEDESTINATIONS SET TTPFOLDER=?, USERID=?, REC_TIMESTAMP=now() WHERE FROMOPCOID=? AND TOOPCOID=? };
	$sth = $dbh->prepare($sql);
	$sth->execute($ttpfolder, $userid, $srcopcoid, $dstopcoid);
	
	$sth->finish();
	$dbh->disconnect();
	
	return 1;
}

##################################
## Return an outsource
##################################
sub getOutsource
{
	my ($srcopcoid, $dstopcoid) = @_;
	my ($dbh, $sql, $sth, %row);
	my ($retsrcopcoid, $retdstopcoid, $retsrcopconame, $retdstopconame, $retuserid, $retttpfolder, $retstatus, $rettimestamp);
	
	$dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1} );
	$sql = qq { SELECT od.FROMOPCOID, od.TOOPCOID, soc.OPCONAME, doc.OPCONAME, od.USERID, od.TTPFOLDER, od.STATUS, od.REC_TIMESTAMP
			 FROM OUTSOURCEDESTINATIONS AS od
			 INNER JOIN OPERATINGCOMPANY AS soc ON od.FROMOPCOID=soc.OPCOID
			 INNER JOIN OPERATINGCOMPANY AS doc ON od.TOOPCOID=doc.OPCOID
			 WHERE FROMOPCOID=? AND TOOPCOID=? };
	$sth = $dbh->prepare($sql);
	$sth->execute($srcopcoid, $dstopcoid);
	
	$sth->bind_columns(\$retsrcopcoid, \$retdstopcoid, \$retsrcopconame, \$retdstopconame, \$retuserid, \$retttpfolder, \$retstatus, \$rettimestamp);
	if( $sth->fetch() )
	{
		%row = (
			srcopcoid	=> $retsrcopcoid,
			dstopcoid	=> $retdstopcoid,
			srcopconame	=> $retsrcopconame,
			dstopconame	=> $retdstopconame,
			userid		=> $retuserid,
			ttpfolder	=> $retttpfolder,
			status		=> $retstatus,
			rec_timestamp	=> $rettimestamp,
			);
	}
	
	$sth->finish();
	$dbh->disconnect();

	return %row;
}

##################################
## Check if an outsource pairing exists
##################################
sub pairingExists
{
	my ($srcopco, $dstopco) = @_;
	my ($dbh, $sql, $sth);
	my ($numpairs);
	
	
	$dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1} );
	$sql = qq { SELECT COUNT(*) FROM OUTSOURCEDESTINATIONS WHERE FROMOPCOID = ? AND TOOPCOID = ? };
	$sth = $dbh->prepare($sql);
	$sth->execute($srcopco, $dstopco);
	
	$sth->bind_columns( \$numpairs );
	if( $sth->fetch() ) { }
	
	$sth->finish();
	$dbh->disconnect();
	
	return $numpairs > 0;
}


1;
