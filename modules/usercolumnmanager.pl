#!/usr/local/bin/perl

package UserColumnManager;

use strict;
use warnings;

use Template;
use Crypt::CBC;
use CGI::Session;
use CGI::Cookie;
use CGI;
use Scalar::Util qw( looks_like_number );
use Encode qw(decode encode);
use JSON;

# Home grown - please do NOT include jobs.pl, databaseJob.pl, databaseOpcos.pl or anything that includes jobs.pl

use lib 'modules';

use CTSConstants;
use SessionConstants;

require "config.pl";

require "databaseColumnManager.pl";
require "databaseAudit.pl"; # db_recordJobAction


##############################################################################
# the template object 
#
##############################################################################
my $tt = Template->new({
	RELATIVE => 1,
}) || die "$Template::ERROR\n";

##############################################################################
####    UpdateColumnManager
####
#################################################################################
sub UpdateColumnManager {
	my ( $query, $session ) = @_;

	my $updatestr   = $query->param( "updatestr" );
	my $mode        = $query->param( "mode" ) || '';

	my $userid      = $session->param( SessionConstants::SESS_USERID );
	my $cmopcoid    = $session->param( SessionConstants::SESS_OPCOID );

	my $vars;

	eval {
		ECMDBColumnManager::db_updateColumnManager( $userid, $updatestr, $cmopcoid );
		my $usersettings_var = join( ",", ECMDBColumnManager::getUserSettings( $userid,$cmopcoid ) );
		$session->param( SessionConstants::SESS_USERSETTINGS, $usersettings_var );

		ECMDBAudit::db_recordJobAction(undef, $userid, CTSConstants::CM_UPDATE ); #   For CTS report
		$vars = { UpdateColumnManager => 1 };
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => $error,  };
	}

	if ( $mode eq "json" ){
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

##############################################################################
##      ShowColumnManagerDialog
##	Called when user clicks on icon to change Column Manager settings
##
###############################################################################
sub ShowColumnManagerDialog {
	my ( $query, $session ) = @_;

	my $mode = $query->param( "mode" ) || '';

	my $userid				= $session->param( SessionConstants::SESS_USERID );
	my $opcoid				= $session->param( SessionConstants::SESS_OPCOID );
	my $roleid				= $session->param( SessionConstants::SESS_ROLEID );
	my $rolename			= $session->param( SessionConstants::SESS_ROLENAME );
	my $relaxroles			= $session->param( SessionConstants::SESS_RELAXROLES );
	my $usersettings_var	= $session->param( SessionConstants::SESS_USERSETTINGS );

	# Avoid loading databaseOpcos.pl
	# my $publishing      = ECMDBOpcos::isPublishing ( $opcoid );
	no warnings 'once';
	my ( $publishing ) = $Config::dba->process_oneline_sql("SELECT publishing FROM operatingcompany WHERE OPCOID=? ", [ $opcoid ] );
	$publishing = 0 unless defined $publishing;

	# TODO do join between usersettings and column_mappings and maybe column_manager_defaults
	# JOIN is on column_id present in all 3 tables

	my $opflags = { publishing => 0, bookingformnav => 0, isjobassignment => 1, isadgatesend => 0, estimateon => 1, ArchantChk => 1, };
 
	my $columns = ECMDBColumnManager::getCurrentColumns( $userid, $opcoid, $opflags, $roleid, $rolename, $relaxroles );

	my $vars = {
        usersettings	=> $usersettings_var,
		publishing		=> $publishing,
		roleid			=> $roleid,
		relaxroles		=> $relaxroles,
		columns			=> $columns,
	};

	if ( $mode eq "json" ) {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
###    ApplyColumnManager
###
################################################################################
sub ApplyColumnManager {
	my ( $query, $session ) = @_;

	my $updatestr   = $query->param( "updatestr" );
	my $mode        = $query->param( "mode" ) || '';

	my $userid		= $session->param( SessionConstants::SESS_USERID );
	my $cmopcoid	= $session->param( SessionConstants::SESS_OPCOID );

	my $vars;

	eval {

		#For new user need to put entry in 'usersettings' table with default column managers settings before try to updating column manager settings.
        my $usersettings_var = join( ",", ECMDBColumnManager::getUserSettings( $userid, $cmopcoid ) );
        $session->param( SessionConstants::SESS_USERSETTINGS, $usersettings_var );

		ECMDBColumnManager::db_applyColumnManager( $userid, $updatestr, $cmopcoid );
        ECMDBAudit::db_recordJobAction(undef, $userid, CTSConstants::CM_UPDATE ); # For CTS report.

		$vars = { ApplyColumnManager => 1, };
    };
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => $error,  };
	}
    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

1;

