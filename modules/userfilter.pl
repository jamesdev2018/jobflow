#!/usr/local/bin/perl
package UserFilter;

use strict;
use warnings;

use CGI::Session;
use CGI::Cookie;
use CGI;

use Scalar::Util qw( looks_like_number );
use Encode qw(decode encode);
use JSON;

# Home grown - please do NOT include jobs.pl or anything that includes jobs.pl
use lib 'modules';

require "config.pl";

require "databaseUserFilter.pl";
require "databaseAudit.pl";	# db_recordJobAction
require "databaseKanbanboard.pl"; # getSwimLaneType, ...
require "databaseMessages.pl";
require "databaseOpcos.pl";
require "Mailer.pl";

use CTSConstants;

##############################################################################
###      ShowSaveFilterDialog
###
################################################################################
sub ShowSaveFilterDialog{
    my $query   = $_[0];
    my $session = $_[1];

    my $mode 					= $query->param( "mode" ) || '';
	my $getflag				   	= $query->param( "getFlag" ) || 0;
	my $filtername             	= $query->param( "filtername" );
	my $sharedfilterid          = $query->param( "sharedfilterid" );
	my $pinningid				= $query->param( "pinningid" );

    my $opcoid      			= $session->param('opcoid');
    my $roleid      			= $session->param( 'roleid' );
	my $userid     				= $session->param( 'userid' );

	my $vars;

	eval {

		my @userfiltersettings_var 	=  ECMDBUserFilter::getUserFilterSettings( $userid, $opcoid );

		my @users;
		my @sharedusers; 

		if ($getflag == 1) {

			@sharedusers	 = ECMDBUserFilter::getSharedFilterData($userid,$opcoid);

		} elsif ($getflag == 2) {

			@sharedusers    = ECMDBUserFilter::getSharedFilterUser($filtername,$opcoid,$userid);
			@users        = ECMDBMessages::db_getUsersForLookup();

		} else { # eg 0 or undef

			@users        = ECMDBMessages::db_getUsersForLookup();
		}

		$vars = {
			userfiltersettings => \@userfiltersettings_var,
			roleid          =>  $roleid,
			userlist        => \@users,
			shareduser      => \@sharedusers,
			filtername		=> $filtername,
			sharedfilterid  => $sharedfilterid,
			pinningid		=> $pinningid,
		};
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = $error;
	}
	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

##############################################################################
####      LoadFilter
####
#################################################################################
sub LoadFilter{
    my $query   = $_[0];
    my $session = $_[1];

    my $mode = $query->param( "mode" ) || '';
	my $filtername = $query->param( "filtername" );

    my $opcoid      = $session->param('opcoid');
	my $userid      = $session->param( 'userid' );
    my $roleid      = $session->param( 'roleid' );
	my $username	= $session->param( 'username' );
	my $filter_username = $username;
	my $filter_userid	= $userid;

	my $vars = {
		full_filtername => $filtername,
		roleid	  => $roleid,
		opcoid	  => $opcoid,
	};

	eval {
		die "Filter name is undefined" if ( ( ! defined $filtername ) || ( $filtername =~ /^\s*$/ ) );
		my $filter_userid = $userid;
		if ( $filtername =~ m/\// ) {
			( $filter_username, $filtername ) = split "\/", $filtername;
			die "Invalid filter name" if (( ! defined $filtername ) || ( $filtername eq '' ));
			( $filter_userid ) = ECMDBUser::db_getUserId( $filter_username );
			die "Invalid filter owner $filter_username" unless defined $filter_userid;
		}
		$vars->{filtername} = $filtername;
		$vars->{filter_userid} = $filter_userid;
		$vars->{filter_username} = $filter_username;

		$vars->{filterdata}= join("~~", ECMDBUserFilter::getFilterData( $filter_userid, $opcoid, $filtername ));

		# Get lane type, can be null if swim lane not configured for filter

		my ( $filter_id, $filterownerid ) = ECMDBKanbanboard::db_getFilterID( $filtername, $filter_userid, $opcoid );
		die "Invalid filter name '$vars->{full_filtername}', filter userid $filter_userid"  unless defined $filter_id;
		$vars->{filter_ownerid} = $filterownerid;
		
		$vars->{lane_type}		= ECMDBKanbanboard::getSwimLaneType( $filterownerid, $opcoid, $filter_id );
		# Get access flags
		my $grid_status = 0;
		my $config_status = 0;
		my $shared_user_access = 0;
		my $filter_owner_access = 0;
		my $filter_exist = 0;
		my $config_button_enabled = 0;

		if ( $filterownerid eq $userid ) {
			$config_status = 1; # If the userid and filterownerid are equal, user is the owner of that filter.
			$filter_owner_access = 1;
			$config_button_enabled = 1;
		} else {
			( $shared_user_access ) = ECMDBKanbanboard::db_sharedUserAccess( $filtername, $userid, $opcoid );
			if ( $shared_user_access ) {
				$config_status = 1;
			}
		}
	
		if ( $config_status ) {
			if ( $filter_owner_access ) {
				$filter_exist = ECMDBKanbanboard::db_checkSwimlaneConfig( $filter_id, $userid, $opcoid );
			}
			if ( $shared_user_access ) {
				$filter_exist = ECMDBKanbanboard::db_checkSwimlaneConfig( $filter_id, $filterownerid, $opcoid );
			}
			if ( $filter_exist ) {
				$grid_status = 1; # If the grid status is one , the owner configured swim lane for this filter. 
			}
		}
		$vars->{filter_flags} = { 
			filter_owner_access 	=> $filter_owner_access,
			shared_user_access		=> $shared_user_access,
			config_button_enabled	=> $config_button_enabled,
			config_status			=> $config_status,
			grid_status				=> $grid_status,
		};
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = $error;
	}
	if ( $mode eq "json") {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

##############################################################################
####    SaveFilter
####
#################################################################################
sub SaveFilter {
    my $query       = $_[0];
    my $session     = $_[1];

    my $params	    = $query->param( "params" );
	my $filtername  = $query->param( "filtername" );
	$filtername =~ s/^\s*(.*?)\s*$/$1/;
    my $mode        = $query->param( "mode" ) || '';
	my $sharedusers	= $query->param( "sharedusers" );

    my $userid      = $session->param( 'userid' );
    my $cmopcoid    = $session->param('opcoid');

    my $vars;

	eval {
    	ECMDBUserFilter::db_saveFilter( $userid, $params, $cmopcoid, $filtername, $sharedusers );
		my $status  = triggerSharedMail( $userid, \$sharedusers, \$filtername, $cmopcoid  ) if ( $sharedusers ne "" );
		my $userfiltersettings_var = join(",", ECMDBUserFilter::getUserFilterSettings( $userid, $cmopcoid ));
		$session->param( 'userfiltersettings', $userfiltersettings_var);

		ECMDBAudit::db_recordJobAction(undef, $userid, CTSConstants::SAVE_FILTER );

		$vars = { SaveFilter => 1, };
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => $error, };
	}
	if ( $mode eq "json" ) {
		my $json = encode_json ( $vars );
		print $json;
		return $json;
	}
}

##############################################################################
#####    RenameFilter
#####
##############################################################################
sub RenameFilter {
    my $query       		= $_[0];
    my $session     		= $_[1];

    my $filtername  		= $query->param( "filtername" );
    $filtername 			=~ s/^\s*(.*?)\s*$/$1/;
	my $selectedfiltername  = $query->param( "selectedfiltername" );
    $selectedfiltername 	=~ s/^\s*(.*?)\s*$/$1/;
    my $mode        		= $query->param( "mode" ) || '';
    my $addedusers 			= $query->param( "addeduser" );	
	my $deletedusers 		= $query->param( "deleteduser" );
	my $sharedusers_flag 	= $query->param( "sharedusers_flag" );
	my $sharedfilterid      = $query->param( "sharedfilterid");

    my $userid      		= $session->param( 'userid' );
    my $cmopcoid    		= $session->param('opcoid');
	my $username			= $session->param('username'); #Owner only can edit the filter
	my $vars;

	eval {
    	ECMDBUserFilter::db_renameFilter($userid, $selectedfiltername, $cmopcoid, $filtername, $addedusers, $deletedusers,$sharedusers_flag,$sharedfilterid);

		my $status	= triggerSharedMail( $userid, \$addedusers, \$selectedfiltername, $cmopcoid ) if ( $addedusers ne "" );
		my $userfiltersettings_var = join(",", ECMDBUserFilter::getUserFilterSettings( $userid, $cmopcoid ));
       	$session->param( 'userfiltersettings', $userfiltersettings_var);
		
       	ECMDBAudit::db_recordJobAction(undef, $userid, CTSConstants::RENAME_FILTER );
    	$vars = {
        	RenameFilter => 1,
			username	 => $username
    	};
	};
	if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        $vars = {
            error => $error,
        };
    }
    if ( $mode eq "json" ) {
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
######    DeleteFilter
######
###############################################################################
sub DeleteFilter {
    my $query       = $_[0];
    my $session     = $_[1];

	my $mode				= $query->param( "mode" ) || '';
	my $sharedfilterid		= $query->param( 'sharedfilterid' );
	my $unshared_flag		= $query->param( 'unshared_flag' );
	my $selectedfiltername	= $query->param( "selectedfiltername" );
	my $filter_username		= $query->param( "filter_username" );

	$selectedfiltername =~ s/^\s*(.*?)\s*$/$1/;

    my $userid      = $session->param( 'userid' );
    my $cmopcoid    = $session->param( 'opcoid' );
	my $vars = {};

	eval {
		my ( $filtername, $filter_username, $filteruserid ) = parseQualifiedFilter( $selectedfiltername,$session->param( 'username' ), $session->param( 'userid' ) );
		$selectedfiltername	=	$filtername;
		push( @{$vars->{history}}, 'Calling db_deleteFilter' );
		ECMDBUserFilter::db_deleteFilter( $userid, $selectedfiltername, $cmopcoid, $sharedfilterid,$unshared_flag , $filteruserid);
		my $userfiltersettings_var = join(",", ECMDBUserFilter::getUserFilterSettings( $userid, $cmopcoid ));
		$session->param( 'userfiltersettings', $userfiltersettings_var );
		push( @{$vars->{history}}, 'Calling db_recordJobAction' );
		ECMDBAudit::db_recordJobAction( undef, $userid, CTSConstants::DELETE_FILTER );
    	$vars = { DeleteFilter => 1, };
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars->{error} = $error;
	}
    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }
}

##############################################################################
#triggerMail: Triggerd shared mail.
##############################################################################
sub triggerSharedMail {

	my $fromUserID				= $_[0];
	my $toUserID_ref			= $_[1];
	my $filterName_ref			= $_[2];
	my $sessionOpcoID			= $_[3];

	my $mailStatus				= 0;
	my $smtp_gateway			= Config::getConfig("smtp_gateway") || 'jobflow@tagworldwide.com';

	my $sessionOpcoName			= ECMDBOpcos::getOpcoName( $sessionOpcoID );
	my @fromUserDetails_ref 	= ECMDBUserFilter::getSharedUserDetails( \$fromUserID );
	my $fromUserName			= $fromUserDetails_ref[0]->{USERNAME};	
	my $fromUserEmail			= $fromUserDetails_ref[0]->{email};	
	my $mailSubject             = "Jobflow Notification";
	my @sharedUserDetails_ref	= ECMDBUserFilter::getSharedUserDetails( $toUserID_ref );

	foreach my $sharedUser_ref  ( @sharedUserDetails_ref ) {
		my $mailBody	= 	"<i><p>Dear $sharedUser_ref->{USERNAME},</p>" .
							"<p>$fromUserName has invited you to participate in a JobFlow project. " .
							"To view this project, log into \"JobFlow\" and select Operating Company: \'$sessionOpcoName\' then load filter:</p>" .
							"<p>\'$$filterName_ref\'</p>" .
							"<p>Regards, $fromUserName</p></i>";
		$mailStatus	= Mailer::send_mail ( $fromUserEmail, $sharedUser_ref->{email}, $mailBody, "$mailSubject", { host => $smtp_gateway, html => $mailBody } );
	}
	return $mailStatus;
}#triggerMaiet
##############################################################################
##updateFilterPinning: To update the pinning status in db
###############################################################################
sub updateFilterPinning {
    my $query       = $_[0];
    my $session     = $_[1];

    my $mode        = $query->param( "mode" ) || '';
	my $username	= $query->param( "username" );
	my $enable		= $query->param( "enable" );  #1 means enable 0 means disable
	my $filtername	= $query->param( "filtername" );

    my $userid      = $session->param( 'userid' );
    my $opcoid    	= $session->param( 'opcoid' );


	my $filteruserid= ECMDBUser::db_getUserId( $username );
	my $ownertype	= ( $filteruserid =~ $userid ) ? 1 : 0; ##1 means owner 'my filters'. 0 means 'shared filters' 
    my $vars 		= {};


    eval {
        push( @{$vars->{history}}, 'Calling db_updateFilterpinning' );
		ECMDBUserFilter::db_updateFilterpinning($filtername, $userid, $opcoid, $ownertype, $enable, $filteruserid);
		my ($pinningstatus, $pinningID, $ownertype)=ECMDBUserFilter::db_getFilterpinning($filtername, $userid, $opcoid, $filteruserid);
        $vars = { 
					Filterpinning 	 => 1, 
					pinningstatus	 => $pinningstatus,
					pinningID		 => $pinningID,
					ownertype		 => $ownertype,
					full_filtername	 => $username."/".$filtername,
					filtername		 => $filtername,
					owner			 => $username
				};
    };
    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        $vars->{error} = $error;
    }
    if ( $mode eq "json" ){
        my $json = encode_json ( $vars );
        print $json;
        return $json;
    }

}
sub parseQualifiedFilter {
    my ( $filtername, $username, $userid ) = @_;
    my $filter_username = $username;
    my $filter_userid = $userid;
    if ( $filtername =~ m/\// ) {
	 ( $filter_username, $filtername ) = split "\/", $filtername;
	 die "Invalid filter name" if (( ! defined $filtername ) || ( $filtername eq '' ));
	( $filter_userid ) = ECMDBUser::db_getUserId( $filter_username );
	die "Invalid filter owner $filter_username" unless defined $filter_userid;
    }
    return ( $filtername, $filter_username, $filter_userid );
} 

1;

