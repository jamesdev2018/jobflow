#!/usr/local/bin/perl

package JFElastic;

use warnings;
use strict;
use Search::Elasticsearch;
use Data::Dumper;
use JSON;


# Elasticsearch host (AWS URL includes auth) 
# Can be a list of nodes
my $environment = $ENV{"TAG_ENV"} || "DEV";
my $endpoint    = 'http://elastic:j0bf1ow@172.27.93.212:8080';
my $index       = '';
my $type        = 'brief';

if($environment eq "DEV")
	{
		$index = "dev";
	}
	elsif($environment eq "STAGING")
	{
		$index = "staging";
	}
	elsif ($environment eq "LIVE")
	{
		$index = "prod";
	}
	else 
	{
		die "Invalid value of TAG_ENV $environment";
	}


##############################################
# Search JobFlow Brief
##############################################
sub searchBrief {
    my $searchString = shift;
    my $opcoid       = shift;
  
    # returns
	my $resultSize = 1000;
    my @job_numbers;
  
    my $es = Search::Elasticsearch->new(
      nodes => [
         $endpoint
      ]
    );
  
    # search ES indexx
    my  $results = $es->search(
        index => $index,
        type  => $type,
        body  => {
        _source => [ "job_number" ],
		size => $resultSize,
           query => {
              bool => {
                 must => [
                     {term => {opcoid => $opcoid}},
                     {match => {message => $searchString}}
                 ]
              }
           }
        }
    );
  
	if ( ! defined $results ) {
		warn "searchBrief: No results returned, opcoid $opcoid, search string '" . $searchString , "'";

	} elsif ( ( ! exists $results->{hits} ) || ( ! exists $results->{hits}->{hits} ) ) {
		warn "searchBrief: Results returned no hits struct, opcoid $opcoid, search string '" . $searchString , "'";
		# warn Dumper( $results );

	} elsif ( ref( $results->{hits}->{hits} ) ne 'ARRAY' ) {
		warn "searchBrief: Expected Array of hits, got " . ref( $results->{hits}->{hits} ) . " opcoid $opcoid, search string '" . $searchString , "'";

	} else {  
		foreach my $entry ( @{ $results->{hits}->{hits} } ) {
			if ( exists $entry->{_source} ) {
				push( @job_numbers, $entry->{_source}->{job_number} );
			}
		}
	}

    #foreach my $i (keys %{ $results->{hits}->{hits} }){
    #      my $job_number = $results->{hits}->{hits}[$i]{_source}{job_number};
    #      push(@job_numbers, $job_number);
    #}
  
    return @job_numbers;
}


##############################################
# Index JobFlow Brief
##############################################
sub indexBrief {
    my $opcoid     = shift;
	my $job_number = shift;
	my $message    = shift;
	
	my $es = Search::Elasticsearch->new(
      nodes => [
         $endpoint
      ]
    );
	
	# index the brief
	my  $results = $es->index(
    index => $index,
    type  => $type,
    body  => {

                job_number => $job_number,
                opcoid     => $opcoid,
                message    => $message
             }
    );

}

1;
