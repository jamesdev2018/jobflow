#!/usr/local/bin/perl
use strict;
use warnings;


package JobImport;

use Template;
use JSON;
use Encode qw(decode encode);

use Spreadsheet::Read;

use lib 'modules';

require 'config.pl';
require 'databaseJob.pl';
require 'databaseJobImport.pl';
require 'data_cacher.pl';

##################################################################
# the template object 
##################################################################
my $tt = Template->new( {
	RELATIVE => 1,
} ) || die "$Template::ERROR\n";

my @field_types = ("text", "number", "datetime");


sub view {
	my ($query, $session) = @_;
	
	my @previous_spreadsheets = ECMDBJobImport::get_spreadsheets_by_user($session->param('userid'));
	splice(@previous_spreadsheets, 10);	## Don't show more than 10 entries

	my $vars = {
		sidemenu	=> scalar($query->param( "sidemenu" )),
		opcoid => $session->param('opcoid'),
		countries => [ ECMDBJob::db_getcountries() ],
		previous_imports => \@previous_spreadsheets,
	};
	
	HTMLHelper::setHeaderVars( $vars, $session );
	
	$tt->process('ecm_html/job_import/import_search.html', $vars )
    		|| die $tt->error(), "\n";
}


sub search {
	my ($query, $session) = @_;
	
	my $search_settings = {};
	my $debug_text = "";
	foreach my $opt (('customer', 'client', 'campaign', 'country', 'mediatype', 'opco')) {
		# Store the IDs and Values in the session
		my $option_id = $query->param($opt) || $session->param($opt . 'id') || '';
		my $option_value = $query->param($opt . '-value') || $session->param($opt);
		
		if($option_id eq '-clear-') {
			$session->param($opt . 'id', undef);
			$session->param($opt, undef);
		} else {
			$session->param($opt . 'id', $option_id);
			$session->param($opt, $option_value);
		}
		
		$search_settings->{$opt . "id"} = $option_id if $option_id ne '-clear-';
		$debug_text .= ($debug_text ? ', ' : '') . "$opt: " . $option_id . ($option_value ? " (" . $option_value . ')' : '') if ($option_value || $option_id) && (($option_id || '') ne '-clear-');
	}
	
	
	
	
	my ($jobs, $fields) = ECMDBJobImport::get_records($search_settings);			## The joblist and fields used
	my @mappable_fields = ECMDBJobImport::get_import_fields($search_settings->{mediatype}, $search_settings->{customer});	## A list of the fields we can map to in the mapping dialog
	for(my $i = scalar @mappable_fields; $i >= 0; $i--) { splice(@mappable_fields, $i, 1) if !$mappable_fields[$i]->{is_mappable}; }
	
	my @mapping_types = ECMDBJobImport::get_mapping_types();						## A list of saved mapping types (used for convertions, parsing, etc. on import)
	my %fields_by_key = map { lc($_->{field_key}) => $_->{id} } @mappable_fields;
	
	## Order the fields by the user mapping if possible
	my @column_layout = ECMDBJobImport::get_column_layout($search_settings);		## A list of the fields used in user-saved order (where applicable)
	my @ordered_fields;
	foreach my $column (@column_layout) {		## Loop through the requested order
		foreach my $field (@$fields) {		## Loop through the available fields
			next if !defined $field;
			
			if($column->{id} eq $field->{id}) {
				push(@ordered_fields, $field);
				undef $field;
			}
		}
	}
	foreach my $field (@$fields) { push(@ordered_fields, $field) if defined $field; }
	
	my $vars = {
		sidemenu	=> "" . ($query->param( "sidemenu" ) || ''),
		jobs => $jobs,
		job_columns => \@ordered_fields,
		mapping_types => \@mapping_types,
		fields_by_key => \%fields_by_key,
		
		automap_limit => Config::getConfig('jobimport_automap_limit') || 10,
		
		buildinfo => $debug_text,
		
		customer => $session->param('customer'),
		client => $session->param('client'),
		campaign => $session->param('campaign'),
		country => $session->param('country'),
		mediatype => $session->param('mediatype'),
		opcoid => $session->param('opcoid'),
		customerid => $session->param('customerid'),
		clientid => $session->param('clientid'),
		campaignid => $session->param('campaignid'),
		countryid => $session->param('countryid'),
		mediatypeid => $session->param('mediatypeid'),
		
		current_spreadsheet_id => $session->param('current_spreadsheet_id') || '',		## Highlight most-recently-uploaded spreadsheet data
		
		mappable_fields => \@mappable_fields,
		column_layout	=> \@column_layout,
	};
	HTMLHelper::setHeaderVars( $vars, $session );
	
	$session->param('current_spreadsheet_id', undef);
	
	$tt->process('ecm_html/job_import/import_list.html', $vars )
    		|| die $tt->error(), "\n";
}

sub show_substitution_rules {
	my ($query, $session) = @_;
	
}

############################################################################
# viewsettings
#
############################################################################
sub viewsettings {
	my ( $query, $session ) = @_;

	my @job_import_settings = ECMDBJobImport::get_job_import_fields();

	my $vars = {
		job_import_settings => \@job_import_settings,
	};

	HTMLHelper::setHeaderVars( $vars, $session );

	my $tt = Template->new({ RELATIVE => 1, }) || die "$Template::ERROR\n";
	$tt->process('ecm_html/job_import/job_import_settings.html', $vars )
						|| die $tt->error(), "\n";
}

############################################################################
# editjobimportfield
#
############################################################################
sub editjobimportfield {
	my ( $query, $session ) = @_;
	my $vars;
	my @import_setting;
	my $id = $query->param("id");

	@import_setting = ECMDBJobImport::get_job_import_field( $id ) if ($id gt 0);

	my @job_import_settings = ECMDBJobImport::get_job_import_fields();

    $vars = {
		import_setting		=> \@import_setting,
        job_import_settings => \@job_import_settings,
		field_types			=> \@field_types,
    };	

	my $json = encode_json ( $vars );
    print $json;
    return $json;
}

############################################################################
# savejobimportfield
#
############################################################################
sub savejobimportfield {
	my ( $query, $session ) = @_;
	my $vars;

	eval {
		my $id			= $query->param("id");
		my $title		= $query->param("title");
		my $desc			= $query->param("description");
		my $metakey		= $query->param("meta-name");
		my $fieldtype	= $query->param("type");
		my $fieldname	= $query->param("cmd-name");
		my $indexhint	= $query->param("ordering-hint");
		my $ismandatory	= $query->param("mandatory");
		my $ismappable	= $query->param("mappable");
		my $isvisible	= $query->param("visible");
		my $mediatypeid	= "";
		my $customerid	= "";

		if ($id){
			$id = ECMDBJobImport::db_update_job_import_field($id, $title, $desc, $metakey, $fieldtype, $fieldname, $indexhint, $ismandatory, $ismappable, $isvisible, $mediatypeid, $customerid);
		}
		else {
			$id = ECMDBJobImport::db_save_job_import_field($title, $desc, $metakey, $fieldtype, $fieldname, $indexhint, $ismandatory, $ismappable, $isvisible, $mediatypeid, $customerid);
		}

		$vars = {
			"savejobimportfield" => "success",
			id => $id,
		};
	};
    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
		$vars = { error => "savejobimportfield: ".$error };
    }

	my $json = encode_json ( $vars );
	print $json;
	return $json;
}

############################################################################
# deletejobimportfield
#
############################################################################
sub deletejobimportfield {
	my ( $query, $session ) = @_;
	my $vars;

	eval {
		my $id = $query->param("id");
		ECMDBJobImport::remove_import_field($id);

		$vars = {
			id => $id,
		};
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => "deletejobimportfield: ".$error };
	}

	my $json = encode_json ( $vars );
	print $json;
	return $json;
}










sub validate_record_data {
	my ($recordid) = @_;
	
}


sub construct_validation_list {
	my ($session, $fieldid, $data) = @_;
	# data is a hash of fieldid:value entries
	
	my @vlist;
	
	## Check each field for any specific validations
	my @validations = ECMDBJobImport::get_validations_from_field($fieldid);
	
	## Go through the validations...
	my $required_dependencies = {};
	foreach my $validation (@validations) {
		my $depend = $validation->{dependencies};
		my $matches = !$depend;
		
		# validations are newline-separated of the form fieldid=['value1','or_value2',...]
		## TODO: Get this to work properly
		$matches = 1;
		while($depend =~ m/^(.*)\$([0-9]+)=(.*)$/m) {
			$depend = "$1$3";
			my ($id) = $2;
			
			## Save a list of dependencies in case nothing matches; then we know where to look
			$required_dependencies->{$id} = ($required_dependencies->{$id} || 0) || ($data->{$id} ? 1 : 0);
			#if($2 =~ m/'$data->{$1}'/) { $matches = 1; last; }
		}
		
		if($matches) {
			my @sublist;
			if($validation->{data_source} eq 'validationlist')	{ @sublist = ECMDBJobImport::build_vlist_from_list($validation->{listid}); }
			elsif($validation->{data_source} eq 'table')			{ @sublist = ECMDBJobImport::build_vlist_from_table($validation->{table_name}, $validation->{source_id_field}, $validation->{source_data_field}); }
			elsif($validation->{data_source} eq 'api')			{ @sublist =                 build_vlist_from_api($validation->{api_uri}, $validation->{source_id_field}, $validation->{source_data_field}, $data, $validation->{cache_age_limit} * 60); }
			elsif($validation->{data_source} eq 'session')		{ @sublist =                 build_vlist_from_session($session, $validation->{source_id_field}, $validation->{source_data_field}); }
			
			push(@vlist, @sublist);	## vlist is additive
		}
	}
	
	my @unmet_dependencies;
	if(!scalar @vlist) {
		foreach my $key (keys %$required_dependencies) { push(@unmet_dependencies, $key); }# if !$required_dependencies->{$key}; }
	}
	
	return (\@vlist, \@unmet_dependencies);
}


sub build_vlist_from_api {
	my ($api_uri, $id_field, $data_field, $data, $expiration_limit) = @_;
	
	while($api_uri =~ m/^(.*)\$\{([^}]+)\}(.*)$/) {
		$api_uri = ($1 || '') . ($data->{$2 || ''} || '') . ($3 || '');
	}
	
	my $output = DataCache::get_data_from_api($api_uri, $expiration_limit);
	die "No output received from '$api_uri'" if !$output;
	
	my $decoded;
	eval { $decoded = decode_json($output) };
	## Ignore this so we can okay invalid results BD
	#die "Couldn't translate api result '$output' ($api_uri)" if $@;
	
	my @result;
	foreach (@$decoded) { push(@result, { id => $_->{$id_field}, text => $_->{$data_field} }); }
	
	return @result;
}

sub build_vlist_from_session {
	my ($session, $id_field, $data_field) = @_;
	
	return ({ id => $session->param($id_field), text => $session->param($data_field) });
}











sub viewimportsubstitutions {
	my ( $query, $session ) = @_;

	my @job_import_substitutions = ECMDBJobImport::get_job_import_substitutions();

	my $vars = {
		job_import_substitutions => \@job_import_substitutions,
	};

	HTMLHelper::setHeaderVars( $vars, $session );

	my $tt = Template->new({ RELATIVE => 1, }) || die "$Template::ERROR\n";
	$tt->process('ecm_html/job_import/job_import_substitutions.html', $vars )
					|| die $tt->error(), "\n";
}

sub deletejobimportsubstitution {
	my ( $query, $session ) = @_;
	my $vars;

	eval {
		my $id = $query->param("id");
		ECMDBJobImport::remove_substitution($id);

		$vars = {
			id => $id,
		};
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => "deletejobimportsubstitution: ".$error };
	}

	my $json = encode_json ( $vars );
	print $json;
	return $json;
}

sub editjobimportsubstitution {
	my ( $query, $session ) = @_;
	my $vars;
	my @substitution;

	eval {
		my $id = $query->param("id");
		@substitution = ECMDBJobImport::get_job_import_substitution( $id ) if ($id gt 0);

		$vars = {
			substitution => \@substitution,
		};
	};
	if ( $@ ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => "editjobimportsubstitution: ".$error };
	}

	my $json = encode_json ( $vars );
	print $json;
	return $json;
}

sub savejobimportsubstitution {
    my ( $query, $session ) = @_;
    my $vars;

    eval {
        my $id          = $query->param("id");
        my $replacement	= $query->param("replacement");

		die "Invalid Id value for Job import substitution" unless ($id gt 0);

		$id = ECMDBJobImport::db_update_job_import_substitution($id, $replacement);

        $vars = {
            "savejobimportsubstitution" => "success",
        };
    };
    if ( $@ ) {
        my $error = $@;
        $error =~ s/ at .*//gs;
        $vars = { error => "savejobimportsubstitution: ".$error };
    }

    my $json = encode_json ( $vars );
    print $json;
    return $json;
}

1;
