#!/usr/local/bin/perl
package JobFilename;

use strict;
use warnings;

use Data::Dumper;

# Home grown

# Please do NOT include jobs.pl or any module that requires jobs.pl

use lib 'modules';
use lib 'modules/Utils'; # if unit testing

use DBAgent; # if unit testing

require "config.pl";
require "databaseJob.pl"; # db_getJobFields

#############################################################################
#       getJobFilename() - get filename for job
#		Uses specific template markers to substitute job attributes into the filename
#		The template is determined on an opco or customer basis and controls the filename
#		of assets delivered by email or FTP.
#		This function takes the template and job details and does the conversion.
# Inputs
# - $jobid, job number 
# - $filenametemplate, template from customerid
# Outputs
# - $error_ref - array of error messages
# Returns translated filename or "" if error
#
##############################################################################
sub getJobFilename {

	my ( $jobid, $filenametemplate, $error_ref ) = @_;

	my @keylist = (
		{ label => 'client', code => 'JFCL', field => 'client' },
		{ label => 'agency', code => 'JFCU', field => 'agency' },
 		{ label => 'project', code => 'JFCA', field => 'project' },
  		{ label => 'publication', code => 'JFPU', field => 'publication' },
  		{ label => 'job number', code => 'JFJN', field => 'job_number' },
  		{ label => 'agency ref', code => 'JFAJ', field => 'agencyref' },
   		{ label => 'opcoid', code => 'JFOC', field => 'opcoid' },
   		{ label => 'headline', code => 'JFHD', field => 'headline' },
   		{ label => 'business ref', code => 'JFBF', field => 'businessref' },
		{ label => 'format size', code => 'JFFS', field => 'formatsize' },
		{ label => 'formatsize width', code => 'JFSW', field => 'szwidth' },
		{ label => 'formatsize height', code => 'JFSH', field => 'szheight' },
		# { label => 'rootpath', code => 'JFRP', field => 'rootpath' },
		# { label => 'letter', code => 'JFLT', field => 'letter' },
	);

	my $finalfilename = $filenametemplate;
	
	my @jobFields	= ECMDBJob::db_getJobFields( $jobid );
	my $jobField = $jobFields[ 0 ];
	$jobField->{job_number} = $jobid;

	my @blank_fields;

	foreach my $item ( @keylist ) {
		# $item is hash with keys label, code, field (label is readable, code is the JF?? code, and field is used to access hash $jobField
		my $code = $item->{code};
		if ( $finalfilename =~ /\%\^$code\%/ ) {
			my $value = $jobField->{ $item->{field} };
			if ( ! defined $value ) {
				push( @blank_fields, $item->{label} . ' (' . $item->{code} . ')' );
			} else {
				$finalfilename =~ s/\%\^$code\%/$value/;
			}
		}
	}

	# if there was a problem building the file name - we abort
	if ( ( scalar @blank_fields ) > 0 ) {
		my $error = join( ", ", @blank_fields );
		if ( ( scalar @blank_fields ) > 1 ) {
			push( @$error_ref, "Job $jobid, fields $error are undefined" );
		} else {
			push( @$error_ref, "Job $jobid, field $error is undefined" );
		}
	}

	return $finalfilename;
}

# Test stubs

sub test_connect {
	eval   { # for unit test only
		Config::initialise_config(); 
	};
	if($@) { 
		my $error = $@;
		print "{\"error\":\"$error\",}\n";
		die "badly";
	}
}


sub test_getJobFilename {

	my $template = '%^JFCA%_%^JFPU%_%^JFJN%.pdf';
	my $jobid = 104000014;
	my @errors;

	# Test for PD-3533 on Live, returned 90807928_HFC_PRESTIGE_GUCCI_DENMARK_INT_KV_3000X1800.pdf
	# $template = '%^JFJN%_%^JFCU%_%^JFCL%_%^JFHD%_%^JFSH%X%^JFSW%.pdf';
	# $jobid = 90807928;

	test_connect();

	warn "getJobFilename supplied template $template\n";
	my @jobFields	= ECMDBJob::db_getJobFields( $jobid );
	warn "Job details " . Dumper( $jobFields[0] ) . "\n";
	my $filename = getJobFilename( $jobid, $template, \@errors );
	warn "getJobFilename returned filename $filename\n";
	warn Dumper( \@errors ) . "\n" if ( scalar @errors );

}

# exit __PACKAGE__->test_getJobFilename() unless caller();

1;

