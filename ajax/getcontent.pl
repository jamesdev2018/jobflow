#!/usr/local/bin/perl -w

use warnings;
use strict;

use lib "../modules";

use CGI;
use DBI;
use JSON;
use URI::Escape;
use Scalar::Util qw( looks_like_number );

require 'config.pl';

my $query = new CGI;
print $query->header(-charset=>'utf-8');
eval   { Config::initialise_config(); };
if($@) { print encode_json({ error => $@ }); }

my $vars			= "";
my $content 		= "";
my $mode			= lc($query->param('mode') || '');

eval {
	my $type_of_content = lc($query->param('type') || '');

	## Grab the content
	if ( $type_of_content =~ /^statusnotes$/i ) {
		my $mediatype		= $query->param('mediatype');
		my $job_number		= $query->param('job_number') || '0';
		my $status_notes	= "";

		if ( (defined $mediatype ) && ( $mediatype =~ /^BROADCAST$/i ) ) {
			($status_notes) = $Config::dba->process_oneline_sql("SELECT SMOKECMD FROM Broadcasttracker3 WHERE JOB_NUMBER=?",
                                                    [ $job_number ]);	
		} else {
			($status_notes) = $Config::dba->process_oneline_sql("SELECT
																	CASE WHEN p.JOBVERSION > 1
															   			THEN p.PRODUCTIONREMARKS
													   				ELSE 
															   			t3p.brief
													   				END AS status_note, p.id from tracker3plus t3p
									   							LEFT OUTER JOIN products p on t3p.job_number =  p.job_number 
									   							WHERE t3p.job_number = ? order by p.ID DESC LIMIT 1;", [ $job_number ] );


		}

		$status_notes   = uri_escape_utf8($status_notes) if defined $status_notes;

		$vars	= {
				status_notes	=> $status_notes,
		};
	
	} elsif($type_of_content eq 'comment') {
		($content) = $Config::dba->process_oneline_sql("SELECT content FROM comments WHERE id=?",
		                                            [ $query->param('id') || 0 ]);
	} elsif($type_of_content eq 'message') {
		($content) = $Config::dba->process_oneline_sql("SELECT content FROM messages WHERE messageid=?",
		                                            [ $query->param('id') || 0 ]);
	} else {
		die "Invalid content type requested: '$type_of_content'\n";
	}
};


## tinyMCE emojis are sourced from the [Jobflow]/js/ folder, but so remind the
## browser that relative links come from [Jobflow]/ and not [Jobflow]/ajax/
#print "<html>\n<head>\n<base href='../'>\n</head>\n<body>";

if($@)
{
	if ( $mode	=~ /^json$/i ) {
		my $error = $@;
		$error =~ s/ at .*//gs;
		$vars = { error => $error };
	} else {
		print "<div style='border: 1px solid #fcc; padding: 2em; text-align: center'>";
		print "<div style='font-weight: bold; color: red'>Error retrieving content:</div><br>";
		print $@;
		print "</div>";
	}
} else {
	if ( $mode  =~ /^json$/i ) {
		my $json = encode_json ( $vars );
		print $json;
    } else {
		print '<pre style="white-space: pre-wrap !important;">'.$content.'</pre>';
	}
}
#print "</body>\n</html>";

