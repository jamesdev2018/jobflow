#!/usr/local/bin/perl -w

use warnings;
use strict;

use lib "../modules";

use CGI;
use DBI;
use JSON;

require "config.pl";

my $query = new CGI;
print $query->header(-charset=>'utf-8');
eval   { Config::initialise_config(); };
if($@) { print encode_json({ error => $@ }); }

#database requires
my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");
my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");

my $userid = $query->param("userid");
my $opcoid = $query->param("opcoid");

print ( getUserCustomers( $userid, $opcoid, "json" ) );


##############################################################################
#      getUserCustomers 
#
##############################################################################
sub getUserCustomers {

	my @usercustomers;
	my ( $userid, $opcoid, $mode, $custid, $custname );
	
	$userid = $_[0];
	$opcoid	= $_[1];
	$mode 	= $_[2];

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ select cu.customerid, c.customer
			from customeruser cu
			inner join customers c ON c.id = cu.customerid
			where cu.userid = ?
			and c.opcoid = ?  };

	my $sth = $dbh->prepare( $sql );
	
	$sth->execute( $userid, $opcoid );

	$sth->bind_columns( \$custid, \$custname);

	while ( $sth->fetch() ) {
		my %row = (
			custid		=> $custid,
			custname	=> $custname,
		);
		push ( @usercustomers, \%row );

	}

	$sth->finish();
	$dbh->disconnect();

	if ( $mode eq "json") {
		return ( encode_json( \@usercustomers ) );
	} else {
		return @usercustomers;
	}
}

