#!/usr/local/bin/perl -w

use warnings;
use strict;

use lib "../modules";

use CGI;
use DBI;
use JSON;
use Scalar::Util qw( looks_like_number );

require "config.pl";

my $query = new CGI;
print $query->header(-charset=>'utf-8');
eval   { Config::initialise_config(); };
if($@) { print encode_json({ error => $@ }); }

#database requires
my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");
my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");

my $userid = $query->param("userid");
my $opcoid = $query->param("opcoid");

print "About to remove opco<br>";
removeuseropcoid( $userid, $opcoid );

print "Deleting roles ...<br>\n";

# delete the old roles first
deleteuserroles( $userid, $opcoid );

print "Deleting customers ...<br>\n";
deleteusercustomers( $userid, $opcoid );

print "Done<br>";


##############################################################################
#      removeuseropcoid 
#
##############################################################################
sub removeuseropcoid {

	my $userid = $_[0];
	my $opcoid = $_[1];
	
	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ DELETE FROM useropcos where userid = ? AND opcoid = ?};
	my $sth = $dbh->prepare( $sql );
	$sth->execute( $userid, $opcoid);
	$sth->finish();
	$dbh->disconnect();
}

##############################################################################
#      deleteuserroles
#
##############################################################################
sub deleteuserroles {

	my $userid = $_[0];
	my $opcoid = $_[1];

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ DELETE FROM USERROLES WHERE userid = ? AND opcoid = ? };
	my $sth = $dbh->prepare( $sql );
	
	$sth->execute( $userid, $opcoid );

	$sth->finish();
	$dbh->disconnect();
}

##############################################################################
#      deleteusercustomers
#
##############################################################################
sub deleteusercustomers {

	my $userid = $_[0];
	my $opcoid = $_[1];

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ DELETE cu.* FROM customeruser cu
					INNER JOIN customers c ON c.id=cu.customerid
					WHERE userid = ? AND c.opcoid=? };
	my $sth = $dbh->prepare( $sql );
	
	$sth->execute( $userid, $opcoid );

	$sth->finish();
	$dbh->disconnect();
}

