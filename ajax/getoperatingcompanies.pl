#!/usr/local/bin/perl -w

use warnings;
use strict;

use lib "../modules";

use CGI;
use DBI;
use JSON;

require "config.pl";

my $query = new CGI;
print $query->header(-charset=>'utf-8');
eval   { Config::initialise_config(); };
if($@) { print encode_json({ error => $@ }); }

#database requires
my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");
my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");


my $userid = $query->param("userid");

print ( getOperatingCompanies( $userid, "json" ) );

##############################################################################
#      getOperatingCompanies 
#
##############################################################################
sub getOperatingCompanies {

	my @opcos;
	my ( $userid, $mode, $opcoid,  $opconame);
	
	$userid	= $_[0];
	$mode 	= $_[1];

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ select o.opcoid, o.opconame 
			from operatingcompany o 
			where opcoid not in (13, 14, 15, 16)
			and opcoid not in ( select opcoid from useropcos where userid = ?)
			order by o.opconame };
	my $sth = $dbh->prepare( $sql );
	
	$sth->execute( $userid );

	$sth->bind_columns( \$opcoid, \$opconame);

	while ( $sth->fetch() ) {
		my %row = (
			opcoid		=> $opcoid,
			opconame	=> $opconame,
		);
		push ( @opcos, \%row );

	}

	$sth->finish();
	$dbh->disconnect();

	if ( $mode eq "json") {
		return ( encode_json( \@opcos ) );
	} else {
		return @opcos;
	}
}

