#!/usr/local/bin/perl -w

use warnings;
use strict;

use CGI;
use JSON;
use DBI;
#use Data::Dumper;
#use Cwd;

use lib "../modules";
#require "databaseOpcos.pl"; # origin of getCustomerAddressees
require "config.pl";

my $query = new CGI;
print $query->header(-charset=>'utf-8');
eval   { Config::initialise_config(); };
if($@) { print encode_json({ error => $@ }); }

my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");

my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");


my $query = new CGI;

print ($query->header() );

my $customerid = $query->param("customerid");

#print "Cwd " . getcwd . "<br>\n";
 
my @addresses = getCustomerAddressees( $customerid );

#push ( @addresses, { id => 0, email => "No customer emails" } ) if ( ( scalar @addresses ) == 0 );

#print Dumper( @addresses );

print ( encode_json( \@addresses ) . "\n" );


##############################################################################
# getCustomerAddressees (lifted from modules/databaseOpcos.pl)
##############################################################################
sub getCustomerAddressees {

	my $customerid = $_[0];

	my @customerAddressees;
	my ( $id, $email );

	my $dbh = DBI->connect("DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 });
	my $sql = qq{ SELECT id, email FROM CUSTOMERADDRESSES WHERE customerid = ?};
	my $sth = $dbh->prepare( $sql );
	$sth->execute( $customerid );

	$sth->bind_columns( \$id, \$email );

	while ( $sth->fetch() ) {
		my %row = (
			id		=> $id,
			email		=> $email,
		);
		push ( @customerAddressees, \%row );
	}

	$sth->finish();
	$dbh->disconnect();

	return @customerAddressees;
}


