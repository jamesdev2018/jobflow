#!/usr/local/bin/perl -w

use warnings;
use strict;

use lib "../modules";

use CGI;
use DBI;
use JSON;

require "config.pl";

my $query = new CGI;
print $query->header(-charset=>'utf-8');
eval   { Config::initialise_config(); };
if($@) { print encode_json({ error => $@ }); }

require "general.pl";
#database requires
my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");
my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");


my $userid = $query->param("userid");
my $opcoid = $query->param("opcoid");

print ( getUserRoles( $userid, $opcoid, "json" ) );

##############################################################################
#      getUserRoles 
#
##############################################################################
sub getUserRoles {

	my @userroles;
	my ( $userid, $opcoid, $mode, $roleid, $rolename );
	
	$userid = $_[0];
	$opcoid	= $_[1];
	$mode 	= $_[2];

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ SELECT r.id, r.role
			FROM roles r
			INNER JOIN userroles ur ON ur.roleid = r.id
			AND ur.userid = ?
			AND ur.opcoid = ? };

	my $sth = $dbh->prepare( $sql );
	
	$sth->execute( $userid, $opcoid );

	$sth->bind_columns( \$roleid, \$rolename);

	while ( $sth->fetch() ) {
		my %row = (
			roleid		=> $roleid,
			rolename	=> $rolename,
		);
		push ( @userroles, \%row );

	}

	$sth->finish();
	$dbh->disconnect();

	if ( $mode eq "json") {
		return ( encode_json( \@userroles ) );
	} else {
		return @userroles;
	}
}
