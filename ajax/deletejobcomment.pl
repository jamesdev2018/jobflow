#!/usr/local/bin/perl -w

use warnings;
use strict;

use lib "../modules";

use CGI;

require "config.pl";

my $query = new CGI;
print $query->header(-charset=>'utf-8');
eval   { Config::initialise_config(); };
if($@) { print encode_json({ error => $@ }); }


my $id = $query->param('id');


$Config::dba->process_sql("UPDATE comments SET status=0 WHERE id=?" , [ $id ] );


my ($jobnumber,$lineitemid,$sequence) = $Config::dba->process_oneline_sql("SELECT job_number,lineitemid,sequence FROM  comments WHERE id=?" , [ $id ] );

$Config::dba->process_sql("update comments set status=0  WHERE sequence=? AND job_number=? AND lineitemid=?" , [  $sequence , $jobnumber , $lineitemid ] );

print $id;
