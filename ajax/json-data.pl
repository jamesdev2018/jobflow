#!/usr/local/bin/perl -w

use warnings;
use strict;

use lib "../modules";

use CGI;
use DBI;
use JSON;

require "config.pl";
require "general.pl";

my $query = new CGI;
print $query->header(-charset=>'utf-8');
eval   { Config::initialise_config(); };
if($@) { print encode_json({ error => $@ }); }

require "databaseUser.pl";

#database requires
my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");
my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");



my $userid = $query->param("userid");

print ( getUser( $userid, "json" ) );

##############################################################################
#      getUser 
#
##############################################################################
sub getUser {

	my @user;
	my ( $id, $status, $opcoid, $companyid, $fname, $lname, $username, $email, $deputyemail, 
		$broadcast, $catalogue, $directmarketing, $interactive, $literature, $outdoor, $packaging, $pointofsale, $press, $artworker, $creative, $design, $junior, $retouching,
		$employeeid, $role, $dept, $rec_timestamp, $cost, $contracted, $workhrsstart,$workhrsend, $media, $selected_skills);
	
	$id 	= $_[0];
	my $mode 	= $_[1];
return 0 if !General::is_valid_id( $id );



	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ SELECT 
					u.id, u.status, u.opcoid, u.companyid, u.fname, u.lname, u.username, u.email, u.deputyemail, 
					u.broadcast, u.catalogue, u.directmarketing, u.interactive, u.literature, u.outdoor, u.packaging, u.pointofsale, u.press, u.artworker, u.creative, u.design, u.junior, u.retouching,
					u.employeeid, u.role, u.dept, u.rec_timestamp, u.cost_center, u.contracted_hours, u.workhoursstarts, u.workhoursends,GROUP_CONCAT(CONCAT(usm.skillid)) as selected_skills,GROUP_CONCAT(CONCAT(m.mediatypeid,'~',m.options)) as media FROM  USERS u 
					LEFT OUTER JOIN userskillsmapping usm ON usm.userid=u.id  AND usm.options='1'
					LEFT OUTER JOIN  usermediatypes m ON  u.id=m.userid 
					WHERE u.id = ? group by m.userid };
	my $sth = $dbh->prepare( $sql );
	
	$sth->execute( $id );

	$sth->bind_columns( \$id, \$status, \$opcoid, \$companyid, \$fname, \$lname, \$username, \$email, \$deputyemail, 
				\$broadcast, \$catalogue, \$directmarketing, \$interactive, \$literature, \$outdoor, \$packaging, \$pointofsale, 
				\$press, \$artworker, \$creative, \$design, \$junior, \$retouching, \$employeeid, \$role, \$dept, \$rec_timestamp, \$cost, \$contracted, \$workhrsstart,\$workhrsend,\$selected_skills,\$media );

	while ( $sth->fetch() ) {
		my %row = (
			id 		=> $id,
			userstatus 	=> $status,
			opcoid 		=> $opcoid,
			companyid 	=> $companyid,
			fname 		=> $fname,
			lname 		=> $lname,
			username 	=> $username,
			email 		=> $email,
			deputyemail 	=> $deputyemail,
			employeeid	=> $employeeid,
			broadcast	=> $broadcast,
			catalogue 	=> $catalogue,
			directmarketing => $directmarketing ,
			interactive 	=> $interactive,
			literature 	=> $literature,
			outdoor 	=> $outdoor,
			packaging 	=> $packaging,
			pointofsale 	=> $pointofsale,
			press 		=> $press,
			artworker	=> $artworker,
			creative 	=> $creative,
			design 		=> $design,
			junior 		=> $junior,
			retouching	=> $retouching,
			role 		=> $role,
      			dept        	=> $dept,
       			costid      	=> $cost,
       			contractid  	=> $contracted,
       			workhrsstart 	=> $workhrsstart,
       			workhrsend  	=> $workhrsend,
			timestamp 	=> $rec_timestamp,
			media       	=> $media,
			selected_skills => $selected_skills,
		);
		push ( @user, \%row );

	}

	$sth->finish();
	$dbh->disconnect();

	if ( $mode eq "json") {
		return ( encode_json( @user[0] ) );
	} else {
		return @user;
	}
}

