#!/usr/local/bin/perl -w

use warnings;
use strict;

use lib "../modules";

use CGI;
use DBI;
use JSON;
use Scalar::Util qw( looks_like_number );

require "config.pl";
require "general.pl";


my $query = new CGI;
print $query->header(-charset=>'utf-8');
eval   { Config::initialise_config(); };
if($@) { print encode_json({ error => $@ }); }

#database requires
my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");
my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");


my $userid = $query->param("id");
my $opcoid = $query->param("opcoid");
my $roleids = $query->param("roleids");
my $customerids = $query->param("customerids");

print "About to save 1234<br>";

saveuseropcoid( $userid, $opcoid );

print "Saving roles ...<br>\n";

# delete the old roles first
deleteuserroles( $userid, $opcoid );

# save the roleids 
my @roles = split(/,/, $roleids);
my $role = 0;
foreach ( @roles ) {
	if ( looks_like_number ( $_ )) {
		saveuserrole( $userid, $opcoid, $_ );
	}
}

print "Saving customers ...<br>\n";
deleteusercustomers( $userid, $opcoid );

# save the roleids 
my @customers = split(/,/, $customerids);
my $customer = 0;
foreach ( @customers ) {
	if ( looks_like_number ( $_ )) {
		saveusercustomer( $userid, $_ );
	}
}

print "Done<br>";


##############################################################################
#      saveuseropcoid 
#
##############################################################################
sub saveuseropcoid {

	my $userid = $_[0];
	my $opcoid = $_[1];
	
       return 0 if !General::is_valid_id( $userid );
   return 0 if !General::is_valid_id( $opcoid ); 

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ INSERT INTO USEROPCOS (USERID, OPCOID) 
    			SELECT * FROM (SELECT ? as userid, ? as opcoid) AS tmp
    			WHERE NOT EXISTS (
    				SELECT userid, opcoid FROM USEROPCOS where userid = ? and opcoid = ? 
    			) LIMIT 1 };
	my $sth = $dbh->prepare( $sql );
	
	$sth->execute( $userid, $opcoid, $userid, $opcoid );

	


	$sth->finish();
	
	$dbh->disconnect();
}

##############################################################################
#      deleteuserroles
#
##############################################################################
sub deleteuserroles {

	my $userid = $_[0];
	my $opcoid = $_[1];

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ DELETE FROM USERROLES 
			WHERE userid = ? 
			AND opcoid = ? };
	my $sth = $dbh->prepare( $sql );
	
	$sth->execute( $userid, $opcoid );

	$sth->finish();
	$dbh->disconnect();
}

##############################################################################
#      saveuserrole 
#
##############################################################################
sub saveuserrole {

	my $userid = $_[0];
	my $opcoid = $_[1];
	my $roleid = $_[2];

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ INSERT INTO USERROLES (USERID, OPCOID, ROLEID) 
    			SELECT * FROM ( SELECT ? as userid, ? as opcoid, ? as roleid ) AS tmp
    			WHERE NOT EXISTS (
    				SELECT userid, opcoid, roleid FROM USERROLES where userid = ? and opcoid = ? and roleid = ?
    			) LIMIT 1 };
	my $sth = $dbh->prepare( $sql );
	
	$sth->execute( $userid, $opcoid, $roleid, $userid, $opcoid, $roleid );

	$sth->finish();
	$dbh->disconnect();
}

##############################################################################
#      saveusercustomer 
#
##############################################################################
sub saveusercustomer {

	my $userid 	= $_[0];
	my $customerid 	= $_[1];

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ INSERT INTO customeruser ( customerid, userid ) 
    			SELECT * FROM ( SELECT ? as customerid, ? as userid ) AS tmp
    			WHERE NOT EXISTS (
    				SELECT customerid, userid FROM CUSTOMERUSER where customerid = ? and userid = ?
    			) LIMIT 1 };
	my $sth = $dbh->prepare( $sql );
	
	$sth->execute( $customerid, $userid, $customerid, $userid );

	$sth->finish();
	$dbh->disconnect();
}

##############################################################################
#      deleteusercustomers
#
##############################################################################
sub deleteusercustomers {

	my $userid = $_[0];
	my $opcoid = $_[1];

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ DELETE FROM customeruser 
			WHERE userid = ? 
			AND customerid IN ( SELECT id FROM CUSTOMERS WHERE OPCOID = ?)
			};
	my $sth = $dbh->prepare( $sql );
	
	$sth->execute( $userid, $opcoid );

	$sth->finish();
	$dbh->disconnect();
}

