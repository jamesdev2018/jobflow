#!/usr/local/bin/perl -w

use strict;
use warnings;

use CGI;
use JSON;
use POSIX qw(strftime);

use lib '../modules';
use Scalar::Util qw( looks_like_number );

require "config.pl";

my $query = new CGI;

print $query->header(-charset=>'utf-8');

eval   { Config::initialise_config(); };
if($@) { print encode_json({ error => $@ }); }

	my $serveraddress       = Config::getConfig("serveraddress");
	my $webportocol         = Config::getConfig("web_protocol");

    my $userid  = $query->param( 'userid' );
    my $opcoid  = $query->param( 'opcoid' );
    my $mode    = $query->param( 'mode' );
    my $messagecount = 0;
    my $jobno = 0;
    my $watchlistcnt = 0;
	my $assigntomecnt = 0;
	my $createdbycount = 0;
	my $employeeid = '';
	my $servertimestamp;
	my $get_messageday;

	my $vars;
	my $t0 = time();
	eval {

		if ( ( defined $userid ) && looks_like_number( $userid ) && ( $userid != 0 ) )
		{
			( $get_messageday, $servertimestamp ) = $Config::dba->process_oneline_sql("SELECT nummessagedays, now() FROM users WHERE id=?",[ $userid ]);

			if ( $get_messageday > 0 ) {
				( $messagecount ) = $Config::dba->process_oneline_sql(
					"SELECT COUNT(messageid) FROM messages WHERE touserid=? AND status=0 AND fromuserid <> ? AND rec_timestamp >= DATE_SUB(CURDATE(), INTERVAL ?  DAY)",
							[ $userid, $userid, $get_messageday ] );
			} else {
				( $messagecount ) = $Config::dba->process_oneline_sql(
					"SELECT COUNT(messageid) FROM messages WHERE touserid=? AND status=0 AND fromuserid <> ? ",[ $userid, $userid ] );
			}
			($jobno) = $Config::dba->process_oneline_sql(
				"SELECT job_number FROM messages WHERE touserid=? AND status=0 order by messageid desc limit 1", [ $userid ]);

			($watchlistcnt)= $Config::dba->process_oneline_sql("SELECT count(*)  FROM jobnotification WHERE USERID=?", [ $userid ]);

			($assigntomecnt) = $Config::dba->process_oneline_sql("SELECT COUNT( t3p.job_number ) FROM  tracker3plus t3p
                WHERE  t3p.job_number IN ( SELECT DISTINCT ah.job_number FROM assignmenthistory ah WHERE ( ah.status IN ( 'A', 'W', 'P', 'R' ) AND ( ah.assigneeid = ? ) ))
				AND ( ( t3p.opcoid = ? ) OR ( t3p.location = ? ) OR ( ( t3p.location = ? ) AND EXISTS ( SELECT 1 FROM shards s WHERE s.job_number = t3p.job_number AND s.location = ? ) ) )",
                [ $userid, $opcoid, $opcoid, $opcoid, $opcoid ]);

			# For time being, dont dynamically update either Assigned to Me count or Created by Me count
			# as these have a severely detrimental impact on getmessagecount which is run very frequently.

			# ($employeeid) = $Config::dba->process_oneline_sql(
			#	"SELECT employeeid FROM users WHERE id = ?", [ $userid ] );
			# $employeeid =~ s/^\s*//g; # Just in case its spaces
			#if ( $employeeid ne '' ) { # Dont bother unless the user has at least a non blank employeeid, as its a wasteful SQL query
			#	($createdbycount) = $Config::dba->process_oneline_sql(
			#		"SELECT count(t.job_number) as createdbycount FROM users u " .
			#		"	INNER JOIN tracker3 t ON t.employeeid = ? " .
			#		"	INNER JOIN tracker3plus t3p ON t3p.job_number= t.job_number " .
			#		" WHERE t3p.opcoid = ? AND t3p.rec_timestamp >= DATE_SUB(CURDATE(), INTERVAL 30  DAY) ", 
			#	[ $employeeid, $opcoid ] );
			#}
    	}
		my $jobflowURL      = "$webportocol" . "$serveraddress";
		$vars = {
			messagecount	=> $messagecount,
			GetMessageCount	=> 1,
			watchlistcnt	=> $watchlistcnt,
			jobflowURL		=> $jobflowURL,
			jobno			=> $jobno,
			assigntomecnt	=> $assigntomecnt,
			createdbycount	=> $createdbycount,
			employeeid		=> $employeeid,
			servertimestamp => $servertimestamp,
		}
	};
	if ( $@ ) {
		my $error = $@;
		$vars = { error => $error, userid => $userid, opcoid => $opcoid, mode => $mode };
	}
	$vars->{elapsed} = time() - $t0;
	
	my $json = encode_json ( $vars );
	print $json;

1;

