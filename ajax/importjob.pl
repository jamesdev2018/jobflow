#!/usr/local/bin/perl


use warnings;
use strict;

package ImportJobAjax;

use CGI;
use CGI::Session;
use DBI;
use JSON;
use Spreadsheet::Read;
use DateTime::Format::Excel;
use Data::Dumper;
use Time::HiRes qw/time/;
use File::Path qw(make_path remove_tree);
use Text::ParseWords;


$CGI::POST_MAX = 1024 * 5000;

use lib "../modules";
require "config.pl";
require 'job_import.pl';
require 'databaseJobImport.pl';
require 'data_cacher.pl';

#################################################################
##  Entry point; shouldn't need editing except to add actions  ##
#################################################################
our $sid;
our $session;
our $query;
our $header_printed = 0;


eval {
	$query = new CGI;
	print $query->header(-charset=>'utf-8', -type=>'application/json');
	$header_printed = 1;
	
	initialise($query->cookie("CGISESSID"));

	my $action = lc($query->param('action') || 'extract_spreadsheet_info');
	execute_actions($action);
};
if($@) {
	my $error = $@;
	
	print "Content-Type: application/json; charset=utf-8\n\n" if !$header_printed;
	
#	$session->param("file", "");
#	$session->param("job_import_filename", "");
#	$session->param("job_import_filetitle", "");
	
	if(defined $session) {
		$session->param('job_import', $error);
		$session->flush();
	}

	print encode_json({ error => $error });
}


sub initialise {
	my ($cookie_id) = @_;
	
	$sid = $cookie_id;
	$session = new CGI::Session("driver:File", $sid, { Directory => '/tmp' } );
	Config::initialise_config();
	DataCache::initialise_cache(Config::getConfig('cache_dir'));		## Prepare the data cache
}


sub execute_actions {
	my ($action) = @_;
	my $default = 'extract_spreadsheet_info';
	$action ||= $default;
	
	##--------<<
	my $actions = {
					extract_spreadsheet_info	=> \&extract_spreadsheet_info,
					import_jobs				=> \&import_jobs,
					delete_jobs				=> \&remove_spreadsheet_entries,
					delete_route_jobs		=> \&remove_spreadsheet_route_entries,
					validate					=> \&get_validation_list,
					update_values			=> \&update_record_values,
					create_rule				=> \&create_rule,
					create_ruleset			=> \&create_ruleset,
					save_layout_data			=> \&save_layout_data,
					test						=> sub { sleep(2); print encode_json({ success => 1 }); },
					
					# Special helper macro
					get_progress				=> sub {
													my $dets = $session->param('import_details');
													$session->param('import_details', '');
													$session->flush();
													
													print encode_json({ progress => $session->param('import_progress') || 0, details => $dets || '' });
												},
				 };
	##--------<<
	
	die "No actions available for '$action'\n" if !$actions->{$action};
	return $actions->{$action}->();
}

#####################################################
##  Extract spreadsheet data for the mapping tool  ##
#####################################################






#####################################################
##  Extract spreadsheet data for the mapping tool  ##
#####################################################

## Query params: file
sub extract_spreadsheet_info {
	my $filetitle = $query->param("file");
	my $filehandle = $query->upload("file");
	
	my $fileroot = Config::getConfig('job_import_spreadsheet_dir');
	$fileroot .= '/' if $fileroot !~ m/\/$/;
	make_path($fileroot);
	
	my $filename = $fileroot . $filetitle;
	$session->param('job_import_filename', $filename);
	$session->param('job_import_filetitle', '' . $filetitle); ## This does some weird globbing if we don't stringify it
	$session->flush();
	
	if($filehandle) {
		open TEMPFILE, ">$filename" or die "$! (File '$filename')";
			binmode TEMPFILE;
			while(<$filehandle>) { print TEMPFILE; }
		close TEMPFILE;
	}
	
	my $book = Spreadsheet::Read->new($filename);
	my $sheetcount = $book->sheets;
	#print "Sheets: $sheetcount<br>\n";
	
	my @sheets;
	foreach my $sheet ($book->sheets) {
		my $sh = { name => $sheet, contents => [] };
		my $count = 0;
		foreach my $row ($book->sheet($sheet)->rows) {
			push(@{ $sh->{contents} }, $row);
			last if $count++ > 20; # Don't bother returning more than 20 lines
		}
		
		push(@sheets, $sh);
	}
	
	my @existing_mapping = ECMDBJobImport::get_spreadsheet_mapping($filetitle);
	
	print encode_json({ sheets => \@sheets, maps => \@existing_mapping });
}




#####################################################
##  Import spreadsheet data given a field mapping  ##
#####################################################

## Query params: sheetid, mapping, start_row
sub import_jobs {
	my $import_filename = $session->param('job_import_filename');
	my $import_filetitle = $session->param('job_import_filetitle');
	
	$session->param('import_progress', 0);
	$session->flush();
	
	my $book = Spreadsheet::Read->new($import_filename);
	my $sheetid = $query->param('sheetid') || '1';
	
	##### mapping.push({ field : field, type : maptype, columns : JSON.stringify(slots) });
	my $column_mappings = decode_json($query->param('mapping'));
	my @rulesets = ECMDBJobImport::get_rulesets($session->param('customerid'), $session->param('clientid'));
	
	
	# Make sure the 5 mandatory fields are filled
		my $search_ids = {};
		my $extra_fields = {};
		foreach my $field_name (('customer', 'client', 'campaign', 'mediatype', 'country', 'opco')) {
			# Get the current field for the given mediatype/customer
			my ($field_info) = ECMDBJobImport::get_import_fields($session->param('mediatypeid'), $session->param('customerid'), $field_name);
			die "Field not found ($field_name)" if !$field_info;
			
			$search_ids->{$field_name . "id"} = $session->param($field_name . "id") || '';
			$extra_fields->{$field_info->{id}} = $session->param($field_name) || '';
		}
		
		$session->param('import_progress', 5);
		$session->flush();
	
	
	
	# Create a new import parent ('spreadsheet id')
		my $pairing_list = "";
		my $field_info;
		my %mapping_types = map { $_->{id} => $_->{mappinginfo} } ECMDBJobImport::get_mapping_types();
		foreach my $pairing (@$column_mappings) {
			$pairing->{mappinginfo} = $mapping_types{$pairing->{type}};
		
			$field_info->{$pairing->{field}} = ECMDBJobImport::get_import_field($pairing->{field});
		}
		my $importid = ECMDBJobImport::create_spreadsheet_import($import_filetitle, $sheetid, undef, $session->param('userid') || 262, $column_mappings);
		
		$session->param('import_progress', 10);
		$session->flush();
		
	
	
	# Import the spreadsheet records
		my $sheet = $book->sheets($sheetid);
		my $started = 0;
		my @jobs;
		
		
		my @new_records;
		my @failures;
		
		my $max = $book->sheet($sheetid)->maxrow;
		my $i = 0;
		
		my $burn_count = $query->param('start_row') || 1;		## Burn at least 1 row (i.e. the header row)
		foreach my $row ($book->sheet($sheetid)->rows) {
			if($burn_count-- > 0) { $i++; next; }
			
			$session->param('import_progress', 10 + int((90 / $max) * $i++));
			$session->flush();
			
			
			# Pre-fill with the mandatory field data (i.e. customer, client, etc)
				my $record = {};
				my $validation_data = {};
				foreach my $fieldid (keys %$extra_fields) { $record->{$fieldid}->{content} = $validation_data->{$fieldid} = $extra_fields->{$fieldid}; }
			
			
			# Map data, perform mapping, simple substitution and basic type validation
				my $invalid = '';
				foreach my $pairing (@$column_mappings) {
					my $data = '';
					eval {
						$data = map_data($pairing, $row);
						$data = type_validate_data($field_info->{$pairing->{field}}, $data);
						my $modified_data = substitute_data($pairing, $data);
						
						$record->{$pairing->{field}} = { modified_content => $modified_data, content => $data };
						$validation_data->{$pairing->{field}} = $modified_data;
					};
					if($@) {
						my $error = $@;
						
						my @letters;
						foreach my $column (@{ $pairing->{columns} }) {
							my $index = $column->{column} + 1;
							my $major_letter = $index > 26 ? chr(int(($index - 1) / 26) + 64) : '';
							my $minor_letter = chr((($index - 1) % 26) + 65);
							push(@letters, "$major_letter$minor_letter");
						}
						
						$invalid .= "Cell " . join('/', @letters) . $i . " ('$data'): $error";
						last;
					}
				}
			
				# Don't add the data if it can't even type-validate basically
				if($invalid)		{ push(@failures, $invalid); next }
			
			
			# Apply rulesets (substitute several fields at a time where requirements match)
				foreach my $ruleset (@rulesets) {
					#warn "Testing $ruleset->{id}";
					## Check to see if the ruleset is applicable
					my $matching_requirements = 0;
					foreach my $req (@{ $ruleset->{requirements} }) {
						my $content = $record->{$req->{fieldid}}->{modified_content} || $record->{$req->{fieldid}}->{content} || '';
						if(uc($content) eq uc($req->{value})) {
							$matching_requirements++;
						} else {
							#warn "!! '$content' doesn't match '$req->{value}'";
						}
					}
					
					## Apply the replacements if all the requirements match
					if($matching_requirements == scalar(@{ $ruleset->{requirements} })) {
						#warn "Ruleset matches. Applying substitutions";
						
						foreach my $rep (@{ $ruleset->{replacements} }) {
							#warn "Applying substitution '$rep->{value}' to field $rep->{fieldid}";
							$record->{$rep->{fieldid}} = { content => '' } if !exists $record->{$rep->{fieldid}};
							$record->{$rep->{fieldid}}->{modified_content} = $rep->{value};
							$validation_data->{$rep->{fieldid}} = $rep->{value};
							
							## TODO: Make auto values automatic
							$record->{$rep->{fieldid}}->{contentid} = $rep->{id} if $rep->{id} ne 'auto';
						}
					}
				}
			
			# Validate/get IDs for the fields
			# We have to do this after the data is already mapped because valid values can depend on other data
			my $debuginfo = '';
			my @order = ECMDBJobImport::order_validations(keys %$record);
			$debuginfo = "\n\nFields: " . join(', ', @order) . "\n";
			
				foreach my $fieldid (@order) {
					my ($validated_id, $comment) = verify_validate_data($fieldid, $validation_data);
					
					if($validated_id) {
						$record->{$fieldid}->{contentid} = $validated_id;
						$validation_data->{$fieldid} = $validated_id;		## If we have an ID, use that for validation entries over basic text
					} else {
						#die "Tried validating '$record->{$fieldid}->{content}'";
					}
					
					$record->{$fieldid}->{comment} = $comment;
				}
			
			push(@new_records, ECMDBJobImport::add_spreadsheet_record($importid, $search_ids, $record));
		}
		
		$session->param('import_progress', 95);
		$session->param('current_spreadsheet_id', $importid);
		$session->flush();
		print encode_json({ new_ids => \@new_records, failures => \@failures });
};



sub map_data {
	my ($mapping_data, $rd) = @_;
	my @row_data = @$rd;
	
	##### mapping.push({ field : field, type : maptype, columns : JSON.stringify(slots) });
	my @columns = map { $row_data[$_->{column}] } @{ $mapping_data->{columns} };
	
	my $data = process_formula($mapping_data->{mappinginfo}, \@columns);
	return $data;
}


sub type_validate_data {
	my ($field, $data) = @_;
	
	
	## By Number
	if($field->{fieldtype} eq 'number') {
		die "Destination field <span class='tableheader'>$field->{title}</span> can only accept numbers\n" if($data !~ m/^[0-9]+(\.[0-9]+)?$/);
	
	## By Date/Time
	} elsif($field->{fieldtype} eq 'datetime') {
		die "Destination field <span class='tableheader'>$field->{title}</span> can only accept dates\n" if($data !~ m/^[0-9]+(\.[0-9]+)?$/);
		$data = DateTime::Format::Excel->parse_datetime($data)->iso8601();
	
	## By Date alone
	} elsif($field->{fieldtype} eq 'date') {
		die "Destination field <span class='tableheader'>$field->{title}</span> can only accept numbers\n" if($data !~ m/^[0-9]+(\.[0-9]+)?$/);
		$data = DateTime::Format::Excel->parse_datetime($data)->ymd('-');
	
	## By Time
	} elsif($field->{fieldtype} eq 'time') {
		die "Destination field <span class='tableheader'>$field->{title}</span> can only accept numbers\n" if($data !~ m/^[0-9]+(\.[0-9]+)?$/);
		$data = DateTime::Format::Excel->parse_datetime($data)->hms();
	}
	
	return $data;
}

sub substitute_data {
	my ($mapping_data, $data) = @_;
	
	return ECMDBJobImport::get_substitution($mapping_data->{field}, $data || '') || $data;
}

sub verify_validate_data {
	my ($fieldid, $validation_data) = @_;
	
	## Get the validation list
		my ($v, $ud);
		eval { ($v, $ud) = JobImport::construct_validation_list($session, $fieldid, $validation_data); };
		return (undef, "Error: $@") if $@;
		
	## Check to see if our data is in there
		my @vlist = @$v;
		foreach my $valid_entry (@vlist) {
			return ($valid_entry->{id}, "Valid") if $valid_entry->{text} eq ($validation_data->{$fieldid} || '');
		}
		
		if(!scalar @vlist) {
			my @fields;
			foreach my $field (@$ud) {
				my $field = ECMDBJobImport::get_import_field($field);
				push(@fields, $field->{title});
			}
			
			my $deplist = 'No validation required';
			$deplist = "Unmet dependencies: " . join(', ', @fields) if scalar @fields;
			
			return (undef, $deplist);
		}
		
	return (undef, "No match found");
}



#############################################
##  Get lists of valid values for a given set of fields  ##
#############################################

sub get_validation_list {
	my $recordid = $query->param('recordid');
	my @fieldids = @{ decode_json($query->param('fieldids')) };
	my $values = decode_json($query->param('values'));
	
	my $vlist = {};
	my $record = ECMDBJobImport::get_spreadsheet_record($recordid);
	delete $record->{id}; delete $record->{status};
	
	foreach my $fieldid (@fieldids) {
		my $data = { $fieldid => '' };
		
		## Build a list of values from the record
		foreach my $record_fieldid (keys %$record) {
					
			$data->{$record_fieldid} = $record->{$record_fieldid}->{content_id} ||
										$record->{$record_fieldid}->{content};
		}
		
		
		## Add any unsaved changes
		foreach my $key (keys %$values) {
			$data->{$key} = substr($values->{$key}, 0, index($values->{$key}, ';'));
		}
		
		
		$vlist->{$fieldid} = [ @{ (JobImport::construct_validation_list($session, $fieldid, $data))[0] } ] if $fieldid;
	}
	
	print encode_json({ recordid => $recordid, records => $vlist });
}






sub update_record_values {
	my $records = decode_json($query->param('recordids'));
	
	my $complete_ids = "";
	my $debug = "";
	foreach my $recordid (@$records) {
		my $values = decode_json($query->param('values'));	## Create a new hash because it gets deleted; little hacky for now for the demo
		
		my $record = ECMDBJobImport::get_spreadsheet_record($recordid);
		delete $record->{id}; delete $record->{status};
		
		my $validation_data;
		foreach my $fieldid (keys %$record) {
			## Check for existence first, or else we auto-vivify the hash and that breaks things later
			if(exists $values->{$fieldid} && $values->{$fieldid}->{text}) { 
				$validation_data->{$fieldid} = $values->{$fieldid}->{text};
			} else {
				$validation_data->{$fieldid} = $record->{$fieldid}->{content};
			}
		}
		
		# Validate/get IDs for the fields
		# We have to do this after the data is already mapped because valid values can depend on other data
		my @order = ECMDBJobImport::order_validations(keys %$record);
		foreach my $fieldid (@order) {
			my ($validated_id, $comment) = verify_validate_data($fieldid, $validation_data);
			
			if($validated_id) {
				$values->{$fieldid}->{id} = $validated_id;
				$validation_data->{$fieldid} = $validated_id;		## If we have an ID, use that for validation entries over basic text
				$values->{$fieldid}->{comment} = "Valid";
			} else {
				$values->{$fieldid}->{comment} = $comment;
			}
		}
		
		
		ECMDBJobImport::update_spreadsheet_record($recordid, $values);
		$complete_ids .= "$recordid, ";
	}
	
	print encode_json({ records => $complete_ids, debug => $debug });
}


sub create_rule {
	my ($fieldid) = $query->param('fieldid');
	my ($text) = $query->param('oldvalue');
	my ($replacement) = $query->param('newvalue');
	
	my $ruleid = ECMDBJobImport::add_substitution($fieldid, $text, $replacement, $session->param('customerid'), $session->param('clientid'));
	ECMDBJobImport::apply_substitutions($ruleid, $session->param('customerid'), $session->param('clientid'));
	
	print encode_json({ success => 1 });
}

sub create_ruleset {
	my $requirements = decode_json($query->param('requirements') || '');
	my $substitutions = decode_json($query->param('substitutions') || '');
	
	my $rulesetid = ECMDBJobImport::add_ruleset($session->param('customerid'), $session->param('clientid'), undef, $requirements, $substitutions);
	
	
#	# Apply rulesets (substitute several fields at a time where requirements match)
#		my @rulesets = ECMDBJobImport::get_rulesets($session->param('customerid', $session->param('clientid')));
#		
#		foreach my $ruleset (@rulesets) {
#			#warn "Testing $ruleset->{id}";
#			## Check to see if the ruleset is applicable
#			my $matching_requirements = 0;
#			foreach my $req (@{ $ruleset->{requirements} }) {
#				my $content = $record->{$req->{fieldid}}->{modified_content} || $record->{$req->{fieldid}}->{content} || '';
#				if(uc($content) eq uc($req->{value})) {
#					$matching_requirements++;
#				} else {
#					#warn "!! '$content' doesn't match '$req->{value}'";
#				}
#			}
#			
#			## Apply the replacements if all the requirements match
#			if($matching_requirements == scalar(@{ $ruleset->{requirements} })) {
#				#warn "Ruleset matches. Applying substitutions";
#				
#				foreach my $rep (@{ $ruleset->{replacements} }) {
#					#warn "Applying substitution '$rep->{value}' to field $rep->{fieldid}";
#					$record->{$rep->{fieldid}} = { content => '' } if !exists $record->{$rep->{fieldid}};
#					$record->{$rep->{fieldid}}->{modified_content} = $rep->{value};
#					$validation_data->{$rep->{fieldid}} = $rep->{value};
#					
#					## TODO: Make auto values automatic
#					$record->{$rep->{fieldid}}->{contentid} = $rep->{id} if $rep->{id} ne 'auto';
#				}
#			}
#		}
#	
#	# Validate/get IDs for the fields
#	# We have to do this after the data is already mapped because valid values can depend on other data
#	my $debuginfo = '';
#	my @order = ECMDBJobImport::order_validations(keys %$record);
#	$debuginfo = "\n\nFields: " . join(', ', @order) . "\n";
#	
#		foreach my $fieldid (@order) {
#			my ($validated_id, $comment) = verify_validate_data($fieldid, $validation_data);
#			
#			if($validated_id) {
#				$record->{$fieldid}->{contentid} = $validated_id;
#				$validation_data->{$fieldid} = $validated_id;		## If we have an ID, use that for validation entries over basic text
#			} else {
#				#die "Tried validating '$record->{$fieldid}->{content}'";
#			}
#			
#			$record->{$fieldid}->{comment} = $comment;
#		}
#	
#	push(@new_records, ECMDBJobImport::add_spreadsheet_record($importid, $search_ids, $record));
	
	print encode_json({ id => $rulesetid });
}









sub save_layout_data {
	ECMDBJobImport::save_column_layout($session->param('userid'),
										{	opcoid => $session->param('opcoid') || undef,
											customerid => $session->param('customerid') || undef,
											clientid => $session->param('clientid') || undef,
											campaignid => $session->param('campaignid') || undef,
											countryid => $session->param('countryid') || undef,
											mediatypeid => $session->param('mediatypeid') || undef,
										},
									$query->param('layout_data'),
								);
								
	print encode_json({ success => 1 });
}



sub remove_spreadsheet_entries {
	my $records = decode_json($query->param('recordids'));
	
	my $count = 0;
	my @ids;
	foreach my $recordid (@$records) {
		push(@ids, $recordid) if ECMDBJobImport::remove_spreadsheet_record($recordid);
		$count++;
	}
	
	print encode_json({ success => 1, count => $count, ids => \@ids });
}

sub remove_spreadsheet_route_entries {
	
	ECMDBJobImport::remove_spreadsheet_records(
										{	opcoid => $session->param('opcoid') || undef,
											customerid => $session->param('customerid') || undef,
											clientid => $session->param('clientid') || undef,
											campaignid => $session->param('campaignid') || undef,
											countryid => $session->param('countryid') || undef,
											mediatypeid => $session->param('mediatypeid') || undef,
										});
										
	print encode_json({ success => 1 });
}











### Create a result by applying a formula to a given set of input columns. Used for advanced mapping of data
sub process_formula {
	my ($formula, $inputs) = @_;
	
	my $local_memory = {	inputs => $inputs,
						variables => { },
						result => '',
						temp => '',
					};
	
	foreach my $operation (quotewords('\s+', 1, $formula)) {
		my @actions;
		
		#print "> $operation\n";
		
		my $safety = 100;
		while($operation =~ m/^(\.([a-z]+))?\(([^)]*)\)(.*)$/ && $safety--) {
			push(@actions, { operator => $2, argument => $3 });
			$operation = $4;
		}
		
		$local_memory->{temp} = '';
		foreach my $action (@actions) {
			operate($action->{operator}, $action->{argument}, $local_memory);
		}
		
		#print "\n\n";
	}
	
	
	return $local_memory->{result};
}


sub operate {
	my ($operator, $argument, $memory) = @_;
	
	$operator = lc($operator ||= 'set');
	
	#print "> '$memory->{temp}'.$operator($argument)    >>    ";
	
	## Variable loading/storing
	if($operator eq 'set')			{ $memory->{temp} = get($memory, $argument);
	} elsif($operator eq 'name')		{ $memory->{variables}->{$argument} = $memory->{temp};
	
	## Parsing
	} elsif($operator eq 'before')	{ my $arg = get($memory, $argument); my $find = index($memory->{temp}, $arg); $memory->{temp} = substr($memory->{temp}, 0, $find) if $find >= 0;
	} elsif($operator eq 'after')		{ my $arg = get($memory, $argument); my $find = index($memory->{temp}, $arg); $memory->{temp} = substr($memory->{temp}, $find + length($arg)) if $find >= 0;
	} elsif($operator eq 'trim')		{ $memory->{temp} =~ s/((^ +)|( +$))//g;
	} elsif($operator eq 'truncate')	{ $memory->{temp} = substr($memory->{temp}, 0, get($memory, $argument));
	} elsif($operator eq 'round')		{ $memory->{temp} = sprintf(get($memory, $argument), $memory->{temp});
	
	## Maths
	} elsif($operator eq 'multiply')	{ $memory->{temp} *= get($memory, $argument);
	} elsif($operator eq 'divide')	{ $memory->{temp} /= get($memory, $argument);
	} elsif($operator eq 'add')		{ $memory->{temp} += get($memory, $argument);
	} elsif($operator eq 'subtract')	{ $memory->{temp} -= get($memory, $argument);
	
	## Commit the output
	} elsif($operator eq 'print')		{ $memory->{result} .= $memory->{temp};
	}
	
	#print "'$memory->{temp}'\n";
	return $memory->{temp};
}

sub get {
	my ($memory, $value) = @_;
	
	if($value =~ m/^'(.*)'$/)			{ my $q = $1 || ''; $q =~ s/\\//g; return $q;		# String literal ## Allow ' and " (which need to be escaped) in arguments
	} elsif($value =~ m/^\$([a-z]+)$/)	{ return $memory->{variables}->{$1} || '';		# Saved variable
	} elsif($value =~ m/^([0-9]+)$/)		{ return @{ $memory->{inputs} }[$1 - 1] || '';	# input data
	}
}























1;
