#!/usr/local/bin/perl -w

use warnings;
use strict;

use lib "../modules";

use CGI;
use DBI;
use JSON;

require "config.pl";

my $query = new CGI;
print $query->header(-charset=>'utf-8');
eval   { Config::initialise_config(); };
if($@) { print encode_json({ error => $@ }); }

#database requires
my $database 		= Config::getConfig("databaseschema");
my $data_source 	= Config::getConfig("databaseserver");
my $db_username 	= Config::getConfig("databaseusername");
my $db_password 	= Config::getConfig("databasepassword");

my $opcoid = $query->param("opcoid");

print ( getCustomers( $opcoid, "json" ) );

##############################################################################
#      getCustomers 
#
##############################################################################
sub getCustomers {

	my @customers;
	my ( $opcoid, $mode, $customerid,  $customer);
	
	$opcoid = $_[0];
	$mode 	= $_[1];

	my $dbh = DBI->connect( "DBI:mysql:database=$database;host=$data_source", $db_username, $db_password, { RaiseError => 1 } );
	my $sql = qq{ select c.id, c.customer
			from customers c  
			where opcoid = ?
			order by c.customer };
	my $sth = $dbh->prepare( $sql );
	
	$sth->execute( $opcoid );

	$sth->bind_columns( \$customerid, \$customer);

	while ( $sth->fetch() ) {
		my %row = (
			id		=> $customerid,
			customer	=> $customer,
		);
		push ( @customers, \%row );

	}

	$sth->finish();
	$dbh->disconnect();

	if ( $mode eq "json") {
		return ( encode_json( \@customers ) );
	} else {
		return @customers;
	}
}

